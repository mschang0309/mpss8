package mpss.plan;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mpss.common.jpa.*;
import mpss.common.dao.*;
import mpss.ac.*;

// for Spring component scanning
@Service
public class MissionPlan
{
	  // for Spring injection
    @Autowired
    protected SatelliteDao satelliteDao;
    
	  @Autowired
	  protected SiteDao siteDao;
	  
	@Autowired
	protected OrbitEventsDao oeDao;
	    
	@Autowired
	protected ActivityRequestListDao actReqListDao;
	   
	@Autowired ExternalactivityrequestinfoDao earDao;
	
    @Autowired
    protected PsDao psDao;
    
    @Autowired
    protected SessionManDao sessionManDao;
    
    @Autowired
    protected MonitoringActivityCreator monitoringActivityCreator;
    
    @Autowired
    protected ManeuverActivityCreator maneuverActivityCreator;
    
    @Autowired
    protected RsiImagingActivityCreator rsiImagingActivityCreator;
    
    // current session
    Session session;
	
	public MissionPlan()
	{
		// default constructor must exist for Spring
	}
	
	public void setCurrentSession(String sessionName)
	{
        this.session = sessionManDao.getSessionByName(sessionName); 
	}
	
	// get all satellites referenced in this session
	public List<Satellite> getRefSatellites()
	{
		return sessionManDao.getRefSatellites(this.session.getName());
	}
	
	public List<Arl> getSatActReqListXrefs(String satName)
	{
		return sessionManDao.getRefSatelliteARLsByName(session.getName(), satName);
	}
	
	// get all sites referenced in this session
	public List<Site> getRefSites()
	{
		return sessionManDao.getRefSites(this.session.getName());
	}
	
	// satellite related parameters used by current session 
	public Satellitep getSatelliteParameters(String satName)
	{
		// get parameter set of current session
		Parameterset ps = sessionManDao.getParameterset(this.session.getName());
		
		// get satellite PS from the parameter set
		Satellitep sps = this.psDao.getSatellitep(ps.getName(), satName);
		return sps;
	}

	// satellite related parameters used by current session 
	public Siteps getSiteParameters(String siteName)
	{
		// get parameter set of current session
		Parameterset ps = sessionManDao.getParameterset(this.session.getName());

		// get site PS from the parameter set
		Siteps sps = this.psDao.getSitep(ps.getName(), siteName);
		return sps;
	}

	// generate cold schedule activities of the satellite within this session
	public boolean createColdActivitiesPerSatellite(Satellite satellite)
	{
		// create monitoring activities from S band contacts
		  System.out.println("create monitoring activities from S band contacts");
		  monitoringActivityCreator.setSatellite(satellite);
		  monitoringActivityCreator.setSession(this.session);
		  List<Activity> acts = monitoringActivityCreator.create("");
		  for (Activity a : acts)
		  {
			  System.out.printf("act name: %s\n", a.getName());
		  }
		  
		  // create activities from referenced ARLs
		  System.out.println("create activities from referenced ARLs");
		  // 1. RSI Imaging

		  // 2. ISUAL Imaging
		  
		  // 3. Maneuvering

		  // 4. Real Time Proc

		  // 5. X-Band Cross

		  // 6. Uplink
		  
		  return true;
	}

	// generate hot schedule activities from a mission time line
	public boolean createHotActivitiesFromExternalRequests(String mtlName)
	{		
		// get ARL by name
		Arl arl = actReqListDao.getArl(mtlName);
		if (arl == null)
		{
			return false;
		}
		Externalactivityrequestinfo ear = earDao.getByArlId(arl.getId().intValue());
		Satellite satellite = satelliteDao.getByName(ear.getSatellite());
      
		@SuppressWarnings("unused")
		List<Activity> acts = null;	    
		
		System.out.println("create maneuver activities from: " + mtlName);
	    maneuverActivityCreator.setSatellite(satellite);
	    maneuverActivityCreator.setSession(this.session);
		acts = maneuverActivityCreator.create(mtlName);
  		
	    System.out.println("create RSI activities from: " + mtlName);
		rsiImagingActivityCreator.setSatellite(satellite);
		rsiImagingActivityCreator.setSession(this.session);
		acts = rsiImagingActivityCreator.create(mtlName);

    return true;
	}

}
