package mpss;

import org.apache.log4j.Logger;

public class config {	
	
	public static void load()
	{		
		try{				
			String copeProps = javae.AppDomain.getPath("config.properties", new String[]{"conf",configFactory.getSatelliteName()});
            javae.AppProperties.load(copeProps);			
		} catch (Exception e) {		
			Logger logger = Logger.getLogger(configFactory.getLogName());
			logger.error("Load config ex ="+e.getMessage());
		}   
	}
    
}