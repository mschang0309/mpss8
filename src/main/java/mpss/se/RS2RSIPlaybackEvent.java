package mpss.se;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

import com.google.common.collect.Range;

import mpss.common.jpa.Extent;
import mpss.util.timeformat.TimeUtil;

@Scope ( "prototype" )
@Component("rs2RSIPlaybackEvent")
public class RS2RSIPlaybackEvent extends ScheduledEventBase
{
	
	public RS2RSIPlaybackEvent()
	{
	    // assume this will be a secondary REC - event generator will correct if
	    // it is in fact an initial REC
	    this.eventMnemonic = "RSI_PBK";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		logger.info("Extent[" + extent.getId() +"] is RS2RSIPlaybackEvent");	
		// base method
		super.generate(extent);

	    int rsiPbkSetupDelay = this.satelliteps.getRsipbsetupdelay();
	    int rsiPbkCleanupDelay = this.satelliteps.getRsipbcleanupdelay();
	    
	    //this.extentDuration = (int) ((extent.getEndtime().getTime() - extent.getStarttime().getTime()) / 1000);
	    // update duration - remove setup/cleanup delays (in seconds)
	    this.duration = this.duration  - rsiPbkSetupDelay - rsiPbkCleanupDelay;
	    // update setup duration
	    this.setupDuration = rsiPbkSetupDelay;
	
	    // offset event time by setup duration
	    this.eventStartTime = TimeUtil.timeAppendSec(this.eventStartTime, this.setupDuration);
	
	    	
	    // parm ?1  - if first transmit event in sequence turn on TMI and enable RF
	    //  defaults to "%" - updated by event generator if required
		this.eventParms.add("%");
		  
		// get imaging mode of PBK.  Must request from ClActivity not ARLEntry since the
	    // REC can request PAN+MS imaging, and can playback each mode separately OR together.
		String mode = this.activity.getInfo();
		
		// parms ?2 and ?3 are dependent on the imaging mode being played back
	    if (mode.equalsIgnoreCase("PAN")) 
	    {
	    	// parm ?2 is PAN filename
	    	this.eventParms.add( Integer.toString(this.extent.getFilename()));
	    
	    	// parm ?3 is MS filename
	    	this.eventParms.add("%");
	    
	    	// parm $4 - SSR video PAN channel status selection
			this.eventParms.add("YES");
	    
			// parm $5 - SSR video MS channel status selection (NO since PAN PBK)
		    this.eventParms.add("NO");
	    }
	    else if (mode.equalsIgnoreCase("MS")) 
	    {
	    	// parm ?2 is PAN filename
	    	this.eventParms.add("%");
	    
	    	// parm ?3 is MS filename
	    	this.eventParms.add( Integer.toString(this.extent.getFilename()));
	    
	    	// parm $4 - SSR video PAN channel status selection (NO since MS PBK)
		    this.eventParms.add("NO");
	    
		 // parm $5 - SSR video MS channel status selection
		    this.eventParms.add("YES");
	
	    }
	    else  // assume PAN+MS mode
	    {
	    	// parm ?2 is PAN filename
	    	this.eventParms.add( Integer.toString(this.extent.getFilename()));
	    
	    	// parm ?3 is MS filename
	    	this.eventParms.add( Integer.toString(this.extent.getSecondfilename()));
	    
	    	// parm $4 - SSR video PAN channel status selection 
			this.eventParms.add("YES");
	    
			// parm $5 - SSR video MS channel status selection
		    this.eventParms.add("YES");
	    }
	
	    // parm ?6 - if last transmit event in sequence turn off TMI and disable RF
	    //  -- defaults to "%" udpated by event generator if needed
		this.eventParms.add("%");
	    
		//System.out.println(eventParms);
	}
	
	  @Override
	  public Range<Timestamp> getOperationalPeriod()
	  {
	      // operational period of rsi record does not include the ssr setup and
	      // cleanup.  Need to get the ssr setup and cleanup times and remove them
	      // from extent times
	      
	      // get ssr setup/cleanup times
	      int ssrSetup = this.satelliteps.getRsirecsetupdelay();
	      int ssrCleanup = this.satelliteps.getRsireccleanupdelay();
	      
	      // determine start/stop times considering ssr setup/cleanup times
	      Timestamp start = new Timestamp(extent.getStarttime().getTime() + ssrSetup * 1000);
	      Timestamp stop = new Timestamp(extent.getEndtime().getTime() - ssrCleanup * 1000);
	      
	      return Range.closed(start, stop);  
	  }
}