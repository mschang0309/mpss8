package mpss.se;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;




import mpss.common.jpa.Extent;

@Scope ( "prototype" )
@Component("rs2SSRDeleteEvent")
public class RS2SSRDeleteEvent extends ScheduledEventBase
{
	public RS2SSRDeleteEvent()
	{
	    this.eventMnemonic = "SSR_DEL";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		logger.info("Extent[" + extent.getId() +"] is SSRDeleteEvent");	
		// base method
		super.generate(extent);
		
	  // parm $1 - first file name 
		String szFilename = Integer.toString(this.extent.getFilename());
		this.eventParms.add(szFilename);
		
    // parm ?2 - optional second filename
		if (hasSecondFilename(extent))
		{
        this.eventParms.add(Integer.toString(this.extent.getSecondfilename()));
		}
		else
		{
			this.eventParms.add("%");
		}
		
	}
}