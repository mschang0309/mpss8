package mpss.se;

import java.util.*;
import java.sql.Timestamp;

import com.google.common.collect.Range;

import mpss.common.jpa.*;

public interface ScheduledEvent extends java.lang.Cloneable
{
	public List<String>	getEventParms();
	public void setEventParms(List<String> eventMnemonic);
	public String getEventMnemonic();
	public void setEventMnemonic(String eventMnemonic);
	public Extent getExtent();
	public void setEventStartTime(Timestamp eventStartTime);
	public Timestamp getEventStartTime();
	public void setDuration(int duration);
	public int getDuration();
	public void setExtentDuration(int extentDuration);
	public int getExtentDuration();
	public void setSetupDuration(int setupDuration);
	public int getSetupDuration();
	public void setExtent(Extent extent);
	public Activity getActivity();
	public void setActivity(Activity activity);
	public Satellite getSatellite();
	public void setSatellite(Satellite satellite);
	public Rev getRev();
	public void setRev(Rev rev);
	public Site getSite();
	public void setSite(Site site);
	public Contact getContact();
	public void setContact(Contact contact);
	public Satellitep getSatelliteps();
	public void setSatelliteps(Satellitep satelliteps);
	public void setSession(Session session);
	public Session getSession();
	
	
	
	public void generate(Extent extent);
	public void generate(Contact contact);
	public Object clone()  throws CloneNotSupportedException;
	
	public boolean hasSecondFilename(Extent extent);
	public Range<Timestamp> getOperationalPeriod();
	
	public void dump();
}