package mpss.se;

import java.util.*;
import java.sql.Timestamp;

import org.springframework.stereotype.Service;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.google.common.collect.Range;

import mpss.config;
import mpss.common.jpa.*;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeUtil;

@Service("fs8scheduledEventGenerator")
public class FS8ScheduledEventGenerator extends ScheduledEventGeneratorBase
{	
	
	public FS8ScheduledEventGenerator()
	{
		try
		{
			config.load();	
			// need modify for satellite by jeff  start  -------------
			
			this.schedEventBeans.put("UPLINK", "rs5UplinkEvent");
			this.schedEventBeans.put("AIPPROC", "rs5CmdProcEvent");
			this.schedEventBeans.put("AIPPBK", "rs5AIPPlaybackEvent");
			this.schedEventBeans.put("RSI:REC", "rs5RSIRecordEvent");
			this.schedEventBeans.put("RSI:DDT", "rs5RSIDDTEvent");
			this.schedEventBeans.put("RSI:PBK", "rs5RSIPlaybackEvent");
			this.schedEventBeans.put("MANEUVER:ATTITUDE", "rs5ManeuverEvent");
			this.schedEventBeans.put("MANEUVER:GOHOME", "rs5SupManeuverEvent");
			this.schedEventBeans.put("XCROSS", "rs5XCrossEvent");
			this.schedEventBeans.put("DELETE", "rs5SSRDeleteEvent");
			this.schedEventBeans.put("MANEUVER:ORBIT", "rs5OrbitManeuverEvent");
			this.schedEventBeans.put("RTCMD", "rs5CmdProcEvent");
			this.schedEventBeans.put("MISC", "rs5CmdProcEvent");
			
			// need modify for satellite by jeff  end  -------------
		}
		catch (Exception e)
		{
			logger.info(e.getMessage());
		}
	}
	
	public boolean generate(List<Session> schSessions)
	{	
		logger.info("FS8 Scheduled Event Generator beginning...");		
		scheduledEvents.clear();
		
		// get parameter set associated with schedule and related resources (todo: constellation)
		this.satelliteps = satellitepDao.getBySatIdNPsId(satId,schSessions.get(0).getPsid());
		
		// generate events from the different types of extents
		generateExtentEvents(schSessions);

		//generate events from scheduled contacts
		generateContactEvents(schSessions);
		
    	Collections.sort(scheduledEvents,
	    		new Comparator<ScheduledEvent>() 
	    		{
	    			public int compare(ScheduledEvent e1, ScheduledEvent e2) 
	    			{
	    				//return e1.getExtent().getStarttime().compareTo(e2.getExtent().getStarttime());
	    				return e1.getEventStartTime().compareTo(e2.getEventStartTime());
	    			}
	    		}
	    	);
    	
		for (ScheduledEvent se:scheduledEvents)
		{
    		se.dump();
		}
		
		return true;
		
	}

	// create scheduled events for a session
	private void generateExtentEvents(List<Session> sessions)
	{
		// Spring application context
		ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf"}));
					
		// result caches
		List<ScheduledEvent> rsiRecEvents = new ArrayList<ScheduledEvent>();
		List<ScheduledEvent> rsiPbkEvents = new ArrayList<ScheduledEvent>();
		List<ScheduledEvent> rsiDDTEvents = new ArrayList<ScheduledEvent>();
		List<ScheduledEvent> imagingEvents = new ArrayList<ScheduledEvent>();
		List<ScheduledEvent> xmitEvents = new ArrayList<ScheduledEvent>();
		List<ScheduledEvent> xcrossEvents = new ArrayList<ScheduledEvent>();
		
		for (Session sess : sessions)
		{
			// get all extents within a session
			List<Extent> extents = this.schDao.listSessionExtents(sess);
			
			// create scheduled event from extent
			for (Extent extent : extents)
			{
				
				String actType = getActivityType(extent);
				
				// create scheduled event dynamically
				ScheduledEvent schedEvent;
			    try 
			    {
			    	String beanName = this.schedEventBeans.get(actType);
			    	if (beanName == null)
			    	{
			    		logger.info("Extent[" + extent.getId() + "] Activity type code not supported: " + actType);	
						continue;
			    	}
			    	
			    	schedEvent = (ScheduledEvent)context.getBean(beanName);
			    	//schedEvent = (ScheduledEvent)((ScheduledEvent)context.getBean(beanName)).clone();
			        if (schedEvent == null) {
			        	logger.info("load bean failed :"+ beanName);	
			        	return;
			        }
			       
			      // generate event specific contents
			      schedEvent.generate(extent);
			      
			      // cache scheduled events for post processing by ARL entry
			      Activity act = schedEvent.getActivity();
			      //System.out.println("Trace2-->Arl entry type:" + act.getId());
			      if (act.getArlentrytype().equalsIgnoreCase("RSI"))
			      {
			          // sub type
			          if (act.getActsubtype().equalsIgnoreCase("REC"))
			          {
			              rsiRecEvents.add(schedEvent);
			              imagingEvents.add(schedEvent);
			              xmitEvents.add(schedEvent);
			              
			          }
			          else if (act.getActsubtype().equalsIgnoreCase("PBK"))
			          {
			              rsiPbkEvents.add(schedEvent);
			              xmitEvents.add(schedEvent);
			              
			          }
			          else if (act.getActsubtype().equalsIgnoreCase("DDT"))
			          {
			              rsiDDTEvents.add(schedEvent);
			              imagingEvents.add(schedEvent);
			              xmitEvents.add(schedEvent);
			              
			          }
			            
			      }
			      else if (act.getArlentrytype().equalsIgnoreCase("XCROSS"))
			      {
			    	  xcrossEvents.add(schedEvent);
			    	  
			      }
			      // no need for post processing, add to results directly
			      else
			      {
			    	  //System.out.println("schedEvent:" + schedEvent.getEventStartTime());
			          this.scheduledEvents.add(schedEvent);
			      }
			       
		    	} catch (Exception ex) {
		    		ex.printStackTrace();
		    	}
			    
			}
		}
		
		// post processing for specific scheduled events
		//updateRecAndDDTAndPbkEventParams(xmitEvents); // mark by jeff 20170802 , use updateRecAndDDTAndPbkEventParamsGroupByEclipse
		// group by eclipse by jeff 20170802
		updateRecAndDDTAndPbkEventParamsGroupByEclipse(xmitEvents);
		//updateRecAndDDTEventParams(imagingEvents);
		updateRecAndDdtEventParams(imagingEvents);
		Map<Integer, List<ScheduledEvent> > sequenceEvents = updatePbkAndDDTEventParams(rsiPbkEvents,rsiDDTEvents);
		updateXCrossEventParams(sequenceEvents,xcrossEvents);

		this.scheduledEvents.addAll(rsiRecEvents);
		this.scheduledEvents.addAll(rsiPbkEvents);
		this.scheduledEvents.addAll(rsiDDTEvents);
		this.scheduledEvents.addAll(xcrossEvents);
	
	}
	
	// create scheduled events for a session
	private void generateContactEvents(List<Session> sessions)
	{
		// Spring application context
		ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf"}));
				
		List<ScheduledEvent> contactEvents = new ArrayList<ScheduledEvent>();;
		for (Session sess : sessions)
		{
			// get all contacts within a session
			List<Contact> contacts = this.schDao.listSessioncontacts(sess);
	
			for (Contact cont : contacts)
		    {
		        // if the contact starts within the schedule period
	//	        if(mySchedule.GetTimeRange().Contains(cont.getRise()))
	//	        {
		        	ScheduledEvent schedEvent;
				    try 
				    {				    	
				    	// need modify for satellite by jeff  start  -------------
				    	
				    	schedEvent = (ScheduledEvent)context.getBean("rs5AcquisitionStartEvent");
					    schedEvent.generate(cont);
				    	contactEvents.add(schedEvent);
				    	schedEvent = (ScheduledEvent)context.getBean("rs5AcquisitionEndEvent");
					    schedEvent.generate(cont);
				    	contactEvents.add(schedEvent);
				    	
				    	// need modify for satellite by jeff  end  -------------
				    } catch (Exception ex) {			    		
			    		logger.info(ex.getMessage());	
			    	}
	//		    }
				    
		    }
		}
		this.scheduledEvents.addAll(contactEvents);		
	}
	
	
	// 20170330 by jeff , one template , group by eclipse
	private void updateRecAndDdtEventParams(List<ScheduledEvent> imagingEvents){
		
		if (imagingEvents.size() == 0)
			return;
		
		// sort extent starttime
	    Collections.sort(imagingEvents,
	    		new Comparator<ScheduledEvent>() 
	    		{
	    			public int compare(ScheduledEvent e1, ScheduledEvent e2) 
	    			{
	    				return e1.getExtent().getStarttime().compareTo(e2.getExtent().getStarttime());	    				
	    			}
	    		}
	    );
		
		int sequenceIndex = -1;
		// Grouping collection
	    Map<Integer, List<ScheduledEvent>> sequenceEvents = new HashMap<Integer, List<ScheduledEvent> >();	    
	    // Grouping by eclipse
	    Range<Timestamp> currentPenExitRange=Range.closed(new Timestamp (0), new Timestamp (0));
	    Range<Timestamp> penExitRange=null;	 
	    List<ScheduledEvent> curSeqEvents=null;
	    for (ScheduledEvent se : imagingEvents){
	    	//List<Timestamp> tr = orbitEventsDao.getAdjacentPenumbraExits(se.getSatellite().getId(), se.getEventStartTime());
	    	List<Timestamp> tr = orbitEventsDao.getAdjacentPenumbraentrance(se.getSatellite().getId(), se.getEventStartTime());
	        if (tr.get(0) != null && tr.get(1) != null){
	        	  try
	              {
	            	  penExitRange= Range.closed(tr.get(0), tr.get(1));	
	              }
	              catch(Exception e)
	              {
	            	  logger.info(e.getMessage());	
	              }
	        	  
	        	  logger.info("currentPenExitRange:" + currentPenExitRange.toString());	
	        	  logger.info("penExitRange:" + penExitRange.toString());
	        	  
	              
	              if (!penExitRange.equals(currentPenExitRange)){
                      sequenceIndex++;
	            	  currentPenExitRange = penExitRange;	            	  
	            	  curSeqEvents = new ArrayList<ScheduledEvent>();
	            	  curSeqEvents.add(se);
	            	  sequenceEvents.put(sequenceIndex, curSeqEvents);
	              }else{	            	  
	            	  sequenceEvents.get(sequenceIndex).add(se);	            	  
	              }
	          }
	          else if (tr.get(0) != null)
	          {
	        	  try
	              {
	            	  penExitRange= Range.closed(Timestamp.valueOf("2010-01-01 00:00:00"), tr.get(0));	
	              }
	              catch(Exception e)
	              {
	            	  System.out.println(e.getMessage());
	              }
	              if (!penExitRange.equals(currentPenExitRange))
	              {
	            	  sequenceIndex++;
	            	  currentPenExitRange = penExitRange;	            	  
	            	  curSeqEvents = new ArrayList<ScheduledEvent>();
	            	  curSeqEvents.add(se);
	            	  sequenceEvents.put(sequenceIndex, curSeqEvents);
	              }
	              else
	              {
	            	  sequenceEvents.get(sequenceIndex).add(se);
	              }
	          }
	          else
	          {
	        	  logger.info("ScheduledEventGenerator error: PenumbraExits is null");
	          }
	    	
	    }
	    
	    // update Rec & DDT Sequence Parameters 
	    for(int inx=0; inx < sequenceEvents.size(); inx++){
	    	
	    	// need modify for satellite by jeff  start---------------------
	    		
	    		if(sequenceEvents.get(inx).size()>0){
	    			ScheduledEvent lastEvent = sequenceEvents.get(inx).get(sequenceEvents.get(inx).size() - 1);
		    		ScheduledEvent firstEvent = sequenceEvents.get(inx).get(0);
		    		// RSI_DDT
		    		if(lastEvent.getEventMnemonic().toUpperCase().contains("RSI_DDT")){             	  
		             	  if (lastEvent.getEventParms().size()>16){
		             		  lastEvent.getEventParms().set(16, "OFF");
		             	  }
		            }else{// RSI_REC	            	            	  
		            	if (lastEvent.getEventParms().size()>13){
		             		  lastEvent.getEventParms().set(13, "OFF");
		             	}
		            }
		              // get the event parameters from the current schedule event
		     	    List<String> firstParmVec= firstEvent.getEventParms();
		     	    firstParmVec.set(0,"ON");
		     	      
		     	    // update event parameters for first event in list
		     	    sequenceEvents.get(inx).get(0).setEventParms(firstParmVec);	
	    		}
	    		    		
	    		   
	     // need modify for satellite by jeff  end---------------------
	    }	    
	    logger.info("ScheduledEventGenerator updateRecAndDdtEventParams completed");	    
	}
	
	
	// 20170330 not use by jeff change updateRecAndDdtEventParams() , REC use 2 template,  DDT use 2 template 
	@SuppressWarnings("unused")
	private void updateRecAndDDTEventParams(List<ScheduledEvent> imagingEvents)
	{
		if (imagingEvents.size() == 0)
			return;
		
		// imaging groups are determined by their separation (min RFPA interval)
		int minSeparation = this.satelliteps.getMinrfpainterval();
		  
		// need to get the start of the imaging of the activity - not the start of the extent
		// when determining imaging range duration
		Range<Timestamp> currentImagingTime = imagingEvents.get(0).getOperationalPeriod();	
		
	    int sequenceIndex = 0;
	    Map<Integer, List<ScheduledEvent> > sequenceEvents = new HashMap<Integer, List<ScheduledEvent> >();
	    
	    // group record and ddt activities into sequences based on RFPA duration
	    for (ScheduledEvent se : imagingEvents)
	    {
	    	// Operational period does not contain the setup or cleanup times, which we don't want
	        Range<Timestamp> nextImagingTime = se.getOperationalPeriod();
	        // if the next imaging time is within the minimum separation from the end 
	        // of the previous (current) imaging activity, then they are in the same sequence
	        List<ScheduledEvent> curSeqEvents = sequenceEvents.get(sequenceIndex);
	        if(nextImagingTime.lowerEndpoint().getTime() <= (currentImagingTime.upperEndpoint().getTime() + minSeparation*1000))
	        {
	        	// empty
	        	if (curSeqEvents == null)
	        	{
	        		curSeqEvents = new ArrayList<ScheduledEvent>();
	        		curSeqEvents.add(se);
	        		sequenceEvents.put(sequenceIndex, curSeqEvents);
	        	}
	        	else
	        	{
	        		curSeqEvents.add(se);
	        	}
	        }
	        // next sequence
	        else 
	        {
	        	sequenceIndex++;
	        	curSeqEvents = new ArrayList<ScheduledEvent>();
	        	curSeqEvents.add(se);
	        	sequenceEvents.put(sequenceIndex, curSeqEvents);
	        }
         // reset the currentImagingTime to the "next" and continue on to the next imaging activity in the list
         currentImagingTime = nextImagingTime;
         logger.info("[" + sequenceIndex + ":" + se.getActivity().getId() + "]" + se.getEventStartTime() + ",EventMnemonic="+se.getEventMnemonic());
         
	    }	  
   
	    // update Rec & DDT Sequence Parameters 
	    Range<Timestamp> currentPenExitRange=Range.closed(new Timestamp (0), new Timestamp (0));
	    boolean firstImage = false;

//	    for (List<ScheduledEvent> curSeqEvents : sequenceEvents.values()) 
	    for(int inx=0; inx < sequenceEvents.size(); inx++)
	    {
	    	// sort se events in time order, based off events start time
	    	Collections.sort(sequenceEvents.get(inx),
	    		new Comparator<ScheduledEvent>() 
	    		{
	    			public int compare(ScheduledEvent e1, ScheduledEvent e2) 
	    			{
	    				return e1.getExtent().getStarttime().compareTo(e2.getExtent().getStarttime());
	    				//return e1.getActivity().getStarttime().compareTo(e2.getActivity().getStarttime());
	    			}
	    		}
	    	);
                           
          // determine first image in current revolution
          // get the penumbra exit times surrounding the time of the first event in the sequence
          //List<Timestamp> tr = orbitEventsDao.getAdjacentPenumbraExits(sequenceEvents.get(inx).get(0).getSatellite().getId(), sequenceEvents.get(inx).get(0).getEventStartTime());
          List<Timestamp> tr = orbitEventsDao.getAdjacentPenumbraentrance(sequenceEvents.get(inx).get(0).getSatellite().getId(), sequenceEvents.get(inx).get(0).getEventStartTime());
          Range<Timestamp> penExitRange=null;

          if (tr.get(0) != null && tr.get(1) != null)
          {
        	  try
              {
            	  penExitRange= Range.closed(tr.get(0), tr.get(1));	
              }
              catch(Exception e)
              {
            	  System.out.println(e.getMessage());
              }
        	  logger.info("currentPenExitRange:" + currentPenExitRange.toString());
        	  logger.info("penExitRange:" + penExitRange.toString());              
              // if this is the first sequence in the rev
              if (!penExitRange.equals(currentPenExitRange))
              {
            	  currentPenExitRange = penExitRange;
            	  firstImage = true;
              }
              // else this is not the first sequence in the rev
              else
              {

            	  firstImage = false;
              }
          }
          else if (tr.get(0) != null)
          {
        	  try
              {
            	  penExitRange= Range.closed(Timestamp.valueOf("2010-01-01 00:00:00"), tr.get(0));	
              }
              catch(Exception e)
              {
            	  System.out.println(e.getMessage());
              }
              if (!penExitRange.equals(currentPenExitRange))
              {
            	  currentPenExitRange = penExitRange;
            	  firstImage = true;
              }
              else
              {
            	  firstImage = false;
              }
          }
          else
          {
        	  firstImage = true;
          }
          
          
          // need modify for satellite by jeff  start--------------------------
          
        	  // last rec/ddt in sequence must "turn off video channel"     
              ScheduledEvent lastEvent = sequenceEvents.get(inx).get(sequenceEvents.get(inx).size() - 1);              
              // RSI_DDT_SEC or RSI_DDT_INIT
              if(lastEvent.getEventMnemonic().toUpperCase().contains("RSI_DDT"))
              {             	  
             	  if (lastEvent.getEventParms().size()>16){
             		  lastEvent.getEventParms().set(16, "OFF");
             	  }
              }
              else
              {
            	  // RSI_REC_SEC or RSI_REC_INIT             	  
             	  if (lastEvent.getEventParms().size()>13){
             		  lastEvent.getEventParms().set(13, "OFF");
             	  }
              }
              // get the event parameters from the current schedule event
     	      List<String> firstParmVec= sequenceEvents.get(inx).get(0).getEventParms();
     	      firstParmVec.set(0,"ON");
     	      
     	      // update event parameters for first event in list
     	     sequenceEvents.get(inx).get(0).setEventParms(firstParmVec);   
     	     
     	     
     	 // need modify for satellite by jeff  end--------------------------  
     	     
         
	      // if this is the first image event in the current rev
	      if (firstImage)
	      {	    	  
	    	  String eventMnem = sequenceEvents.get(inx).get(0).getEventMnemonic();
	    	  eventMnem = eventMnem.replace("SEC","INIT");
	    	  sequenceEvents.get(inx).get(0).setEventMnemonic(eventMnem);
	      }
	      
	    }
	}

	private Map<Integer, List<ScheduledEvent> > updatePbkAndDDTEventParams(List<ScheduledEvent> rsiPbkEvents,List<ScheduledEvent> rsiDDTEvents)
	{	
		Map<Integer, List<ScheduledEvent> > pbkContactMap = new HashMap<Integer, List<ScheduledEvent> >();
		for (ScheduledEvent pbkEvent : rsiPbkEvents)
		{
			List<ScheduledEvent> pbkContEvents = pbkContactMap.get(pbkEvent.getContact().getId().intValue());
			if (pbkContEvents == null)
			{
				pbkContEvents = new ArrayList<ScheduledEvent>();
				pbkContEvents.add(pbkEvent);
				pbkContactMap.put(pbkEvent.getContact().getId().intValue(), pbkContEvents);
			}
			else
			{
				pbkContEvents.add(pbkEvent);
			}
		}
		
		// group playback and ddt activities into sequences based on DDT and contact time overlap
		
	    // get contact from each pbkcontact in map, and get contact time range.  Attempt to
	    // find all ddt extents that overlap contact time (AOS-LOS).  If found then DDTs and
	    // associated contact pbks are 1 sequence

		// sequence counter - preset to number of sequences in list
	    int sequenceIndex = 0;
	    Map<Integer, List<ScheduledEvent> > sequenceEvents = new HashMap<Integer, List<ScheduledEvent> >();
	    List<ScheduledEvent> remainDDTEvents = new ArrayList<ScheduledEvent>();
	    remainDDTEvents.addAll(rsiDDTEvents);
		for (Integer contid : pbkContactMap.keySet())
		{
			//remainDDTEvents.clear();
			List<ScheduledEvent> pbkAndDDTEvents = pbkContactMap.get(contid);
			Contact cont = contactDao.get((long)contid);
			// if DDTs time range (extents range) overlaps contact, then add ddts events to the vector with the playbacks
			for (ScheduledEvent ddtEvent:rsiDDTEvents)
			{
				if (cont.getRise().compareTo(ddtEvent.getExtent().getEndtime()) < 0 
					&& cont.getFade().compareTo(ddtEvent.getExtent().getStarttime()) > 0)
				{
					pbkAndDDTEvents.add(ddtEvent);
					remainDDTEvents.remove(ddtEvent);
				}
//				else
//				{
//					remainDDTEvents.add(ddtEvent);
//				}
			}
			sequenceEvents.put(sequenceIndex, pbkAndDDTEvents);
			sequenceIndex++;
//			rsiDDTEvents=remainDDTEvents;
//			rsiDDTEvents.clear();
//			rsiDDTEvents.addAll(remainDDTEvents);
		}
		
		// Group DDTs into sequences by RFPA durations and DDT occurrences
		if (remainDDTEvents.size()>0)
//		if (rsiDDTEvents.size()>0)
		{
			// imaging groups are determined by their separation (min RFPA interval)
			int minSeparation = this.satelliteps.getMinrfpainterval();
			  
			// need to get the start of the imaging of the activity - not the start of the extent
			// when determining imaging range duration
	 	    Range<Timestamp> currentImagingTime = rsiDDTEvents.get(0).getOperationalPeriod();
	 	    
		    // group record and ddt activities into sequences based on RFPA duration
		    for (ScheduledEvent se : rsiDDTEvents)
		    {
		    	// Operational period does not contain the setup or cleanup times, which we don't want
		        Range<Timestamp> nextImagingTime = se.getOperationalPeriod();
		        // if the next imaging time is within the minimum separation from the end 
		        // of the previous (current) imaging activity, then they are in the same sequence
		        List<ScheduledEvent> curSeqEvents = sequenceEvents.get(sequenceIndex);
		        if(nextImagingTime.lowerEndpoint().getTime() <= (currentImagingTime.upperEndpoint().getTime() + minSeparation*1000))
		        {
		        	// empty
		        	if (curSeqEvents == null)
		        	{
		        		curSeqEvents = new ArrayList<ScheduledEvent>();
		        		curSeqEvents.add(se);
		        		sequenceEvents.put(sequenceIndex, curSeqEvents);
		        	}
		        	else
		        	{
		        		curSeqEvents.add(se);
		        	}
		        }
		        // next sequence
		        else 
		        {
		        	sequenceIndex++;
		        	curSeqEvents = new ArrayList<ScheduledEvent>();
		        	curSeqEvents.add(se);
		        	sequenceEvents.put(sequenceIndex, curSeqEvents);
		        }
	         // reset the currentImagingTime to the "next" and continue on to the next imaging activity in the list
	         currentImagingTime = nextImagingTime;
		    }	
		}
		
//		for (List<ScheduledEvent> curSeqEvents : sequenceEvents.values())
		for(int inx=0; inx < sequenceEvents.size(); inx++)
	    {
	    	// sort se events in time order, based off events start time
	    	Collections.sort(sequenceEvents.get(inx),
	    		new Comparator<ScheduledEvent>() 
	    		{
	    			public int compare(ScheduledEvent e1, ScheduledEvent e2) 
	    			{
//	    				return e1.getEventStartTime().compareTo(e2.getEventStartTime());
	    				return e1.getExtent().getStarttime().compareTo(e2.getExtent().getStarttime());
	    			}
	    		}
	    	);
	    	
	    	// first pbk/ddt in sequence must "turn on RF"
	    	List<String> firstParmVec= sequenceEvents.get(inx).get(0).getEventParms();
	    	if (sequenceEvents.get(inx).get(0).getEventMnemonic().contains("RSI_DDT"))
	    	{
	    		firstParmVec.set(1,"ON");
	    	}
	    	else	// must be RSI_PBK
	    	{
	    		firstParmVec.set(0,"ON");
	    	}
	    	sequenceEvents.get(inx).get(0).setEventParms(firstParmVec);
	    	
	    	// last pbk/ddt in sequence must "turn off RF and switch Xband antenna"     
	    	ScheduledEvent lastEvent = sequenceEvents.get(inx).get(sequenceEvents.get(inx).size() - 1);
	    	List<String> lastParmVec= lastEvent.getEventParms();
	    	if(lastEvent.getEventMnemonic().toUpperCase().contains("RSI_DDT"))
            {
	    		if (lastParmVec.size() > 17)
	    			lastParmVec.set(17, "OFF");
            }
            else	// must be RSI_PBK
            {
            	if (lastParmVec.size() > 3)
            		lastParmVec.set(3, "OFF");
            }
	    	lastEvent.setEventParms(lastParmVec);
	    }
		return sequenceEvents;
	}	
  
	private void updateXCrossEventParams(Map<Integer, List<ScheduledEvent> > sequenceEvents, List<ScheduledEvent> xcrossEvents) 
	{
		Map<TimeRange, List<ScheduledEvent>> RFseqEventsMap = new HashMap<TimeRange, List<ScheduledEvent>>();
		// only continue if there are sequence events
	    if(sequenceEvents.size() == 0)
	        return;

	    // get TWTA preheat delay - includes warm up for RF
	    int twta = satelliteps.getTwtaheatdelay();

	    // retrieve sequences from sequence map, get time window of events in map.
	    // Subtract TWTA preheat from start, then create map of "RF" time and sequence
//	    for(int iter : sequenceEvents.keySet())
	    for(int iter=0; iter < sequenceEvents.size(); iter++)
	    {
	    	ScheduledEvent firstEvent = sequenceEvents.get(iter).get(0);
	    	ScheduledEvent lastEvent = sequenceEvents.get(iter).get(sequenceEvents.get(iter).size() -1);

	        // get time of first event in sequence
	        Timestamp seqStart = firstEvent.getEventStartTime();

	        // need to subtract off any setup times that would have been removed from the event time
	        // now subtract off twta preheat from first events start
	        seqStart = TimeUtil.timeAppendSec(seqStart, - (firstEvent.getSetupDuration() + twta));
	        
	        // get end time of last event - since events remove all setup/cleanup durations
	        // I need to add them back in to get the correct endtime.
	        Timestamp seqEnd = TimeUtil.timeAppendSec(lastEvent.getEventStartTime(),lastEvent.getExtentDuration()- lastEvent.getSetupDuration());

	        TimeRange sequenceRange = new TimeRange(seqStart, seqEnd);
	        RFseqEventsMap.put(sequenceRange, sequenceEvents.get(iter));
	    }
	    
	    
	    
	 // iterate through each xcross event, determine if it overlaps any pbks or ddts
	    for(ScheduledEvent xEv : xcrossEvents)
	    {
	        // will hold event parameters for xcross events
	        List<String> eventParms = xEv.getEventParms();

	        // get xcross event time window
	        Timestamp xcrossStart = xEv.getEventStartTime();
	        Timestamp xcrossStop = TimeUtil.timeAppendSec(xcrossStart, xEv.getDuration());
	        TimeRange xcrossWindow = new TimeRange(xcrossStart, xcrossStop);

//	        int pbkOrDDTCount = 0;

	        boolean RFcommands = false;
	        boolean SSRpause = false;

	        // iterate through pbk/DDT sequences to determine the following if the xcross
	        //      occurs during a sequence
	        //      if YES: RF = true
	        //              1. does XCROSS overlap PBK or DDT
	        //                  a. is XCROSS contained within PBK or DDT 
	        //                      YES - overlap = true
	        //                  b. does XCROSS overlap PBK or DDT start/stop
	        //                      YES - is XCROSS during setup/cleanup
	        //                          NO overlap = true
	        //
	        //      if NO: RF= false, overlap = false
	        //
	        // iterate through pbks first
	        for(TimeRange rfWindow : RFseqEventsMap.keySet())
	        { 
	        	logger.info("RF window: " + rfWindow);
	        	logger.info("XCROSS window: " + xcrossWindow );      	

	            // check if xcross event (start + duration) overlaps any part of the RF sequence
	            if(xcrossWindow.overlaps(rfWindow))
	            {
	            	logger.info("xcross overlaps rfwindow");  

	                RFcommands = true;
	                // now we need to know what part of the RF window the xcross overlaps
	                // 1. TWTA preheat period
	                // 2. contained within PBK or DDT event
	                // 3. overlap PBK or DDT setup period
	                // 4. overlap PBK or DDT stop

	                // check if contained in any of the pbk or ddt events
	                // iterate through all events in sequence
	                for(ScheduledEvent rfSE : RFseqEventsMap.get(rfWindow))
	                {
	                    // get event time window
	                	Timestamp eventStart = rfSE.getEventStartTime();
	                	Timestamp eventStop = TimeUtil.timeAppendSec(eventStart, rfSE.getDuration());
	                    TimeRange eventRange = new TimeRange(eventStart, eventStop);

	                    if(eventRange.contains(xcrossStart))
	                    {
	                        SSRpause = true;
	                    }
	                }
	            }
	        }
	                            
	        // update event parms to indicate overlapping PBK or DDT
	        if(SSRpause == true)
	        {
	        	eventParms.set(0,"XmitPause_Required");
	        }

	        if(RFcommands == true)
	        {
	        	eventParms.set(1,"RF_Required");
	        }

//	        // if I updated the event parameters re-apply them to the xcross event
//	        if(SSRpause == true || RFcommands == true)
//	        {
//	            (*xEv)->SetEventParms(eventParms);
//	        }
	    }
	} 
	
	// group by eclipse
	private void updateRecAndDDTAndPbkEventParamsGroupByEclipse(List<ScheduledEvent> xmitEvents)
	{
		if (xmitEvents.size() == 0)
			return;
		
		// sort extent starttime
	    Collections.sort(xmitEvents,
	    		new Comparator<ScheduledEvent>() 
	    		{
	    			public int compare(ScheduledEvent e1, ScheduledEvent e2) 
	    			{
	    				return e1.getExtent().getStarttime().compareTo(e2.getExtent().getStarttime());	    				
	    			}
	    		}
	    );
		
		int sequenceIndex = -1;
		// Grouping collection
	    Map<Integer, List<ScheduledEvent>> sequenceEvents = new HashMap<Integer, List<ScheduledEvent> >();	    
	    // Grouping by eclipse
	    Range<Timestamp> currentPenExitRange=Range.closed(new Timestamp (0), new Timestamp (0));
	    Range<Timestamp> penExitRange=null;	 
	    List<ScheduledEvent> curSeqEvents=null;
	    for (ScheduledEvent se : xmitEvents){
	    	//List<Timestamp> tr = orbitEventsDao.getAdjacentPenumbraExits(se.getSatellite().getId(), se.getEventStartTime());
	    	List<Timestamp> tr = orbitEventsDao.getAdjacentPenumbraentrance(se.getSatellite().getId(), se.getEventStartTime());
	        if (tr.get(0) != null && tr.get(1) != null){
	        	  try
	              {
	            	  penExitRange= Range.closed(tr.get(0), tr.get(1));	
	              }
	              catch(Exception e)
	              {
	            	  logger.info("ScheduledEventGenerator updateRecAndDDTAndPbkEventParamsGroupByEclipse ex="+e.getMessage());	            	  
	              }
	        	  
	              
	              if (!penExitRange.equals(currentPenExitRange)){
                      sequenceIndex++;
	            	  currentPenExitRange = penExitRange;	            	  
	            	  curSeqEvents = new ArrayList<ScheduledEvent>();
	            	  curSeqEvents.add(se);
	            	  sequenceEvents.put(sequenceIndex, curSeqEvents);
	              }else{	            	  
	            	  sequenceEvents.get(sequenceIndex).add(se);	            	  
	              }
	          }
	          else if (tr.get(0) != null)
	          {
	        	  try
	              {
	            	  penExitRange= Range.closed(Timestamp.valueOf("2010-01-01 00:00:00"), tr.get(0));	
	              }
	              catch(Exception e)
	              {
	            	  System.out.println(e.getMessage());
	              }
	              if (!penExitRange.equals(currentPenExitRange))
	              {
	            	  sequenceIndex++;
	            	  currentPenExitRange = penExitRange;	            	  
	            	  curSeqEvents = new ArrayList<ScheduledEvent>();
	            	  curSeqEvents.add(se);
	            	  sequenceEvents.put(sequenceIndex, curSeqEvents);
	              }
	              else
	              {
	            	  sequenceEvents.get(sequenceIndex).add(se);
	              }
	          }
	          else
	          {
	        	  logger.info("ScheduledEventGenerator updateRecAndDDTAndPbkEventParamsGroupByEclipse error: PenumbraExits is null");
	          }
	    	
	    }
	    
	 // update Sequence Parameters 
	    for(int inx=0; inx < sequenceEvents.size(); inx++){	    	
	    	// first rec/pbk/ddt in sequence, SSR exit Power Saving
	    	if(sequenceEvents.get(inx).size()>0){
	    		ScheduledEvent firstEvent = sequenceEvents.get(inx).get(0);
		    	if (firstEvent.getEventMnemonic().toUpperCase().contains("RSI_DDT"))
		    	{
		    		if (firstEvent.getEventParms().size()>18)
		    			firstEvent.getEventParms().set(18,"ON");
		    	}
		    	else if (firstEvent.getEventMnemonic().toUpperCase().contains("RSI_PBK"))
		    	{
		    		if (firstEvent.getEventParms().size() > 4)
		    			firstEvent.getEventParms().set(4,"ON");
		    	}
		    	else	// must be RSI_REC
		    	{
		    		if (firstEvent.getEventParms().size() > 14)
		    			firstEvent.getEventParms().set(14,"ON");
		    	}
		    	
		    	sequenceEvents.get(inx).get(0).setEventParms(firstEvent.getEventParms());
		    	ScheduledEvent lastEvent = sequenceEvents.get(inx).get(sequenceEvents.get(inx).size() - 1);	    		
		    	
		    	// last rec/pbk/ddt in sequence, SSR enter Power Saving 	    	
		    	if(lastEvent.getEventMnemonic().toUpperCase().contains("RSI_DDT"))
	            {
		    		if (lastEvent.getEventParms().size() > 19)
		    			lastEvent.getEventParms().set(19, "OFF");
	            }
		    	else if(lastEvent.getEventMnemonic().toUpperCase().contains("RSI_PBK"))
	            {
		    		if (lastEvent.getEventParms().size() > 5)
		    			lastEvent.getEventParms().set(5, "OFF");
	            }
	            else	// must be RSI_REC
	            {
	            	if (lastEvent.getEventParms().size() > 15)
	            		lastEvent.getEventParms().set(15, "OFF");
	            }	    	
		    	sequenceEvents.get(inx).get(sequenceEvents.get(inx).size() - 1).setEventParms(lastEvent.getEventParms());
	    	}		    	
	    }	    
	}
  
	// group by revid
	@SuppressWarnings("unused")
	private void updateRecAndDDTAndPbkEventParams(List<ScheduledEvent> xmitEvents)
	{
		Map<Integer, List<ScheduledEvent> > rsiOrbitMap = new HashMap<Integer, List<ScheduledEvent> >();
		for (ScheduledEvent rsiEvent : xmitEvents)
		{
			List<ScheduledEvent> rsiOrbitEvents = rsiOrbitMap.get(rsiEvent.getRev().getId().intValue());
			if (rsiOrbitEvents == null)
			{
				rsiOrbitEvents = new ArrayList<ScheduledEvent>();
				rsiOrbitEvents.add(rsiEvent);
				rsiOrbitMap.put(rsiEvent.getRev().getId().intValue(), rsiOrbitEvents);
			}
			else
			{
				rsiOrbitEvents.add(rsiEvent);
			}
		}
//		int i=0;
    	
		for (List<ScheduledEvent> curOrbitEvents : rsiOrbitMap.values()) 
	    {
	    	// sort se events in time order, based off events start time
	    	Collections.sort(curOrbitEvents,
	    		new Comparator<ScheduledEvent>() 
	    		{
	    			public int compare(ScheduledEvent e1, ScheduledEvent e2) 
	    			{
//	    				return e1.getEventStartTime().compareTo(e2.getEventStartTime());
	    				return e1.getExtent().getStarttime().compareTo(e2.getExtent().getStarttime());
	    			}
	    		}
	    	);
	    	
	    	// first rec/pbk/ddt in sequence, SSR exit Power Saving
	    	List<String> firstParmVec= curOrbitEvents.get(0).getEventParms();
	    	if (curOrbitEvents.get(0).getEventMnemonic().contains("RSI_DDT"))
	    	{
	    		if (firstParmVec.size() > 18)
	    			firstParmVec.set(18,"ON");
	    	}
	    	else if (curOrbitEvents.get(0).getEventMnemonic().contains("RSI_PBK"))
	    	{
	    		if (firstParmVec.size() > 4)
	    			firstParmVec.set(4,"ON");
	    	}
	    	else	// must be RSI_REC
	    	{
	    		if (firstParmVec.size() > 14)
	    			firstParmVec.set(14,"ON");
	    	}
	    	curOrbitEvents.get(0).setEventParms(firstParmVec);
	    	
	    	// last rec/pbk/ddt in sequence, SSR enter Power Saving     
	    	ScheduledEvent lastEvent = curOrbitEvents.get(curOrbitEvents.size() - 1);
	    	List<String> lastParmVec= lastEvent.getEventParms();
	    	if(lastEvent.getEventMnemonic().toUpperCase().contains("RSI_DDT"))
            {
	    		if (lastParmVec.size() > 19)
	    			lastParmVec.set(19, "OFF");
            }
	    	else if(lastEvent.getEventMnemonic().toUpperCase().contains("RSI_PBK"))
            {
	    		if (lastParmVec.size() > 5)
	    			lastParmVec.set(5, "OFF");
            }
            else	// must be RSI_REC
            {
            	if (lastParmVec.size() > 15)
            		lastParmVec.set(15, "OFF");
            }
	    	lastEvent.setEventParms(lastParmVec);
	    }

	}
	
	private String getActivityType(Extent extent)
	{
			String actType;
			
			// get activity on which the extent is based
			Activity act = this.activityDao.get(extent.getActivityid().longValue());
			
			// maneuver activity
			if (act.getArlentrytype().equalsIgnoreCase("MANEUVER"))
			{
			    // get maneuver type from source ARL entry
			    Maneuverrequest man = this.maneuverrequestDao.get(act.getArlentryid().longValue());
				if (man == null){
					return "";
				}
			    String manType = man.getType();
			    if (manType.equalsIgnoreCase("ATTITUDE"))
			    {
			    	  actType = "MANEUVER:ATTITUDE";
			    }
			    else if (manType.equalsIgnoreCase("GOHOME"))
			    {
			    	  actType = "MANEUVER:GOHOME";
			    }
			    else if (manType.equalsIgnoreCase("ORBIT"))
			    {
			    	  actType = "MANEUVER:ORBIT";
			    }
			    else
			    {
			    	actType = "MANEUVER:UNKNOWN";			    	
			    }
			}
			// other activity
			else
			{
			    // has subtype (RSI, ISUAL)
			    if (!act.getActsubtype().equalsIgnoreCase("NO"))
			    {
			    	actType = act.getArlentrytype() + ":" + act.getActsubtype();
			    }
			    // no subtype
			    else
			    {
			    	actType = act.getArlentrytype();			    	
			    }
			    actType.toUpperCase();
			}			
			return actType;			
	}
}