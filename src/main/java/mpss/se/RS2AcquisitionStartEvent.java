package mpss.se;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import mpss.common.jpa.Contact;

@Scope ( "prototype" )
@Component("rs2AcquisitionStartEvent")
public class RS2AcquisitionStartEvent extends ScheduledEventBase
{	

	public RS2AcquisitionStartEvent()
	{
		this.eventMnemonic = "ACQ_S";
	}
	
	@Override
	public void generate(Contact contact) 
	{
		logger.info("Contact[" + contact.getId() +"] is RS2AcquisitionStartEvent");	
		// base method
		super.generate(contact);
	}
}