package mpss.se;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

import com.google.common.collect.Range;

import mpss.common.jpa.Extent;
import mpss.common.jpa.Maneuverrequest;
import mpss.common.dao.ManeuverrequestDao;
import mpss.util.timeformat.TimeRange;

@Scope ( "prototype" )    
@Component("rs5OrbitManeuverEvent")
public class RS5OrbitManeuverEvent extends ScheduledEventBase
{	
	@Autowired ManeuverrequestDao maneuverrequestDao;
	public RS5OrbitManeuverEvent()
	{
		this.eventMnemonic = "ORB_MAN";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		logger.info("Extent[" + extent.getId() +"] is RS5OrbitManeuverEvent");	
		// base method
		super.generate(extent);
		Maneuverrequest maneuverrequest = this.maneuverrequestDao.get((long) this.activity.getArlentryid());
	  
		TimeRange tr= new TimeRange(extent.getStarttime(), extent.getEndtime());
		this.duration = tr.duration;
		
		// parm $1 - orbit maneuver procedure
		this.eventParms.add(maneuverrequest.getManeuverproc());		

	}
	
	  @Override
	  public Range<Timestamp> getOperationalPeriod()
	  {
	      // operational period of rsi record does not include the ssr setup and
	      // cleanup.  Need to get the ssr setup and cleanup times and remove them
	      // from extent times
	      
	      // get ssr setup/cleanup times
	      int ssrSetup = this.satelliteps.getRsirecsetupdelay();
	      int ssrCleanup = this.satelliteps.getRsireccleanupdelay();
	      
	      // determine start/stop times considering ssr setup/cleanup times
	      Timestamp start = new Timestamp(extent.getStarttime().getTime() + ssrSetup * 1000);
	      Timestamp stop = new Timestamp(extent.getEndtime().getTime() - ssrCleanup * 1000);
	      
	      return Range.closed(start, stop);  
	  }
}