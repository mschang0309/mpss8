package mpss.se;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import mpss.common.jpa.Contact;

@Scope ( "prototype" )
@Component("rs2AcquisitionEndEvent")
public class RS2AcquisitionEndEvent extends ScheduledEventBase
{	

	public RS2AcquisitionEndEvent()
	{
		this.eventMnemonic = "ACQ_E";		
	}
	
	@Override
	public void generate(Contact contact) 
	{
		logger.info("Contact[" + contact.getId() +"] is RS2AcquisitionEndEvent");		
		super.generate(contact);
		this.eventStartTime = acqTime.second;
	}
}