package mpss.se;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

import com.google.common.collect.Range;

import mpss.common.jpa.Extent;
import mpss.common.jpa.Maneuverrequest;
import mpss.common.dao.ManeuverrequestDao;
import mpss.util.timeformat.TimeRange;

@Scope("prototype")
@Component("rs5ManeuverEvent")
public class RS5ManeuverEvent extends ScheduledEventBase {
	@Autowired
	ManeuverrequestDao maneuverrequestDao;

	public RS5ManeuverEvent() {
		this.eventMnemonic = "ATT_MAN";
	}

	@Override
	public void generate(Extent extent) {
		logger.info("Extent[" + extent.getId() + "] is RS5ManeuverEvent");
		// base method
		super.generate(extent);
		Maneuverrequest maneuverrequest = this.maneuverrequestDao
				.get((long) this.activity.getArlentryid());

		TimeRange tr = new TimeRange(extent.getStarttime(), extent.getEndtime());
		this.duration = tr.duration;

		// parm $1 - Q1
		this.eventParms.add(String.format("%-6.5f", maneuverrequest.getQ1()));

		// parm $2 - Q2
		this.eventParms.add(String.format("%-6.5f", maneuverrequest.getQ2()));

		// parm $3 - Q3
		this.eventParms.add(String.format("%-6.5f", maneuverrequest.getQ3()));

		// parm $4 - Q4
		this.eventParms.add(String.format("%-6.5f", maneuverrequest.getQ4()));

		// parm $5 - Duration
		this.eventParms.add(Float.toString(maneuverrequest.getManeuverduration()));

		// parm $6 - r_rate
		this.eventParms.add(String.format("%-6.5f", maneuverrequest.getRRate()));

		// parm $7 - p_rate
		this.eventParms.add(String.format("%-6.5f", maneuverrequest.getPRate()));

		// parm $8 - y_rate
		this.eventParms.add(String.format("%-6.5f", maneuverrequest.getYRate()));

		// parm $9 - Asynchronous flag
		this.eventParms.add(maneuverrequest.getAsyncflag().toString());

		// parm $10 - ratefit1_1
		this.eventParms.add(maneuverrequest.getRatefit1_1());
				
		// parm $11 - ratefit1_2
		this.eventParms.add(maneuverrequest.getRatefit1_2());
			
		// parm $12 - ratefit1_3
		this.eventParms.add(maneuverrequest.getRatefit1_3());
				
		// parm $13 - ratefit2_1
		this.eventParms.add(maneuverrequest.getRatefit2_1());
				
		// parm $14 - ratefit2_2
		this.eventParms.add(maneuverrequest.getRatefit2_2());
				
		// parm $15 - ratefit2_3
		this.eventParms.add(maneuverrequest.getRatefit2_3());
				
		// parm $16 - ratefit3_1
		this.eventParms.add(maneuverrequest.getRatefit3_1());		
						
		// parm $17 - ratefit3_2
		this.eventParms.add(maneuverrequest.getRatefit3_2());		
						
		// parm $18 - ratefit3_3
		this.eventParms.add(maneuverrequest.getRatefit3_3());

	}

	@Override
	public Range<Timestamp> getOperationalPeriod() {
		// operational period of rsi record does not include the ssr setup and
		// cleanup. Need to get the ssr setup and cleanup times and remove them
		// from extent times

		// get ssr setup/cleanup times
		int ssrSetup = this.satelliteps.getRsirecsetupdelay();
		int ssrCleanup = this.satelliteps.getRsireccleanupdelay();

		// determine start/stop times considering ssr setup/cleanup times
		Timestamp start = new Timestamp(extent.getStarttime().getTime()
				+ ssrSetup * 1000);
		Timestamp stop = new Timestamp(extent.getEndtime().getTime()
				- ssrCleanup * 1000);

		return Range.closed(start, stop);
	}
}