package mpss.se;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import mpss.configFactory;
import mpss.common.dao.ActivityDao;
import mpss.common.dao.ActivityManDao;
import mpss.common.dao.ContactDao;
import mpss.common.dao.ExtentDao;
import mpss.common.dao.RevDao;
import mpss.common.dao.SatelliteDao;
import mpss.common.dao.SatellitepDao;
import mpss.common.dao.SessionDao;
import mpss.common.dao.SiteDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Rev;
import mpss.common.jpa.Satellite;
import mpss.common.jpa.Satellitep;
import mpss.common.jpa.Session;
import mpss.common.jpa.Site;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeUtil;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.google.common.collect.Range;

@Scope("prototype")
@Component("scheduledEventBase")
public class ScheduledEventBase implements ScheduledEvent
{
	@Autowired
	ActivityDao activityDao;
	@Autowired
	SatelliteDao satelliteDao;
	@Autowired
	SessionDao sessionDao;
	@Autowired
	SatellitepDao satellitepDao;
	@Autowired
	ContactDao contactDao;
	@Autowired
	SiteDao siteDao;
	@Autowired
	RevDao revDao;
	@Autowired
	ExtentDao extentDao;
	@Autowired
	ActivityManDao actManDao;
	
	protected Logger logger = Logger.getLogger(configFactory.getLogName());
	
	
	protected Timestamp	eventStartTime;
	// duration of event, based off event types minus any setup/cleanup delays
	protected int duration;
  
	// total duration of related extent
	protected int extentDuration;

	// duration of any setups for event
	protected int  setupDuration;

	// event mnemonic of event:
	//ACQ_S, ACQ_E, MAN, ATC_LOAD, MPQ_LOAD, RT_PROC/MISC_PROC, RSI_REC/RSI_PBK/RSI_DDT, ISUAL_PBK, DEL, XCROSS
	protected String	eventMnemonic;

	// string list of event parameters, based off event type.
	protected List<String>	eventParms = new ArrayList<String>();

	// list of linked events.  For example, for pbk events, this list contains all the record events associated.
	protected List<ScheduledEvent>  linkedEvents;
	
	protected TimeRange acqTime;
	protected Extent extent;
	protected Activity activity;
	// onwer session
	protected Session session;

	// satellite on which event is related
	protected Satellite	satellite;

	// sat rev on which event happens during
	protected Rev rev;
	
	
	// event related site
	protected Site site;

	// event related contact, if event requires contact
	protected Contact contact;


	protected Satellitep satelliteps;
	
	public ScheduledEventBase()
	{
	}  
	public void dump(){		
		logger.info(eventStartTime + "	[" + eventMnemonic + "]");
		logger.info(eventParms);
		
	}
	// public properties
	public Extent getExtent()
	{
		return this.extent;
	}
	public void setExtent(Extent extent)
	{
		this.extent = extent;
	}
	public Activity getActivity()
	{
		return this.activity;
	}
	public void setActivity(Activity activity)
	{
		this.activity = activity;
	}
	public void setSession(Session session)
	{
	  	this.session = session;		
	}
	public Session getSession()
	{
		return this.session;
	}
	public void setSatellite(Satellite satellite)
	{
	  	 this.satellite = satellite;		
	}
	public Satellite getSatellite()
	{
		return this.satellite;
	}
	public void setRev(Rev rev)
	{
	  	 this.rev = rev;		
	}
	public Rev getRev()
	{
		return this.rev;
	}
	public void setSite(Site site)
	{
	  	 this.site = site;		
	}
	public Site getSite()
	{
		return this.site;
	}  
	public void setContact(Contact contact)
	{
	  	 this.contact = contact;		
	}
	public Contact getContact()
	{
		return this.contact;
	}  
	public void setSatelliteps(Satellitep satelliteps)
	{
	  	 this.satelliteps = satelliteps;		
	}
	public Satellitep getSatelliteps()
	{
		return this.satelliteps;
	}
	public String getEventMnemonic()
	{
		return this.eventMnemonic;
	}
	public void setEventMnemonic(String eventMnemonic)
	{
		this.eventMnemonic = eventMnemonic;
	}
	public List<String>	getEventParms()
	{
		return this.eventParms;
	}
	public void setEventParms(List<String> eventParms)
	{
		this.eventParms = eventParms;
	}
	public int getDuration()
	{
		return this.duration;
	}
	public void setDuration(int duration)
	{
		this.duration = duration;
	}
	public int getExtentDuration()
	{
		return this.extentDuration;
	}
	public void setExtentDuration(int extentDuration)
	{
		this.extentDuration = extentDuration;
	}
	public int getSetupDuration()
	{
		return this.setupDuration;
	}
	public void setSetupDuration(int setupDuration)
	{
		this.setupDuration = setupDuration;
	}
	public Timestamp getEventStartTime()
	{
		return this.eventStartTime;
	}
	public void setEventStartTime(Timestamp eventStartTime)
	{
		this.eventStartTime = eventStartTime;
	}	
	
	// public methods
	public void generate(Extent extent)
	{

		// create a scheduled event from extent
		setExtent(extent);
		  
		// set activity on which the extent is based
		setActivity(getActivity(this.extent.getActivityid()));
		  
		// onwer session
		setSession(getSession(this.activity.getSessionid()));
		  
		Contact c = getContact(this.extent.getContactid());
		setContact(c);
		if (c !=null)
		{
			// set event related site
			setSite(getSite(c.getSiteid()));
		  
			// sat rev on which event happens during
			setRev(getRev(c.getRevid()));
		}
		else
		{
			Rev r = revDao.getRevbyTime(extent.getStarttime(), this.session.getIsbranch() == 0 ? 0 : this.activity.getSessionid());
			if (r != null)
				setRev(r);
		}	
		
		 
		// satellite on which event is related
		setSatellite(getSatellite(this.activity.getSatelliteid()));
		  
		// set satellite parameter set
		Satellitep ps = getSatelliteParameterset(this.activity.getSatelliteid().intValue(),this.session.getPsid());
		setSatelliteps(ps);
		  
		this.eventStartTime = extent.getStarttime();
		//this.eventStartTime = activity.getStarttime();
		TimeRange tr= new TimeRange(extent.getStarttime(), extent.getEndtime());
		this.duration = tr.duration;
		this.extentDuration = tr.duration;
		this.setupDuration = 0;
	}

	// public methods
	public void generate(Contact contact)
	{
		this.duration = 1;
		this.extentDuration = 1;
		this.setupDuration = 0;
		setContact(contact);
		
		// set event related site
		setSite(getSite(contact.getSiteid()));
		  
		// sat rev on which event happens during
		setRev(getRev(contact.getRevid()));
		
		// satellite on which event is related
		setSatellite(getSatellite(contact.getSatelliteid()));
		
//		// create a scheduled event from extent
//		setExtent(extent);
//		  
//		// set activity on which the extent is based
//		setActivity(getActivity(this.extent.getActivityid()));
//		  
//		// onwer session
//		setSession(getSession(this.activity.getSessionid()));
//		 
//		// set satellite parameter set
//		Satellitep ps = getSatelliteParameterset(this.activity.getSatelliteid().intValue(),this.session.getPsid());
//		setSatelliteps(ps);
		
		List<Extent> exts = extentDao.getByContactId(contact.getId().intValue());
		acqTime = actManDao.getOperationalPeriod(exts.get(0));
		//acqTime = new TimeRange(exts.get(0).getStarttime(),exts.get(0).getEndtime());
		for (int i=1; i< exts.size(); i++)
		{
			TimeRange tr = new TimeRange(exts.get(i).getStarttime(),exts.get(i).getEndtime());
			acqTime = acqTime.expandTo(tr);
		}
		acqTime.first = TimeUtil.timeAppendSec(acqTime.first ,- (contact.getFrontexp() + contact.getFrontbias()));
		acqTime.second = TimeUtil.timeAppendSec(acqTime.second, contact.getBackexp() + contact.getBackbias());
		   
		this.eventStartTime = acqTime.first;
	}
		
	// get activity on which the extent is based
	private Activity getActivity(int activityId)
	{	
		if (this.activityDao == null)
		{
			//System.out.println("activityDao is not injected");
			return null;
		}
		Activity act = this.activityDao.get((long)activityId);
		return act;
	}

	// get contact on which the extent is based
	private Contact getContact(int contactId)
	{	
		if (this.contactDao == null)
		{
			return null;
		}
		Contact cont = this.contactDao.get((long)contactId);
		return cont;
	}
  
	// get site on which the contact is based
  	private Site getSite(int siteId)
  	{	
  		if (this.siteDao == null)
  		{
  			return null;
  		}
  		Site st = this.siteDao.get((long)siteId);
  		return st;
  	}
  	
	// get site on which the contact is based
  	private Rev getRev(int revId)
  	{	
  		if (this.revDao == null)
  		{
  			return null;
  		}
  		Rev r = this.revDao.get((long)revId);
  		return r;
  	}
  
	private Session getSession(int sessionId)
	{
		if (this.sessionDao == null)
		{
			logger.debug("sessionDao is not injected");
			return null;
		}	
		Session s = this.sessionDao.get((long)sessionId);
		return s;
	}

	private Satellite getSatellite(int scId)
	{		
		if (this.satelliteDao == null)
		{
			logger.debug("satelliteDao is not injected");
			return null;
		}	
		Satellite sat = this.satelliteDao.get((long)scId);
		return sat;
  }

	private Satellitep getSatelliteParameterset(int scId, int psId)
	{		
		  //Parameterset ps = this.parametersetDao.get((long)psId);
		  // get satellite parameters belonging to this parameter set
		if (this.satellitepDao == null)
		{
			logger.debug("satelliteDao is not injected");
			return null;
		}
		Satellitep ps = this.satellitepDao.getBySatIdNPsId(scId,psId);
		return ps;
  }
	
	

  public Object clone() throws CloneNotSupportedException 
  {
	  ScheduledEventBase o = (ScheduledEventBase)super.clone();
	  return o;
  }
  
  public boolean hasSecondFilename(Extent extent)
  {
	  boolean result = false;
	  
	  if (!extent.getSecondfilename().equals(0))
		  result = true;
	  
		  return result;
  }
  
  public Range<Timestamp> getOperationalPeriod()
  {
  	  // default to extent time window
      //return Range.closed(this.extent.getStarttime(), this.extent.getEndtime());
      return Range.closed(this.activity.getStarttime(), this.activity.getEndtime());
  }
  
}