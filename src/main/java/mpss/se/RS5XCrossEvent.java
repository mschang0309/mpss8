package mpss.se;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;




import mpss.common.jpa.Extent;

@Scope ( "prototype" )
@Component("rs5XCrossEvent")
public class RS5XCrossEvent extends ScheduledEventBase
{
	public RS5XCrossEvent()
	{
	    this.eventMnemonic = "XCROSS";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		logger.info("Extent[" + extent.getId() +"] is RS5XCrossEvent");	
		// base method
		super.generate(extent);
		
	    // Event parameters are updated by the event generator based
	    // on overlaps of PBK/DDT activity sequences
		
		//parm $1 - default to "%" meaning optional 
	    // - not overlapping acts requiring xband antenna
		this.eventParms.add("%");
		
		//parm $2 - default to "%" meaning optional 
	    // - not overlapping RF sequence
		this.eventParms.add("%");		
	}
}