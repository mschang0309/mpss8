package mpss.se;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mpss.common.jpa.Extent;
import mpss.common.jpa.Isualprocrequest;
import mpss.common.jpa.Rtcmdprocrequest;
import mpss.common.dao.IsualprocrequestDao;
import mpss.common.dao.RtcmdprocrequestDao;
import mpss.common.jpa.Misccmdprocrequest;
import mpss.common.dao.MisccmdprocrequestDao;

@Scope ( "prototype" )    
@Component("rs2cmdProcEvent")
public class RS2CmdProcEvent extends ScheduledEventBase
{	
	@Autowired RtcmdprocrequestDao rtcmdprocrequestDao;
	@Autowired MisccmdprocrequestDao misccmdprocrequestDao;
	@Autowired IsualprocrequestDao isualprocrequestDao;
	public RS2CmdProcEvent()
	{
		this.eventMnemonic = "";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		logger.info("Extent[" + extent.getId() +"] is RS2CmdProcEvent");	
		// base method
		super.generate(extent);
		
		String arlEntryType=this.activity.getArlentrytype();
		if (arlEntryType.equalsIgnoreCase("RTCMD")) //GlARL::RTCMD
		{
			logger.info("Extent[" + extent.getId() +"] is RS2CmdProcEvent -- RTCMD");	
			this.eventMnemonic = "RT_PROC";
			
			Rtcmdprocrequest rtcmdprocrequest = this.rtcmdprocrequestDao.get((long) this.activity.getArlentryid());	
			// parm $1 - runtime proc
			this.eventParms.add(rtcmdprocrequest.getRtcmdproc());
		}
		else if (arlEntryType.equalsIgnoreCase("MISC")) //GlARL::MISCPROC
		{
			logger.info("Extent[" + extent.getId() +"] is RS2CmdProcEvent -- MISC");
			this.eventMnemonic = "MISC_PROC";
			//Misccmdprocrequest misccmdprocrequest = this.misccmdprocrequestDao.get((long) this.activity.getArlentryid());
			Misccmdprocrequest misccmdprocrequest = this.misccmdprocrequestDao.getbyId(this.activity.getArlentryid());
			// parm $1
			this.eventParms.add(misccmdprocrequest.getUserproc());
		}
		else if (arlEntryType.equalsIgnoreCase("ISUALPROC")) //GlARL::ISUALPROC
		{
			logger.info("Extent[" + extent.getId() +"] is RS2CmdProcEvent -- ISUALPROC");
			this.eventMnemonic = "ISUAL_PROC";
			
			Isualprocrequest isualprocrequest = this.isualprocrequestDao.get((long) this.activity.getArlentryid());
			// parm $1
			this.eventParms.add(isualprocrequest.getIsualproc());
		}
		else
		{
			logger.error("Error: Activity ARL entry type neither RTCMD or MISC or ISUALPROC");			
		}  	

	}
	
}