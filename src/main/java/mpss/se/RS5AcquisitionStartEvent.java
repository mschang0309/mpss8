package mpss.se;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import mpss.common.jpa.Contact;

@Scope ( "prototype" )
@Component("rs5AcquisitionStartEvent")
public class RS5AcquisitionStartEvent extends ScheduledEventBase
{	

	public RS5AcquisitionStartEvent()
	{
		this.eventMnemonic = "ACQ_S";
	}
	
	@Override
	public void generate(Contact contact) 
	{
		logger.info("Contact[" + contact.getId() +"] is RS5AcquisitionStartEvent");	
		// base method
		super.generate(contact);
	}
}