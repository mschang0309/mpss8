package mpss.se;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import mpss.common.jpa.Contact;

@Scope ( "prototype" )
@Component("rs5AcquisitionEndEvent")
public class RS5AcquisitionEndEvent extends ScheduledEventBase
{	

	public RS5AcquisitionEndEvent()
	{
		this.eventMnemonic = "ACQ_E";
		
	}
	
	@Override
	public void generate(Contact contact) 
	{
		logger.info("Contact[" + contact.getId() +"] is RS5AcquisitionEndEvent");	
		// base method
		super.generate(contact);
		this.eventStartTime = acqTime.second;
	}
}