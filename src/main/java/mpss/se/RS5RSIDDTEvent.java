package mpss.se;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import java.sql.Timestamp;
import com.google.common.collect.Range;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Rsiimagingrequest;
import mpss.common.dao.RsiimagingrequestDao;
import mpss.util.timeformat.TimeUtil;

@Scope ( "prototype" )
@Component("rs5RSIDDTEvent")
public class RS5RSIDDTEvent extends ScheduledEventBase
{
	@Autowired RsiimagingrequestDao rsiimagingrequestDao;
	
	public RS5RSIDDTEvent()
	{
		// assume this will be a secondary DDT - event generator will correct if
		// it is in fact an initial DDT
		//this.eventMnemonic = "RSI_DDT_SEC";
		this.eventMnemonic = "RSI_DDT";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		logger.info("Extent[" + extent.getId() +"] is RS5RSIDDTEvent");	
		// base method
		super.generate(extent);

		float ddtDelay = this.satelliteps.getPostddtdelay();
		int cleanup = (int) Math.ceil(extent.getRecorderdelta() * ddtDelay);
		
		// update setup duration
		this.setupDuration = this.satelliteps.getRsirecsetupdelay();
		// update duration - remove setup/cleanup delays (in seconds)
		this.duration = this.duration  - this.setupDuration - cleanup;
		
		// offset event time by setup duration
		this.eventStartTime = TimeUtil.timeAppendSec(this.eventStartTime, this.setupDuration);
	
	    // offset event time by setup duration
	    //myEventTime += mySetupDuration;
	
	    Rsiimagingrequest rsiimagingrequest = this.rsiimagingrequestDao.get((long) this.activity.getArlentryid());
			
	    // parm ?1  - if first imaging event in sequence turn on appropriate video channels
	    //  defaults to "%" - updated by event generator if required
		  this.eventParms.add("%");
		
		// parm ?2  - if first transmit event in sequence turn on TMI and enable RF
		//  defaults to "%" - updated by event generator if required
		  this.eventParms.add("%");
		  
		// parms ?2 through $9 are all dependent on the imaging mode
		//int mode = rsiimagingrequest.getImagingmode();
		String mode = this.activity.getInfo();
		// parms ?3 through $10 are all dependent on the imaging mode
		if (mode.equalsIgnoreCase("PAN+MS")) 
	    {
			// parm ?3 - PAN file name
		    this.eventParms.add( Integer.toString(this.extent.getFilename()));
	    
		    // parm ?4 - PAN file size (in sectors)
		    this.eventParms.add(Integer.toString(this.extent.getRecorderdelta()));
	    
		    // parm ?5 - MS file name
		    this.eventParms.add( Integer.toString(this.extent.getSecondfilename()));
		    
		    // parm ?6 - MS file size (in sectors)
		    this.eventParms.add(Integer.toString(this.extent.getSecondrecorderdelta()));
	
		    // parms $7 and $8 are the compression ratios for PAN+MS mode
		    if (rsiimagingrequest != null)
		    {
		    	// parm $7 - PAN compression ratio
			    switch (rsiimagingrequest.getPancompressionratio())
				{
					case "NONE":
						this.eventParms.add("0X03");
						break;
					case "LOW":
						this.eventParms.add("0X00");
						break;
					case "MEDIUM":
						this.eventParms.add("0X01");
						break;
					case "HIGH":
						this.eventParms.add("0X02");
						break;
				}
		    	
			    // parm $8 - MS compression ratio
			    switch (rsiimagingrequest.getMscompressionratio())
				{
					case "NONE":
						this.eventParms.add("0X03");
						break;
					case "LOW":
						this.eventParms.add("0X00");
						break;
					case "MEDIUM":
						this.eventParms.add("0X01");
						break;
					case "HIGH":
						this.eventParms.add("0X02");
						break;
				}
			    
				// parm $9 - TOTAL_IMGLINE	(Total PAN Imaging Lines Divided by 40)
				if (System.getProperty("imgLineFactor") != null)
					this.eventParms.add(Double.toString(Math.ceil(rsiimagingrequest.getPanlinecompressionratio()/Float.valueOf(System.getProperty("imgLineFactor")))));
				else
					this.eventParms.add("%");
		    }
	
		    // parm $10 - SSR video PAN channel status selection (YES/NO)
		    this.eventParms.add("YES");
	    
		    // parm $11 - SSR video MS channel status selection (YES/NO)
		    this.eventParms.add("YES");
	    }
	    else if (mode.equalsIgnoreCase("PAN")) 
	    {
	    	// parm ?3 - PAN file name
	    	this.eventParms.add( Integer.toString(this.extent.getFilename()));
	    
	        // parm ?4 - PAN file size (in sectors)
	    	this.eventParms.add(Integer.toString(this.extent.getRecorderdelta()));
	    
		    // parm ?5 - MS file name
	    	this.eventParms.add("%");
		    
		    // parm ?6 - MS file size (in sectors)
		    this.eventParms.add("%");
	
	        
		    if (rsiimagingrequest != null)
		    {
		    	// parm $7 - PAN compression ratio
			    switch (rsiimagingrequest.getPancompressionratio())
				{
					case "NONE":
						this.eventParms.add("0X03");
						break;
					case "LOW":
						this.eventParms.add("0X00");
						break;
					case "MEDIUM":
						this.eventParms.add("0X01");
						break;
					case "HIGH":
						this.eventParms.add("0X02");
						break;
				}
			    
			    // parm $8 - MS compression ratio, since only doing PAN imaging, will set to LOW
			    this.eventParms.add("0X00");
				
			    // parm $9 - TOTAL_IMGLINE	(Total PAN Imaging Lines Divided by 40)
				if (System.getProperty("imgLineFactor") != null)
					this.eventParms.add(Double.toString(Math.ceil(rsiimagingrequest.getPanlinecompressionratio()/Float.valueOf(System.getProperty("imgLineFactor")))));
				else
					this.eventParms.add("%");
//			    this.eventParms.add(Double.toString(Math.ceil(rsiimagingrequest.getPanlinecompressionratio())));
		    }
		    else
		    {
		    	this.eventParms.add("0X02");
		    	this.eventParms.add("0X00");
		    	this.eventParms.add("%");
		    }
		    
		    // parm $10 - SSR video PAN channel status selection (YES)
		    this.eventParms.add("YES");
	    
		    // parm $11 - SSR video MS channel status selection (NO since only performing PAN mode)
		    this.eventParms.add("NO");
	
	    }
	    else  // assume MS
	    {
	        // parm ?3 - PAN file name
	    	this.eventParms.add("%");
	    
	        // parm ?4 - PAN file size (in sectors)
		    this.eventParms.add("%");
	    
		    // parm ?5 - MS file name
		    this.eventParms.add( Integer.toString(this.extent.getFilename()));
		    
		    // parm ?6 - MS file size (in sectors)
		    this.eventParms.add(Integer.toString(this.extent.getRecorderdelta()));
	
	        // parm $7 - PAN compression ratio (set to LOW since performing MS imaging)
		    this.eventParms.add("0X00");
		
		    if (rsiimagingrequest != null)
		    {
		    	 // parm $8 - MS compression ratio
			    switch (rsiimagingrequest.getMscompressionratio())
				{
					case "NONE":
						this.eventParms.add("0X03");
						break;
					case "LOW":
						this.eventParms.add("0X00");
						break;
					case "MEDIUM":
						this.eventParms.add("0X01");
						break;
					case "HIGH":
						this.eventParms.add("0X02");
						break;
				}
			    
			    // parm $9 - TOTAL_IMGLINE	(Total PAN Imaging Lines Divided by 40)
				if (System.getProperty("imgLineFactor") != null)
					this.eventParms.add(Double.toString(Math.ceil(rsiimagingrequest.getPanlinecompressionratio()/Float.valueOf(System.getProperty("imgLineFactor")))));
				else
					this.eventParms.add("%");
		    }
		    else
		    {
		    	this.eventParms.add("0X02");
		    	this.eventParms.add("%");
		    }
	
		    // parm $10 - SSR video PAN channel status selection (NO since MS imaging)
			this.eventParms.add("NO");
	    
		    // parm $11 - SSR video MS channel status selection (YES)
		    this.eventParms.add("YES");
	    }
	
		if (rsiimagingrequest != null)
		{
			// parm $12 - PAN video gain
			this.eventParms.add(rsiimagingrequest.getVideopangain());
		
		    // parm $13 - MB1 video gain
			this.eventParms.add(rsiimagingrequest.getVideomb1gain());
		
		    // parm $14 - MB2 video gain
			this.eventParms.add(rsiimagingrequest.getVideomb2gain());
		
		    // parm $15 - MB3 video gain
			this.eventParms.add(rsiimagingrequest.getVideomb3gain());
		
		    // parm $16 - MB4 video gain
			this.eventParms.add(rsiimagingrequest.getVideomb4gain());
		}
		else
		{
			this.eventParms.add("%");
			this.eventParms.add("%");
			this.eventParms.add("%");
			this.eventParms.add("%");
			this.eventParms.add("%");
		}
		
	    // parm ?17 - if last imaging event in sequence turn off video channels
	    //  -- defaults to "%" udpated by event generator if needed
		this.eventParms.add("%");
	    
	    // parm ?18 - if last transmit event in sequence turn off TMI and disable RF
	    //  -- defaults to "%" udpated by event generator if needed
		this.eventParms.add("%");
		
		// parm ?19 - if first DDT,PBK,REC , SSR exit Power Saving
	    //  -- defaults to "%" udpated by event generator if needed
		this.eventParms.add("%");
		
		// parm ?20 - if last DDT,PBK,REC , SSR enter Power Saving
	    //  -- defaults to "%" udpated by event generator if needed
		this.eventParms.add("%");
		
	}
	
	  @Override
	  public Range<Timestamp> getOperationalPeriod()
	  {
	      // operational period of rsi record does not include the ssr setup and
	      // cleanup.  Need to get the ssr setup and cleanup times and remove them
	      // from extent times
	      
	      // get ssr setup/cleanup times
	      int ssrSetup = this.satelliteps.getRsirecsetupdelay();
	      int ssrCleanup = this.satelliteps.getRsireccleanupdelay();
	      
	      // determine start/stop times considering ssr setup/cleanup times
	      Timestamp start = new Timestamp(extent.getStarttime().getTime() + ssrSetup * 1000);
	      Timestamp stop = new Timestamp(extent.getEndtime().getTime() - ssrCleanup * 1000);
	      
	      return Range.closed(start, stop);  
	  }
}