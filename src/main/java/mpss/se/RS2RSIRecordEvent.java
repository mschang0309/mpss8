package mpss.se;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

import com.google.common.collect.Range;

import mpss.common.jpa.Extent;
import mpss.common.jpa.Rsiimagingrequest;
import mpss.common.dao.RsiimagingrequestDao;
import mpss.util.timeformat.TimeUtil;

@Scope ( "prototype" )
@Component("rs2RSIRecordEvent")
public class RS2RSIRecordEvent extends ScheduledEventBase
{
	@Autowired RsiimagingrequestDao rsiimagingrequestDao;
	
	public RS2RSIRecordEvent()
	{
	    // assume this will be a secondary REC - event generator will correct if
	    // it is in fact an initial REC
		this.eventMnemonic = "RSI_REC_SEC";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		logger.info("Extent[" + extent.getId() +"] is RS2RSIRecordEvent");	
		// base method
		super.generate(extent);

		int rsiRecSetupDelay = this.satelliteps.getRsirecsetupdelay();
		int rsiRecCleanupDelay = this.satelliteps.getRsireccleanupdelay();
		
		//this.extentDuration = (int) ((extent.getEndtime().getTime() - extent.getStarttime().getTime()) / 1000);
		// update duration - remove setup/cleanup delays (in seconds)
		this.duration = this.duration  - rsiRecSetupDelay - rsiRecCleanupDelay;
		// update setup duration
		this.setupDuration = rsiRecSetupDelay;
		
		// offset event time by setup duration
		this.eventStartTime = TimeUtil.timeAppendSec(this.eventStartTime, this.setupDuration);
		
		Rsiimagingrequest rsiimagingrequest = this.rsiimagingrequestDao.get((long) this.activity.getArlentryid());
			
		// parm ?1  - if first imaging event in sequence turn on appropriate video channels
		//  defaults to "%" - updated by event generator if required
		  this.eventParms.add("%");
		  
		// parms ?2 through $9 are all dependent on the imaging mode
		//int mode = rsiimagingrequest.getImagingmode();
		String mode = this.activity.getInfo();
		if (mode.equalsIgnoreCase("PAN+MS")) 
		{
			// parm ?2 - PAN file name
			this.eventParms.add( Integer.toString(this.extent.getFilename()));
		
		    // parm ?3 - PAN file size (in sectors)
			this.eventParms.add(Integer.toString(this.extent.getRecorderdelta()));
		
			// parm ?4 - MS file name
			this.eventParms.add( Integer.toString(this.extent.getSecondfilename()));
		    
			// parm ?5 - MS file size (in sectors)
			this.eventParms.add(Integer.toString(this.extent.getSecondrecorderdelta()));
		
		    // parms $6 and $7 are the compression ratios for PAN+MS mode
		    // In PAN+MS mode, assume DEFAULT = HIGH compression ratio
		    
		    
			if (rsiimagingrequest != null)
		    {
				// parm $6 - PAN compression ratio (HIGH COMP RATIO or LOW COMP RATIO)
		    	if (rsiimagingrequest.getPancompressionratio().equalsIgnoreCase("DEFAULT"))
			        this.eventParms.add("HIGH_COMP_RATIO");
			    else
			        this.eventParms.add(rsiimagingrequest.getPancompressionratio() + "_COMP_RATIO");
		    	
			    // parm $7 - MS compression ratio (HIGH COMP RATIO or LOW COMP RATIO)
			    if (rsiimagingrequest.getMscompressionratio().equalsIgnoreCase("DEFAULT"))
				    this.eventParms.add("HIGH_COMP_RATIO");
			    else
			    	this.eventParms.add(rsiimagingrequest.getMscompressionratio() + "_COMP_RATIO");
		    }
		    else
		    {
		    	this.eventParms.add("HIGH_COMP_RATIO");
		    	this.eventParms.add("HIGH_COMP_RATIO");
		    }
				
		    // parm $8 - SSR video PAN channel status selection (YES/NO)
			this.eventParms.add("YES");
		
		    // parm $9 - SSR video MS channel status selection (YES/NO)
		    this.eventParms.add("YES");
		}
		else if (mode.equalsIgnoreCase("PAN")) 
		{
		    // parm ?2 - PAN file name
			this.eventParms.add( Integer.toString(this.extent.getFilename()));
		
		    // parm ?3 - PAN file size (in sectors)
			this.eventParms.add(Integer.toString(this.extent.getRecorderdelta()));
		
		    // parm ?4 - MS file name
			this.eventParms.add("%");
		    
		    // parm ?5 - MS file size (in sectors)
		    this.eventParms.add("%");
		
		    // parm $6 - PAN compression ratio (HIGH COMP RATIO or LOW COMP RATIO)
		    //      In PAN mode, assume DEFAULT = LOW
		    if (rsiimagingrequest != null)
		    {
		    	if (rsiimagingrequest.getPancompressionratio().equalsIgnoreCase("DEFAULT"))
			        this.eventParms.add("LOW_COMP_RATIO");
			    else
			        this.eventParms.add(rsiimagingrequest.getPancompressionratio() + "_COMP_RATIO");
		    }
		    else
		    {
		    	this.eventParms.add("LOW_COMP_RATIO");
		    }
		    
		
		    // parm $7 - MS compression ratio, since only doing PAN imaging, will set to LOW
		    this.eventParms.add("LOW_COMP_RATIO");
		
		      // parm $8 - SSR video PAN channel status selection (YES)
		    this.eventParms.add("YES");
		
		      // parm $9 - SSR video MS channel status selection (NO since only performing PAN mode)
		    this.eventParms.add("NO");
		
		}
		else  // assume MS
		{
		    // parm ?2 - PAN file name
			this.eventParms.add("%");
		
		    // parm ?3 - PAN file size (in sectors)
		    this.eventParms.add("%");
		
		    // parm ?4 - MS file name
		    this.eventParms.add( Integer.toString(this.extent.getFilename()));
		    
		    // parm ?5 - MS file size (in sectors)
		    this.eventParms.add(Integer.toString(this.extent.getRecorderdelta()));
		
		    // parm $6 - PAN compression ratio (set to LOW since performing MS imaging)
			this.eventParms.add("LOW_COMP_RATIO");
		
		    // parm $7 - MS compression ratio (HIGH COMP RATIO or LOW COMP RATIO)
		    //      In MS mode, assume DEFAULT = LOW
			if (rsiimagingrequest.getMscompressionratio().equalsIgnoreCase("DEFAULT"))
				this.eventParms.add("LOW_COMP_RATIO");
			else
				this.eventParms.add(rsiimagingrequest.getMscompressionratio() + "_COMP_RATIO");
		
		    // parm $8 - SSR video PAN channel status selection (NO since MS imaging)
			this.eventParms.add("NO");
		
		    // parm $9 - SSR video MS channel status selection (YES)
		    this.eventParms.add("YES");
		}
		
		if (rsiimagingrequest != null)
		{
			// parm $10 - PAN video gain
			this.eventParms.add(rsiimagingrequest.getVideopangain());
			
			// parm $11 - MB1 video gain
			this.eventParms.add(rsiimagingrequest.getVideomb1gain());
			
			// parm $12 - MB2 video gain
			this.eventParms.add(rsiimagingrequest.getVideomb2gain());
			
			// parm $13 - MB3 video gain
			this.eventParms.add(rsiimagingrequest.getVideomb3gain());
			
			// parm $14 - MB4 video gain
			this.eventParms.add(rsiimagingrequest.getVideomb4gain());
		}
		else
		{
			this.eventParms.add("%");
			this.eventParms.add("%");
			this.eventParms.add("%");
			this.eventParms.add("%");
			this.eventParms.add("%");
		}
		
		
		// parm ?15 - if last imaging event in sequence turn off video channels
		//  -- defaults to "%" udpated by event generator if needed
		this.eventParms.add("%");		
		
	}
	
	  @Override
	  public Range<Timestamp> getOperationalPeriod()
	  {
	      // operational period of rsi record does not include the ssr setup and
	      // cleanup.  Need to get the ssr setup and cleanup times and remove them
	      // from extent times
	      
	      // get ssr setup/cleanup times
	      int ssrSetup = this.satelliteps.getRsirecsetupdelay();
	      int ssrCleanup = this.satelliteps.getRsireccleanupdelay();
	      
	      // determine start/stop times considering ssr setup/cleanup times
	      Timestamp start = new Timestamp(extent.getStarttime().getTime() + ssrSetup * 1000);
	      Timestamp stop = new Timestamp(extent.getEndtime().getTime() - ssrCleanup * 1000);
	      
	      return Range.closed(start, stop);  
	  }
}