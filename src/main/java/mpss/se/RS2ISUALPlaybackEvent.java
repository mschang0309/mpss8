package mpss.se;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

import com.google.common.collect.Range;

import mpss.common.jpa.Extent;
import mpss.util.timeformat.TimeUtil;

@Scope ( "prototype" )    
@Component("rs2ISUALPlaybackEvent")
public class RS2ISUALPlaybackEvent extends ScheduledEventBase
{
	
	public RS2ISUALPlaybackEvent()
	{
	    this.eventMnemonic = "ISUAL_PBK";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		logger.info("Extent[" + extent.getId() +"] is RS2ISUALPlaybackEvent");
		// base method
		super.generate(extent);

		int rsiPbkSetupDelay = this.satelliteps.getRsipbsetupdelay();
	    int rsiPbkCleanupDelay = this.satelliteps.getRsipbcleanupdelay();
	    
	    //this.extentDuration = (int) ((extent.getEndtime().getTime() - extent.getStarttime().getTime()) / 1000);
	    // update duration - remove setup/cleanup delays (in seconds)
	    this.duration = this.duration  - rsiPbkSetupDelay - rsiPbkCleanupDelay;
	    // update setup duration
	    this.setupDuration = rsiPbkSetupDelay;
	
	    // offset event time by setup duration
	    this.eventStartTime = TimeUtil.timeAppendSec(this.eventStartTime, this.setupDuration);
	
	    // offset event time by setup duration
	    //myEventTime += mySetupDuration;
	
	    // ISUAL flip-flops between 2 different files on SSR.  Extent indicates
	    // which file to playback and the number of sectors.  We need to determine
	    // which file is being played back to pass the "other" file as the
	    // new file number.
	    
	    if (extent.getFilename().equals(126))
	    {
	    	//parm ?1 - playback ISUAL file number 126
	    	this.eventParms.add("YES");
	    	
	    	//parm ?2 - optional param
	    	this.eventParms.add("%");
	    }
	    else
	    {
	    	//parm ?1 - optional param
	    	this.eventParms.add("%");
	    	
	    	//parm ?2 - playback ISUAL file number 127
	    	this.eventParms.add("YES");
	    }    
	
	}
	
	  @Override
	  public Range<Timestamp> getOperationalPeriod()
	  {
	      // operational period of rsi record does not include the ssr setup and
	      // cleanup.  Need to get the ssr setup and cleanup times and remove them
	      // from extent times
	      
	      // get ssr setup/cleanup times
	      int ssrSetup = this.satelliteps.getRsirecsetupdelay();
	      int ssrCleanup = this.satelliteps.getRsireccleanupdelay();
	      
	      // determine start/stop times considering ssr setup/cleanup times
	      Timestamp start = new Timestamp(extent.getStarttime().getTime() + ssrSetup * 1000);
	      Timestamp stop = new Timestamp(extent.getEndtime().getTime() - ssrCleanup * 1000);
	      
	      return Range.closed(start, stop);  
	  }
}