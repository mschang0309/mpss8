package mpss.se;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mpss.common.jpa.Aipprocrequest;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Rtcmdprocrequest;
import mpss.common.dao.AipprocrequestDao;
import mpss.common.dao.RtcmdprocrequestDao;
import mpss.common.jpa.Misccmdprocrequest;
import mpss.common.dao.MisccmdprocrequestDao;

@Scope ( "prototype" )    
@Component("rs5cmdProcEvent")
public class RS5CmdProcEvent extends ScheduledEventBase
{	
	@Autowired RtcmdprocrequestDao rtcmdprocrequestDao;
	@Autowired MisccmdprocrequestDao misccmdprocrequestDao;
	@Autowired AipprocrequestDao aipprocrequestDao;
	public RS5CmdProcEvent()
	{
		this.eventMnemonic = "";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		//System.out.println("Extent[" + extent.getId() +"] is RS5CmdProcEvent");
		// base method
		super.generate(extent);
		
		String arlEntryType=this.activity.getArlentrytype();
		if (arlEntryType.equalsIgnoreCase("RTCMD")) //GlARL::RTCMD
		{
			logger.info("Extent[" + extent.getId() +"] is RS5CmdProcEvent -- RTCMD");	
			this.eventMnemonic = "RT_PROC";
			
			Rtcmdprocrequest rtcmdprocrequest = this.rtcmdprocrequestDao.get((long) this.activity.getArlentryid());	
			// parm $1 - runtime proc
			this.eventParms.add(rtcmdprocrequest.getRtcmdproc());
		}
		else if (arlEntryType.equalsIgnoreCase("MISC")) //GlARL::MISCPROC
		{
			logger.info("Extent[" + extent.getId() +"] is RS5CmdProcEvent -- MISC");
			this.eventMnemonic = "MISC_PROC";
			
			Misccmdprocrequest misccmdprocrequest = this.misccmdprocrequestDao.get((long) this.activity.getArlentryid());
			// parm $1
			this.eventParms.add(misccmdprocrequest.getUserproc());
		}
		else if (arlEntryType.equalsIgnoreCase("AIPPROC")) //GlARL::AIPPROC
		{
			logger.info("Extent[" + extent.getId() +"] is RS5CmdProcEvent -- AIPPROC");
			this.eventMnemonic = "AIP_PROC";
			
			Aipprocrequest aipprocrequest = this.aipprocrequestDao.get((long) this.activity.getArlentryid());
			// parm $1
			this.eventParms.add(aipprocrequest.getAipproc());
		}
		else
		{
			logger.error("Error: Activity ARL entry type neither RTCMD or MISC or AIPPROC");
		}
  	
	
	}
	
}