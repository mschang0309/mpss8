package mpss.se;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mpss.configFactory;
import mpss.common.dao.ActivityDao;
import mpss.common.dao.ContactDao;
import mpss.common.dao.ManeuverrequestDao;
import mpss.common.dao.OrbitEventsDao;
import mpss.common.dao.SatelliteDao;
import mpss.common.dao.SatellitepDao;
import mpss.common.dao.SchedulerDao;
import mpss.common.jpa.Satellitep;
import mpss.common.jpa.Session;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("scheduledEventGeneratorBase")
public class ScheduledEventGeneratorBase implements ScheduledEventGenerator
{
	@Autowired
	SchedulerDao schDao;
	
	@Autowired
	ActivityDao activityDao;
	
	@Autowired
	OrbitEventsDao orbitEventsDao;
	
	@Autowired
	ManeuverrequestDao maneuverrequestDao;
	
	@Autowired
	SatellitepDao satellitepDao;
	
	@Autowired
	ContactDao contactDao;
	
	@Autowired
	SatelliteDao satDao;
	
	protected Map<String, String> schedEventBeans = new HashMap<String, String>();
	
	protected List<ScheduledEvent> scheduledEvents = new ArrayList<ScheduledEvent>();
	
	protected int satId;
	protected Satellitep satelliteps; 
	
	protected Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public List<ScheduledEvent> getScheduledEvents()
	{
		return this.scheduledEvents;
	}
	
	public void setScheduledEvents(List<ScheduledEvent> scheduledEvents)
	{
		this.scheduledEvents = scheduledEvents;
	}
	
	public void setSatelliteId(int satId)
	{
		this.satId = satId;
	}
	
	public boolean generate(List<Session> schSessions){
		return true;
	}
	
}