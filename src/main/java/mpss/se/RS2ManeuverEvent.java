package mpss.se;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

import com.google.common.collect.Range;

import mpss.common.jpa.Extent;
import mpss.common.jpa.Maneuverrequest;
import mpss.common.dao.ManeuverrequestDao;
import mpss.util.timeformat.TimeRange;

@Scope ( "prototype" )    
@Component("rs2ManeuverEvent")
public class RS2ManeuverEvent extends ScheduledEventBase
{	
	@Autowired ManeuverrequestDao maneuverrequestDao;
	public RS2ManeuverEvent()
	{
		this.eventMnemonic = "ATT_MAN";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		logger.info("Extent[" + extent.getId() +"] is RS2ManeuverEvent");
		// base method
		super.generate(extent);
		Maneuverrequest maneuverrequest = this.maneuverrequestDao.get((long) this.activity.getArlentryid());
		
		TimeRange tr= new TimeRange(extent.getStarttime(), extent.getEndtime());
		this.duration = tr.duration;
	  
		// parm $1 - Q1
		//this.eventParms.add( Float.toString(maneuverrequest.getQ1()));
		this.eventParms.add( String.format("%-6.5f", maneuverrequest.getQ1()));
    
		// parm $2 - Q2
		//this.eventParms.add( Float.toString(maneuverrequest.getQ2()));
		this.eventParms.add( String.format("%-6.5f", maneuverrequest.getQ2()));
		
		// parm $3 - Q3
		//this.eventParms.add( Float.toString(maneuverrequest.getQ3()));
		this.eventParms.add( String.format("%-6.5f", maneuverrequest.getQ3()));
    
		// parm $4 - Q4
		//this.eventParms.add( Float.toString(maneuverrequest.getQ4()));
		this.eventParms.add( String.format("%-6.5f", maneuverrequest.getQ4()));

		// parm $5 - Duration
		this.eventParms.add( Float.toString(maneuverrequest.getManeuverduration()));
		
		// parm $6 - Number of thrusters involved in maneuver
		//myEventParms.push_back(GlToString(double (0)));
		// parm $7 - Which thrusters involved in maneuver
		//myEventParms.push_back(GlToString(double (0)));		

	}
	
	  @Override
	  public Range<Timestamp> getOperationalPeriod()
	  {
	      // operational period of rsi record does not include the ssr setup and
	      // cleanup.  Need to get the ssr setup and cleanup times and remove them
	      // from extent times
	      
	      // get ssr setup/cleanup times
	      int ssrSetup = this.satelliteps.getRsirecsetupdelay();
	      int ssrCleanup = this.satelliteps.getRsireccleanupdelay();
	      
	      // determine start/stop times considering ssr setup/cleanup times
	      Timestamp start = new Timestamp(extent.getStarttime().getTime() + ssrSetup * 1000);
	      Timestamp stop = new Timestamp(extent.getEndtime().getTime() - ssrCleanup * 1000);
	      
	      return Range.closed(start, stop);  
	  }
}