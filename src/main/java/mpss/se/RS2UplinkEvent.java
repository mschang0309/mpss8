package mpss.se;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mpss.common.jpa.Extent;
import mpss.common.jpa.Uplinkrequest;
import mpss.common.dao.UplinkrequestDao;

@Scope ( "prototype" )
@Component("rs2UplinkEvent")
public class RS2UplinkEvent extends ScheduledEventBase
{	
	@Autowired UplinkrequestDao uplinkrequestDao;
	public RS2UplinkEvent()
	{
		this.eventMnemonic = "";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		logger.info("Extent[" + extent.getId() +"] is RS2UplinkEvent");	
		// base method
		super.generate(extent);
		Uplinkrequest uplinkrequest = this.uplinkrequestDao.get((long) this.activity.getArlentryid());
		
		String loadType=uplinkrequest.getLoadtype();
	  
		// parm $1 - load type
		this.eventParms.add(loadType);
		
		// set our event mnemonic
		this.eventMnemonic = loadType + "_LOAD";  	
	
	}
	
}