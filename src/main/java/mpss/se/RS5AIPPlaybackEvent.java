package mpss.se;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import mpss.common.jpa.Extent;

@Scope ( "prototype" )
@Component("rs5AIPPlaybackEvent")
public class RS5AIPPlaybackEvent extends ScheduledEventBase
{
	public RS5AIPPlaybackEvent()
	{
	    this.eventMnemonic = "AIP_PBK";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		logger.info("Extent[" + extent.getId() +"] is RS5AIPPlaybackEvent");	
		// base method
		super.generate(extent);
		
		// parm $1 - file name 
		this.eventParms.add(this.activity.getInfo());
	}
}