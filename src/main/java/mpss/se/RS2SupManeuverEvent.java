package mpss.se;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

import com.google.common.collect.Range;

import mpss.common.jpa.Extent;
import mpss.util.timeformat.TimeRange;

@Scope ( "prototype" )
@Component("rs2SupManeuverEvent")
public class RS2SupManeuverEvent extends ScheduledEventBase
{	

	public RS2SupManeuverEvent()
	{
		this.eventMnemonic = "GOHOME_MAN";
	}
	
	@Override
	public void generate(Extent extent) 
	{
		logger.info("Extent[" + extent.getId() +"] is RS2SupManeuverEvent");	
		// base method
		super.generate(extent);
		
		TimeRange tr= new TimeRange(extent.getStarttime(), extent.getEndtime());
		this.duration = tr.duration;
		
		// Sun pointing maneuvers have no event parameters		
	}
	
	  @Override
	  public Range<Timestamp> getOperationalPeriod()
	  {
	      // operational period of rsi record does not include the ssr setup and
	      // cleanup.  Need to get the ssr setup and cleanup times and remove them
	      // from extent times
	      
	      // get ssr setup/cleanup times
	      int ssrSetup = this.satelliteps.getRsirecsetupdelay();
	      int ssrCleanup = this.satelliteps.getRsireccleanupdelay();
	      
	      // determine start/stop times considering ssr setup/cleanup times
	      Timestamp start = new Timestamp(extent.getStarttime().getTime() + ssrSetup * 1000);
	      Timestamp stop = new Timestamp(extent.getEndtime().getTime() - ssrCleanup * 1000);
	      
	      return Range.closed(start, stop);  
	  }
}