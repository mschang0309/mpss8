package mpss.se;

import java.util.List;

import mpss.common.jpa.Session;


public interface ScheduledEventGenerator
{	
	public List<ScheduledEvent> getScheduledEvents();	
	public void setScheduledEvents(List<ScheduledEvent> scheduledEvents);	
	public void setSatelliteId(int satId);		
	public boolean generate(List<Session> schSessions);
}