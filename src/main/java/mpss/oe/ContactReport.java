package mpss.oe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import mpss.util.timeformat.TimeToString;

public class ContactReport {

	private ConsParser consParser;
	
	public ContactReport(ConsParser consParser)
	{
		this.consParser = consParser;
	}

	public void genReport(String ingestFile, String reportFile) {
		
		File file = new File(reportFile);
		if(!file.exists())
		{
		    try {
		        file.createNewFile();
		    } catch (Exception e) {
		        System.err.println("OrbitEventsLoader Error: Contact Report file can not be create: " + reportFile);
		    }
		}
		
	    try {
	    	FileOutputStream fileOutputStream = new FileOutputStream(reportFile);
	    	OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
	    	
	    	 //Header
		    Timestamp currentTime = new Timestamp(System.currentTimeMillis());
	    	outputStreamWriter.write("Ingesting " + ingestFile + " at " + TimeToString.AsRptTime(currentTime) + "\n\n");
	    	outputStreamWriter.write(" Signature:  " + consParser.signature_ + "\n");
	    	outputStreamWriter.write(" Extracted:  " + TimeToString.AsShortTime(new Timestamp(consParser.extracted.getTime())) + "\n");
	    	outputStreamWriter.write("     Title:  " + consParser.title + "\n");
	    	outputStreamWriter.write("  Subtitle:  " + consParser.subtitle + "\n");
	    	outputStreamWriter.write(" Ephemeris:  " + consParser.ephemeris + "\n");
	    	outputStreamWriter.write("Spacecraft:  " + consParser.spacecraft + "\n");
	    	outputStreamWriter.write("   Equinox:  " + consParser.equinox + "\n");
	    	outputStreamWriter.write("  Attitude:  " + consParser.attitude + "\n\n");
	    	
	    	String sat = consParser.spacecraft.substring(1, 7);
	    	
	    	//Processing contacts:
	    	//error site
	    	for (String csError : consParser.getConsSiteError())
	        {
	        	outputStreamWriter.write("**** ERROR: Invalid site: " + csError + "\n");
	        }
	    	
	    	
	    	for(String sn : consParser.getContactListMap().keySet()){  
    			outputStreamWriter.write("\nProcessing contacts for " + sn + ":\n");
	    		
    			for(ContactRecord cr : consParser.getContactListMap().get(sn)){
    				if(cr.cmdrise == null || cr.cmdfade == null || cr.xbandrise == null || cr.xbandfade == null)
		    		{
		    			if ((cr.xbandrise == null || cr.xbandfade == null) && !consParser.getSbandSites().contains(sn)){
		    				if (!consParser.getSbandSites().contains(sn))
		    				{
				    			outputStreamWriter.write("**** INFO:  Invalid or incomplete XBAND pass found at " + 
					    			TimeToString.AsRptTime(new Timestamp(cr.rise.getTime())) + " at " + sn + ":\n");
		    				}
		    			}
		    			else if((cr.xbandrise == null || cr.xbandfade == null) && consParser.getSbandSites().contains(sn) && (cr.cmdrise != null && cr.cmdfade != null))
		    			{
		    				outputStreamWriter.write("(contact[" + cr.id + ": " + 
			    		    		TimeToString.AsLogTime(new Timestamp(cr.rise.getTime())) + " - " +
			    		    		TimeToString.AsLogTime(new Timestamp(cr.fade.getTime())) + "]  Rev: " + cr.revno +" Sat: " + 
			    		    		sat + " Site: " + sn + " Space: 0+0, 0+0)\n");	
		    			}
		    			else{
			    			outputStreamWriter.write("**** INFO:  Invalid or incomplete SBAND pass found at " + 
				    			TimeToString.AsRptTime(new Timestamp(cr.rise.getTime())) + " at " + sn + ":\n");
		    			}
		    				
		    			if (cr.cmdrise == null)
		    				outputStreamWriter.write("    Command Rise not defined for contact.\n");
		    			if (cr.xbandrise == null && !consParser.getSbandSites().contains(sn))
		    				outputStreamWriter.write("    X-BAND Downlink Command Rise not defined for contact.\n");
		    			if (cr.cmdfade == null)
		    				outputStreamWriter.write("    Command Fade not defined for contact.\n");
		    			if (cr.xbandfade == null && !consParser.getSbandSites().contains(sn))
		    				outputStreamWriter.write("    X-BAND Downlink Command Fade not defined for contact.\n");
		    		}
		    		else
		    		{
	    				outputStreamWriter.write("(contact[" + cr.id + ": " + 
	    		    		TimeToString.AsLogTime(new Timestamp(cr.rise.getTime())) + " - " +
	    		    		TimeToString.AsLogTime(new Timestamp(cr.fade.getTime())) + "]  Rev: " + cr.revno +" Sat: " + 
	    		    		sat + " Site: " + sn + " Space: 0+0, 0+0)\n");	
		    		}
	    		
	            }	
            }
	
	    	//Processing eclipses:
	    	outputStreamWriter.write("\nProcessing eclipses:\n");
	    	for (EclipseRecord er : consParser.getEclipses())
	        {
	        	outputStreamWriter.write("(eclipse: Satellite: " + sat + " Rev: " + er.revno +
	    	    	" Type: " + er.eclipseType + ")\n");
	        }
	    	
	    	outputStreamWriter.close();
	    	
	    } catch (Exception e) {
	    	System.err.println(e.getMessage());
	    }
	}
	
}
