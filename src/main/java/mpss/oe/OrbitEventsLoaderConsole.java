package mpss.oe;

//import java.io.FileInputStream;

import mpss.config;
import mpss.configFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class OrbitEventsLoaderConsole 
{
	public static void main(String args[])
	{
		  int scid = 1;
		  int branchid = 0;
		  String filename = "FS5_2018253_7days";      
      try {
    	  
    	  System.setProperty("satName", "FORMOSAT5");
  		  config.load();  
	      // Spring application context
    	  ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf",configFactory.getSatelliteName()}));
    	  org.apache.log4j.xml.DOMConfigurator.configure(javae.AppDomain.getPath("log4j.xml", new String[]{"conf",configFactory.getSatelliteName()}));          

    	  OrbitEventsLoader oeLoader = (OrbitEventsLoader)context.getBean(configFactory.getOrbitEventsLoader());	  
        if (oeLoader == null) {
        	System.out.println("load OrbitEventsLoader bean failed");
        	return;
        }
         
        oeLoader.setScid(scid); 
        oeLoader.setBranchid(branchid); 
    	oeLoader.setFileName(filename);
    	String oeInputFilePath = System.getProperty("dataFileDir") + System.getProperty("oeFileDir");
    	String revsfilename = oeInputFilePath + filename + "_revs.rpt";
    	String consfilename = oeInputFilePath + filename + "_cons.rpt";
    	
    	if (oeLoader.Load(revsfilename, consfilename))
		{
			System.out.println("Orbit Events Loader complete");
		}	
    	else
    	{
    		String msg = (oeLoader.getReturnMsg() == null) ? "Orbit Events Loader failed" : oeLoader.getReturnMsg();
			System.out.println(msg);
    	}
  	} catch (Exception ex) {
  		System.err.println(ex.getMessage());
    }

	}
}
