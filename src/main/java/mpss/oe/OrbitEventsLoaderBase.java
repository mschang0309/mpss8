package mpss.oe;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import mpss.configFactory;
import mpss.common.dao.AcqsumfileDao;
import mpss.common.dao.ContactDao;
import mpss.common.dao.EclipseDao;
import mpss.common.dao.ExtentDao;
import mpss.common.dao.GlobalparametersDao;
import mpss.common.dao.LatitudeDao;
import mpss.common.dao.RevDao;
import mpss.common.dao.SatelliteDao;
import mpss.common.dao.SessionDao;
import mpss.common.dao.SiteDao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Range;

// for Spring component scanning
@Service
public class OrbitEventsLoaderBase implements  OrbitEventsLoader
{
	protected int scId;
	protected int branchId;
	protected String fileName;
	protected String scName;
	protected String branchName = "";
	protected String productId;
	protected String logFileName;
	protected String revReportName;
	protected String consReportName;
	protected String oeFilePath;
	
	protected RevParser revParser;
	protected ConsParser consParser;
	
	protected OutputStreamWriter outputStreamWriter;
	protected FileOutputStream fileOutputStream;
	
	protected SortedMap<Integer, Integer> revIdMap = new TreeMap<Integer, Integer>();
	protected Range<Timestamp> revRange;// = Range.closed(Timestamp.valueOf("2000-01-01 00:00:00"), Timestamp.valueOf("2100-01-01 00:00:00"));
	protected Range<Timestamp> contactRange = Range.closed(Timestamp.valueOf("2000-01-01 00:00:00"), Timestamp.valueOf("2100-01-01 00:00:00"));
	public String returnMsg;
	
	protected List<RevContactVO> rev_contact = new ArrayList<RevContactVO>();
	
	protected Logger logger = Logger.getLogger(configFactory.getLogName());
	
	// for Spring injection
	@Autowired
	SatelliteDao satelliteDao;
	
	@Autowired
	SessionDao sessionDao;
	
	@Autowired
	SiteDao siteDao;
	
	@Autowired
	RevDao revDao;
	
	@Autowired
	EclipseDao eclipseDao;
	
	@Autowired
	LatitudeDao latitudeDao;
	
	@Autowired
	ContactDao contactDao;
	
	@Autowired
	AcqsumfileDao acqsumDao;
	
	@Autowired
	ExtentDao extentDao;
	
	@Autowired
	GlobalparametersDao gpsDao;

	public void setScid(int scid)
	{
	  this.scId = scid;
	}

	public void setBranchid(int branchid)
	{
	  this.branchId = branchid;
	}
	
	public void setFileName(String fileName)
	{
	  this.fileName = fileName;
	}
	
	public String getLogFileName()
	{
	  return logFileName;
	}
	
	public OrbitEventsLoaderBase()
	{
		// default constructor must exist for Spring
		returnMsg=null;
	}
	
	public boolean Load(String revFileName, String consFileName){
		return true;
	}
	
	public String getReturnMsg(){
		return returnMsg;
	}

  
}
