package mpss.oe;

import java.util.Date;

@SuppressWarnings("rawtypes")
public class EclipseRecord implements Comparable
{
	public int scid;
	public int revno;
	public String eclipseType;
	public Date penumbraentrancetime;
	public Date umbraentrancetime;
	public Date umbraexittime;
	public Date penumbraexittime;
	
	public String toString() {
		return "revno=" + revno + " " + "eclipseType=" + eclipseType + " " + "penumbraentrancetime=" + penumbraentrancetime + " " + "penumbraexittime=" + penumbraexittime;
	}
	
	public int compareTo(Object obj) {
		EclipseRecord compareWith = (EclipseRecord) obj;
		int priorityA = (this.eclipseType.equalsIgnoreCase("Earth/Sun")) ? 1 : 100;
		int priorityB = (compareWith.eclipseType.equalsIgnoreCase("Earth/Sun")) ? 1 : 100;
		return (int) (priorityA * this.revno - priorityB * compareWith.revno);  
	}
}
