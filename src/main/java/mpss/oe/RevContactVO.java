package mpss.oe;

public class RevContactVO {

	private int oldrevid;
	private int siteid;
	private int newrevid;
	
	public int getOldrevid() {
		return oldrevid;
	}
	public void setOldrevid(int oldrevid) {
		this.oldrevid = oldrevid;
	}
	public int getSiteid() {
		return siteid;
	}
	public void setSiteid(int siteid) {
		this.siteid = siteid;
	}
	public int getNewrevid() {
		return newrevid;
	}
	public void setNewrevid(int newrevid) {
		this.newrevid = newrevid;
	}
	
	
}
