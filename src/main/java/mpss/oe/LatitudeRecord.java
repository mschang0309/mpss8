package mpss.oe;

import java.util.Date;

@SuppressWarnings("rawtypes")
public class LatitudeRecord implements Comparable
{
	public int scid;
	public int revno;	
	public Date latitude70time;
	public Date latitude40time;
	
	
	public String toString() {
		return "revno=" + revno + " " + " " + "latitude70time=" + latitude70time + " " + "Latitude40time=" + latitude40time;
	}	
	
	public int compareTo(Object obj) {
		LatitudeRecord compareWith = (LatitudeRecord) obj;
		
		return (int) (this.revno -  compareWith.revno);  
	}
	
}
