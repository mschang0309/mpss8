package mpss.oe;

import java.util.Scanner;
import java.util.Date;
import java.io.*;
import java.lang.reflect.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.LinkedList;

import mpss.util.intervaltree.*;

public class RevParser 
{
	// context
	int scId;
	String scName;
	
	Scanner input;	
	
	String signature_;
	Date extracted;
	String title;
	String subtitle;
	String ephemeris;
	String spacecraft;
	String equinox;
	String attitude;
	
	public String getSignature()
	{
	    return this.signature_;
	}
	
	public Date getExtracted()
	{
		  return this.extracted;
	}
	
	public String geTitle()
	{
	    return this.title;
	}
	
	public String getSubtitle()
	{
	    return this.subtitle;
	}

	public String getEphemeris()
	{
	    return this.ephemeris;
	}

	public String getSpacecraft()
	{
	    return this.spacecraft;
	}

	public String getEquinox()
	{
	    return this.equinox;
	}

	public String getAttitude()
	{
	    return this.attitude;
	}

	List<RevRecord> revolutions = new LinkedList<RevRecord>();
	
	public List<RevRecord> getRevolutions()
	{
		return this.revolutions;
	}
	
	public IntervalTree<RevRecord> getRevolutionIntervals()
	{
	    IntervalTree<RevRecord> it = new IntervalTree<RevRecord>();
	    for (RevRecord r : this.revolutions)
	    {
	        it.addInterval(r.antime.getTime(),r.revendtime.getTime(),r);
	    }
	    return it;
	}
	
	Date parsedAnTime = new Date();
	Date parsedDnTime = new Date();
	Date parsedRevEndTime = new Date();
	int  parsedOrbit;
	
	public RevParser(int scId, String scName)
	{
		this.scId = scId;
		this.scName = scName;
	}
	
	//public static void main(String args[])
	//{
	//	String inputFileName="F:\\Projects\\NSPO\\MCC\\05 MPSS\\10 References\\Interface Files\\In\\FDF\\ROCSAT2_056_061_revs.rpt";
	//	
	//	RevParser parser = new RevParser();
	//	parser.open(inputFileName);
	//	parser.parse();
	//	
	//	System.out.println("Total orbits: " + parser.revolutions.size());
	//}
	
	public boolean open(String filepath)
	{
		try 
		{
		    File file = new File(filepath);
			this.input = new Scanner(file);
		} 
		catch (FileNotFoundException e) {
			System.out.println("Exception: fail to open " + filepath + "(" + e.getMessage() + ")");
			return false;
	    }		
		return true;
	}
	
	public boolean parse()
	{
		 if (!parseHeader())
		 {
		     return false;
		 }
		 
		 if (!parseRevolutions())
		 {
		     return false;
		 }
		 
		 // close all resources
		 // will it close the file, too?
		 this.input.close();
		
	   return true;
	}

  private boolean startsWith(String str, String prefix, boolean ignoreCase) {
      if (str == null || prefix == null) {
          return (str == null && prefix == null);
      }
      if (prefix.length() > str.length()) {
          return false;
      }
      return str.regionMatches(ignoreCase, 0, prefix, 0, prefix.length());
  }
  
	private boolean parseHeader()
	{
     // skip Event Report
     while (this.input.hasNext())
     {
         String line = this.input.nextLine();
         line = line.trim();
         if (line.equals("Ephemeris Report"))
         {
             break;
         }
     }
     
     
     //Ephemeris Report
     //
     //
     //Signature:  OASYS v4.3.6 (December 6, 2002), Integral Systems, Inc.
     while (this.input.hasNext())
     {
         String line = this.input.nextLine();
         line = line.trim();
         if (startsWith(line, "Signature:", true))
         {
         	   String tok[] = line.split(" +"); // split by one or more spaces
         	   if (tok.length < 2 && !tok[1].equals("OASYS"))
         	   {
         	   	   System.out.println("Parse fails by unknown file signature (" + line + ")");
         	       return false;
         	   }
         	   else
         	   {
         		  signature_ = line.replace("Signature:  ", "");
         	   }
             break;
         }
     }
     
     //Extracted:  02/26/09 07:31:19
     //Title:  d:\mpss.ops\data\OASYS\temp\056_061_rev.adb
     //Subtitle:  
     //Ephemeris:  \mpss.ops\data\ROCSAT2\elements\RS2_20090224_god_15days.eph
     //Spacecraft:  [ROCSAT2] rocsat2
     //Equinox:  MEME of Epoch [JD2000]
     //Attitude:  AttitudeReference
		 String[] headerFields = new String[]{"Extracted", "Title", "Subtitle", "Ephemeris", "Spacecraft", "Equinox",  "Attitude"};
		 String[] headerMembers = new String[]{"extracted", "title", "subtitle", "ephemeris", "spacecraft", "equinox", "attitude"};
		 int[] headerTypes = new int[]{2,1,1,1,1,1,1};
     for (int lineno = 0; lineno < headerFields.length; lineno++)
     {
	    	try
	    	{
                if (this.input.hasNext())
                {
            	    Scanner lineTokenizer = new Scanner(this.input.nextLine());
            	    lineTokenizer.useDelimiter(":");
            	    String name = lineTokenizer.next();
            	    name = name.trim();
            	    if (!name.equals(headerFields[lineno]))
            	    {
            	    	System.out.println("Field not found: " + name);
            	    	lineTokenizer.close();
            	        return false;
            	    }
            	
            	    if (lineTokenizer.hasNext())
            	    {
                	    String data = lineTokenizer.next();
    	            	// date and filepath format will conflict with delimiter
                	    while (lineTokenizer.hasNext())
                	    {
        	            	data = data.concat(":").concat(lineTokenizer.next());
                	    }
                	    data = data.trim();
                	    
                	    // use reflection to set class members
            	        Class<?>  clazz = this.getClass();
            	        Field field = clazz.getDeclaredField(headerMembers[lineno]);
            	        switch (headerTypes[lineno])
            	        {
            	            // string
            	            case 1:
            	            	data = data.trim();
            	                field.set(this, data);
            	                break;
            	            // date
            	            case 2:
            	    	        DateFormat formatter = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
            	    	        Date date = (Date) formatter.parse(data);              	        
            	    	        field.set(this, date);
            	    	        //System.out.println(this.datetime);
              	                break;            	    	  
            	        }
            	    }
            	    lineTokenizer.close();
	    	    }
	    	}
        catch (Exception e)
	    	{
	    	    System.out.println("Exception: " + e.getMessage());
	    	}
     }

     //
     //
     //
     //
     //                   UTC  Orbit Number
     //                              orbits
     
     // skip unnecessary lines (including empty lines)
     while (this.input.hasNext())
     {
         String line = this.input.nextLine();
         line = line.trim();
         if (line.equals("orbits"))
         {
             break;
         }
     }
     
	   return true;
	}

	private boolean parseRevolutions()
	{
     // skip unnecessary lines (including empty lines)
     String line = new String();
     while (this.input.hasNextLine())
     {
         line = this.input.nextLine();
         line = line.trim();
         if (line.length() > 0)
         {
        	 // should be first AN
        	 if (line.indexOf("Descending")>0)
        	 {
        		 line = this.input.nextLine();
        		 line = this.input.nextLine();
        	 }
        	 break;
         }
     }
     
     // should be first AN
     if (!parseAN(line, true))
     {
    	 return false;
     }

     
     while (true)
     {
         // parse orbit number
     	   if (this.input.hasNextLine())
     	   {
             parseOrbit(this.input.nextLine());
         }
         else
         {
         	   // eof
             break;
         }
         
         // parse DN 
     	   if (this.input.hasNextLine())
     	   {
             parseDN(this.input.nextLine());
         }
         else
         {
         	   // eof
             break;
         }
         
         // skip half orbit number
     	   if (this.input.hasNextLine())
     	   {
             this.input.nextLine();
         }
         else
         {
         	   // eof
             break;
         }

         // parse AN (look ahead to determine revolution endtime)
     	   if (this.input.hasNextLine())
     	   {
             parseAN(this.input.nextLine(), false);
         }
         else
         {
         	   // eof
             break;
         }
     }
     
	   return true;
	}
	
  private boolean parseAN(String text, boolean first)
  {
      String tok[] = text.split(" +"); // split by one or more spaces
      if (tok.length < 8)
      {
      	   System.out.println("Parse AN fails (" + text + ")");
          return false;
      }
      String szType = tok[2].concat(" ").concat(tok[3]);
      szType = szType.trim();
      if (!szType.equals("Ascending Node"))
      {
      	  System.out.println("Parse AN fails (" + text + ")");
          return false;
      }
      String szAnTime = tok[0].concat(" ").concat(tok[1]);
      DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
      Date anTime = new Date();
      try
      {
          anTime = (Date) formatter.parse(szAnTime);   
      }
      catch (Exception e)
      {
    	  System.out.println("Exception: " + e.getMessage());
      }
      
      if (first)
      {
          // set AN time
          this.parsedAnTime = anTime;
      }
      else
      {
    	  // revno must be more than zero
      	  if (this.parsedOrbit == -1)
      	  {
      		  // set AN time
      		  this.parsedAnTime = anTime;
    		  return true;
      	  }

    	  // set AN time to be end time of last revolution
      	  this.parsedRevEndTime = anTime;
      	  
      	  // add to revolution entry list
      	  RevRecord rec = new RevRecord();
      	  //String scid;
	        //int revno;
	        //Date antime;
	        //Date dntime;
	        //Date revendtime;
      	  rec.scid = this.scId;
      	  rec.revno = this.parsedOrbit;
      	  rec.antime = this.parsedAnTime;
      	  rec.dntime = this.parsedDnTime;
      	  rec.revendtime = this.parsedRevEndTime;
      	  
      	  this.revolutions.add(rec);
      	  
          // set AN time
          this.parsedAnTime = anTime;
      }
      return true;
  }

  private boolean parseDN(String text)
  {
      String tok[] = text.split(" +"); // split by one or more spaces
      if (tok.length < 8)
      {
      	   System.out.println("Parse DN fails (" + text + ")");
          return false;
      }
      String szType = tok[2].concat(" ").concat(tok[3]);
      szType = szType.trim();
      if (!szType.equals("Descending Node"))
      {
      	  System.out.println("Parse DN fails (" + text + ")");
          return false;
      }
      String szDnTime = tok[0].concat(" ").concat(tok[1]);
      DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
      Date dnTime = new Date();
      try
      {
          dnTime = (Date) formatter.parse(szDnTime);   
      }
      catch (Exception e)
      {
    	  System.out.println("Exception: " + e.getMessage());
      }
      this.parsedDnTime = dnTime;      
           	        
      return true;
  }

  private boolean parseOrbit(String text)
  {
      String tok[] = text.split(" +"); // split by one or more spaces
      if (tok.length != 3)
      {
      	  System.out.println("Parse Orbit fails (" + text + ")");
          return false;
      }
      
      try
      {
          Double ob = Double.parseDouble(tok[2]);
          if (ob < 0)
        	  this.parsedOrbit = -1;
          else
        	  this.parsedOrbit = (int)Double.parseDouble(tok[2]);
      }
      catch (NumberFormatException e)
      {
    	  System.out.println("ParseOrbit exception (" + e.getMessage() + ")");
    	  return false;
      }
      
      return true;
  }
}