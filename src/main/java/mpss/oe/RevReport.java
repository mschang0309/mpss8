package mpss.oe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;

import mpss.util.timeformat.TimeToString;

public class RevReport {
	private RevParser revParser;
	
	public RevReport(RevParser revParser)
	{
		this.revParser = revParser;
	}

	public void genReport(String ingestFile, String reportFile) {
		
		File file = new File(reportFile);
		if(!file.exists())
		{
		    try {
		        file.createNewFile();
		    } catch (Exception e) {
		        System.err.println("OrbitEventsLoader Error: Rev Report file can not be create: " + reportFile);
		    }
		}
		
	    try {
	    	FileOutputStream fileOutputStream = new FileOutputStream(reportFile);
	    	OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
	    	
	    	 //Header
		    Timestamp currentTime = new Timestamp(System.currentTimeMillis());
	    	outputStreamWriter.write("Ingesting " + ingestFile + " at " + TimeToString.AsRptTime(currentTime) + "\n\n");
	    	outputStreamWriter.write(" Signature:  " + revParser.signature_ + "\n");
	    	outputStreamWriter.write(" Extracted:  " + TimeToString.AsShortTime(new Timestamp(revParser.extracted.getTime())) + "\n");
	    	outputStreamWriter.write("     Title:  " + revParser.title + "\n");
	    	outputStreamWriter.write("  Subtitle:  " + revParser.subtitle + "\n");
	    	outputStreamWriter.write(" Ephemeris:  " + revParser.ephemeris + "\n");
	    	outputStreamWriter.write("Spacecraft:  " + revParser.spacecraft + "\n");
	    	outputStreamWriter.write("   Equinox:  " + revParser.equinox + "\n");
	    	outputStreamWriter.write("  Attitude:  " + revParser.attitude + "\n\n");
	    	
	    	//Record detail
	    	String sat = revParser.getSpacecraft().split("]")[0].substring(1);//[FS5] FS5
	    	for (RevRecord rr : revParser.getRevolutions())
	        {
	        	outputStreamWriter.write("(rev: Satellite: " + sat + " RevNo: " + rr.revno +
	    	    	" AN: " + TimeToString.AsLogTime(new Timestamp(rr.antime.getTime())) +
	    	    	" DN: " + TimeToString.AsLogTime(new Timestamp(rr.dntime.getTime())) +
	    	    	" EndTime: " + TimeToString.AsLogTime(new Timestamp(rr.revendtime.getTime())) +
	    	    	")\n");
	        }
	    	outputStreamWriter.close();
	    	
	    } catch (Exception e) {
	    	System.err.println(e.getMessage());
	    }
	}
	
}
