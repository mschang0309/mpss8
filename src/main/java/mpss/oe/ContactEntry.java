package mpss.oe;

import java.util.Date;

@SuppressWarnings("rawtypes")
public class ContactEntry implements Comparable
{
	public int typeCode;
	//public String scid;
	public int revno;
	public Date datetime;
	public String site;
	public String paramName;
	public double paramValue;
	public String paramUnit;

  public int compareTo(Object obj) {
    ContactEntry compareWith = (ContactEntry) obj;
    
    //ascending order
		return this.typeCode - compareWith.typeCode;  
	}

  public boolean equals(Object obj) {
    if (!(obj instanceof EclipseEntry)) {
      return false;
    }
    ContactEntry compareWith = (ContactEntry) obj;
    
		return this.typeCode == compareWith.typeCode;  
  }
}
