package mpss.oe;

import java.util.Date;

@SuppressWarnings("rawtypes")
public class ContactRecord implements Comparable
{
	public int scid;
	public String siteid;
	public int revno;
	public Date rise;
	public Date fade;
	public Date cmdrise;
	public Date cmdfade;
	public Date xbandrise;
	public Date xbandfade;
	public Date maxeltime;
	public int id;
	public int maxel;
	
	public String toString() {
		return "revno=" + revno + " " + "siteid=" + siteid + " " + "rise=" + rise + " " + "fade=" + fade +
				 " " + "cmdrise=" + cmdrise +  " " + "cmdfade=" + cmdfade + " " + "xbandrise=" + xbandrise +
				 " " + "xbandfade=" + xbandfade;
	}

	public int compareTo(Object obj) {
		ContactRecord compareWith = (ContactRecord) obj;
		    
		return (int) (this.rise.getTime() - compareWith.rise.getTime());  
	}
	
//	  public boolean equals(Object obj) {
//
//		  ContactRecord compareWith = (ContactRecord) obj;
//		    
//				return this.revno == compareWith.revno;  
//		  }

}
