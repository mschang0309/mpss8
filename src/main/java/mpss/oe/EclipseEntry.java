package mpss.oe;

import java.util.Date;

@SuppressWarnings("rawtypes")
public class EclipseEntry implements Comparable
{
	public int typeCode;
	//public String scid;
	public int revno;
	public Date datetime;
	public String eclipseType;
	public String paramName;
	public double paramValue;
	public String paramUnit;

  public int compareTo(Object obj) {
    EclipseEntry compareWith = (EclipseEntry) obj;
    
    //ascending order
		return this.typeCode - compareWith.typeCode;  
	}

  public boolean equals(Object obj) {
    if (!(obj instanceof EclipseEntry)) {
      return false;
    }
    EclipseEntry compareWith = (EclipseEntry) obj;
    
		return this.typeCode == compareWith.typeCode;  
  }
}
