package mpss.oe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import mpss.common.jpa.Acqsumfile;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Eclipse;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Latitude;
import mpss.common.jpa.Rev;
import mpss.common.jpa.Site;
import mpss.util.intervaltree.IntervalTree;
import mpss.util.timeformat.TimeToString;

import org.springframework.stereotype.Component;

import com.google.common.collect.Range;

// for Spring component scanning
@Component("FS5orbitEventsLoader")
public class FS5OrbitEventsLoader  extends OrbitEventsLoaderBase
{	
	
	public FS5OrbitEventsLoader()
	{
		// default constructor must exist for Spring
		returnMsg=null;
	}

	public boolean Load(String revFileName, String consFileName)
	{
		if (satelliteDao == null) {			
			logger.info("OrbitEventsLoader Load failed: satelliteDao not injected");
			return false;
		}
		if (sessionDao == null) {
			logger.info("OrbitEventsLoader Load failed: sessionDao not injected");
			return false;
		}
		if (siteDao == null) {
			logger.info("OrbitEventsLoader Load failed: siteDao not injected");
			return false;
		}
		if (revDao == null) {
			logger.info("OrbitEventsLoader Load failed: revDao not injected");
			return false;
		}
		if (eclipseDao == null) {
			logger.info("OrbitEventsLoader Load failed: eclipseDao not injected");
			return false;
		}
		
		if (latitudeDao == null) {
			logger.info("OrbitEventsLoader Load failed: latitudeDao not injected");
			return false;
		}		
		
		if (contactDao == null) {
			logger.info("OrbitEventsLoader Load failed: contactDao not injected");
			return false;
		}
		if (acqsumDao == null) {
			logger.info("OrbitEventsLoader Load failed: acqsumDao not injected");
			return false;
		}
		
		scName = satelliteDao.get((long)scId).getName();
		revReportName = fileName + "_revs";
		consReportName = fileName + "_cons";
		if(branchId > 0)
		{
			branchName = sessionDao.get((long)branchId).getName();
			productId = fileName + "_" + branchName;
			revReportName = revReportName + "_" + branchName + ".rpt";
			consReportName = consReportName + "_" + branchName + ".rpt";
		}
		else
		{
			productId = fileName;
			revReportName += ".rpt";
			consReportName += ".rpt";
		}
		logFileName = "log_" + productId + ".html";
		
		oeFilePath = System.getProperty("reportFileDir") + System.getProperty("oeReportDir");
	    String reportFilePath = oeFilePath + System.getProperty("oeIngestReportDir");
	    logger.debug("reportFilePath =" + reportFilePath);
		
		if (!LoadRevs(revFileName))
		{
			return false;
		}
		else
		{
		    //creat rev report file
		    RevReport revReport = new RevReport(revParser);
		    revReport.genReport(revFileName, reportFilePath + revReportName);
		}
		
	  
		// build revno to revid map
		if (!LoadCons(consFileName))
		{
			return false;
		}
		else
		{
		    //creat rev report file
			ContactReport consReport = new ContactReport(consParser);
			consReport.genReport(consFileName, reportFilePath + consReportName);
		}
	  
		// Method used to write ingest log file header
		WriteLog();
		return true;
	  
	}
	
	// load revs file
	private boolean LoadRevs(String revFileName)
	{
		revIdMap.clear();
		this.revParser = new RevParser(this.scId, this.scName);
	    if (!revParser.open(revFileName))
	    {
	    	returnMsg = "File [" + revFileName + "] not exist!";
	    	logger.info("LoadRevs: revParser open failed:"+ revFileName);
	    	return false;
	    	
	    }
	    
	    if (!revParser.parse())
	    {
	    	return false;
	    }
		
	    logger.info("Total orbits: " + revParser.revolutions.size());
	    

	    //check ingested file not conflicts with DB
	    boolean flgFirst = true;
	    int existRevno = 0;
	    try
	    { 	
	    	int maxDrift = satelliteDao.get((long)scId).getMaxandrifttime();
	    	if (revDao.getMaxRev(scId, branchId) != null)
	    		existRevno = revDao.getMaxRev(scId, branchId).getRevno();
//	    	if (revDao.getMaxRev(scId, branchId) != null)
	    	if (existRevno > 0)
		    {
	    		for (RevRecord rr : revParser.getRevolutions())
		    	{
		    	    /*********************************************************************
		    	     * we want to keep track of first revs orbit number and start        *
		    	     * we will then check the new rev info against existing revs in db   *
		    	     * do not allow rev events to be ingested if:                        *
		    	     *     new rev # < latest rev # in DB                                *
		    	     *     && new rev time > latest rev time                             *
		    	     *                                                                   *
		    	     *  If this case is found, throw an exception, and do not continue   *
		    	     ********************************************************************/
		        	if (flgFirst)
					{
		    			flgFirst = false;
//						existRevno = revDao.getMaxRev(scId, branchId).getRevno();
						Timestamp existAntme = revDao.getMaxRev(scId, branchId).getAntime();
		    			SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");    
		    	    	if (rr.antime.getTime() >= existAntme.getTime() && rr.revno < existRevno
		    				|| rr.antime.getTime() < existAntme.getTime() && rr.revno > existRevno)
		    			{
		    				returnMsg = "New orbit(revno:" + rr.revno + " - " + df.format(rr.antime) + ")" +
		    					"conflicts with latest orbit in DB(revno:" + existRevno + " - " + df.format(existAntme) + ")";
		    				return false;
		    			}
					}
		    		
		    		/*********************************************************************
		    		* we need to iterate through all rev events and pair them up twice - *
		    		* the first time we need to find the revs so they may be compared to *
		    		* existing ClRev instances (if we create a ClRev we will replace and *
		    		* we cannot do that until we verify the drift is not too great), the *
		    		* second time through we create ClRevs.                              *
		    		**********************************************************************/
		    		if (rr.revno <= existRevno)
					{
		    			Rev dbRev = revDao.getByRevno(rr.revno, scId, branchId);
		    			if (dbRev != null)
		    			{
		    				long drift = Math.abs(rr.antime.getTime() - revDao.getByRevno(rr.revno, scId, branchId).getAntime().getTime());
			    			if (drift > maxDrift * 1000)
			    			{
			    				returnMsg ="revno:" + rr.revno + " Drift of " + drift/1000.0 + " exceeds maximum drift of " +
			    				           maxDrift + ".  Ingest aborted";
			    				return false;
			    			}
			    			
			    			//check db contact is scheduled ,by jeff 20170830
//			    			for(Contact c : contactDao.getContactsByRevid(dbRev.getId().intValue()))
//		        	    	{		        	    		
//		        	    		if(findContactHasExtent(c)){
//		        	    			returnMsg ="Contact: "+ c.getId() +" is scheduled ! Ingest aborted";
//				    				return false;
//		        	    		}		        	    		
//		        	    	}
			    			
			    			
		    			}
					}
		    		else
		    		{
		    			break;
		    		}
		    	}
		    }
	    }
	    catch (Exception e)
		{
			logger.info("LoadRevs ex= " + e.getMessage());
			return false;
		}
	    rev_contact.clear();
	    try
	    {
	    	int countRevs = 0;
	    	revRange = Range.closed(Timestamp.valueOf("2000-01-01 00:00:00"), Timestamp.valueOf("2100-01-01 00:00:00"));
    		for (RevRecord rr : revParser.getRevolutions())
	        {
	        	Rev rev = new Rev();
	    	    rev.setBranchid(this.branchId);
	    	    rev.setScid(this.scId);
	        	rev.setRevno(rr.revno);
	            rev.setAntime(new Timestamp(rr.antime.getTime()));
	            rev.setDntime(new Timestamp(rr.dntime.getTime()));
	            rev.setRevendtime(new Timestamp(rr.revendtime.getTime()));
	    		logger.debug("LoadRevs: save Rev: " + rev.getRevno());
	    		
	    		// Get the range from the rev file 
	    		if (countRevs == 0 && (rr.antime.getTime() > revRange.lowerEndpoint().getTime()))
	    		{
	    			revRange = Range.closed(new Timestamp(rr.antime.getTime()), revRange.upperEndpoint());
	    		}
	    		if ((countRevs == revParser.revolutions.size() - 1) && (rr.revendtime.getTime() < revRange.upperEndpoint().getTime()))
	    		{
	    			revRange = Range.closed(revRange.lowerEndpoint() , new Timestamp(rr.revendtime.getTime()));
	    		}
	    		
	    		
	    		Rev newRev = new Rev();
	    		try
	    		{
	    			
	    			//RevContactVO vo =null;
	        	    if (rr.revno <= existRevno)
	        	    {
	        	    	Rev delRev = revDao.getByRevno(rr.revno, scId, branchId);	        	    	
	        	    	if (delRev != null )
	        	    	{
	        	    		
	        	    			revDao.delete(delRev);
			        	    	for(Eclipse e : eclipseDao.getEclipsesByRevid(delRev.getId().intValue()))
			        	    	{
			        	    		eclipseDao.delete(e);
			        	    	}
			        	    	
			        	    	for(Latitude e : latitudeDao.getLatitudesByRevid(delRev.getId().intValue()))
			        	    	{
			        	    		latitudeDao.delete(e);
			        	    	}			        	    	
			        	    	
			        	    	for(Contact c : contactDao.getContactsByRevid(delRev.getId().intValue()))
			        	    	{
//			        	    		//have three steps : 1. update contact has extent can't delete  ,by jeff 20170830
//			        	    		if(findContactHasExtent(c)){
//			        	    			vo = new RevContactVO();	
//			        	    			vo.setOldrevid(c.getRevid());
//			        	    			vo.setSiteid(c.getSiteid());
//			        	    			//System.out.println("old revid ="+c.getRevid()+",contact id ="+c.getId());
//			        	    		}else{
//			        	    			contactDao.delete(c);
//			        	    		}
			        	    		
			        	    		contactDao.delete(c);
			        	    		
			        	    	}       	    			
	        	    		
	        	    	}
	        	    }
	        	    
	        	    
	        	    newRev = revDao.create(rev);
		        	countRevs++;
		        	
//		        	if(vo!=null){	
//		        		//2. keep data  ,by jeff 20170830 
//	        	    	vo.setNewrevid(newRev.getId().intValue());
//	        	    	rev_contact.add(vo);
//	        	    	//System.out.println("add new revid="+vo.getNewrevid());
//	        	    }
	        	    
	        	    
	    		}
	    		catch (Exception e)
	    		{
	    			logger.info("create Rev no "+rr.revno+" failed "+  e.getMessage());
	    			newRev = null;
	    		}
	        	if (newRev != null)
	        	{
	        	    logger.debug("Rev Id = "+newRev.getId());
		        	// create rev Id mapping
		        	if (!this.revIdMap.containsKey(newRev.getRevno())) {
		        	  this.revIdMap.put(newRev.getRevno(), newRev.getId().intValue());
		            }
	        	    	        		
	        	}
	        	
	        }
	        
        }
	    catch (Exception e)
	    {
	    	logger.info("Exception: "+e.getMessage());
	    	return false;
	    }
	    
	    return true;
	}

  // load cons file	
	private boolean LoadCons(String consFileName)
	{
		logger.info("LoadCons: Enter");
		this.consParser = new ConsParser(this.scId, this.scName);

		// load sites
		List<String> sbandSites = new ArrayList<String>();
        Map<String, Integer> siteIdMap = new HashMap<String, Integer>();		
        List<Site> sites = this.siteDao.getAll();
		for (Site s : sites)
		{
	      if (!siteIdMap.containsKey(s.getName())) {
	        siteIdMap.put(s.getName().toLowerCase(), s.getId().intValue());
	      }
	      if (s.getSitetype().equalsIgnoreCase("SBAND"))
	      {
	    	  sbandSites.add(s.getName());
	      }
		}
    	consParser.setSiteNames(siteIdMap.keySet());
    	consParser.setSbandSites(sbandSites);
    	
    	
		if (!consParser.open(consFileName))
		{
			returnMsg = "File [" + consFileName + "] not exist!";
			logger.info("LoadCons: ConsParser open failed: "+ consFileName);
			return false;
		}
		
	    // build revoultion interval tree
		IntervalTree<RevRecord> it = this.revParser.getRevolutionIntervals();
		this.consParser.setRevolutionIntervals(it);
	    //this.consParser.setRevolutionIntervals(this.revParser.getRevolutionIntervals());
	  
		//System.out.println("LoadCons: ConsParser setRevolutionIntervals complete");
		
		if (!consParser.parse())
		{
			logger.info("LoadCons: ConsParser parse failed\n");
			return false;
		}

		// Get the range from the contacts file
		contactRange = Range.closed(consParser.getRangFirst(), consParser.getRangSecond());
		
	    SimpleDateFormat dtFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		// get eclipses
		logger.info("LoadCons: Total eclispes: " + consParser.getEclipses().size());
		int erCount = 0;
		//int countEC = 0;
		try
		{
			for (EclipseRecord er : consParser.getEclipses())
			{
				erCount++;
				
				Eclipse ec = new Eclipse();
				logger.info("er type"+er.eclipseType);
				logger.info("LoadCons: process record("+erCount+") pass: "+er.revno+", penumbraentrancetime: "+dtFormatter.format(er.penumbraentrancetime)+", penumbraexittime: "+dtFormatter.format(er.penumbraexittime) );
				
		        ec.setBranchid(this.branchId);
		        ec.setSatelliteid(this.scId);		        
		        ec.setRevid(this.revIdMap.containsKey(er.revno) ? this.revIdMap.get(er.revno) : 0);
		        ec.setEclipsetype(er.eclipseType);
		        ec.setPenumbraentrancetime(new Timestamp(er.penumbraentrancetime.getTime()));
		        ec.setPenumbraexittime(new Timestamp(er.penumbraexittime.getTime()));
		        ec.setUmbraentrancetime(new Timestamp(er.umbraentrancetime.getTime()));
		        ec.setUmbraexittime(new Timestamp(er.umbraexittime.getTime()));  

		        try{
		        		this.eclipseDao.create(ec);
		        }
		        catch (Exception e){
			        logger.info("LoadCons: Create Eclipse exception: "+ e.getMessage());
		        }		        
			}
			
			
			for (LatitudeRecord er : consParser.getLatitudes())
			{
				Latitude ec = new Latitude();
				ec.setBranchid(this.branchId);
		        ec.setSatelliteid(this.scId);		        
		        ec.setRevid(this.revIdMap.containsKey(er.revno) ? this.revIdMap.get(er.revno) : 0);		        
		        ec.setLatitude70time(new Timestamp(er.latitude70time.getTime()));
		        ec.setLatitude40time(new Timestamp(er.latitude40time.getTime()));
		        
				try{
		        		this.latitudeDao.create(ec);
		        }	
		        catch (Exception e){
			        logger.info("LoadCons: Create Latitude exception: "+ e.getMessage());
		        }
			}
			
		}
	    catch (Exception e)
	    {
	    	logger.info("Exception: "+e.getMessage());
	    	return false;
	    }

		// get contacts
		logger.info("LoadCons: Total contacts: " + consParser.getContacts().size());
		int maxel=5;
		gpsDao.reset();
		if(gpsDao.getById(51)!=null){
			maxel=gpsDao.getById(51).getOrbminmaxel();
		}
		 
		int ctCount = 0;
		//int countCT = 0;
		//Timestamp defaultTimestamp = new Timestamp((long) 0);
        try
        {
        	Map<String, SortedSet<ContactRecord> > contactListMap = new HashMap<String, SortedSet<ContactRecord> >();
        	for (ContactRecord cr : consParser.getContacts())
    		{
    			ctCount++;
    			
    			logger.info("LoadCons: process record("+ctCount+") site: "+cr.siteid+", pass: "+ cr.revno+", rise: "+dtFormatter.format(cr.rise)+", fade: "+dtFormatter.format(cr.fade));
    			// map site id
    			int siteId;
    			if (siteIdMap.containsKey(cr.siteid.toLowerCase()))
    		    {
    				siteId = siteIdMap.get(cr.siteid.toLowerCase());
    				
    				if (contactListMap.get(cr.siteid.toUpperCase()) == null)
    	        	{
    	        		contactListMap.put(cr.siteid.toUpperCase(), new TreeSet<ContactRecord>());
    	        	}
    	        	SortedSet<ContactRecord> consList = contactListMap.get(cr.siteid.toUpperCase());
    	        	try
    	        	{
    	        		consList.add(cr);
    	        	}
    	        	catch (Exception e)
    	        	{
    	        	    logger.info("Can not add contact Record Map (" + e.getMessage() + ")");    	    			
    	        	}
    				
    			}
    			// skip record of unknown sites
    			else
    			{
    				logger.info("LoadCons: skip record of unknown site: "+ cr.siteid);  
    				continue;
    				
    			}
    			Contact ct = new Contact();
    	        //Long id;
    	        //Integer satelliteid;
                //Integer siteid;
                //Integer revid
    	        //Integer branchid;
    	        //Timestamp rise;
    	        //Timestamp fade;
    	        //Timestamp cmdrise;
    	        //Timestamp cmdfade;
    	        //Timestamp maxeltime;
    	        //Timestamp xbandrise;
    	        //Timestamp xbandfade;
    	        ct.setBranchid(this.branchId);
    	        ct.setSatelliteid(this.scId);
    	        ct.setSiteid(siteId);
    	        ct.setRevid(this.revIdMap.containsKey(cr.revno) ? this.revIdMap.get(cr.revno) : 0);
    	        if (cr.rise != null)
    	        {
    	            ct.setRise(new Timestamp(cr.rise.getTime()));
    	            ct.setStartuot(new Timestamp(cr.rise.getTime()));
    	        }
    	        else
    	        {
    	            logger.info("rise is null ("+ctCount+": "+cr.siteid+")");  
    	        }
    	        if (cr.fade != null)
    	        {
    		        ct.setFade(new Timestamp(cr.fade.getTime()));
    		        ct.setEnduot(new Timestamp(cr.fade.getTime()));
    	        }
    	        else
    	        {
    	            logger.info("fade is null ("+ctCount+": "+cr.siteid+")");  
    	        }
    	        if (cr.cmdrise != null)
    	        {
    		        ct.setCmdrise(new Timestamp(cr.cmdrise.getTime()));
    	        }
    	        else
    	        {
    	            logger.info("cmdrise is null ("+ctCount+": "+cr.siteid+")"); 
    	            continue;
    	        }
    	        if (cr.cmdfade != null)
    	        {
    		        ct.setCmdfade(new Timestamp(cr.cmdfade.getTime()));
    	        }
    	        else
    	        {
    	            logger.info("cmdfade is null ("+ctCount+": "+cr.siteid+")"); 
    	            continue;
    	        }
    	        if (cr.maxeltime != null)
    	        {
    		        ct.setMaxeltime(new Timestamp(cr.maxeltime.getTime()));
    		        ct.setMaxel(cr.maxel);
    	            //System.out.println("maxeltime: " + cr.maxeltime);
    	        }
    	        else
    	        {
    	            logger.info("maxeltime is null ("+ctCount+": "+cr.siteid+")"); 
    	        	
    	        }
    	        if (cr.xbandrise != null)
    	        {
    		        ct.setXbandrise(new Timestamp(cr.xbandrise.getTime()));
    	        }
    	        else
    	        {
    	            logger.info("xbandrise is null ("+ctCount+": "+cr.siteid+")"); 
    	            if (sbandSites.contains(cr.siteid.toUpperCase()))
    	            {
    	            	ct.setXbandrise(Timestamp.valueOf("2004-01-01 00:00:00"));
    	            }
    	            else
    	            {
    	            	continue;
    	            }
    	        }
    	        if (cr.xbandfade != null)
    	        {
    		        ct.setXbandfade(new Timestamp(cr.xbandfade.getTime()));
    	        }
    	        else
    	        {
    	            logger.info("xbandfade is null ("+ctCount+": "+cr.siteid+")"); 
    	            if (sbandSites.contains(cr.siteid.toUpperCase()))
    	            {
    	            	ct.setXbandfade(Timestamp.valueOf("2004-01-01 00:00:00"));
    	            }
    	            else
    	            {
    	            	continue;
    	            }
    	        }
    	        // default values (not nullable)
    	        //Integer available;
    	        //Integer backbias;
    	        //Integer backexp;
    	        //Integer frontbias;
    	        //Integer frontexp;
    	        //Integer maxel;
    	        //Integer uotcnt;
    	        //Timestamp startuot;
    	        //Timestamp enduot;
    	        ct.setAvailable(1);
    	        ct.setBackbias(0);
    	        ct.setBackexp(0);
    	        ct.setFrontbias(0);
    	        ct.setFrontexp(0);
    	        ct.setUotcnt(1);
    	        
    	        Contact newCt=null;
    	        try
    	        {
    	        	//rev_contact
    	        	if(rev_contact.size()>0){
    	        		boolean createflag = true;
//    	        		for(RevContactVO vo:rev_contact){    	        			
//    	        			if(vo.getNewrevid()==ct.getRevid() && vo.getSiteid() == ct.getSiteid()){
//    	        				createflag=false;
//    	        				//3. update contact old revid to new revid ,by jeff 20170830
//    	        				String sql = "update contact set revid='"+vo.getNewrevid()+"' where revid='"+vo.getOldrevid()+"' and siteid='"+vo.getSiteid()+"'";
//    	        				//System.out.println("update contact sql ="+sql);
//    	        				this.contactDao.doSql(sql);
//    	        				newCt = null;
//    	        			}
//    	        		}
    	        		if(createflag){
    	        			if(ct.getMaxel() >= maxel){
    	        				newCt = this.contactDao.create(ct);
    	        			}
    	        			
    	        		}
    	        	}else{
    	        		if(ct.getMaxel() >= maxel){
    	        			newCt = this.contactDao.create(ct);
    	        		}
    	        	}
    	        	
		        	   	        	
    	        	//countCT++;

    	        }
    	        catch (Exception e)
    	        {
    	        	logger.info("LoadCons: create contact exception: "+e.getMessage()); 
    	        	newCt = null;
    	        }
    	        if (newCt != null)
	        	{
	        	    // create contact Id mapping
//		        	if (!this.consIdMap.containsKey(ct.getSiteid() + "-" + ct.getRise().getTime())) {
//		        	  this.consIdMap.put(ct.getSiteid() + "-" + ct.getRise().getTime(), ct.getId().intValue());
//		            }
    	        	cr.id=ct.getId().intValue();
	        	    	        		
	        	}
    		}
        	
        	
        	consParser.setContactListMap(contactListMap);
        }
	    catch (Exception e)
	    {
	    	logger.info("Exception: "+e.getMessage()); 
	    	return false;
	    }
		return true;
	}
	
	private void WriteLog()
	{
		// define ingest session range
		Timestamp Utimestamp,Ltimestamp;
		if (revRange.lowerEndpoint().getTime() < contactRange.lowerEndpoint().getTime())
		{
			Ltimestamp = revRange.lowerEndpoint();
		}
		else
		{
			Ltimestamp = contactRange.lowerEndpoint();
		}
		if (revRange.upperEndpoint().getTime() > contactRange.upperEndpoint().getTime())
		{
			Utimestamp = revRange.upperEndpoint();
		}
		else
		{
			Utimestamp = contactRange.upperEndpoint();
		}
		
		// build the log file name with directory path
		//String logFilePath = reportFileDir + "Orbit_event_reports\\ing_log_files\\" + logFileName;
		String logFilePath = oeFilePath + System.getProperty("oeIngestLogDir") + logFileName;
		logger.info("OrbitEventsLoader file : " + logFileName); 

		/// open the log file for output
		File file = new File(logFilePath);
		if(!file.exists())
		{
		    try {
		        file.createNewFile();
		    } catch (Exception e) {
		       logger.info("OrbitEventsLoader Error: Log file can not be create: " + logFileName); 
		    }
		}
	    try {
	    	fileOutputStream = new FileOutputStream(logFilePath);
	        outputStreamWriter = new OutputStreamWriter(fileOutputStream);
	    } catch (Exception e) {
	        logger.info("OrbitEventsLoader Error: Log file can not be opened: " + logFileName); 
	    }
		
		
		// get the current time
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
				
		try
		{
			outputStreamWriter.write("<html>\n<head>\n</head>\n<body>\n<pre>\n");
			outputStreamWriter.write("***  Running Orbital Event Ingest software on " +
					TimeToString.AsRptTime(currentTime) + " (" + TimeToString.AsShortDate(currentTime) + ")\n\n");

			
			outputStreamWriter.write("***  Log File Name :             " + logFilePath + "\n");
			outputStreamWriter.write("***  Product ID :                " + productId + "\n");
			outputStreamWriter.write("***  Base Ingest File Name:      " + fileName + "\n");
			outputStreamWriter.write("***  Satellite Name:             " + scName + "\n");
			if (branchName != "")
			{
				outputStreamWriter.write("***  Branch Name:                " + branchName + "\n");
			}
			outputStreamWriter.write("\n\n");
			
			outputStreamWriter.write("Rev Report Range:          [" + 
				TimeToString.AsLogTime(revRange.lowerEndpoint()) + " - " + 
				TimeToString.AsLogTime(revRange.upperEndpoint()) + "]\n");
			outputStreamWriter.write("Contacts Report Range:     [" + 
				TimeToString.AsLogTime(contactRange.lowerEndpoint()) + " - " + 
				TimeToString.AsLogTime(contactRange.upperEndpoint()) + "]\n");
			outputStreamWriter.write("Ingest Session Range:      [" + 
				TimeToString.AsLogTime(Ltimestamp) + " - " + 
				TimeToString.AsLogTime(Utimestamp) + "]\n\n");
			
			outputStreamWriter.write("Ingesting OASYS Rev report: " +
				"<a href=\"http://" + System.getProperty("mqIP") + ":8080/" + System.getProperty("web.path","mpssweb") + "/OrbitEvent/showOeRpt.jsp?filetype=rev&filename=" +
				revReportName + "\">" + revReportName + "</a> at " + 
				TimeToString.AsRptTime(currentTime) + "\n\n");
			
			outputStreamWriter.write("Ingesting OASYS Contacts report: " +
				"<a href=\"http://" + System.getProperty("mqIP") + ":8080/" + System.getProperty("web.path","mpssweb") + "/OrbitEvent/showOeRpt.jsp?filetype=contacts&filename=" +
				consReportName + "\">" + consReportName + "</a> at " + 
				TimeToString.AsRptTime(currentTime) + "\n\n");
			
			outputStreamWriter.close();
		}
		catch (Exception e){
			logger.info(e.getMessage()); 
		}
		
		//Store a Acqsumfile record
		Acqsumfile acqsum = new Acqsumfile();
		acqsum.setFilename(fileName);
		acqsum.setStarttime(Ltimestamp);
		acqsum.setStop(Utimestamp);
		acqsum.setStartrevid(revIdMap.get(revIdMap.firstKey()));
		acqsum.setEndrevid(revIdMap.get(revIdMap.lastKey()));
		if (!branchName.equals(""))
			acqsum.setBranch(branchName);
		
		@SuppressWarnings("unused")
		Acqsumfile newAcqsum = new Acqsumfile();
		try
		{
			newAcqsum = acqsumDao.create(acqsum);
		}
		catch (Exception e)
		{
			logger.info("create Acqsumfile failed "+ e.getMessage()); 
			newAcqsum = null;
		}		
	}
	
	@SuppressWarnings("unused")
	private boolean findContactHasExtent(Contact contact){			
			List<Extent> extents =extentDao.getByContactId(contact.getId().intValue());
			if(extents.size()>0){
				return true;
			}		
		return false;		
	}
}
