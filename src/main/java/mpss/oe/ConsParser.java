package mpss.oe;

import java.util.Scanner;
import java.util.Date;
import java.io.*;
import java.lang.reflect.*;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map;
import java.util.HashMap;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.util.intervaltree.*;

public class ConsParser 
{
	private Timestamp timestamp_f = Timestamp.valueOf("2100-01-01 00:00:00");
	private Timestamp timestamp_l = Timestamp.valueOf("2100-01-01 00:00:00");
	// context
	int scId;
	String scName;
	
	Scanner input;	
	
	String signature_;
	Date extracted;
	String title;
	String subtitle;
	String ephemeris;
	String spacecraft;
	String equinox;
	String attitude;
	
	SortedSet<EclipseEntry> earthMoonCache = new TreeSet<EclipseEntry>();
	SortedSet<EclipseEntry> earthSunCache = new TreeSet<EclipseEntry>();
	List<LatitudeEntry> latitudeCache = new ArrayList<LatitudeEntry>();
	SortedSet<EclipseRecord> eclipseList = new TreeSet<EclipseRecord>();
	SortedSet<LatitudeRecord> latitudeList = new TreeSet<LatitudeRecord>();
	List<String> consSiteError = new ArrayList<String>();
	
	Map<String, SortedSet<ContactEntry> > contactCache = new HashMap<String, SortedSet<ContactEntry> >();
	Map<String, SortedSet<ContactRecord> > contactListMap = new HashMap<String, SortedSet<ContactRecord> >();
	List<ContactRecord> contactList = new LinkedList<ContactRecord>();
	Set<String> siteNames = new HashSet<String>();
	List<String> sbandSites = new ArrayList<String>();
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	String[] kwEventType = new String[]{
	    "Entering Earth/Moon Penumbra",
	    "Entering Earth/Moon Umbra",
	    "Exiting  Earth/Moon Umbra",
	    "Exiting  Earth/Moon Penumbra",
	    "In       Earth/Moon Umbra",
	    "Entering  Earth/Sun Penumbra",
	    "Entering  Earth/Sun Umbra",
	    "Exiting   Earth/Sun Umbra",
	    "Exiting   Earth/Sun Penumbra",
	    "In        Earth/Sun Umbra",
        "Satellite in view (Rise)",
	    "Enter command window (CMDON)",
	    "Enter X-band DL window (XON)",
	    "Maximum Elevation     (ELM)",
	    "Exit X-band DL window (XOFF)",
	    "Exit command window (CMDOFF)",
	    "Satellite out of view (Fade)",
	    "Geodetic Latitude+75 Crossing",
	    "Geodetic Latitude-40 Crossing"
	};
	
	String[] eclipseFields = new String[]{
      "penumbraentrancetime",
  	  "umbraentrancetime",
  	  "umbraexittime",
  	  "penumbraexittime"
	};
	
	String[] latitudeFields = new String[]{
		      "latitude70time",
		  	  "latitude40time"		  	  
			};
	
	String[] contactFields = new String[]{
	  "rise",
  	  "cmdrise",
  	  "xbandrise",
  	  "maxeltime",
  	  "xbandfade",
  	  "cmdfade",
  	  "fade"
	};
	
	final int InEarthMoonUmbraTypeCodeOffset = 4;
	final int InEarthSunUmbraTypeCodeOffset = 9;
	final int ContactTypeCodeStart = 10;
	boolean hasRevolutionIntervals = false;
	IntervalTree<RevRecord> revolutionIntervals;
	
	boolean isInEarthMoonUmbra = false;
	boolean isInEarthSunUmbra = false;
	boolean isEclipseStart = true;
	
	public Timestamp getRangFirst()
	{
	    return this.timestamp_f;
	}
	
	public Timestamp getRangSecond()
	{
	    return this.timestamp_l;
	}
	
	public String getSignature()
	{
	    return this.signature_;
	}
	
	public Date getExtracted()
	{
		  return this.extracted;
	}
	
	public String geTitle()
	{
	    return this.title;
	}
	
	public String getSubtitle()
	{
	    return this.subtitle;
	}

	public String getEphemeris()
	{
	    return this.ephemeris;
	}

	public String getSpacecraft()
	{
	    return this.spacecraft;
	}

	public String getEquinox()
	{
	    return this.equinox;
	}

	public String getAttitude()
	{
	    return this.attitude;
	}

	public SortedSet<EclipseRecord> getEclipses()
	{
	    return this.eclipseList;
	}
	
	public SortedSet<LatitudeRecord> getLatitudes()
	{
	    return this.latitudeList;
	}
	
	public List<ContactRecord> getContacts()
	{
	    return this.contactList;
	}
	
	public Map<String, SortedSet<ContactRecord> > getContactListMap()
	{
	    return this.contactListMap;
	}
	
	public void setContactListMap(Map<String, SortedSet<ContactRecord> > contactListMap)
	{
		 this.contactListMap = contactListMap;
	}
	
	public void setRevolutionIntervals(IntervalTree<RevRecord> it)
	{
		 this.revolutionIntervals = it;
		 this.hasRevolutionIntervals = true;
	}
	
	public void setSiteNames(Set<String> name)
	{
		 this.siteNames = name;
	}
	
	public Set<String> getSiteNames()
	{
	    return this.siteNames;
	}
	
	
	public void setSbandSites(List<String> sbandSites)
	{
		 this.sbandSites = sbandSites;
	}
	
	public List<String> getSbandSites()
	{
	    return this.sbandSites;
	}
	
	public List<String> getConsSiteError()
	{
	    return this.consSiteError;
	}
	
	Date parsedAnTime = new Date();
	Date parsedDnTime = new Date();
	Date parsedRevEndTime = new Date();
	int  parsedOrbit;
	
	//public static void main(String args[])
	//{
	//	String inputFileName="F:\\Projects\\NSPO\\MCC\\05 MPSS\\10 References\\Interface Files\\In\\FDF\\ROCSAT2_056_061_cons.rpt";
	//	
	//	ConsParser parser = new ConsParser();
	//	parser.open(inputFileName);
	//	parser.parse();
	//	
	//	for (EclipseRecord er : parser.getEclipses())
	//	{
	//	    System.out.println(er.toString());
	//	}
	//	System.out.println("Total eclispes: " + parser.getEclipses().size());
	//	
	//	for (ContactRecord cr : parser.getContacts())
	//	{
	//	    System.out.println(cr.toString());
	//	}
	//	System.out.println("Total contacts: " + parser.getContacts().size());
	//}
	public ConsParser(int scId, String scName)
	{
		this.scId = scId;
		this.scName = scName;
	}
	
	public boolean open(String filepath)
	{
		try 
		{
		    File file = new File(filepath);
			this.input = new Scanner(file);
		} 
		catch (FileNotFoundException e) {
			logger.info("Exception: fail to open " + filepath + "(" + e.getMessage() + ")");
			return false;
	    }		
		return true;
	}
	
	public boolean parse()
	{
		 if (!this.hasRevolutionIntervals)
		 {
		     logger.info("Revolution intervals has not been set");
		     return false;
		 }
		 
		 if (!parseHeader())
		 {
		     return false;
		 }
		 
		 parseContacts();
		 
		 // close all resources
		 // will it close the file, too?
		 this.input.close();
		
	   return true;
	}

  private boolean startsWith(String str, String prefix, boolean ignoreCase) {
      if (str == null || prefix == null) {
          return (str == null && prefix == null);
      }
      if (prefix.length() > str.length()) {
          return false;
      }
      return str.regionMatches(ignoreCase, 0, prefix, 0, prefix.length());
  }
  
	private boolean parseHeader()
	{
     //
     //
     //Event Report
     while (this.input.hasNext())
     {
         String line = this.input.nextLine();
         line = line.trim();
         if (line.equals("Event Report"))
         {
             break;
         }
     }
     
     //
     //
     //Signature:  OASYS v4.3.6 (December 6, 2002), Integral Systems, Inc.
     while (this.input.hasNext())
     {
         String line = this.input.nextLine();
         line = line.trim();
         if (startsWith(line, "Signature:", true))
         {
         	   String tok[] = line.split(" +"); // split by one or more spaces
         	   if (tok.length < 2 && !tok[1].equals("OASYS"))
         	   {
         	   	   logger.info("Parse fails by unknown file signature (" + line + ")");
         	       return false;
         	   }
         	  else
        	   {
        		  signature_ = line.replace("Signature:  ", "");
        	   }
             break;
         }
     }
     
     //Extracted:  02/26/09 07:31:19
     //Title:  d:\mpss.ops\data\OASYS\temp\056_061_rev.adb
     //Subtitle:  
     //Ephemeris:  \mpss.ops\data\ROCSAT2\elements\RS2_20090224_god_15days.eph
     //Spacecraft:  [ROCSAT2] rocsat2
     //Equinox:  MEME of Epoch [JD2000]
     //Attitude:  AttitudeReference
		 String[] headerFields = new String[]{"Extracted", "Title", "Subtitle", "Ephemeris", "Spacecraft", "Equinox",  "Attitude"};
		 String[] headerMembers = new String[]{"extracted", "title", "subtitle", "ephemeris", "spacecraft", "equinox", "attitude"};
		 int[] headerTypes = new int[]{2,1,1,1,1,1,1};
     for (int lineno = 0; lineno < headerFields.length; lineno++)
     {
	    	try
	    	{
                if (this.input.hasNext())
                {
            	    Scanner lineTokenizer = new Scanner(this.input.nextLine());
            	    lineTokenizer.useDelimiter(":");
            	    String name = lineTokenizer.next();
            	    name = name.trim();
            	    if (!name.equals(headerFields[lineno]))
            	    {
            	    	logger.info("Field not found: " + name);
            	    	lineTokenizer.close();
            	        return false;
            	    }
            	
            	    if (lineTokenizer.hasNext())
            	    {
                	    String data = lineTokenizer.next();
    	            	// date and filepath format will conflict with delimiter
                	    while (lineTokenizer.hasNext())
                	    {
        	            	data = data.concat(":").concat(lineTokenizer.next());
                	    }
                	    data = data.trim();
                	    
                	    // use reflection to set class members
            	        Class<?>  clazz = this.getClass();
            	        Field field = clazz.getDeclaredField(headerMembers[lineno]);
            	        switch (headerTypes[lineno])
            	        {
            	            // string
            	            case 1:
            	            	data = data.trim();
            	                field.set(this, data);
            	                break;
            	            // date
            	            case 2:
            	    	        DateFormat formatter = new SimpleDateFormat("MM/dd/yy HH:mm:ss");
            	    	        Date date = (Date) formatter.parse(data);              	        
            	    	        field.set(this, date);
            	    	        //System.out.println(this.datetime);
              	                break;            	    	  
            	        }
            	    }
            	    lineTokenizer.close();
	    	    }
	    	}
        catch (Exception e)
	    	{
	    	    logger.info("Exception: " + e.getMessage());
	    	}
     }
     
	   return true;
	}

	private boolean parseContacts()
	{
     // skip unnecessary lines (including empty lines)
     String line = new String();
     while (this.input.hasNextLine())
     {
         line = this.input.nextLine();
         line = line.trim();
         if (line.length() > 0)
         {
             break;
         }
     }
     // 2009/02/24 23:09:17.243  Entering Earth/Moon Penumbra			    Occulted:      0.000000 %         
     // 2009/02/24 23:09:29.235  Entering Earth/Moon Umbra			    Occulted:      99.99998 %         
     // 2009/02/24 23:39:56.967  Exiting  Earth/Moon Umbra			    Duration:      30.46220 mins      
     // 2009/02/24 23:40:08.942  Exiting  Earth/Moon Penumbra			    Duration:      30.86165 mins      
     // 
     // 2009/02/24 23:10:35.020  Entering  Earth/Sun Penumbra			    Occulted:      0.000000 %         
     // 2009/02/24 23:10:48.518  Entering  Earth/Sun Umbra			    Occulted:      100.0000 %         
     // 2009/02/24 23:39:50.708  Exiting   Earth/Sun Umbra			    Duration:      29.03649 mins      
     // 2009/02/24 23:40:04.148  Exiting   Earth/Sun Penumbra			    Duration:      29.48547 mins      
     // 
     // 2009/02/24 23:40:33.984  Satellite in view (Rise)[x_svalbard (x_svalbard)]     Azimuth:      258.9390 degs      
     // 2009/02/24 23:40:56.015  Enter command window (CMDON)[x_svalbard (x_svalbard)]     Azimuth:      260.7302 degs      
     // 2009/02/24 23:42:22.504  Enter X-band DL window (XON)[x_svalbard (x_svalbard)]     Azimuth:      268.9443 degs      
     // 2009/02/24 23:47:17.495  Maximum Elevation     (ELM)[x_svalbard (x_svalbard)]   Elevation:      14.45925 degs      
     // 2009/02/24 23:52:12.960  Exit X-band DL window (XOFF)[x_svalbard (x_svalbard)]    Duration:      9.840948 mins      
     // 2009/02/24 23:53:39.733  Exit command window (CMDOFF)[x_svalbard (x_svalbard)]    Duration:      12.72864 mins      
     // 2009/02/24 23:54:01.844  Satellite out of view (Fade)[x_svalbard (x_svalbard)]    Duration:      13.46432 mins      
     //      
     if (!parseEvent(line))
     {
    	 //return false;
     }
     
     while (this.input.hasNextLine())
     {
//         parseEvent(this.input.nextLine());
    	 line = this.input.nextLine();
         line = line.trim();
         if (line.length() > 0)
         {
        	 parseEvent(line);
         }
     }
	   return true;
	}
	
  private boolean parseEvent(String text)
  {
  	  // extract timestamp
      String szTimestamp = text.substring(0,23);
      DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
      Date timestamp_ = new Date();
      try
      {
          timestamp_ = (Date) formatter.parse(szTimestamp);
          //Get the Time range bound from the contacts file
          if (timestamp_f.getTime() > timestamp_.getTime())
          {
        	  timestamp_f = new Timestamp(timestamp_.getTime());
        	  timestamp_l = new Timestamp(timestamp_.getTime());
          }
          if (timestamp_l.getTime() < timestamp_.getTime())
          {
        	  timestamp_l = new Timestamp(timestamp_.getTime());
          }
      }
      catch (Exception e)
      {
    	  logger.info("parseEvent date format Exception: (" + e.getMessage() + text + ")");
    	  return false;
      }
      
      // cut timestamp
      String eventText = text.substring(25);
      
      // match event type
      int typeCode = -1;
      for (int id = 0; id < kwEventType.length; id++)
      {
          if (startsWith(eventText, this.kwEventType[id], true))
          {
        	  // found
              typeCode = id;
              break;
          }
      }
      if (typeCode == -1)
      {
          logger.info("Unknown event type: " + eventText);
          return false;
      }
      
      // cut event type keyword
      int dataStart = kwEventType[typeCode].length();
      String eventData = eventText.substring(dataStart);
      eventData = eventData.trim();
      
      // typeCode >16 , for Geodetic Latitude+70 Crossing ,Geodetic Latitude-40 Crossing
      if(typeCode<=16){
    	// parse event data      
          parseEventData(timestamp_, typeCode, eventData, eventText);
      }else{
    	// parse event data   , for Geodetic Latitude+70 Crossing ,Geodetic Latitude-40 Crossing 
    	  parseLatitudeEventData(timestamp_, typeCode, eventData, eventText);
      }      
      
      logger.debug("timestamp:" + timestamp_ + ", typeCode:" + typeCode + ", eventData:" + eventData + ", eventText:" +eventText);
      return true;
  }
  
  // typeCode>16
    private boolean parseLatitudeEventData(Date timestamp_, int typeCode, String eventData, String eventText){
    	String[] tok = eventData.split("\\s+");
    	if (tok.length != 4)
		{
		    return false;
		}
    	
    	LatitudeEntry entry = new LatitudeEntry();		
		entry.datetime = timestamp_;		
		entry.paramName = tok[0]+" "+tok[1];
		entry.paramValue = Double.parseDouble(tok[2]);
		entry.paramUnit = tok[3];
		
		
		try
		{
			// Latitude 70 : penumbraentrancetime and umbraentrancetime , Latitude -40 : umbraexittime and penumbraexittime			
			if(!this.isEclipseStart){
				logger.info("Latitude 70/-40  not in Eclipse");
				this.latitudeCache.add(entry);				
			}else{
				logger.info("Latitude 70/-40 in Eclipse");
			}
			
		}
		catch (Exception e)
		{
			logger.info("Can not add Latitude 70/-40 entry (" + e.getMessage() + ")");
		}
		
		// cache full
		if (this.latitudeCache.size() == 2)
		{
			LatitudeRecord rec = makeEclipseRecordLatitude(this.latitudeCache);
		    // add to eclipse record list
		    if (rec != null)
		    	this.latitudeList.add(rec);
		    this.latitudeCache.removeAll(this.latitudeCache);
		}
		else if (this.isEclipseStart && this.latitudeCache.size() == 1)
		{
		    this.latitudeCache.removeAll(this.latitudeCache);		    
		}
		
    	return true;
    }

	private boolean parseEventData(Date timestamp_, int typeCode, String eventData, String eventText)
	{
		String[] tok = eventData.split("\\s+");
	  
		// eclipse
		if (typeCode <= InEarthSunUmbraTypeCodeOffset)
		{
			if (tok.length != 3)
			{
			    return false;
			}
			EclipseEntry entry = new EclipseEntry();
			entry.typeCode = typeCode;
			entry.datetime = timestamp_;
			entry.eclipseType = (typeCode < 5) ? "Earth/Moon" : "Earth/Sun";
			entry.paramName = tok[0];
			entry.paramValue = Double.parseDouble(tok[1]);
			entry.paramUnit = tok[2];
  	      
			// earth/moon
			if (typeCode <= InEarthMoonUmbraTypeCodeOffset)
			{
				if (typeCode == InEarthMoonUmbraTypeCodeOffset) {
					this.isInEarthMoonUmbra = true;
				}
				try
				{
					this.earthMoonCache.add(entry);
				}
				catch (Exception e)
				{
					logger.info("Can not add earth/moon entry (" + e.getMessage() + ")");
				}
				// cache full
				if (!this.isInEarthMoonUmbra && this.earthMoonCache.size() == 4) 
				{
					EclipseRecord rec = makeEclipseRecord("Earth/Moon", this.earthMoonCache);
					// add to eclipse record list
					if (rec != null)
						this.eclipseList.add(rec);        	      
					this.earthMoonCache.removeAll(this.earthMoonCache);
				}
				else if (this.isInEarthMoonUmbra && this.earthMoonCache.size() == 3)
				{
					// skip eclipse information
					this.earthMoonCache.removeAll(this.earthMoonCache);
				    this.isInEarthMoonUmbra = false;
				}
			}
			// earth/sun
			else
			{
				if (typeCode == InEarthSunUmbraTypeCodeOffset) {
					this.isInEarthSunUmbra = true;
				}
				try
				{
					this.earthSunCache.add(entry);
				}
				catch (Exception e)
				{
					logger.info("Can not add earth/sun entry (" + e.getMessage() + ")");
				}
				// cache full
				if (!this.isInEarthSunUmbra && this.earthSunCache.size() == 4)
				{
				    EclipseRecord rec = makeEclipseRecord("Earth/Sun", this.earthSunCache);
				    // add to eclipse record list
				    if (rec != null)
				    	this.eclipseList.add(rec);
				    this.earthSunCache.removeAll(this.earthSunCache);
				}
				else if (this.isInEarthSunUmbra && this.earthSunCache.size() == 3)
				{
				    this.earthSunCache.removeAll(this.earthSunCache);
				    this.isInEarthSunUmbra  = false;
				}
				
				// 20171116 by jeff for Latitude 70/-40
				// "Entering  Earth/Sun Penumbra" ==> isEclipseStart = true  , "Exit  Earth/Sun Penumbra" ==> isEclipseStart = false , for   Latitude 70/-40
				if (typeCode == 5) {
					this.isEclipseStart = true;
				}else if(typeCode == 8){
					this.isEclipseStart = false;
				}
				
			}
		}
		// contact
		else
		{
			if (tok.length != 5)
			{
			    return false;
			}

			ContactEntry entry = new ContactEntry();
			entry.typeCode = typeCode;
			entry.datetime = timestamp_;
			entry.site = tok[0].substring(1, tok[0].length());
			entry.paramName = tok[2];
			entry.paramValue = Double.parseDouble(tok[3]);
			entry.paramUnit = tok[4];

			if (siteNames.contains(entry.site.toLowerCase()))
			{
				// new site?
				if (this.contactCache.get(entry.site) == null)
				{
					this.contactCache.put(entry.site, new TreeSet<ContactEntry>());					
					logger.debug("new site: "+ entry.site);
				}
	  	      
				SortedSet<ContactEntry> cache = this.contactCache.get(entry.site);
				try
				{
				    cache.add(entry);
				}
				catch (Exception e)
				{
				    logger.info("Can not add contact entry (" + e.getMessage() + ")");
				}
				// cache full
				//if (cache.size() == 7)
				// fade occurs, cache ends
				if (entry.typeCode == 16)
				{
					try
					{
						ContactRecord rec = makeContactRecord(entry.site, cache);
						if (rec != null)
						{
							// add to contact record list
							this.contactList.add(rec);	
						}
						
						cache.removeAll(cache);
					}
					catch (Exception e)
					{
					    logger.info("Can not add ContactRecord (" + e.getMessage() + ")");
					}
				}
			}
			else
			{
				try
				{
					consSiteError.add(eventText);
				}
				catch (Exception e)
				{
				    logger.info("Can not add Error Site contact (" + entry.site + ")");
				}
			}
			
   	  }
      return true;
  }
  
  private EclipseRecord makeEclipseRecord(String eclipseType, SortedSet<EclipseEntry> cache)
  {
      EclipseRecord rec = new EclipseRecord();
      rec.scid = this.scId;
      rec.eclipseType = eclipseType;
      
	  // penumbraentrancetime
  	  // umbraentrancetime
  	  // umbraexittime
  	  // penumbraexittime
  	  int i = 0;
  	  for(EclipseEntry e :  cache) 
  	  {
          // use reflection to set class members
          Class<?>  clazz = rec.getClass();
          try
          {
            Field field = clazz.getDeclaredField(eclipseFields[i++]);
            field.set(rec, e.datetime);
          }
          catch (Exception ex)
          {
              logger.info("Exception: " + ex.getMessage());
              return null;
          }

  	  }

      // determine revno from penumbra entrance time
  	  List<RevRecord> res = this.revolutionIntervals.get(rec.penumbraentrancetime.getTime());
  	  if (res.size() > 0)
  	  {
  	      rec.revno = res.get(0).revno;
  	  }
  	  else
  		  return null;

  	  return rec;
  } 
  
  private LatitudeRecord makeEclipseRecordLatitude(List<LatitudeEntry> cache)
  {
	  LatitudeRecord rec = new LatitudeRecord();
      rec.scid = this.scId; 
      
	  
  	  int i = 0;
  	  for(LatitudeEntry e :  cache) 
  	  {
          // use reflection to set class members
          Class<?>  clazz = rec.getClass();
          try
          {
            Field field = clazz.getDeclaredField(latitudeFields[i++]);
            field.set(rec, e.datetime);
          }
          catch (Exception ex)
          {
              logger.info("Exception: " + ex.getMessage());
              return null;
          }

  	  }

      // determine revno from penumbra entrance time
  	  List<RevRecord> res = this.revolutionIntervals.get(rec.latitude40time.getTime());
  	  if (res.size() > 0)
  	  {
  	      rec.revno = res.get(0).revno;
  	  }
  	  else
  		  return null;

  	  return rec;
  }

  private ContactRecord makeContactRecord(String site, SortedSet<ContactEntry> cache)
  {
      ContactRecord rec = new ContactRecord();
       rec.scid = this.scId;
      rec.siteid = site;
      
	    // rise
  	  // cmdrise
  	  // xbandrise
  	  // maxeltime
  	  // xbandfade
  	  // cmdfade
  	  // fade
  	  for(ContactEntry e :  cache) 
  	  {
          // use reflection to set class members
          Class<?>  clazz = rec.getClass();
          try
          {
          	// event type code starts at 10 (offset)
          	int id = (int) e.typeCode - ContactTypeCodeStart;
            Field field = clazz.getDeclaredField(contactFields[id]);
            field.set(rec, e.datetime);
            if (id == 3)
            {
            	field = clazz.getDeclaredField("maxel");
                field.set(rec, (int) Math.round(e.paramValue));
            }
          }
          catch (Exception ex)
          {
              logger.info("makeContactRecord Exception: " + ex.getMessage());
              return null;
          }
          
  	  }

      // determine revno from Maximum Elevation time
  	  if (rec.rise == null){  		
  		logger.info("makeContactRecord Exception: " + site + "no Maximum Elevation time");
  		return null;
  	  }
  	  else
  	  {
  	  	  //List<RevRecord> res = this.revolutionIntervals.get(rec.rise.getTime());
  		  List<RevRecord> res = this.revolutionIntervals.get(rec.maxeltime.getTime());
  	  	  if (res.size() > 0)
  	  	  {
  	  	      rec.revno = res.get(0).revno;
  	  	  }
  	  	  else
  	  		return null;
  	  		  
  	  }
  	  return rec;
  } 

}