package mpss.oe;

import org.springframework.stereotype.Service;

// for Spring component scanning
@Service
public interface OrbitEventsLoader{	

	public void setScid(int scid);

	public void setBranchid(int branchid);
	
	public void setFileName(String fileName);
	
	public String getLogFileName();	
	
	public boolean Load(String revFileName, String consFileName);	
	
	public String getReturnMsg();
	
}
