package mpss.pg;


import mpss.util.timeformat.TimeRange;
import org.springframework.stereotype.Service;



@Service("scheduleProductSession")
public class ScheduleProductSession {

	private boolean mpqSelected;
	private boolean ttqSelected;
	private boolean passPlanSelected;
	private boolean integratedRptSelected;
	private boolean rasSelected;
	private boolean rasGroundSelected;
	private boolean isNominal;
	private int	branchId;
	private int SatelliteId;
	
	private TimeRange loadRange;
	private TimeRange productRange;
	private int prodDurHour;
	private int prodDurMinute;
	private int loadDurHour;
	private int loadDurMinute;
	
	private String productId;
	
	public void setIsNominal (boolean isNominal){
		this.isNominal = isNominal;
	}
	
	public boolean getIsNominal (){
		return this.isNominal; 
	}
	
	public void setMPQSelected (boolean mpqSelected){
		this.mpqSelected = mpqSelected;
	}
	
	public boolean getMPQSelected (){
		return this.mpqSelected; 
	}
	
	
	public void setTTQSelected (boolean ttqSelected){
		this.ttqSelected = ttqSelected;
	}
	
	public boolean getTTQSelected (){
		return this.ttqSelected; 
	}
	
	public void setPassPlanSelected (boolean passPlanSelected){
		this.passPlanSelected = passPlanSelected;
	}
	
	public boolean getPassPlanSelected (){
		return this.passPlanSelected; 
	}
	
	public void setIntegratedRptSelected (boolean integratedRptSelected){
		this.integratedRptSelected = integratedRptSelected;
	}
	
	public boolean getIntegratedRpt (){
		return this.integratedRptSelected; 
	}
	
	public void setRasSelected (boolean rasSelected){
		this.rasSelected = rasSelected;
	}
	
	public boolean getRasSelected (){
		return this.rasSelected; 
	}
	
	public void setRasGroundSelected (boolean rasGroundSelected){
		this.rasGroundSelected = rasGroundSelected;
	}
	
	public boolean getRasGroundSelected (){
		return this.rasGroundSelected; 
	}	
	
	public void setBranchId (int branchId){
		this.branchId = branchId;
	}
	
	public int getBranchId (){
		return this.branchId; 
	}	
	
	public void setProductId (String productId){
		this.productId = productId;
	}

	public String getProductId (){
		return this.productId; 
	}	

	public void setSatelliteId (int SatelliteId){
		this.SatelliteId = SatelliteId;
	}
	
	public int getSatelliteId (){
		return this.SatelliteId; 
	}	
	
	public void setLoadTimeRange (TimeRange loadRange){
		this.loadRange = loadRange;
	}
	
	public TimeRange getLoadTimeRange (){
		return this.loadRange; 
	}	
	
	public void setProductTimeRange (TimeRange productRange){
		this.productRange = productRange;
	}
	
	public TimeRange getProductTimeRange (){
		return this.productRange; 
	}
	
	public void setProdDurHour (int prodDurHour){
		this.prodDurHour = prodDurHour;
	}
	
	public int getProdDurHour (){
		return this.prodDurHour; 
	}
	
	public void setProdDurMinute (int prodDurMinute){
		this.prodDurMinute = prodDurMinute;
	}
	
	public int getProdDurMinute (){
		return this.prodDurMinute; 
	}
	
	public void setLoadDurHour (int loadDurHour){
		this.loadDurHour = loadDurHour;
	}
	
	public int getLoadDurHour (){
		return this.loadDurHour; 
	}
	
	public void setLoadDurMinute (int loadDurMinute){
		this.loadDurMinute = loadDurMinute;
	}
	
	public int getLoadDurMinute (){
		return this.loadDurMinute; 
	}
}
