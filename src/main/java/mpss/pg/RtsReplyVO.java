package mpss.pg;

import mpss.util.timeformat.DateUtil;

public class RtsReplyVO {
	
	//2017-09-11T00:25:49Z,2017-09-11T00:33:51Z,satellite,SG6,ACCEPTED,S,YES
	
	private String start_time;
	private String end_time;
	private String satellite_name;
	private String antenna;
	private String status;
	private String bands;
	private String uplink;
	
	
    public RtsReplyVO() {	
		
	}
	
    public RtsReplyVO(String data) {	
    	String[] tempdatas = data.split(",");		
		this.setStart_time(tempdatas[0]);
		this.setEnd_time(tempdatas[1]);
		this.setSatellite_name(tempdatas[2]);
		this.setAntenna(tempdatas[3]);
		this.setStatus(tempdatas[4]);
		this.setBands(tempdatas[5]);
		this.setUplink(tempdatas[6]);		
	}
	
	
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String start_time) {		
		start_time=start_time.replace("T", "");
		start_time =start_time.replace("Z", "");
		this.start_time = DateUtil.dateToString(DateUtil.stringToDate(start_time,"yyyy-MM-ddHH:mm:ss"),"yyyy-DDD-HH:mm:ss");
	}
	
	public String getEnd_time() {
		return end_time;
	}
	public void setEnd_time(String end_time) {	
		end_time=end_time.replace("T", "");
		end_time=end_time.replace("Z", "");
		this.end_time = DateUtil.dateToString(DateUtil.stringToDate(end_time,"yyyy-MM-ddHH:mm:ss"),"yyyy-DDD-HH:mm:ss");
	}
	
	public String getSatellite_name() {
		return satellite_name;
	}
	public void setSatellite_name(String satellite_name) {
		this.satellite_name = satellite_name;
	}
	
	public String getAntenna() {
		return antenna;
	}
	public void setAntenna(String antenna) {
		this.antenna = antenna;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBands() {
		return bands;
	}
	public void setBands(String bands) {
		this.bands = bands;
	}
	public String getUplink() {
		return uplink;
	}
	public void setUplink(String uplink) {
		this.uplink = uplink;
	}
	
	

}
