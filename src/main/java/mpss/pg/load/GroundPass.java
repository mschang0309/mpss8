package mpss.pg.load;

import mpss.pg.EventExpander;
import mpss.pg.EventTemplateLine;
import mpss.pg.ProductFile;
import mpss.pg.product.ScheduleProductBuilder;
import mpss.util.timeformat.RangeUtil;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeToString;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.*;

import org.springframework.stereotype.Component;


@Component("groundPass")
public class GroundPass extends ProductFile { 

	public GroundPass() {
		super();
	}

	public boolean Load (ScheduleProductBuilder builder)
	{
		try
		{
			/// calculate the extended stop and extended start times
			TimeRange range = new TimeRange(builder.getProductSession().getProductTimeRange().first,builder.getProductSession().getProductTimeRange().second);
			range = RangeUtil.rangeAppendSec(range,-1200,1200);
			
			super.Load(builder,ProductType.GroundSchedule);
					
			/// create an event expander with our schedule and satellite
			EventExpander eventExpander = new EventExpander(builder);
					
			/// expand retrieved events
			List<EventTemplateLine> eventTemplateLines = eventExpander.expand(builder.getScheduledEvents(), "ALL");
			if(eventTemplateLines.size()>0)
			{
				/// trim the schedule to the desired time period
				eventTemplateLines = removeLastSessionEvents(eventTemplateLines,range);
				eventTemplateLines = removePostSessionEvents(eventTemplateLines,range);
			}
		    
		    super.setEventLines(eventTemplateLines);

		}
		catch (Exception e) {
	        System.err.println(e.getMessage());
	        return false;
	    }
		
		genProduct();
		genReport();
		return true;
	}
	
	private void genProduct()
	{
		File file = new File(productName);
        if(!file.exists())
		{
		    try {
		        file.createNewFile();
		    } catch (Exception e) {
		        e.printStackTrace();
		    }
		}
        
		try 
		{	
		    FileOutputStream fileOutputStream = new FileOutputStream(productName);
	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
	
	        List<EventTemplateLine> eventTemplateLines = new ArrayList<EventTemplateLine>();
	
			/// for each micro event in the schedule
			for (EventTemplateLine etl : etls)
			{
				/// get a copy of the micro-event to cut up
				String outputText = etl.getMicEventText().trim();
	
				// If is not ACQ add the events to the ground pass init
				if (etl.getScheduledEvent().getEventMnemonic().equalsIgnoreCase("ACQ_S") ||
					etl.getScheduledEvent().getEventMnemonic().equalsIgnoreCase("ACQ_E"))
				{
					if (!outputText.substring(0, 1).equalsIgnoreCase("#"))
					{
						// if is a command make sure the source is RLT for both cases
						if ((etl.getMicIsCommand() && etl.getMicCmdSource().equalsIgnoreCase("RLT")) || 
							!etl.getMicIsCommand())
						{
							if (outputText.indexOf("GOTO") != -1)
							{
								outputStreamWriter.write("     " + String.format("%-85s", outputText) + "\n");
								// also add it to my temp micro event used for generating the report
								eventTemplateLines.add(etl);
							}
							else if ((outputText.indexOf(" THEN") != -1) ||
								(outputText.indexOf("ENDIF") != -1) ||
								(outputText.indexOf("SKIP") != -1))
							{
								outputStreamWriter.write(String.format("%-85s", outputText) + "\n");
								// also add it to my temp micro event used for generating the report
								eventTemplateLines.add(etl);
							}
							else
							{
								/// strip off leading whitespace
								outputStreamWriter.write(etl.getExpandedText() + "\n");

								// also add it to my temp micro event used for generating the report
								eventTemplateLines.add(etl);
							}
						}			 
					}
				}
			}
			outputStreamWriter.close();
			etls = eventTemplateLines;
	    } catch (Exception e) {
	    	logger.info(e.getMessage());
	    }
	}
	
	private void genReport()
	{
		/// open the Report file for output
		File file = new File(reportName);
        if(!file.exists())
		{
		    try {
		        file.createNewFile();
		    } catch (Exception e) {
		        e.printStackTrace();
		    }
		}
		try 
		{
			FileOutputStream fileOutputStream = new FileOutputStream(reportName);
	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);

			// create the report header
			outputStreamWriter.write(" *******************************************************************************\n");
			outputStreamWriter.write("# Report Name: " + loadFile + ".rpt\n");
			outputStreamWriter.write("# Report Time: " + TimeToString.AsLogTime(schProdBuilder.getProductSession().getProductTimeRange().first) + 
				"-" + TimeToString.AsLogTime(schProdBuilder.getProductSession().getProductTimeRange().second) + "\n");
			outputStreamWriter.write("# Branch Name: ");
			if (!schProdBuilder.getBranchTag().equalsIgnoreCase(""))
				outputStreamWriter.write( schProdBuilder.getProductSessionName() + "\n\n");
			else
				outputStreamWriter.write("\n");
	
			outputStreamWriter.write("# *******************************************************************************\n"); 
	
			int i = 0;
			for (EventTemplateLine etl : etls)
			{
				i++;
				/// get a copy of the micro-event to cut up
				String outputText = etl.getMicEventText().trim();
	
				/// write the line number to the output file
				outputStreamWriter.write( String.format("%04d", i) + " ");
								
				outputStreamWriter.write( TimeToString.AsLogTime(etl.getEventTriggerTag().getTriggerTime()) +
					 "." + String.format("%04d", etl.getEventTriggerTag().getTriggerTime().getNanos()) + " " + outputText +
					 etl.getMicEventComment() + "\n");

			}
			outputStreamWriter.close();
	    } catch (Exception e) {
	        System.err.println(e.getMessage());
	    }
	}
	
	/// method to remove events that occur past the end
	private List<EventTemplateLine> removePostSessionEvents(List<EventTemplateLine> eventTemplateLines,TimeRange tr)
	{

		int start = -1;						/// Index of the first micro-event in vector
		int stop  = eventTemplateLines.size();		/// Index of the last session micro-event
		int lastACQEindex = 0;					/// Index of the last ACQ_E related event found
		boolean stopFound     = false;			/// Start of Daily Schedule flag
		boolean acqInProgress = false;			/// Flag that indicates whether we are in pass or not
		boolean endingInPass = false;			/// Flag that indicates if this schedule ends in the
											/// middle of a pass

		/// while we have not found where the current schedule should end
		while (!stopFound)
		{
			/// Increment the current session event stop index 
			++start;						

			/// if we are hit the end of the vector
			if (start == stop)
			{
				/// it's the end of the schedule
				stopFound = true;			
			}

			/// else if this micro-event is associated with a macro-event
			else if (eventTemplateLines.get(start).getMicHasEvent())
			{
				/// if the macro-event is an acquisition start event
				if (eventTemplateLines.get(start).getScheduledEvent().getEventMnemonic().equalsIgnoreCase("ACQ_S"))
				{
					/// if the acquisition rise is after the schedule end
					if (eventTemplateLines.get(start).getScheduledEvent().getContact().getRise().after(tr.second))
					{
						/// it should be part of the next schedule
						stopFound = true;			
					}

					/// else the acquisition starts in this schedule
					else
					{
						/// set the acquisition in progress flag
						acqInProgress = true;
					}
				}

				/// else if the macro-event is an acquisition end event
				else if (eventTemplateLines.get(start).getScheduledEvent().getEventMnemonic().equalsIgnoreCase("ACQ_E"))
				{
					/// clear the acquisition in progress flag and save the index
					/// index just in case this is the last ACQ_E event in the schedule
					acqInProgress = false;
					lastACQEindex = start;

					/// if this pass is straddeling the schedule end
					if (eventTemplateLines.get(start).getScheduledEvent().getContact().getFade().after(tr.second))
					{
						/// set the flag to indicate that the schedule ends during a pass
						endingInPass = true;
					}
				}

				/// else if we are not in a pass, this schedule is not ending in the middle of
				/// a pass, and we have a macro-event that occurs after the schedule end
				else if ((!acqInProgress) &&
					     (!endingInPass) &&
					     (eventTemplateLines.get(start).getScheduledEvent().getEventStartTime().after(tr.second)))
				{
					/// it should be part of the next schedule
					stopFound = true;
				}
			}

			/// else if we are not in a pass, this schedule is not ending in the middle of a pass,
			/// and the micro-event time if after the schedule end
			else if ((!acqInProgress) &&
				     (!endingInPass) &&
					 (eventTemplateLines.get(start).getEventTriggerTag().getTriggerTime().after(tr.second)))
			{
				/// it should be part of the next schedule
				stopFound = true;		
			}
		}

		/// if we looked into the next schedule
		if (endingInPass) start = lastACQEindex + 1;

		/// delete all micro-events that don't belong in the current schedule
		int i =0;
		for( Iterator<EventTemplateLine> iter = eventTemplateLines.iterator(); iter.hasNext();)
		{
			iter.next();
			if (i >= start && i< stop)
			{
		        iter.remove();
		    }
			i++;
		}
		return eventTemplateLines;
	}
	
	/// method to remove events that occur prior to start
	private List<EventTemplateLine> removeLastSessionEvents(List<EventTemplateLine> eventTemplateLines,TimeRange tr)
	{
		int start = 0;					/// Index of the first micro-event in vector
		int stop  = -1;					/// Index of the last session micro-event
		int lastACQEindex = -1;			/// Index of the last ACQ_E related event found
		boolean startFound    = false;		/// Start of Daily Schedule flag
		boolean acqInProgress = false;		/// Flag that indicates whether we are in pass or not
		boolean startingInPass = false;	/// Flag that indicates if the current schedule
										/// starts during a pass

		/// while we have not found the start of the schedule
		while (!startFound)
		{
			/// Increment the current schedule start index
			++stop;

			/// if we reached the end of the vector and no events, just get out of here
			if (stop == eventTemplateLines.size()) break;	

			/// if this micro-event is associated with a macro-event
			if (eventTemplateLines.get(stop).getMicHasEvent())
			{
				/// if the macro-event is an acquisition start event
				if (eventTemplateLines.get(stop).getScheduledEvent().getEventMnemonic().equalsIgnoreCase("ACQ_S"))
				{
					/// if the acquisition starts after the schedule start
					if (eventTemplateLines.get(stop).getScheduledEvent().getContact().getRise().after(tr.first))
					{
						/// we found our start
						startFound = true;			
					}

					/// else if the contact straddels the schedule start
					else if ((eventTemplateLines.get(stop).getScheduledEvent().getContact().getRise().before(tr.first)) &&
							 (eventTemplateLines.get(stop).getScheduledEvent().getContact().getFade().after(tr.first)))
					{
						/// set the flag to indicate we are in pass, but don't mark this
						/// as the start because this pass was in the prior schedule
						acqInProgress = true;
						startingInPass = true;
					}
				}

				/// else if the macro-event is an acquisition end event
				else if (eventTemplateLines.get(stop).getScheduledEvent().getEventMnemonic().equalsIgnoreCase("ACQ_E"))
				{
					/// set the flag to indicate we are not in pass
					acqInProgress = false;

					/// save the index just in case this is the
					/// last ACQ_E event from the prior schedule
					lastACQEindex = stop;
				}

				/// else if we are not in acquisition, we are not starting in a pass, and
				/// we have a macro event that occurs after the schedule start
				else if ((!acqInProgress) &&
					     (!startingInPass) &&
						 (!eventTemplateLines.get(stop).getScheduledEvent().getEventStartTime().before(tr.first)))
				{
					/// we found our start
					startFound = true;		
				}
			}

			/// else if we are not in acquisition, we are not starting in a pass, and the
			/// micro-event time is after the schedule start time
			else if ((!acqInProgress) &&
				     (!startingInPass) &&
					 (!eventTemplateLines.get(stop).getEventTriggerTag().getTriggerTime().before(tr.first)))
			{
				/// we found our start
				startFound = true;		
			}
		}

		/// if the schedule started during a pass
		if (startingInPass)
		{
			/// if we never found the next pass
			if (!startFound)
			{
				/// if we found the ACQ_E event
				if (lastACQEindex != -1)
				{
					/// indicate that we found our start
					startFound = true;

					/// set our start to the last ACQ_E micro-event + 1
					stop = lastACQEindex + 1;
				}
			}

			/// else we found the next pass
			else
			{
				/// set our start to the last ACQ_E micro-event + 1
				stop = lastACQEindex + 1;
			}
		}

		/// if we found our start
		if (startFound)
		{
			/// delete all micro events that don't belong in the current schedule
			int i =0;
			for( Iterator<EventTemplateLine> iter = eventTemplateLines.iterator(); iter.hasNext();)
			{
				iter.next();
				if (i >= start && i< stop)
				{
			        iter.remove();
			    }
				i++;
			}
		}
		return eventTemplateLines;
	}
	
}
