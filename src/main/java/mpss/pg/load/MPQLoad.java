package mpss.pg.load;

import mpss.common.jpa.Rsirpt;
import mpss.pg.*;
import mpss.pg.product.ScheduleProductBuilder;
import mpss.se.*;
import mpss.util.files.FileInfo;
import mpss.util.timeformat.TimeToString;
import mpss.util.timeformat.TimeUtil;
import mpss.util.timeformat.YearDayTimeOffset;
import nspo.xpsoc.LoadEncoder.ScheduleData;


import nspo.xpsoc.LoadEncoder.MPQEncoder.MpqEncoder_FS2_FS5;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.*;

import org.springframework.stereotype.Component;


@Component("mpqLoad")
public class MPQLoad extends ProductFile {

	int seqCount;
	private Map<Integer, List<ScheduledEvent> > macroSeqMap = new HashMap<Integer, List<ScheduledEvent> >();
	private Map<Integer, List<EventTemplateLine> > microSeqMap = new HashMap<Integer, List<EventTemplateLine> >();
	
	private MpqEncoder_FS2_FS5 mpqencoder;	
	
	public MPQLoad (){
		super();
		try{
			mpqencoder =new MpqEncoder_FS2_FS5(mpqPath,CmdDataPath);
		}						   
		catch(Exception e)
		{
			logger.info(e.getMessage());
		}
	}
	
	public boolean Load (ScheduleProductBuilder builder,String MPQType)
	{
		logger.info("MPQLoad Load "+MPQType);
		try
		{
			if(!super.Load(builder,"MPQ",MPQType))
				return false;

		    /// expand the macro-events into micro-events for our source
			/// create an event expander with our schedule and satellite
			EventExpander eventExpander = new EventExpander(builder);
			etls = eventExpander.expand(filteredEvents, super.getCmdSource());   

			/// initialize number of map entries to zero
			seqCount = 0;
			Set<ScheduledEvent> curMacros = new HashSet<ScheduledEvent>();
			Set<EventTemplateLine> curMicros = new HashSet<EventTemplateLine>();
			Timestamp seqEndTime = null;
			Timestamp seqStartTime, macroStart, macroEnd;

		    boolean firstSeq = true;
		    for(EventTemplateLine etl : etls)
			{
		        // determine the start of the macroEvent.  Must take into consideration the setup time
		        macroStart = TimeUtil.timeAppendSec(etl.getScheduledEvent().getEventStartTime(), - etl.getScheduledEvent().getSetupDuration());
		        
		        if(firstSeq)
		        {
		            firstSeq = false;

					//seqStartTime = macroStart;
		            seqStartTime =new Timestamp(macroStart.getTime());
					seqEndTime = TimeUtil.timeAppendSec(seqStartTime, etl.getScheduledEvent().getExtentDuration());
		        }

		        if(!macroStart.before(seqEndTime))
		        {
		        	seqCount++;
		        	sortMap(curMacros,curMicros);

		        	curMacros = new HashSet<ScheduledEvent>();
					curMicros = new HashSet<EventTemplateLine>();

					//seqStartTime = macroStart;
					seqStartTime =new Timestamp(macroStart.getTime());
					seqEndTime = TimeUtil.timeAppendSec(seqStartTime, etl.getScheduledEvent().getExtentDuration());
		        }

		    
			    curMicros.add(etl);
			    if (!curMacros.contains(etl.getScheduledEvent()))
			    	curMacros.add(etl.getScheduledEvent());
		    
			    macroEnd = TimeUtil.timeAppendSec(macroStart, etl.getScheduledEvent().getExtentDuration());
		        if(macroEnd.after(seqEndTime))
		            //seqEndTime = macroEnd;
		        	seqEndTime = new Timestamp(macroEnd.getTime());
		    }

		    // close and validate the last sequence
		    if(etls.size()!=0){
		    	seqCount++;
		    }
		    
		    sortMap(curMacros,curMicros);
		    
		}
		catch (Exception e){
			logger.info("MPQLoad Load "+MPQType+ ","+e.getMessage());
			builder.setReturnMsg("MPQ Load failed : " + e.getMessage());
			return false;
		}
		
		// 20170515 by jeff
		if(seqCount>256){
			logger.info("MPQ Load failed : sequence > 256 ");
			builder.setReturnMsg("MPQ Load failed : sequence > 256 ");
			return false;
		}
		
		genReport();
		genProduct();
		genStatistic(MPQType);
		
		// 20170525 check file size by jeff  , rsi(PAYLOAD) 40000 bytes , aip(AIP) 20000 bytes
		if(MPQType.equalsIgnoreCase("PAYLOAD")){
			FileInfo info = new FileInfo(new File(productName));
			if(info.getSize()>40000){
				builder.setReturnMsg("MPQ Load failed : file size is too large ");
				logger.info("MPQ PAYLOAD Load failed : file size is too large ");
				return false;
			}
			
		}else if(MPQType.equalsIgnoreCase("AIP")){
			FileInfo info = new FileInfo(new File(productName));
			if(info.getSize()>20000){
				builder.setReturnMsg("MPQ Load failed : file size is too large ");
				logger.info("MPQ AIP Load failed : file size is too large ");
				return false;
			}
		}
		
		return true;
	}
	
	private void genReport()
	{
		/// get the current time
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		
		File file = new File(reportName);
		if(!file.exists())
		{
		    try {
		        file.createNewFile();
		    } catch (Exception e) {
		        e.printStackTrace();
		    }
		}
		try
		{
	        FileOutputStream fileOutputStream = new FileOutputStream(file);
	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
	        
	        /// write the current time to the report file as the load file generation time
			outputStreamWriter.write("***  Generating a " + loadSubType + " Load File on " +
			String.format("%tD",currentTime) + String.format(" %tT",currentTime) + " (" +
			String.format("%tY/",currentTime) + String.format("%tj",currentTime) + ")\n\n");
	        
		
			/// write the report file name to the report file
			outputStreamWriter.write("***  Report File Name\t\t\t" + reportName + "\n");

			/// write the load file name to the report file
			outputStreamWriter.write("***  Product File Name\t\t\t" + productName + "\n");
			
			/// write the load start time to the report file
			outputStreamWriter.write("***  Product Start Date\t\t\t" + 
			String.format("%tY/",schProdBuilder.getProductSession().getProductTimeRange().first) + 
			String.format("%tj",schProdBuilder.getProductSession().getProductTimeRange().first) + "\n");

			/// write the load file end time to the report file
			outputStreamWriter.write("***  Product End Date\t\t\t" +
			String.format("%tY/",schProdBuilder.getProductSession().getProductTimeRange().second) + 
			String.format("%tj",schProdBuilder.getProductSession().getProductTimeRange().second) + "\n");

			/// write the load file duration to the report file
			outputStreamWriter.write("***  Product Duration (in days)\t\t" +((schProdBuilder.getProductSession().getProdDurHour() * 60 + schProdBuilder.getProductSession().getProdDurMinute())*60/ 86400.0) + "\n\n\n");
			
			/// if no sequences were found
			if (seqCount == 0)
			{
				/// issue a warning to the report file
				outputStreamWriter.write("**** WARNING: No " + loadSubType+ " commands in selected time range. Load File *NOT* Generated\n");

				/// delete the load file
				//remove(myProductName.c_str());
			}
			else
			{
				/// write the number of MPQ sequences to the report file
				outputStreamWriter.write("***  Number of MPQ Sequences Generated\t" + seqCount + "\n\n");

				for (int it = 1; it <= seqCount; it++)
				{
					/// write the macro sequence header
					outputStreamWriter.write("Macro events for sequence " + it + ":" + "\n");

					/// write out all macro-events in the sequence
					for (ScheduledEvent s : macroSeqMap.get(it))
					{
						outputStreamWriter.write(TimeToString.AsRptTime(s.getEventStartTime()) + "  "+ satName + "  ");
						if (s.getContact() != null)
						{
							outputStreamWriter.write(String.format("%06d", s.getRev().getRevno()) + "  "+ String.format("%-12s", s.getSite().getName()) + "  ");
						}
						else
						{
							outputStreamWriter.write("                      ");
						}
						//TimeRange tr = new TimeRange(s.getExtent().getStarttime(),s.getExtent().getEndtime());
						outputStreamWriter.write(TimeToString.AsTime(YearDayTimeOffset.toTimestamp(0, 0, s.getDuration())) + "  ");
						outputStreamWriter.write(String.format("%-14s", s.getEventMnemonic()) + "  "+ s.getEventParms() + "\n");
					}

					/// write the micro sequence header
					outputStreamWriter.write("\nMicro events (" + microSeqMap.get(it).size()+ ") for sequence " + it + ":" + "\n");

					/// write out all macro-events in the sequence
					for (EventTemplateLine t : microSeqMap.get(it))
					{
						outputStreamWriter.write(TimeToString.AsLogTime(t.getEventTriggerTag().getTriggerTime()) +
						String.format(".%tL", t.getEventTriggerTag().getTriggerTime()) + " #  " + t.getExpandedText().substring(13) + "\n");
					}

					/// write a sequence separator to the file
					outputStreamWriter.write("\n****" + "\n\n");
				}
			}      
	        
		   
	       outputStreamWriter.close();
	       fileOutputStream.close();
	       
	       
	     //check MPQ Load 
	     //LoadInspecter(etls);
	       checkLoadseq();
		}
		catch (Exception e){
			logger.info(e.getMessage());
		}
	}
	
	private void genProduct()
	{
		ScheduleData scheduleData = new ScheduleData();
		scheduleData.setFileName(super.getProductName());
		scheduleData.setRocsatName(satName);
		Date onBoardTime = null;

		try
		{
			for (int it = 1; it <= seqCount; it++)
			{
				for (EventTemplateLine etl : microSeqMap.get(it))
				{
					//AIP command change
					String loadCmd = "";
					String cmd = "";					
			
					loadCmd = etl.getLoadComment();
					cmd = etl.getLoadComment();
					
					String loadText= etl.getLoadText();
					loadText = loadText.substring(0, loadText.indexOf("SRC")).trim();					
					onBoardTime = new Date(etl.getEventTriggerTag().getTriggerTime().getTime());					
					scheduleData.addEventItem(cmd, loadCmd + " " + loadText,onBoardTime,it);
					logger.info("Seq:" + it + ", Comment:" + cmd + ", Text:"+ etl.getLoadText()+ ", Time:" + onBoardTime);
				}
			}
			
			scheduleData.setFileName(productName);
			scheduleData.setRocsatName(cmdSatName);
			if (scheduleData.getAllEventItem().size() >0){
				mpqencoder.createMPQLoad(scheduleData);
			}			
			
		}
		catch (Exception e){
			logger.info("MPQLoad genProduct ex="+e.getMessage());
			setErrorCount();
			setErrorMsg(e.getMessage());
		}
	}
	
	private void genStatistic(String MPQType)
	{
		Rsirpt rsirpt = schProdBuilder.getRsirpt();
		int rsi = 0;
		int pbk = 0;
		int ddt = 0;
		int del = 0;
		
		
		try
		{
			if (MPQType.equalsIgnoreCase("AOCS"))
			{
				//aocs
				rsirpt.setAocs(seqCount);
				//man
				rsirpt.setMan(seqCount);
			}
			else if(MPQType.equalsIgnoreCase("PAYLOAD"))
			{
				System.out.println("genStatistic type="+MPQType);
				//pdm
				rsirpt.setPdm(seqCount);
				for (int it = 1; it <= seqCount; it++)
				{
					for (ScheduledEvent s : macroSeqMap.get(it))
					{
						if (s.getEventMnemonic().indexOf("RSI_REC")>=0)
							rsi++;
						if (s.getEventMnemonic().indexOf("RSI_DDT")>=0)
							ddt++;
					}
					for (EventTemplateLine etl : microSeqMap.get(it))
					{
						if(etl.getLoadText().indexOf("DICDELFILE")>=0)
							del++;
						if(etl.getLoadText().indexOf("DICFILEDUMPSTART")>=0)
							pbk++;
					}
				}
				//rsi
				rsirpt.setRsi(rsi);
				//ddt
				rsirpt.setDdt(ddt);	
				//rsidel
				rsirpt.setRsidel(del);
				//pbk
				rsirpt.setPbk(pbk);	
			}else if(MPQType.equalsIgnoreCase("AIP")){
				// AIP
				rsirpt.setAip(seqCount);
				
			}else{
				logger.info("genStatistic type="+MPQType +", no this type");
				logger.info("no rsi rpt");				
			}
			
			
		}
		catch (Exception e){
			logger.info(e.getMessage());
		}	
	}
	
	private void sortMap(Set<ScheduledEvent> macroEvents,Set<EventTemplateLine> microEvents)
	{
	    List<EventTemplateLine> sortMicros = new ArrayList<EventTemplateLine>(microEvents);
	   	Collections.sort(sortMicros,
	   		new Comparator<EventTemplateLine>() 
    		{
    			public int compare(EventTemplateLine et1, EventTemplateLine et2) 
    			{
    				return et1.getEventTriggerTag().getTriggerTime().compareTo(et2.getEventTriggerTag().getTriggerTime());
    			}
    		}
	    );
	   	
	   	List<ScheduledEvent> sortMacros = new ArrayList<ScheduledEvent>(macroEvents);
	   	Collections.sort(sortMacros,
	   		new Comparator<ScheduledEvent>() 
    		{
    			public int compare(ScheduledEvent se1, ScheduledEvent se2) 
    			{
    				return se1.getExtent().getStarttime().compareTo(se2.getExtent().getStarttime());
    			}
    		}
	    );
    	microSeqMap.put(seqCount, sortMicros);
    	macroSeqMap.put(seqCount, sortMacros);	
	}
	
	private void checkLoadseq()
	{
		for (int it = 1; it <= seqCount; it++)
		{

			for (EventTemplateLine t : microSeqMap.get(it))
			{
				if (!macroSeqMap.get(it).contains(t.getScheduledEvent()))
				{
					setWarningCount();
					logger.info("sequence warning  -- time:" + t.getEventTriggerTag().getTriggerTime() + "  message:" + t.getExpandedText());
					setWarningMsg("sequence warning   " + TimeToString.AsLogTime(t.getEventTriggerTag().getTriggerTime()) +
					String.format(".%tL", t.getEventTriggerTag().getTriggerTime()) + " #  " + t.getExpandedText().substring(13));
				}
				
			}
		}
		
	}
	
}
