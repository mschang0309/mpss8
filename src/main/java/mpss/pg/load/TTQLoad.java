package mpss.pg.load;


import mpss.common.jpa.Rsirpt;
import mpss.pg.EventExpander;
import mpss.pg.EventTemplateLine;
import mpss.pg.ProductFile;
import mpss.pg.product.ScheduleProductBuilder;
import mpss.util.timeformat.TimeToString;
import nspo.xpsoc.LoadEncoder.ScheduleData;
import nspo.xpsoc.LoadEncoder.TTQEncoder.TtqEncoder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.*;

import org.springframework.stereotype.Component;


@Component("ttqLoad")
public class TTQLoad extends ProductFile { 

	private TtqEncoder ttqencoder;
	
	public TTQLoad() {
		super();
		try{
			ttqencoder = new TtqEncoder(CmdDataPath);
		}						   
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public boolean Load (ScheduleProductBuilder builder)
	{
		try
		{
			if(!super.Load(builder,"TTQ",""))
				return false;
			
		    /// expand the macro-events into micro-events for our source
			/// create an event expander with our schedule and satellite
			EventExpander eventExpander = new EventExpander(builder);
			super.setEventLines(eventExpander.expand(filteredEvents, super.getCmdSource()));
		    
		}
		catch (Exception e){
			logger.info(e.getMessage());
			builder.setReturnMsg("TTQ Load failed : " + e.getMessage());
			return false;
		}
		
		genReport();
		genProduct();
		genStatistic();
		return true;
	}
	
	private void genReport()
	{
		/// get the current time
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		
		File file = new File(reportName);
		if(!file.exists())
		{
		    try {
		        file.createNewFile();
		    } catch (Exception e) {
		        e.printStackTrace();
		    }
		}
		try
		{
	        FileOutputStream fileOutputStream = new FileOutputStream(file);
	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
	        
	        /// write the current time to the report file as the load file generation time
			outputStreamWriter.write("***  Generating a TTQ Load File on " +
			String.format("%tD",currentTime) + String.format(" %tT",currentTime) + " (" +
			String.format("%tY/",currentTime) + String.format("%tj",currentTime) + ")\n\n");
	        
		
			/// write the report file name to the report file
			outputStreamWriter.write("***  Report File Name\t\t\t" + reportName + "\n");

			/// write the load file name to the report file
			outputStreamWriter.write("***  Product File Name\t\t\t" + productName + "\n");
			
			/// write the load start time to the report file
			outputStreamWriter.write("***  Product Start Date\t\t\t" + 
			String.format("%tY/",schProdBuilder.getProductSession().getProductTimeRange().first) + 
			String.format("%tj",schProdBuilder.getProductSession().getProductTimeRange().first) + "\n");

			/// write the load file end time to the report file
			outputStreamWriter.write("***  Product End Date\t\t\t" +
			String.format("%tY/",schProdBuilder.getProductSession().getProductTimeRange().second) + 
			String.format("%tj",schProdBuilder.getProductSession().getProductTimeRange().second) + "\n");

			/// write the load file duration to the report file
			outputStreamWriter.write("***  Product Duration (in days)\t\t" +((schProdBuilder.getProductSession().getProdDurHour() * 60 + schProdBuilder.getProductSession().getProdDurMinute())*60/ 86400.0) + "\n\n\n");
			
				for (EventTemplateLine etl : etls)
				{ 
					outputStreamWriter.write(TimeToString.AsLogTime(etl.getEventTriggerTag().getTriggerTime()) +
					String.format(".%tL", etl.getEventTriggerTag().getTriggerTime()) + " #  " + etl.getExpandedText().substring(13) + "\n");
				}
				
			//}	        
	        
		   		
	       outputStreamWriter.close();
	       fileOutputStream.close();
	       
	       //check TTQ Load 
	       //LoadInspecter(etls);
		}
		catch (Exception e){
			logger.info(e.getMessage());
		}
	}
	
	private void genProduct()
	{
		ScheduleData scheduleData = new ScheduleData();
		scheduleData.setFileName(super.getProductName());
		scheduleData.setRocsatName(satName);
		Date onBoardTime = null;
		
		try
		{
			for (EventTemplateLine etl: etls)
			{
			onBoardTime = new Date(etl.getEventTriggerTag().getTriggerTime().getTime());
			scheduleData.addEventItem(etl.getLoadComment(), etl.getLoadComment() + " " + etl.getLoadText(),onBoardTime,null);	
			logger.debug("Comment:" + etl.getLoadComment() + ", Text:"+ etl.getLoadText()+ ", Time:" + onBoardTime);
			}
			ttqencoder.createTTQLoad(reportName, cmdSatName, productName);
			
		}
		catch (Exception e){
			logger.info(e.getMessage());
			setErrorCount();
			setErrorMsg(e.getMessage());
		}	

	}
	
	private void genStatistic()
	{
		Rsirpt rsirpt = schProdBuilder.getRsirpt();
		int txOn = 0;
		int txOff = 0;
		int goHome = 0;
		
		try
		{
			for (EventTemplateLine etl: etls)
			{
				if(etl.getLoadComment().indexOf("CSTXDEFON")>=0)
					txOn++;
				if(etl.getLoadComment().indexOf("CSTXOFF")>=0)
					txOff++;
				if(etl.getLoadComment().indexOf("CNMGOHOME")>=0)
					goHome++;
			}
			//ttq
			rsirpt.setTtq(etls.size());
			//tx
			if (txOn != txOff)
				rsirpt.setTx(-1);
			else
				rsirpt.setTx(txOn);
			//gohome
			rsirpt.setGohome(goHome);	
			
		}
		catch (Exception e){
			logger.info(e.getMessage());
		}	
	}
}
