package mpss.pg;


import mpss.pg.product.ScheduleProductBuilder;
import mpss.util.timeformat.RangeUtil;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeToString;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.*;

import org.springframework.stereotype.Component;

@Component("dailySchedule")
public class DailySchedule extends ProductFile { 
	
	public DailySchedule() {
		super();
	}

	public boolean Load (ScheduleProductBuilder builder)
	{
		try
		{
			/// calculate the extended stop and extended start times
			TimeRange range = new TimeRange(builder.getProductSession().getProductTimeRange().first,
				builder.getProductSession().getProductTimeRange().second);
			range = RangeUtil.rangeAppendSec(range,-1200,1200);
			
			super.Load(builder,ProductType.Schedule);
					
			/// create an event expander with our schedule and satellite
			EventExpander eventExpander = new EventExpander(builder);
					
			/// trim the schedule to the desired time period
			List<EventTemplateLine> eventTemplateLines = new ArrayList<EventTemplateLine>();
			
		    for (EventTemplateLine etl: eventExpander.expand(builder.getScheduledEvents(), "ALL"))
			{
				if (range.contains(etl.getScheduledEvent().getEventStartTime()))
				{
					eventTemplateLines.add(etl);
				}
			}
		    
		    super.setEventLines(eventTemplateLines);

//			for (EventTemplateLine etl:eventTemplateLines)
//			{
//				//if (etl.getMicCmdSource().equalsIgnoreCase("RLT"))
//				if (!etl.getMicEventText().substring(0, 1).equalsIgnoreCase("#")){
//					System.out.println("MicEventText : " + etl.getMicEventText());
//					System.out.println("Mnemonic : " + etl.getScheduledEvent().getEventMnemonic() + "; MicIsCommand : " + etl.getMicIsCommand() + "; MicCmdSource : " + etl.getMicCmdSource());
//				}
//			}
		}
		catch (Exception e) {
	        System.err.println("DailySchedule Load() ex="+e.getMessage());
	        return false;
	    }
		
		genProduct();
		genReport();
		return true;
	}
	
	private void genProduct()
	{
		File file = new File(productName);
        if(!file.exists())
		{
		    try {
		        file.createNewFile();
		    } catch (Exception e) {
		        e.printStackTrace();
		    }
		}
        
		try {	
		    FileOutputStream fileOutputStream = new FileOutputStream(productName);
	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
	
	        List<EventTemplateLine> eventTemplateLines = new ArrayList<EventTemplateLine>();
	        
			/// for each micro event in the schedule
			for (EventTemplateLine etl : etls)
			{
				
				/// get a copy of the micro-event to cut up
				String outputText = etl.getMicEventText().trim();
	
				// If is not ACQ add the events to the ground pass init
				if (!etl.getScheduledEvent().getEventMnemonic().equalsIgnoreCase("ACQ_S") &&
					!etl.getScheduledEvent().getEventMnemonic().equalsIgnoreCase("ACQ_E"))
				{
					if (outputText.length()!=0 && !outputText.substring(0, 1).equalsIgnoreCase("#"))
					{
						// if is a command make sure the source is RLT for both cases
						if ((etl.getMicIsCommand() && etl.getMicCmdSource().equalsIgnoreCase("RLT")) || 
							!etl.getMicIsCommand())
						{
							/// strip off leading whitespace
							outputStreamWriter.write(etl.getExpandedText() + "\n");
	
							// also add it to my temp micro event used for generating the report
							eventTemplateLines.add(etl);
						}			 
					}
					
				}
				else if (outputText.length()!=0 && !outputText.substring(0, 1).equalsIgnoreCase("#"))
				{
					// if is a command make sure the source is RLT for both cases
					if ((etl.getMicIsCommand() && etl.getMicCmdSource().equalsIgnoreCase("RLT")) || 
						!etl.getMicIsCommand())
					{
						if (outputText.indexOf("GOTO") != -1)
						{
							outputStreamWriter.write("     " + String.format("%-85s", outputText) + "\n");
							// also add it to my temp micro event used for generating the report
							eventTemplateLines.add(etl);
						}
						else if ((outputText.indexOf(" THEN") != -1) ||
							(outputText.indexOf("ENDIF") != -1) ||
							(outputText.indexOf("SKIP") != -1))
						{
							outputStreamWriter.write(String.format("%-85s", outputText) + "\n");
							// also add it to my temp micro event used for generating the report
							eventTemplateLines.add(etl);
						}
					}
				}
				
			}
			outputStreamWriter.close();
			etls = eventTemplateLines;
	    } catch (Exception e) {
	    	System.err.println("DailySchedule genProduct() ex="+e.getMessage());
	    }
	}
	
	private void genReport()
	{
		/// open the Report file for output
		File file = new File(reportName);
        if(!file.exists())
		{
		    try {
		        file.createNewFile();
		    } catch (Exception e) {
		        e.printStackTrace();
		    }
		}
		try 
		{
			FileOutputStream fileOutputStream = new FileOutputStream(reportName);
	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);

			// create the report header
			outputStreamWriter.write(" *******************************************************************************\n");
			outputStreamWriter.write("# Report Name: " + loadFile + ".rpt\n");
			outputStreamWriter.write("# Report Time: " + TimeToString.AsLogTime(schProdBuilder.getProductSession().getProductTimeRange().first) + 
				"-" + TimeToString.AsLogTime(schProdBuilder.getProductSession().getProductTimeRange().second) + "\n");
			outputStreamWriter.write("# Branch Name: ");
			if (!schProdBuilder.getBranchTag().equalsIgnoreCase(""))
				outputStreamWriter.write( schProdBuilder.getProductSessionName() + "\n\n");
			else
				outputStreamWriter.write("\n");
	
			outputStreamWriter.write("# *******************************************************************************\n"); 
	
			int i = 0;
			for (EventTemplateLine etl : etls)
			{
				i++;
				/// get a copy of the micro-event to cut up
				String outputText = etl.getMicEventText().trim();
	
				/// write the line number to the output file
				outputStreamWriter.write( String.format("%04d", i) + " ");
								
				outputStreamWriter.write( TimeToString.AsLogTime(etl.getEventTriggerTag().getTriggerTime()) +
					 "." + String.format("%04d", etl.getEventTriggerTag().getTriggerTime().getNanos()) + " " + outputText +
					 etl.getMicEventComment() + "\n");

			}
			outputStreamWriter.close();
	    } catch (Exception e) {
	        System.err.println("DailySchedule genReport() ex="+e.getMessage());
	    }
	}

}
