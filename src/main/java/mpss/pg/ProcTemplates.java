package mpss.pg;

import java.io.*;
import java.util.*;

public class ProcTemplates
{
	
	private String procPath = "";
	List<EventTemplateLine> procEvents = new ArrayList<EventTemplateLine>();
  
	public ProcTemplates(String procPath)
	{
	    this.procPath = procPath;
	    //load();
	}
	
	// load proc templates
	public void load()
	{
		try
		{			
			// open the schedule proc for input
			FileInputStream procFile = new FileInputStream(this.procPath);
			// Get the object of DataInputStream
        	DataInputStream in = new DataInputStream(procFile);
        	BufferedReader br = new BufferedReader(new InputStreamReader(in));
        	
    		/// set a flag indicating we haven't read from the file yet
    		boolean firstProcEvent = true;
        	String inputLine;
        	// Read File Line By Line
        	int eventSequence = 0;
        	//List<EventTemplateLine> templateLines = new ArrayList<EventTemplateLine>();
        	while ((inputLine = br.readLine()) != null)   
			{
        		// Print the content on the console
				System.out.println (inputLine);
				if (inputLine.indexOf('#') == 0)
				{
					continue;
				}
				
				//EventTemplateLine etl = new EventTemplateLine(inputLine);
				EventTemplateLine etl = new EventTemplateLine();
				etl.setFullLineText(inputLine);
				etl.parse(false);
				if (etl.getSkipped() == true)
				{
					continue;
				}
				etl.setMicEventSequence(eventSequence++);
				
				// if this is the first useable line we have read see if it is
    			// marked with a time relative to the prior micro-event
    			if (firstProcEvent)
    			{
    				if (inputLine.indexOf('^') > -1 && inputLine.indexOf('^') < 15)
    				{
    					inputLine.replace("^", " ");
    					// set flag to indicate that we have read a useable line
    					firstProcEvent = false;
    				}
    			}

    			//this.procEvents = new ArrayList<EventTemplateLine>();
    			this.procEvents.add(etl);
			}
			if (this.procEvents == null)
			{
				System.err.println("Source Proc Events File is Empty");
			}
        	//Close the input stream
			in.close();
		}
		catch (Exception e) {//Catch exception if any
			System.err.println("ProcTemplates load Error: " + e.getMessage());
		}
	}
	
	public List<EventTemplateLine> getProcEvents()
	{
	    return this.procEvents;
	}

}
