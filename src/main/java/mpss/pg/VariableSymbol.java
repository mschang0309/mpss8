package mpss.pg;

public class VariableSymbol
{
		private String symbolName;
		private int symbolType;
		private int startPos;
		private int endPos;
		
		public String getSymbolName()
		{
		    return this.symbolName;
		}
		
		public void setSymbolName(String symbolName)
		{
		    this.symbolName = symbolName;
		}
		
		public int getSymbolType()
		{
		    return this.symbolType;
		}
		
		public void setSymbolType(int symbolType)
		{
		    this.symbolType = symbolType;
		}
		
		public int getStartPos()
		{
		    return this.startPos;
		}
		
		public void setStartPos(int startPos)
		{
		    this.startPos = startPos;
		}
		
		public int getEndPos()
		{
		    return this.endPos;
		}
		
		public void setEndPos(int endPos)
		{
		    this.endPos = endPos;
		}
		
		/// ctor
		public VariableSymbol(String symbolName, int startPos, int endPos)
		{
		    this.symbolName = symbolName;
		    this.startPos = startPos;
		    this.endPos = endPos;
		}

}

