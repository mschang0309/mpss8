package mpss.pg;

import java.io.*;
import java.util.*;

public class EventTemplates
{
	
	private String templateDir = "";
  
	private Map<String, List<EventTemplateLine>> templateBuffer = new HashMap<String, List<EventTemplateLine>>();
  
	public EventTemplates(String templateDir)
	{
	    this.templateDir = templateDir;
	}
	
	// load event templates
	public void load()
	{
		// load event template files (extension .etf) in designated directory
		File folder = new File(this.templateDir);
		File[] listOfFiles = folder.listFiles(); 
		for (int i = 0; i < listOfFiles.length; i++) 
		{
			if (listOfFiles[i].isFile()) 
			{
				String name = listOfFiles[i].getName();
				if (name.endsWith(".etf") || name.endsWith(".ETF"))
				{
					//System.out.println(name);
                    // extract event name from event template file name
	                String eventName = name.substring(0, name.lastIndexOf('.'));
	                // full path
	                String path = templateDir + "/" + name;
	                try
	                {
	                	// Open the file 
	                	FileInputStream fstream = new FileInputStream(path);
                        // Get the object of DataInputStream
	                	DataInputStream in = new DataInputStream(fstream);
	                	BufferedReader br = new BufferedReader(new InputStreamReader(in));
	                	String inputLine;
                        //Read File Line By Line
	                	int eventSequence = 0;
	                	while ((inputLine = br.readLine()) != null)   {
                            // Print the content on the console
	                		//System.out.println (inputLine);
                            //EventTemplateLine etl = new EventTemplateLine(inputLine);
	                		EventTemplateLine etl = new EventTemplateLine();
	                		etl.setFullLineText(inputLine);
                            etl.parse(true);
                            if (etl.getSkipped() == true)
                            {
                                continue;
                            }
                            etl.setMicEventSequence(eventSequence++);
                            List<EventTemplateLine> templateLines = this.templateBuffer.get(eventName);
                            if (templateLines == null)
                            {
                            	templateLines = new ArrayList<EventTemplateLine>();
                            }
                            // append event template line
                            templateLines.add(etl);
                            // update event template dictionary
                            this.templateBuffer.put(eventName, templateLines);
	                	}
                        //Close the input stream
	                	in.close();
	                }
	                catch (Exception e) {//Catch exception if any
                    System.err.println("EventTemplates load Error 1: " + e.getMessage());
	                }
				}
			}
		}	
	}	

  // load event templates
	public List<EventTemplateLine> getTemplate(String eventName)
	{
	    return this.templateBuffer.get(eventName);
	}

	
}
