package mpss.pg;

import java.sql.Timestamp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;

public class EventTriggerTag implements Cloneable 
{
	/// flag indicating if tag is valid
	private boolean triggerTagValid;
	
	/// relative time in seconds (including milliseconds)
	//private double deltaTime;
	private int deltaHour;
	private int deltaMinute;
	private int deltaSecond;
	private int deltaMilliSec;

	
	/// time trigger type
	private String deltaType;
	
	/// trigger time resolved
	private Timestamp triggerTime;
	/// time of prior micro-event
	private Timestamp previousTime;

    // Character classes in regex
    // [abc]	a, b, or c (simple class)
    // [^abc]	Any character except a, b, or c (negation)
    // [a-zA-Z]	a through z or A through Z, inclusive (range)
    // [a-d[m-p]]	a through d, or m through p: [a-dm-p] (union)
    // [a-z&&[def]]	d, e, or f (intersection)
    // [a-z&&[^bc]]	a through z, except for b and c: [ad-z] (subtraction)
    // [a-z&&[^m-p]]	a through z, and not m through p: [a-lq-z](subtraction)		
	private Pattern tagPattern = Pattern.compile("[^0123456789:.+-~@^]");
	private Pattern numPattern = Pattern.compile("[^0-9]");

	/// constructor to instantiate an event time trigger tag - parses time
	/// trigger tag from given event string and dervies relative value
	public EventTriggerTag(String triggeredEvent)
	{
		/// assume the trigger tag is valid
		this.triggerTagValid = true;
			
		/// find the first white-space
		int stopPos = triggeredEvent.indexOf(" ");

		/// parse out the time trigger
		String triggerTag = triggeredEvent.substring(0, stopPos);

		/// check for any non-standard characters in the trigger tag
		Matcher matcher = tagPattern.matcher(triggerTag);
            
		/// trigger tag is invalid
		if (matcher.find())
        {
           	triggerTagValid = false;
        }
		/// trigger tag is valid
        else
        {
        	ParseTimeTag(triggerTag);
        }
	}
		
	/// method to parse a time trigger in hh:mm:ss.mmm format
	public void ParseTimeTag(String triggerTag)
	{	
		
		/// a time tag is not to be preceeded with ANY other character
		Matcher matcher = numPattern.matcher(triggerTag.substring(0, 1));
		if (!matcher.find())
		{
			/// convert hours, minutes, and seconds to integers
			this.deltaHour = Integer.parseInt(triggerTag.substring(0,2));
			this.deltaMinute = Integer.parseInt(triggerTag.substring(3,5));
			this.deltaSecond = Integer.parseInt(triggerTag.substring(6,8));
			this.deltaMilliSec = Integer.parseInt(triggerTag.substring(9,12));

			/// validate time values assuming a 2-day limit on time-tags
			if ((deltaHour >= 0)   && (deltaHour < 49)   && (deltaMinute >= 0) && 
				(deltaMinute < 60) && (deltaSecond >= 0) && (deltaSecond < 60) &&
				(deltaMilliSec >= 0)   && (deltaMilliSec <= 999))
			{
				/// convert time to total number of seconds
				//this.deltaTime = (hours * 60 * 60) + (minutes * 60) + seconds + ((double)milli/1000.0);

				/// determine what type of trigger this is
				if (triggerTag.substring(12).equals("^")) 
				{
					/// tags followed with a ^ are relative to the prior micro event
					this.deltaType = "Relative";
				}
				else if (triggerTag.substring(12).equals("-"))
				{
					/// tags followed with a - are that much prior to the macro event
					this.deltaType = "Negative";
				}
				else if (triggerTag.substring(12).equals("@"))
				{
					/// los time ,add time
					this.deltaType = "Los";
				}
				else if (triggerTag.substring(12).equals("~"))
				{
					/// los time , - time
					this.deltaType = "Los1";
				}
				else
				{
					/// tags not followed by anything are that much after the macro event
					this.deltaType = "Normal";
				}
			}

			/// else the trigger time tag is invalid
			else
			{
				this.triggerTagValid = false;
			}

		}
		
	}
	
	
	/// operator resolve time trigger using the macro event time
	public EventTriggerTag Parse(Timestamp eventTime,Timestamp previousTime,Timestamp LosTime)
	{       
//		if (previousTime == null)
//			previousTime = eventTime;
		//System.out.println("previousTimeTag-1:" + previousTime );
		/// if time tag invalid
		if (!this.triggerTagValid)
		{
			/// set trigger time to event time
			this.triggerTime = eventTime;
		}

		/// else if time tag is relative to prior micro event
		else if (deltaType.equals("Relative"))
		{
			/// add prior micro event time to relative time
			this.triggerTime = TimeOperator(previousTime, deltaHour, deltaMinute, deltaSecond, deltaMilliSec);
		}

		/// else if time tag is negative type
		else if (deltaType.equals("Negative"))
		{
			/// subtract relative time from macro event time
			this.triggerTime = TimeOperator(eventTime, (- deltaHour), (- deltaMinute), (- deltaSecond), (- deltaMilliSec));
		}
		else if (deltaType.equals("Los"))
		{
			/// Los add time
			if(LosTime==null){
				System.out.println("EventTriggerTag getLostime is null,use @");
			}
			this.triggerTime = TimeOperator(LosTime, deltaHour, deltaMinute, deltaSecond, deltaMilliSec);
		}
		else if (deltaType.equals("Los1"))
		{
			/// Los - time	
			if(LosTime==null){
				System.out.println("EventTriggerTag getLostime is null,use ~");
			}
			this.triggerTime = TimeOperator(LosTime,  (- deltaHour), (- deltaMinute), (- deltaSecond), (- deltaMilliSec));
		}
		/// else if time tag is normal
		else
		{
			/// add relative time to macro event time
			this.triggerTime = TimeOperator(eventTime, deltaHour, deltaMinute, deltaSecond, deltaMilliSec);
		}

		/// current time tag becomes the prior time tag
		this.previousTime = this.triggerTime;
		
		
		/// return resolved time
		//System.out.println("previousTimeTag-2:" + this.previousTime );
		//System.out.println("eventTime:" + eventTime );
		//System.out.println("triggerTime:" + this.triggerTime );
		return this;
	
	}
	
	public Timestamp getTriggerTime()
	{
		return this.triggerTime;
	}
	
	public Timestamp getPreviousTime()
	{
		return this.previousTime;
	}
	
	public void setTriggerTime(Timestamp triggerTime)
	{
		this.triggerTime= triggerTime;
	}
	
	private Timestamp TimeOperator(Timestamp targetTime, int hours, int minutes, int secs, int millis)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(targetTime.getTime());
		
		cal.add(Calendar.HOUR_OF_DAY, hours);
		cal.add(Calendar.MINUTE, minutes);
		cal.add(Calendar.SECOND, secs);
		cal.add(Calendar.MILLISECOND, millis);
		
		return  new Timestamp(cal.getTimeInMillis());
		
	}
	
	public Object clone() throws CloneNotSupportedException 
	{
		EventTriggerTag o = (EventTriggerTag)super.clone();

		return o;
	}
}

