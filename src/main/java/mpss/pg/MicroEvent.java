package mpss.pg;

import mpss.se.*;

public class MicroEvent
{
		/// command as defined by database
		//private CmdDirective cmdDirective;

		/// pointer to macro-event this micro-event was expanded from
		private ScheduledEvent macroEvent;

		/// time trigger
		//private EventTriggerTag eventTrigger;

		/// micro-event string
		private String microEventName;

		/// text (command or directive) section of micro-event
		private String micEventText;

		/// comment section of micro-event
		private String micEventComment;

		/// current character index in symbol search
		private int symbolIndex;

		/// flag indicating if the micro-event has been parsed
		private boolean micEventParsed;

		/// flag indicating if the micro-event is selected for inclusion
		/// of generated products
		private boolean micSelected;

		/// flag indicating if the micro-event is command
		private boolean micIsCommand;

		/// command source
		private String micCmdSource;

		/// flag indicating if the micro-event was expanded from a macro-event
		private boolean micHasEvent;

		/// flag indicating if the micro-event is valid
		private boolean micIsValid;

		/// the micro-events position relative to other micro-events
		private int micEventSequence;
		
		// properties
		public ScheduledEvent getMacroEvent()
		{
			return this.macroEvent;
		}
		
		public void setMacroEvent(ScheduledEvent macroEvent)
		{
		    this.macroEvent = macroEvent;
		}
		
		public String getMicroEventName()
		{
			return this.microEventName;
		}
		
		public void setMicroEventName(String microEventName)
		{
			this.microEventName = microEventName;
		}
		
		public String getMicEventText()
		{
			return this.micEventText;
		}
		
		public void setMicEventText(String micEventText)
		{
			this.micEventText= micEventText;
		}
		
		public String getMicEventComment()
		{
			return this.micEventComment;
		}
		
		public void setMicEventComment(String micEventComment)
		{
			this.micEventComment = micEventComment;
		}

		public int getSymbolIndex()
		{
			return this.symbolIndex;
		}
		
		public void setSymbolIndex(int symbolIndex)
		{
			this.symbolIndex = symbolIndex;
		}

		public boolean getMicEventParsed()
		{
			return this.micEventParsed;
		}
		
		public void setMicEventParsed(boolean micEventParsed)
		{
			this.micEventParsed = micEventParsed;
		}

		public boolean getMicSelected()
		{
			return this.micSelected;
		}
		
		public void setMicSelected(boolean micSelected0)
		{
			this.micSelected = micSelected0;
		}

		public boolean getMicIsCommand()
		{
			return this.micIsCommand;
		}
		
		public void setMicIsCommand(boolean micIsCommand)
		{
			this.micIsCommand = micIsCommand;
		}

		public String getMicCmdSource()
		{
			return this.micCmdSource;
		}
		
		public void setMicCmdSource(String micCmdSource)
		{
			this.micCmdSource = micCmdSource;
		}

		public boolean getMicHasEvent()
		{
			return this.micHasEvent;
		}
		
		public void setMicHasEvent(boolean micHasEvent)
		{
			this.micHasEvent = micHasEvent;
		}

		public boolean getMicIsValid()
		{
			return this.micIsValid;
		}
		
		public void setMicIsValid(boolean micIsValid)
		{
			this.micIsValid = micIsValid;
		}

		public int getMicEventSequence()
		{
			return this.micEventSequence;
		}
		
		public void setMicEventSequence(int micEventSequence)
		{
			this.micEventSequence = micEventSequence;
		}
}

