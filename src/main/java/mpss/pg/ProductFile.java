package mpss.pg;

import java.io.File;
import java.util.*;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.pg.product.ScheduleProductBuilder;
import mpss.se.ScheduledEvent;
import mpss.util.inspect.inspecter.Inspecter;
import mpss.util.timeformat.TimeToString;

public class ProductFile {
	
	protected Logger logger = Logger.getLogger(configFactory.getLogName());
	protected ScheduleProductBuilder schProdBuilder;
	protected List<EventTemplateLine> etls;
	
	protected enum ProductType{Load, Schedule, GroundSchedule, Memory};

	/// count of errors encountered
	private int errorCount;
	private List<String> errorMsgs = new ArrayList<String>();
	
	/// count of warnings encountered
	private int warningCount;
	private List<String> warningMsgs = new ArrayList<String>();
	
	/// command source of product (used as filter)
	private String cmdSource;
	
	/// load subtype (set to TTQ for TTQ loads, AOCS or PDM for MPQ loads)
	protected String loadSubType;
	
	/// full pathname of the product being generated
	protected String  productName;

	/// full pathname of the product report file being generated
	protected String reportName;

	/// type of product being generated
	@SuppressWarnings("unused")
	private ProductType productType;

	protected String loadFile;
	private String loadPath;

	protected String satName;
	protected String cmdSatName; //ROCSAT2,ROCSAT5
	protected List<ScheduledEvent> filteredEvents;
	
	//protected String uplinkePath;// = javae.AppDomain.getPath("uplinke2.cfg", new String[]{"conf"});
	protected String mpqPath;// = javae.AppDomain.getPath("command5.cfg", new String[]{"conf"});
	protected String CmdDataPath = System.getProperty("dataFileDir") + System.getProperty("cmdDataPath");
	
	public ProductFile(){
		satName = System.getProperty("satName");
		mpqPath = javae.AppDomain.getPath(configFactory.getCmdcfg(), new String[]{"conf",configFactory.getSatelliteName()});
		cmdSatName = configFactory.getRocsatName();		
	}
	
	public void Load(ScheduleProductBuilder builder,ProductType pType)
	{
		this.schProdBuilder = builder;
		this.etls = new ArrayList<EventTemplateLine>();
		
		this.warningCount = 0;
		this.errorCount = 0;

		String prodIdentifier = "";
		loadPath = builder.getFileDir() + builder.getSatellite().getName() + "\\" + "Resolved_Acquisition_Schedules" + "\\";

		/// if we are creating a daily schedule
		if (pType == ProductType.Schedule)
			prodIdentifier = "RASR_";
		/// if we are creating a ground daily schedule
		else if (pType ==ProductType.GroundSchedule)
			prodIdentifier = "GP_Init_";
		
		/// build the product and report file names
		loadFile = prodIdentifier  + builder.getSatellite().getOps4number().toUpperCase() +
				"_" + TimeToString.AsNameTime(builder.getProductSession().getLoadTimeRange().first) +
				"_" + TimeToString.AsNameTime(builder.getProductSession().getLoadTimeRange().second) +
				builder.getBranchTag();
		
		/// build the load file and load file report name
		productName = loadPath + loadFile + ".prc";
		reportName  = loadPath + loadFile + ".rpt";
		
		/// move the report file to *.bak if it already exists
		File reportFile = new File(reportName);
		if(reportFile.exists())
		{
			/// delete back file and move the combined file to .bak if it already exists
			String backupReportName = reportName + ".bak";
			File backupReport = new File(backupReportName);
	  		if (backupReport.exists())
	  			backupReport.delete();
	 
	  		reportFile.renameTo(new File(backupReportName));
		}
		
		/// move the product file to *.bak if it already exists
		File productFile = new File(productName);
		if(productFile.exists())
		{
			/// delete back file and move the combined file to .bak if it already exists
			String backupProductName = productName + ".bak";
			File backupProduct = new File(backupProductName);
	  		if (backupProduct.exists())
	  			backupProduct.delete();
	  		
	  		productFile.renameTo(new File(backupProductName));
	  		
		}
	}
	
	public boolean Load(ScheduleProductBuilder builder, String loadType,String MPQType)
	{			
		this.schProdBuilder = builder;
		this.etls = new ArrayList<EventTemplateLine>();
		
		this.warningCount = 0;
		this.errorCount = 0;
				
		/// indicate that this is a load product
		this.productType = ProductType.Load;

		/// get the command source
		cmdSource = loadType;

		/// if MPQ load, get subtype, otherwise set it equal to cmdSource
		if (loadType.equalsIgnoreCase("MPQ"))
		{
			if(MPQType.equalsIgnoreCase("AIP"))
		    	loadSubType = "AIP";
			else if(MPQType.equalsIgnoreCase("PAYLOAD"))
		    	loadSubType = "PDM";
			else
				loadSubType = "AOCS";
				
		}
		else
		{
			loadSubType = cmdSource;
		}

	    // make all characters lowercase 
	    String loadIdentifier = loadSubType.toLowerCase();

		/// retrieve the load files directory from the config file
		String configValue = cmdSource + "s";
		loadPath = builder.getFileDir() + builder.getSatellite().getName() + "\\" +  configValue + "\\";

		// build the load file name (with appended branch tag, 
		// if the schedule used for the session has a branch specified)
		loadFile = loadIdentifier + 
			"_" + TimeToString.AsNameTime(builder.getProductSession().getLoadTimeRange().first) +
			"_" + TimeToString.AsNameTime(builder.getProductSession().getLoadTimeRange().second) +
			builder.getBranchTag();
		
		/// build the load file and load file report name
		productName = loadPath + loadFile + ".dat";
		reportName  = loadPath + loadFile + ".rpt";
		
		/// set the desired source
		List<String> cmdSources = new ArrayList<String>(); 
		cmdSources.add(cmdSource);

		/// retrieve the macro events for expansion
		filteredEvents = new ArrayList<ScheduledEvent>();
			
			for( ScheduledEvent se : builder.getScheduledEvents())
		    {
		    	// loop thru the list of sat names and add any events that
		        // are related to any satellites in the list.
		        if (se.getSatellite() == null)
		        	continue;

	            if( satName.equalsIgnoreCase(se.getSatellite().getName()))
	            {
	            	// check if event is within given time range.
					if( se.getEventStartTime().before(builder.getProductSession().getLoadTimeRange().first) ||
						se.getEventStartTime().after(builder.getProductSession().getLoadTimeRange().second)) 
						continue;
					else
					{
						//filteredEvents.add(se);
						if (!se.getEventMnemonic().startsWith("ACQ"))
						{
							switch (loadSubType)
							{
								case "AIP" :
									if (se.getActivity().getArlentrytype().equalsIgnoreCase("AIPPROC"))
										filteredEvents.add(se);
									break;
									
								case "AOCS" :
									if (se.getActivity().getArlentrytype().equalsIgnoreCase("MANEUVER"))
										filteredEvents.add(se);
									break;
									
								case "PDM" :
									if (!se.getActivity().getArlentrytype().equalsIgnoreCase("MANEUVER") &&
										!se.getActivity().getArlentrytype().equalsIgnoreCase("AIPPROC"))
										filteredEvents.add(se);
									break;
									
								default:
									filteredEvents.add(se);
							        break;
							}
						}
						else
							filteredEvents.add(se);	
					}
							 
	            }
		    }
	//	}
	    return true;
	}
	
		
	protected void LoadInspecter(List<EventTemplateLine> etls) {
		
		Inspecter inspec = new Inspecter(etls);
		inspec.run();		
		
		for (EventTemplateLine unpair : inspec.getPairDates() )
		{
			setWarningCount();
			setWarningMsg("Command not in pairs       " + TimeToString.AsLogTime(unpair.getEventTriggerTag().getTriggerTime()) +
			String.format(".%tL", unpair.getEventTriggerTag().getTriggerTime()) + " #  " + unpair.getExpandedText().substring(13));
		}
		
		for (int i =0;i<inspec.getExceptionDates().size(); i++ )		
		{			
			setWarningCount();
			setWarningMsg("Exception Command = "+inspec.getExceptionDates_Desc().get(i)+"     ," + TimeToString.AsLogTime(inspec.getExceptionDates().get(i).getEventTriggerTag().getTriggerTime()) +
			String.format(".%tL", inspec.getExceptionDates().get(i).getEventTriggerTag().getTriggerTime()) + " #  " + inspec.getExceptionDates().get(i).getExpandedText().substring(13));
		}
		
		for (EventTemplateLine overlap : inspec.getTimeOverlapDates() )
		{
			setWarningCount();
			setWarningMsg("Command time overlap    " + TimeToString.AsLogTime(overlap.getEventTriggerTag().getTriggerTime()) +
			String.format(".%tL", overlap.getEventTriggerTag().getTriggerTime()) + " #  " + overlap.getExpandedText().substring(13));
		}
		
		for (int i =0;i<inspec.getTimeCmdDates().size(); i++ )
		{			
			warningMsgs.add("Command = "+inspec.getTimeCmdDates_Desc().get(i)+"     ," + TimeToString.AsLogTime(inspec.getTimeCmdDates().get(i).getEventTriggerTag().getTriggerTime()) +
			String.format(".%tL", inspec.getTimeCmdDates().get(i).getEventTriggerTag().getTriggerTime()) + " #  " + inspec.getTimeCmdDates().get(i).getExpandedText().substring(13));
		}
		
	}
	
	
	public String getCmdSource (){
		return this.cmdSource;
	}
	
	public int getWarningCount (){
		return this.warningCount;
	}
	
	public void setWarningCount (){
		this.warningCount++;
	}
	
	public List<String> getWarningMsg (){
		return this.warningMsgs;
	}
	
	public void setWarningMsg (String warningMsg){
		this.warningMsgs.add(warningMsg);
	}

	public int getErrorCount (){
		return this.errorCount;
	}
	
	public void setErrorCount (){
		this.errorCount++;
	}
	
	public List<String> getErrorMsg (){
		return this.errorMsgs;
	}
	
	public void setErrorMsg (String errorMsg){
		this.errorMsgs.add(errorMsg);
	}
	
	public String getProductName (){
		return this.productName;
	}
	
	public String getReportName (){
		return this.reportName;
	}
	
	public String getLoadPath (){
		return this.loadPath;
	}

	public void setEventLines (List<EventTemplateLine> etls){
		this.etls = etls;
	}
	
	public List<EventTemplateLine> getEventLines (){
		return this.etls;
	}	
	
}
