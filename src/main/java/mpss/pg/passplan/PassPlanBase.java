package mpss.pg.passplan;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import mpss.configFactory;
import mpss.common.dao.ActivityDao;
import mpss.common.dao.ActivityManDao;
import mpss.common.dao.ContactDao;
import mpss.common.dao.EclipseDao;
import mpss.common.dao.ExtentDao;
import mpss.common.dao.RecorderDao;
import mpss.common.dao.RevDao;
import mpss.common.dao.SessionDao;
import mpss.common.dao.SiteDao;
import mpss.common.dao.UnavailableTimeRangeDao;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Eclipse;
import mpss.common.jpa.Recorder;
import mpss.common.jpa.Rev;
import mpss.common.jpa.Satellite;
import mpss.common.jpa.Session;
import mpss.pg.EventExpander;
import mpss.pg.EventTemplateLine;
import mpss.pg.OrbitalEvent;
import mpss.pg.RtsReplyVO;
import mpss.pg.product.ScheduleProductBuilder;
import mpss.se.ScheduledEvent;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeToString;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


//for Spring component scanning
@Service
public abstract class PassPlanBase implements PassPlan
{
	@Autowired
	RevDao revDao;
	@Autowired
	ContactDao contDao;
	@Autowired
	EclipseDao eclipseDao;
	@Autowired
	SiteDao siteDao;
	@Autowired
	RecorderDao recDao;
	@Autowired
	ActivityManDao actManDao;
	@Autowired
	SessionDao sessDao;	
	@Autowired
	ActivityDao actDao;	
	@Autowired
	ExtentDao extentDao;
	@Autowired
	UnavailableTimeRangeDao unavailableTimeRangeDao;
	
	protected Logger logger = Logger.getLogger(configFactory.getLogName());
	
	protected String[] oeSubType = new String[]{
		    "penumbraentrancetime",
		    "umbraentrancetime",
		    "umbraexittime",
		    "penumbraexittime",
		};
	
	protected ScheduleProductBuilder builder;
	protected List<String> fileNames = new ArrayList<String>();
	
	protected List<RtsReplyVO> replys= new ArrayList<RtsReplyVO>();
	
	public PassPlanBase()
	{
	}	
	
	public abstract void genPassPlan(ScheduleProductBuilder builder);
	
	public boolean Load(ScheduleProductBuilder builder)
	{
		this.builder = builder;
		Satellite sat = builder.getSatellite();
		String directory = builder.getFileDir() + sat.getName() + "\\Pass_plans\\";
		TimeRange trSch = builder.getProductSession().getProductTimeRange();
		int branchId = builder.getProductSession().getBranchId();
		
		List<Rev> revs = revDao.getRevbyRange(trSch, branchId);
		List<Contact> conts = contDao.getByRangeNSat(trSch,sat.getId().intValue(),branchId);
		for(Rev rev : revs)
		{
			for(Contact cont : conts)
			{
				
				// check if the orbit has an scheduled contact. IF it does, create the pass plan
				TimeRange trRev = new TimeRange(rev.getAntime(),rev.getRevendtime());
				if (trRev.encloses(cont.getTimeRange()))
				{
					logger.debug("cont:" + cont.getTimeRange().first.toString() + " ~ " + cont.getTimeRange().second.toString());
					try{
						String fileName = "Pass_Plan_" + sat.getOps4number() +  "_" + TimeToString.AsNameTime(rev.getAntime()) +
							"_" + TimeToString.AsNameTime(rev.getRevendtime()) + builder.getBranchTag() + ".txt";
						
						FileOutputStream fileOutputStream = new FileOutputStream(directory + fileName);
				        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
				        
				        //generate a header for the file
				        outputStreamWriter.write(genHeader(sat.getName(), rev));
				        
				        //generate PassPlan
				        SortedMap<Timestamp, List<String>> passPlanMap = generatePassPlan(sat,rev,branchId);
				        if(passPlanMap.size()!=0){
				        	for (Timestamp tmp : passPlanMap.keySet())
					        {
					        	for(String st : passPlanMap.get(tmp))
					        		outputStreamWriter.write(st + "\n");
					        }
				        	 //generate foot
					        outputStreamWriter.write(genFoot(sat,trRev,branchId));
				        }
				        			       
		
				        outputStreamWriter.close();
				        fileNames.add(directory + fileName);
				        break;
					}catch (Exception e){
						logger.info(e.getMessage());
						return false;
					}
				}
			}
		}
		
		genPassPlan(builder);			
		
		return true;
	}
	
	private SortedMap<Timestamp, List<String>> generatePassPlan(Satellite sat,Rev rev,int branchId)
	{
		TimeRange trSch = new TimeRange(rev.getAntime(), rev.getRevendtime());
		int satId = sat.getId().intValue();
		SortedMap<Timestamp, List<String>> timePassMap = new TreeMap<Timestamp, List<String>>();

		//generate the events
		List<ScheduledEvent> filteredEvents = new ArrayList<ScheduledEvent>();
		if(builder.getScheduledEvents()==null){
			return timePassMap;
		}
		for( ScheduledEvent se : builder.getScheduledEvents())
	    {
	    	if (se.getSatellite() == null)
	        	continue;

            if( sat.getName().equalsIgnoreCase(se.getSatellite().getName()))
            {
            	if( se.getEventStartTime().before(trSch.first) || se.getEventStartTime().after(trSch.second)) 
					continue;
				else
					filteredEvents.add(se);	
            }
	    }

		logger.debug("rev no:" + rev.getRevno());
		logger.debug("TimeRange:" + trSch.first.toString() + " ~ " + trSch.second.toString());
		for(ScheduledEvent s :filteredEvents)
		{
			logger.debug("Events:" + s.getEventStartTime());
		}
		EventExpander eventExpander = new EventExpander(builder);
		List<EventTemplateLine> etls;
		etls = eventExpander.expand(filteredEvents, "ALL");
		for(EventTemplateLine etl : etls)
		{
			Timestamp eventTime = etl.getEventTriggerTag().getTriggerTime();
			if(trSch.contains(eventTime))
	        {
				List<String> txts = new ArrayList<String>();
				txts.add(getPrefix(eventTime) + etl.getExpandedText().substring(13));
				timePassMap.put(eventTime, txts);
	        }
		}
		for (Timestamp tt : timePassMap.keySet())
		{
			logger.debug("PP time:" + tt + ", text:" + timePassMap.get(tt));
		}
		
		//generate the OrbitalEvents
		List<OrbitalEvent> oes = new ArrayList<OrbitalEvent>();
		OrbitalEvent oe;
		for (String oeType : oeSubType)
		{
			List<Eclipse> es = eclipseDao.getByColumnNSatNRange(oeType, satId, trSch, branchId);
			switch (oeType)
			{
				case "penumbraentrancetime" :
					for (Eclipse e : es)
					{
						oe = new OrbitalEvent("ECLIPSE",sat,e.getPenumbraentrancetime(),e.getEclipsetype(),"PENUMBRA ENTER");
						oes.add(oe);
					}
					break;
				case "umbraentrancetime" :
					for (Eclipse e : es)
					{
						oe = new OrbitalEvent("ECLIPSE",sat,e.getUmbraentrancetime(),e.getEclipsetype(),"UMBRA ENTER");
						oes.add(oe);
					}
					break;
				case "umbraexittime" :
					for (Eclipse e : es)
					{
						oe = new OrbitalEvent("ECLIPSE",sat,e.getUmbraexittime(),e.getEclipsetype(),"UMBRA EXIT");
						oes.add(oe);
					}
					break;
				case "penumbraexittime" :
					for (Eclipse e : es)
					{
						oe = new OrbitalEvent("ECLIPSE",sat,e.getPenumbraexittime(),e.getEclipsetype(),"PENUMBRA EXIT");
						oes.add(oe);
					}
					break;
			}
		}
		for(OrbitalEvent oEvent : oes)
		{
			Timestamp eventTime = oEvent.getEventTime();
			List<String> txts = new ArrayList<String>();
			if (timePassMap.get(eventTime) != null)
				txts = timePassMap.get(eventTime);
			txts.add(getPrefix(eventTime) + oEvent.getCategory() + " " + oEvent.getType() + " " + oEvent.getSubtype());
			timePassMap.put(eventTime, txts);
		}
		
		for (Timestamp tt : timePassMap.keySet()){			
			logger.debug("PP time:" + tt + ", text:" + timePassMap.get(tt));
		}
		
		//generate Rev
		if(trSch.contains(rev.getAntime()))
		{
			List<String> txts = new ArrayList<String>();
			if (timePassMap.get(rev.getAntime()) != null)
				txts = timePassMap.get(rev.getAntime());
			txts.add(getPrefix(rev.getAntime(), rev.getRevno()) + "ORBIT Ascending Node");
			timePassMap.put(rev.getAntime(), txts);
		}
		if(trSch.contains(rev.getDntime()))
		{
			List<String> txts = new ArrayList<String>();
			if (timePassMap.get(rev.getDntime()) != null)
				txts = timePassMap.get(rev.getDntime());
			txts.add(getPrefix(rev.getDntime()) + "ORBIT Descending Node");
			timePassMap.put(rev.getDntime(), txts);
		}
		
		for (Timestamp tt : timePassMap.keySet())
		{
			logger.debug("PP time:" + tt + ", text:" + timePassMap.get(tt));
		}

		//generate the Contact Events
		List<Contact> contSBAND = new ArrayList<Contact>(); ;
		for (Session session:builder.getSchSessions())
		{
			List<Contact> contTemp = contDao.getBySiteTypeNSatNBranchNRange("SBAND",satId,branchId,rev.getAntime(),rev.getRevendtime(),session.getId().intValue());
			for (Contact c : contTemp)
			{
				if (!contSBAND.contains(c))
					contSBAND.add(c);
			}
		}
		for(Contact cnt : contSBAND)
		{
			TimeRange cntTR = new TimeRange(cnt.getRise(), cnt.getFade());
			Contact cont = contDao.getBySatNBranchNotInSite(cnt.getSiteid(), satId, branchId, cntTR);

			List<String> temptxts = new ArrayList<String>();
			if(cont != null)
			{
				TimeRange contTR = new TimeRange(cont.getRise(), cont.getFade());
				temptxts = genContactHeader(cnt.getSiteid(),cont.getSiteid(),cntTR,contTR);
			}
			else
			{
				temptxts = genContactHeader(cnt.getSiteid(),cntTR);
			}
			List<String> txts = new ArrayList<String>();
			if (timePassMap.get(cntTR.first) != null)
				txts = timePassMap.get(cntTR.first);
			txts.addAll(temptxts);
			timePassMap.put(cntTR.first, txts);
			
			
			if( trSch.contains(cnt.getMaxeltime()))
	        {
				List<String> texts = new ArrayList<String>();
				if (timePassMap.get(cnt.getMaxeltime()) != null)
					texts = timePassMap.get(cnt.getMaxeltime());
				texts.add(getPrefix(cnt.getMaxeltime()) + "Pass Max Elevation");
				timePassMap.put(cnt.getMaxeltime(), texts);
	        }

			if( trSch.contains(cnt.getCmdrise()))
	        {
				List<String> texts = new ArrayList<String>();
				if (timePassMap.get(cnt.getCmdrise()) != null)
					texts = timePassMap.get(cnt.getCmdrise());
				texts.add(getPrefix(cnt.getCmdrise()) + "S-Band Command Rise");
				timePassMap.put(cnt.getCmdrise(), texts);
	        }

			if( trSch.contains(cnt.getCmdfade()))
	        {
				List<String> texts = new ArrayList<String>();
				if (timePassMap.get(cnt.getCmdfade()) != null)
					texts = timePassMap.get(cnt.getCmdfade());
				texts.add(getPrefix(cnt.getCmdfade()) + "S-Band Command Fade");
				timePassMap.put(cnt.getCmdfade(), texts);
	        }
		}
		
		for (Timestamp tt : timePassMap.keySet())
		{
			logger.debug("PP time:" + tt + ", text:" + timePassMap.get(tt));
		}
		
		return timePassMap;
	}
	
	private String genHeader(String satName,Rev rev)
	{
		String header = satName + " Mission Operations\n\n";
		header += "Pass Plan\n\n";
		header += "Mission Date: " + TimeToString.AsLogDate(rev.getAntime()) + "\n\n";
		header += "Orbit Number: " + rev.getRevno() + "\n\n";

		return header;
	}
	
	private String getPrefix(Timestamp time)
	{
		return TimeToString.AsEpochTime(time).substring(4) + "        ";
	}
	
	private String getPrefix(Timestamp time,int revNo)
	{
		return TimeToString.AsEpochTime(time).substring(4) + "-" + String.format("%05d", revNo) + "  ";
	}
	
	private List<String> genContactHeader(int Siteid,TimeRange tr)
	{
		String siteName = siteDao.get((long)Siteid).getName();
		List<String> headers = new ArrayList<String>();
		headers.add("Prime site: " + siteName);
		headers.add(siteName);
		headers.add("Predicted AOS/Actual AOS");
		headers.add("     " + TimeToString.AsTime(tr.first) + "//_:_:_");
		headers.add("Predicted LOS/Actual LOS");
		headers.add("     " + TimeToString.AsTime(tr.second) + "//_:_:_");
		headers.add(String.format("%-132s", "_").replace(" ", "_"));

		return headers;
	}
	
	private List<String> genContactHeader(int Siteid1,int Siteid2,TimeRange tr1,TimeRange tr2)
	{
		String siteName1 = siteDao.get((long)Siteid1).getName();
		String siteName2 = siteDao.get((long)Siteid2).getName();
		List<String> headers = new ArrayList<String>();
		headers.add("Prime site: " + siteName1);
		headers.add(String.format("%-108s", siteName1) + String.format("%24s", siteName2));
		headers.add(String.format("%-108s", "Predicted AOS/Actual AOS") + "Predicted AOS/Actual AOS");
		headers.add(String.format("%-108s", "     " + TimeToString.AsTime(tr1.first) + "//_:_:_") + "       " + TimeToString.AsTime(tr2.first) + "//_:_:_");
		headers.add(String.format("%-108s", "Predicted LOS/Actual LOS") + "Predicted LOS/Actual LOS");
		headers.add(String.format("%-108s", "     " + TimeToString.AsTime(tr1.second) + "//_:_:_") + "       " + TimeToString.AsTime(tr2.second) + "//_:_:_");
		headers.add(String.format("%-132s", "_").replace(" ", "_"));

		return headers;
	}
	
	private String genFoot(Satellite sat,TimeRange trSch,int branchId)
	{
		String foot = "Data accounting\n";
		foot += "NOTE:\n";
		foot += String.format("%-80s", "_").replace(" ", "_") + "\n";
		foot += String.format("%-80s", "_").replace(" ", "_") + "\n";
		foot += String.format("%-80s", "On-Shift-Personnel:").replace(" ", "_") + "\n\n";

		List<Recorder> recs = recDao.listRecorders(sat.getId().intValue());
		//int sessionId = sessDao.getSessionContainTime(trSch.second, branchId == 0 ? 0 : 1).getId().intValue();
		int sessionId = builder.getSchSessions().get(0).getId().intValue();
		
		for(Recorder rec : recs)
		{
			foot += "Recorder " + rec.getName() + " SSR Accounting Info (total/predicted/actual):" + "\n";
			//total
			foot += "RSI:   " + (int)actManDao.calculateSectors(rec.getId().intValue(), sessionId, "RSI") + "/";
			//actual
			foot +=  actManDao.getRSICapacity(trSch.second, rec.getId().intValue(), sessionId) + "/___\n\n";
		}
		return foot;
	}
	
	public List<String> getFileNames (){
		return this.fileNames;
	}
	
	
	
	

	
	
	
}

