package mpss.pg.passplan;

import java.util.List;

import mpss.pg.product.ScheduleProductBuilder;

import org.springframework.stereotype.Service;

@Service
public interface PassPlan
{	
	
	public boolean Load(ScheduleProductBuilder builder);	
	public List<String> getFileNames() ;
	public void genPassPlan(ScheduleProductBuilder builder);
	
}

