package mpss.pg.passplan;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import mpss.common.jpa.Activity;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Rev;
import mpss.common.jpa.Satellite;
import mpss.common.jpa.Unavailtimerange;
import mpss.pg.RtsReplyVO;
import mpss.pg.product.ScheduleProductBuilder;
import mpss.util.timeformat.DateUtil;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeUtil;

import org.springframework.stereotype.Component;

@Component("FS8passPlan")
public class FS8PassPlan extends PassPlanBase {

	public void genPassPlan(ScheduleProductBuilder builder) {
		// pass plan
		genPassPlan2Files(builder);
	}

	// 1 . contact has MON for TS1,TS2,TS3,svabarl(sitetype is SBAND)
	// 2 . other contacts
	private void genPassPlan2Files(ScheduleProductBuilder builder) {
		List<Integer> contact_ids = getPassPlanTimeOnSchedule(builder);
		genPassPlanTimeNotOnSchedule(builder, contact_ids);
	}

	// contact has MON for TS1,TS2,TS3,svabarl(sitetype is SBAND)
	private List<Integer> getPassPlanTimeOnSchedule(ScheduleProductBuilder builder) {
		List<Integer> contact_ids = new ArrayList<Integer>();
		try {
			TimeRange trSch = builder.getProductSession().getProductTimeRange();
			Collection<Activity> monacts = actDao.getMonActForPassPlan(trSch.first, trSch.second);
			for (Activity act : monacts) {
				contact_ids.add(extentDao.getByActId(act.getId().intValue()).getContactid());
			}
			return contact_ids;
		} catch (Exception ex) {
			logger.info("FS8PassPlan genPassPlanTimeOnSchedule() ex="+ ex.getMessage());
			return contact_ids;
		}
	}

	// other contacts
	private void genPassPlanTimeNotOnSchedule(ScheduleProductBuilder builder,List<Integer> onscheduleContactids) {
		// file name is fs??_yyyy_DDD_HH.rpt for james
		// file name is fs??_webitd_DDD.sch for webitd
		try {
			List<PassPlanVO> datas = new ArrayList<PassPlanVO>();
			List<PassPlanVO> webitd_datas= new ArrayList<PassPlanVO>();
			Satellite sat = builder.getSatellite();
			// get rpt file name
			String rpt = sat.getOps4number()+"_"	+ DateUtil.dateToString(DateUtil.timestampToDate(builder.getProductSession().getProductTimeRange().first),"yyyy_DDD_HH") + ".rpt";
			String rpt_webitd = sat.getOps4number()+"_webitd_"+ DateUtil.dateToString(DateUtil.timestampToDate(builder.getProductSession().getProductTimeRange().first),"DDD") + ".sch";
			// get data
			
			int branchId = builder.getProductSession().getBranchId();
			TimeRange trSch = builder.getProductSession().getProductTimeRange();			

			// in session contact
			List<Contact> contacts = contDao.getConsbyRangeSBandSite(trSch, sat.getId().intValue(), branchId);
			for (Contact contact : contacts) {
				String site_name = siteDao.get(contact.getSiteid().longValue()).getName4();
				String temp1 = DateUtil.dateToString(DateUtil.timestampToDate(contact.getRise()),"yyyy-DDD-HH:mm:ss");
				String temp2 = DateUtil.dateToString(DateUtil.timestampToDate(contact.getFade()),"yyyy-DDD-HH:mm:ss");
				PassPlanVO tmpvo = new PassPlanVO(site_name, temp1, temp2);
				tmpvo.setOrbit(getOrbit(contact.getRise(), contact.getFade()));
				for (Integer contactid : onscheduleContactids) {
					if (contactid.intValue() == contact.getId().intValue()) {
						tmpvo.setType("Add");
					}
				}
				datas.add(tmpvo);
				webitd_datas.add(tmpvo);
			}

			// out of session + 28 Hours and contact is not in unavailtimerange
			trSch.first = trSch.second;
			trSch.second = TimeUtil.timeAppendHour(trSch.second, 28);
			contacts = contDao.getConsbyRangeSBandSite(trSch, sat.getId().intValue(), branchId);
			for (Contact contact : contacts) {
				String site_name = siteDao.get(contact.getSiteid().longValue()).getName4();
				String temp1 = DateUtil.dateToString(DateUtil.timestampToDate(contact.getRise()),"yyyy-DDD-HH:mm:ss");
				String temp2 = DateUtil.dateToString(DateUtil.timestampToDate(contact.getFade()),"yyyy-DDD-HH:mm:ss");
				PassPlanVO tmpvo = new PassPlanVO(site_name, temp1, temp2);
				tmpvo.setOrbit(getOrbit(contact.getRise(), contact.getFade()));
				if (contact.getSiteid().intValue() == 111 || contact.getSiteid().intValue() == 150) {
					if (checkContackCanUse(branchId, contact.getSiteid().intValue(), contact.getRise(), contact.getFade())) {
						tmpvo.setType("Add");
					}
				}
				datas.add(tmpvo);
			}
			
			// out of session  all contact is not in unavailtimerange	     	
	     	trSch.second =TimeUtil.timeAppendHour(trSch.second,9999);
	     	contacts =contDao.getConsbyRangeSBandSite(trSch, sat.getId().intValue(), branchId);			
	     	for(Contact contact : contacts){
	     		String site_name =siteDao.get(contact.getSiteid().longValue()).getName4();
	     		String temp1 =DateUtil.dateToString(DateUtil.timestampToDate(contact.getRise()),"yyyy-DDD-HH:mm:ss");
	     		String temp2 =DateUtil.dateToString(DateUtil.timestampToDate(contact.getFade()),"yyyy-DDD-HH:mm:ss");	
	     		PassPlanVO tmpvo = new PassPlanVO(site_name, temp1, temp2);
	     		tmpvo.setOrbit(getOrbit(contact.getRise(),contact.getFade()));				
	     		if(contact.getSiteid().intValue()==111 || contact.getSiteid().intValue()==150){
	     			if(checkContackCanUse(branchId,contact.getSiteid().intValue(),contact.getRise(),contact.getFade())){
	     				tmpvo.setType("Add");
	     			}					
	     		}				
	     		webitd_datas.add(tmpvo);
	     	}

			// set RTS Antenna
			checkRTSAntenna4CountDown(datas, replys);
			
			// set RTS Antenna
		    checkRTSAntenna4CountDown(webitd_datas,replys);

			// write file
			String directory = builder.getFileDir() + sat.getName()+ "\\Pass_plans\\";
			FileOutputStream fileOutputStream = new FileOutputStream(directory+ rpt);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);

			for (PassPlanVO vo : datas) {
				outputStreamWriter.write(vo.toString() + "\n");
			}			
			outputStreamWriter.close();

			
			// write file			
		    fileOutputStream = new FileOutputStream(directory + rpt_webitd);
		    outputStreamWriter = new OutputStreamWriter(fileOutputStream);
		    for(PassPlanVO vo :webitd_datas){
		   	 outputStreamWriter.write(vo.toString() + "\n");
		    }	     
		    outputStreamWriter.close();
		    
		    datas.clear();
		    webitd_datas.clear();
			// 20190103 for OD , OD need long term pass paln	        
	        //FileOperate op = new FileOperate();
			//op.copyFile(directory + rpt, directory + rpt_webitd);	        

		 fileNames.add(directory + rpt);

		} catch (Exception ex) {
			logger.info("FS8 PassPlan genPassPlanTimeNotOnSchedule() ex="+ ex.getMessage());
		}
	}
	
	private String getOrbit(Timestamp aos, Timestamp los) {
		TimeRange trSch = builder.getProductSession().getProductTimeRange();
		int branchId = builder.getProductSession().getBranchId();
		// get rev start -4H , end time +(4+2*24)H for pass plan
		trSch = new TimeRange(TimeUtil.timeAppendHour(trSch.first, -4),
				TimeUtil.timeAppendHour(trSch.second, 52));
		List<Rev> revs = revDao.getRevbyRange(trSch, branchId);
		for (Rev rev : revs) {
			TimeRange trRev = new TimeRange(rev.getAntime(),rev.getRevendtime());
			TimeRange actRev = new TimeRange(aos, los);
			if (trRev.encloses(actRev)) {
				return rev.getRevno().toString();
			}
		}
		// exception
		for (Rev rev : revs) {
			TimeRange trRev = new TimeRange(rev.getAntime(),rev.getRevendtime());
			TimeRange actRev = new TimeRange(aos, los);
			if (trRev.overlaps(actRev)) {
				return String.valueOf(rev.getRevno().intValue() + 1);
			}
		}

		return "";
	}
	
	private boolean checkContackCanUse(int branch, int siteid,Timestamp starttime, Timestamp endtime) {
		String temp1 = DateUtil.dateToString(DateUtil.timestampToDate(starttime), "yyyy-MM-dd HH:mm:ss");
		String temp2 = DateUtil.dateToString(DateUtil.timestampToDate(endtime),"yyyy-MM-dd HH:mm:ss");
		List<Unavailtimerange> datas = unavailableTimeRangeDao.getSiteUnavailtimerangeByTime(branch, siteid, temp1, temp2);
		if (datas.size() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	private boolean checkRTSAntenna4CountDown(List<PassPlanVO> datas,List<RtsReplyVO> replys) {

		for (PassPlanVO ppvo : datas) {
			for (RtsReplyVO rrvo : replys) {
				if (DateUtil.checkDateInPeriod(ppvo.getAos(), rrvo.getStart_time(),Integer.valueOf(System.getProperty("rts.gap", "300")).intValue())) {
					ppvo.setAntenna(rrvo.getAntenna());
					ppvo.setStatus(rrvo.getStatus());
					if (System.getProperty("aos.los.source", "RTS").equalsIgnoreCase("RTS")) {
						ppvo.setAos(rrvo.getStart_time());
						ppvo.setLos(rrvo.getEnd_time());
						ppvo.setSource("RTS");
					}
				}
			}
		}

		boolean hasantenna = true;
		for (PassPlanVO ppvo : datas) {
			if (ppvo.getAntenna().equalsIgnoreCase("")) {
				ppvo.setAntenna("SG?");
				ppvo.setStatus("ACCEPTED");
				hasantenna = false;

				if (ppvo.getSite().equals("SVAL")) {
					ppvo.setType("None");
				}
			}
		}
		return hasantenna;
	}

}
