package mpss.pg.passplan;

import java.util.Date;

import mpss.configFactory;
import mpss.util.timeformat.DateUtil;

public class PassPlanVO {

	// FS5,TS1,Add,06/19/17,170,00:33:08,06/19/17,170,00:33:08,00:10:46,High Rate
	// FS5,TS1,None,06/19/17,170,00:33:08,06/19/17,170,00:33:08,00:10:46,High Rate
	
	private String satellite_name = configFactory.getSatelliteName();
	private String type = "None";
	@SuppressWarnings("unused")
	private String rate = "High Rate";
	private String site = "";
	private String aos = "";
	private String los = "";	
	private String orbit="";
	private int aosminus =0;
	private int losminus =0;
	private String antenna="";
	private String status="";
	private String source="FDF";
	private boolean ismainsite =false;
	
	public PassPlanVO(String site, String aos, String los) {
		setSite(site);
		setAos(aos);
		setLos(los);
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getAntenna() {
		return antenna;
	}

	public void setAntenna(String antenna) {
		this.antenna = antenna;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAos() {
		return aos;
	}
	
	public void setAosMinus(int seconds){
		this.aosminus = seconds;
	}
	public void setLosMinus(int seconds){
		this.losminus = seconds;
	}
	
	public String getAosMinus() {
		return  DateUtil.dateToString(DateUtil.DateAddSec(DateUtil.stringToDate(aos,"yyyy-DDD-HH:mm:ss"),aosminus),"yyyy-DDD-HH:mm:ss");		
	}
	
	public String getLosMinus() {
		return  DateUtil.dateToString(DateUtil.DateAddSec(DateUtil.stringToDate(los,"yyyy-DDD-HH:mm:ss"),losminus),"yyyy-DDD-HH:mm:ss");		
	}
	
	public String getFormatAos() {
		if(this.aos!=null){
			Date temp =DateUtil.stringToDate(aos,"yyyy-DDD-HH:mm:ss");
			return DateUtil.dateToString(temp, "yyyy-MM-dd HH:mm:ss")+","+DateUtil.getDateDOY(temp);
		}
		return "";
	}

	public void setAos(String aos) {
		this.aos = aos;
	}

	public String getLos() {
		return los;
	}
	
	public String getFormatLos() {
		if(this.los!=null){
			Date temp =DateUtil.stringToDate(los,"yyyy-DDD-HH:mm:ss");
			return DateUtil.dateToString(temp, "yyyy-MM-dd HH:mm:ss")+","+DateUtil.getDateDOY(temp);
		}
		return "";
	}
	
	public int getDuration(){
		if(this.los!=null && this.aos!=null){
			return DateUtil.getDuration(DateUtil.stringToDate(aos,"yyyy-DDD-HH:mm:ss"), DateUtil.stringToDate(los,"yyyy-DDD-HH:mm:ss"));
		}
		return 0;
	}

	public void setLos(String los) {
		this.los = los;
	}
	
	public String getOrbit() {
		return orbit;
	}

	public void setOrbit(String orbit) {
		this.orbit = orbit;
	}
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String toString() {
		if(!type.equals("None")){
			return satellite_name+","+site+",C,"+type+","+getFormatAos()+","+getFormatLos()+","+getDuration()+","+orbit+","+antenna;
		}else{
			return satellite_name+","+site+",O,"+type+","+getFormatAos()+","+getFormatLos()+","+getDuration()+","+orbit+","+antenna;
		}		
		
	}
	
	public boolean getIsMainSite() {
		return ismainsite;
	}

	public void setMainSite(boolean ismainsite) {
		this.ismainsite = ismainsite;
	}

}
