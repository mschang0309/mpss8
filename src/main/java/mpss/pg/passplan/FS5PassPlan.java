package mpss.pg.passplan;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import mpss.common.jpa.Activity;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Eclipse;
import mpss.common.jpa.Rev;
import mpss.common.jpa.Satellite;
import mpss.common.jpa.Unavailtimerange;
import mpss.pg.RtsReplyVO;
import mpss.pg.product.ScheduleProductBuilder;
import mpss.util.FileOperate;
import mpss.util.files.ReadTxtFile;
import mpss.util.timeformat.DateUtil;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeUtil;

import org.springframework.stereotype.Component;

@Component("FS5passPlan")
public class FS5PassPlan extends PassPlanBase {
	
	public void genPassPlan(ScheduleProductBuilder builder) {

		gen_prc4Gps(builder);
		// rpp
		gen_prc4OCByRTSPassPlanTime(builder);
		// gen_prc4OCByTaiwanSitePassPlanTime(builder);

		// pass plan
		genPassPlan2Files(builder);

	}

	private void gen_prc4Gps(ScheduleProductBuilder builder) {
		// file name is FS5_AOCS_yyyy_DDD_HH.prc
		Satellite sat = builder.getSatellite();
		String directory = builder.getFileDir() + sat.getName() + "\\TTQs\\";

		try {
			TimeRange trSch = builder.getProductSession().getProductTimeRange();
			List<TimeRange> trs = new ArrayList<TimeRange>();
			// TimeRange t1 = RangeUtil.startTimeAppendHourMinu(trSch.first, 12,
			// 0);
			// TimeRange t2 = RangeUtil.startTimeAppendHourMinu(t1.second, 12,
			// 0);
			// trs.add(t1);
			// trs.add(t2);
			trs.add(trSch);

			for (TimeRange tr : trs) {
				// get rpt file name by time
				String filetime = "";
				filetime = DateUtil.dateToString(
						DateUtil.timestampToDate(tr.first), "yyyy_DDD_HH");
				String rpt = "FS5_AOCS_" + filetime + ".prc";

				// write file
				FileOutputStream fileOutputStream = new FileOutputStream(
						directory + rpt);
				OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
						fileOutputStream);

				List<Eclipse> es = eclipseDao.getByColumnNSatNRangeType(
						"umbraentrancetime", "Earth/Sun", sat.getId()
								.intValue(), tr, 0);

				// write header
				List<String> headers = genGpsPrcHeader(tr, es.size(), rpt);
				for (String header : headers) {
					outputStreamWriter.write(header + "\n");
				}

//				List<String> scripts = genGpsPrcScriptHeader("EGPSRASW", "GPSA");
//				for (String script : scripts) {
//					outputStreamWriter.write(script + "\n");
//				}
//
//				// write data
//				for (Eclipse eclipse : es) {
//					// eclipse.getUmbraentrancetime();
//					String mydateA = DateUtil.timestampAddSecToString(
//							eclipse.getUmbraentrancetime(),
//							"yyyy DDD HH mm ss", 961);
//					outputStreamWriter.write("        cmd ttcmd " + mydateA
//							+ " 0 {DAGPSNOMINALATTA EARTH_ORIENT = 1 } \n");
//				}
//
//				List<String> foots = genGpsPrcScriptFoot("GPSA");
//				for (String foot : foots) {
//					outputStreamWriter.write(foot + "\n");
//				}
//
//				outputStreamWriter.write("\n");
//				outputStreamWriter.write("    WAIT 1 \n");

				 List<String> scripts =genGpsPrcScriptHeader("EGPSRBSW","GPSB");
				 for(String script :scripts){
					 outputStreamWriter.write(script + "\n");
				 }
				
				 for(Eclipse eclipse : es){
					 //eclipse.getUmbraentrancetime();
					 String mydateB=DateUtil.timestampAddSecToString(eclipse.getUmbraentrancetime(),"yyyy DDD HH mm ss",960);
					 outputStreamWriter.write("        cmd ttcmd "+mydateB+" 0 {DAGPSNOMINALATTB EARTH_ORIENT = 1 } \n");
				 }
				
				 List<String> foots =genGpsPrcScriptFoot("GPSB");
				 for(String foot :foots){
					 outputStreamWriter.write(foot + "\n");
				 }				
				 
				 outputStreamWriter.write("\n");
				 outputStreamWriter.write("    WAIT 1 \n");

				//MTQ
				for (Eclipse eclipse : es) {
					//String mydateB=DateUtil.timestampAddSecToString(eclipse.getUmbraentrancetime(),"yyyy DDD HH mm ss",-20*60);	
					String mydateB=DateUtil.timestampAddSecToString(eclipse.getUmbraentrancetime(),"yyyy DDD HH mm ss",-1400); //-200 -20*60			
					outputStreamWriter.write("        cmd ttcmd "+mydateB +" 0 { PMODNMCTL {UMOMGAIN (0.00306) (0.00306) (0.00306)}} \n");
					//mydateB=DateUtil.timestampAddSecToString(eclipse.getUmbraexittime(),"yyyy DDD HH mm ss",-17*60);
					//mydateB=DateUtil.timestampAddSecToString(eclipse.getUmbraentrancetime(),"yyyy DDD HH mm ss",-30);
					mydateB=DateUtil.timestampAddSecToString(eclipse.getUmbraentrancetime(),"yyyy DDD HH mm ss",-230);    //-200 -30
					outputStreamWriter.write("        cmd ttcmd "+mydateB +" 0 { PMODNMCTL {UMOMGAIN (0.0) (0.0) (0.0)}} \n");
				}

				outputStreamWriter.close();
				fileNames.add(directory + rpt);

			}

		} catch (Exception ex) {
			logger.info("PassPlan gen_prc4Gps() ex=" + ex.getMessage());
		}
	}

	private List<String> genGpsPrcHeader(TimeRange trSch, int count,
			String filename) {
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		List<String> datas = new ArrayList<String>();
		datas.add("# Generating a GPS PRC File on "	+ String.format("%tD", currentTime)	+ String.format(" %tT", currentTime) + " ("	+ String.format("%tY/", currentTime)+ String.format("%tj", currentTime) + ")");
		datas.add("# PRC File Name\t\t\t " + filename);
		datas.add("# PRC File Start Date\t\t\t "+ DateUtil.dateToString(DateUtil.timestampToDate(trSch.first),"yyyy-MM-dd HH:mm:ss"));
		datas.add("# PRC File End Date\t\t\t "+ DateUtil.dateToString(DateUtil.timestampToDate(trSch.second),"yyyy-MM-dd HH:mm:ss"));
		//datas.add("# PRC File Command counter : \t\t\t GPSA("+count+")" +"      GPSB("+count+")");
		//datas.add("# PRC File Command counter : \t\t\t GPSA(0)" +"      GPSB("+count+")      MTQ("+count*2+")" );
		//datas.add("# PRC File Command counter : \t\t\t GPSA(" + count + ")"	+ "      GPSB(0)      MTQ(" + count * 2 + ")");
		datas.add("# PRC File Command counter : \t\t\t GPSA(0)"	+ "      GPSB(" + count + ")      MTQ(" + count * 2 + ")");
		datas.add("");
		datas.add("procedure popup");
		return datas;
	}

	private List<String> genGpsPrcScriptHeader(String pointname, String gps) {
		List<String> datas = new ArrayList<String>();

		datas.add("");
		datas.add("    IF(" + pointname + " == \"ON\") THEN");
		datas.add("        WRITE \"  " + gps + " is ON\" ");
		return datas;
	}

	private List<String> genGpsPrcScriptFoot(String gps) {
		List<String> datas = new ArrayList<String>();		
		datas.add("    ENDIF");
		return datas;
	}

	private void gen_prc4OCByRTSPassPlanTime(ScheduleProductBuilder builder) {
		// file name is yyyy_DDD_HH_rts.prc
		Satellite sat = builder.getSatellite();
		String directory = builder.getFileDir() + sat.getName()
				+ "\\Pass_plans\\";
		LoadProperties(directory + "config\\RTS.properties");
		checkRtsReplyXml(directory);
		LoadRTS_Reply(directory + "config\\RTS_reply_week_first.txt", directory
				+ "config\\RTS_reply_week_second.txt");

	}

	private void LoadProperties(String configFile) {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(configFile));
			@SuppressWarnings("rawtypes")
			Iterator iterator = properties.keySet().iterator();
			while (iterator.hasNext()) {
				String key = iterator.next().toString();
				String value = properties.getProperty(key);
				System.setProperty(key, value);
			}
		} catch (FileNotFoundException ex) {
			logger.info("LoadProperties file not found ex=" + ex.getMessage());
		} catch (IOException ex) {
			logger.info("LoadProperties ex=" + ex.getMessage());
		}
	}

	private boolean checkRtsReplyXml(String directory) {
		try {
			String newfile = directory + "config\\RTS_reply.txt";
			String week_first = directory + "config\\RTS_reply_week_first.txt";
			String week_second = directory
					+ "config\\RTS_reply_week_second.txt";

			List<String> newdatas = ReadTxtFile.readFile(newfile);
			List<String> secnddatas = ReadTxtFile.readFile(week_second);

			Date newxmldate = DateUtil.stringToDate(
					new RtsReplyVO(newdatas.get(0)).getStart_time(),
					"yyyy-DDD-HH:mm:ss");
			Date secondxmldate = DateUtil.stringToDate(new RtsReplyVO(
					secnddatas.get(0)).getStart_time(), "yyyy-DDD-HH:mm:ss");

			if (newxmldate.compareTo(secondxmldate) == 0) {
				// copy file
				// if has new replay,
				FileOperate op = new FileOperate();
				op.copyFile(newfile, week_second);
				return true;
			} else if (newxmldate.after(secondxmldate)) {
				// copy file
				FileOperate op = new FileOperate();
				op.copyFile(week_second, week_first);
				op.copyFile(newfile, week_second);
				return true;
			} else {
				return false;
			}

		} catch (Exception ex) {
			logger.info("checkRtsReplyXml ex=" + ex.getMessage());
			return false;
		}
	}

	private void LoadRTS_Reply(String replyFileFirst, String replyFileScond) {
		replys.clear();
		try {
			List<String> datas = ReadTxtFile.readFile(replyFileFirst);
			for (String data : datas) {
				replys.add(new RtsReplyVO(data));
			}

			datas = ReadTxtFile.readFile(replyFileScond);
			for (String data : datas) {
				replys.add(new RtsReplyVO(data));
			}

		} catch (Exception ex) {
			logger.info("LoadRTS_Reply ex=" + ex.getMessage());
		}
	}

	@SuppressWarnings("unused")
	private void gen_prc4OCByTaiwanSitePassPlanTime(
			ScheduleProductBuilder builder) {
		// file name is
		// yyyy_DDD_HH_ts1.prc,yyyy_DDD_HH_ts2.prc,yyyy_DDD_HH_ts3.prc
		Satellite sat = builder.getSatellite();
		String directory = builder.getFileDir() + sat.getName()
				+ "\\Pass_plans\\";
		LoadProperties(directory + "config\\TAIWAN.properties");
		try {
			List<PassPlanVO> ts1datas = new ArrayList<PassPlanVO>();
			List<PassPlanVO> ts2datas = new ArrayList<PassPlanVO>();
			List<PassPlanVO> ts3datas = new ArrayList<PassPlanVO>();

			// get data

			TimeRange trSch = builder.getProductSession().getProductTimeRange();
			Collection<Activity> monacts = actDao.getMonActForPassPlan(
					trSch.first, trSch.second);

			for (Activity act : monacts) {
				String site_name = siteDao.get(contDao.get(extentDao.getByActId(act.getId().intValue()).getContactid().longValue()).getSiteid().longValue()).getName4();
				String temp1 = DateUtil.dateToString(DateUtil.timestampToDate(act.getStarttime()),"yyyy-DDD-HH:mm:ss");
				String temp2 = DateUtil.dateToString(DateUtil.timestampToDate(act.getEndtime()),"yyyy-DDD-HH:mm:ss");	
				PassPlanVO tmpvo = new PassPlanVO(site_name, temp1, temp2);
				tmpvo.setOrbit(getOrbit(act.getStarttime(), act.getEndtime()));
				if (tmpvo.getSite().equalsIgnoreCase("TS1")	|| tmpvo.getSite().equalsIgnoreCase("TS2") || tmpvo.getSite().equalsIgnoreCase("TS3")) {
					ts1datas.add(getTaiwanContact(sat.getId(), 0, 149,tmpvo.getSite(), "TS1", tmpvo.getOrbit()));
					ts2datas.add(getTaiwanContact(sat.getId(), 0, 150,tmpvo.getSite(), "TS2", tmpvo.getOrbit()));
					ts3datas.add(getTaiwanContact(sat.getId(), 0, 151,tmpvo.getSite(), "TS3", tmpvo.getOrbit()));
				}

			}

			// get prc file name
			String ts1rpt = genTaiwanPrc("TS1", directory, ts1datas);
			String ts2rpt = genTaiwanPrc("TS2", directory, ts2datas);
			String ts3rpt = genTaiwanPrc("TS3", directory, ts3datas);

			fileNames.add(directory + ts1rpt);
			fileNames.add(directory + ts2rpt);
			fileNames.add(directory + ts3rpt);

		} catch (Exception ex) {
			logger.info("PassPlan gen_prc4OCByTaiwanSitePassPlanTime() ex="+ ex.getMessage());
		}
	}

	private String genTaiwanPrc(String site, String directory,
			List<PassPlanVO> datas) {
		// get rpt file name by time
		String rpt = "default.prc";
		String filetime = "";
		filetime = DateUtil.dateToString(DateUtil.timestampToDate(builder.getProductSession().getProductTimeRange().first),
				"yyyy_DDD_HH");
		rpt = filetime + "_" + site.toLowerCase() + ".prc";

		try {
			// write file
			FileOutputStream fileOutputStream = new FileOutputStream(directory+ rpt);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);

			List<String> headers = genPrcHeader(directory+ "config\\TAIWAN_Header.txt");
			for (String header : headers) {
				outputStreamWriter.write(header + "\n");
			}

			for (PassPlanVO vo : datas) {
				outputStreamWriter.write("\n");
				outputStreamWriter.write("########################################################################   \n");
				outputStreamWriter.write("# Orbit number : " + vo.getOrbit()+ ", " + vo.getSource() + " AOS : " + vo.getAos()+ " , " + vo.getSource() + " LOS : " + vo.getLos()	+ " \n");
				outputStreamWriter.write("#    AOS Minus : "+ Integer.valueOf(System.getProperty("taiwan.aos.minus", "-600"))+ "(s) , LOS Minus: "+ Integer.valueOf(System.getProperty("taiwan.los.minus1", "600")) + "(s) \n");
				outputStreamWriter.write("########################################################################   \n");
				outputStreamWriter.write("\n");
				outputStreamWriter.write("\n");
				if (vo.getIsMainSite()) {
					outputStreamWriter.write(String.format(genPrcScript(directory+ "config\\TAIWAN_main_script.txt"), vo.getAosMinus(), vo.getLosMinus(), vo.getSite().toUpperCase(), vo.getSite().toLowerCase())+ "\n");
				} else {
					outputStreamWriter.write(String.format(genPrcScript(directory+ "config\\TAIWAN_backup_script.txt"), vo.getAosMinus(), vo.getLosMinus(), vo.getSite().toUpperCase(), vo.getSite().toLowerCase())	+ "\n");
				}

			}
			outputStreamWriter.close();
		} catch (Exception ex) {
			logger.info("PassPlan genTaiwanPrc() ex=" + ex.getMessage());
		}

		return rpt;
	}

	private List<String> genPrcHeader(String header_filename) {
		List<String> datas = ReadTxtFile.readFile(header_filename);
		datas.add("");
		return datas;
	}

	private String genPrcScript(String script_filename) {
		StringBuffer script = new StringBuffer();
		List<String> datas = ReadTxtFile.readFile(script_filename);
		for (String data : datas) {
			script.append(data + "\n");
		}
		script.append("\n");
		return script.toString();
	}

	private String getOrbit(Timestamp aos, Timestamp los) {
		TimeRange trSch = builder.getProductSession().getProductTimeRange();
		int branchId = builder.getProductSession().getBranchId();
		// get rev start -4H , end time +(4+2*24)H for pass plan
		trSch = new TimeRange(TimeUtil.timeAppendHour(trSch.first, -4),
				TimeUtil.timeAppendHour(trSch.second, 52));
		List<Rev> revs = revDao.getRevbyRange(trSch, branchId);
		for (Rev rev : revs) {
			TimeRange trRev = new TimeRange(rev.getAntime(),
					rev.getRevendtime());
			TimeRange actRev = new TimeRange(aos, los);
			if (trRev.encloses(actRev)) {
				return rev.getRevno().toString();
			}
		}

		// exception
		for (Rev rev : revs) {
			TimeRange trRev = new TimeRange(rev.getAntime(),
					rev.getRevendtime());
			TimeRange actRev = new TimeRange(aos, los);
			if (trRev.overlaps(actRev)) {
				return String.valueOf(rev.getRevno().intValue() + 1);
			}
		}

		return "";
	}

	private PassPlanVO getTaiwanContact(long satId, int branchId, int siteId,
			String mainsite, String site_name, String revid) {
		Rev rev = revDao.getByRevno(Integer.valueOf(revid).intValue(), 1, 0);
		Contact contact = contDao.getConbySiteRev(satId, branchId, siteId, rev
				.getId().toString());
		String temp1 = DateUtil.dateToString(
				DateUtil.timestampToDate(contact.getRise()),
				"yyyy-DDD-HH:mm:ss");
		String temp2 = DateUtil.dateToString(
				DateUtil.timestampToDate(contact.getFade()),
				"yyyy-DDD-HH:mm:ss");
		PassPlanVO tmpvo = new PassPlanVO(site_name, temp1, temp2);
		tmpvo.setOrbit(revid);
		tmpvo.setAosMinus(Integer.valueOf(
				System.getProperty("taiwan.aos.minus", "-600")).intValue());
		tmpvo.setLosMinus(Integer.valueOf(
				System.getProperty("taiwan.los.minus1", "600")).intValue());
		if (mainsite.equalsIgnoreCase(site_name)) {
			tmpvo.setMainSite(true);
		} else {
			tmpvo.setMainSite(false);
		}

		return tmpvo;
	}

	// 1 . contact has MON for TS1,TS2,TS3,svabarl(sitetype is SBAND)
	// 2 . other contacts
	private void genPassPlan2Files(ScheduleProductBuilder builder) {
		List<Integer> contact_ids = getPassPlanTimeOnSchedule(builder);
		genPassPlanTimeNotOnSchedule(builder, contact_ids);
	}

	// contact has MON for TS1,TS2,TS3,svabarl(sitetype is SBAND)
	private List<Integer> getPassPlanTimeOnSchedule(
			ScheduleProductBuilder builder) {
		List<Integer> contact_ids = new ArrayList<Integer>();
		try {
			TimeRange trSch = builder.getProductSession().getProductTimeRange();
			Collection<Activity> monacts = actDao.getMonActForPassPlan(
					trSch.first, trSch.second);

			for (Activity act : monacts) {
				contact_ids.add(extentDao.getByActId(act.getId().intValue())
						.getContactid());
			}

			return contact_ids;
		} catch (Exception ex) {
			logger.info("PassPlan genPassPlanTimeOnSchedule() ex="
					+ ex.getMessage());
			return contact_ids;
		}
	}

	// other contacts
	private void genPassPlanTimeNotOnSchedule(ScheduleProductBuilder builder,List<Integer> onscheduleContactids) {
		// file name is fs5_yyyy_DDD_HH.rpt for james
				// file name is fs5_webitd_DDD.sch for webitd
				try{
					List<PassPlanVO> datas= new ArrayList<PassPlanVO>();
					List<PassPlanVO> webitd_datas= new ArrayList<PassPlanVO>();
					// get rpt file name
					String rpt = "fs5_"+DateUtil.dateToString(DateUtil.timestampToDate(builder.getProductSession().getProductTimeRange().first),"yyyy_DDD_HH")+".rpt";
					String rpt_webitd = "fs5_webitd_"+DateUtil.dateToString(DateUtil.timestampToDate(builder.getProductSession().getProductTimeRange().first),"DDD")+".sch";
					// get data 
					Satellite sat = builder.getSatellite();			
					int branchId = builder.getProductSession().getBranchId();
					TimeRange trSch = builder.getProductSession().getProductTimeRange();		
					
					//Collection<Activity> monacts = actDao.getMonActForPassPlan(trSch.first,trSch.second);
					
					// in session contact
					List<Contact> contacts =contDao.getConsbyRangeSBandSite(trSch, sat.getId().intValue(), branchId);
					for(Contact contact : contacts){
						String site_name =siteDao.get(contact.getSiteid().longValue()).getName4();
						String temp1 =DateUtil.dateToString(DateUtil.timestampToDate(contact.getRise()),"yyyy-DDD-HH:mm:ss");
						String temp2 =DateUtil.dateToString(DateUtil.timestampToDate(contact.getFade()),"yyyy-DDD-HH:mm:ss");	
						PassPlanVO tmpvo = new PassPlanVO(site_name, temp1, temp2);
						tmpvo.setOrbit(getOrbit(contact.getRise(),contact.getFade()));
						for(Integer contactid:onscheduleContactids){
							if(contactid.intValue() == contact.getId().intValue()){
								tmpvo.setType("Add");
							}
						}				
						datas.add(tmpvo);
						webitd_datas.add(tmpvo);
					}
					
					// out of session + 28 Hours and contact is not in unavailtimerange
					trSch.first = trSch.second;
					trSch.second =TimeUtil.timeAppendHour(trSch.second,28);
					contacts =contDao.getConsbyRangeSBandSite(trSch, sat.getId().intValue(), branchId);			
					for(Contact contact : contacts){
						String site_name =siteDao.get(contact.getSiteid().longValue()).getName4();
						String temp1 =DateUtil.dateToString(DateUtil.timestampToDate(contact.getRise()),"yyyy-DDD-HH:mm:ss");
						String temp2 =DateUtil.dateToString(DateUtil.timestampToDate(contact.getFade()),"yyyy-DDD-HH:mm:ss");	
						PassPlanVO tmpvo = new PassPlanVO(site_name, temp1, temp2);
						tmpvo.setOrbit(getOrbit(contact.getRise(),contact.getFade()));				
						if(contact.getSiteid().intValue()==111 || contact.getSiteid().intValue()==150){
							if(checkContackCanUse(branchId,contact.getSiteid().intValue(),contact.getRise(),contact.getFade())){
								tmpvo.setType("Add");
							}					
						}				
						datas.add(tmpvo);
					}
					
					// out of session  all contact is not in unavailtimerange	     	
			     	trSch.second =TimeUtil.timeAppendHour(trSch.second,9999);
			     	contacts =contDao.getConsbyRangeSBandSite(trSch, sat.getId().intValue(), branchId);			
			     	for(Contact contact : contacts){
			     		String site_name =siteDao.get(contact.getSiteid().longValue()).getName4();
			     		String temp1 =DateUtil.dateToString(DateUtil.timestampToDate(contact.getRise()),"yyyy-DDD-HH:mm:ss");
			     		String temp2 =DateUtil.dateToString(DateUtil.timestampToDate(contact.getFade()),"yyyy-DDD-HH:mm:ss");	
			     		PassPlanVO tmpvo = new PassPlanVO(site_name, temp1, temp2);
			     		tmpvo.setOrbit(getOrbit(contact.getRise(),contact.getFade()));				
			     		if(contact.getSiteid().intValue()==111 || contact.getSiteid().intValue()==150){
			     			if(checkContackCanUse(branchId,contact.getSiteid().intValue(),contact.getRise(),contact.getFade())){
			     				tmpvo.setType("Add");
			     			}					
			     		}				
			     		webitd_datas.add(tmpvo);
			     	}
					
					
					
					// set RTS Antenna
					checkRTSAntenna4CountDown(datas,replys);	
					
					// set RTS Antenna
				    checkRTSAntenna4CountDown(webitd_datas,replys);
					
					// write file			
					String directory = builder.getFileDir() + sat.getName() + "\\Pass_plans\\";
					FileOutputStream fileOutputStream = new FileOutputStream(directory + rpt);
			        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
			        
			        for(PassPlanVO vo :datas){
			        	outputStreamWriter.write(vo.toString() + "\n");
			        }	        
			        outputStreamWriter.close();	     			
				     			
				    // write file			
				    fileOutputStream = new FileOutputStream(directory + rpt_webitd);
				    outputStreamWriter = new OutputStreamWriter(fileOutputStream);
				    for(PassPlanVO vo :webitd_datas){
				   	 outputStreamWriter.write(vo.toString() + "\n");
				    }	     
				    outputStreamWriter.close();
				    
				    datas.clear();
				    webitd_datas.clear();
			        
			        // 20190103 for OD , OD need long term pass paln	        
			        //FileOperate op = new FileOperate();
					//op.copyFile(directory + rpt, directory + rpt_webitd); 
			    
					
			     fileNames.add(directory + rpt);

		} catch (Exception ex) {
			logger.info("PassPlan genPassPlanTimeNotOnSchedule() ex="+ ex.getMessage());
		}
	}

	private boolean checkContackCanUse(int branch, int siteid,
			Timestamp starttime, Timestamp endtime) {
		String temp1 = DateUtil.dateToString(
				DateUtil.timestampToDate(starttime), "yyyy-MM-dd HH:mm:ss");
		String temp2 = DateUtil.dateToString(DateUtil.timestampToDate(endtime),
				"yyyy-MM-dd HH:mm:ss");
		List<Unavailtimerange> datas = unavailableTimeRangeDao
				.getSiteUnavailtimerangeByTime(branch, siteid, temp1, temp2);
		if (datas.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

	private boolean checkRTSAntenna4CountDown(List<PassPlanVO> datas,
			List<RtsReplyVO> replys) {

		for (PassPlanVO ppvo : datas) {
			for (RtsReplyVO rrvo : replys) {
				if (DateUtil.checkDateInPeriod(ppvo.getAos(), rrvo
						.getStart_time(),
						Integer.valueOf(System.getProperty("rts.gap", "300"))
								.intValue())) {
					ppvo.setAntenna(rrvo.getAntenna());
					ppvo.setStatus(rrvo.getStatus());
					if (System.getProperty("aos.los.source", "RTS")
							.equalsIgnoreCase("RTS")) {
						ppvo.setAos(rrvo.getStart_time());
						ppvo.setLos(rrvo.getEnd_time());
						ppvo.setSource("RTS");
					}

				}
			}
		}

		boolean hasantenna = true;
		for (PassPlanVO ppvo : datas) {
			if (ppvo.getAntenna().equalsIgnoreCase("")) {
				ppvo.setAntenna("SG?");
				ppvo.setStatus("ACCEPTED");
				hasantenna = false;

				if (ppvo.getSite().equals("SVAL")) {
					ppvo.setType("None");
				}

			}
		}

		return hasantenna;
	}

	@SuppressWarnings("unused")
	private boolean checkRTSAntenna(List<PassPlanVO> datas,
			List<RtsReplyVO> replys) {

		for (PassPlanVO ppvo : datas) {
			for (RtsReplyVO rrvo : replys) {
				if (DateUtil.checkDateInPeriod(ppvo.getAos(), rrvo
						.getStart_time(),
						Integer.valueOf(System.getProperty("rts.gap", "300"))
								.intValue())) {
					ppvo.setAntenna(rrvo.getAntenna());
					ppvo.setStatus(rrvo.getStatus());
					if (System.getProperty("aos.los.source", "RTS")
							.equalsIgnoreCase("RTS")) {
						ppvo.setAos(rrvo.getStart_time());
						ppvo.setLos(rrvo.getEnd_time());
						ppvo.setSource("RTS");
					}

				}
			}
		}

		boolean hasantenna = true;
		for (PassPlanVO ppvo : datas) {
			if (ppvo.getAntenna().equalsIgnoreCase("")) {
				ppvo.setAntenna("SG?");
				ppvo.setStatus("ACCEPTED");
				hasantenna = false;
			}
		}

		return hasantenna;
	}
}
