package mpss.pg;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;

import mpss.se.ScheduledEvent;

public class EventTemplateLine implements Cloneable 

{
	// ex : 00:01:05.000- CMD ESTXAEN SRC=TTQ   # S-Band Transmitter-A Enable		(TTQ)
	
	/// full event template line text ,ex: 00:01:05.000- CMD ESTXAEN SRC=TTQ   # S-Band Transmitter-A Enable		(TTQ)
	private String fullLineText;

	/// text (command or directive) section of micro-event ,ex: CMD ESTXAEN SRC=TTQ
	private String micEventText;
	//ex : SRC=TTQ   # S-Band Transmitter-A Enable		(TTQ)
	private String loadText;

	/// comment section of micro-event ,ex: # S-Band Transmitter-A Enable		(TTQ)
	private String micEventComment;
	// ex: ESTXAEN
	private String loadComment;

	/// flag indicating if the micro-event is selected for inclusion
	/// of generated products
	private boolean micSelected;

	/// flag indicating if the micro-event is command
	private boolean micIsCommand;

	/// command source : RLT , MPQ , TTQ , ex: TTQ
	private String micCmdSource;

	/// flag indicating if the micro-event was expanded from a macro-event
	private boolean micHasEvent;

	/// flag indicating if the micro-event is valid
	private boolean micIsValid;

	/// the micro-events position relative to other micro-events , ex : 0
	private int micEventSequence;
		
	// Event template line is empty or blank or first char is ! to true ==> not add elts  , default : false
	private boolean skipped;
	
	/// after variable substitutions , ex: 00:01:05.000- CMD ESTXAEN SRC=TTQ   # S-Band Transmitter-A Enable		(TTQ)
	private String expandedText;
	
	// parse 00:01:05.000- to object
	private EventTriggerTag eventTriggerTag;
	private ScheduledEvent scheduledEvent;
		
	// EventTemplateLine parse variable symbols, maybe 0 ~ 9 , sign value use EventExpander.substitute
	private List<VariableSymbol> variableSymbols = new ArrayList<VariableSymbol>();
	// can parse variable symbols , total size = 9
	private Map<String, Integer> variableSymbolType = new HashMap<String, Integer>();
	//private List<VariableSymbol> variableSymbols;
	//private Map<String, Integer> variableSymbolType;
	
    // Character classes in regex
    // [abc]	a, b, or c (simple class)
    // [^abc]	Any character except a, b, or c (negation)
    // [a-zA-Z]	a through z or A through Z, inclusive (range)
    // [a-d[m-p]]	a through d, or m through p: [a-dm-p] (union)
    // [a-z&&[def]]	d, e, or f (intersection)
    // [a-z&&[^bc]]	a through z, except for b and c: [ad-z] (subtraction)
    // [a-z&&[^m-p]]	a through z, and not m through p: [a-lq-z](subtraction)		
	private Pattern startPattern = Pattern.compile("[$?]");
	private Pattern endPattern = Pattern.compile("[,!* )(<>:%^;&@'/\"\t+}{-]");
	
	
	// ctor
	public EventTemplateLine(){
			//this.fullLineText = fullText;
			this.micIsValid = true;
		    this.micSelected = true;
		    this.micHasEvent = false;
		    this.skipped = false;
			    
		    // load
		    this.variableSymbolType.put("$LOADFILE", 1);
		    this.variableSymbolType.put("$LOAD_START", 2);
		    this.variableSymbolType.put("$LOAD_STOP", 3);
		    // contact
		    this.variableSymbolType.put("$OPS", 4);
		    this.variableSymbolType.put("$REV", 5);
		    this.variableSymbolType.put("$SITE", 6);
		    this.variableSymbolType.put("$PASS_END_TIME", 7);
		    this.variableSymbolType.put("$SKIP_TAG", 8);
		    this.variableSymbolType.put("$M_EVENT_DUR", 9);
	}

	/// public properties	
	public String getFullLineText()
	{
		return this.fullLineText;
	}
		
	public void setFullLineText(String fullLineText)
	{
		this.fullLineText = fullLineText;
	}
		
	public String getMicEventText()
	{
		return this.micEventText;
	}
		
	public void setMicEventText(String micEventText)
	{
		this.micEventText= micEventText;
	}
		
	public String getMicEventComment()
	{
		return this.micEventComment;
	}
		
	public void setMicEventComment(String micEventComment)
	{
		this.micEventComment = micEventComment;
	}

	public String getLoadComment()
	{
		return this.loadComment;
	}
	
	public void setLoadComment(String loadComment)
	{
		this.loadComment = loadComment;
	}

	public String getLoadText()
	{
		return this.loadText;
	}
	
	public void setLoadText(String loadText)
	{
		this.loadText = loadText;
	}

	public boolean getMicSelected()
	{
		return this.micSelected;
	}
		
	public void setMicSelected(boolean micSelected)
	{
		this.micSelected = micSelected;
	}

	public boolean getMicIsCommand()
	{
		return this.micIsCommand;
	}
		
	public void setMicIsCommand(boolean micIsCommand)
	{
		this.micIsCommand = micIsCommand;
	}

	public String getMicCmdSource()
	{
		return this.micCmdSource;
	}
		
	public void setMicCmdSource(String micCmdSource)
	{
		this.micCmdSource = micCmdSource;
	}

	public boolean getMicHasEvent()
	{
		return this.micHasEvent;
	}
		
	public void setMicHasEvent(boolean micHasEvent)
	{
		this.micHasEvent = micHasEvent;
	}

	public boolean getMicIsValid()
	{
		return this.micIsValid;
	}
		
	public void setMicIsValid(boolean micIsValid)
	{
		this.micIsValid = micIsValid;
	}

	public int getMicEventSequence()
	{
		return this.micEventSequence;
	}
		
	public void setMicEventSequence(int micEventSequence)
	{
		this.micEventSequence = micEventSequence;
	}
		
	public boolean getSkipped()
	{
	    return this.skipped;
	}
	
	public ScheduledEvent getScheduledEvent()
	{
		return this.scheduledEvent;
	}
		
	public void setScheduledEvent(ScheduledEvent scheduledEvent)
	{
		this.scheduledEvent= scheduledEvent;
	}
	
	public EventTriggerTag getEventTriggerTag()
	{
		return this.eventTriggerTag;
	}
		
	public void setEventTriggerTag(EventTriggerTag eventTriggerTag)
	{
		this.eventTriggerTag= eventTriggerTag;
	}
	
	public List<VariableSymbol> getVariableSymbols()
	{
	    return this.variableSymbols;
	}
	
	public Map<String, Integer>  getVariableSymbolType()
	{
	    return this.variableSymbolType;
	}
	
	public String getExpandedText()
	{
		return this.expandedText;
	}
		
	public void setExpandedText(String expandedText)
	{
		this.expandedText= expandedText;
	}
	public Object clone() throws CloneNotSupportedException 
	{
		EventTemplateLine o = (EventTemplateLine)super.clone();
		//o.variableSymbolType = new HashMap<String, Integer>();
		if (this.eventTriggerTag != null)
			o.eventTriggerTag=(EventTriggerTag)this.eventTriggerTag.clone();
		//o.variableSymbols = new ArrayList<VariableSymbol>();

		return o;
	}
	/// methods	
	
	// isSymbolReplace=true for event template , isSymbolReplace=false for proc template
	public void parse(boolean isSymbolReplace)
	{
	    // check whether event template line is blank or it is a comment line (start with '!')
	    // will be skipped
	    if (this.fullLineText.isEmpty() || this.fullLineText.trim().length() == 0)
	    {
	    	//System.out.println("Event template line is empty or blank");
	    	this.skipped = true;
	    	return;
		    	 
	    }
	    else
	    {
	    	/// trim the leading whitespace
	    	/// if the string starts with a comment character or a space
	    	if (this.fullLineText.trim().startsWith("!"))
	    			
	    	{
	    		//System.out.println("Event template line is a comment Line");
	    		this.skipped = true;
	    		return;
	    	}
	    }
        
	    /// check whether this is a command
	    int cmdPosition = fullLineText.toUpperCase().indexOf(" CMD ");
	    if ((cmdPosition >= 1) && (cmdPosition <= 15)) 
	    {
	    	/// mark the micro-event as a command
	    	this.setMicIsCommand(true);
		    	
	    	/// look for the command source keyword
	    	int sourcePosition = this.fullLineText.toUpperCase().indexOf(" SRC=");
	    	if (sourcePosition == -1)
	    		sourcePosition = this.fullLineText.toUpperCase().indexOf("	SRC=");
        
	    	/// if the command source keyword is found
	    	if (sourcePosition != -1)
	    	{
	    		/// parse out the command source (assumed to be a 3 character source)
	    		String micCmdSrc = this.fullLineText.substring(sourcePosition + 5 , sourcePosition + 8).toUpperCase();
	    		this.setMicCmdSource(micCmdSrc);
	    	}
	    	/// else the command is real-time
	    	else 
	    	{
	    		this.setMicCmdSource("RLT");
	    	}
	    }
        
	    // this micro-event is a directive
	    else
	    {
	    	this.setMicIsCommand(false);
	    	this.setMicCmdSource("RLT");
	    }

	    if (isSymbolReplace)
	    {
	    	// parse variable symbols
		    parseVariableSymbols();
	    }
	}
	  
	private void parseVariableSymbols()
	{

		String subText = this.fullLineText;
	  	String symbolName="";
	  	int start = -1;
	  	int end = -1;
	  	int lTrimLen = 0;
	  	Matcher matcher = null;
	  	int endOfLine = this.fullLineText.length() - 1;
	  	boolean run = true;
	  	while (run)
	  	{
	  		// find first occurrance of $ or ?
	        matcher = startPattern.matcher(subText);
	        /// if there is a symbol
	        if (matcher.find()) 
	        {
	        	start =  matcher.start();
                
                // start at end of line (not acceptable)
                
                // advance the subtext to start of symbol
                subText = subText.substring(start);
	        	// left trim length
                lTrimLen += start; 
                // symbol name
	        	symbolName = subText.toUpperCase();
                /// mark the end of the symbol 
                matcher = endPattern.matcher(subText);
                if (matcher.find())
                {
                	end = matcher.start();
                    // more subtext, advance for next symbol
                	if (end < endOfLine)
                    {
                		subText = subText.substring(end + 1);
                        lTrimLen += end + 1; 
                        symbolName=symbolName.substring(0,end);
                    }
                    // no more subtext, stop run
                    else
                    {   
                    	run = false;
                        lTrimLen += end + 1; 
                    }
                }
                // last symbol extends through the end of the string
                else 
                {
                	end = subText.length();
	        		lTrimLen = endOfLine + 2;
                	run = false;
                }
                // make a new variable symbol
                VariableSymbol s = new VariableSymbol(symbolName, lTrimLen - end -1 , lTrimLen - 2);
                if (this.variableSymbolType.get(symbolName) == null)
                {
                	// parameter (default symbol type to 100)
                    s.setSymbolType(100);
                }
                else
                {
                	// contact or load
                	s.setSymbolType(this.variableSymbolType.get(symbolName));
                }
                this.variableSymbols.add(s);
            }
            // no more symbol
            else
            {
                run = false;
            }
	  	} // while
	}
	
	/// method to filter the micro-event based on command type
	public boolean isSelected(List<String> sourceVector)
	{
		/// if the micro-event is a command and it has not already been put
		/// in the not-selected list (happens when optional parameter not supplied)
		if (this.micIsCommand && this.micSelected)
		{
			/// found the command source flag
			boolean found = false;

			int i = 0;

			/// while there are sources to check
			while ((i < sourceVector.size()) && !found) 
			{
				/// if our source matches a source in the source vector
				if (sourceVector.get(i).equalsIgnoreCase(this.micCmdSource))
				{
					/// indicate we found the source
					found = true;
				}
				i++;
			}

			/// return the found the command source flag
			return found;
		}

		/// else the micro-event is not a command and we are making a specific
		/// request for source
		else if (sourceVector.size() == 1)
		{
			/// if the source is real-time
			if (sourceVector.get(0) == "RLT")
			{
				return this.micSelected;
			}

			/// else we are searching for load commands and don't want
			/// any micro-events that are not commands
			else
			{
				return false;
			}
		}

		/// else we are looking for a variety of sources
		else
		{
			return this.micSelected;
		}
	}

	
}

