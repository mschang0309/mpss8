package mpss.pg;

import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mpss.pg.product.ScheduleProductBuilder;
import mpss.se.*;
import mpss.util.timeformat.TimeToString;


public class EventExpander
{
	private Timestamp previousTime;
	private EventTriggerTag eventTriggerTag;
	
	// owner schedule product builder
	private ScheduleProductBuilder builder;
	private ScheduleProductSession productSession;
	
	private EventTemplates eventTemplates;
	private ProcTemplates procTemplates;
	
	private List<ScheduledEvent> scheduledEvents;
	private Map<String, Integer> dataTypeCount = new HashMap<String, Integer>();
	private List<EventTemplateLine> etls = new ArrayList<EventTemplateLine>();
	
	private Pattern symbolPattern = Pattern.compile("[$?]");
	private Pattern triggerPattern = Pattern.compile("[- \\^ @ ~]");
	//private String templateDir=System.getProperty("dataFileDir") + System.getProperty("eventTemplates");
	boolean templateOpenTag = false;
	
	public List<ScheduledEvent> getScheduledEvents()
	{
		return this.scheduledEvents;
	}
	
	/// ctor
	public EventExpander(ScheduleProductBuilder builder)
	{
		  // owner builder
	    this.builder = builder;
	    this.productSession = builder.getProductSession();
	    
	    /*
	    this.substituteMap.put("$", new SubstituteVariable() {
                                     String getValue() { return "xyz" }
                                    });
	  

	    /// determine branch name to be used as a tag, if one has been specified for schedule
	    myBranchTag = "";
	    //String scheduleName = mySchedule.GetScheduleName();
	    //if (scheduleName != "Nominal Schedule") myBranchTag = "_" + scheduleName;
	    String scheduleName = "Nominal Schedule";
  	
		*/
      
	}
	
	public void open(String templateDir)
	{
		 this.eventTemplates = new EventTemplates(templateDir);
		 this.eventTemplates.load();
		 
		 templateOpenTag = true;
	}
	
	public List<EventTemplateLine> expand(List<ScheduledEvent> scheduledEvents, String source)
	{
		if(!templateOpenTag)
		{
			String satName = this.builder.getSatellite().getName();
			String templateDir=System.getProperty("dataFileDir") + satName + "\\" + System.getProperty("eventTemplates");
			open(templateDir);
		}
		
		/// build our source vector
		List<String> sources = new ArrayList<String>();
		if (source == "ALL")
		{
			sources.add("RLT");
			sources.add("MPQ");
			sources.add("TTQ");
		}
		else
		{
			sources.add(source);
		}
		
		etls.clear();
		for (ScheduledEvent se : scheduledEvents)
	    {
			//System.out.println("scheduledEvents: " + se.getEventMnemonic() + "[eid:"+ se.getExtent().getId() +"]");
		    // retrieve the event mnemonic and the event parameters
			String em = se.getEventMnemonic();
	        List<String> ep = se.getEventParms();	        
	        
	        System.out.println("em : " + em);
	        
	        // save a second copy of the event mnemonic for a later check
		    String emPbk = em;
//		    this.previousTime=null;  
		    /// if the macro event is a real-time or miscellaneous procedure
		    if (em.equalsIgnoreCase("MISC_PROC") || em.equalsIgnoreCase("ORB_MAN")|| em.equalsIgnoreCase("AIP_PROC"))
		    {
		    	try
		    	{
					/// instantiate a PgScheduleProc
		    		this.procTemplates = new ProcTemplates(ep.get(0));
		    		this.procTemplates.load();
					/// get the micro events from PgScheduleProc
					List<EventTemplateLine> procEvents = procTemplates.getProcEvents();

					/// for each retrieved micro-event
					
					for (EventTemplateLine etl : procEvents)
					{
						// compute time of micro event
						etl = (EventTemplateLine) etl.clone();
						etl.setExpandedText(etl.getFullLineText());
						etl = ParseTimeTrigger (etl, se);
						etl.setScheduledEvent(se);

						/// filter out micro-events that don't match the requested source
						if (etl.isSelected(sources)) 
						{
							this.etls.add(etl);
							System.out.println("[" + etl.getEventTriggerTag().getTriggerTime() + "]" + etl.getExpandedText()+",Comment="+etl.getMicEventComment());
						}
						else
						{
							//System.out.println("[" + etl.getEventTriggerTag().getTriggerTime() + "]" + etl.getExpandedText());
						}
						
						//System.out.println("MISC_PROC/ORB_MAN: " + etl.isSelected(sources) + "//"+ etl.getExpandedText());
					}
				}
		    	catch (Exception e) {//Catch exception if any
		    		System.err.println("EventExpander Error 1: " + e.getMessage());
		    	}
		    }
		    // else if the macro event is a load event
		    else if (em.contains("_LOAD"))
		    {
		    	try
				{
		    		
		    		// get the load file name
		    		String loadFileName = "";
					String loadSubType = ep.get(0);

					if (loadSubType.equalsIgnoreCase("MPQ"))
					{
						loadFileName = this.builder.getMPQName();
					}
					else
					{
						loadFileName = this.builder.getTTQName();
					}
					
					List<EventTemplateLine>  templates = this.eventTemplates.getTemplate(em);
					// apply value to parameters
					for (EventTemplateLine etl : templates)
					{
						etl = (EventTemplateLine) etl.clone();
						etl.setExpandedText(substitute(etl, loadFileName, se));
						etl = ParseTimeTrigger (etl, se);
						etl.setScheduledEvent(se);
						
						/// if the micro-event is for the requested source
						if (etl.isSelected(sources)) 
						{
							/// keep it
							this.etls.add(etl);
							System.out.println("[" + etl.getEventTriggerTag().getTriggerTime() + "]" + etl.getExpandedText()+",Comment="+etl.getMicEventComment());
						}
						else
						{
							//System.out.println("[" + etl.getEventTriggerTag().getTriggerTime() + "]" + etl.getExpandedText());
						}
						
						//System.out.println("LOAD: " + etl.isSelected(sources) + "//"+ etl.getExpandedText());
					}
				}
		    	catch (Exception e) {//Catch exception if any
		    		System.err.println("EventExpander Error 2: " + e.getMessage());
		    	}
		    }
		    /// ignore MON (monitor events)
		    else if (!em.equalsIgnoreCase("MON"))							
		    {
				String myKey = "NULL";
				if (em.equalsIgnoreCase("ACQ_S") || em.equalsIgnoreCase("ACQ_E"))
				{
					/// reset playback data type counters
					if (em.equalsIgnoreCase("ACQ_S"))						
					{
						this.dataTypeCount.put("PN", 0);	/// panchromatic
						this.dataTypeCount.put("MS", 0);	/// multi-spectral
						this.dataTypeCount.put("PM", 0);	/// pan-ms
					}
					//System.out.println("em : " + em);
				}

				/// if this is a site specific macro-event, check for a site specific template
				else if (se.getContact()!= null)
				{
					/// append site name to event mnemonic
					myKey = em + "_" + se.getSite().getName();
				}
				
				List<EventTemplateLine>  templates = this.eventTemplates.getTemplate(myKey);
				if (templates != null)
				{
					/// site specific template has been found
					em = myKey;
				}
				else
				{
					templates = this.eventTemplates.getTemplate(em);
				}

				/// if this is a playback event, increment data type counter for specific type
				if (emPbk.equalsIgnoreCase("PBK"))
				{
					detPlaybackCounts(ep);
				}

				/// go thru all lines of event template for this event
				try
				{
					for (EventTemplateLine etl : templates)
					{
						etl = (EventTemplateLine) etl.clone();
						etl.setExpandedText(substitute(etl, "", se));
						etl = ParseTimeTrigger (etl, se);
						etl.setScheduledEvent(se);
						
						if (etl.isSelected(sources)) 
						{							
							etls.add(etl);
							System.out.println("[" + etl.getEventTriggerTag().getTriggerTime() + "]" + etl.getExpandedText());
						}
						else
						{
							//System.out.println("[" + etl.getEventTriggerTag().getTriggerTime() + "]" + etl.getExpandedText());
						}
						//System.out.println(etl.isSelected(sources) + "----"+ etl.getExpandedText());
					}
				}
				catch (Exception e) {//Catch exception if any
		    		System.err.println("EventExpander Error 3: mykey= " + myKey +"," + e.getMessage());
		    	}
		    }
			
	    }

		/// time sort the generated events
	   	Collections.sort(etls,
	   		new Comparator<EventTemplateLine>() 
	    		{
	    			public int compare(EventTemplateLine et1, EventTemplateLine et2) 
	    			{
	    				return et1.getEventTriggerTag().getTriggerTime().compareTo(et2.getEventTriggerTag().getTriggerTime());
	    			}
	    		}
	    	);
	   	
//	   	for (EventTemplateLine ee : this.etls)
//	    {
//	   		System.out.println(ee.getExpandedText());
//	    }
	   	
	   	return etls;
	   	
	   	
	}
	
	
	
	// parser $xxx
	private String substitute(EventTemplateLine etl, String loadFileName, ScheduledEvent se)
	{
		String expandedString = "";
		int fromPos = 0;
		int toPos = -1;
      
      // replace the parameter placeholders in the micro-events
		for (VariableSymbol vs : etl.getVariableSymbols())
		{
			String paramValue = "";
			toPos = vs.getStartPos();
			switch (vs.getSymbolType())
 	        {
 	        	case 1: // $LOADFILE
 	        		paramValue = loadFileName;
 	        		break;
 	        		
 	        	case 2: // $LOAD_START
 	        		if(this.productSession.getMPQSelected() || this.productSession.getMPQSelected())
 	        		{
 	        			String startTime = TimeToString.AsLoadTime(this.productSession.getLoadTimeRange().first);
 	 	        		paramValue = startTime;
 	 	        		break;
 	        		}

 	        	case 3: // $LOAD_STOP
 	        		if(this.productSession.getMPQSelected() || this.productSession.getMPQSelected())
 	        		{
 	        			String stopTime = TimeToString.AsLoadTime(this.productSession.getLoadTimeRange().second);
 	        			paramValue = stopTime;
 	        			break;
 	        		}
                
 	        	case 4: // $OPS 	        		
 	        		paramValue = se.getSatellite().getName();
 	        		break;
 	        		
                case 5: // $REV
                	// 5 characters, 0 filled on the left. (nnnnn)
                	paramValue = String.format("%05d", se.getRev().getRevno());
                    break;
                case 6: // $SITE
                	paramValue = se.getSite().getName();
                	break;
                    
                case 7: // $PASS_END_TIME
                	paramValue = TimeToString.AsEpochTime(se.getContact().getFade());
                	break;
                    
                case 8: // $SKIP_TAG //"SKIP%j%H%M%S"
                	paramValue = String.format("SKIP%tj", se.getContact().getFade()) + String.format("%tT", se.getContact().getFade()).replace(":", "");
                    break;
                    
                case 9: // $M_EVENT_DUR
                	int dur = se.getActivity().getDuration();
                	int h = dur/3600;
                	int m = (dur - h * 3600) / 60;
                	int s = dur - h * 3600 - m * 60;
                	paramValue = String.format("%1$02d", h) + ":" + String.format("%1$02d", m) + ":" + String.format("%1$02d", s) + ".000";
                	//paramValue = "00:00:00.000";
                    break;
                
                default:// method to replace numeric parameters with their values
                	String symbol = vs.getSymbolName();
                	int parameterIndex;
            		// if it is a mandatory parameter
            		if (symbol.substring(0, 1).equals("$"))
            		{
            			// derive the parameter index
            			String param = symbol.substring(1);
            			parameterIndex = Integer.parseInt(param) - 1 ;

            			/// if the parameter value is available
            			if (parameterIndex < se.getEventParms().size())
            			{
            				/// assign it to value
            				paramValue = se.getEventParms().get(parameterIndex);
            			}

            			/// else the parameter isn't available
            			else
            			{
            				/// assign a default value to value
            				paramValue = "N/A";
            			}
            		} 

            		/// else it is some other kind of parameter
            		else
            		{
            			/// if it is an optional parameter
            			if (symbol.substring(0, 1).equals("?")) 
            			{
            				/// derive the parameter index
            				String param = symbol.substring(1);
            				parameterIndex = Integer.parseInt(param) - 1 ;
            				String tempParamValue="";

            				/// if the parameter value is available
            				if (parameterIndex < se.getEventParms().size())
            				{
            					/// assign it to value
            					tempParamValue = se.getEventParms().get(parameterIndex);
            				}

            				/// if value is defined
            				if ((tempParamValue.length() != 0) && (!tempParamValue.equals("%")))
            				{
            					/// replace the parameter with value
            					paramValue = tempParamValue;
            				}

            				/// else the parameter value wasn't specified
            				else
            				{
            					/// mark the micro-event to be left out
            					etl.setMicSelected(false);
            				}
            			}
            		}
                	break;
 	        }
 	        expandedString += etl.getFullLineText().substring(fromPos, toPos);
 	        expandedString += paramValue;
 	        // update next copy from index
 	        fromPos = vs.getEndPos() + 1;
 	    }
		if (fromPos <= etl.getFullLineText().length())
			expandedString += etl.getFullLineText().substring(fromPos);
		return expandedString;
	}

	
	public EventTemplateLine ParseTimeTrigger (EventTemplateLine etl, ScheduledEvent se)
	{
		String expandedText = etl.getExpandedText();
		/// instantiate a PgEventTriggerTag
		etl.setMicHasEvent(true);
		eventTriggerTag = new EventTriggerTag(expandedText);
		Matcher matcher = null;
        matcher = symbolPattern.matcher(expandedText);
        /// find first occurrance of $ or ?
		/// if the substitutions are complete
        if (!matcher.find()) 
        {
        	
			/// set start of event text as end of time trigger
        	matcher = triggerPattern.matcher(expandedText.substring(3));
        	if (matcher.find())
        	{
        		if (expandedText.indexOf("CMD") != -1)
        		{
        			String tempCommentText = expandedText.substring(expandedText.indexOf("CMD") + 4).trim();
        			int endPoint = tempCommentText.indexOf(" ");
        			if (endPoint == -1)
        				endPoint = tempCommentText.indexOf("	");
//        			System.out.println("endPoint:" + endPoint);
        			if (endPoint != -1)
        			{
        				etl.setLoadComment(tempCommentText.substring(0, endPoint));
//        				System.out.println("LoadComment:" + tempCommentText.substring(0, endPoint));
        				etl.setLoadText(tempCommentText.substring(endPoint).trim());
//        				System.out.println("LoadText:" + tempCommentText.substring(endPoint).trim());
        			}
        		}
        		
        		int eventBegin = matcher.start() + 5;

    			/// set end of event text as start of comment
    			int eventEnd = expandedText.indexOf("#", 15);

    			/// if there is a comment in the event
    			if (eventEnd != -1) 
    			{
    				/// save the event text and event comment
    				/// trim trailing blanks in event text
    				etl.setMicEventText(expandedText.substring(eventBegin, eventEnd).trim());
    				etl.setMicEventComment(expandedText.substring(eventEnd));
    			}

    			/// else there is no comment
    			else 
    			{
    				/// save the event text and default the comment
    				/// trim trailing blanks in event text
    				etl.setMicEventText(expandedText.substring(eventBegin).trim());
    				etl.setMicEventComment("");
    			}

    			/// if the micro-event is a command
    			if (etl.getMicIsCommand() && etl.getMicSelected())
    			{
    				
    			}
        	}
        	
		}

        
        Timestamp losTime=null;
        if(se.getExtent()!=null){
        	losTime=se.getExtent().getEndtime();        	
        }
        
        
		/// if the macro-event is an acquisition start event
		if (se.getEventMnemonic().equalsIgnoreCase("ACQ_S"))
		{
			///add the acquisition biased rise to the micro-event delta time
			eventTriggerTag = eventTriggerTag.Parse(se.getContact().getRise(),this.previousTime,losTime);
//			System.out.println("ACQ_S event Time:" + se.getContact().getRise().toString());
		}

		/// else if the macro-event is an acquisition end event
		else if (se.getEventMnemonic().equalsIgnoreCase("ACQ_E"))
		{
			/// add the acquisition biased fade to the micro-event delta time
			eventTriggerTag = eventTriggerTag.Parse(se.getContact().getFade(),this.previousTime,losTime);			
//			System.out.println("ACQ_E event Time:" + se.getContact().getFade().toString());
		}
		
		// Extent start time + pbk setup delay (1 sec)
		else if (se.getEventMnemonic().equalsIgnoreCase("RSI_PBK"))
		{
			Calendar cal = Calendar.getInstance();
	        cal.setTimeInMillis(se.getExtent().getStarttime().getTime());
	        //cal.add(Calendar.SECOND, 1);
	        cal.add(Calendar.SECOND, se.getSatelliteps().getRsipbsetupdelay());
	        Timestamp startTime=new Timestamp(cal.getTimeInMillis());
			eventTriggerTag = eventTriggerTag.Parse(startTime,this.previousTime,losTime);
		}

		/// Activity start time
		else
		{	/// add the Event Time to the micro-event delta time
			eventTriggerTag = eventTriggerTag.Parse(se.getEventStartTime(),this.previousTime,losTime);
		}
		
		this.previousTime=eventTriggerTag.getPreviousTime();
/*		String formatTime = String.format("%tY/", eventTriggerTag.getTriggerTime()) +
			String.format("%tj/", eventTriggerTag.getTriggerTime()) +
			String.format("%tT", eventTriggerTag.getTriggerTime()) + 
			String.format(".%tL", eventTriggerTag.getTriggerTime());
		etl.setExpandedText(formatTime + " #  " + etl.getExpandedText().substring(13));*/
		
		etl.setEventTriggerTag(eventTriggerTag);
		return etl;
	}
	
	
	/// method to increment the number of playbacks in current contact
	private void detPlaybackCounts(List<String> eventParms)
	{
		/// data type is in the first parameter
		String dataType;

		/// if data type is available
		for (int paramIndex=0; paramIndex < eventParms.size(); paramIndex++)
		{
			/// retrieve the data type
			dataType = eventParms.get(paramIndex);

			/// if the data type is valid
			if (dataType.equalsIgnoreCase("PN") || dataType.equalsIgnoreCase("MS") || dataType.equalsIgnoreCase("PM"))
			{
				/// increment the number of playbacks for this type
				this.dataTypeCount.put(dataType, this.dataTypeCount.get(dataType) + 1);
	
			}
		}
	}
	
	public List<EventTemplateLine> getEventTemplateLines(){
		return this.etls;
	}
	
	public void setEventTriggerTag(EventTriggerTag eventTriggerTag)
	{
		this.eventTriggerTag = eventTriggerTag;  
	}

	public void setPreviousTime(Timestamp previousTime)
	{
		this.previousTime = previousTime;  
	}
}