package mpss.pg.product;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import mpss.configFactory;
import mpss.common.dao.ActivityDao;
import mpss.common.dao.AipcommandDao;
import mpss.common.dao.ExtentDao;
import mpss.common.dao.PsDao;
import mpss.common.dao.RevDao;
import mpss.common.dao.RsirptDao;
import mpss.common.dao.SatelliteDao;
import mpss.common.dao.SatellitepDao;
import mpss.common.dao.SessionDao;
import mpss.common.dao.SessionSectionDao;
import mpss.common.jpa.Rsirpt;
import mpss.common.jpa.Satellite;
import mpss.common.jpa.Session;
import mpss.pg.ScheduleProductSession;
import mpss.se.ScheduledEvent;
import mpss.util.timeformat.TimeRange;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScheduleProductBuilderBase implements ScheduleProductBuilder {

	@Autowired
	SessionDao sessionDao;

	@Autowired
	SatelliteDao satelliteDao;

	@Autowired
	PsDao psDao;

	@Autowired
	ExtentDao extentDao;

	@Autowired
	ActivityDao activityDao;

	@Autowired
	SatellitepDao satellitepDao;

	@Autowired
	RsirptDao rsirptDao;

	@Autowired
	SessionSectionDao ssDao;

	@Autowired
	RevDao revDao;

	@Autowired
	AipcommandDao aipcmdDao;
	
	protected Logger logger = Logger.getLogger(configFactory.getLogName());

	protected List<ScheduledEvent> macros;
	//protected Map<String, String> aipCmdMap = new HashMap<String, String>();
	protected Rsirpt rsirpt;
	protected Satellite satellite;
	protected ScheduleProductSession spSession;
	protected String mpqFileName = "UNKNOWN";
	protected String ttqFileName = "UNKNOWN";
	protected String logFileName;

	protected String branchTag;
	protected String productId;
	protected String productSessionName;
	protected String SatelliteName;
	protected TimeRange validRange;

	protected List<Session> schSessions = new ArrayList<Session>();
	protected String fileDir;
	protected int isBranch = 0;
	protected int branchId = 0;
	protected Timestamp genTime;
	protected String returnMsg = null;
	protected FileOutputStream fileOutputStream;
	protected OutputStreamWriter outputStreamWriter;

	public ScheduleProductBuilderBase() {

	}

	// / method to generate the user requested schedule products - throws
	// PgInputScheduleNotValid
	// / if the input macro-event schedule has not been validated, throws the
	// caught exception
	// / if any other errors occur
	public boolean run(ScheduleProductSession session) {		
		return true;
	}	

	// / method to return a HTML text tag string denoting product completion
	// status
	public String printProductStatus(int errorCount, int warningCount) {
		if (errorCount > 0) {
			// / return the error count in red
			String errorCnt = "0" + Integer.toString(errorCount);
			return "<FONT COLOR=\"RED\">E"+ errorCnt.substring(errorCnt.length() - 2) + " </FONT>";
		} else if (warningCount > 0) {
			// / return the warning count in blue
			String warnCnt = "0" + Integer.toString(warningCount);
			return "<FONT COLOR=\"BLUE\">W"+ warnCnt.substring(warnCnt.length() - 2) + " </FONT>";
		} else {
			// / return a placeholder
			return "--- ";
		}
	}

	// / method to return a HTML text tag string denoting product Error message
	public String printErrorMsg(List<String> errorMsgs) {
		// / return the error message in red
		String errorString = "";
		int i = 1;
		for (String errMsg : errorMsgs) {
			errorString += "<FONT COLOR=\"RED\">E" + i + ": " + errMsg+ " </FONT><br>";
			i++;
		}
		return errorString;
	}

	// / method to return a HTML text tag string denoting product warning
	// message
	public String printWarningMsg(List<String> warningMsgs) {
		// / return the warning message in blue
		String warningString = "";
		int i = 1;
		for (String warnMsg : warningMsgs) {
			warningString += "<FONT COLOR=\"RED\">W" + i + ": " + warnMsg+ " </FONT><br>";
			i++;
		}
		return warningString;
	}

	public Rsirpt getRsirpt() {
		return this.rsirpt;
	}

	public ScheduleProductSession getProductSession() {
		return this.spSession;
	}

	public String getBranchTag() {
		return this.branchTag;
	}

	public String getMPQName() {
		return this.mpqFileName;
	}

	public String getTTQName() {
		return this.ttqFileName;
	}

	public String getLogName() {
		return this.logFileName;
	}

	public TimeRange getValidRange() {
		return this.validRange;
	}

	public String getProductSessionName() {
		return this.productSessionName;
	}

	public List<Session> getSchSessions() {
		return this.schSessions;
	}

	public String getProductId() {
		return this.productId;
	}

	public String getFileDir() {
		return this.fileDir;
	}

	public void setFileDir(String fileDir) {
		this.fileDir = fileDir;
	}

	public void setSatellite(Satellite satellite) {
		this.satellite = satellite;
	}

	public Satellite getSatellite() {
		return this.satellite;
	}

	public Satellite getSatellite(int scId) {
		if (this.satelliteDao == null) {
			System.err.println("satelliteDao is not injected");
			return null;
		}
		Satellite sat = this.satelliteDao.get((long) scId);
		return sat;
	}
	

	public List<ScheduledEvent> getScheduledEvents() {
		return this.macros;
	}
	
	public String getReturnMsg() {
		return this.returnMsg;
	}
	
	public void setReturnMsg(String msg) {
		this.returnMsg = msg;
	}

}
