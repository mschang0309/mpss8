package mpss.pg.product;

import java.sql.Timestamp;

import mpss.config;
import mpss.configFactory;
import mpss.pg.ScheduleProductSession;
import mpss.pg.product.ScheduleProductBuilder;
import mpss.util.timeformat.RangeUtil;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class ScheduleProductConsole 
{
	public static void main(String args[])
	{
		  
		try {
			System.setProperty("satName", "FORMOSAT5");
			config.load();  
			// Spring application context
			ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf",configFactory.getSatelliteName()}));
			ScheduleProductBuilder pgBuilder = (ScheduleProductBuilder)context.getBean(configFactory.getProductBuilder());	  
			if (pgBuilder == null) {
		    	System.err.println("load ScheduleProductBuilder bean failed");
		    	return;
		    }
		    
		    //Product Generation Window Inputs
		    ScheduleProductSession spSession = new ScheduleProductSession();
		    spSession.setIntegratedRptSelected(true);
		    spSession.setMPQSelected(true);
		    spSession.setPassPlanSelected(true);
		    spSession.setRasSelected(true);
		    spSession.setRasGroundSelected(true);
		    spSession.setTTQSelected(true);
		    spSession.setLoadTimeRange(RangeUtil.startTimeAppendHourMinu(Timestamp.valueOf("2018-02-26 00:00:00"), 12, 0));
			spSession.setProductTimeRange(RangeUtil.startTimeAppendHourMinu(Timestamp.valueOf("2018-02-27 00:00:00"),12, 0));
			spSession.setSatelliteId(1);
			spSession.setIsNominal(true);
			spSession.setLoadDurHour(12);
			spSession.setLoadDurMinute(0);
			spSession.setProdDurHour(12);
			spSession.setProdDurMinute(0);			
			
			pgBuilder.setFileDir(System.getProperty("reportFileDir"));
			if(pgBuilder.run(spSession))
			{
				System.out.println("PG Builder complete");
			}
			else
			{
				String msg = (pgBuilder.getReturnMsg() == null) ? "PG Builder failed" : pgBuilder.getReturnMsg();
				System.out.println(msg);
				return;
			}
    	
		} catch (Exception ex) {
			System.err.println(ex.getMessage());
		}
    
	}
}
