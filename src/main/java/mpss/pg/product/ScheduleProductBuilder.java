package mpss.pg.product;

import java.util.List;
import mpss.common.jpa.Rsirpt;
import mpss.common.jpa.Satellite;
import mpss.common.jpa.Session;
import mpss.pg.ScheduleProductSession;
import mpss.se.ScheduledEvent;
import mpss.util.timeformat.TimeRange;

import org.springframework.stereotype.Service;

@Service
public interface ScheduleProductBuilder {
    
    
	public boolean run (ScheduleProductSession session);
	
	/// method to return a HTML text tag string denoting product completion status
	public String printProductStatus (int errorCount, int warningCount);
	/// method to return a HTML text tag string denoting product Error message
	
	public String printErrorMsg (List<String> errorMsgs);
	
	/// method to return a HTML text tag string denoting product warning message
	public String printWarningMsg (List<String> warningMsgs);
	
	public Rsirpt getRsirpt ();
	
	public ScheduleProductSession getProductSession ();
	
	public String getBranchTag ();

	public String getMPQName ();
	
	public String getTTQName ();
	
	public String getLogName ();
	
	public TimeRange getValidRange ();

	public String getProductSessionName ();
	
	public List<Session> getSchSessions ();
	
	public String getProductId ();
	
	public String getFileDir ();
	
	public void setFileDir (String fileDir);
	
	public void setSatellite(Satellite satellite);
	
	public Satellite getSatellite();
	
	public Satellite getSatellite(int scId);	
		
	public List<ScheduledEvent> getScheduledEvents();
	
	public String getReturnMsg();
	
	public void setReturnMsg(String msg);
}
