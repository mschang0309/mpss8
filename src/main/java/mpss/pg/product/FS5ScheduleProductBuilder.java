package mpss.pg.product;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import mpss.configFactory;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Rev;
import mpss.common.jpa.Rsirpt;
import mpss.common.jpa.Session;
import mpss.common.jpa.SessionSection;
import mpss.pg.DailySchedule;
import mpss.pg.EventTemplateLine;
import mpss.pg.ScheduleProductSession;
import mpss.pg.load.GroundPass;
import mpss.pg.load.MPQLoad;
import mpss.pg.load.TTQLoad;
import mpss.pg.passplan.PassPlan;
import mpss.se.ScheduledEvent;
import mpss.se.ScheduledEventGenerator;
import mpss.util.inspect.inspecter.Inspecter;
import mpss.util.timeformat.RangeUtil;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeToString;
import mpss.util.timeformat.TimeUtil;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component("FS5scheduleProductBuilder")
public class FS5ScheduleProductBuilder extends ScheduleProductBuilderBase {

	
	@SuppressWarnings("unused")
	public boolean run(ScheduleProductSession session) {

		DaoReset();
		this.spSession = session;
		int SatelliteId = spSession.getSatelliteId();
		setSatellite(getSatellite(SatelliteId));
		this.SatelliteName = satellite.getName();

		if (spSession.getRasSelected()) {			
			String filename = creatLogFile(this.spSession);

			// / set the default schedule pad
			int startPad = 3600;
			int stopPad = 3600;

			// / if we are generating loads
			if (spSession.getRasSelected()) {
				// / pad the schedule time range for the worst case
				startPad = 3600;
				stopPad = 3600;
			}

			// / try to generate the schedule products
			try {
				// / create GlTimeRange covering Product Time Range and Load
				// time range
				// this.validRange = spSession.getProductTimeRange();
				this.validRange = new TimeRange(spSession.getProductTimeRange().first,spSession.getProductTimeRange().second);

				if (spSession.getTTQSelected() || spSession.getMPQSelected()) {
					Timestamp startTime, endTime;
					if (spSession.getProductTimeRange().first.compareTo(spSession.getLoadTimeRange().first) < 0) {
						startTime = spSession.getProductTimeRange().first;
					} else {
						startTime = spSession.getLoadTimeRange().first;
					}
					if (spSession.getProductTimeRange().second.compareTo(spSession.getLoadTimeRange().second) > 0) {
						endTime = spSession.getProductTimeRange().second;
					} else {
						endTime = spSession.getLoadTimeRange().second;
					}
					this.validRange.closed(startTime, endTime);
				}

				this.validRange = RangeUtil.rangeAppendSec(validRange,-(startPad), stopPad);

				// use extent in time range find sessions , by jeff 20170911
				// find pg error
				schSessions.clear();
				List<Extent> extents = extentDao.getStartWithinTimeRange(validRange, isBranch);
				String actids = "";
				for (Extent ex : extents) {
					actids += ex.getActivityid() + ",";
				}
				actids = actids.substring(0, actids.length() - 1);

				List<Integer> sessionsids = activityDao.getGroupSessionbyActId(actids);
				for (Integer sessionid : sessionsids) {					
					Session tempsession = sessionDao.getSessionById(sessionid);
					if (tempsession == null	|| tempsession.getNeedsvalidation() == 1) {
						returnMsg = "No Validated Sessions :"+ tempsession.getName();
						outputStreamWriter.write("</body>\n</html>\n");
						outputStreamWriter.close();
						return false;
					} else {
						this.schSessions.add(tempsession);
					}
				}

				// Spring application context
				ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml",new String[] { "conf" }));
				ScheduledEventGenerator seGenerator = (ScheduledEventGenerator) context.getBean(configFactory.getEventGenerator());
				seGenerator.setSatelliteId(SatelliteId);
				// Generate ScheduledEvent
				macros = new ArrayList<ScheduledEvent>();
				// for (Session ss :schSessions)
				// {
				if (!seGenerator.generate(schSessions)) {
					logger.info("Scheduled Event Generator failed");
					returnMsg = "Scheduled Event Generator failed";
					return false;
				} else
					this.macros.addAll(seGenerator.getScheduledEvents());

				// }

				// Statistics report data
				rsirpt = new Rsirpt();
				genStatistic(filename);

				// / If the resolved acquisition schedule option is selected
				if (spSession.getRasSelected()) {
					genRASRS();
				}

				// / If the resolved acquisition schedule option is selected
				if (spSession.getRasGroundSelected()) {
					genGroundRASRS();
				}

				if (spSession.getTTQSelected() || spSession.getMPQSelected()) {

					List<EventTemplateLine> etls = new ArrayList<EventTemplateLine>();

					// / If the TTQ loads option is selected
					if (spSession.getTTQSelected()) {
						etls.addAll(genTTQLoads());
					}

					// / If the MPQ loads option is selected
					if (spSession.getMPQSelected()) {
						etls.addAll(genMPQLoads());
					}

					genALLLoads(etls);

				}

				// create Statistics report
				Rsirpt oldRsirpt = rsirptDao.getByName(filename.substring(1));
				if (oldRsirpt == null)
					rsirptDao.create(rsirpt);
				else {
					rsirptDao.delete(oldRsirpt);
					rsirptDao.create(rsirpt);
				}

				if (!spSession.getPassPlanSelected()) {
					// / close the log file
					outputStreamWriter.write("</body>\n</html>\n");
					outputStreamWriter.close();
				}
			}
			catch (Exception e) {				
				logger.info("FS5ScheduleProductBuilder.run() ex ="+e.getMessage());
				return false;
			}
		}

		// / If the Pass Plans option is selected
		if (spSession.getPassPlanSelected()) {
			if (outputStreamWriter == null) {
				String filename = creatLogFile(this.spSession);
			}
			genPassPlans();

			try {
				// / close the log file
				outputStreamWriter.write("</body>\n</html>\n");
				outputStreamWriter.close();
			} catch (Exception e) {				
				logger.info("FS5ScheduleProductBuilder.run(),pass plan gen ex ="+e.getMessage());
				return false;
			}
		}

		return true;
	}

	private void DaoReset() {
		sessionDao.reset();
		activityDao.reset();
		extentDao.reset();
		outputStreamWriter = null;
	}

	private String creatLogFile(ScheduleProductSession spSession) {
		// / set the product id from the user session
		this.productId = TimeToString.AsNameDate(spSession.getProductTimeRange().first)+ "_"+ TimeToString.AsNameDate(spSession.getProductTimeRange().second);

		// / if a branch name was specified in the user session
		this.branchTag = "";
		if (!spSession.getIsNominal()) {
			// / append it to the product id
			this.branchId = spSession.getBranchId();
			this.productSessionName = sessionDao.get((long) this.branchId).getName();
			this.branchTag = "_" + productSessionName;
			this.productId += branchTag;
			isBranch = 1;
		} else {
			this.productSessionName = "Nominal Schedule";
		}

		// / build the load file names (we have to do it here because this is
		// the only
		String filename = "";
		// if (spSession.getMPQSelected() || spSession.getTTQSelected())
		// {
		// / formate "_%Y%j%H%M%S.dat"
		filename = "_"+ TimeToString.AsNameTime(spSession.getLoadTimeRange().first)	+ "_"+ TimeToString.AsNameTime(spSession.getLoadTimeRange().second)+ this.branchTag + ".dat";
		// }
		if (spSession.getMPQSelected())
			this.mpqFileName = "mpq" + filename;
		if (spSession.getTTQSelected())
			this.ttqFileName = "ttq" + filename;

		// / build the log file name with directory path from the product id
		this.logFileName = fileDir + "Product_gen_reports\\log_" + productId+ ".html";

		// / open the log file for output
		File file = new File(this.logFileName);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (Exception e) {
				logger.info("PgSchProductBuilder Error: Log file can not be create: "+ logFileName);
			}
		}
		try {
			fileOutputStream = new FileOutputStream(this.logFileName);
			outputStreamWriter = new OutputStreamWriter(fileOutputStream);
		} catch (Exception e) {
			logger.info("PgSchProductBuilder Error: Log file can not be opened: "+ logFileName);
		}
		// / write the schedule product session inputs to the log file
		printLog();
		return filename;
	}

	// / method used to generate TTQ loads
	private void genALLLoads(List<EventTemplateLine> etls) {
		logger.info("Start All Load");
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		String reportName = fileDir + satellite.getName() + "\\MPQs\\"+ mpqFileName.replace("mpq", "all").replace("dat", "rpt");

		try {
			outputStreamWriter.write("\n--- "+ TimeToString.AsShortTime(currentTime)+ "  Creating ALL Load file & report for " + SatelliteName+ "\n");
		} catch (IOException e) {
			logger.info(e.getMessage());
		}

		// sort etls start
		List<EventTemplateLine> sortMicros = new ArrayList<EventTemplateLine>(etls);
		Collections.sort(sortMicros, new Comparator<EventTemplateLine>() {
			public int compare(EventTemplateLine et1, EventTemplateLine et2) {
				return et1.getEventTriggerTag().getTriggerTime().compareTo(et2.getEventTriggerTag().getTriggerTime());
			}
		});
		// sort etls end

		if (combineAllLoad(sortMicros, reportName)) {
			currentTime = new Timestamp(System.currentTimeMillis());
			try {
				// / Write a message to log indicating Report is created
				outputStreamWriter.write("--- "	+ TimeToString.AsShortTime(currentTime)	+ "  --- Created Report: " + "<a href=\"http://"
						+ System.getProperty("mqIP") + ":8080/"
						+ System.getProperty("web.path", "mpssweb")
						+ "/ProdGen/showRpt.jsp?filetype=ttq&filename="
						+ reportName + "\">" + reportName + "</a>\n");
			} catch (IOException e) {
				logger.info(e.getMessage());
			}
			logger.info("All Load complete");

			// check ALL Load
			LoadInspecter(sortMicros, outputStreamWriter);
		} else {
			logger.info("All Load failed");
		}
	}

	// / method used to generate MPQ loads
	private List<EventTemplateLine> genMPQLoads() {
		logger.info("Start MPQ Load");
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		List<EventTemplateLine> microEvents = new ArrayList<EventTemplateLine>();
		try {
			outputStreamWriter.write("\n--- "+ TimeToString.AsShortTime(currentTime)+ "  Creating MPQ file & report for " + SatelliteName+ "\n");
		} catch (IOException e) {
			logger.info(e.getMessage());
		}

		// Create an mpq file object for AOCS
		MPQLoad aocsLoad = new MPQLoad();
		if (aocsLoad.Load(this, "AOCS")) {
			microEvents.addAll(aocsLoad.getEventLines());
			currentTime = new Timestamp(System.currentTimeMillis());
			try {
				// / Write a message to log indicating Report is created
				outputStreamWriter.write(printProductStatus(aocsLoad.getErrorCount(), aocsLoad.getWarningCount())+ TimeToString.AsShortTime(currentTime)
						+ "  --- Created Report: "
						+ "<a href=\"http://"+ System.getProperty("mqIP")+ ":8080/"+ System.getProperty("web.path", "mpssweb")
						+ "/ProdGen/showRpt.jsp?filetype=aocs&filename="+ aocsLoad.getReportName()+ "\">"
						+ aocsLoad.getReportName() + "</a> ");

				// / if there were no errors - the load file was generated so
				// write
				// / a message to log indicating that it has been created
				if (aocsLoad.getErrorCount() == 0)
					outputStreamWriter.write("   ( "+ aocsLoad.getProductName() + " )\n");
				else {
					outputStreamWriter.write("   ( Load File not created )\n");
					outputStreamWriter.write(printErrorMsg(aocsLoad.getErrorMsg()) + "\n");
				}

				if ((aocsLoad.getWarningCount() > 0))
					outputStreamWriter.write(printWarningMsg(aocsLoad.getWarningMsg()) + "\n");

			} catch (IOException e) {
				logger.info(e.getMessage());
			}
			logger.info("MPQ Load(AOCS) complete");
		} else {
			logger.info("MPQ Load(AOCS) failed");
		}

		// Create an mpq file object for PAYLOAD
		MPQLoad pdmLoad = new MPQLoad();
		if (pdmLoad.Load(this, "PAYLOAD")) {
			microEvents.addAll(pdmLoad.getEventLines());
			currentTime = new Timestamp(System.currentTimeMillis());
			try {
				// / Write a message to log indicating Report is created
				outputStreamWriter.write(printProductStatus(pdmLoad.getErrorCount(), pdmLoad.getWarningCount())+ TimeToString.AsShortTime(currentTime)
						+ "  --- Created Report: "
						+ "<a href=\"http://"+ System.getProperty("mqIP")+ ":8080/"+ System.getProperty("web.path", "mpssweb")+ "/ProdGen/showRpt.jsp?filetype=pdm&filename="+ pdmLoad.getReportName()+ "\">"
						+ pdmLoad.getReportName() + "</a> ");
				if (pdmLoad.getErrorCount() == 0)
					outputStreamWriter.write("   ( " + pdmLoad.getProductName()	+ " )\n");
				else {
					outputStreamWriter.write("   ( Load File not created )\n");
					outputStreamWriter.write(printErrorMsg(pdmLoad.getErrorMsg()) + "\n");
				}

				if ((pdmLoad.getWarningCount() > 0))
					outputStreamWriter.write(printWarningMsg(pdmLoad.getWarningMsg()) + "\n");

			} catch (IOException e) {
				logger.info(e.getMessage());
			}
			logger.info("MPQ Load(PDM) complete");
		} else {
			logger.info("MPQ Load(PDM) failed");
		}

		MPQLoad aipLoad = new MPQLoad();
		if (aipLoad.Load(this, "AIP")) {
			microEvents.addAll(aipLoad.getEventLines());
			currentTime = new Timestamp(System.currentTimeMillis());
			try {
				// / Write a message to log indicating Report is created
				outputStreamWriter.write(printProductStatus(aipLoad.getErrorCount(), aipLoad.getWarningCount())+ TimeToString.AsShortTime(currentTime)
						+ "  --- Created Report: "
						+ "<a href=\"http://"+ System.getProperty("mqIP")+ ":8080/"+ System.getProperty("web.path", "mpssweb")+ "/ProdGen/showRpt.jsp?filetype=pdm&filename="+ aipLoad.getReportName()+ "\">"
						+ aipLoad.getReportName() + "</a> ");

				if ((aipLoad.getErrorCount() == 0)
						&& (aipLoad.getWarningCount() == 0))
					outputStreamWriter.write("   ( " + aipLoad.getProductName()+ " )\n");
				else
					outputStreamWriter.write("   ( Load File not created )\n");
			} catch (IOException e) {
				logger.info("ScheduleProductBuilder MPQLoad aip , ex="+ e.getMessage());
			}
			logger.info("MPQ Load(AIP) complete");
		} else {
			logger.info("MPQ Load(AIP) failed");
		}

		combineMPQLoad(aocsLoad, pdmLoad, aipLoad);
		return microEvents;
	}

	// / method used to generate TTQ loads
	private List<EventTemplateLine> genTTQLoads() {
		logger.info("Start TTQ Load");
		List<EventTemplateLine> microEvents = new ArrayList<EventTemplateLine>();
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		try {
			outputStreamWriter.write("\n--- "+ TimeToString.AsShortTime(currentTime)+ "  Creating TTQ file & report for " + SatelliteName+ "\n");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		TTQLoad ttqLoad = new TTQLoad();
		if (ttqLoad.Load(this)) {
			microEvents.addAll(ttqLoad.getEventLines());
			currentTime = new Timestamp(System.currentTimeMillis());
			try {
				// / Write a message to log indicating Report is created
				outputStreamWriter.write(printProductStatus(ttqLoad.getErrorCount(), ttqLoad.getWarningCount())+ TimeToString.AsShortTime(currentTime)+ "  --- Created Report: "
						+ "<a href=\"http://"
						+ System.getProperty("mqIP")+ ":8080/"+ System.getProperty("web.path", "mpssweb")+ "/ProdGen/showRpt.jsp?filetype=ttq&filename="+ ttqLoad.getReportName()+ "\">"
						+ ttqLoad.getReportName() + "</a> ");

				// / if there were no errors - the load file was generated so
				// write
				// / a message to log indicating that it has been created
				if (ttqLoad.getErrorCount() == 0)
					outputStreamWriter.write("   ( " + ttqLoad.getProductName()	+ " )\n");
				else {
					outputStreamWriter.write("   ( Load File not created )\n");
					outputStreamWriter.write(printErrorMsg(ttqLoad.getErrorMsg()) + "\n");
				}

				if ((ttqLoad.getWarningCount() > 0))
					outputStreamWriter.write(printWarningMsg(ttqLoad.getWarningMsg()) + "\n");

			} catch (IOException e) {
				logger.info(e.getMessage());				
			}
			logger.info("TTQ Load complete");
		} else {
			logger.info("TTQ Load failed");
		}

		return microEvents;
	}

	// / method used to generate daily schedules
	private void genRASRS() {
		logger.info("Start generate RASRS");
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		try {
			outputStreamWriter.write("\n--- "+ TimeToString.AsShortTime(currentTime)+ "  Creating Resolved Acquisition Schedule & report for "+ SatelliteName + "\n");
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}

		// / generate the resolved acquisition schedule file
		DailySchedule rasr = new DailySchedule();
		if (rasr.Load(this)) {
			currentTime = new Timestamp(System.currentTimeMillis());
			try {
				// / get the number of warnings and erros formated for html file
				outputStreamWriter.write(printProductStatus(rasr.getErrorCount(), rasr.getWarningCount())+ TimeToString.AsShortTime(currentTime)+ "  --- Created Resolved Acquisition Schedule file "
						+ "<a href=\"http://"+ System.getProperty("mqIP")+ ":8080/"+ System.getProperty("web.path", "mpssweb")+ "/ProdGen/showRpt.jsp?filetype=schedule&filename="+ rasr.getProductName()+ "\">"
						+ rasr.getProductName()	+ "</a> \n");

				// / write a link to the generated daily schedule report to the
				// log file
				outputStreamWriter.write("                     "+ "  --- Created Resolved Acquisition Schedule Report File "
								+ "<a href=\"http://"+ System.getProperty("mqIP")+ ":8080/"	+ System.getProperty("web.path", "mpssweb")	+ "/ProdGen/showRpt.jsp?filetype=schedule&filename="
								+ rasr.getReportName() + "\">"+ rasr.getReportName() + "</a> \n");
			} catch (IOException e) {
				logger.info(e.getMessage());
			}
			logger.info("Resolved Acquisition Schedule Report complete");
		} else {
			logger.info("Resolved Acquisition Schedule Report failed");
		}
	}

	// / method used to generate daily schedules
	private void genGroundRASRS() {
		logger.info("Start generate GroundRASRS");
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		try {
			outputStreamWriter.write("\n--- "+ TimeToString.AsShortTime(currentTime)+ "  Creating Ground Pass Init & Report for "+ SatelliteName + "\n");
		} catch (IOException e) {
			logger.info(e.getMessage());
		}

		// / generate the resolved acquisition schedule file
		GroundPass grasr = new GroundPass();
		if (grasr.Load(this)) {
			currentTime = new Timestamp(System.currentTimeMillis());
			try {
				// / get the number of warnings and erros formated for html file
				outputStreamWriter.write(printProductStatus(grasr.getErrorCount(),grasr.getWarningCount())+ TimeToString.AsShortTime(currentTime)+ "  --- Created Ground Pass Init File "
								+ "<a href=\"http://"+ System.getProperty("mqIP")+ ":8080/"	+ System.getProperty("web.path", "mpssweb")	+ "/ProdGen/showRpt.jsp?filetype=groundschedule&filename="
								+ grasr.getProductName()+ "\">"+ grasr.getProductName() + "</a> \n");

				// / write a link to the generated daily schedule report to the
				// log file
				outputStreamWriter.write("                     "+ "  --- Created Ground Pass Init Report File "+ "<a href=\"http://"+ System.getProperty("mqIP")+ ":8080/"+ System.getProperty("web.path", "mpssweb")
								+ "/ProdGen/showRpt.jsp?filetype=groundschedule&filename="+ grasr.getReportName() + "\">"+ grasr.getReportName() + "</a> \n");
			} catch (IOException e) {
				logger.info(e.getMessage());
			}
			logger.info("Ground Pass complete");
		} else {
			logger.info("Ground Pass failed");
		}
	}

	// / method used to generate pass plans
	private void genPassPlans() {
		logger.info("Start generate PassPlans");
		try {
			outputStreamWriter.write("Creating Pass Plans...\n\n");
			outputStreamWriter.write("Generated the following files:\n\n");
		} catch (IOException e) {
			logger.info(e.getMessage());
		}

		ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml",new String[] { "conf" }));
		// PassPlan pp = (PassPlan)context.getBean(PassPlan.class);
		PassPlan pp = (PassPlan) context.getBean(configFactory.getPassPlanName());
		if (pp.Load(this)) {
			try {
				List<String> fNames = pp.getFileNames();
				for (String st : fNames) {
					if (st.indexOf(".rpt") > 0) {
						outputStreamWriter.write("Creating Pass Plans for James ...\n");
						outputStreamWriter.write("<a href=\"http://"+ System.getProperty("mqIP")+ ":8080/"+ System.getProperty("web.path","mpssweb")+ "/ProdGen/showRpt.jsp?filetype=passplan&filename="+ st + "\">" + st + "</a> \n");
					} else if (st.indexOf("GPS") > 0) {
						outputStreamWriter.write("\n\n");
						outputStreamWriter.write("<font color=red >Creating GPS prc ... , need to Print to SA </font>\n");
						outputStreamWriter.write("<a href=\"http://"+ System.getProperty("mqIP") + ":8080/"+ System.getProperty("web.path", "mpssweb")
								+ "/ProdGen/showRpt.jsp?filetype=ttq&filename="+ st + "\">" + st + "</a> \n");
						outputStreamWriter.write("\n");
					} else if (st.indexOf(".prc") > 0) {
						outputStreamWriter.write("\n\n");
						outputStreamWriter.write("Creating Rpp for OC ...\n");
						outputStreamWriter.write("<a href=\"http://"+ System.getProperty("mqIP")+ ":8080/"+ System.getProperty("web.path","mpssweb")
										+ "/ProdGen/showRpt.jsp?filetype=passplan&filename="+ st + "\">" + st + "</a> \n");
						outputStreamWriter.write("\n");
					} else {
						outputStreamWriter.write("<a href=\"http://"+ System.getProperty("mqIP")+ ":8080/"+ System.getProperty("web.path","mpssweb")
										+ "/ProdGen/showRpt.jsp?filetype=passplan&filename="+ st + "\">" + st + "</a> \n");
					}
				}

			} catch (IOException e) {
				logger.info(e.getMessage());
			}
			logger.info("PassPlan Load complete");
		} else {
			logger.info("PassPlan Load failed");
		}

	}

	@SuppressWarnings("deprecation")
	private void genStatistic(String filename) {
		Timestamp pStartTime = spSession.getProductTimeRange().first;
		Timestamp sectionTime = Timestamp.valueOf("1970-01-01 "	+ TimeToString.AsTime(pStartTime));
		Timestamp sectionDay = Timestamp.valueOf(TimeToString.AsLongDashDate(pStartTime) + " 00:00:00");
		
		Timestamp orbitStart = Timestamp.valueOf(TimeToString.AsLongDashDate(TimeUtil.timeAppendDay(pStartTime, -1))+ " 23:00:00");
		Timestamp orbitEnd = Timestamp.valueOf(TimeToString.AsLongDashDate(TimeUtil.timeAppendDay(pStartTime, 1))+ " 02:00:00");

		try {
			SessionSection section = ssDao.getSectionbyTime(sectionTime);
			if (section == null) {
				sectionTime.setDate(2);
				section = ssDao.getSectionbyTime(sectionTime);				
				orbitStart = Timestamp.valueOf(TimeToString.AsLongDashDate(TimeUtil.timeAppendDay(pStartTime, -2))+ " 23:00:00");
				orbitEnd = Timestamp.valueOf(TimeToString.AsLongDashDate(pStartTime) + " 2:00:00");
				rsirpt.setSectionname("A");
			}
			// sessionday
			rsirpt.setSessionday(sectionDay);
			// sectionname
			if (section != null)
				rsirpt.setSectionname(section.getSectionname());
			// orbitstart,orbitend
			// System.out.println("orbitStart:" + orbitStart);
			Rev revS = revDao.getRevbyTime(orbitStart, branchId);
			if (revS != null)
				rsirpt.setOrbitstart(revS.getRevno());
			// System.out.println("orbitEnd:" + orbitEnd);
			Rev revE = revDao.getRevbyTime(orbitEnd, branchId);
			if (revE != null)
				rsirpt.setOrbitend(revE.getRevno());
			if (revS == null && revE != null) {
				int oeS = (revE.getRevno() - 15) < 0 ? 0: (revE.getRevno() - 15);
				rsirpt.setOrbitstart(oeS);
			}
			if (revE == null && revS != null) {
				rsirpt.setOrbitend(revS.getRevno() + 15);
			}
			// filename
			rsirpt.setFilename(filename.substring(1));
			// gendate
			rsirpt.setGendate(genTime);

			rsirpt.setAocs(0);
			rsirpt.setMan(0);
			rsirpt.setPdm(0);
			rsirpt.setRsi(0);
			rsirpt.setDdt(0);
			rsirpt.setRsidel(0);
			rsirpt.setPbk(0);
			rsirpt.setTtq(0);
			rsirpt.setTx(0);
			rsirpt.setGohome(0);

		} catch (Exception e) {
			logger.info("Create Statistics report exception:"+e.getMessage());
		}
	}

	// /Combine MPQ load from aocs,pdm,aip
	private void combineMPQLoad(MPQLoad f1, MPQLoad f2, MPQLoad f3) {
		String fullCombLoadFile = f1.getLoadPath() + mpqFileName;
		String backupLoadName = fullCombLoadFile + ".bak";
		String aocsLoadFile = f1.getProductName();
		String pdmLoadFile = f2.getProductName();
		String aipLoadFile = f3.getProductName();
		List<String> loadNames = new ArrayList<String>();
		loadNames.add(0, aocsLoadFile);
		loadNames.add(1, pdmLoadFile);
		loadNames.add(2, aipLoadFile);

		// / create combined load file (if possible and write message to log
		// / indicating the contents of the combined load file
		File combLoadFile = new File(fullCombLoadFile);
		if (combLoadFile.exists()) {
			// / delete back file and move the combined file to .bak if it
			// already exists
			File backupLoad = new File(backupLoadName);
			if (backupLoad.exists())
				backupLoad.delete();

			combLoadFile.renameTo(new File(backupLoadName));
		}

		// /creat CombLoadFile
		try {
			combLoadFile.createNewFile();
			FileOutputStream out = new FileOutputStream(fullCombLoadFile, true);
			byte[] buf = new byte[1024];
			int len;

			for (String name : loadNames) {
				int size = 0;
				File loadFile = new File(name);
				if (loadFile.exists()) {
					FileInputStream fInstream = new FileInputStream(name);
					DataInputStream in = new DataInputStream(fInstream);

					while ((len = in.read(buf)) > 0) {
						out.write(buf, 0, len);
						size += len;
					}
					in.close();
					logger.info(name + "=========>" + size);
				}
			}
			out.close();
			if (combLoadFile.exists()) {
				outputStreamWriter.write("                     "+ "  --- Created combined MPQ file: " + mpqFileName	+ "\n");
				logger.info("MPQ Load complete");
			} else {
				outputStreamWriter.write("                     "+ "  --- Error creating combined MPQ file\n");
				logger.info("MPQ Load failed");
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
	}

	// /Combine MPQ load,TTQ load
	private boolean combineAllLoad(List<EventTemplateLine> etls,String reportName) {

		// / get the current time
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());

		File file = new File(reportName);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(reportName);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					fileOutputStream);

			// / write the current time to the report file as the load file
			// generation time
			outputStreamWriter.write("***  Generating a All Load File on "
					+ String.format("%tD", currentTime)
					+ String.format(" %tT", currentTime) + " ("
					+ String.format("%tY/", currentTime)
					+ String.format("%tj", currentTime) + ")\r\n\r\n");

			// / write the report file name to the report file
			outputStreamWriter.write("***  Report File Name\t\t\t" + reportName	+ "\r\n");

			// / write the load start time to the report file
			outputStreamWriter.write("***  Product Start Date\t\t\t"+ String.format("%tY/",spSession.getProductTimeRange().first)+ String.format("%tj",	spSession.getProductTimeRange().first) + "\r\n");

			// / write the load file end time to the report file
			outputStreamWriter.write("***  Product End Date\t\t\t"+ String.format("%tY/",spSession.getProductTimeRange().second)+ String.format("%tj",spSession.getProductTimeRange().second) + "\r\n");

			// / write the load file duration to the report file
			outputStreamWriter.write("***  Product Duration (in days)\t\t"+ ((spSession.getProdDurHour() * 60 + spSession.getProdDurMinute()) * 60 / 86400.0)+ "\r\n\r\n\r\n");

			for (EventTemplateLine etl : etls) {
				// System.out.println("report:[" +
				// etl.getEventTriggerTag().getTriggerTime() + "]," +
				// TimeToString.AsLogTime(etl.getEventTriggerTag().getTriggerTime()));
				outputStreamWriter.write(TimeToString.AsLogTime(etl.getEventTriggerTag().getTriggerTime())+ String.format(".%tL", etl.getEventTriggerTag().getTriggerTime())+ " #  "+ etl.getExpandedText().substring(13).trim() + "\r\n");
			}

			outputStreamWriter.close();
		} catch (Exception e) {
			logger.info(e.getMessage());
			return false;
		}
		return true;
	}

	private void LoadInspecter(List<EventTemplateLine> etls,OutputStreamWriter writer) {

		Inspecter inspec = new Inspecter(etls);
		inspec.run();
		List<String> warningMsgs = new ArrayList<String>();

		for (EventTemplateLine unpair : inspec.getPairDates()) {

			warningMsgs.add("Command not in pairs       "+ TimeToString.AsLogTime(unpair.getEventTriggerTag().getTriggerTime())
					+ String.format(".%tL", unpair.getEventTriggerTag().getTriggerTime()) + " #  "+ unpair.getExpandedText().substring(13));
		}

		for (int i = 0; i < inspec.getExceptionDates().size(); i++) {
			warningMsgs.add("Exception Command = "+ inspec.getExceptionDates_Desc().get(i)+ "     ,"
					+ TimeToString.AsLogTime(inspec.getExceptionDates().get(i).getEventTriggerTag().getTriggerTime())
					+ String.format(".%tL", inspec.getExceptionDates().get(i).getEventTriggerTag().getTriggerTime())
					+ " #  "+ inspec.getExceptionDates().get(i).getExpandedText().substring(13));
		}

		for (EventTemplateLine overlap : inspec.getTimeOverlapDates()) {
			warningMsgs.add("Command time overlap    "+ TimeToString.AsLogTime(overlap.getEventTriggerTag().getTriggerTime())
					+ String.format(".%tL", overlap.getEventTriggerTag().getTriggerTime()) + " #  "+ overlap.getExpandedText().substring(13));
		}

		for (int i = 0; i < inspec.getTimeCmdDates().size(); i++) {
			warningMsgs.add("Command = "+ inspec.getTimeCmdDates_Desc().get(i)+ "     ,"+ TimeToString.AsLogTime(inspec.getTimeCmdDates().get(i).getEventTriggerTag().getTriggerTime())
					+ String.format(".%tL", inspec.getTimeCmdDates().get(i).getEventTriggerTag().getTriggerTime())+ " #  "+ inspec.getTimeCmdDates().get(i).getExpandedText().substring(13));
		}

		try {
			writer.write(printWarningMsg(warningMsgs));
		} catch (IOException e) {
			logger.info("ScheduleProductBuilder LoadInspecter IOException="+ e.getMessage());
		}
	}

	// / method to write the user inputs to the log file
	private void printLog() {
		// / get the current time
		genTime = new Timestamp(System.currentTimeMillis());
		double dur;

		try {
			// / insert html control fields
			outputStreamWriter.write("<html>\n<head>\n</head>\n<body>\n<pre>\n");

			// / write the current time to the log file as PG run time
			outputStreamWriter.write("***  Running Schedule Product Generation Software on "+ TimeToString.AsShortTime(genTime)+ " ("+ TimeToString.AsLogDate(genTime) + ")\n\n");

			// / write the log file name to the log file
			outputStreamWriter.write("***  Log File Name :                    "+ logFileName + "\n");

			// / write the product id to the log file
			outputStreamWriter.write("***  Product ID :                       "+ productId + "\n");

			// / write the schedule start time to the log file
			outputStreamWriter.write("***  Session Start Time :               "+ TimeToString.AsLogTime(spSession.getProductTimeRange().first) + "\n");

			// / write the schedule end time to the log file
			outputStreamWriter.write("***  Session End   Time :               "+ TimeToString.AsLogTime(spSession.getProductTimeRange().second) + "\n");

			// / write the session duration to the log file
			dur = (spSession.getProdDurMinute() * 60)+ (spSession.getProdDurHour() * 3600);
			outputStreamWriter.write("***  Session Duration (in days) :       "+ (dur / 86400.0) + "\n");

			// / write the branch name to the log file
			outputStreamWriter.write("***  Branch Name :                      ");
			outputStreamWriter.write(spSession.getIsNominal() ? "None\n": productSessionName + "\n");

			// / write the load start time to the log file
			if (spSession.getTTQSelected() || spSession.getMPQSelected()) {
				outputStreamWriter.write("***  Load Start Time :                  "+ TimeToString.AsLogTime(spSession.getLoadTimeRange().first) + "\n");
				outputStreamWriter.write("***  Load End   Time :                  "+ TimeToString.AsLogTime(spSession.getLoadTimeRange().second) + "\n");

				// / write the load file duration to the log file
				outputStreamWriter.write("***  Load File Duration (hh:mm) :       "
								+ String.format("%1$02d",spSession.getProdDurHour())+ ":"
								+ String.format("%1$02d",spSession.getLoadDurMinute()) + "\n\n");

			} else {
				outputStreamWriter.write("***  Load Start Time :                  N/A\n");
				outputStreamWriter.write("***  Load End   Time :                  N/A\n");
				outputStreamWriter.write("***  Load File Duration (hh:mm) :       N/A\n");
			}

			// / write the satellites selected for generation to the log file
			outputStreamWriter.write("***  Satellites Selected\t\t"+ SatelliteName + "\n");

			// / write the schedule products selected for generation to the log
			// file
			outputStreamWriter.write("*** Resolved Acquisition Schedule Option Selected :  ");
			outputStreamWriter.write(spSession.getRasSelected() ? "Y\n" : "N\n");

			outputStreamWriter.write("*** TTQ Loads Option Selected :                      ");
			outputStreamWriter.write(spSession.getTTQSelected() ? "Y\n" : "N\n");

			outputStreamWriter.write("*** MPQ Loads Option Selected :                      ");
			outputStreamWriter.write(spSession.getMPQSelected() ? "Y\n" : "N\n");

			outputStreamWriter.write("*** Pass Plans Selected :                            ");
			outputStreamWriter.write(spSession.getPassPlanSelected() ? "Y\n": "N\n");

			outputStreamWriter.write("*** Integrated Reports Option Selected :             ");
			outputStreamWriter.write(spSession.getIntegratedRpt() ? "Y\n": "N\n");

		} catch (Exception e) {
			logger.info(e.getMessage());
		}
	}
}
