package mpss.pg;

import java.sql.Timestamp;

import mpss.common.jpa.Satellite;


public class OrbitalEvent
{
	private String category;
	private Timestamp eventTime;
	private String type;
	private String subtype;
	private Satellite satellite;
	
	public OrbitalEvent(String category,Satellite satellite,Timestamp tp,String type,String subtype)
	{
		this.category = category;
		this.satellite = satellite;
		this.eventTime = tp;
		this.type = type;
		this.subtype = subtype;
	}
	 
	public String getCategory()
	{
		return this.category;
	}
	
	public String getType()
	{
		return this.type;
	}
	
	public String getSubtype()
	{
		return this.subtype;
	}
	
	public Timestamp getEventTime()
	{
		return this.eventTime;
	}
	
	public Satellite getSatellite()
	{
		return this.satellite;
	}
}

