package mpss;

import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedList;

import mpss.aip.AIPCreator;
import mpss.ftp.json.source.FS5HttpJsonStream;
import mpss.mtl.MTLLoader;
import mpss.oe.OrbitEventsLoader;
import mpss.pg.product.ScheduleProductBuilder;
import mpss.pg.ScheduleProductSession;
import mpss.rpt.ReportCreator;
import mpss.rts.RTSCreator;
import mpss.schedule.Schedule;
import mpss.schedule.UIDB;
import mpss.schedule.base.GlLog;
import mpss.schedule.mis.MisCreator;
import mpss.smart.SmartFtp;
import mpss.sv.Validator;
import mpss.util.IlogLogUtil;
import mpss.util.ResultVO;
import mpss.util.timeformat.RangeUtil;
import javae.jmx.amq.topic.AmqJmxDriver;
import javae.jmx.topic.JmxConnection;
import javae.jmx.topic.JmxDriverManager;
import javae.jmx.topic.JmxException;
import javae.jmx.topic.JmxSubscriber;
import javae.jmx.topic.JmxSubscriber.Reply;
import javae.jmx.topic.JmxTopicListener;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import nspo.mpss.amq.IMpssAmqData.DBModel;
import nspo.mpss.amq.IMpssAmqData.FtpType;
import nspo.mpss.amq.IMpssAmqData.Function;
import nspo.mpss.amq.data.*;
import nspo.mpss.amq.*;
import nspo.mpss.amq.cmd.*;
import nspo.xpsoc.process.AmqService;

public class StartMQ
{
	//=============ActiveMQ==================
	private AmqService amqWeb = new AmqService();
	private int amqID;
	private static JmxConnection<IMpssAmqData, IMpssAmqData> conn = null;	
	ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf"}));
	
		
	Schedule sch = new Schedule();	
	private static Logger logger;
	
	public static void main(String args[])
	{				
		try {  
			System.setProperty("satName", args[0]);
			config.load();
            org.apache.log4j.xml.DOMConfigurator.configure(javae.AppDomain.getPath("log4j.xml", new String[]{"conf",configFactory.getSatelliteName()}));
            logger = Logger.getLogger(configFactory.getLogName());
            
            if(args.length!=2){
            	logger.info("StartMQ Main has no args");    			
    			return;
    		}            
    		
            logger.info("StartMQ Main args0 is "+args[0]);
    		logger.info("StartMQ Main args1 is "+args[1]);    		
    		StartMQ mq = new StartMQ();    		
    		mq.startAMQ(args[1]);            
            mq.setDebugging();
            logger.info(System.getProperty("satName")+" Server start success..........");
		} catch (Exception e) {           
            logger.info(System.getProperty("satName")+" main ex ="+e.getMessage());
		}	

	} 
	
	// set ilog debugging , default 10
	@SuppressWarnings("static-access")
	public void setDebugging(){
		try{
			sch.debugging = Integer.valueOf(System.getProperty("debugging","10"));	
			logger.debug("Set ilog debugging mode:"+sch.debugging);
		}catch(Exception ex){			
			logger.info("Ilog set debugging ex="+ex.getMessage()); 
		}			
	}

	
	public void startAMQ(String mqName) 
	{
		JmxDriverManager.register("ActiveMQ", new AmqJmxDriver());
		try {			
			conn = JmxDriverManager.connectObject(IMpssAmqData.class, IMpssAmqData.class, "ActiveMQ","tcp://" + System.getProperty("mqIP") + ":" + System.getProperty("mqPort"));
			conn.start();
		} catch (JmxException ex) {
			logger.warn("startAMQ ex="+ex.getMessage()); 			
		}
		
		conn.createSubscriber().register(mqName, new JmxTopicListener<IMpssAmqData, IMpssAmqData>() 
		{
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public void onMessage(JmxSubscriber js, IMpssAmqData i, Reply<IMpssAmqData> reply) 
			{	
				Function func=i.getFunction();
				String msg = ""; 
				switch (func)
				{
					case EARS_CMD :
						Ears commandEar=(Ears)i;
						EarsResp respEi = new EarsResp();//Reply
						
						String mtlfilePath = System.getProperty("dataFileDir");							
						mtlfilePath += configFactory.getMtlFolder(commandEar.getFileType()) +  commandEar.getFileName();						
						logger.debug("EARS CMD ,FileType ="+commandEar.getFileType()+", mtlfilePath="+mtlfilePath);
						
						if (commandEar.getFileName() == null)
						{
							msg = "mtlfilename cannot be found in property file";
							logger.debug("EARS CMD ,FileName is null ,msg="+msg);
							respEi.setSuccess(false);
							respEi.setResponseStr(msg);
							respEi.setNeedConfirm(false);
							reply.send(respEi);
							break;
						}    		
				      
						try 
						{
							// Spring application context							
							String beanName =configFactory.getMtlLoaderName();							
							logger.debug("EARS CMD ,MTL Loader beanName is "+beanName);
							
							MTLLoader mtlLoader = (MTLLoader)context.getBean(beanName);							
							if (mtlLoader == null) {
								msg = "load MtlLoader bean failed";		
								logger.debug("EARS CMD ,MTLLoader is null ,msg="+msg);
								respEi.setSuccess(false);
								respEi.setResponseStr(msg);
								respEi.setNeedConfirm(false);
								reply.send(respEi);
								break;
							}
						    
							mtlLoader.setIsBranch(commandEar.isBranch());							
							if (mtlLoader.Load(mtlfilePath,commandEar.isCheck()))
							{
								msg = !mtlLoader.getNeedConfirm() ? "MTL Loader complete" : mtlLoader.getConfirmMsg();								
								respEi.setActivityList(mtlLoader.getActIdList());
								respEi.setSessionId(mtlLoader.getSessionId());
								respEi.setSuccess(true);
								respEi.setNeedConfirm(mtlLoader.getNeedConfirm());
								
							}
							else
							{
								msg = (mtlLoader.getMsg() == null) ? "MTL Loader failed" : mtlLoader.getMsg();
								respEi.setSuccess(false);								
							}
							
							respEi.setResponseStr(msg.replace("null", ""));
							reply.send(respEi);
							break;
							
						} catch (Exception ex) {							
							logger.warn("EARS_CMD ex="+ex.getMessage());
						}
					
					case SCH_VALID_CMD:
						ScheduleValid  commandSV=(ScheduleValid)i;
						ScheduleValidResp  respSV = new ScheduleValidResp();//Reply
						
						try 
						{
							Validator validator = (Validator)context.getBean(configFactory.getValidator());	  
							if (validator == null) {								
								logger.warn("Schedule Validator bean failed");
								return;
							}
							if (validator.Validate(commandSV.getSessionId() , commandSV.isBranch()))
							{
								respSV.setResponseStr("Schedule Validator complete");
								respSV.setSuccess(true);
								respSV.setAdvList(validator.getAdvisorySummary());
								respSV.setVioList(validator.getViolationSummary());
								respSV.setVaildated(validator.getSVStatus());
							}
							else
							{
								msg = (validator.getReturnMsg() == null) ? "Schedule Validator failed" : validator.getReturnMsg();
								respSV.setResponseStr(msg);
								respSV.setSuccess(false);								
							}
							reply.send(respSV);
							
				      	} catch (Exception ex) {				      		
				      		logger.warn("SCH_VALID_CMD ex="+ex.getMessage());
				        }
				    	
						break;
						
					case SV_VIEWCRIT_CMD:						
						ViewCriterionResp  respVC = new ViewCriterionResp();
						
						try 
						{
							Validator validator = (Validator)context.getBean(configFactory.getValidator());	  
							if (validator == null) {								
								logger.warn("View Criterion bean failed");
								return;
							}

							validator.Criterion();
							respVC.setResponseStr("View Criterion complete");
							respVC.setSuccess(true);
							respVC.setAdvCriterion(validator.getAdvisoryCriterion());
							respVC.setVioCriterion(validator.getViolationCriterion());
							reply.send(respVC);							
				      	} catch (Exception ex) {
				      		System.err.println(ex.getMessage());
				      		logger.warn("SV_VIEWCRIT_CMD ex="+ex.getMessage());
				        }
				    	
						break;
						
					case PG_CMD :
						ProductGen commandPG=(ProductGen)i;
						ProdGenResp respPG = new ProdGenResp();						
						ScheduleProductBuilder pgBuilder = (ScheduleProductBuilder)context.getBean(configFactory.getProductBuilder());	  
				        if (pgBuilder == null) {				        	
				        	logger.warn("load ScheduleProductBuilder bean failed");
				        	return;
				        }
				        //Product Generation Window Inputs
				        ScheduleProductSession spSession = new ScheduleProductSession();
				        spSession.setIntegratedRptSelected(commandPG.isIntegrateRpt());
				        spSession.setMPQSelected(commandPG.isMpq());
				        spSession.setPassPlanSelected(commandPG.isPassPlan());
				        spSession.setRasSelected(commandPG.isResolveAcq());
				        spSession.setRasGroundSelected(commandPG.isResolveAcq());
				        spSession.setTTQSelected(commandPG.isTtq());
				        spSession.setIsNominal(commandPG.isNominal());
				        spSession.setLoadTimeRange(RangeUtil.startTimeAppendHourMinu(new Timestamp(commandPG.getLoadStartTime().getTime()), commandPG.getLoadDurHour(), commandPG.getLoadDurMin()));
					    spSession.setProductTimeRange(RangeUtil.startTimeAppendHourMinu(new Timestamp(commandPG.getProdStartTime().getTime()), commandPG.getProdDurHour(), commandPG.getProdDurMin()));
					    if (commandPG.isNominal()){
					    	spSession.setBranchId(0);
					    }else spSession.setBranchId(commandPG.getBranchId());
					    spSession.setSatelliteId(commandPG.getSatelliteId());
					    spSession.setLoadDurHour(commandPG.getLoadDurHour());
					    spSession.setLoadDurMinute(commandPG.getLoadDurMin());
					    spSession.setProdDurHour(commandPG.getProdDurHour());
					    spSession.setProdDurMinute(commandPG.getProdDurMin());					    
						pgBuilder.setFileDir(System.getProperty("reportFileDir"));
						
						if(pgBuilder.run(spSession))
						{							
							logger.debug("PG Builder complete");
							respPG.setSuccess(true);
							respPG.setResponseStr("log_" + pgBuilder.getProductId() + ".html");
						}
						else
						{
							msg = (pgBuilder.getReturnMsg() == null) ? "PG Builder failed" : pgBuilder.getReturnMsg();
							logger.debug(msg);
							respPG.setSuccess(false);
							respPG.setResponseStr(msg);
						}
						
				    	reply.send(respPG);
						break;
						
					case OE_INGEST_CMD :
						OrbitEvtIng commandOE=(OrbitEvtIng)i;
						OrbiEvtIngResp respOE = new OrbiEvtIngResp();//Reply
						
						try {	        

				        	OrbitEventsLoader oeLoader = (OrbitEventsLoader)context.getBean(configFactory.getOrbitEventsLoader());	  
				        	if (oeLoader == null) {				        		
				        		logger.warn("load OrbitEventsLoader bean failed");
				        		return;
				        	}
				        	oeLoader.setScid(commandOE.getSatelliteId()); 
				        	oeLoader.setBranchid(commandOE.getBranchId());
				        	oeLoader.setFileName(commandOE.getFilename()); 
				        	String oeInputFilePath = System.getProperty("dataFileDir") + System.getProperty("oeFileDir");
				        	String revsfilename = oeInputFilePath + commandOE.getFilename() + "_revs.rpt";
				        	String consfilename = oeInputFilePath + commandOE.getFilename() + "_cons.rpt";
				        	
				        	if (oeLoader.Load(revsfilename, consfilename))
							{
								respOE.setResponseStr(oeLoader.getLogFileName());
								respOE.setSuccess(true);
							}
							else
							{
								msg = (oeLoader.getReturnMsg() == null) ? "Orbit Events Loader failed" : oeLoader.getReturnMsg();		
								logger.debug(msg);
								respOE.setResponseStr(msg);
								respOE.setSuccess(false);
							}
							reply.send(respOE);				        	
				        	
				      	} catch (Exception ex) {				      		
				      		logger.warn("OE_INGEST_CMD ex="+ex.getMessage());
				        }
				        
				    	
						break;
						
					case RE_SCH_AIP_CMD :
						ReScheduleAip commandRSAIP=(ReScheduleAip)i;
						ReScheduleAipResp respRSAIP = new ReScheduleAipResp();//Reply
						
						try {
							AIPCreator aipCreator = (AIPCreator)context.getBean(configFactory.getAipCreator());	  
				        	if (aipCreator == null) {				        		
				        		logger.warn("Load AIPCreator bean failed");
				        		return;
				        	}
				        	
				        	boolean status = false;
				        	if (commandRSAIP.getContactId()< 0)
				        		status=aipCreator.ReSchPROC((int)commandRSAIP.getActiviyId());
				        	else
				        		status=aipCreator.ReSchPBK((int)commandRSAIP.getActiviyId(), (int)commandRSAIP.getContactId());
				        	
				        	if (status)
							{
				        		msg = "Successfully scheduled 1 activity ! ";
				        		msg += (aipCreator.returnMsg.length() < 1) ? "" : aipCreator.returnMsg;								
								respRSAIP.setResponseStr(msg);
				        		respRSAIP.setSuccess(true);
							}
							else
							{
								msg = "0 activity been scheduled! ";
				        		msg += (aipCreator.returnMsg.length() < 1) ? "" : aipCreator.returnMsg;
				        		logger.debug(msg);
								respRSAIP.setResponseStr(msg);
								respRSAIP.setSuccess(false);
							}
							reply.send(respRSAIP);
				        	
				        	
				      	} catch (Exception ex) {				      		
				      		logger.warn("RE_SCH_AIP_CMD ex="+ex.getMessage());
				        }				        
				    	
						break;
						
					case SCH_RPT_AIP_CMD :
						ScheduleRptAip commandAIP=(ScheduleRptAip)i;
						ScheduleRptAipResp respAIP = new ScheduleRptAipResp();//Reply
						
						try {
							ReportCreator rptCreator = (ReportCreator)context.getBean(configFactory.getReportCreator());	  
							
				        	if (rptCreator == null) {				        		
				        		logger.warn("Load ReportCreator bean failed");
				        		return;
				        	}
				        		
				        	rptCreator.setFileDir (System.getProperty("reportFileDir")); 
				        	rptCreator.setSatName (System.getProperty("satName")); 					
				        	
				        	if (rptCreator.GenAIP(commandAIP.getSessionId()))
							{
				        		respAIP.setResponseStr(rptCreator.getLogFileName());
				        		respAIP.setSuccess(true);
							}
							else
							{
								msg = (rptCreator.getReturnMsg() == null) ? "AIP Report generation failed" : rptCreator.getReturnMsg();
								logger.debug(msg);
								respAIP.setResponseStr(msg);
								respAIP.setSuccess(false);
							}
							reply.send(respAIP);
				        	
				        	
				      	} catch (Exception ex) {
				      		logger.warn("SCH_RPT_AIP_CMD ex="+ex.getMessage());
				        }				        
				        
						logger.debug("AIP Report generation complete");
						break;
						
					case SCH_RPT_CMD :
						ScheduleRpt commandRPT=(ScheduleRpt)i;
						ScheduleRptResp respRPT = new ScheduleRptResp();//Reply
						
						try {
							ReportCreator rptCreator = (ReportCreator)context.getBean(configFactory.getReportCreator());	  
				        	if (rptCreator == null) {				        		
				        		logger.warn("Load ReportCreator bean failed");
				        		return;
				        	}
				        					        	
				        	rptCreator.setSection(commandRPT.getSectionId()); 
				        	rptCreator.setBranch(!commandRPT.isNominal(),commandRPT.getBranchId());
				        	rptCreator.setSelRSI (commandRPT.isRsi()); 
				        	rptCreator.setSelSSR (commandRPT.isSsr());
				        	rptCreator.setRSIFileName (commandRPT.getRsiFileName()); 
				        	rptCreator.setSSRFileName (commandRPT.getSsrFileName());
				        	rptCreator.setRptTimeRange (commandRPT.getSectionStart(), commandRPT.getSectionStop());
				        	rptCreator.setFileDir (System.getProperty("reportFileDir")); 
				        	rptCreator.setSatName (System.getProperty("satName")); 					
				        	if (rptCreator.GenReport())
							{
				        		respRPT.setResponseStr(rptCreator.getLogFileName());
				        		respRPT.setSuccess(true);
							}
							else
							{
								msg = (rptCreator.getReturnMsg() == null) ? "Report generation failed" : rptCreator.getReturnMsg();
								logger.debug(msg);
								respRPT.setResponseStr(msg);
								respRPT.setSuccess(false);
							}
							reply.send(respRPT);
				        	
				        	
				      	} catch (Exception ex) {				      		
				      		logger.warn("SCH_RPT_CMD ex="+ex.getMessage());
				        }				        
				        
						logger.debug("Report generation complete");
						break;
						
					case RTS_SCH_CMD :
						RtsSchReq commandRTS=(RtsSchReq)i;
						RtsSchReqResp respRTS = new RtsSchReqResp();//Reply
						
						try {
							RTSCreator rtsCreator = (RTSCreator)context.getBean(configFactory.getRTSCreator());	  
				        	if (rtsCreator == null) {				        		
				        		logger.warn("Load RTSCreator bean failed");
				        		return;
				        	}
				        					        	
				        	rtsCreator.setStartTime(commandRTS.getStartTime()); 
				        	rtsCreator.setStopTime(commandRTS.getStopTime());
//				        	rtsCreator.setFileName (commandRTS.getFileName());
				        	rtsCreator.setContactList (commandRTS.getContactList());
				        	rtsCreator.setBranch(commandRTS.isBranch(), commandRTS.getSessionId());
				        	rtsCreator.setNoCheckList (commandRTS.getNocheckList());
				        	
				        	if (rtsCreator.GenReport())
							{
				        		respRTS.setResponseStr(rtsCreator.getLogFileName());
				        		respRTS.setSuccess(true);
							}
							else
							{
								msg = (rtsCreator.getReturnMsg() == null) ? "RTS generation failed" : rtsCreator.getReturnMsg();								
								logger.debug(msg);
								respRTS.setResponseStr(msg);
								respRTS.setSuccess(false);
							}
							reply.send(respRTS);
				        	
				        	
				      	} catch (Exception ex) {				      		
				      		logger.warn("RTS_SCH_CMD ex="+ex.getMessage());
				        }        
				        
						logger.debug("RTS generation complete");
				    	
						break;
						
					case COLD_SCH_CMD:

						ColdSchedule cs = (ColdSchedule) i;

						String sessionName = cs.getSessionName();							
						Date starttime = new Date();						
						logger.debug("cold run sessionName="+sessionName);
						boolean res = sch.runCold(sessionName);	
						int count=0;
						
						if(System.getProperty("aocs.maneuver.mis.enable","0").equalsIgnoreCase("1")){
							MisCreator misCreator = (MisCreator) context.getBean(configFactory.getMisCreator());
							count = misCreator.create(cs.getSessionId(), 1);
						}
						
						long schtime =(new Date().getTime()-starttime.getTime())/1000;
						
						ColdScheduleResp resObj = new ColdScheduleResp();							
						resObj.setResponseStr(String.valueOf(Schedule.coldrun1+count)+","+String.valueOf(Schedule.coldrun1-Schedule.coldrun2)+","+(int)Math.round(schtime));
						resObj.setSuccess(res);
						
						reply.send(resObj);						
						logger.debug("Reply OK");
						break;

					case HOT_SCH_CMD:
						
						HotSchedule cs1 = (HotSchedule) i;

						int sessionid = (int)cs1.getSessionId();						
						logger.debug("hot sch session id ="+sessionid);						
						Schedule.updateDB = true;
						boolean res1 = sch.runHot(sessionid);
						HotScheduleResp resObj1 = new HotScheduleResp();							
						resObj1.setResponseStr(String.valueOf(Schedule.hotrun1)+","+String.valueOf(Schedule.hotrun1-Schedule.hotrun2)+","+String.valueOf(Schedule.hotrun2));
						resObj1.setSuccess(res1);
						
						reply.send(resObj1);						
						logger.debug("hot Reply OK");
						break;
						
					case UI_DB_CMD:
						
						UiDb dbobj= (UiDb) i;
						UiDbResp resObj2 = new UiDbResp();							
						UIDB fact =Schedule.getUiDbFactory(dbobj.getTablename());
						if(dbobj.getModel()==DBModel.INSERT){								
							boolean result =fact.create(dbobj.getObj());
							resObj2.setSuccess(result);
						}else if(dbobj.getModel()==DBModel.UPDATE){
							boolean result =fact.update(dbobj.getObj());
							resObj2.setSuccess(result);
						}else if(dbobj.getModel()==DBModel.DELETE){
							boolean result=fact.delete(dbobj.getObj());
							resObj2.setSuccess(result);
						}else if(dbobj.getModel()==DBModel.SQL){
							boolean result=fact.doSql((String)dbobj.getObj());
							resObj2.setSuccess(result);
						}		
						
						
						reply.send(resObj2);						
						logger.debug("UI DB  OK");
						break;
						
					case RE_SCH_CMD:
						
						ReSchedule resch = (ReSchedule) i;
						long sessionid1 =resch.getSessionId();
						long contactid1=resch.getContactId();
						String sation =resch.getStation();
						
						LinkedList<Long> activityids =resch.getActivityList();
						LinkedList<Long> deleteactivityids =resch.getDelActivityList();
						Schedule.updateDB = true;						
						logger.debug("get re sch cmd , session id = "+sessionid1 + "contact id = "+contactid1);
						for(Long activityid:activityids){							
							logger.debug("activity id ="+activityid.intValue());
						}
						boolean res2 = sch.runUIReHot(Long.valueOf(sessionid1).intValue(),resch.getReschActId(),sation, activityids,Long.valueOf(contactid1).intValue(),deleteactivityids);

						ReScheduleResp reschresp = new ReScheduleResp();							
						reschresp.setResponseStr(Integer.valueOf(Schedule.resch).toString());
						reschresp.setSuccess(res2);
						
						reply.send(reschresp);						
						logger.debug("resch Reply OK");
						break;
					
					case  MOVE_DEL_CMD:
						
						MoveDelActivity cmd = (MoveDelActivity) i;
						long sessionid2 =cmd.getSessionId();
						long activityid = cmd.getActivityId();
						Date date =cmd.getMoveDate();							
						Schedule.updateDB = true;						
						logger.debug("get move delete activity sch cmd , session id = "+sessionid2 + "activity id = "+activityid);
						boolean res3 = sch.AddResourceChange(Long.valueOf(sessionid2).intValue(),Long.valueOf(activityid).intValue() ,date);

						MoveDelActivityResp movedelete = new MoveDelActivityResp();							
						movedelete.setResponseStr("OK ");
						movedelete.setSuccess(res3);
						
						reply.send(movedelete);						
						logger.debug("Delete activity move  Reply OK");
						break;
						
					case  ADJ_ACT_CMD:
						
						AdjActivity adjcmd = (AdjActivity) i;
						long sessionid3 =adjcmd.getSessionId();
						long conactid3 = adjcmd.getContactId();
						LinkedList<Long> adjactivityids = adjcmd.getActivityList();
						Date adjdate =adjcmd.getNewDate();							
						Schedule.updateDB = true;						
						logger.debug("get adjuest activity sch cmd , session id = " + sessionid3);
						
						boolean res4 = sch.AdjustActivity(Long.valueOf(sessionid3).intValue(),Long.valueOf(conactid3).intValue(),adjactivityids ,adjdate);

						AdjActivityResp adjresp = new AdjActivityResp();							
						adjresp.setResponseStr("OK ");
						adjresp.setSuccess(res4);
						
						reply.send(adjresp);						
						logger.debug("Adjust activity Reply OK");
						break;	
						
					case  RE_SCH_LIST_CMD:
						
                        ReScheduleList reschlistcmd = (ReScheduleList) i;
                        long sessionid4 =reschlistcmd.getSessionId();

                        LinkedList<Long> reschactivityids = reschlistcmd.getActivityList();
                        LinkedList<Long> reschsites = reschlistcmd.getSiteList();
                        LinkedList<Long> reschdeleteactivityids =reschlistcmd.getDelActivityList();                                                                
                        Schedule.updateDB = true;                        
                        logger.debug("get re sch list cmd , session id = " + sessionid4);
                        boolean res5;
                        if(sessionid4==0){
                             res5 = sch.runUIReFilterHot(reschlistcmd.getRangeStart(),reschlistcmd.getRangeEnd(),reschsites,reschactivityids ,reschdeleteactivityids);                                     
                        }else{                                   
                             res5 = sch.runUIReFilterHot(Long.valueOf(sessionid4).intValue(),reschsites,reschactivityids ,reschdeleteactivityids);
                        }                          


                        ReScheduleListResp reschlistresp = new ReScheduleListResp();                               
                        reschlistresp.setResponseStr("OK ");
                        reschlistresp.setSuccess(res5);

                        reply.send(reschlistresp);                        
                        logger.debug("re sch list  Reply OK");
                        break;
                        
					case FTP_TRANSFER_CMD :					
						
						logger.debug("FTP_TRANSFER_CMD...");
						FtpTransfer ftp = (FtpTransfer) i;
						SmartFtp samrtftp = new SmartFtp(new FS5HttpJsonStream("http://"+System.getProperty("mqIP","140.110.226.174")+":8080/"+System.getProperty("web.path","mpssweb")+"/ftp.json") );
						ResultVO ftpresult = new ResultVO();
						
						if(ftp.getFtpType()==FtpType.MTL){
							ftpresult =samrtftp.pullMTLRpt();						
						}else if(ftp.getFtpType()==FtpType.ORBIT){
							ftpresult =samrtftp.pullOERpt();
						}else if(ftp.getFtpType()==FtpType.AIPMTL){
							ftpresult =samrtftp.pullAIPMTLRpt();
						}else if(ftp.getFtpType()==FtpType.RTS_REPLY){
							ftpresult =samrtftp.pullRTSReplyRpt();
						}else if(ftp.getFtpType()==FtpType.PASS_PLAN){
							ftpresult =samrtftp.pushPassPlan("");
						}else if(ftp.getFtpType()==FtpType.TTQ){
							ftpresult =samrtftp.pushTTQLoad("");
						}else if(ftp.getFtpType()==FtpType.MPQ){
							ftpresult =samrtftp.pushMPQLoad("");							
						}else if(ftp.getFtpType()==FtpType.AIP_CONFIRM){
							ftpresult =samrtftp.pushAIPConfirm("");
						}else if(ftp.getFtpType()==FtpType.RTS_REQUEST){
							ftpresult =samrtftp.pushRtsRequest("");
						}else if(ftp.getFtpType()==FtpType.ASR){
							ftpresult =samrtftp.pushASR("");
						}else{
							logger.info("no ftp config");							
							ftpresult.setResult(false);
						}	
						
						FtpTransferResp ftpresp = new FtpTransferResp();
						ftpresp.setSuccess(ftpresult.getResult());
						if(ftpresult.getResult()==true){
							ftpresp.setFileList(ftpresult.getLists());
							ftpresp.setResponseStr("FTP Transfer is Successful !");
						}else{							
							logger.info("ftp faile ="+ftpresult.getMsg());
							ftpresp.setResponseStr(ftpresult.getMsg());
						}
						
						reply.send(ftpresp);
						break ;	    
						
					default:
				        break;
				}
				GlLog.SetLog(IlogLogUtil.getFullFileName());
			}
			
		}
		
				);
	}

	@SuppressWarnings("static-access")
	public void stopAmq() {
		amqWeb.stopAmq(amqID);
	}
	
	
}

