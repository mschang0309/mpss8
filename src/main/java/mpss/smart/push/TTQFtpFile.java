package mpss.smart.push;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import nspo.mpss.FtpFileVO;
import mpss.smart.AFtpFile;

public class TTQFtpFile extends AFtpFile {
	
	private boolean localmove=false;
		
	public TTQFtpFile(String ip,String user,String password,int port){
		this.ip=ip;
		this.user =user;
		this.password =password;
		this.port=port;
	}
	
	public TTQFtpFile(String ip,String user,String password){
		this(ip,user,password,21);
	}
	
	public List<FtpFileVO> filterFile(List<FtpFileVO> files){		
    	if(this.file_filter!=null && this.file_filter.size()>0){
			List<FtpFileVO> temps = new ArrayList<FtpFileVO>();
			for(FtpFileVO vo : files){
				for(String filter:file_filter){
					if(vo.getFile_name().endsWith(filter)){
						if(new File(this.local_path+vo.getFile_name()).length()!=0){
							temps.add(vo);
						}						
					}
				}				
			}			
			return temps;
		}else{			
			return files;
		}	
	}
	
	
	
	public boolean isPush(){
		return true;
	}
	
    public boolean needLocalDelete(){
    	return false;
    }
	
	public boolean needLocalMove(){
		return localmove;
	}
	
	public void setLocalMove(boolean flag){
		this.localmove =flag;
	}
	
	public boolean needFTPMove(){
		return true;
	}
	
	
	
}
