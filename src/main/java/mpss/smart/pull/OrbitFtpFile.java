package mpss.smart.pull;

import mpss.smart.AFtpFile;

public class OrbitFtpFile extends AFtpFile {
	
	private boolean localbackup=false;
	
	public OrbitFtpFile(String ip,String user,String password,int port){
		this.ip=ip;
		this.user =user;
		this.password =password;
		this.port=port;
	}
	
	public OrbitFtpFile(String ip,String user,String password){
		this(ip,user,password,21);
	}	
	
	public boolean isPush(){
		return false;
	}
	
    public boolean needFTPDelete(){
    	return false;
    }	
	
	public boolean needFTPMove(){
		return true;
	}	
	
	public boolean needLocalMove(){
		return localbackup;
	}
	
    public void setLocalMove(boolean flag){
    	localbackup = flag;
	}
	
	
	
}
