package mpss.smart;

import mpss.ftp.json.source.FS5FileJsonStream;

import org.junit.Test;

public class SmartFtpTest {

	@Test
	public void MTLpull() {
		// martFtp ftp = new SmartFtp(new FS5HttpJsonStream("http://140.110.225.55:8080/mpssweb/ftp.json") );
		SmartFtp ftp = new SmartFtp(new FS5FileJsonStream("D:\\share\\ftp.json"));
		ftp.pullMTLRpt();
		System.out.println("ftp success");
	}

	//@Test
	public void OEpull() {
		// martFtp ftp = new SmartFtp(new FS5HttpJsonStream("http://140.110.225.55:8080/mpssweb/ftp.json") );
		SmartFtp ftp = new SmartFtp(new FS5FileJsonStream("D:\\share\\ftp.json"));
		ftp.pullOERpt();
		System.out.println("ftp success");
	}

	//@Test
	public void AIPMLTpull() {
		// martFtp ftp = new SmartFtp(new FS5HttpJsonStream("http://140.110.225.55:8080/mpssweb/ftp.json") );
		SmartFtp ftp = new SmartFtp(new FS5FileJsonStream("D:\\share\\ftp.json"));
		ftp.pullAIPMTLRpt();
		System.out.println("ftp success");
	}
	
	//@Test
	public void RTSReplyPull() {
		// SmartFtp ftp = new SmartFtp(new FS5HttpJsonStream("http://140.110.225.55:8080/mpssweb/ftp.json") );
		SmartFtp ftp = new SmartFtp(new FS5FileJsonStream("D:\\share\\ftp.json"));
		ftp.pullRTSReplyRpt();
		System.out.println("ftp success");
	}

	//@Test
	public void PassPlantestPush() {
		// SmartFtp ftp = new SmartFtp(new FS5HttpJsonStream("http://140.110.225.55:8080/mpssweb/ftp.json") );
		SmartFtp ftp = new SmartFtp(new FS5FileJsonStream("D:\\share\\ftp.json"));
		ftp.pushPassPlan("");
		System.out.println("ftp success");
	}

	// @Test
	public void LoadTTQPush() {
		// SmartFtp ftp = new SmartFtp(new FS5HttpJsonStream("http://140.110.225.55:8080/mpssweb/ftp.json") );
		SmartFtp ftp = new SmartFtp(new FS5FileJsonStream("D:\\share\\ftp.json"));
		ftp.pushTTQLoad("");
		System.out.println("ftp success");
	}

	// @Test
	public void LoadMPQPush() {
		// SmartFtp ftp = new SmartFtp(new FS5HttpJsonStream("http://140.110.225.55:8080/mpssweb/ftp.json") );
		SmartFtp ftp = new SmartFtp(new FS5FileJsonStream("D:\\share\\ftp.json"));
		ftp.pushMPQLoad("");
		System.out.println("ftp success");
	}

	//@Test
	public void AIPConfirmPush() {
		// SmartFtp ftp = new SmartFtp(new FS5HttpJsonStream("http://140.110.225.55:8080/mpssweb/ftp.json") );
		SmartFtp ftp = new SmartFtp(new FS5FileJsonStream("D:\\share\\ftp.json"));
		ftp.pushAIPConfirm("");
		System.out.println("ftp success");
	}

	//@Test
	public void RTSRequestPush() {
		// SmartFtp ftp = new SmartFtp(new FS5HttpJsonStream("http://140.110.225.55:8080/mpssweb/ftp.json") );
		SmartFtp ftp = new SmartFtp(new FS5FileJsonStream("D:\\share\\ftp.json"));
		ftp.pushRtsRequest("");
		System.out.println("ftp success");
	}

	// @Test
	public void ASRPush() {
		// SmartFtp ftp = new SmartFtp(new FS5HttpJsonStream("http://140.110.225.55:8080/mpssweb/ftp.json") );
		SmartFtp ftp = new SmartFtp(new FS5FileJsonStream("D:\\share\\ftp.json"));
		ftp.pushASR("");
		System.out.println("ftp success");
	}
	

}
