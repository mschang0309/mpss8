package mpss.smart;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mpss.util.FileOperate;
import nspo.mpss.FtpFileVO;
import nspo.mpss.fs7.Fs7Ftp;
import nspo.mpss.fs7.Fs7SFtp;
import nspo.mpss.fs7.IFS7Ftp;


public abstract class AFtpFile implements FtpFile {
	
	protected String ip;
	protected String user;
	protected String password;
	protected int port=21;
	
	protected String FTP_path;
	protected String local_path;	
		
	IFS7Ftp ftp ;	
	
	FTP_TYPE type = FtpFile.FTP_TYPE.FTP;	

	protected String msg="";
	
	protected List<String> file_filter;
	
	protected String ftp_backup_path;
	
	protected String local_backup_path;
	
	protected String yyyy_doy;
	
	private Map<String,Boolean> result;
	
	private FileWriter writer;
	
	public void setLogFile(FileWriter writer){		
		this.writer = writer;
	}
	
	private void writeFtpLog(String msg){
		try{			
			if(this.writer!=null){				
				this.writer.write(msg+System.getProperty( "line.separator" ));
			}
		}catch(Exception ex){			
			
		}		
	}
	
		
	public void setType(FTP_TYPE type) {
		this.type = type;
	}
	
	public String pushFtpSubFolder(){
		return "";
	}
		
    protected boolean FTPDelete(){
    	if(needFTPDelete()){
    		// delete
    		writeFtpLog("FTP Delete ......");
    		List<String> deletefiles = new ArrayList<String>();
    		for(FtpFileVO filename: this.filterFile(this.getFTPFolderFiles())){
    			deletefiles.add(filename.getFile_name());	
    			writeFtpLog("FTP Delete file:"+filename.getFile_name());
        	 }
    		Map<String,Boolean> result =ftp.deleteFiles(deletefiles);
    		for (String key : result.keySet()) {
        		if(!result.get(key).booleanValue()){
        			this.msg = "FTP Delete file fault , file="+key;
        			writeFtpLog(this.msg);
            		return false;
        		}            
            }
    	}
    	return true;
    }
	
	protected boolean FTPMove(){		
		if(needFTPMove()){
    		// move						
			List<FtpFileVO> temp =this.getFTPFolderFiles();		
			//writeFtpLog("FTP Move Folder:"+getMoveFTPSubPath()+"......");
			ftp.mkFolder(getMoveFTPSubPath());
			for(FtpFileVO filename: temp){
				//System.out.println("FTP MOVE 4,"+FTP_path+filename.getFile_name()+","+FTP_path+getMoveFTPSubPath()+"/"+filename.getFile_name());
				if(!ftp.moveFile(FTP_path+filename.getFile_name(), FTP_path+getMoveFTPSubPath()+"/"+filename.getFile_name())){
					this.msg = "FTP Move file fault , file="+filename.getFile_name();
					writeFtpLog(this.msg);
            		return false;
				}else{
					//writeFtpLog("FTP Move file:"+filename.getFile_name());
				}		
				
        	}			
    	}
    	return true;		
	}
	
	protected boolean localDelete(){
		if(needLocalDelete()){
			// delete 
			writeFtpLog("FTP Delete Local File.....");
			FileOperate fileop= new FileOperate();
    		for(FtpFileVO filename: this.filterFile(this.getLocalFolderFiles())){    			
    			if(!fileop.delFile(this.local_path+filename.getFile_name())){
    				this.msg = "FTP Delete local file fault , file="+filename.getFile_name();
    				writeFtpLog("FTP Delete local file fault , file="+filename.getFile_name());
            		return false;
    			}else{
    				writeFtpLog("FTP Delete local file="+filename.getFile_name());
    			}
        	}    		
		}
		return true;
	}
	
	protected boolean localMove(){		
		if(needLocalMove()){			
			FileOperate fileop= new FileOperate();
			if(local_backup_path!=null && local_backup_path.length()>0){
				local_backup_path =local_backup_path+"\\";
			}else{
				local_backup_path="";
			}
			writeFtpLog("FTP Local Move....");
			fileop.newFolder(local_path+this.local_backup_path);
			//System.out.println("local move file size="+this.filterFile(this.getLocalFolderFiles()).size());
			for(FtpFileVO filename: this.filterFile(this.getLocalFolderFiles())){    			
    			if(!fileop.moveFile(this.local_path+filename.getFile_name(),this.local_path+this.local_backup_path+filename.getFile_name())){
    				this.msg = "FTP Move local file fault , file="+filename.getFile_name();
    				writeFtpLog("FTP Move local file fault , file="+filename.getFile_name());
            		return false;
    			}else{
    				writeFtpLog("FTP Move local file ="+filename.getFile_name());
    			}
        	} 			
		}
		return true;
	}	
	
	private boolean localAllFileMove(){		
		if(needLocalMove()){			
			FileOperate fileop= new FileOperate();
			if(local_backup_path!=null && local_backup_path.length()>0){
				local_backup_path =local_backup_path+"\\";
			}else{
				local_backup_path="";
			}
			writeFtpLog("FTP Local Move....");
			fileop.newFolder(local_path+this.local_backup_path);			
			for(FtpFileVO filename: this.getLocalFolderFiles()){    			
    			if(!fileop.moveFile(this.local_path+filename.getFile_name(),this.local_path+this.local_backup_path+filename.getFile_name())){
    				this.msg = "FTP Move local file fault , file="+filename.getFile_name();
    				writeFtpLog("FTP Move local file fault , file="+filename.getFile_name());
            		return false;
    			}else{
    				writeFtpLog("FTP Move local file ="+filename.getFile_name());    				
    			}
        	} 			
		}
		return true;
	}
	
	public boolean needFTPDelete(){
    	return false;
    }
	
	public boolean needFTPMove(){
		return false;
	}
	
	public boolean needLocalDelete(){
		return false;
    }
	
	public boolean needLocalMove(){
		return false;
	}	
	
	public void setLocalMove(boolean flag){
		
	}
	
	public Map<String,Boolean> getResult(){
		return this.result;
	}
	
	protected boolean exitLocalPath(){
		return new FileOperate().exitFolder(this.local_path);		
	}
	
	protected boolean exitFTPPath(){
		return true;
	}
	
	protected List<FtpFileVO> getFTPFolderFiles(){
		return this.ftp.listInfoFiles();
	}
	
	protected List<FtpFileVO> getLocalFolderFiles(){
		List<FtpFileVO> files = new ArrayList<FtpFileVO>();
		File file =new File(this.local_path);
		File temp =null;
		File[] tempList = file.listFiles();
		for (int i =0; i < tempList.length; i++) {
			temp = new File(this.local_path + tempList[i].getName());
			if (temp.isDirectory() == false) {				
	            FtpFileVO vo = new FtpFileVO(temp.getName(),0,new Date()); 
	            files.add(vo);
	        }
		}
		
		return files;
	}
	
    public List<FtpFileVO> filterFile(List<FtpFileVO> files){		
    	if(this.file_filter!=null && this.file_filter.size()!=0){
			List<FtpFileVO> temps = new ArrayList<FtpFileVO>();
			for(FtpFileVO vo : files){
				for(String filter:file_filter){
					if(vo.getFile_name().endsWith(filter)){
						temps.add(vo);
					}
				}				
			}
			return temps;
		}else{
			return files;
		}	
	}
	
	public String getMessage(){
		return this.msg;
	}
	
	public void setFileFilter(String filter){
		String[] tokens = filter.split(";");
		List<String> lfilter =new ArrayList<String>();
		for (String token:tokens) {
			lfilter.add(token);
		}
		this.file_filter = lfilter;
	}
	
		
	
	public void setMoveFTPSubPath(String path){
		this.ftp_backup_path = path;
	}
	
	public String getMoveFTPSubPath(){
		return this.ftp_backup_path;
	}
	
	public String getMoveLocalSubPath(){
		return this.local_backup_path;
    }
	
	public void setMoveLocalSubPath(String path){
		this.local_backup_path = path;
	}
	
	public void setPath(String FTP_path,String local_path){
		this.local_path = local_path;
		this.FTP_path = FTP_path;
	}
	
	protected boolean push(){  
		if(this.type == FtpFile.FTP_TYPE.FTP){
			ftp = new Fs7Ftp(this.ip,this.user,this.password,this.port);
		}else{
			ftp = new Fs7SFtp(this.ip,this.user,this.password,this.port);
		}
		
		if(!ftp.connection()){
    		this.msg = "FTP Connection fault , ip="+ip+",user="+user+",password="+password+",port="+port;
    		writeFtpLog(this.msg);
    		return false;
    	}else{
    		writeFtpLog("FTP Connection : ip="+ip+",user="+user+",password="+password+",port="+port);    		
    	}  		
		
		ftp.changFolder(FTP_path);
		writeFtpLog("FTP Change Folder:"+FTP_path);
		
		if(!this.FTPMove()){
        	return false;
        }
		
		if(this.yyyy_doy!=null && this.yyyy_doy.length()>0){
			ftp.mkFolder(this.yyyy_doy);	    	
	    	if(!ftp.changFolder(FTP_path+this.yyyy_doy+"/")){
	    		this.msg = "FTP Folder fault 1 , path="+FTP_path;
	    		writeFtpLog("FTP Change Folder fault :"+FTP_path);
	    		return false;
	    	}else{
	    		writeFtpLog("FTP Change Folder :"+FTP_path);
	    	}			
		}
		
		if(this.pushFtpSubFolder()!=null && this.pushFtpSubFolder().length()>0){
			ftp.mkFolder(this.pushFtpSubFolder());	    	
	    	if(!ftp.changFolder(FTP_path+this.yyyy_doy+"/"+this.pushFtpSubFolder()+"/")){
	    		this.msg = "FTP Folder fault 2 , path="+FTP_path;
	    		writeFtpLog("FTP Change Folder fault :"+FTP_path);
	    		return false;
	    	}else{
	    		writeFtpLog("FTP Change Folder :"+FTP_path);
	    	}			
		}
		
    	
    	List<String> pushfiles = new ArrayList<String>();
    	for(FtpFileVO filename: this.filterFile(this.getLocalFolderFiles())){
    		writeFtpLog("FTP Filter file :"+filename.getFile_name());
    		pushfiles.add(filename.getFile_name());    		
    	}
    	
    	if(this.yyyy_doy!=null && this.yyyy_doy.length()>0){
    		ftp.setFtpSubFolder(this.yyyy_doy);
    	}
    	
    	if(this.pushFtpSubFolder()!=null && this.pushFtpSubFolder().length()>0){
    		ftp.setFtpSubFolder(this.pushFtpSubFolder());
    	}
    	
    	result =ftp.storeFiles(this.local_path,pushfiles);
    	for (String key : result.keySet()) {
    		if(!result.get(key).booleanValue()){
    			this.msg = "FTP Push file fault , file="+key;
    			writeFtpLog(this.msg);
        		return false;
    		}else{
    			writeFtpLog("FTP Push file , file="+key);
    		}            
        }
    	
    	if(!ftp.disconnection()){
        	this.msg = "FTP Disconnection fault ";
        	writeFtpLog(this.msg);
    		return false;        	
        }else{
        	writeFtpLog("FTP Disconnection ");
        }
    	
//    	if(!this.localMove()){
//        	this.msg = "ftp move local file fault ";
//    		return false;
//        }
    	
    	if(!this.localAllFileMove()){
        	this.msg = "ftp move local file fault ";
    		return false;
        }
        
        if(!this.localDelete()){
        	this.msg = "ftp delete local file fault ";
    		return false;
        }        
        
		return true;
    	
    }
	
	protected boolean pull(){
		System.out.println("ftp pull");
		if(this.type == FtpFile.FTP_TYPE.FTP){
			ftp = new Fs7Ftp(this.ip,this.user,this.password,this.port);
		}else{
			ftp = new Fs7SFtp(this.ip,this.user,this.password,this.port);
		}
		
    	if(!ftp.connection()){
    		this.msg = "FTP Connection fault , ip="+ip+",user="+user+",password="+password+",port="+port;
    		writeFtpLog(this.msg);   
    		return false;
    	}else{
    		writeFtpLog("FTP Connection : ip="+ip+",user="+user+",password="+password+",port="+port);    		
    	}    	
    	
    	if(!ftp.changFolder(FTP_path)){
    		this.msg = "FTP Change Folder fault , path="+FTP_path;
    		writeFtpLog(this.msg);
    		return false;
    	}else{
    		writeFtpLog("FTP Change Folder:"+FTP_path);
    	}
    	
    	if(!this.localMove()){
        	this.msg = "ftp move local file fault ";
    		return false;
        }
    	
    	List<String> downloadfile = new ArrayList<String>();
    	for(FtpFileVO filename: this.filterFile(this.getFTPFolderFiles())){
    		downloadfile.add(filename.getFile_name());    		
    	}
    	
    	if(!exitLocalPath()){
    		this.msg = "Ftp localhost folder path not exist ";
    		return false;
    	}
        
    	result = ftp.downloads(downloadfile, local_path);
    	
    	for (String key : result.keySet()) {
    		if(!result.get(key).booleanValue()){
    			this.msg = "Ftp Download file fault , file="+key;
    			writeFtpLog(this.msg);
        		return false;
    		}else{
    			writeFtpLog("Ftp Download file , file="+key);
    		}         
        }
        
        if(!this.FTPMove()){
        	//this.msg = "ftp move file fault ";
    		return false;
        }
        
        if(!this.FTPDelete()){
        	this.msg = "FTP Delete file fault ";
    		return false;
        }
        
        if(!ftp.disconnection()){
        	this.msg = "FTP Disconnection fault ";
        	writeFtpLog(this.msg);
    		return false;        	
        }else{
        	writeFtpLog("FTP Disconnection ");
        }
        
		return true;
	}
	
	public boolean execute(){
		if(this.isPush()){
			return this.push();
		}else{
			return this.pull();
		}
	}
	
	public void setPushFtpSubFolder(String yyyydoy){
		this.yyyy_doy = yyyydoy;
	}

}
