package mpss.smart;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;

import nspo.mpss.FtpFileVO;

public interface FtpFile {
	
	public enum FTP_TYPE {
	    FTP,SFTP
	} 
	
	public List<FtpFileVO> filterFile(List<FtpFileVO> files);
	
	public void setFileFilter(String filter);
	
	public void setPath(String source_path,String output_path);
	
	public boolean execute();	
	
	public Map<String,Boolean> getResult();
	
	public String getMessage();
	
	public boolean isPush();
	
	public boolean needFTPDelete();
	
	public boolean needFTPMove();	
	
	public String getMoveFTPSubPath();
	
	public void setMoveFTPSubPath(String path);
	
    public boolean needLocalDelete();
	
	public boolean needLocalMove();	
	
	public void setLocalMove(boolean flag);		
	
	public String getMoveLocalSubPath();
	
	public void setMoveLocalSubPath(String path);
	
	public void setPushFtpSubFolder(String yyyydoy);
	
	public void setType(FTP_TYPE type);
	
	public String pushFtpSubFolder();
	
	public void setLogFile(FileWriter writer);
	
		
}
