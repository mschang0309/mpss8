package mpss.smart;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;
import mpss.ftp.json.FtpFileVO;
import mpss.ftp.json.NameVO;
import mpss.ftp.json.source.JsonStream;
import mpss.smart.pull.AIPFtpFile;
import mpss.smart.pull.MTLFtpFile;
import mpss.smart.pull.OrbitFtpFile;
import mpss.smart.pull.RTSReplyFtpFile;
import mpss.smart.push.AIPConfirmFtpFile;
import mpss.smart.push.ASRFtpFile;
import mpss.smart.push.MPQFtpFile;
import mpss.smart.push.PassPlanFtpFile;
import mpss.smart.push.RTSRequestFtpFile;
import mpss.smart.push.TTQFtpFile;
import mpss.util.ResultVO;


public class SmartFtp {

	private JsonStream json;

	public SmartFtp(JsonStream json) {
		this.json = json;
	}

	public ResultVO pullMTLRpt() {
		ResultVO result = new ResultVO();
		FileWriter writer ;
		try{
			writer = new FileWriter(System.getProperty("FTP.Log.Path","Z:\\MPSS.Launch\\FS5\\reports\\FTP_LOG\\")+"ftp.log");			
			NameVO myvo = this.json.getFtpJsonData();
			if (myvo.getFtpfiles().containsKey("MTL")) {
				boolean backupflag = true;			
				writer.write("Pull MTL ......"+System.getProperty( "line.separator" ));			
				List<FtpFileVO> myftps = myvo.getFtpfiles().get("MTL").getFtpfiles();
				for (FtpFileVO vo : myftps) {
					FtpFile ftpfile = new MTLFtpFile(vo.getIp(), vo.getUser(),vo.getPassword(), vo.getPort());
					writer.write("Server IP:"+vo.getIp()+System.getProperty( "line.separator" ));
					ftpfile.setPath(vo.getFtp_path(), vo.getLocal_path());
					ftpfile.setFileFilter(vo.getFile_filter());
					ftpfile.setMoveFTPSubPath(vo.getFtp_backup_path());
					ftpfile.setLogFile(writer);
					if(backupflag){
						ftpfile.setLocalMove(true);
						ftpfile.setMoveLocalSubPath("backup");
						backupflag = false;
					}
					
					if (ftpfile.execute()) {
						Map<String, Boolean> key = ftpfile.getResult();
						for (String file : key.keySet()) {
							result.addList(file);
						}

					} else {
						result.setResult(false);
						result.clearList();
						result.setMsg(ftpfile.getMessage());
						writer.close();
						return result;
					}
				}
				writer.close();
				return result;
			} else {
				writer.close();
				return result;
			}
			
		}catch(Exception ex){
			result.setResult(false);
			result.clearList();
			result.setMsg("");			
			return result;
		}
		
	}
	
	public ResultVO pullOERpt() {
		ResultVO result = new ResultVO();
		FileWriter writer ;
		try{
			writer = new FileWriter(System.getProperty("FTP.Log.Path")+"ftp.log");
			NameVO myvo = this.json.getFtpJsonData();
			if (myvo.getFtpfiles().containsKey("OE")) {
				boolean backupflag = true;
				writer.write("Pull OE ......"+System.getProperty( "line.separator" ));
				List<FtpFileVO> myftps = myvo.getFtpfiles().get("OE").getFtpfiles();
				for (FtpFileVO vo : myftps) {
					FtpFile ftpfile = new OrbitFtpFile(vo.getIp(), vo.getUser(),vo.getPassword(), vo.getPort());
					writer.write("Server IP:"+vo.getIp()+System.getProperty( "line.separator" ));
					ftpfile.setPath(vo.getFtp_path(), vo.getLocal_path());
					ftpfile.setFileFilter(vo.getFile_filter());
					ftpfile.setMoveFTPSubPath(vo.getFtp_backup_path());
					ftpfile.setLogFile(writer);
					if(backupflag){
						ftpfile.setLocalMove(true);
						ftpfile.setMoveLocalSubPath("backup");
						backupflag = false;
					}
					if (ftpfile.execute()) {
						Map<String, Boolean> key = ftpfile.getResult();
						for (String file : key.keySet()) {
							result.addList(file);
						}

					} else {
						result.setResult(false);
						result.clearList();
						result.setMsg(ftpfile.getMessage());
						writer.close();
						return result;
					}
				}
				writer.close();
				return result;
			} else {
				writer.close();
				return result;
			}
		}catch(Exception ex){
			result.setResult(false);
			result.clearList();
			result.setMsg("");			
			return result;
		}
		
	}
	
	public ResultVO pullAIPMTLRpt() {
		ResultVO result = new ResultVO();
		FileWriter writer ;
		try{
			writer = new FileWriter(System.getProperty("FTP.Log.Path")+"ftp.log");
			NameVO myvo = this.json.getFtpJsonData();
			if (myvo.getFtpfiles().containsKey("AIPMTL")) {
				boolean backupflag = true;
				writer.write("Pull AIP MTL ......"+System.getProperty( "line.separator" ));
				List<FtpFileVO> myftps = myvo.getFtpfiles().get("AIPMTL").getFtpfiles();
				for (FtpFileVO vo : myftps) {
					FtpFile ftpfile = new AIPFtpFile(vo.getIp(), vo.getUser(),vo.getPassword(), vo.getPort());
					writer.write("Server IP:"+vo.getIp()+System.getProperty( "line.separator" ));
					ftpfile.setPath(vo.getFtp_path(), vo.getLocal_path());
					ftpfile.setFileFilter(vo.getFile_filter());
					ftpfile.setMoveFTPSubPath(vo.getFtp_backup_path());
					ftpfile.setLogFile(writer);
					if(backupflag){
						ftpfile.setLocalMove(true);
						ftpfile.setMoveLocalSubPath("backup");
						backupflag = false;
					}
					if (ftpfile.execute()) {
						Map<String, Boolean> key = ftpfile.getResult();
						for (String file : key.keySet()) {
							result.addList(file);
						}

					} else {
						result.setResult(false);
						result.clearList();
						result.setMsg(ftpfile.getMessage());
						return result;
					}
				}
				writer.close();
				return result;
			} else {
				writer.close();
				return result;
			}
		}catch(Exception ex){
			result.setResult(false);
			result.clearList();
			result.setMsg("");			
			return result;
		}
		
	}
	
	
	public ResultVO pullRTSReplyRpt() {
		ResultVO result = new ResultVO();
		FileWriter writer ;
		try{
			writer = new FileWriter(System.getProperty("FTP.Log.Path")+"ftp.log");
			NameVO myvo = this.json.getFtpJsonData();
			if (myvo.getFtpfiles().containsKey("RTSREPLY")) {
				writer.write("Pull RTS REPLY ......"+System.getProperty( "line.separator" ));
				boolean backupflag = true;
				List<FtpFileVO> myftps = myvo.getFtpfiles().get("RTSREPLY").getFtpfiles();
				for (FtpFileVO vo : myftps) {
					FtpFile ftpfile = new RTSReplyFtpFile(vo.getIp(), vo.getUser(),vo.getPassword(), vo.getPort());
					writer.write("Server IP:"+vo.getIp()+System.getProperty( "line.separator" ));
					ftpfile.setPath(vo.getFtp_path(), vo.getLocal_path());
					ftpfile.setFileFilter(vo.getFile_filter());
					ftpfile.setMoveFTPSubPath(vo.getFtp_backup_path());
					ftpfile.setLogFile(writer);
					if(backupflag){
						ftpfile.setLocalMove(true);
						ftpfile.setMoveLocalSubPath("backup");
						backupflag = false;
					}
					if (ftpfile.execute()) {
						Map<String, Boolean> key = ftpfile.getResult();
						for (String file : key.keySet()) {
							result.addList(file);
						}

					} else {
						result.setResult(false);
						result.clearList();
						result.setMsg(ftpfile.getMessage());
						writer.close();
						return result;
					}
				}
				writer.close();
				return result;
			} else {
				writer.close();
				return result;
			}
		}catch(Exception ex){
			result.setResult(false);
			result.clearList();
			result.setMsg("");			
			return result;
		}
		
	}
	
	
	public ResultVO pushPassPlan(String ftp_folder){		
		ResultVO result = new ResultVO();	
		FileWriter writer ;
		try{
			writer = new FileWriter(System.getProperty("FTP.Log.Path")+"ftp.log");
			NameVO myvo = this.json.getFtpJsonData();
			if (myvo.getFtpfiles().containsKey("PASS_PLAN")) {
				int i = 1;
				writer.write("Push PASS PLAN ......"+System.getProperty( "line.separator" ));
				List<FtpFileVO> myftps = myvo.getFtpfiles().get("PASS_PLAN").getFtpfiles();
				for (FtpFileVO vo : myftps) {				
					FtpFile ftpfile = new PassPlanFtpFile(vo.getIp(), vo.getUser(),vo.getPassword(), vo.getPort());
					writer.write("Server IP:"+vo.getIp()+System.getProperty( "line.separator" ));
					ftpfile.setPath(vo.getFtp_path(), vo.getLocal_path());
					ftpfile.setFileFilter(vo.getFile_filter());
					ftpfile.setMoveLocalSubPath(vo.getLocal_backup_path());
					ftpfile.setPushFtpSubFolder(ftp_folder);
					ftpfile.setMoveFTPSubPath(vo.getFtp_backup_path());
					ftpfile.setLogFile(writer);
					if(vo.getPort()==22){
						ftpfile.setType(FtpFile.FTP_TYPE.SFTP);	
					}else{
						ftpfile.setType(FtpFile.FTP_TYPE.FTP);	
					}
								
					if(myftps.size()==i){
						ftpfile.setLocalMove(true);								
					}
					
					if (ftpfile.execute()) {
						Map<String, Boolean> key = ftpfile.getResult();
						for (String file : key.keySet()) {
							result.addList(file);
						}

					} else {
						result.setResult(false);
						result.clearList();
						result.setMsg(ftpfile.getMessage());
						writer.close();
						return result;
					}
					i=i+1;
				}			
				writer.close();		
				return result;
			} else {
				writer.close();
				return result;
			}
		}catch(Exception ex){
			result.setResult(false);
			result.clearList();
			result.setMsg("");			
			return result;
		}
		
	}
	
	
	public ResultVO pushTTQLoad(String ftp_folder){
		ResultVO result = new ResultVO();	
		FileWriter writer ;
		try{
			writer = new FileWriter(System.getProperty("FTP.Log.Path")+"ftp.log");
			NameVO myvo = this.json.getFtpJsonData();
			if (myvo.getFtpfiles().containsKey("TTQLOAD")) {
				int i = 1;
				writer.write("Push TTQLOAD ......"+System.getProperty( "line.separator" ));
				List<FtpFileVO> myftps = myvo.getFtpfiles().get("TTQLOAD").getFtpfiles();
				for (FtpFileVO vo : myftps) {				
					FtpFile ftpfile = new TTQFtpFile(vo.getIp(), vo.getUser(),vo.getPassword(), vo.getPort());
					writer.write("Server IP:"+vo.getIp()+System.getProperty( "line.separator" ));
					ftpfile.setPath(vo.getFtp_path(), vo.getLocal_path());
					ftpfile.setFileFilter(vo.getFile_filter());
					ftpfile.setMoveLocalSubPath(vo.getLocal_backup_path());
					ftpfile.setPushFtpSubFolder(ftp_folder);
					ftpfile.setMoveFTPSubPath(vo.getFtp_backup_path());
					ftpfile.setLogFile(writer);
					if(vo.getPort()==22){
						ftpfile.setType(FtpFile.FTP_TYPE.SFTP);	
					}else{
						ftpfile.setType(FtpFile.FTP_TYPE.FTP);	
					}
								
					if(myftps.size()==i){
						ftpfile.setLocalMove(true);								
					}
					
					if (ftpfile.execute()) {
						Map<String, Boolean> key = ftpfile.getResult();
						for (String file : key.keySet()) {
							result.addList(file);
						}

					} else {
						result.setResult(false);
						result.clearList();
						result.setMsg(ftpfile.getMessage());
						writer.close();
						return result;
					}
					i=i+1;
				}			
				writer.close();	
				return result;
			} else {
				writer.close();
				return result;
			}
		}catch(Exception ex){
			result.setResult(false);
			result.clearList();
			result.setMsg("");			
			return result;
		}
		
	}
	
	public ResultVO pushMPQLoad(String ftp_folder){
		ResultVO result = new ResultVO();	
		FileWriter writer ;
		try{
			writer = new FileWriter(System.getProperty("FTP.Log.Path")+"ftp.log");
			NameVO myvo = this.json.getFtpJsonData();
			if (myvo.getFtpfiles().containsKey("MPQLOAD")) {
				int i = 1;
				writer.write("Push MPQLOAD ......"+System.getProperty( "line.separator" ));
				List<FtpFileVO> myftps = myvo.getFtpfiles().get("MPQLOAD").getFtpfiles();
				for (FtpFileVO vo : myftps) {				
					FtpFile ftpfile = new MPQFtpFile(vo.getIp(), vo.getUser(),vo.getPassword(), vo.getPort());
					writer.write("Server IP:"+vo.getIp()+System.getProperty( "line.separator" ));
					ftpfile.setPath(vo.getFtp_path(), vo.getLocal_path());
					ftpfile.setFileFilter(vo.getFile_filter());
					ftpfile.setMoveLocalSubPath(vo.getLocal_backup_path());
					ftpfile.setPushFtpSubFolder(ftp_folder);
					ftpfile.setMoveFTPSubPath(vo.getFtp_backup_path());
					ftpfile.setLogFile(writer);
					if(vo.getPort()==22){
						ftpfile.setType(FtpFile.FTP_TYPE.SFTP);	
					}else{
						ftpfile.setType(FtpFile.FTP_TYPE.FTP);	
					}
								
					if(myftps.size()==i){
						ftpfile.setLocalMove(true);								
					}
					
					if (ftpfile.execute()) {
						Map<String, Boolean> key = ftpfile.getResult();
						for (String file : key.keySet()) {
							result.addList(file);
						}

					} else {
						result.setResult(false);
						result.clearList();
						result.setMsg(ftpfile.getMessage());
						writer.close();
						return result;
					}
					i=i+1;
				}			
				writer.close();		
				return result;
			} else {
				writer.close();
				return result;
			}
		}catch(Exception ex){
			result.setResult(false);
			result.clearList();
			result.setMsg("");			
			return result;
		}
		
	}
	
	public ResultVO pushAIPConfirm(String ftp_folder){
		ResultVO result = new ResultVO();	
		FileWriter writer ;
		try{
			writer = new FileWriter(System.getProperty("FTP.Log.Path")+"ftp.log");
			NameVO myvo = this.json.getFtpJsonData();
			if (myvo.getFtpfiles().containsKey("AIPCONFIRM")) {
				int i = 1;
				writer.write("Push AIP CONFIRM ......"+System.getProperty( "line.separator" ));
				List<FtpFileVO> myftps = myvo.getFtpfiles().get("AIPCONFIRM").getFtpfiles();
				for (FtpFileVO vo : myftps) {				
					FtpFile ftpfile = new AIPConfirmFtpFile(vo.getIp(), vo.getUser(),vo.getPassword(), vo.getPort());
					writer.write("Server IP:"+vo.getIp()+System.getProperty( "line.separator" ));
					ftpfile.setPath(vo.getFtp_path(), vo.getLocal_path());
					ftpfile.setFileFilter(vo.getFile_filter());
					ftpfile.setMoveLocalSubPath(vo.getLocal_backup_path());
					ftpfile.setPushFtpSubFolder(ftp_folder);
					ftpfile.setMoveFTPSubPath(vo.getFtp_backup_path());
					ftpfile.setLogFile(writer);
					if(vo.getPort()==22){
						ftpfile.setType(FtpFile.FTP_TYPE.SFTP);	
					}else{
						ftpfile.setType(FtpFile.FTP_TYPE.FTP);	
					}
								
					if(myftps.size()==i){
						ftpfile.setLocalMove(true);								
					}
					
					if (ftpfile.execute()) {
						Map<String, Boolean> key = ftpfile.getResult();
						for (String file : key.keySet()) {
							result.addList(file);
						}

					} else {
						result.setResult(false);
						result.clearList();
						result.setMsg(ftpfile.getMessage());
						writer.close();
						return result;
					}
					i=i+1;
				}			
				writer.close();		
				return result;
			} else {
				writer.close();
				return result;
			}
		}catch(Exception ex){
			result.setResult(false);
			result.clearList();
			result.setMsg("");			
			return result;
		}
		
	}
	
	public ResultVO pushRtsRequest(String ftp_folder){
		ResultVO result = new ResultVO();	
		FileWriter writer ;
		try{
			writer = new FileWriter(System.getProperty("FTP.Log.Path")+"ftp.log");
			NameVO myvo = this.json.getFtpJsonData();
			if (myvo.getFtpfiles().containsKey("RTSREQUEST")) {
				int i = 1;
				writer.write("Push RTS REQUEST ......"+System.getProperty( "line.separator" ));
				List<FtpFileVO> myftps = myvo.getFtpfiles().get("RTSREQUEST").getFtpfiles();
				for (FtpFileVO vo : myftps) {				
					FtpFile ftpfile = new RTSRequestFtpFile(vo.getIp(), vo.getUser(),vo.getPassword(), vo.getPort());
					writer.write("Server IP:"+vo.getIp()+System.getProperty( "line.separator" ));
					ftpfile.setPath(vo.getFtp_path(), vo.getLocal_path());
					ftpfile.setFileFilter(vo.getFile_filter());
					ftpfile.setMoveLocalSubPath(vo.getLocal_backup_path());
					ftpfile.setPushFtpSubFolder(ftp_folder);
					ftpfile.setMoveFTPSubPath(vo.getFtp_backup_path());
					ftpfile.setLogFile(writer);
					if(vo.getPort()==22){
						ftpfile.setType(FtpFile.FTP_TYPE.SFTP);	
					}else{
						ftpfile.setType(FtpFile.FTP_TYPE.FTP);	
					}
								
					if(myftps.size()==i){
						ftpfile.setLocalMove(true);								
					}
					
					if (ftpfile.execute()) {
						Map<String, Boolean> key = ftpfile.getResult();
						for (String file : key.keySet()) {
							result.addList(file);
						}

					} else {
						result.setResult(false);
						result.clearList();
						result.setMsg(ftpfile.getMessage());
						writer.close();
						return result;
					}
					i=i+1;
				}			
				writer.close();		
				return result;
			} else {
				writer.close();
				return result;
			}
		}catch(Exception ex){
			result.setResult(false);
			result.clearList();
			result.setMsg("");			
			return result;
		}
		
	}
	
	
	public ResultVO pushASR(String ftp_folder){
		ResultVO result = new ResultVO();	
		FileWriter writer ;
		try{
			writer = new FileWriter(System.getProperty("FTP.Log.Path")+"ftp.log");
			NameVO myvo = this.json.getFtpJsonData();
			if (myvo.getFtpfiles().containsKey("ASR")) {
				int i = 1;
				writer.write("Push ASR ......"+System.getProperty( "line.separator" ));
				List<FtpFileVO> myftps = myvo.getFtpfiles().get("ASR").getFtpfiles();
				for (FtpFileVO vo : myftps) {				
					FtpFile ftpfile = new ASRFtpFile(vo.getIp(), vo.getUser(),vo.getPassword(), vo.getPort());
					writer.write("Server IP:"+vo.getIp()+System.getProperty( "line.separator" ));
					ftpfile.setPath(vo.getFtp_path(), vo.getLocal_path());
					ftpfile.setFileFilter(vo.getFile_filter());
					ftpfile.setMoveLocalSubPath(vo.getLocal_backup_path());
					ftpfile.setPushFtpSubFolder(ftp_folder);
					ftpfile.setMoveFTPSubPath(vo.getFtp_backup_path());
					ftpfile.setLogFile(writer);
					if(vo.getPort()==22){
						ftpfile.setType(FtpFile.FTP_TYPE.SFTP);	
					}else{
						ftpfile.setType(FtpFile.FTP_TYPE.FTP);	
					}
								
					if(myftps.size()==i){
						ftpfile.setLocalMove(true);								
					}
					
					if (ftpfile.execute()) {
						Map<String, Boolean> key = ftpfile.getResult();
						for (String file : key.keySet()) {
							result.addList(file);
						}

					} else {
						result.setResult(false);
						result.clearList();
						result.setMsg(ftpfile.getMessage());
						writer.close();
						return result;
					}
					i=i+1;
				}			
				writer.close();		
				return result;
			} else {
				writer.close();
				return result;
			}
		}catch(Exception ex){
			result.setResult(false);
			result.clearList();
			result.setMsg("");			
			return result;
		}
		
	}

}
