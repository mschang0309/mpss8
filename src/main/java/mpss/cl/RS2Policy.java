package mpss.cl;

import mpss.common.jpa.Activity;

public class RS2Policy {

	// determines if passed activity is atomic for schedule loading (can activity be loaded into
    // schedule by itself or does it require other associated activities - activities in chains
    // REC/PBK/DEL for example
	public boolean AtomicForScheduleLoading(Activity act)
	{
	    boolean atomic = true;

	    // if RSI related and REC/PBK/DEL then need entire chain to load for scheduling
	    if(act.getArlentrytype().equalsIgnoreCase("RSI") || !act.getActsubtype().equalsIgnoreCase("DDT"))
	        atomic = false;

	    // DELete activities are not atomic for loading - need REC/PBK/DEL chain when loading
	    if(act.getArlentrytype().equalsIgnoreCase("DELETE"))
	        atomic = false;

	    return atomic;
	}
	
	public boolean NeedsRecorder(Activity act)
	{
		if (act.getArlentrytype().equalsIgnoreCase("RSI") || act.getArlentrytype().equalsIgnoreCase("ISUAL")|| act.getArlentrytype().equalsIgnoreCase("DELETE"))
			return true;
		
		return false;
	}
	
}
