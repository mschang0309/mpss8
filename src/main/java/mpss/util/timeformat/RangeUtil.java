package mpss.util.timeformat;

import java.sql.Timestamp;
import java.util.Calendar;
import com.google.common.collect.Range;

public class RangeUtil {


	public static TimeRange startTimeAppendHourMinu (Timestamp starttime, int hoursDur, int minutesDur){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(starttime.getTime());

        cal.add(Calendar.HOUR_OF_DAY, hoursDur);
        cal.add(Calendar.MINUTE, minutesDur);
        Timestamp endTime=new Timestamp(cal.getTimeInMillis());

        return new TimeRange(starttime, endTime);    
	}
	
	public static TimeRange startTimeAppendSec (Timestamp starttime, int secsDur){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(starttime.getTime());

        cal.add(Calendar.SECOND, secsDur);
        Timestamp endTime= new Timestamp(cal.getTimeInMillis());
        
        return new TimeRange(starttime, endTime);
	}

	
	public static TimeRange timeAppendSec (Timestamp targettime, int firstSec, int secondSec){
        Calendar cal = Calendar.getInstance();
        
        cal.setTimeInMillis(targettime.getTime());
        cal.add(Calendar.SECOND, firstSec);
        Timestamp starttime =  new Timestamp(cal.getTimeInMillis());
        
        cal.setTimeInMillis(targettime.getTime());
        cal.add(Calendar.SECOND, secondSec);
        Timestamp endTime =  new Timestamp(cal.getTimeInMillis());

        return new TimeRange(starttime, endTime);
	}
	
	public static TimeRange rangeAppendSec (Range<Timestamp> tRange, int firstSec, int secondSec){
        Calendar cal = Calendar.getInstance();
        
        cal.setTimeInMillis(tRange.lowerEndpoint().getTime());
        cal.add(Calendar.SECOND, firstSec);
        Timestamp starttime =  new Timestamp(cal.getTimeInMillis());
        
        cal.setTimeInMillis(tRange.upperEndpoint().getTime());
        cal.add(Calendar.SECOND, secondSec);
        Timestamp endTime =  new Timestamp(cal.getTimeInMillis());
 
        return new TimeRange(starttime, endTime);
	}
	
	public static TimeRange rangeAppendSec (TimeRange tRange, int firstSec, int secondSec){
        Calendar cal = Calendar.getInstance();
        
        cal.setTimeInMillis(tRange.first.getTime());
        cal.add(Calendar.SECOND, firstSec);
        Timestamp starttime =  new Timestamp(cal.getTimeInMillis());
        
        cal.setTimeInMillis(tRange.second.getTime());
        cal.add(Calendar.SECOND, secondSec);
        Timestamp endTime =  new Timestamp(cal.getTimeInMillis());
 
        return tRange.closed(starttime, endTime);
	}
	
	
	
}
