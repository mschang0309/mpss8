package mpss.util.timeformat;

import java.util.Calendar;
import java.sql.Timestamp;

public class YearDayTimeOffset {

    int year;
    int dayOfYear;
    int secondsOfDay;
    int milliseconds;
    
    public YearDayTimeOffset() {
    	java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
    	fromTimestamp(currentTimestamp);
    }
    
    public YearDayTimeOffset(Timestamp timestamp) {
    	fromTimestamp(timestamp);
    }
    
    private void fromTimestamp(Timestamp timestamp) {
        Calendar cal = Calendar.getInstance();

        cal.setTimeInMillis(timestamp.getTime());

        this.year = cal.get(Calendar.YEAR);
        this.dayOfYear = cal.get(Calendar.DAY_OF_YEAR);
        int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);
        int minuteOfHour = cal.get(Calendar.MINUTE );
        int secondOfMinute = cal.get(Calendar.SECOND );
        this.secondsOfDay = (hourOfDay * 60 + minuteOfHour) * 60 + secondOfMinute;
        this.milliseconds = cal.get(Calendar.MILLISECOND );
    }
    
    public static Timestamp toTimestamp(int year, int dayOfYear, int secondsOfDay) {
        int secondOfMinute = secondsOfDay % 60;
        int minutesOfHour = (secondsOfDay / 60) % 60;
        int hourOfDay = secondsOfDay / 3600;
        
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_YEAR, dayOfYear);
        cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
        cal.set(Calendar.MINUTE, minutesOfHour);
        cal.set(Calendar.SECOND, secondOfMinute);
        cal.set(Calendar.MILLISECOND , 0);

        return new Timestamp(cal.getTimeInMillis());   
    }
    
    public int getYear() {
        return this.year;
    }

    public int getDayOfYear() {
        return this.dayOfYear;
    }

    public int getSecondsOfDay() {
        return this.secondsOfDay;
    }
    
}