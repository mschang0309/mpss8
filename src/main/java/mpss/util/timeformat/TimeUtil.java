package mpss.util.timeformat;

import java.sql.Timestamp;
import java.util.Calendar;

public class TimeUtil {


	
	public static Timestamp timeAppendSec (Timestamp time, int secs){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time.getTime());

        cal.add(Calendar.SECOND, secs);
        Timestamp newTime=new Timestamp(cal.getTimeInMillis());
        
        return newTime; 
	}
	
	public static Timestamp timeAppendDay (Timestamp time, int days){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time.getTime());

        cal.add(Calendar.DAY_OF_MONTH, days);
        Timestamp newTime=new Timestamp(cal.getTimeInMillis());
        
        return newTime; 
	}
	
	public static Timestamp timeAppendHour (Timestamp time, int hours){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time.getTime());

        cal.add(Calendar.HOUR_OF_DAY, hours);
        Timestamp newTime=new Timestamp(cal.getTimeInMillis());
        
        return newTime; 
	}
	
	
	// time1 ==time2 return true ; 
	public static boolean compareTimestamp(Timestamp time1,Timestamp time2){
		int duration = (int) ((time1.getTime() - time2.getTime())/1000);
		if(duration < 2 && duration > -2){
			return true;
		}else{
			return false;
		}		
	}
	
	public static double getDurationTimestamp(Timestamp time1,Timestamp time2){
		return ((time1.getTime() - time2.getTime())/1000);				
	}
}
