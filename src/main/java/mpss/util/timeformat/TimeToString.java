package mpss.util.timeformat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.sql.Timestamp;

public class TimeToString {

    
    // convert Timestamp to EPOCH time format:
    //    YYYY DOY HH MM SS 00
    public static String AsLoadTime(Timestamp loadTime)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyy DDD HH mm ss 00");    
    	String timeString = df.format(loadTime);    
	    return timeString;
	}
    
    // convert Timestamp to EPOCH time format:
    //    YYY-JJJ-HH:MM:SS.mmm
    public static String AsEpochTime(Timestamp time)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyy-DDD-HH:mm:ss.SSS");    
    	String timeString = df.format(time).substring(1);    
	    return timeString;
	}
    
    //    YYYYJJJHHMMSS
    public static String AsNameTime(Timestamp NameTime)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyyDDDHHmmss");    
    	String timeString = df.format(NameTime);    
	    return timeString;
	}
    
    //    YYYYJJJHHMMSS
    public static String AsNameTime(Date NameTime)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyyDDDHHmmss");    
    	String timeString = df.format(NameTime);    
	    return timeString;
	}
    
    //    YYYY/JJJ/HH:MM:SS
    public static String AsLogTime(Timestamp LogTime)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyy/DDD/HH:mm:ss");
    	//df.setTimeZone(TimeZone.getTimeZone("UTC"));    
    	String timeString = df.format(LogTime);    
	    return timeString;
	}
    //   YYYY/JJJ/HH:mm:ss.SSS
    public static String AsFullTime(Timestamp LogTime)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyy/DDD/HH:mm:ss.SSS");
    	//df.setTimeZone(TimeZone.getTimeZone("UTC"));    
    	String timeString = df.format(LogTime);    
	    return timeString;
	}
    
    //    [YYYY/JJJ/HH:MM:SS - YYYY/JJJ/HH:MM:SS] 
    public static String AsLogTime(TimeRange tr)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyy/DDD/HH:mm:ss");
    	//df.setTimeZone(TimeZone.getTimeZone("UTC"));    
    	String timeString = "[" + df.format(tr.first) + " - " + df.format(tr.second) + "]";    
	    return timeString;
	}
    
    //    YYYY/JJJ HH:MM:SS
    public static String AsRptTime(Timestamp LogTime)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyy/DDD HH:mm:ss");    
    	String timeString = df.format(LogTime);    
	    return timeString;
	}
    
    //    MM/DD/YY HH:MM:SS
    public static String AsShortTime(Timestamp ShortTime)
	{
    	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy HH:mm:ss");    
    	String timeString = df.format(ShortTime);    
	    return timeString;
	}
    
    public static String AsRTSTime(Timestamp time)
	{
    	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy,DDD,HH:mm:ss");    
    	String timeString = df.format(time);    
	    return timeString;
	}
    
    //    MM/DD/YYYY HH:MM:SS
    public static String AsLongTime(Timestamp LongTime)
	{
    	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");    
    	String timeString = df.format(LongTime);    
	    return timeString;
	}
    
    //    YYYY/MM/DD HH:MM:SS
    public static String AsStdTime(Timestamp LongTime)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");    
    	String timeString = df.format(LongTime);    
	    return timeString;
	}
    
    //    YYYY-MM-DDTHH:MM:SSZ
    public static String AsRTSXMLTime(Date LongTime)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");    
    	String timeString = df.format(LongTime);    
	    return timeString.replace(" ", "T") + "Z";
	}
    
    //  YYYYMMDDTHHMMSS
    public static String AsRTSNameTime(Date LongTime)
	{
  	SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HHmmss");    
  	String timeString = df.format(LongTime);    
	    return timeString.replace(" ", "T");
	}
    
    //    HH:MM:SS
    public static String AsTime(Timestamp LongTime)
	{
    	SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");    
    	String timeString = df.format(LongTime);    
	    return timeString;
	}
    
    //    YYYYJJJ
    public static String AsNameDate(Timestamp NameDate)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyyDDD");    
    	String timeString = df.format(NameDate);    
	    return timeString;
	}
    
    //    YYYY/JJJ
    public static String AsLogDate(Timestamp LogDate)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyy/DDD");    
    	String timeString = df.format(LogDate);    
	    return timeString;
	}
    
    //    MM/DD/YY
    public static String AsShortDate(Timestamp ShortTime)
	{
    	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy");    
    	String timeString = df.format(ShortTime);    
	    return timeString;
	}
    
    //    YYYYMMDD
    public static String AsLongDate(Timestamp LongDate)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");    
    	String timeString = df.format(LongDate);    
	    return timeString;
	}
    
    //    YYYY/MM/DD
    public static String AsLongSlashDate(Timestamp LongDate)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");    
    	String timeString = df.format(LongDate);    
	    return timeString;
	}
    
    //    YYYY-MM-DD
    public static String AsLongDashDate(Timestamp LongDate)
	{
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");    
    	String timeString = df.format(LongDate);    
	    return timeString;
	}
    
    public static String getYYYYMMDDTime(Date LongTime)
   	{
     	SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");    
     	String timeString = df.format(LongTime);    
   	    return timeString;
   	}
}