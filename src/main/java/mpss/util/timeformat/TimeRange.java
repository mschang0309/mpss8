package mpss.util.timeformat;

import java.sql.Timestamp;
import com.google.common.collect.Range;

public class TimeRange {

	public Timestamp first;
	public Timestamp second;
	public int duration;	//seconds
	
//	private Range<Timestamp> range;	
//	public Timestamp first (){
//		return this.first; 
//	}	
//	
//	public Timestamp second (){
//		return this.second; 
//	}
//	
//	public int duration (){
//		return this.duration; 
//	}
	public TimeRange(){

	}
	
	public TimeRange(Timestamp starttime, Timestamp endtime){
		this.first = starttime;
		this.second = endtime; 
		this.duration = (int) ((second.getTime() - first.getTime())/1000);
	}
	
	public TimeRange closed (Timestamp starttime, Timestamp endtime){
		this.first = starttime;
		this.second = endtime; 
		this.duration = (int) ((second.getTime() - first.getTime())/1000);
		return this;
	}
	
	public boolean contains (Timestamp checkTime){
		
		if (second.before(checkTime) || first.after(checkTime))
			return false;
		
		return true;
	}
	
	public boolean encloses (TimeRange checkTR){
		
		if (contains(checkTR.first) && contains(checkTR.second))
			return true;
		
		return false;
	}
	
	public boolean overlaps (Range<Timestamp> checkTR){
		
		if (checkTR.lowerEndpoint().after(second) || checkTR.upperEndpoint().before(first))
			return false;
		
		return true;
	}
	
	public boolean overlaps (TimeRange checkTR){
		
		if (checkTR.first.after(second) || checkTR.second.before(first))
			return false;
		
		return true;
	}
	
	public TimeRange contractTo (Range<Timestamp> checkTR){
		if (overlaps(checkTR))
		{
			first = first.before(checkTR.lowerEndpoint()) ? checkTR.lowerEndpoint() : first;
			second = second.after(checkTR.upperEndpoint()) ? checkTR.upperEndpoint() : second;
			duration = (int) ((second.getTime() - first.getTime())/1000);
	    	return this;
		}
		
		return null;
	}
	
	public TimeRange contractTo (TimeRange checkTR){
		if (overlaps(checkTR))
		{
			first = first.before(checkTR.first) ? checkTR.first : first;
			second = second.after(checkTR.second) ? checkTR.second : second;
			duration = (int) ((second.getTime() - first.getTime())/1000);
	    	return this;
		}
		
		return null;
	}
	
	public TimeRange expandTo (TimeRange expandTR){

			first = first.before(expandTR.first) ? first : expandTR.first;
			second = second.after(expandTR.second) ? second : expandTR.second;
			duration = (int) ((second.getTime() - first.getTime())/1000);
	    	return this;
	}
	
//	public Range<Timestamp> startTimeAppendHourMinu (Timestamp starttime, int hoursDur, int minutesDur){
//        Calendar cal = Calendar.getInstance();
//        cal.setTimeInMillis(starttime.getTime());
//
//        cal.add(Calendar.HOUR_OF_DAY, hoursDur);
//        cal.add(Calendar.MINUTE, minutesDur);
//        Timestamp endTime=new Timestamp(cal.getTimeInMillis());
//        
//        this.first = starttime;
//        this.second = endTime;
//        this.duration = (int) ((endTime.getTime() - starttime.getTime())/1000);
//        this.range = Range.closed(starttime, endTime); 
//		return this.range; 
//	}
//	
//	public Range<Timestamp> startTimeAppendSec (Timestamp starttime, int secsDur){
//        Calendar cal = Calendar.getInstance();
//        cal.setTimeInMillis(starttime.getTime());
//
//        cal.add(Calendar.SECOND, secsDur);
//        Timestamp endTime=new Timestamp(cal.getTimeInMillis());
//        
//        this.first = starttime;
//        this.second = endTime;
//        this.duration = (int) ((endTime.getTime() - starttime.getTime())/1000);
//        this.range = Range.closed(starttime, endTime); 
//		return this.range; 
//	}
//
//	public Range<Timestamp> timeAppendSec (Timestamp targettime, int firstSec, int secondSec){
//        Calendar cal = Calendar.getInstance();
//        
//        cal.setTimeInMillis(targettime.getTime());
//        cal.add(Calendar.SECOND, firstSec);
//        this.first = new Timestamp(cal.getTimeInMillis());
//        
//        cal.setTimeInMillis(targettime.getTime());
//        cal.add(Calendar.SECOND, secondSec);
//        this.second = new Timestamp(cal.getTimeInMillis());
//
//        this.duration = secondSec - firstSec;
//        this.range = Range.closed(this.first, this.second); 
//		return this.range; 
//	}
//	
//	public Range<Timestamp> rangeAppendSec (Range<Timestamp> tRange, int firstSec, int secondSec){
//        Calendar cal = Calendar.getInstance();
//        
//        cal.setTimeInMillis(tRange.lowerEndpoint().getTime());
//        cal.add(Calendar.SECOND, firstSec);
//        this.first = new Timestamp(cal.getTimeInMillis());
//        
//        cal.setTimeInMillis(tRange.upperEndpoint().getTime());
//        cal.add(Calendar.SECOND, secondSec);
//        this.second = new Timestamp(cal.getTimeInMillis());
//
//        this.duration = (int) ((second.getTime() - first.getTime())/1000);
//        this.range = Range.closed(this.first, this.second); 
//		return this.range; 
//	}
//	
//	public Timestamp timeAppendSec (Timestamp time, int secs){
//        Calendar cal = Calendar.getInstance();
//        cal.setTimeInMillis(time.getTime());
//
//        cal.add(Calendar.SECOND, secs);
//        Timestamp newTime=new Timestamp(cal.getTimeInMillis());
//        
//        return newTime; 
//	}
	
}
