package mpss.util.timeformat;

import java.util.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import mpss.schedule.base.GlTime;
import mpss.schedule.base.GlTimeRange;

/**
 * 
 * @author Administrator
 */
public class DateUtil {

	public static String dateToString(Date dateVal, String dateFormat) {
		String dateStr = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			if (dateVal != null) {
				dateStr = sdf.format(dateVal);
			}
		} catch (Exception e) {
			System.out.println(" dateToString " + dateFormat + " error");
		}
		return dateStr;
	}

	public static Date stringToDate(String dateStr, String dateFormat) {
		Date dateVal = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			
			if (dateStr != null) {
				dateVal = sdf.parse(dateStr);
			}
		} catch (Exception e) {
			System.out.println(dateStr + " stringToDate " + dateFormat + " error");
		}		
		return dateVal;
	}
	
	public static Timestamp stringToTimestamp(String dateStr, String dateFormat) {
		Timestamp dateVal = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			
			if (dateStr != null) {
				Date date = sdf.parse(dateStr);
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(date.getTime());
		        Timestamp newTime=new Timestamp(cal.getTimeInMillis());		        
		        return newTime; 
			}
		} catch (Exception e) {
			System.out.println(dateStr + " stringToTimestamp " + dateFormat + " error");
			
		}		
		return dateVal;
	}

	public static int getDuration(Date startDate, Date endDate) {
		int duration = 0;
		try {
			duration = (int) (endDate.getTime() - startDate.getTime()) / 1000;
		} catch (Exception e) {
			System.out.println("getDuration " + startDate + " error");
		}
		return duration;
	}

	public static int parseHmsToSeconds(Date dStart) {
		int starttime = 0;
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dStart);

			int hour = cal.get(Calendar.HOUR_OF_DAY);
			int min = cal.get(Calendar.MINUTE);
			int sec = cal.get(Calendar.SECOND);
			starttime = hour * 3600 + min * 60 + sec;
		} catch (Exception e) {
			System.out.println("parseHmsToSeconds " + dStart + " error");
		}
		return starttime;
	}

	public static String parseSecondsToHms(int seconds) {
		int ss = 0;
		int mm = 0;
		int hh = 0;
		try {

			ss = seconds % 60;
			mm = seconds / 60;

			if (mm >= 60) {
				hh = mm / 60;
				mm = mm % 60;

			}

		} catch (Exception e) {
			System.out.println("parseSecondsToHms " + seconds + " error");
			System.out.println(e.toString());
		}
		return String.format("%02d", hh) + ":" + String.format("%02d", mm) + ":" + String.format("%02d", ss);
	}

	public static int getDateYear(Date dStart) {
		int year = 0;
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dStart);
			year = cal.get(Calendar.YEAR);

		} catch (Exception e) {
			System.out.println("getDateYear " + dStart + " error");
		}
		return year;
	}

	public static int getDateDOY(Date dStart) {
		int day = 0;
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dStart);
			day = cal.get(Calendar.DAY_OF_YEAR);

		} catch (Exception e) {
			System.out.println("getDateDOY " + dStart + " error");
		}
		return day;
	}

	public static Timestamp dateToTimestamp(Date date) {
		Timestamp tsStart = null;
		try {
			tsStart = new Timestamp(date.getTime());
		} catch (Exception e) {
			System.out.println("dateToTimestamp " + date + " error");
			return null;
		}
		return tsStart;
	}

	public static long getClSeconds(int year, int month, int date, int hour, int min, int sec) {
		long seconds;
		try {
			Calendar calendar1 = Calendar.getInstance();
			Calendar calendar2 = Calendar.getInstance();
			calendar1.set(1990, 0, 1, 0, 0, 0);

			calendar2.set(year, month, date, hour, min, sec);
			long milliseconds1 = calendar1.getTimeInMillis();
			long milliseconds2 = calendar2.getTimeInMillis();
			long diff = milliseconds2 - milliseconds1;
			seconds = diff / 1000;

		} catch (Exception ex) {
			System.out.println("dateToTimestamp " + date + " error");
			return 0;
		}
		return seconds;
	}
	
	public static GlTime timestampToGlTIme(Timestamp time){	
		if(time==null){
			return new GlTime();
		}else{
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date(time.getTime()));			
			long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
			return new GlTime(sec1);
		}				
	}
	
		
	 public static GlTimeRange getGlTimeRange(Date startTime,Date EndTime) {
        Calendar cal=Calendar.getInstance();
       
       cal.setTime(startTime);
       long clSec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

       cal.setTime(EndTime);
       long clSec2 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
       
       return new GlTimeRange(new GlTime(clSec1), new GlTime(clSec2));
       
   }
	 
	 
	 
	 public static Date GlTimeAddSec(GlTime time,int sec){
		 Date temp = stringToDate(time.asString("%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S");
		 Calendar cal=Calendar.getInstance();	       
	     cal.setTime(temp);
	     cal.add(Calendar.SECOND, sec);
		 return cal.getTime();
	 }
	
	 public static Date DateAddSec(Date time,int sec){

         Calendar cal=Calendar.getInstance();

         cal.setTime(time);

         cal.add(Calendar.SECOND, sec);

         return cal.getTime();

     }
	 
	 public static Date timestampToDate(Timestamp time){
		 
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(time.getTime()));		
		return cal.getTime();	
		
	 }
	 
	 public static boolean checkDateInPeriod(String time ,String time_s,int gap){
		 Date s_date =  stringToDate(time,"yyyy-DDD-HH:mm:ss");
		 Date b_date_s =  DateAddSec(stringToDate(time_s,"yyyy-DDD-HH:mm:ss"),-gap);
		 Date b_date_e =  DateAddSec(stringToDate(time_s,"yyyy-DDD-HH:mm:ss"),gap);
		 if (s_date.compareTo(b_date_s) != -1 && s_date.compareTo(b_date_e) == -1) {
		     return true;
		 }else{
			 return false;
		 }
		 
	 }
	 
	 public static String timestampAddSecToString(Timestamp time, String dateFormat,int sec){
		 
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date(time.getTime()));	
			cal.add(Calendar.SECOND, sec);
			String dateStr = null;
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);				
				dateStr = sdf.format(cal.getTime());				
			} catch (Exception e) {
				System.out.println(" timestampAddSecToString  error ="+e.getMessage());
			}
			return dateStr;	
			
		 }
	 
	 public static Timestamp timestampAddSec(Timestamp time, String dateFormat,int sec){		 
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date(time.getTime()));	
			cal.add(Calendar.SECOND, sec);
			String dateStr = null;
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);				
				dateStr = sdf.format(cal.getTime());				
				return DateUtil.dateToTimestamp(sdf.parse(dateStr));
			} catch (Exception e) {
				System.out.println(" timestampAddSec  error ="+e.getMessage());
				return null;	
			}
			
			
	 }


}
