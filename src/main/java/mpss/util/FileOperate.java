package mpss.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class FileOperate {
	
	/**
	* new folder
	* @param folderPath String, ex= c:/fqf
	* @return boolean
	*/
	
	
	public boolean newFolder(String folderPath) {
		try {
			String filePath = folderPath;
			filePath = filePath.toString();
			java.io.File myFilePath = new java.io.File(filePath);
			if (!myFilePath.exists()) {
				myFilePath.mkdir();
			}
			return true;
		}catch(Exception e) {
			System.out.println("FileOperate newFolder ex="+e.getMessage());
			return false;
		}
	}
	
	public boolean exitFolder(String folderPath) {
		try {
			String filePath = folderPath;
			filePath = filePath.toString();
			java.io.File myFilePath = new java.io.File(filePath);
			return myFilePath.exists();			
		}catch(Exception e) {
			System.out.println("FileOperate exitFolder ex="+e.getMessage());
			return false;
		}
	}

	 
	/**
	* new file
	* @param filePathAndNameString path + filename , ex= c:/fqf.txt
	* @param fileContent String 
	* @return boolean
	*/
	public boolean newFile(String filePathAndName,String fileContent) {	 
		try{
			String filePath = filePathAndName;
			filePath = filePath.toString();
			File myFilePath = new File(filePath);
			if (!myFilePath.exists()) {
				myFilePath.createNewFile();
			}
			FileWriter resultFile = new FileWriter(myFilePath);
			PrintWriter myFile = new PrintWriter(resultFile);
			String strContent = fileContent;
			myFile.println(strContent);
			resultFile.close();
			return true;
		}catch(Exception e) {
			System.out.println("FileOperate newFile ex="+e.getMessage());	
			return false;
		}
	}

	 
	/**
	* delete file
	* @param filePathAndNameString path + filename ,ex = c:/f.txt
	* @return boolean
	*/
	public boolean delFile(String filePathAndName){
		try {		
			java.io.File myDelFile = new java.io.File(filePathAndName);
			if(myDelFile.delete()){
				//System.out.println("fileOperate delete true");
			}else{
				System.out.println("fileOperate delete false");
			}	
			
			myDelFile = null;
			return true;
		}catch(Exception e) {
			System.out.println("FileOperate delFile ex="+e.getMessage());	
			return false;
		}
	}

	 
	/**
	* delete folder
	* @param filePathAndNameString  path + filename, ex =c:/fqf
	* @param fileContentString
	* @return boolean
	*/
	public boolean delFolder(String folderPath) {
		try {
			delAllFile(folderPath); 
			String filePath = folderPath;
			filePath = filePath.toString();
			java.io.File myFilePath = new java.io.File(filePath);
			myFilePath.delete(); 
			return true;
		}catch(Exception e) {
			System.out.println("FileOperate delFile ex="+e.getMessage());
			return false;
		}
	}

	 
	/**
	*  delete all file in folder
	* @param path String folder path, ex= c:/fqf
	*/
	public void delAllFile(String path) {
		File file =new File(path);
		if(!file.exists()) {
			return ;
		}
		if(!file.isDirectory()) {
			return;
		}
		String[]tempList = file.list();
		File temp =null;
		for (int i =0; i < tempList.length; i++) {
			if (path.endsWith(File.separator)) {
				temp = new File(path + tempList[i]);
			}
			else {
				temp = new File(path + File.separator + tempList[i]);
			}
			if (temp.isFile()) {
				temp.delete();
			}
			if (temp.isDirectory()) {
				delAllFile(path+File.separator+ tempList[i]);
				delFolder(path+File.separator+ tempList[i]);
			}
		}
	}
	
	/**
	*  get all file in folder
	* @param path String folder path, ex= c:/fqf
	*/
	public List<String> getAllFile(String path) {
		List<String> files = new ArrayList<String>();
		File file =new File(path);
		if(!file.exists()) {
			return files;
		}
		if(!file.isDirectory()) {
			return files;
		}
		
		String[] tempList = file.list();		
		for (int i =0; i < tempList.length; i++) {
			File file_temp = new File(path+tempList[i]);
			if(!file_temp.isDirectory()){
				files.add(tempList[i]);	
			}
						
		}
		return files;
	}

	 
	/**
	* copy file
	* @param oldPath String  ：c:/fqf.txt
	* @param newPath String  ：f:/fqf.txt
	* @return boolean
	*/
	public boolean copyFile(String oldPath, String newPath) {
		try {
			@SuppressWarnings("unused")
			int bytesum = 0;
			int byteread = 0;
			File oldfile = new File(oldPath);
			if (oldfile.exists()) { 
				InputStream inStream = new FileInputStream(oldPath);
				FileOutputStream fs = new FileOutputStream(newPath);
				byte[] buffer = new byte[1444];				
				while ( (byteread = inStream.read(buffer)) != -1) {
					bytesum += byteread; 					
					fs.write(buffer, 0, byteread);
				}
				inStream.close();
				fs.close();
			}
			return true;
		}catch(Exception e) {
			System.out.println("FileOperate copyFile ex="+e.getMessage());	
			return false;
		}
	}

	 
	/**
	* copy folder 
	* @param oldPath String c:/fqf
	* @param newPath String f:/fqf/ff
	* @return boolean
	*/
	public boolean copyFolder(String oldPath, String newPath) {	 
		try{
			(new File(newPath)).mkdirs(); //folder is not exit , mkfolder
			File a=new File(oldPath);
			String[] file=a.list();
			File temp=null;
			for (int i = 0; i < file.length; i++) {
				if(oldPath.endsWith(File.separator)){
					temp=new File(oldPath+file[i]);
				}else{
					temp=new File(oldPath+File.separator+file[i]);
				}
				if(temp.isFile()){
					FileInputStream input = new FileInputStream(temp);
					FileOutputStream output = new FileOutputStream(newPath + "/"+(temp.getName()).toString());
					byte[] b = new byte[1024 * 5];
					int len;
					while ( (len = input.read(b)) != -1) {
						output.write(b, 0, len);
					}
					output.flush();
					output.close();
					input.close();
				}
				if(temp.isDirectory()){
					copyFolder(oldPath+"/"+file[i],newPath+"/"+file[i]);
				}
			}
			return true;
		}catch(Exception e) {
			System.out.println("FileOperate copyFolder ex="+e.getMessage());
			return false;
		}
	}

	/**
	* move file to folder
	* @param oldPath String c:/fqf.txt
	* @param newPath String  d:/fqf.txt
	*/
	public boolean moveFile(String oldPath, String newPath) {
		if(!copyFile(oldPath, newPath)){
			//System.out.println("fileOperate copy.... return false ");
			return false;
		}
		if(!delFile(oldPath)){
			//System.out.println("fileOperate delete.... return false ");
			return false;
		}
		return true;
	}

	/**
	* move folder files to folder
	* @param oldPath String如：c:/fqf.txt
	* @param newPath String如：d:/fqf.txt
	*/
	public void moveFolder(String oldPath, String newPath) {
		copyFolder(oldPath, newPath);
		delFolder(oldPath);
	}

}
