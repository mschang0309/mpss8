package mpss.util;

import java.io.File;
 
public class Utils
{
    static public File openResourceFile(String filename)
    {
    	  //Root path
 		    //String rootPath=psenginetest.class.getResource("/").getFile().toString();  
 		    
 		    //Current path
	    //String currentPath1=Utils.class.getResource(".").getFile().toString();  
	    String currentPath1=Utils.class.getClassLoader().getResource(".").getFile().toString();  
    	System.out.println("resource path: " + currentPath1);
		    //String currentPath2=psenginetest.class.getResource("").getFile().toString();  
		    
    	try
    	{
		    return new File(Utils.class.getClassLoader().getResource(filename).getFile());
    	}
    	catch (java.lang.NullPointerException e)
    	{
    		System.out.println("File " + filename + " not found");
    		return null;
    	}
   
   }

}
