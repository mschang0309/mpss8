package mpss.util;

import java.util.ArrayList;
import java.util.List;

public class ResultVO {
	
	private boolean result=true;
	private List<String> lists = new ArrayList<String>();
	private String msg ="";
	
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public boolean getResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public List<String> getLists() {
		return lists;
	}
	public void setLists(List<String> lists) {
		this.lists = lists;
	}
	
	public void addList(String file){
		if(lists!=null){
			this.lists.add(file);
		}else{
			System.out.println("ResultVO lists is null....");
		}		
	}
	
	public void clearList(){
		this.lists.clear();
	}

}
