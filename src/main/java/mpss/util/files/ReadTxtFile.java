package mpss.util.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ReadTxtFile {
	
	public static List<String> readFile(String filename){	
		
		List<String> datas = new ArrayList<String>();
		try {
			File f = new File(filename);

	        BufferedReader b;
			b = new BufferedReader(new FileReader(f));
			String readLine = "";

	        while ((readLine = b.readLine()) != null) {
	        	datas.add(readLine);
	        }
	        
	        b.close();			
	        return datas;
		} catch (Exception e) {
			System.out.println("ReadTxtFile ex ="+e.getMessage());
			datas.clear();
			return datas;
		}

        
		
	}

}
