package mpss.util.files;

import java.io.File;
import java.util.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
 
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "fileInfo")
public class FileInfo
{
    private String name;
    private int size;
    @XmlElement(name = "lastModified")
    @XmlSchemaType(name = "date")    
    private Date lastModified;
 
    public String getName()
    {
    	return this.name;
    }
    public int getSize()
    {
    	return this.size;
    }
    public Date getlastModified()
    {
    	return this.lastModified;
    }
    
    public FileInfo()
    {
    }
    
    public FileInfo(File _file)
    {
        this.name = _file.getName();
        this.size = (int) _file.length();
        this.lastModified = new Date(_file.lastModified());
    }
}
