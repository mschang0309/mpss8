package mpss.util.files;

import java.io.File;
import java.util.*;
 
public class FileLister
{
 
    public FileLister()
    {
    }
    
    public List<String> listFileNames(String dirName) 
    {
    
        List<String> names = new ArrayList<String>();
        File folder = new File(dirName);
        File[] listOfFiles = folder.listFiles(); 
        if (listOfFiles.length == 0)
        {
        	System.out.println("No file in directory("+dirName+")");
        }
        
        for (int i = 0; i < listOfFiles.length; i++) 
        {
        
            if (listOfFiles[i].isFile()) 
            {
                String fileName = listOfFiles[i].getName();
                names.add(fileName);
            }
        }
        return names;
   }

    public List<FileInfo> listFiles(String dirName) 
    {
    
        List<FileInfo> fileInfo = new ArrayList<FileInfo>();
        File folder = new File(dirName);
        File[] listOfFiles = folder.listFiles(); 
        if (listOfFiles.length == 0)
        {
        	System.out.println("No file in directory("+dirName+")");
        }
        
        for (int i = 0; i < listOfFiles.length; i++) 
        {
        
            if (listOfFiles[i].isFile()) 
            {
                FileInfo fi = new FileInfo(listOfFiles[i]);
                fileInfo.add(fi);
            }
        }
        return fileInfo;
   }
}
