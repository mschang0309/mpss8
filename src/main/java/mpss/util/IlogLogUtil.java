package mpss.util;

import java.io.File;
import java.util.Date;

import mpss.util.timeformat.TimeToString;

public class IlogLogUtil {
	
	public static String myfolder ="./mpss_log/ILOG";
	
	public static String getFullFileName(){
		// ./mpss_log/mpss_log1111.txt
		File ifolder =new File(myfolder);
		if(!ifolder.isDirectory()){
			ifolder.mkdir();			
		}
		String filename = myfolder + "/mpss_log_"+TimeToString.getYYYYMMDDTime(new Date())+".txt";
		
		return filename;
	}

}
