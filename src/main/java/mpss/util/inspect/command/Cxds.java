package mpss.util.inspect.command;

import java.util.ArrayList;
import java.util.List;

import mpss.util.inspect.BaseCommand;
import mpss.util.inspect.ICommand;
import mpss.util.inspect.ParameterVO;
import mpss.util.inspect.Parameters;

public class Cxds extends BaseCommand {

	public Cxds(){
		   
		   List<String> commands = new ArrayList<String>();
		   commands.add("CXDSPWON2LVON");
		   commands.add("CXDSFENARF");
		   commands.add("CXDSOPER2PWON");
		   initCommands(commands);
		   
		   List<Parameters> parameters = new ArrayList<Parameters>();
		   Parameters par1s = new Parameters();
		   par1s.addParameter(new ParameterVO("SRC","TTQ",true));
		   
		   Parameters par2s = new Parameters();
		   par2s.addParameter(new ParameterVO("SRC","TTQ",true));
		   
		   Parameters par3s = new Parameters();
		   par3s.addParameter(new ParameterVO("SRC","TTQ",true));
		   
		   parameters.add(par1s);
		   parameters.add(par2s);
		   parameters.add(par3s);
		   initParameter(parameters);
	   }
	   
	   public boolean isFirstCmd(String cmd){
		   cmd =getTrimCommand(cmd);
		   if(cmd.indexOf(getAllCommand(0))==0){
			   return true;
		   }else{
			   return false;
		   }	  
	   }
	   
	   public ICommand getInstance(){
		   return new Cxds();
	   }

}
