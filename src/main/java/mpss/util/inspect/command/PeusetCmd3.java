package mpss.util.inspect.command;

import java.util.ArrayList;
import java.util.List;

import mpss.util.inspect.BaseCommand;
import mpss.util.inspect.ICommand;
import mpss.util.inspect.ParameterVO;
import mpss.util.inspect.Parameters;

public class PeusetCmd3 extends BaseCommand {

	public PeusetCmd3(){
		   
		   List<String> commands = new ArrayList<String>();
		   commands.add("PEUSETCMD");
		   commands.add("PEUSETCMD");		   
		   initCommands(commands);
		   
		   List<Parameters> parameters = new ArrayList<Parameters>();
		   Parameters par1s = new Parameters();
		   par1s.addParameter(new ParameterVO("DICFILEDUMPSTART",null,false));
		   par1s.addParameter(new ParameterVO("FILE_ID","1",true));	
		   par1s.addParameter(new ParameterVO("SRC","MPQ",true));
		   
		   Parameters par2s = new Parameters();
		   par2s.addParameter(new ParameterVO("DICFILEDUMPSTOP",null,false));
		   par2s.addParameter(new ParameterVO("FILE_ID","1",true));
		   par2s.addParameter(new ParameterVO("SRC","MPQ",true));		   
		   
		   parameters.add(par1s);
		   parameters.add(par2s);
		   initParameter(parameters);
	   }
	   
	   public boolean isFirstCmd(String cmd){
		   cmd =getTrimCommand(cmd);
		   if(cmd.indexOf(getMainCommand(0)+"DICFILEDUMPSTART")==0){
			   return true;
		   }else{
			   return false;
		   }	  
	   }
	   
	   public ICommand getInstance(){
		   return new PeusetCmd3();
	   }

}
