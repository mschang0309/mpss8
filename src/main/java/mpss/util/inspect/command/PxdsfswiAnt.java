package mpss.util.inspect.command;

import java.util.ArrayList;
import java.util.List;

import mpss.util.inspect.BaseCommand;
import mpss.util.inspect.ICommand;
import mpss.util.inspect.ParameterVO;
import mpss.util.inspect.Parameters;

public class PxdsfswiAnt extends BaseCommand {

	public PxdsfswiAnt(){
		   
		   List<String> commands = new ArrayList<String>();
		   commands.add("PXDSFSWIANT");
		   commands.add("PXDSFSWIANT");		  
		   initCommands(commands);
		   
		   List<Parameters> parameters = new ArrayList<Parameters>();
		   Parameters par1s = new Parameters();
		   par1s.addParameter(new ParameterVO("SWITCH_ID","\'SW_A\'",true));
		   par1s.addParameter(new ParameterVO("POS_ID","\'POS2\'",true));
		   par1s.addParameter(new ParameterVO("SRC","TTQ",true));
		   
		   Parameters par2s = new Parameters();
		   par2s.addParameter(new ParameterVO("SWITCH_ID","\'SW_A\'",true));
		   par2s.addParameter(new ParameterVO("POS_ID","\'POS2\'",true));
		   par2s.addParameter(new ParameterVO("SRC","TTQ",true));
		   
		   parameters.add(par1s);
		   parameters.add(par2s);
		   initParameter(parameters);		   
	   }
	   
	   public boolean isFirstCmd(String cmd){
		   cmd =getTrimCommand(cmd);
		   if(cmd.indexOf(getMainCommand(0))==0){
			   return true;
		   }else{
			   return false;
		   }	  
	   }
	   
	   public String reverse(String pars){
		   if(pars.equals("\'POS1\'")){
			   return "\'POS2\'";			   
		   }else if(pars.equals("\'POS2\'")){
			   return "\'POS1\'";
		   }else{
			   return pars;
		   }
			
		}
	   
	   public ICommand getInstance(){
		   return new PxdsfswiAnt();
	   }

}
