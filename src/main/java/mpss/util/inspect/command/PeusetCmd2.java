package mpss.util.inspect.command;

import java.util.ArrayList;
import java.util.List;

import mpss.util.inspect.BaseCommand;
import mpss.util.inspect.ICommand;
import mpss.util.inspect.ParameterVO;
import mpss.util.inspect.Parameters;

public class PeusetCmd2 extends BaseCommand {

	public PeusetCmd2(){
		   
		   List<String> commands = new ArrayList<String>();
		   commands.add("PEUSETCMD");
		   commands.add("PEUSETCMD");		   
		   initCommands(commands);
		   
		   List<Parameters> parameters = new ArrayList<Parameters>();
		   Parameters par1s = new Parameters();
		   par1s.addParameter(new ParameterVO("DICCIPHER",null,false));
		   par1s.addParameter(new ParameterVO("CIPHER_CTRL","\'ENABLE\'",false));		   
		   
		   Parameters par2s = new Parameters();
		   par2s.addParameter(new ParameterVO("DICCIPHER",null,false));
		   par2s.addParameter(new ParameterVO("CIPHER_CTRL","\'DISABLE\'",false));		   
		   
		   parameters.add(par1s);
		   parameters.add(par2s);
		   initParameter(parameters);
	   }
	   
	   public boolean isFirstCmd(String cmd){
		   cmd =getTrimCommand(cmd);
		   if(cmd.indexOf(getMainCommand(0)+"DICCIPHER")==0){
			   return true;
		   }else{
			   return false;
		   }	  
	   }
	   
	   public ICommand getInstance(){
		   return new PeusetCmd2();
	   }

}
