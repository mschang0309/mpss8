package mpss.util.inspect;

import java.util.List;

public interface IExceptionCommand {
	
	public static String prefix ="CMD";
	
	public IExceptionCommand getInstance();
	
    public void initCommands(List<String> commands,List<String> exceptioncmd);
	
	public void initParameter(List<Parameters> pars,List<Parameters> expectionpar);
	
    public boolean isFirstCmd(String cmd);
	
	public void inspectFirstCmd(String cmd);
	
	public boolean inspectCmd(String cmd);	
		
	public boolean InspectEnd();
	
	public boolean inspectExceptionCmd(String cmd);
	
	public boolean isExpection();

	public void setExpection(boolean flag);
	
	public String getExceptionDesc();
    	

}
