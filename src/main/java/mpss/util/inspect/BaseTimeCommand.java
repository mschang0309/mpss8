package mpss.util.inspect;

import java.util.ArrayList;
import java.util.List;
import mpss.pg.EventTemplateLine;

public abstract class BaseTimeCommand implements ITimeCommand {	 
	
	private List<Parameters> parameters = new ArrayList<Parameters>();
	
	protected List<String> commands = new ArrayList<String>();
	
	protected int inspect_index=0;	
	
	protected double cmd_gag=0;
	
	protected EventTemplateLine tmpdata1 ;
	protected EventTemplateLine tmpdata2 ;
		
	public void initCommands(List<String> commands){
		this.commands = commands;
	}
		
	public void initParameter(List<Parameters> pars){
		this.parameters = pars;
	}
	
	
	
	public void inspectFirstCmd(EventTemplateLine cmd){
		String temp_cmd = getTrimCommand(getDataString(cmd));
		//
		String[] cmds =temp_cmd.split(" ");
		List<ParameterVO> pars =parameters.get(0).getParameters();
		for(int i = 2 ; i<cmds.length;i++){
			if(pars.get(i-2) !=null && pars.get(i-2).isChange() ){
				String value =cmds[i].replaceAll(pars.get(i-2).getKey()+"=", "");
				for(int j=1;j<parameters.size();j++){
						parameters.get(j).setChangeVar(i-2, reverse(value));
				}
			}
		}		
		inspect_index =inspect_index+1;
		tmpdata1 = cmd;
	}
	
	
	public boolean inspectCmd(EventTemplateLine cmd){
		String temp_cmd = getTrimCommand(getDataString(cmd));
		
		if(getAllCommand(inspect_index).indexOf(temp_cmd)==0){
			inspect_index =inspect_index+1;
			tmpdata2 = cmd;
			return true;
		}else{
			return false;
		}
		
	}	
	
	// replace "{" , "}"
	public String getTrimCommand(String cmd){
		try{
			cmd=cmd.replaceAll("\\{","");
			cmd=cmd.replaceAll("\\}","");
			cmd = normalizeString(cmd);
		}catch(Exception e){
			System.out.println("no {  } ");
		}
		return cmd;
	}
	
	
	
	
	public String getMainCommand(int index){
		return ICommand.prefix +" " + commands.get(index) +" "	;
	}
	
	// not include "{" , "}" 
	public String getAllCommand(int index){
		return ICommand.prefix +" " + commands.get(index) +parameters.get(index).getParametersStr();
	}
	
		
	public String reverse(String pars){
		return pars;
	}
	
	
	private String normalizeString(String txt) {
        String cmdStr = this.ltrim(txt);

        if (cmdStr.indexOf("\t") != -1) {
            cmdStr = cmdStr.replace("\t", " ");
        }

        if (cmdStr.indexOf("\n") != -1) {
            cmdStr = cmdStr.replace("\n", " ");
        }

        // trim "a =b" --> "a=b"
        while (cmdStr.indexOf(" =") != -1) {
            cmdStr = cmdStr.replace(" =", "=");
        }

        // trim "a= b" --> "a=b"
        while (cmdStr.indexOf("= ") != -1) {
            cmdStr = cmdStr.replace("= ", "=");
        }

        // two space char convert to one space char
        while (cmdStr.indexOf("  ") != -1) {
            cmdStr = cmdStr.replace("  ", " ");
        }
        return cmdStr;
    }

    private String ltrim(String s) {
        int i = 0;
        while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
            i++;
        }
        return s.substring(i);
    }

    public String rtrim(String s) {
        int i = s.length() - 1;
        while (i > 0 && Character.isWhitespace(s.charAt(i))) {
            i--;
        }
        return s.substring(0, i + 1);
    }
    
    protected String getDataString(EventTemplateLine data){
		String[] temps =data.getLoadText().split("#");
		String parameter = temps[0].trim();
		return "CMD"+" " +data.getLoadComment() + " "+parameter;
	}

}
