package mpss.util.inspect;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseExceptionCommand implements IExceptionCommand {

	private List<Parameters> parameters = new ArrayList<Parameters>();
	@SuppressWarnings("unused")
	private List<Parameters> expectionpar=new ArrayList<Parameters>();

	private List<String> commands = new ArrayList<String>();
	private List<String> exceptioncmd= new ArrayList<String>();

	private int inspect_index = 0;
	private boolean expection=false;	

	public boolean isExpection() {
		return expection;
	}
	
	public void setExpection(boolean flag) {
		expection=flag;
	}

	public void initCommands(List<String> commands, List<String> exceptioncmd) {
		this.commands = commands;
		this.exceptioncmd = exceptioncmd;
	}

	public void initParameter(List<Parameters> pars, List<Parameters> expectionpar) {
		this.parameters = pars;
		this.expectionpar = expectionpar;
	}

	public boolean InspectEnd() {
		if (inspect_index >= commands.size()) {
			return true;
		} else {
			return false;
		}
	}
	
	

	public void inspectFirstCmd(String cmd) {
		String temp_cmd = getTrimCommand(cmd);
		//
		String[] cmds = temp_cmd.split(" ");
		List<ParameterVO> pars = parameters.get(0).getParameters();
		for (int i = 2; i < cmds.length; i++) {
			if (pars.get(i - 2) != null && pars.get(i - 2).isChange()) {
				String value = cmds[i].replaceAll(pars.get(i - 2).getKey()
						+ "=", "");
				for (int j = 1; j < parameters.size(); j++) {
					parameters.get(j).setChangeVar(i - 2, reverse(value));
				}
			}
		}
		inspect_index = inspect_index + 1;
	}

	public boolean inspectCmd(String cmd) {
		String temp_cmd = getTrimCommand(cmd);

		if (getAllCommand(inspect_index).indexOf(temp_cmd) == 0) {
			inspect_index = inspect_index + 1;
			return true;
		} else {
			return false;
		}
	}

	// replace "{" , "}"
	public String getTrimCommand(String cmd) {
		try {
			cmd = cmd.replaceAll("\\{", "");
			cmd = cmd.replaceAll("\\}", "");
			cmd = normalizeString(cmd);
		} catch (Exception e) {
			System.out.println("no {  } ");
		}
		return cmd;
	}

	// not include "{" , "}"
	public String getAllCommand(int index) {
		return ICommand.prefix + " " + commands.get(index)
				+ parameters.get(index).getParametersStr();
	}
	
	// not include "{" , "}"
	public String getExpectionCommand(int index) {
			return ICommand.prefix + " " + exceptioncmd.get(index)	;
	}
	
	public String getMainCommand(int index){
		return ICommand.prefix +" " + commands.get(index) +" "	;
	}

	private String normalizeString(String txt) {
		String cmdStr = this.ltrim(txt);

		if (cmdStr.indexOf("\t") != -1) {
			cmdStr = cmdStr.replace("\t", " ");
		}

		if (cmdStr.indexOf("\n") != -1) {
			cmdStr = cmdStr.replace("\n", " ");
		}

		// trim "a =b" --> "a=b"
		while (cmdStr.indexOf(" =") != -1) {
			cmdStr = cmdStr.replace(" =", "=");
		}

		// trim "a= b" --> "a=b"
		while (cmdStr.indexOf("= ") != -1) {
			cmdStr = cmdStr.replace("= ", "=");
		}

		// two space char convert to one space char
		while (cmdStr.indexOf("  ") != -1) {
			cmdStr = cmdStr.replace("  ", " ");
		}
		return cmdStr;
	}

	private String ltrim(String s) {
		int i = 0;
		while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
			i++;
		}
		return s.substring(i);
	}

	public String rtrim(String s) {
		int i = s.length() - 1;
		while (i > 0 && Character.isWhitespace(s.charAt(i))) {
			i--;
		}
		return s.substring(0, i + 1);
	}

	
	public String reverse(String pars){
		return pars;
	}
}
