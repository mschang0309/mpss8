package mpss.util.inspect;

public class ParameterVO {
	
	private String key ="";
	private String value ="";
	private boolean change= false;
	private String chang_value ="";
	
	public boolean isChange() {
		return change;
	}
	

	public String getKey() {
		return key;
	}


	// if value is not null =>  key = value  , value is null => key 
	public ParameterVO(String key, String value, boolean change) {
		super();
		this.key = key;
		this.value = value;
		this.change = change;
	}
	
	public void setChangValue(String chang_value){
		this.chang_value = chang_value;
	}
	
	
	public String getString(){
		if(this.change == false){
			if(value ==null){
				return key;	
			}else{
				return key+"="+value;	
			}
						
		}else{
			if(value ==null){
				return key;					
			}else{
				return key+"="+chang_value;
			}
		}		
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}
	

}
