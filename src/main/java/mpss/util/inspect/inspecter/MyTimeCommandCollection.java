package mpss.util.inspect.inspecter;

import mpss.pg.EventTemplateLine;
import mpss.util.inspect.ITimeCommand;

public class MyTimeCommandCollection {
	
	private EventTemplateLine commandline;
	private ITimeCommand command;
	
	public MyTimeCommandCollection(EventTemplateLine commandline, ITimeCommand command) {
		super();
		this.commandline = commandline;
		this.command = command;
	}
	public EventTemplateLine getCommandline() {
		return commandline;
	}
	public void setCommandline(EventTemplateLine commandline) {
		this.commandline = commandline;
	}
	public ITimeCommand getCommand() {
		return command;
	}
	public void setCommand(ITimeCommand command) {
		this.command = command;
	}
	
	public boolean inspectCmd(EventTemplateLine commandline){
		return command.inspectCmd(commandline);
	}
	
	public boolean InspectEnd(){
		return command.InspectEnd();
	}
	
	public void inspectFirstCmd(EventTemplateLine commandline){
		command.inspectFirstCmd(commandline);
	}

}
