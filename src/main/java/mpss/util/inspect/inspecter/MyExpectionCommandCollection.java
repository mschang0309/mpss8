package mpss.util.inspect.inspecter;

import mpss.pg.EventTemplateLine;
import mpss.util.inspect.IExceptionCommand;

public class MyExpectionCommandCollection {
	
	private EventTemplateLine commandline;
	private IExceptionCommand command;
	
	
	public MyExpectionCommandCollection(EventTemplateLine commandline, IExceptionCommand command) {
		super();
		this.commandline = commandline;
		this.command = command;
	}
	public EventTemplateLine getCommandline() {
		return commandline;
	}
	public void setCommandline(EventTemplateLine commandline) {
		this.commandline = commandline;
	}
	public IExceptionCommand getCommand() {
		return command;
	}
	public void setCommand(IExceptionCommand command) {
		this.command = command;
	}
	
	public boolean inspectCmd(String cmd){
		return command.inspectCmd(cmd);
	}
	
	public boolean inspectExpectionCmd(String cmd){
		return command.inspectExceptionCmd(cmd);
	}
	
	public boolean InspectEnd(){
		return command.InspectEnd();
	}
	
	public void inspectFirstCmd(String cmd){
		command.inspectFirstCmd(cmd);
	}
	
	public boolean isExpection() {
		return command.isExpection();
	}
	
	public void setExpection(boolean flag) {
		command.setExpection(flag);
	}

}
