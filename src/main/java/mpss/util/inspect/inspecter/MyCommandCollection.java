package mpss.util.inspect.inspecter;

import mpss.pg.EventTemplateLine;
import mpss.util.inspect.ICommand;

public class MyCommandCollection {
	
	private EventTemplateLine commandline;
	private ICommand command;
	
	public MyCommandCollection(EventTemplateLine commandline, ICommand command) {
		super();
		this.commandline = commandline;
		this.command = command;
	}
	public EventTemplateLine getCommandline() {
		return commandline;
	}
	public void setCommandline(EventTemplateLine commandline) {
		this.commandline = commandline;
	}
	public ICommand getCommand() {
		return command;
	}
	public void setCommand(ICommand command) {
		this.command = command;
	}
	
	public boolean inspectCmd(String cmd){
		return command.inspectCmd(cmd);
	}
	
	public boolean InspectEnd(){
		return command.InspectEnd();
	}
	
	public void inspectFirstCmd(String cmd){
		command.inspectFirstCmd(cmd);
	}

}
