package mpss.util.inspect.inspecter;

import java.util.ArrayList;
import java.util.List;
import mpss.pg.EventTemplateLine;

public class InspecterTest {

	// @Test
	public void testInspecter() {

		List<EventTemplateLine> datas = new ArrayList<EventTemplateLine>();
		EventTemplateLine data1 = new EventTemplateLine();
		data1.setLoadComment("ESTXAON");
		data1.setLoadText("SRC=TTQ     # test ttttttt");
		datas.add(data1);

		EventTemplateLine data3 = new EventTemplateLine();
		data3.setLoadComment("ESTXAOFaa");
		data3.setLoadText("{desc SRC=TTQ }     # test ttttttt");
		datas.add(data3);

		EventTemplateLine data2 = new EventTemplateLine();
		data2.setLoadComment("ESTXAOF");
		data2.setLoadText("SRC=TTQ     # test ttttttt");
		datas.add(data2);

		Inspecter test = new Inspecter(datas);
		test.run();
		
		System.out.println("ex parir count=" + test.getPairDates().size());
		System.out.println("ex exception count=" + test.getExceptionDates().size());
	}

	// @Test
	public void testInspecter1() {

		List<EventTemplateLine> datas = new ArrayList<EventTemplateLine>();
		EventTemplateLine data1 = new EventTemplateLine();
		data1.setLoadComment("CXDSPWON2LVON");
		data1.setLoadText("SRC=TTQ     # test ttttttt");
		datas.add(data1);

		EventTemplateLine data3 = new EventTemplateLine();
		data3.setLoadComment("CXDSFENARF");
		data3.setLoadText("{desc SRC=TTQ }     # test ttttttt");
		datas.add(data3);

		EventTemplateLine data2 = new EventTemplateLine();
		data2.setLoadComment("CXDSFENARF");
		data2.setLoadText("SRC=TTQ     # test ttttttt");
		datas.add(data2);

		EventTemplateLine data4 = new EventTemplateLine();
		data4.setLoadComment("CXDSOPER2PWON");
		data4.setLoadText("SRC=TTQ     # test ttttttt");
		datas.add(data4);

		Inspecter test = new Inspecter(datas);
		test.run();
		
		System.out.println("ex parir count=" + test.getPairDates().size());
		System.out.println("ex exception count=" + test.getExceptionDates().size());
	}

	// @Test
	public void testInspecter2() {

		List<EventTemplateLine> datas = new ArrayList<EventTemplateLine>();
		EventTemplateLine data1 = new EventTemplateLine();
		data1.setLoadComment("PEUSETCMD");
		data1.setLoadText("{DICFILEREC FILE_ID=3} SRC=MPQ     # test ttttttt");
		datas.add(data1);

		EventTemplateLine data3 = new EventTemplateLine();
		data3.setLoadComment("PEUSETCMD");
		data3.setLoadText("{desc SRC=TTQ }     # test ttttttt");
		// datas.add(data3);

		EventTemplateLine data2 = new EventTemplateLine();
		data2.setLoadComment("PEUSETCMD");
		data2.setLoadText("{DICFILENOREC FILE_ID=3}     # test ttttttt");
		// data2.setLoadText("{DICFILENOREC FILE_ID=4}     # test ttttttt");
		datas.add(data2);

		Inspecter test = new Inspecter(datas);
		test.run();
		
		System.out.println("ex parir count=" + test.getPairDates().size());
		System.out.println("ex exception count=" + test.getExceptionDates().size());
	}

	// @Test
	public void testInspecter3() {

		List<EventTemplateLine> datas = new ArrayList<EventTemplateLine>();
		EventTemplateLine data1 = new EventTemplateLine();
		data1.setLoadComment("PEUSETCMD");
		data1.setLoadText("{DICCIPHER CIPHER_CTRL = 'ENABLE'}     # test ttttttt");
		datas.add(data1);

		EventTemplateLine data3 = new EventTemplateLine();
		data3.setLoadComment("PEUSETCMD");
		data3.setLoadText("{desc SRC=TTQ }     # test ttttttt");
		// datas.add(data3);

		EventTemplateLine data2 = new EventTemplateLine();
		data2.setLoadComment("PEUSETCMD");
		data2.setLoadText("{DICCIPHER CIPHER_CTRL = 'DISABLE'}     # test ttttttt");
		datas.add(data2);

		Inspecter test = new Inspecter(datas);
		test.run();
		
		System.out.println("ex parir count=" + test.getPairDates().size());
		System.out.println("ex exception count=" + test.getExceptionDates().size());
	}

	// @Test
	public void testInspecter4() {

		List<EventTemplateLine> datas = new ArrayList<EventTemplateLine>();
		EventTemplateLine data1 = new EventTemplateLine();
		data1.setLoadComment("PXDSFSWIANT");
		data1.setLoadText("SWITCH_ID='SW_A' POS_ID='POS2' SRC=TTQ     # test ttttttt");
		datas.add(data1);

		EventTemplateLine data2 = new EventTemplateLine();
		data2.setLoadComment("PXDSFSWIANT");
		// data2.setLoadText("SWITCH_ID='SW_A' POS_ID='POS1' SRC=TTQ   # test ttttttt");
		data2.setLoadText("SWITCH_ID='SW_A' POS_ID='POS2' SRC=TTQ   # test ttttttt");
		datas.add(data2);

		Inspecter test = new Inspecter(datas);
		test.run();
		
		System.out.println("ex parir count=" + test.getPairDates().size());
		System.out.println("ex exception count=" + test.getExceptionDates().size());
	}

	 //@Test
	public void testInspecter5() {

		List<EventTemplateLine> datas = new ArrayList<EventTemplateLine>();
		EventTemplateLine data1 = new EventTemplateLine();
		data1.setLoadComment("DCDSSTARTDUMP");
		data1.setLoadText("DUMP_FLAG = 'From_Start' FILE_NAME = '1' SRC=TTQ     # test ttttttt");
		datas.add(data1);

		EventTemplateLine data2 = new EventTemplateLine();
		data2.setLoadComment("DCDSSPDUMP");
		data2.setLoadText("FILE_NAME = '1' SRC=TTQ   # test ttttttt");
		datas.add(data2);

		Inspecter test = new Inspecter(datas);
		test.run();
		
		System.out.println("ex parir count=" + test.getPairDates().size());
		System.out.println("ex exception count=" + test.getExceptionDates().size());
	}
	 
	// @Test
	public void testInspecterExpectionCeueStoping() {
		 
		 

			List<EventTemplateLine> datas = new ArrayList<EventTemplateLine>();
			EventTemplateLine data1 = new EventTemplateLine();
			data1.setLoadComment("PEUSETCMD");
			data1.setLoadText("{DICFILEDUMPSTART FILE_ID=52} SRC=MPQ    # test ttttttt");
			
			

			EventTemplateLine data2 = new EventTemplateLine();
			data2.setLoadComment("PEUSETCMD");
			data2.setLoadText("{DICFILEDUMPSTOP FILE_ID=52} SRC=MPQ    # test ttttttt");
			
			
			EventTemplateLine data3 = new EventTemplateLine();
			data3.setLoadComment("CEUESTOPIMG");
			data3.setLoadText("SRC=MPQ");	
			
			
			
			datas.add(data1);	
			datas.add(data3);
			datas.add(data2);
			
			
			
			Inspecter test = new Inspecter(datas);
			test.run();
			
			System.out.println("ex parir count=" + test.getPairDates().size());
			System.out.println("ex exception count=" + test.getExceptionDates().size());
	}
	 
	// @Test
		public void testInspecterExpectionMcReset() {

				List<EventTemplateLine> datas = new ArrayList<EventTemplateLine>();
				EventTemplateLine data1 = new EventTemplateLine();
				data1.setLoadComment("PEUSETCMD");
				data1.setLoadText("{DICFILEDUMPSTART FILE_ID=52} SRC=MPQ    # test ttttttt");
				

				EventTemplateLine data2 = new EventTemplateLine();
				data2.setLoadComment("PEUSETCMD");
				data2.setLoadText("{DICFILEDUMPSTOP FILE_ID=52} SRC=MPQ    # test ttttttt");
				
				
				EventTemplateLine data3 = new EventTemplateLine();
				data3.setLoadComment("PEUSETCMD");
				data3.setLoadText("{DICRSIRESET RESET_ID=\'MC_RESET\'} SRC=TTQ");	
				
				
				
				datas.add(data1);
				datas.add(data3);
				datas.add(data2);
				
				
				
				Inspecter test = new Inspecter(datas);
				test.run();
				
				System.out.println("ex parir count=" + test.getPairDates().size());
				System.out.println("ex exception count=" + test.getExceptionDates().size());
		}
	 
	//@Test
		public void testInspecterExpectionMcReset1() {

				List<EventTemplateLine> datas = new ArrayList<EventTemplateLine>();
				EventTemplateLine data1 = new EventTemplateLine();
				data1.setLoadComment("CEUESTARTIMG");
				data1.setLoadText("SRC=MPQ    # test ttttttt");
				

				EventTemplateLine data2 = new EventTemplateLine();
				data2.setLoadComment("CEUESTOPIMG");
				data2.setLoadText("SRC=MPQ    # test ttttttt");
				
				
				EventTemplateLine data3 = new EventTemplateLine();
				data3.setLoadComment("PEUSETCMD");
				data3.setLoadText("{DICRSIRESET RESET_ID=\'MC_RESET\'} SRC=MPQ");	
				
				
				
				datas.add(data1);
				datas.add(data3);
				datas.add(data2);
				
				
				
				Inspecter test = new Inspecter(datas);
				test.run();
				
				System.out.println("ex parir count=" + test.getPairDates().size());
				System.out.println("ex exception count=" + test.getExceptionDates().size());
		}
		
		
}
