package mpss.util.inspect.inspecter;

import java.util.ArrayList;
import java.util.List;

import mpss.pg.EventTemplateLine;
import mpss.util.inspect.ICommand;
import mpss.util.inspect.IExceptionCommand;
import mpss.util.inspect.ITimeCommand;
import mpss.util.inspect.command.*;
import mpss.util.inspect.expectioncmd.ExpectionCeueStoping;
import mpss.util.inspect.expectioncmd.ExpectionMcReset;
import mpss.util.inspect.expectioncmd.ExpectionRF;
import mpss.util.inspect.timecmd.TimeXcrossPbk;
import mpss.util.timeformat.TimeToString;

public class Inspecter {
	
	private List<ICommand> commands = new ArrayList<ICommand>();	
	private List<MyCommandCollection> checkCommands = new ArrayList<MyCommandCollection>();
	
	private List<IExceptionCommand> expectioncommands = new ArrayList<IExceptionCommand>();
	private List<MyExpectionCommandCollection> expectioncheckCommands = new ArrayList<MyExpectionCommandCollection>();	
	
	private List<ITimeCommand> timecommands = new ArrayList<ITimeCommand>();
	private List<MyTimeCommandCollection> timecheckCommands = new ArrayList<MyTimeCommandCollection>();
	
	private List<EventTemplateLine> datas ;
	private List<EventTemplateLine> pairdatas = new ArrayList<EventTemplateLine>();
	private List<EventTemplateLine> exdatas = new ArrayList<EventTemplateLine>();
	private List<EventTemplateLine> timecmddatas = new ArrayList<EventTemplateLine>();
	private List<String> exdatas_desc = new ArrayList<String>();
	private List<String> timedatas_desc = new ArrayList<String>();
	private List<EventTemplateLine> timeoverlapdatas = new ArrayList<EventTemplateLine>();
	//loadComment ,loadText #
	
	public Inspecter(List<EventTemplateLine> datas){
		
		
		
		commands.add(new CeueStartStop());
		commands.add(new Cxds());
		commands.add(new Cxds2());
		commands.add(new Dcds());
		commands.add(new EstxaEnDs());		
		commands.add(new EstxaOnOff());
		commands.add(new PeusetCmd());
		commands.add(new PeusetCmd2());
		commands.add(new PeusetCmd3());
		commands.add(new PeusetCmd4());
		commands.add(new PxdsfswiAnt());
		
		
		expectioncommands.add(new ExpectionRF());	
		expectioncommands.add(new ExpectionMcReset());
		expectioncommands.add(new ExpectionCeueStoping());	
		
		
		timecommands.add(new TimeXcrossPbk(7.5));
		
		initData(datas);
	}
	
	
	private void initData(List<EventTemplateLine> datas){
		this.datas = datas;
	}
	
	public List<EventTemplateLine> getExceptionDates(){
		return this.exdatas;
	}
	
	public List<String> getExceptionDates_Desc(){
		return this.exdatas_desc;
	}
	
	public List<EventTemplateLine> getTimeCmdDates(){
		return this.timecmddatas;
	}
	
	public List<String> getTimeCmdDates_Desc(){
		return this.timedatas_desc;
	}
	
	public List<EventTemplateLine> getPairDates(){
		return this.pairdatas;
	}
	
	public List<EventTemplateLine> getTimeOverlapDates(){
		return this.timeoverlapdatas;
	}
	
	public void run(){
		checkCommands.clear();
		expectioncheckCommands.clear();
		timeoverlapdatas.clear();
		timecheckCommands.clear();
		
		for(EventTemplateLine data : datas){
			System.out.println(data.getFullLineText());
		}
		
		// for time overlap 
		EventTemplateLine temp =null ;
		int i =0;
		if(this.datas!=null){
			for(EventTemplateLine data : datas){	
				if(i==82){
					System.out.println("");
				}
				i=i+1;
				boolean flag = true;				
				for(int j = checkCommands.size() - 1; j >= 0; j--){
					checkCommands.get(j).inspectCmd(getDataString(data));
					 if(checkCommands.get(j).InspectEnd()){
						 checkCommands.remove(checkCommands.get(j));
						 flag=false;
						 break;
					 }
				 }
				
				if(flag){
					for(ICommand basecommand :commands){
						if(basecommand.isFirstCmd(getDataString(data))){
							MyCommandCollection col = new MyCommandCollection(data,basecommand.getInstance());
							col.inspectFirstCmd(getDataString(data));
							checkCommands.add(col);
							break;
						}
					}
				}
				
				// exception command	
				flag = true;
				for(int j = expectioncheckCommands.size() - 1; j >= 0; j--){
					if(!expectioncheckCommands.get(j).isExpection()){
						expectioncheckCommands.get(j).inspectCmd(getDataString(data));						 
						 if(expectioncheckCommands.get(j).inspectExpectionCmd(getDataString(data)) ){
							 expectioncheckCommands.get(j).setExpection(true);
							 //System.out.println("Inspecter Expection");
							 //break;
						 }
						 
						 if(expectioncheckCommands.get(j).InspectEnd()){
							 expectioncheckCommands.remove(j);
							 flag=false;
							 //break;
						 }	
						 
						 
					}	
				}
			
				if(flag){
					for(IExceptionCommand basecommand :expectioncommands){
						if(basecommand.isFirstCmd(getDataString(data))){
							MyExpectionCommandCollection col = new MyExpectionCommandCollection(data,basecommand.getInstance());
							col.inspectFirstCmd(getDataString(data));
							expectioncheckCommands.add(col);
							break;
						}
					}
				}	
				
				// check command time overlap
				if(temp==null){
					temp = data;
				}else{
					if(TimeToString.AsFullTime(data.getEventTriggerTag().getTriggerTime()).equals(TimeToString.AsFullTime(temp.getEventTriggerTag().getTriggerTime()))){
						timeoverlapdatas.add(temp);
					}
					temp = data;
				}
				
				// check command time need gap time
				flag = true;
				for(int j = timecheckCommands.size() - 1; j >= 0; j--){
					timecheckCommands.get(j).inspectCmd(data);
					 if(timecheckCommands.get(j).InspectEnd()){
						 timecheckCommands.remove(timecheckCommands.get(j));
						 flag=false;
						 break;
					 }
				 }
				
				if(flag){
					for(ITimeCommand basecommand :timecommands){
						if(basecommand.isFirstCmd(data)){
							MyTimeCommandCollection col = new MyTimeCommandCollection(data,basecommand.getInstance());
							col.inspectFirstCmd(data);
							timecheckCommands.add(col);
							break;
						}
					}
				}

			}
		}else{
			System.out.println("Inspecter datas is null");
		}
		
		
		pairdatas.clear();
		for(MyCommandCollection command :checkCommands){
			pairdatas.add(command.getCommandline());
		}
		
		exdatas.clear();
		exdatas_desc.clear();
		for(MyExpectionCommandCollection command :expectioncheckCommands){
			exdatas_desc.add(command.getCommand().getExceptionDesc());
			exdatas.add(command.getCommandline());			
		}
		
		timecmddatas.clear();
		timedatas_desc.clear();
		for(MyTimeCommandCollection command :timecheckCommands){
			timedatas_desc.add(command.getCommand().getExceptionDesc());
			timecmddatas.add(command.getCommandline());			
		 }
		
	}
	
	
	private String getDataString(EventTemplateLine data){
		String[] temps =data.getLoadText().split("#");
		String parameter = temps[0].trim();
		return "CMD"+" " +data.getLoadComment() + " "+parameter;
	}
	

}
