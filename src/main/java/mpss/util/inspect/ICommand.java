package mpss.util.inspect;

import java.util.List;

public interface ICommand {
	
	public static String prefix ="CMD";
	
	public ICommand getInstance();
	
    public void initCommands(List<String> commands);
	
	public void initParameter(List<Parameters> pars);
	
	public boolean isFirstCmd(String cmd);
	
	public void inspectFirstCmd(String cmd);
	
	public boolean inspectCmd(String cmd);	
		
	public boolean InspectEnd();	
	
	public String getMainCommand(int index);
	
	public String getAllCommand(int index);
	
	public String getTrimCommand(String cmd);	
	
	public String reverse(String pars);
	

}
