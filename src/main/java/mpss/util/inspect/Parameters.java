package mpss.util.inspect;

import java.util.ArrayList;
import java.util.List;

public class Parameters {
	
	private List<ParameterVO> parameters = new ArrayList<ParameterVO>();
	
	public void addParameter(ParameterVO vo){
		this.parameters.add(vo);
	}
	
	public void setChangeVar(int index , String var){
		for(int i=0 ; i<this.parameters.size();i++){
			if(i == index){
				this.parameters.get(index).setChangValue(var);
			}			
		}
	}
	
	public String getParametersStr(){
		String str ="";
		for(ParameterVO vo :parameters){
			if(vo.getValue()==null && vo.getKey().equals("")){
				
			}else{
				str =str+" "+vo.getString();
			}
			
		}
		return str;
	}

	public List<ParameterVO> getParameters() {
		return parameters;
	}

	
}
