package mpss.util.inspect;

import java.util.List;

import mpss.pg.EventTemplateLine;

public interface ITimeCommand {
	
	public static String prefix ="CMD";
	
	public ITimeCommand getInstance();
	
    public void initCommands(List<String> commands);
	
	public void initParameter(List<Parameters> pars);
	
	public boolean isFirstCmd(EventTemplateLine cmd);
	
	public void inspectFirstCmd(EventTemplateLine cmd);
	
	public boolean inspectCmd(EventTemplateLine cmd);	
		
	public boolean InspectEnd();	
	
	public String getMainCommand(int index);
	
	public String getAllCommand(int index);
	
	public String getTrimCommand(String cmd);
	
	public String getExceptionDesc();
}
