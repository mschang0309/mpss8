package mpss.util.inspect.timecmd;

import java.util.ArrayList;
import java.util.List;

import mpss.pg.EventTemplateLine;
import mpss.util.inspect.BaseTimeCommand;
import mpss.util.inspect.ITimeCommand;
import mpss.util.inspect.ParameterVO;
import mpss.util.inspect.Parameters;
import mpss.util.timeformat.TimeUtil;

public class TimeXcrossPbk extends BaseTimeCommand {

	public TimeXcrossPbk(Double cmdgap){		   
		   List<String> commands = new ArrayList<String>();
		   commands.add("CXDSFENARF");	
		   commands.add("CXDSFDISRF");	
		   commands.add("CXDSOPER2PWON");	
		   initCommands(commands);
		   
		   List<Parameters> parameters = new ArrayList<Parameters>();
		   Parameters par1s = new Parameters();
		   par1s.addParameter(new ParameterVO("SRC","TTQ",true));
		   
		   Parameters par2s = new Parameters();
		   par2s.addParameter(new ParameterVO("SRC","TTQ",true));
		   
		   Parameters par3s = new Parameters();
		   par3s.addParameter(new ParameterVO("SRC","TTQ",true));
		   
		   parameters.add(par1s);
		   parameters.add(par2s);
		   parameters.add(par3s);
		   initParameter(parameters);
		   
		   this.cmd_gag = cmdgap;
	   }
	   
	   public boolean isFirstCmd(EventTemplateLine cmd){
		   String temcmd =getTrimCommand(getDataString(cmd));
		   if(temcmd.indexOf(getAllCommand(0))==0){
			   return true;
		   }else{
			   return false;
		   }	  
	   }
	   
	   
	   public boolean inspectCmd(EventTemplateLine cmd){
			String temp_cmd = getTrimCommand(getDataString(cmd));
			
			if(getAllCommand(1).indexOf(temp_cmd)==0){
				inspect_index =2;
				tmpdata2 = cmd;
				return true;
			}else if(getAllCommand(2).indexOf(temp_cmd)==0){
				inspect_index =3;
				tmpdata2 = cmd;
				return true;
			}else{
				return false;
			}
			
		}
	   
	   public boolean InspectEnd(){
			if(inspect_index ==2){
				// check time			
				double duration = TimeUtil.getDurationTimestamp(tmpdata2.getEventTriggerTag().getTriggerTime(), tmpdata1.getEventTriggerTag().getTriggerTime());
				if(duration > this.cmd_gag){				
					return true;
				}else{
					inspect_index = 1;
					return false;				
				}			
			}else if(inspect_index ==3){
				return true;				
			}else{
				return false;
			}
		}
	   
	   
	   
	   public ITimeCommand getInstance(){
		   return new TimeXcrossPbk(this.cmd_gag);
	   }
	   
	   public String getExceptionDesc(){
		   return " Enable RF and Disable RF duration < "+this.cmd_gag;
		   
	   }

}
