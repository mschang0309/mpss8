package mpss.util.inspect.expectioncmd;

import java.util.ArrayList;
import java.util.List;

import mpss.util.inspect.BaseExceptionCommand;
import mpss.util.inspect.IExceptionCommand;
import mpss.util.inspect.ParameterVO;
import mpss.util.inspect.Parameters;

public class ExpectionMcReset extends BaseExceptionCommand {

	public ExpectionMcReset(){	
		
		   List<String> commands = new ArrayList<String>();
		   commands.add("CEUESTARTIMG");
		   commands.add("CEUESTOPIMG");
		   List<String> excommands = new ArrayList<String>();
		   excommands.add("PEUSETCMD");
		   initCommands(commands,excommands);
		   
		   List<Parameters> parameters = new ArrayList<Parameters>();
		   Parameters par1s = new Parameters();
		   par1s.addParameter(new ParameterVO("SRC","MPQ",true));
		   
		   Parameters par2s = new Parameters();
		   par2s.addParameter(new ParameterVO("SRC","MPQ",true));
		   
		   Parameters par3s = new Parameters();
		   par3s.addParameter(new ParameterVO("DICRSIRESET",null,false));
		   par3s.addParameter(new ParameterVO("RESET_ID","MC_RESET",false));
		   par3s.addParameter(new ParameterVO("SRC","MPQ",false));
		   
		   parameters.add(par1s);
		   parameters.add(par2s);
		   List<Parameters> exparameters = new ArrayList<Parameters>();
		   exparameters.add(par3s);
		   initParameter(parameters,exparameters);
		  
	}

	@Override
	public IExceptionCommand getInstance() {
		 return new ExpectionMcReset();
	}

	public boolean isFirstCmd(String cmd){
		cmd =getTrimCommand(cmd);
		if(cmd.indexOf(getMainCommand(0)+"SRC=")==0){
		   return true;
		}else{
		   return false;
		}	  
	}
	
	public boolean inspectExceptionCmd(String cmd){
		String temp_cmd = getTrimCommand(cmd);		
		 if(temp_cmd.indexOf(getExpectionCommand(0)+" DICRSIRESET RESET_ID=\'MC_RESET\' SRC=")==0){
			   return true;
		   }else{
			   return false;
		   }	
		
	}
	   
	public String getExceptionDesc(){
		return "MC RESET Error";
	}  

}
