package mpss.util.inspect.expectioncmd;

import java.util.ArrayList;
import java.util.List;

import mpss.util.inspect.BaseExceptionCommand;
import mpss.util.inspect.IExceptionCommand;
import mpss.util.inspect.ParameterVO;
import mpss.util.inspect.Parameters;

public class ExpectionRF extends BaseExceptionCommand {
	
	private int i =0;

	public ExpectionRF(){	
		
		   List<String> commands = new ArrayList<String>();
		   commands.add("CXDSFDISRF");
		   commands.add("CXDSOPER2PWON");
		   List<String> excommands = new ArrayList<String>();
		   excommands.add("CXDSFENARF");
		   excommands.add("CXDSFENARF");
		   initCommands(commands,excommands);
		   
		   List<Parameters> parameters = new ArrayList<Parameters>();
		   Parameters par1s = new Parameters();
		   par1s.addParameter(new ParameterVO("SRC","TTQ",true));
		   
		   Parameters par2s = new Parameters();
		   par2s.addParameter(new ParameterVO("SRC","TTQ",true));
		   
		   Parameters par3s = new Parameters();
		   par3s.addParameter(new ParameterVO("SRC","TTQ",false));
		   
		   Parameters par4s = new Parameters();
		   par3s.addParameter(new ParameterVO("SRC","TTQ",false));
		   
		   parameters.add(par1s);
		   parameters.add(par2s);
		   List<Parameters> exparameters = new ArrayList<Parameters>();
		   exparameters.add(par3s);
		   exparameters.add(par4s);
		   initParameter(parameters,exparameters);
		  
	}

	@Override
	public IExceptionCommand getInstance() {
		 return new ExpectionRF();
	}

	public boolean isFirstCmd(String cmd){
		cmd =getTrimCommand(cmd);
		if(cmd.indexOf(getMainCommand(0)+"SRC=")==0){
		   return true;
		}else{
		   return false;
		}	  
	}
	
	public boolean inspectExceptionCmd(String cmd){
		String temp_cmd = getTrimCommand(cmd);		
		 if(temp_cmd.indexOf(getExpectionCommand(0))==0){
			 if(i==0){
				i = i+1; 
				return false;
			 }else{
				 return true;
			 }
			  
		   }else{
			   return false;
		   }	
		
	}
	   
	public String getExceptionDesc(){
		return "RF Error";
	}  

}
