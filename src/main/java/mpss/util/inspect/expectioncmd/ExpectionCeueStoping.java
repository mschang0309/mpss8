package mpss.util.inspect.expectioncmd;

import java.util.ArrayList;
import java.util.List;

import mpss.util.inspect.BaseExceptionCommand;
import mpss.util.inspect.IExceptionCommand;
import mpss.util.inspect.ParameterVO;
import mpss.util.inspect.Parameters;

public class ExpectionCeueStoping extends BaseExceptionCommand {

	public ExpectionCeueStoping(){	
		
		   List<String> commands = new ArrayList<String>();
		   commands.add("PEUSETCMD");
		   commands.add("PEUSETCMD");	  
		   List<String> excommands = new ArrayList<String>();
		   excommands.add("CEUESTOPIMG");
		   excommands.add("PEUSETCMD");
		   initCommands(commands,excommands);
		   
		   List<Parameters> parameters = new ArrayList<Parameters>();
		   Parameters par1s = new Parameters();
		   par1s.addParameter(new ParameterVO("DICFILEDUMPSTART",null,false));
		   par1s.addParameter(new ParameterVO("FILE_ID","52",true));
		   par1s.addParameter(new ParameterVO("SRC","MPQ",true));
		   
		   Parameters par2s = new Parameters();
		   par2s.addParameter(new ParameterVO("DICFILEDUMPSTOP",null,false));
		   par2s.addParameter(new ParameterVO("FILE_ID","52",true));
		   par2s.addParameter(new ParameterVO("SRC","MPQ",true));
		   
		   Parameters par3s = new Parameters();
		   par3s.addParameter(new ParameterVO("SRC","MPQ",false));
		   
		   parameters.add(par1s);
		   parameters.add(par2s);
		   
		   List<Parameters> exparameters = new ArrayList<Parameters>();
		   exparameters.add(par3s);
		   Parameters par4s = new Parameters();
		   par4s.addParameter(new ParameterVO("DICRSIRESET",null,false));
		   par4s.addParameter(new ParameterVO("RESET_ID","MC_RESET",false));
		   par4s.addParameter(new ParameterVO("SRC","MPQ",false));
		   exparameters.add(par4s);
		   initParameter(parameters,exparameters);
		  
	}

	@Override
	public IExceptionCommand getInstance() {
		 return new ExpectionCeueStoping();
	}

	public boolean isFirstCmd(String cmd){
		cmd =getTrimCommand(cmd);
		if(cmd.indexOf(getMainCommand(0)+"DICFILEDUMPSTART FILE_ID=")==0){
		   return true;
		}else{
		   return false;
		}	  
	}
	
	public boolean inspectExceptionCmd(String cmd){
		String temp_cmd = getTrimCommand(cmd);	
		if(temp_cmd.indexOf("FILE_ID=25")>0){
			System.out.println("");
		}
		 if(temp_cmd.indexOf(getExpectionCommand(0)+" SRC=")==0){
			   return true;
		 }else if(temp_cmd.indexOf(getExpectionCommand(1)+" DICRSIRESET RESET_ID=\'MC_RESET\' SRC=")==0){
			 return true;
		 }else{
			   return false;
		   }	
		
	}
	
	public String getExceptionDesc(){
		return "Stop Imaging Error";
	}
	   
	   

}
