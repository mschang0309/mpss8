package mpss.util.jaxb;
     
import javax.xml.bind.annotation.XmlRootElement;
 
@XmlRootElement
public class JaxbItem<T> {
 
	//@XmlElement
    private T item;
 
    public JaxbItem() {
    }
 
    public JaxbItem(T item) {
        this.item = item;
    }
 
    public T getItem() {
        return item;
    }
 
    public void setItem(T item) {
        this.item = item;
    }
 
}