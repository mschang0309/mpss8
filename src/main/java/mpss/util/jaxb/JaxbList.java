package mpss.util.jaxb;
     
import java.util.*;
import javax.xml.bind.annotation.XmlAnyElement;
 
public class JaxbList<T> {
 
    private List<T> items;
 
    public JaxbList() {
        items = new ArrayList<T>();
    }
 
    public JaxbList(List<T> items) {
        this.items = items;
    }
 
    @XmlAnyElement(lax=true)
    public List<T> getItems() {
        return items;
    }
 
}