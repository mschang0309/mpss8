package mpss.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import mpss.common.jpa.Extentactivitytimerange;
import mpss.util.timeformat.DateUtil;

public class ExtentActivityTimeRangeUtils {
	
	public static int rsirecsetupdelay =3;
	public static int rsireccleanupdelay =1;
	public static int rsipbsetupdelay =1;
	public static int rsipbcleanupdelay =3;
	
	public static int isualrecsetupdelay =1;
	public static int isualreccleanupdelay =1;
	public static int isualpbsetupdelay =1;
	public static int isualpbcleanupdelay =1;
	
	
	public static enum ActivityType {
		RSI_REC,RSI_PBK,ISUAL_PBK,ISUAL_REC,DDT
	}
	
	
	public static ArrayList<Extentactivitytimerange> getExtentActivityTimeRanges(int parentid,ActivityType type,Date starttime,Date endtime){
		ArrayList<Extentactivitytimerange>  al = new ArrayList<Extentactivitytimerange>();
		if(type ==ActivityType.RSI_REC){
			// five row
			Extentactivitytimerange rang = new Extentactivitytimerange();
			rang.setParentid(parentid);
			rang.setStringdata("Extent");
			rang.setStarttime(DateUtil.dateToTimestamp(starttime));
			Timestamp timestamp1 = DateUtil.dateToTimestamp(DateUtil.DateAddSec(starttime, rsirecsetupdelay));
			rang.setEndtime(timestamp1);			
			
			
			Extentactivitytimerange rang1 = new Extentactivitytimerange();
			rang1.setParentid(parentid);
			rang1.setStringdata("Extent");
			Timestamp timestamp2 =DateUtil.dateToTimestamp(DateUtil.DateAddSec(endtime, -rsireccleanupdelay));
			rang1.setStarttime(timestamp2);
			rang1.setEndtime(DateUtil.dateToTimestamp(endtime));			
			
			
			Extentactivitytimerange rang2 = new Extentactivitytimerange();
			rang2.setParentid(parentid);
			rang2.setStringdata("Extent");			
			rang2.setStarttime(timestamp1);
			rang2.setEndtime(timestamp2);			
					
			
			Extentactivitytimerange rang3 = new Extentactivitytimerange();
			rang3.setParentid(parentid);
			rang3.setStringdata("Extent");			
			rang3.setStarttime(DateUtil.dateToTimestamp(DateUtil.DateAddSec(starttime, 1)));
			rang3.setEndtime(DateUtil.dateToTimestamp(DateUtil.DateAddSec(starttime, 2)));
			
			
			
			al.add(rang);
			al.add(rang2);
			al.add(rang3);
			al.add(rang1);
			al.add(rang);
			
			
			
		}else if(type ==ActivityType.RSI_PBK){
			// three row
			Extentactivitytimerange rang = new Extentactivitytimerange();
			rang.setParentid(parentid);
			rang.setStringdata("Extent");
			rang.setStarttime(DateUtil.dateToTimestamp(starttime));
			Timestamp timestamp1 = DateUtil.dateToTimestamp(DateUtil.DateAddSec(starttime, rsipbsetupdelay));
			rang.setEndtime(timestamp1);			
			
			
			Extentactivitytimerange rang1 = new Extentactivitytimerange();
			rang1.setParentid(parentid);
			rang1.setStringdata("Extent");
			Timestamp timestamp2 =DateUtil.dateToTimestamp(DateUtil.DateAddSec(endtime, -rsipbcleanupdelay));
			rang1.setStarttime(timestamp2);
			rang1.setEndtime(DateUtil.dateToTimestamp(endtime));			
			
			
			Extentactivitytimerange rang2 = new Extentactivitytimerange();
			rang2.setParentid(parentid);
			rang2.setStringdata("Extent");			
			rang2.setStarttime(timestamp1);
			rang2.setEndtime(timestamp2);
			
			al.add(rang);
			al.add(rang2);
			al.add(rang1);
			
		}else if(type ==ActivityType.DDT){
			// 7 row
			Extentactivitytimerange rang = new Extentactivitytimerange();
			rang.setParentid(parentid);
			rang.setStringdata("Extent");
			
			Timestamp timestamp = DateUtil.dateToTimestamp(DateUtil.DateAddSec(starttime, rsirecsetupdelay));
			rang.setStarttime(timestamp);			
			rang.setEndtime(DateUtil.dateToTimestamp(endtime));			
			
			
			Extentactivitytimerange rang1 = new Extentactivitytimerange();
			rang1.setParentid(parentid);
			rang1.setStringdata("Extent");
			Timestamp timestamp2 =DateUtil.dateToTimestamp(DateUtil.DateAddSec(starttime, 1));
			rang1.setStarttime(timestamp2);
			Timestamp timestamp3 =DateUtil.dateToTimestamp(DateUtil.DateAddSec(starttime, 2));
			rang1.setEndtime(timestamp3);			
			
			
			Extentactivitytimerange rang2 = new Extentactivitytimerange();
			rang2.setParentid(parentid);
			rang2.setStringdata("Extent");			
			rang2.setStarttime(DateUtil.dateToTimestamp(endtime));
			rang2.setEndtime(DateUtil.dateToTimestamp(endtime));			
					
			
			Extentactivitytimerange rang3 = new Extentactivitytimerange();
			rang3.setParentid(parentid);
			rang3.setStringdata("Extent");			
			rang3.setStarttime(DateUtil.dateToTimestamp(starttime));			
			rang3.setEndtime(DateUtil.dateToTimestamp(endtime));
			
			
			Extentactivitytimerange rang4 = new Extentactivitytimerange();
			rang4.setParentid(parentid);
			rang4.setStringdata("Extent");			
			rang4.setStarttime(DateUtil.dateToTimestamp(starttime));			
			rang4.setEndtime(DateUtil.dateToTimestamp(endtime));
			
			Extentactivitytimerange rang5 = new Extentactivitytimerange();
			rang5.setParentid(parentid);
			rang5.setStringdata("Extent");			
			rang5.setStarttime(DateUtil.dateToTimestamp(starttime));			
			rang5.setEndtime(timestamp);		
			
			
			
			al.add(rang5);
			al.add(rang);
			al.add(rang1);
			al.add(rang2);
			al.add(rang3);
			al.add(rang4);
			al.add(rang4);		
			
			
					
			
		}
		
		return al;
	}
	
	

}
