package mpss.sched.gantt;

import java.util.ArrayList;
import java.util.List;

import mpss.sched.solver.*;

import psengine.PSObjectList;
import psengine.PSResource;
import psengine.PSToken;
import psengine.PSTokenList;

/** Copied from the original EuropaPlugin */
public class SchedGantt {
	/** Visible bounds on all activities */
	private int start, end;
	private PSObjectList resources;

	public SchedGantt(SolverEngine solver) {
		this(solver, "Object");
	}

	public SchedGantt(SolverEngine solver, String objectsTypes) {
		this.resources = solver.getEngine().getObjectsByType(objectsTypes);
		int[] hor = solver.getHorizon();

		// Compute start and end of the time line, truncate at horizon
		boolean first = true;
		for (int r = 0; r < resources.size(); r++) {
			PSTokenList tokens = resources.get(r).getTokens();
			for (int i = 0; i < tokens.size(); i++) {
				PSToken token = tokens.get(i);
				int s = (int) (token.getStart().getLowerBound());
				if (s < hor[0])
					s = hor[0];
				int e = (int) (token.getEnd().getUpperBound());
				if (e > hor[1])
					e = hor[1];
				if (first) {
					start = s;
					end = e;
					first = false;
				} else {
					start = Math.min(start, s);
					end = Math.max(end, e);
				}
			}
		}
	}

	public int getResourceCount() {
		return resources.size();
	}

	public List<IGanttActivity> getActivities(int resource) {
		assert (resource >= 0 && resource < getResourceCount());

		// Original comment: cache activities?
		ArrayList<IGanttActivity> acts = new ArrayList<IGanttActivity>();

		PSTokenList tokens = resources.get(resource).getTokens();
		for (int i = 0; i < tokens.size(); i++) {
			PSToken token = tokens.get(i);

			acts.add(new SchedGanttActivity(token, start, end));
		}

		return acts;
	}

	public IGanttResource getResource(int resource) {
		PSResource r = PSResource.asPSResource(resources.get(resource));
		if (r == null)
			return null;
		return new SchedGanttResource(r);
	}

	public int getStart() {
		return start;
	}

	public int getEnd() {
		return end;
	}

	public String getResourceName(int index) {
		return resources.get(index).getEntityName();
	}
}
