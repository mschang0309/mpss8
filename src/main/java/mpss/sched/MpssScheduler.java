package mpss.sched;

import java.io.File;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mpss.sched.ac.*;
import mpss.sched.solver.*;
import mpss.sched.gantt.*;

@Service("mpssScheduler")
public class MpssScheduler
{
	//private int horizonStart;
	//private int horizonEnd;
	
	@Autowired
	SchedProblem schedProblem;
	
	SchedSolver schedSolver = new SchedSolver();
	
	public MpssScheduler()
	{
	}

  public void open(File plannerConfigFile, int horizonStart, int horizonEnd)
  {
 	  schedSolver.openEngine(plannerConfigFile, horizonStart, horizonEnd);
  }
  
	public String defineProblem(String sessionName)
	{
		String nddl = "";
      nddl = schedProblem.generate(sessionName);
      schedSolver.execute(nddl);
      
      // get horizon from session
      
      return nddl;
	}
	
	public void solveProblem(String nddl)
	{
		schedSolver.solve();
	}

	public void getGantts()
	{
		SchedGantt gantt = new SchedGantt(schedSolver.getEngine());
		int resourceCount  = gantt.getResourceCount();
		for (int i = 0; i  < resourceCount; i++)
		{
			String name = gantt.getResourceName(i) ;
			System.out.println("resource: " + name);
			
			List<IGanttActivity> acts = gantt.getActivities(i);
			for (IGanttActivity a : acts)
			{
			    System.out.println("Act: " + a.getText() );
			}
			
		}
	}

}