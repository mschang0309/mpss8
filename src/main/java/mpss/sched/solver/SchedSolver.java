package mpss.sched.solver;

import java.io.*;
import psengine.PSUtil;


//@Service
public class SchedSolver
{
	private SolverEngine engine = new SolverEngine();
	
	public SolverEngine getEngine()
	{
		return this.engine;
	}
	
	public SchedSolver()
	{
	}

  //== public methods
	public void openEngine(File plannerConfigFile, int horizonStart, int horizonEnd) 
	{
		this.engine.configure(plannerConfigFile, horizonStart, horizonEnd);
		
		// load PSEngine dynamically
		PSUtil.loadLibraries("o");
		
		// make engine instance
		this.engine.startEngine();
	}

	public void execute(String nddl) 
	{
		// execute NDDL script
		this.engine.executeNddl(nddl);
	}
	
	public boolean solve() {
		// start solver (NDDL script should execute successfully before this)
		this.engine.startSolver();
		
		// run N steps
		
		return true;
	}
	

}
