package mpss.sched.solver;

import java.util.*;
import java.io.*;
import psengine.PSEngine;
import psengine.PSSolver;
import psengine.StringErrorStream;
import psengine.PSUtil;


//@Service
public class SolverEngine
{
  // this class refers to org.ops.ui.solver.model.SolverModel implementation in EuropaPlugin
	private PSEngine engine = null;
	private PSSolver solver = null;	
	
	private boolean nddlLoaded = false;
	
	/** Property name in the engine config: list of search paths for includes */
	private final String INCLUDE_PATH = "nddl.includePath";
	
	private ArrayList<SolverListener> listeners = new ArrayList<SolverListener>();
	private ArrayList<StepStatisticsRecord> stepStatistics = new ArrayList<StepStatisticsRecord>();
	

	public SolverEngine()
	{
	}

  /** model file to load */
	private File modelFile = null;
	/** Planner configuration file */
	private File plannerConfig = null;
	/** Horizon bounds */
	private int horizonStart, horizonEnd;
	
	//== public properties
	public PSEngine getEngine() {
		return engine;
	}

	public File getModelFile() {
		return this.modelFile;
	}
	
	public File getPlannerConfig() {
		return this.plannerConfig;
	}

	/** @return two numbers: horizon start and horizon end */
	public int[] getHorizon() {
		int[] res = new int[2];
		// In case model is build from BSH file, not NDDL
		if (solver == null)
			return res;
		res[0] = solver.getHorizonStart();
		res[1] = solver.getHorizonEnd();
		return res;
	}

	public void setHorizon(int horizonStart, int horizonEnd) {
		this.solver.configure(horizonStart, horizonEnd);
	}

	public int getStepCount() {
		return solver.getStepCount();
	}

  //== public methods
	public void configure(File plannerConfigFile, int horizonStart, int horizonend) 
	{
		this.plannerConfig = plannerConfigFile;
		
		// horizon information required by PSSolver
		this.horizonStart = horizonStart;
		this.horizonEnd = horizonend;
	}

	/** @return true if the engine is not up and running. */
	public boolean isTerminated() {
		return solver == null;
	}
	
	/** Start and configure engine. Assume it is not already running */
	public synchronized void startEngine() {
		if (engine != null)
			throw new IllegalStateException("Restarting solver model without shutdown");

		PSUtil.loadLibraries("o");
		System.out.println("start Engine");
		StringErrorStream.setErrorStreamToString();
		this.engine = PSEngine.makeInstance();
		this.engine.start();
		System.out.println("Engine started");

	}

	public synchronized void startSolver() {

		if (this.nddlLoaded == false)
		{
			return;
		}
		System.out.println("create solver");
		this.solver = engine.createSolver(this.plannerConfig.getAbsolutePath());
		System.out.println("solver created");
		
		//log.log(Level.INFO, "Solver created with {0}", this.plannerConfig);
		this.setHorizon(horizonStart, horizonEnd);

		// Initialize records
		stepStatistics.clear();
		stepStatistics.add(new StepStatisticsRecord(solver, 0));
		for (SolverListener lnr : listeners)
			lnr.solverStarted();
	}
	
	/** Shutdown the engine */
	public synchronized void terminate() {
		if (engine == null)
			throw new IllegalStateException(
					"Cannot shutdown - nothing initialized");
		if (solver != null)
			solver.delete();
		engine.shutdown();
		engine.delete();
		solver = null;
		engine = null;
		for (SolverListener lnr : listeners)
			lnr.solverStopped();
	}

	public void addSolverListener(SolverListener lnr) {
		if (!listeners.contains(lnr))
			listeners.add(lnr);
	}

	public void removeSolverListener(SolverListener lnr) {
		listeners.remove(lnr);
	}

	public void executeNddl(File file) {
		String oldPath = this.engine.getConfig().getProperty(INCLUDE_PATH);
		try {
			String newPath = file.getParent();
			if (oldPath != null)
			    newPath = newPath + File.pathSeparator + oldPath;
			this.engine.getConfig().setProperty(INCLUDE_PATH, newPath);
			// Call plain nddl, not AST, so that it loads
			// second argument is either full path name (when isFile flag is true) 
			// or NDDL script text (when isFile flag is false) 
			this.engine.executeScript("nddl", file.getAbsolutePath(), true);
		} catch (Exception e) {
			System.err.println("Cannot load NDDL file? " + e);
		} finally {
			this.engine.getConfig().setProperty(INCLUDE_PATH, oldPath);
			this.nddlLoaded = true;
		}
	}
	
	public void executeNddl(String nddl) {
		try {
			System.out.println("execute Script");
			this.engine.executeScript("nddl", nddl, false);	
			System.out.println("Script executed");
		} catch (Exception e) {
			System.err.println("Cannot load NDDL file? " + e);
		} finally {
			this.nddlLoaded = true;
		}
	}
	
	public boolean canStep() {
		return solver.hasFlaws() && !solver.isExhausted()
				&& !solver.isTimedOut();
	}

	/**
	 * Make one step (assuming we can)
	 * 
	 * @return time in ms
	 */
	public long stepOnce() {
		assert (canStep());
		for (SolverListener lnr : listeners)
			lnr.beforeStepping();
		long time = System.currentTimeMillis();
		solver.step();
		time -= System.currentTimeMillis();
		StepStatisticsRecord rec = new StepStatisticsRecord(solver, time);
		assert (rec.getStep() == solver.getStepCount());
		this.stepStatistics.add(rec);
		for (SolverListener lnr : listeners)
			lnr.afterStepping();
		return time;
	}

	/** Run for up to N steps. @return the actual number of steps performed */
	public long stepN(int times, boolean notifyEachStep) {
		for (SolverListener lnr : listeners)
			lnr.beforeStepping();
		int actual = 0;
		while (canStep() && actual < times) {
			long time = System.currentTimeMillis();
			solver.step();
			time = System.currentTimeMillis() - time;
			StepStatisticsRecord rec = new StepStatisticsRecord(solver, time);
			assert (rec.getStep() == solver.getStepCount());
			this.stepStatistics.add(rec);
			actual++;
			if (notifyEachStep)
				for (SolverListener lnr : listeners)
					lnr.afterOneStep(time);
		}
		for (SolverListener lnr : listeners)
			lnr.afterStepping();
		return actual;
	}

	public StepStatisticsRecord getStepStatistics(int step) {
		if (step >= this.stepStatistics.size())
			return StepStatisticsRecord.getEmpty();
		return this.stepStatistics.get(step);
	}

	/** Get contents of the engine output stream and clean the stream up */
	public String retrieveEngineOutput() {
		return StringErrorStream.retrieveString();
	}

}
