package mpss.sched;

import java.io.File;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import mpss.util.Utils;

public class MpssSchedulerConsole 
{
	public static void main(String args[])
	{
      
      try {
	      // Spring application context
    	  ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf"}));
        //if (context == null) {
        //	System.out.println("load application context failed");
        //	return;
        //}
    	  File plannerConfigFile = Utils.openResourceFile("PlannerConfig.xml");
    	  MpssScheduler scheduler = (MpssScheduler)context.getBean("mpssScheduler");	  
        if (scheduler == null) {
        	System.out.println("load MpssScheduler bean failed");
        	return;
        }
        
        // get schedule horizon from session information
        int horizonStart = 0;
        int horizonEnd = 86400;
        
        // open scheduler (engine)
        scheduler.open(plannerConfigFile, horizonStart, horizonEnd);
        
        // define scheduling problem (in NDDL format)
        String nddl = scheduler.defineProblem("day153");
		System.out.println("Generated nddl: ");
		System.out.println(nddl);
		
		// solve scheduling problem
		scheduler.solveProblem(nddl);
		
		scheduler.getGantts();
		
		
   	} catch (Exception ex) {
    		ex.printStackTrace();
      }
      
      System.out.println("MpssScheduler complete");
	    
	}
}
