package mpss.sched.ac;

import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import mpss.common.jpa.*;
import mpss.common.dao.*;


@Service("schedProblem")
public class SchedProblem
{
	@Autowired
	SessionDao sessionDao;
	@Autowired
	SchedulerDao schDao;
	
	//Map<String, Class<?>> factories = new HashMap<String, Class<?>>();
	Map<String, String> schedActBeans = new HashMap<String, String>();
	Map<String, Integer> schedActCounts = new HashMap<String, Integer>();
	
	List<ScActivity> dynamicActs = new ArrayList<ScActivity>();
	
	public List<ScActivity> getDynamicActivities()
	{
		return this.dynamicActs;
	}
	
	public SchedProblem()
	{
		try
		{
		    //this.factories.put("MON", ScMonitoringActivityFactory.class);
		    this.schedActBeans.put("MON", "scMonitoringActivity");
		    this.schedActCounts.put("MON", 0);
		    this.schedActBeans.put("MANEUVER", "scManeuverActivity");
		    this.schedActCounts.put("MANEUVER", 0);
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	// generate scheduling model
	public String generate(String sessionName)
	{
		String text = "";
		
		// generate model
		text += generateModel();
		
		// generate initial states
		text += generateInitialStates(sessionName);
		
		return text;
	}
	
	private String generateModel()
	{
		String text = "";
		text += "class Satellite extends Timeline {\n";
		// constructor
		text += "  Satellite () {\n";

		text += "  }\n";
		
		// predicate Monitoring
		text += "  predicate Monitoring {\n";
		text += "  }\n";

		// predicate Maneuver
		text += "  predicate Maneuver {\n";
		text += "  }\n";
		
		// end of Satellite class declaration
		text += "}\n";

        // predicate Monitoring implementation
		text += "Satellite::Monitoring {\n";
		text += "}\n";

	    // predicate Maneuver implementation
		text += "Satellite::Maneuver {\n";
		text += "}\n";


		text += "Satellite satellite1 = new Satellite();\n";
		text += "close();\n";
		
		return text;
	}

	private String generateInitialStates(String sessionName)
	{
		createSchedlueActivities(sessionName);
		
		String text = "";
		// generate goals (initial states) from planned activities
		for (ScActivity act : dynamicActs)
		{
			text += act.generate();
		}
		return text;
	}
	
	// create scheduler activities for a session
	private void createSchedlueActivities(String sessionName)
	{
		Session session = this.schDao.getSession(sessionName);
		
		// get all planned activities within a session
		List<Activity> acts = this.schDao.listSessionActivities(session);

		// Spring application context
		ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf"}));

		// 
		for (Activity act : acts)
		{
			String actType;
			if (!act.getActsubtype().equalsIgnoreCase("NO"))
			{
				actType = act.getArlentrytype() + ":" + act.getActsubtype();
			}
			else
			{
				actType = act.getArlentrytype();
			}
			actType.toUpperCase();
			// get a schedule activity creator by ARL entry type
			//Class<?> c = this.factories.get(actType);
			//if (c == null) 
			//{
				//System.out.printf("Activity type code not valid: %s\n", actType);
			//	continue;
		    //}
		    try {
		    	// get schedule activity bean from application context
		    	String beanName = this.schedActBeans.get(actType);
		    	if (beanName == null)
		    	{
					System.out.printf("Activity type code not supported: %s\n", actType);
					continue;
		    	}
		    	ScActivity schedAct = (ScActivity)context.getBean(beanName);	  
		        if (schedAct == null) {
		        	System.out.printf("load %s bean failed\n", beanName);
		        	return;
		        }
		        // assign sequence no
		        int seqNo = this.schedActCounts.get(actType);
		        seqNo++;
		        this.schedActCounts.put(actType, seqNo);
		        
				// create a schedule activity from a planned activity
		        schedAct.create(seqNo, act);
		        // note: must clone from Spring managed bean (otherwise every object reference points to the same Spring bean)
		        dynamicActs.add((ScActivity)schedAct.clone());
		        
	    	} catch (Exception ex) {
	    		ex.printStackTrace();
	      }
			
		}
	}
	
}