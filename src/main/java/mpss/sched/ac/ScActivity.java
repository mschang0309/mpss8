package mpss.sched.ac;

import mpss.common.jpa.Activity;

public interface ScActivity extends java.lang.Cloneable
{
	int getId();
	void setId(int id);
	int getSeqNo();
	boolean create(int seqNo, Activity act);
	String generate();
	public Object clone()  throws CloneNotSupportedException;
}