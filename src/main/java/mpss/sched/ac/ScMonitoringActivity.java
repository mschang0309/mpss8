package mpss.sched.ac;

import org.springframework.stereotype.Component;
import mpss.common.jpa.Activity;


@Component("scMonitoringActivity")
public class ScMonitoringActivity extends ScActivityBase implements Cloneable
{
	
  public ScMonitoringActivity()
  {
  }  
  
  public boolean create(int seqNo, Activity act)
  {
	  super.create(seqNo, act);
	  System.out.println("mon"+this.seqNo);
	  System.out.println("activity start= "+act.getStarttime());
	  System.out.println("session start= "+this.session.getStarttime());
	  System.out.println("startoffset= "+this.startOffset);
	  return true;
  }
  
  public String generate()
  {
	  String tokenName = String.format("mon%d", seqNo);
    // initial tokens
    //goal(Satellite.Monitoring mon1);
    //eq(mon1.m_duration, 15);
    //mon1.start.specify(0);
    String initialToken = String.format("goal(Satellite.Monitoring %s);\n", tokenName);
    //initialToken += String.format("eq(%s.duration, %s)\n", tokenName, plannedActivity.getDuration());
    //initialToken += String.format("%s.start.specify(%d)\n", tokenName, startOffset);
    initialToken += String.format("%s.start.specify([%d %d]);\n", tokenName, startOffset, endOffset);
    initialToken += String.format("%s.end.specify([%d %d]);\n", tokenName, startOffset, endOffset);
    initialToken += String.format("%s.duration.specify([%d %d]);\n", tokenName, plannedActivity.getDuration(), endOffset - startOffset);
    initialToken += String.format("%s.activate();\n", tokenName, startOffset);
	  return initialToken;
  }
  @Override
  public Object clone() throws CloneNotSupportedException {
   ScMonitoringActivity o = (ScMonitoringActivity) super.clone();
   return o;
  }
}