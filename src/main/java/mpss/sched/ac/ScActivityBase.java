package mpss.sched.ac;

import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import mpss.common.jpa.Activity;
import mpss.common.jpa.Parameterset;
import mpss.common.jpa.Session;

import mpss.common.dao.SessionManDao;

@Component
public class ScActivityBase implements ScActivity
{
	@Autowired 
	SessionManDao sessionManDao;
	
	protected int id;
	// planned activity
	protected Activity plannedActivity;
	// owner session
	protected Session session;
	// parameter set
	protected Parameterset parameterSet;
	
	protected long startOffset;
	protected long endOffset;
	
	protected int seqNo;

	public int getId()
	{
		return this.id;
	}
	public void setId(int id)
	{
		this.id =id;
	}

	public int getSeqNo()
	{
		return this.seqNo;
	}

  public Activity getPlannedActivity()
  {
	  return this.plannedActivity;
  }

  public ScActivityBase()
  {
  }  

  public boolean create(int seqNo, Activity act)
  {
	  //System.out.println("ScActivityBase.create: activity= "+act.getName());
	  
	  this.seqNo = seqNo;
	  this.plannedActivity = act;
	  
	  //System.out.println("ScActivityBase.create: seq no= "+seqNo);
	  //System.out.println("ScActivityBase.create: session id= "+this.plannedActivity.getSessionid());
	  
	  // get owner session of the planned activity
	  if (this.sessionManDao == null)
	  {
		  System.out.println("ScActivityBase.create: sessionManDao is not injected");
	  }
	  this.session = this.sessionManDao.getSessionById(this.plannedActivity.getSessionid());
	  //System.out.println("ScActivityBase.create: session= "+session.getName());
	  
	  // start time offset
	  this.startOffset = TimeUnit.SECONDS.convert(this.plannedActivity.getStarttime().getTime() - this.session.getStarttime().getTime(), TimeUnit.MILLISECONDS);
	  this.endOffset = TimeUnit.SECONDS.convert(this.plannedActivity.getEndtime().getTime() - this.session.getStarttime().getTime(), TimeUnit.MILLISECONDS);
	  //System.out.println("ScActivityBase.create: startOffset= "+startOffset);
		
 	  // get parameter set of the session
	  this.parameterSet = this.sessionManDao.getParameterset(this.session.getName());
	  
	  return true;
  }
  
  public String generate()
  {
	  // empty content
	  return "\n";
  }
  
  public Object clone() throws CloneNotSupportedException 
  {
	  ScActivityBase o = (ScActivityBase)super.clone();
	  return o;
  }
}