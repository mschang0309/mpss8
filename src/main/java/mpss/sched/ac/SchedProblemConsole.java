package mpss.sched.ac;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class SchedProblemConsole 
{
	public static void main(String args[])
	{
      
      try {
	      // Spring application context
    	  ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf"}));
        //if (context == null) {
        //	System.out.println("load application context failed");
        //	return;
        //}
    	  SchedProblem schedProblem = (SchedProblem)context.getBean("schedProblem");	  
        if (schedProblem == null) {
        	System.out.println("load SchedProblem bean failed");
        	return;
        }
        String nddl = schedProblem.generate("day153");
        System.out.println("generate NDDL:");
        System.out.println(nddl);
    	} catch (Exception ex) {
    		ex.printStackTrace();
      }
      
      System.out.println("SchedProblem generated");
	    
	}
}
