package mpss.aip;

//import java.io.FileInputStream;

import mpss.config;
import mpss.configFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class AIPCreatorConsole 
{
	public static void main(String args[])
	{
		      
      try {
    	  
    	  System.setProperty("satName", "FORMOSAT5");
  		  config.load();  
	      // Spring application context
    	  ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf"}));

    	  AIPCreator aipCreator = (AIPCreator)context.getBean(configFactory.getAipCreator());	  
        if (aipCreator == null) {
        	System.out.println("AIP creator bean failed");
        	return;
        }
        
        // int actid
        aipCreator.ReSchPROC(23);
        
        // int act_id , int contact_id
        aipCreator.ReSchPBK(111, 123);
        
         
        
  	} catch (Exception ex) {
  		System.err.println(ex.getMessage());
    }

	}
}
