package mpss.aip;


import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import mpss.configFactory;
import mpss.common.dao.ActivityDao;
import mpss.common.dao.ContactDao;
import mpss.common.dao.ExtentDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Extent;
import mpss.util.timeformat.TimeRange;

public class AIPCreator {
	
	@Autowired
    ActivityDao actDao;
	@Autowired
	ExtentDao extDao;
	@Autowired
	ContactDao contDao;

    
    public String returnMsg;
    private Logger logger = Logger.getLogger(configFactory.getLogName());


	public boolean ReSchPROC (int actId)
	{
		logger.info("Start AIP Procedure ReSchedule");
		returnMsg="";
		actDao.reset();
		Activity act = actDao.get((long) actId);
		if (act == null)
		{
			returnMsg = "Activity ID " + actId + " not found.";
			logger.error("Activity ID " + actId + " not found.");
			return false;
		}
		
		if (!createAIPExtent(act))
		{
			returnMsg = "ReSchPROC: AIP Create Extent failed.";
			return false;
		}

		return true;
	}
	
	public boolean ReSchPBK (int actId,int contId)
	{
		logger.info("Start AIP PBK ReSchedule");
		returnMsg="";
		actDao.reset();
		Activity act = actDao.get((long) actId);
		Contact cont = contDao.get((long) contId);
		
		if (act == null)
		{
			returnMsg = "Activity ID " + actId + " not found.";
			logger.error("Activity ID " + actId + " not found.");
			return false;
		}
		
		if (cont == null)
		{
			returnMsg = "Contact ID " + contId + " not found.";
			logger.error("Contact ID " + contId + " not found.");
			return false;
		}
		
		Timestamp pbkStart = cont.getCmdrise();
		Timestamp pbkEnd = cont.getCmdfade();
		if (act.getStarttime().before(cont.getCmdfade()) && act.getStarttime().after(cont.getCmdrise()))
			pbkStart = act.getStarttime();
		if (act.getEndtime().before(cont.getCmdfade()) && act.getEndtime().after(cont.getCmdrise()))
			pbkEnd = act.getEndtime();
		TimeRange pbkTR = new TimeRange(pbkStart, pbkEnd);
		
		// BPK starttime must be after REC Stoptime
		Timestamp StopRectime = getStopRECEndtime(act.getStarttime(), act.getInfo());
		if (!pbkStart.after(StopRectime))
		{
			returnMsg = "ReSchedule failed. PBK start time[" + pbkStart.toString() + "] must be after REC Stop time[" + StopRectime.toString() + "]";
			return false;
		}
		//AIP PBK Duration < 12min Alarm
		if (pbkTR.duration < 12*60)
			returnMsg = "Warning: Duration[" + pbkTR.duration + "] less than 7200 seconds";
		
		if (!createAIPExtent(act,pbkTR,contId))
		{
			returnMsg = "ReSchPBK: AIP Create Extent failed.";
			return false;
		}
		
		return true;
	}
	
	private boolean createAIPExtent(Activity act)
	{
		
		Extent ext = new Extent();
		
		ext.setActivityid(act.getId().intValue());
		ext.setStarttime(act.getStarttime());
		ext.setEndtime(act.getEndtime());
		ext.setContactid(0);
		ext.setBranchid(0);
		ext.setTransmitterid(0);
		ext.setRecorderid(0);
		ext.setAntennaid(0);
		ext.setRecorderdelta(0);
		ext.setSecondrecorderdelta(0);
		ext.setFilename(0);
		ext.setSecondfilename(0);
		ext.setUsermodified(0);
		ext.setUserlocked(0);
		ext.setSoftconstraintviolated(0);
		ext.setHardconstraintviolated(0);
		ext.setIsaip(1);
		
		try
		{
			ext = extDao.create(ext);
			
			act.setExtentid(ext.getId().intValue());
			act.setScheduled(1);
			actDao.update(act);
			logger.info("AIP Extent created: " + ext.getId());			
		}
		catch (Exception e)
		{
			logger.info("AIP Create Extent failed :"+e.getMessage());
			return false;
		}
		
		
		return true;
	}
	
	private boolean createAIPExtent(Activity act,TimeRange tr,int contId)
	{
		
		Extent ext = new Extent();
		
		ext.setActivityid(act.getId().intValue());
		ext.setStarttime(tr.first);
		ext.setEndtime(tr.second);
		ext.setContactid(contId);
		ext.setBranchid(0);
		ext.setTransmitterid(0);
		ext.setRecorderid(0);
		ext.setAntennaid(0);
		ext.setRecorderdelta(0);
		ext.setSecondrecorderdelta(0);
		ext.setFilename(0);
		ext.setSecondfilename(0);
		ext.setUsermodified(0);
		ext.setUserlocked(0);
		ext.setSoftconstraintviolated(0);
		ext.setHardconstraintviolated(0);
		ext.setIsaip(1);
		
		try
		{
			ext = extDao.create(ext);
			
			act.setExtentid(ext.getId().intValue());
			act.setScheduled(1);
			actDao.update(act);
			logger.info("AIP Extent created: " + ext.getId());			
		}
		catch (Exception e)
		{
			logger.info("AIP Create Extent failed :"+e.getMessage());
			return false;
		}
		
		
		return true;
	}
	
	private Timestamp getStopRECEndtime(Timestamp pbkTime, String filename)
	{
		Activity procAct =actDao.getStopRec(pbkTime, filename);
		if (procAct != null)
			return procAct.getEndtime();
		else
			return null;
	}

}
