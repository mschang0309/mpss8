package mpss.rpt;

import java.util.Date;

import mpss.config;
import mpss.configFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class ReportCreatorConsole 
{
	public static void main(String args[])
	{
		      
      try {
    	  System.setProperty("satName", "FORMOSAT5");
  		  config.load(); 
	      // Spring application context
    	  ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf",configFactory.getSatelliteName()}));

    	  ReportCreator rptCreator = (ReportCreator)context.getBean(configFactory.getReportCreator());  
          if (rptCreator == null) {
        	System.out.println("rpt creator bean failed");
        	return;
          }
        
          // aip rpt
          int sessionid =1111;
          //rptCreator.setFileDir (System.getProperty("reportFileDir")); 
      	  //rptCreator.setSatName (System.getProperty("satName")); 	
          //rptCreator.GenAIP(sessionid);
          
          
          // rsi rpt
          rptCreator.setSection(sessionid); 
      	  rptCreator.setBranch(false,0);
      	  rptCreator.setSelRSI (true); 
      	  rptCreator.setSelSSR (true);
      	  rptCreator.setRSIFileName ("rsi_file_name.txt"); 
      	  rptCreator.setSSRFileName ("ssr_file_name.txt");
      	  rptCreator.setRptTimeRange (new Date(), new Date());
      	  rptCreator.setFileDir (System.getProperty("reportFileDir")); 
      	  rptCreator.setSatName (System.getProperty("satName")); 
      	  
      	  rptCreator.GenReport();
        
  	} catch (Exception ex) {
  		System.err.println(ex.getMessage());
    }

	}
}
