package mpss.rpt;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import mpss.cl.RS2Policy;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Aipprocrequest;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Recorderp;
import mpss.common.jpa.Rev;
import mpss.common.jpa.Rsiimagingrequest;
import mpss.common.jpa.Session;
import mpss.common.jpa.Site;
import mpss.util.timeformat.RangeUtil;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeToString;

import org.springframework.stereotype.Component;

@Component("FS5reportCreator")
public class FS5ReportCreator extends ReportCreatorBase{	
   
	
	public boolean GenReport ()
	{
		WriteRSILog();
		
		boolean status = true;
		if (selRSI)
			if(!GenRsiReport())
				status = status && false;
		if (selSSR)
			if(!GenSsrReport())
				status = status && false;

	    try 
	    {
	    	logOSW.write("</body>\n</html>\n");
	        logOSW.close();	 

	    } catch (Exception e) {
	        logger.error("ReportCreator Error: Log File create Error");
	    }
	
		return status;
	}
	
	public boolean GenAIP (int sessionid)
	{
		Session sess = sessManDao.getSessionById(sessionid);
		rptTimeRange = new TimeRange(sess.getStarttime(), sess.getEndtime());
		WriteAIPLog();
		
		boolean status = true;
		if(!GenAipReport(sessionid))
			status = false;
		
	    try 
	    {
	    	logOSW.write("</body>\n</html>\n");
	        logOSW.close();	 

	    } catch (Exception e) {
	        logger.error("ReportCreator Error: Log File create Error");
	    }
	
		return status;
	}
	
	private boolean GenRsiReport()
	{
		logger.info("Start generate RSI Report ...");
		
		try
		{
			List<Extent> extents = extentDao.getStartWithinTimeRange(rptTimeRange, branchId);
			List<Extent> rsiPbkExts = new ArrayList<Extent>();
			List<Extent> rsiRelatedExts = new ArrayList<Extent>();
			List<Timestamp> XCrossTimes = new ArrayList<Timestamp>();
			for (Extent e : extents)
			{
				if (!rptTimeRange.encloses(actManDao.getOperationalPeriod(e)))
					continue;
				
				Activity act = actDao.get((long)e.getActivityid());
				if (act.getArlentrytype().equalsIgnoreCase("RSI"))
				{
					if (act.getActsubtype().equalsIgnoreCase("PBK"))
						rsiPbkExts.add(e);
					else
						rsiRelatedExts.add(e);
				}
				else if (act.getArlentrytype().equalsIgnoreCase("XCROSS"))
				{
					XCrossTimes.add(e.getStarttime());
				}
			}
			
			//pbk group by site
			List<Extent> temprsiRelatedExts = new ArrayList<Extent>();
			temprsiRelatedExts.addAll(rsiRelatedExts);
			Map<String, List<Extent> > sitePbkExtsMap = new HashMap<String, List<Extent> >();
			Map<String, List<Extent> > sitersiExtsMap = new HashMap<String, List<Extent> >();
			for(Extent e : rsiPbkExts)
			{
				String siteName="";
				Contact cont = contDao.get((long)e.getContactid());
				if (cont != null)
				{
					Site site = siteDao.get((long)cont.getSiteid());
					if(site != null)
						siteName = (site.getName().equalsIgnoreCase("MOSCOW1")) ? "MOSCOW" : site.getName().replace("X_", "");
				}
				if (sitePbkExtsMap.get(siteName) == null)
				{
					sitePbkExtsMap.put(siteName, new ArrayList<Extent>());
					sitersiExtsMap.put(siteName, new ArrayList<Extent>());
				}
				List<Extent> strpbkList = sitePbkExtsMap.get(siteName);
				strpbkList.add(e);
				
				//rsi group by site
				Activity rsiAct = actDao.get((long)e.getActivityid());
				while (rsiAct.getPrevactid()>0)
					rsiAct = actDao.get((long)rsiAct.getPrevactid());
				
				for(Extent rsiE : rsiRelatedExts)
				{
					if (rsiE.getActivityid().compareTo(rsiAct.getId().intValue()) == 0)
					{
						List<Extent> strrsiList = sitersiExtsMap.get(siteName);
						if (!strrsiList.contains(rsiE))
							strrsiList.add(rsiE);
						temprsiRelatedExts.remove(rsiE);
					}
				}
			}
			//NSPO site
			for(Extent e : temprsiRelatedExts)
			{
				Activity act = actDao.get((long)e.getActivityid());
				Rsiimagingrequest rsiEntry = rsiimgreqDao.get((long)act.getArlentryid());
				//Rsiimagingrequest rsiEntry = (Rsiimagingrequest)actManDao.getActEntry(act);
				String station = rsiEntry.getStation();
				if (station.equalsIgnoreCase("NSPO"))
				{
					if (sitersiExtsMap.get(station) == null)
						sitersiExtsMap.put(station, new ArrayList<Extent>());
					List<Extent> strrsiList = sitersiExtsMap.get(station);
					strrsiList.add(e);
				}
			}

			this.sitePbkExtsMap = sitePbkExtsMap;
			this.sitersiExtsMap = sitersiExtsMap;
			
			// Get FileName
			String rsiRptFilepath = fileDir + satName + "\\" + "Acquisition_schedule_reports" + "\\";
			String asrFilename = (rsiFileName.trim().length() > 0) ? "ASR_FILENAME_" + rsiFileName.trim() :
				"ASR_FILENAME_" + TimeToString.AsLongDate(rptTimeRange.first) + "_" + section +".txt";
			String pbkFilename = (rsiFileName.trim().length() > 0) ? "PBK_FILENAME_" + rsiFileName.trim() : 
				"PBK_FILENAME_" + TimeToString.AsLongDate(rptTimeRange.first) + "_" + section +".txt";
			String asrFilepath = rsiRptFilepath + asrFilename;
			String pbkFilepath = rsiRptFilepath + pbkFilename;
			
			File asrFile = new File(asrFilepath);
			File pbkFile = new File(pbkFilepath);
			
			if(!asrFile.exists())
			{
			    try {
			    	asrFile.createNewFile();
			    } catch (Exception e) {
			        e.printStackTrace();
			    }
			}
			if(!pbkFile.exists())
			{
			    try {
			    	pbkFile.createNewFile();
			    } catch (Exception e) {
			        e.printStackTrace();
			    }
			}
		
			FileOutputStream asrFOS = new FileOutputStream(asrFile);
	        FileOutputStream pbkFOS = new FileOutputStream(pbkFile);
	        OutputStreamWriter asrOSW = new OutputStreamWriter(asrFOS);
	        OutputStreamWriter pbkOSW = new OutputStreamWriter(pbkFOS);

	        //Print header information
	        asrOSW.write(DisplayRsiHeader("MMC MCC" ,asrFilename, "IPS PSS"));
	        pbkOSW.write(DisplayRsiHeader("MMC MCC", pbkFilename, "IPS PSS"));

			
			//Print PBK Content
			pbkOSW.write("Playback Window:\n");
			List<String> pbkLines = DisplayPbkInfo("ALL",rsiPbkExts);
			if (pbkLines != null)
			{
				for (String s : pbkLines)
					pbkOSW.write(s);
			}
			pbkOSW.write("END OF REPORT");
			pbkOSW.close();
			
			//Print RSI Content
	        asrOSW.write("INSTRUMENT: RSI" + "\n");
			List<String> asrRsiLines = DisplayRsiInfo ("ALL", rsiRelatedExts, XCrossTimes);
			if (asrRsiLines != null)
			{
				for (String s : asrRsiLines)
					asrOSW.write(s);
			}
			asrOSW.write("END OF REPORT");
			asrOSW.close();
			
	        // write log
	        logOSW.write("RSI ASR Report: " +
				"<a href=\"http://" + System.getProperty("mqIP") + ":8080/" + System.getProperty("web.path","mpssweb") + "/Schedule/showAsrRpt.jsp?filetype=asr&filename=" +
				asrFilepath + "\">" + asrFilepath + "</a> \n");
	        logOSW.write("RSI PBK Report: " +
				"<a href=\"http://" + System.getProperty("mqIP") + ":8080/" + System.getProperty("web.path","mpssweb") + "/Schedule/showAsrRpt.jsp?filetype=asr&filename=" +
				pbkFilepath + "\">" + pbkFilepath + "</a> \n\n");
			
			//file by site
			Set<String> siteSet = (sitePbkExtsMap.size() > sitersiExtsMap.size()) ? sitePbkExtsMap.keySet() : sitersiExtsMap.keySet();
			for (String sName : siteSet)
			{
				String sitePbkFilename = (rsiFileName.trim().length() > 0) ? "PBK_FILENAME_" + rsiFileName.trim() + "_" + sName : 
					"PBK_FILENAME_" + TimeToString.AsLongDate(rptTimeRange.first) + "_" + section + "_" + sName + ".txt";
				String siteAsrFilename = (rsiFileName.trim().length() > 0) ? "ASR_FILENAME_" + rsiFileName.trim() + "_" + sName : 
					"ASR_FILENAME_" + TimeToString.AsLongDate(rptTimeRange.first) + "_" + section + "_" + sName + ".txt";

				String sitePbkFilepath = rsiRptFilepath + sitePbkFilename;
				String siteAsrFilepath = rsiRptFilepath + siteAsrFilename;

				File sitePbkfile = new File(sitePbkFilepath);
				File siteAsrfile = new File(siteAsrFilepath);

				if(!sitePbkfile.exists())
				{
				    try {
				    	sitePbkfile.createNewFile();
				    } catch (Exception e) {
				        e.printStackTrace();
				    }
				}
				if(!siteAsrfile.exists())
				{
				    try {
				    	siteAsrfile.createNewFile();
				    } catch (Exception e) {
				        e.printStackTrace();
				    }
				}


		        FileOutputStream sitePbkFOS = new FileOutputStream(sitePbkFilepath);
		        FileOutputStream siteAsrFOS = new FileOutputStream(siteAsrFilepath);
		        OutputStreamWriter sitePbkOSW = new OutputStreamWriter(sitePbkFOS);
		        OutputStreamWriter siteAsrOSW = new OutputStreamWriter(siteAsrFOS);
		        //Print header information
		        sitePbkOSW.write(DisplayRsiHeader("MMC MCC",sitePbkFilename, sName));
		        siteAsrOSW.write(DisplayRsiHeader("MMC MCC",siteAsrFilename, sName));
				
		        //Print PBK Content
				sitePbkOSW.write("Playback Window:\n");
		        List<String> sitePbkLines = DisplayPbkInfo(sName,rsiPbkExts);
				if (sitePbkLines != null)
				{
					for (String s : sitePbkLines)
						sitePbkOSW.write(s);	
				}
				
				//Print RSI Content
				siteAsrOSW.write("INSTRUMENT: RSI" + "\n");
				List<String> siteAsrLines = DisplayRsiInfo (sName,rsiRelatedExts, XCrossTimes);
				if (siteAsrLines != null)
				{
					for (String s : siteAsrLines)
						siteAsrOSW.write(s);
				}
				sitePbkOSW.write("END OF REPORT");
				siteAsrOSW.write("END OF REPORT");
				sitePbkOSW.close();
				siteAsrOSW.close();
				
		        // write log
				logOSW.write("Group by site : " + sName + "\n"); 
		        logOSW.write("RSI ASR Report: " +
					"<a href=\"http://" + System.getProperty("mqIP") + ":8080/" + System.getProperty("web.path","mpssweb") + "/Schedule/showAsrRpt.jsp?filetype=asr&filename=" +
					siteAsrFilepath + "\">" + siteAsrFilepath + "</a> \n");
		        logOSW.write("RSI PBK Report: " +
					"<a href=\"http://" + System.getProperty("mqIP") + ":8080/" + System.getProperty("web.path","mpssweb") + "/Schedule/showAsrRpt.jsp?filetype=asr&filename=" +
					sitePbkFilepath + "\">" + sitePbkFilepath + "</a> \n\n");
			}
		}
		catch (Exception e){
			logger.error(e.getMessage());
			return false;
		}
		
		return true;
	}
	
	private boolean GenSsrReport()
	{
		logger.info("Start generate SSR Report ...");
		
		// Get FileName
		String ssrRptFilepath = fileDir + satName + "\\" + "SSR_Util_reports" + "\\";
		String ssrFilename = (ssrFileName.trim().length() > 0) ? ssrFileName.trim() : "SSR_Util_" + TimeToString.AsLongDate(rptTimeRange.first) + ".sch";
		String ssrFilepath = ssrRptFilepath + ssrFilename;
		File ssrfile = new File(ssrFilepath);
		try
		{
			FileOutputStream FOS = new FileOutputStream(ssrfile);
	        OutputStreamWriter OSW = new OutputStreamWriter(FOS);
	        
	        //Print header information
	        OSW.write(satName + " SSR Utilization Report\n\n");
	        OSW.write("Report Time Window:			" + TimeToString.AsRptTime(rptTimeRange.first) + " - " + TimeToString.AsRptTime(rptTimeRange.second) + "\n\n");
	        OSW.write("# SSR Filename:					References the data file stored on SSR\n");
	        OSW.write("# ISUAL Usage:					ISUAL SSR usage in sectors.  Value is (+) if record, (-) if deleting a file from SSR\n");
	        OSW.write("# ISUAL Available:			ISUAL SSR capacity - ISUAL usage\n");
	        OSW.write("# RSI Usage:						RSI SSR usage in sectors.  Value is (+) if record, (-) if deleting a file from SSR\n");
	        OSW.write("# RSI Available:				RSI SSR capacity - RSI usage\n");
	        OSW.write("# Total SSR Available:	Total SSR capacity - RSI usage - ISUAL usage\n\n");
	        OSW.write("Initial Capacities:\n");
	        
	        List<Extent> ssrExtents = new ArrayList<Extent>(extentDao.getStartWithinTimeRange(rptTimeRange,branchId));
	        RS2Policy rs2Policy = new RS2Policy();
	        boolean isfirstUsage = true;
	        for(Extent ext : ssrExtents)
	        {
	        	if (rptTimeRange.contains(ext.getEndtime()))
	        	{
	        		Activity ssrAct = actDao.get((long)ext.getActivityid());
	        		if (rs2Policy.NeedsRecorder(ssrAct))
	        		{
		        		if (isfirstUsage)
		        		{
		        			double rsiUsage = actManDao.getAvailCapacity(ext,"RSI",rptTimeRange.first);
		        			double isaulUsage = actManDao.getAvailCapacity(ext,"ISUAL",rptTimeRange.first);
		        			OSW.write("RSI   = " + String.format("%1.0f", rsiUsage) + "\n");
		        	        OSW.write("ISUAL = " + String.format("%1.0f", isaulUsage) + "\n");
		        	        OSW.write("Total = " + String.format("%1.0f", (rsiUsage + isaulUsage)) + "\n\n");
		        	        OSW.write("                                                SSR   RSI Usage        RSI  RSI DDT    ISUAL      ISUAL  Total SSR\n");
		        	        OSW.write("Time                            Activity   Filename  Write/Read  Available    Usage    Usage  Available  Available\n");
		        	        OSW.write("------------------------------------------------------------------------------------------------------------------\n");
		        	        //OSW.write(DisplaySSRInfo (ext, ssrAct, isfirstUsage));
		        	        isfirstUsage = false;
		        		}
		        		OSW.write(DisplaySSRInfo (ext, ssrAct, isfirstUsage));
	        		
	        			
	        		}
	        	}
	        }
	        
	        OSW.write("\nEND OF REPORT");
	        OSW.close();
	       
	        // write log
	        logOSW.write("SSR Report: " +
				"<a href=\"http://" + System.getProperty("mqIP") + ":8080/" + System.getProperty("web.path","mpssweb") + "/Schedule/showAsrRpt.jsp?filetype=asr&filename=" +
				ssrFilepath + "\">" + ssrFilepath + "</a> \n\n");
		}
		catch (Exception e){
			logger.info(e.getMessage());
			return false;
		}

        
		return true;
	}
	
	private boolean GenAipReport(int sessionid)
	{
		logger.info("Start generate AIP Report ...");		
		// FileName
		Timestamp startTime = rptTimeRange.first;
		String aipRptFilepath = fileDir + satName + "\\" + "AIP" + "\\";
		String aipASRname = "AIP_" + TimeToString.AsLongDate(startTime) + ".sch";
		String aipConfirmname = "AIP_" + TimeToString.AsLongDate(startTime) + "_confirm.rpt";
		String aipASRpath = aipRptFilepath + aipASRname;
		String aipConfirmpath = aipRptFilepath + aipConfirmname;
		
		File aipASRfile = new File(aipASRpath);
		File aipConfirmfile = new File(aipConfirmpath);
		try
		{
			FileOutputStream asrFOS = new FileOutputStream(aipASRfile);
	        OutputStreamWriter asrOSW = new OutputStreamWriter(asrFOS);
	        FileOutputStream confirmFOS = new FileOutputStream(aipConfirmfile);
	        OutputStreamWriter confirmOSW = new OutputStreamWriter(confirmFOS);
	        
	        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
	        
	        //Print ASR header information
	        asrOSW.write("SOURCE:             SOCC\n");
	        asrOSW.write("DESTINATION:        AIP SDC\n");
	        asrOSW.write("FILE NAME:          " + aipASRname + "\n");
	        asrOSW.write("DATE TIME:          " + TimeToString.AsStdTime(currentTime) + "\n");
	        asrOSW.write("SPACECRAFT:         " + satName + "\n");
	        asrOSW.write("INSTRUMENT:         AIP\n");
	        asrOSW.write("UTC                 DUR  Q1        Q2        Q3        Q4        Procedure\n");
	        
	        List<Activity> aipActs = actDao.getBySessionNType(sessionid, "AIPPROC");
	        String unschASR = "";
	        int i = 0;
	        for(Activity aipAct : aipActs)
	        {
	        	Aipprocrequest aipreq = aipreqDao.get((long) aipAct.getArlentryid());
	        	i++;
	        	if (i == 1)
	        	{
	        		//Print Confirm header information
	    	        confirmOSW.write("MISSION TIMELINE FILE: " + arlDao.get((long)aipreq.getArlid()).getName() + "\n");
	    	        confirmOSW.write("EVENT ACTIVITY            UTC                 Confirmation Status\n");
	        	}
	        	String procFullName = aipreq.getAipproc();
	        	String procName = procFullName.substring(procFullName.lastIndexOf("\\") +1);
	        	if (aipAct.getExtentid()>0)
        		{
	        		asrOSW.write(String.format("%-20s", TimeToString.AsRptTime(aipAct.getStarttime())) +
        				String.format("%-5s", aipAct.getDuration()) + "NA        NA        NA        NA        " +
        				procName + "\n");
	        		
	        		confirmOSW.write(String.format("%-5s",i) + String.format("%-20s",procName) +
	        			String.format("%-20s", TimeToString.AsRptTime(aipAct.getStarttime())) +
	        			String.format("%-19s", "accept") + "\n");
	        	}
        		else
        		{
        			unschASR += String.format("%-20s", procName) + 
        				String.format("%-20s", TimeToString.AsRptTime(aipAct.getStarttime())) +
        				"command mismatch\n";
        			confirmOSW.write(String.format("%-5s",i) + String.format("%-20s",procName) +
    	        			String.format("%-20s", TimeToString.AsRptTime(aipAct.getStarttime())) +
    	        			String.format("%-19s", "not checked") + "\n");
        		}
	        }
	        
	        asrOSW.write("Unscheduled Requests:\n");
	        asrOSW.write("Activity            UTC                 Error_MSG\n");
	        asrOSW.write(unschASR);
	        asrOSW.write("#");
	        asrOSW.write("END OF REPORT");
	        asrOSW.close();
	        
	        confirmOSW.write("#");
	        confirmOSW.write("END OF REPORT");
	        confirmOSW.close();
	       
	        // write log
	        logOSW.write("ASR Report: " +
				"<a href=\"http://" + System.getProperty("mqIP") + ":8080/" + System.getProperty("web.path","mpssweb") + "/Schedule/showAsrRpt.jsp?filetype=aip&filename=" +
				aipASRpath + "\">" + aipASRpath + "</a> \n\n");
	        logOSW.write("Confirm Report: " +
					"<a href=\"http://" + System.getProperty("mqIP") + ":8080/" + System.getProperty("web.path","mpssweb") + "/Schedule/showAsrRpt.jsp?filetype=aip&filename=" +
					aipConfirmpath + "\">" + aipConfirmpath + "</a> \n\n");
		}
		catch (Exception e){
			logger.info(e.getMessage());
			return false;
		}
		
		return true;
	}
	
	private String DisplayRsiHeader (String source, String fileName, String destination)
	{
		logger.info("Print Header File[" + fileName +"]");
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        String header = "SOURCE: " +source+ "\n" +
	        	"DESTINATION: " + destination + "\n" +
	        	"FILE NAME: " + fileName +"\n" +
	        	"DATE TIME: " + TimeToString.AsStdTime(currentTime) + "\n" +
	        	"SPACECRAFT: " + satName + "\n";
        
        return header;
	}
	
	//STATION   DATE        BOI_X     EOI_X     SIG_INT   ELEV    AZIM    ORBIT  GI   GJ_S GJ_E GK   LAT_S     LAT_E     MODE TYPE SSR_STATUS   FILENAME
	//ANY       2014/295    14:07:20  14:07:53  NA        -30.1   269.6   53299  0    0    0    0    +19.2000  +17.5000  3    1    REC       31,32,    
	//For ROCSAT2
	@SuppressWarnings("unused")
	private List<String> DisplayRsiInfo (String siteName,List<Extent> rsiExtents, List<Timestamp> XCrossTimes, boolean hasFileName)
	{
		logger.info("Display Rsi Info. By site[" + siteName +"], hasFileName[" + hasFileName + "]");
		List<Extent> rsiExts = new ArrayList<Extent>();
		switch (siteName)
		{
			case "ALL":
				rsiExts = rsiExtents;
				break;
//			case "NSPO":
//				break;
			default:
				rsiExts = sitersiExtsMap.get(siteName);
				break;
		}
		if (rsiExts.size() == 0)
			return null;
		
		//sort
		Collections.sort(rsiExts,
		   		new Comparator<Extent>() 
	    		{
	    			public int compare(Extent Ext1, Extent Ext2) 
	    			{
	    				return Ext1.getStarttime().compareTo(Ext2.getStarttime());
	    			}
	    		}
		    );
		
		List<String> linesStr = new ArrayList<String>();
		String heaherName = "STATION   DATE        BOI_X     EOI_X     SIG_INT   ELEV    AZIM    ORBIT  " +
			"GI   GJ_S GJ_E GK   LAT_S     LAT_E     MODE TYPE SSR_STATUS";
		heaherName += (hasFileName) ? "   FILENAME\n" :"\n";	
		linesStr.add(heaherName);
		for (Extent rsiRelExt : rsiExts)
		{
			Activity act = actDao.get((long)rsiRelExt.getActivityid());
			Rsiimagingrequest rsiimg = rsiimgreqDao.get((long)act.getArlentryid());
			// STATION
			String lineStr = String.format("%-10s", rsiimg.getStation());
			// DATE
			lineStr += TimeToString.AsLogDate(rsiRelExt.getStarttime()) + "    ";
			// BOI_X     EOI_X
			TimeRange oiTR = actManDao.getOperationalPeriod(rsiRelExt);
			lineStr += TimeToString.AsTime(oiTR.first) + "  " + TimeToString.AsTime(oiTR.second) + "  ";
			// SIG_INT
			String SIG_INT = "NA";
			for (Timestamp XTime : XCrossTimes)
			{
				if( oiTR.contains(XTime))
			    {
					SIG_INT = TimeToString.AsTime(XTime);
				    continue;
			    }
			}
			lineStr += String.format("%-10s", SIG_INT);
			// ELEV
			lineStr += String.format("%-8s", rsiimg.getElevation());
			// AZIM
			lineStr += String.format("%-8s", rsiimg.getAzimuth());
			// ORBIT
			String ORBIT = "";
			Rev rev = revDao.getMaxTimeRev(satId, branchId, oiTR.first);
			if (rev != null)
				ORBIT = rev.getRevno().toString();
			lineStr += String.format("%-7s", ORBIT);
			// GI   GJ_S GJ_E GK
			String GI = rsiimg.getTrackGi().equals("99999") ? "NA" : rsiimg.getTrackGi(); 
			String GJ_S = rsiimg.getGridJs().equals("99999") ? "NA" : rsiimg.getGridJs();
			String GJ_E = rsiimg.getGridJe().equals("99999") ? "NA" : rsiimg.getGridJe();
			String GK = rsiimg.getGridK().equals("99999") ? "NA" : rsiimg.getGridK();
			lineStr += String.format("%-5s", GI) + String.format("%-5s", GJ_S) + 
				String.format("%-5s", GJ_E) + String.format("%-5s", GK);
			// LAT_S     LAT_E
			String LAT_S = Double.compare(rsiimg.getStartlatitude(), 99.99) == 0 ? "NA      " : String.format("%+08.4f", rsiimg.getStartlatitude());
			String LAT_E = Double.compare(rsiimg.getStoplatitude(), 99.99) == 0 ? "NA      " : String.format("%+08.4f", rsiimg.getStoplatitude());
			lineStr += LAT_S + "  " + LAT_E + "  ";
			// MODE
			lineStr += rsiimg.getImagingmode() + "    ";
			// TYPE
			lineStr += rsiimg.getImagingtype() + "    ";
			// SSR_STATUS
			String SSR_STATUS = act.getActsubtype();
			if(SSR_STATUS.equalsIgnoreCase("REC"))
	        {
	            // does this record have any chained (next) activities
	            if(act.getNextactid() > 0)
	            {
	                // is the next activity a playback?
	            	Activity pbkAct = actDao.get((long)act.getNextactid());
	            	if (pbkAct != null)
	            	{
	            		if(pbkAct.getActsubtype().equalsIgnoreCase("PBK"))
		                {
		                    // is the playback activity scheduled
		                    if(pbkAct.getScheduled().compareTo(1) == 0)
		                    {
		                        // does the scheduled playback overlap the scheduled record
		                        Extent pbkExt = extentDao.get((long)pbkAct.getExtentid());
		                        if (pbkExt != null)
		                        {
		                        	TimeRange pbkTR = new TimeRange(pbkExt.getStarttime(), pbkExt.getEndtime());
		                        	TimeRange rsiTR = new TimeRange(rsiRelExt.getStarttime(), rsiRelExt.getEndtime());
		                        	if(pbkTR.overlaps(rsiTR))
			                            SSR_STATUS = "REC/TRANS";	
		                        }
		                    }
		                }
	            	}
	            }
	        }
			lineStr += String.format("%-13s", SSR_STATUS);
			//FILENAME
			String FILENAME = rsiRelExt.getFilename().compareTo(0) == 0 ? "" : rsiRelExt.getFilename().toString();
			FILENAME += rsiRelExt.getSecondfilename().compareTo(0) == 0 ? "" : "," +rsiRelExt.getSecondfilename().toString();
			FILENAME = String.format("%-10s", FILENAME);
			
			lineStr = hasFileName ? (lineStr + FILENAME + "\n") : (lineStr + "\n"); 
			linesStr.add(lineStr);
		}
		
		return linesStr;
	}
	
	//	STATION        DATE        BPBK_X    EPBK_X    ORBIT  MODE      FILENAME  
	//	IRKUTSK        2014/295    15:14:44  15:15:14  53300  PAN       1         
	//  For ROCSAT2
	@SuppressWarnings("unused")
	private List<String> DisplayPbkInfo (String siteName,List<Extent> pbkExtents, boolean hasFileName)
	{
		logger.info("Display Pbk Info. By site[" + siteName +"], hasFileName[" + hasFileName + "]");
		if (siteName.equalsIgnoreCase("ALL"))
		{
			siteOrbitMap = new HashMap<String, List<String>>();
			siteorbitRangeMap = new HashMap<String, TimeRange>();
		}
		
		List<Extent> pbkExts = new ArrayList<Extent>();
		switch (siteName)
		{
			case "ALL":
				pbkExts = pbkExtents;
				break;
//			case "NSPO":
//				return null;
			default:
				pbkExts = sitePbkExtsMap.get(siteName);
				break;
		}
		if (pbkExts == null)
			return null;
		if (pbkExts.size() == 0)
			return null;
		
		List<String> linesStr = new ArrayList<String>();
		String heaherName = "STATION        DATE        BPBK_X    EPBK_X    ORBIT  ";
		heaherName += (hasFileName) ? "MODE      FILENAME  \n" :"\n";	
		linesStr.add(heaherName);
		for (Extent pbk : pbkExts)
		{
			String sName="";
			Contact cont = contDao.get((long)pbk.getContactid());
			if (cont != null)
			{
				Site site = siteDao.get((long)cont.getSiteid());
				if(site != null)
					sName = (site.getName().equalsIgnoreCase("MOSCOW1")) ? "MOSCOW" : site.getName().replace("X_", "");
			}
			// STATION
			String lineStr = String.format("%-15s", sName);
			//DATE        BPBK_X    EPBK_X
			lineStr += TimeToString.AsLogDate(pbk.getStarttime()) + "    ";
			lineStr += TimeToString.AsTime(pbk.getStarttime()) + "  " + TimeToString.AsTime(pbk.getEndtime()) + "  ";
			// ORBIT
			String ORBIT = "";
			Rev rev = revDao.getMaxTimeRev(satId, branchId, pbk.getStarttime());
			if (rev != null)
				ORBIT = rev.getRevno().toString();
			lineStr += String.format("%-7s", ORBIT);
			// MODE
			String appendStr = String.format("%-10s", actDao.get((long)pbk.getActivityid()).getInfo());
			// FILENAME
			String FILENAME = pbk.getFilename().compareTo(0) == 0 ? "" : pbk.getFilename().toString();
			FILENAME += pbk.getSecondfilename().compareTo(0) == 0 ? "" : "," +pbk.getSecondfilename().toString();
			appendStr += String.format("%-10s", FILENAME);
			
			lineStr = hasFileName ? (lineStr + appendStr + "\n") : (lineStr + "\n"); 
			linesStr.add(lineStr);
			
			//if (!hasFileName) continue;
			if (siteName.equalsIgnoreCase("ALL") && hasFileName)
			{
				if (siteOrbitMap.get(sName) == null)
					siteOrbitMap.put(sName, new ArrayList<String>());
				List<String> OrbitList = siteOrbitMap.get(sName);
				if (!OrbitList.contains(ORBIT))
				{
					OrbitList.add(ORBIT);
					siteorbitRangeMap.put(sName + "," + ORBIT, new TimeRange(pbk.getStarttime(),pbk.getEndtime()));
				}
				else
				{
					Timestamp firstTime = siteorbitRangeMap.get(sName + "," + ORBIT).first;
					siteorbitRangeMap.remove(sName + "," + ORBIT);
					siteorbitRangeMap.put(sName + "," + ORBIT, new TimeRange(firstTime,pbk.getEndtime()));
				}
			}
		}
		
		return linesStr;
	}
	
	//STATION   DATE        BOI_X     EOI_X     SIG_INT   ELEV    AZIM    ORBIT  GI   GJ_S GJ_E GK   LAT_S     LAT_E     MODE TYPE SSR_STATUS   FILENAME
	//ANY       2014/295    14:07:20  14:07:53  NA        -30.1   269.6   53299  0    0    0    0    +19.2000  +17.5000  3    1    REC       31,32,    
	//For ROCSAT5
	private List<String> DisplayRsiInfo (String siteName,List<Extent> rsiExtents, List<Timestamp> XCrossTimes)
	{
		logger.info("Display Rsi Info. By site[" + siteName +"]");
		List<Extent> rsiExts = new ArrayList<Extent>();
		switch (siteName)
		{
			case "ALL":
				rsiExts = rsiExtents;
				break;
//				case "NSPO":
//					break;
			default:
				rsiExts = sitersiExtsMap.get(siteName);
				break;
		}
		if (rsiExts.size() == 0)
			return null;
		
		//sort
		Collections.sort(rsiExts,
		   		new Comparator<Extent>() 
	    		{
	    			public int compare(Extent Ext1, Extent Ext2) 
	    			{
	    				return Ext1.getStarttime().compareTo(Ext2.getStarttime());
	    			}
	    		}
		    );
		
		List<String> linesStr = new ArrayList<String>();
		String heaherName = "STATION   DATE        BOI_X     EOI_X     SIG_INT   ORBIT  MODE TYPE SSR_STATUS   FILENAME " +
			"STATION        DATE        BPBK_X    EPBK_X    ORBIT  MODE\n";	
		linesStr.add(heaherName);
		for (Extent rsiRelExt : rsiExts)
		{
			Activity act = actDao.get((long)rsiRelExt.getActivityid());
			Rsiimagingrequest rsiimg = rsiimgreqDao.get((long)act.getArlentryid());
			// STATION
			String lineStr = String.format("%-10s", rsiimg.getStation());
			// DATE
			lineStr += TimeToString.AsLogDate(rsiRelExt.getStarttime()) + "    ";
			// BOI_X     EOI_X
			TimeRange oiTR = actManDao.getOperationalPeriod(rsiRelExt);
			lineStr += TimeToString.AsTime(oiTR.first) + "  " + TimeToString.AsTime(oiTR.second) + "  ";
			// SIG_INT
			String SIG_INT = "NA";
			for (Timestamp XTime : XCrossTimes)
			{
				if( oiTR.contains(XTime))
			    {
					SIG_INT = TimeToString.AsTime(XTime);
				    continue;
			    }
			}
			lineStr += String.format("%-10s", SIG_INT);
			// ORBIT
			String ORBIT = "";
			Rev rev = revDao.getMaxTimeRev(satId, branchId, oiTR.first);
			if (rev != null)
				ORBIT = rev.getRevno().toString();
			lineStr += String.format("%-7s", ORBIT);
			// MODE
			lineStr += rsiimg.getImagingmode() + "    ";
			// TYPE
			if (rsiimg.getImagingtype() != null)
				lineStr += rsiimg.getImagingtype() + "    ";
			else
				lineStr += "NA   ";
			// SSR_STATUS
			String SSR_STATUS = act.getActsubtype();
			if(SSR_STATUS.equalsIgnoreCase("REC"))
	        {
	            // does this record have any chained (next) activities
	            if(act.getNextactid() > 0)
	            {
	                // is the next activity a playback?
	            	Activity pbkAct = actDao.get((long)act.getNextactid());
	            	if (pbkAct != null)
	            	{
	            		if(pbkAct.getActsubtype().equalsIgnoreCase("PBK"))
		                {
		                    // is the playback activity scheduled
		                    if(pbkAct.getScheduled().compareTo(1) == 0)
		                    {
		                        // does the scheduled playback overlap the scheduled record
		                        Extent pbkExt = extentDao.get((long)pbkAct.getExtentid());
		                        if (pbkExt != null)
		                        {
		                        	TimeRange pbkTR = new TimeRange(pbkExt.getStarttime(), pbkExt.getEndtime());
		                        	TimeRange rsiTR = new TimeRange(rsiRelExt.getStarttime(), rsiRelExt.getEndtime());
		                        	if(pbkTR.overlaps(rsiTR))
			                            SSR_STATUS = "REC/TRANS";	
		                        }
		                    }
		                }
	            	}
	            }
	        }
			lineStr += String.format("%-13s", SSR_STATUS);
			//FILENAME
			Integer fName1 = rsiRelExt.getFilename();
			String FILENAME1 = fName1.compareTo(0) == 0 ? "" : rsiRelExt.getFilename().toString();
			FILENAME1 = String.format("%-9s", FILENAME1);
			//STATION        DATE        BPBK_X    EPBK_X    ORBIT  MODE
			if (fileNamePbkStr.get(fName1) != null)
				linesStr.add(lineStr + FILENAME1 + fileNamePbkStr.get(fName1) + "\n");
			else
				linesStr.add(lineStr + FILENAME1 + "\n");
			if (rsiRelExt.getSecondfilename() > 0)
			{
				Integer fName2 = rsiRelExt.getSecondfilename();
				String FILENAME2 = String.format("%-9s", fName2.toString());
				if (fileNamePbkStr.get(fName2) != null)
					linesStr.add(lineStr + FILENAME2 + fileNamePbkStr.get(fName2) + "\n");
				else
					linesStr.add(lineStr + FILENAME2 + "\n");
			}
				
		}
		
		return linesStr;
	}
		
	//	STATION        DATE        BPBK_X    EPBK_X    ORBIT  MODE      FILENAME  
	//	IRKUTSK        2014/295    15:14:44  15:15:14  53300  PAN       1
	//  For ROCSAT5
	private List<String> DisplayPbkInfo (String siteName,List<Extent> pbkExtents)
	{
		logger.info("Display Pbk Info. By site[" + siteName +"]");
		
		fileNamePbkStr = new HashMap<Integer,String>();
		
		if (siteName.equalsIgnoreCase("ALL"))
		{
			siteOrbitMap = new HashMap<String, List<String>>();
			siteorbitRangeMap = new HashMap<String, TimeRange>();
		}
		
		List<Extent> pbkExts = new ArrayList<Extent>();
		switch (siteName)
		{
			case "ALL":
				pbkExts = pbkExtents;
				break;
//			case "NSPO":
//				return null;
			default:
				pbkExts = sitePbkExtsMap.get(siteName);
				break;
		}
		if (pbkExts == null)
			return null;
		if (pbkExts.size() == 0)
			return null;
		
		List<String> linesStr = new ArrayList<String>();
		String heaherName = "STATION        DATE        BPBK_X    EPBK_X    ORBIT  MODE      FILENAME  \n";	
		linesStr.add(heaherName);
		for (Extent pbk : pbkExts)
		{
			String sName="";
			Contact cont = contDao.get((long)pbk.getContactid());
			if (cont != null)
			{
				Site site = siteDao.get((long)cont.getSiteid());
				if(site != null)
					sName = (site.getName().equalsIgnoreCase("MOSCOW1")) ? "MOSCOW" : site.getName().replace("X_", "");
			}
			// STATION
			String lineStr = String.format("%-15s", sName);
			//DATE        BPBK_X    EPBK_X
			lineStr += TimeToString.AsLogDate(pbk.getStarttime()) + "    ";
			lineStr += TimeToString.AsTime(pbk.getStarttime()) + "  " + TimeToString.AsTime(pbk.getEndtime()) + "  ";
			// ORBIT
			String ORBIT = "";
			Rev rev = revDao.getMaxTimeRev(satId, branchId, pbk.getStarttime());
			if (rev != null)
				ORBIT = rev.getRevno().toString();
			lineStr += String.format("%-7s", ORBIT);
			// MODE
			lineStr += String.format("%-10s", actDao.get((long)pbk.getActivityid()).getInfo());
			// FILENAME
			String FILENAME = pbk.getFilename().compareTo(0) == 0 ? "" : pbk.getFilename().toString();
//			FILENAME += pbk.getSecondfilename().compareTo(0) == 0 ? "" : "," +pbk.getSecondfilename().toString();
			FILENAME = String.format("%-10s", FILENAME);
			
			fileNamePbkStr.put(pbk.getFilename(), lineStr);
			lineStr = lineStr + FILENAME + "\n";
			linesStr.add(lineStr);
			
			//if (!hasFileName) continue;
			if (siteName.equalsIgnoreCase("ALL"))
			{
				if (siteOrbitMap.get(sName) == null)
					siteOrbitMap.put(sName, new ArrayList<String>());
				List<String> OrbitList = siteOrbitMap.get(sName);
				if (!OrbitList.contains(ORBIT))
				{
					OrbitList.add(ORBIT);
					siteorbitRangeMap.put(sName + "," + ORBIT, new TimeRange(pbk.getStarttime(),pbk.getEndtime()));
				}
				else
				{
					Timestamp firstTime = siteorbitRangeMap.get(sName + "," + ORBIT).first;
					siteorbitRangeMap.remove(sName + "," + ORBIT);
					siteorbitRangeMap.put(sName + "," + ORBIT, new TimeRange(firstTime,pbk.getEndtime()));
				}
			}
		}
		
		return linesStr;
	}
	
	
	@SuppressWarnings("unused")
	private List<String> DisplayDisInfo (String sName)
	{
		SortedMap<Integer, TimeRange> orbitRangeMap = new TreeMap<Integer, TimeRange>();
		List<String> linesStr = new ArrayList<String>();
		linesStr.add("STATION: " + sName +"\n");
		linesStr.add("DATE          BOI_X      EOI_X    SIG_INT    ELEV    ORBIT    GRID_LINE    LAT_S    LAT_E    MODE\n");
		//sort
		for (String s : siteorbitRangeMap.keySet())
		{
			if (s.startsWith(sName))
			{
				TimeRange tr = RangeUtil.rangeAppendSec(siteorbitRangeMap.get(s), -60, 10);
				orbitRangeMap.put(Integer.valueOf(s.split(",")[1]), tr);
			}
		}
		for (int orbit : orbitRangeMap.keySet())
		{
			//DATE
			String lineStr = TimeToString.AsLongSlashDate(orbitRangeMap.get(orbit).first) + "   ";
			//BOI_X
			lineStr += TimeToString.AsTime(orbitRangeMap.get(orbit).first) + "   ";
			//EOI_X
			lineStr += TimeToString.AsTime(orbitRangeMap.get(orbit).second) + "      ";
			//SIG_INT    ELEV
			lineStr += "NA      NA      ";
			//ORBIT
			lineStr += String.format("%-12s", orbit);
			//GRID_LINE    LAT_S    LAT_E    MODE
			lineStr += "NA          NA      NA      3\n";
			
			linesStr.add(lineStr);	
		}
 
		return linesStr;
	}

	// Time                            Activity   Filename  Write/Read  Available    Usage    Usage  Available  Available
	// 2014/204/14:07:48         RSI REC:PAN+MS        1/2       44/10        539        -        -         31        571
	private String DisplaySSRInfo (Extent ext, Activity act, boolean isfirstISUAL)
	{
		String actName = "";
		double rsiAvail = 0;
		double isualAvail = 0;
		String rsiUsage = "-";
		String isualUsage = "-";
		String ddtUsage = "-";
		double totAvail = 0;
		
		String fileNames = (ext.getFilename() >0 ) ? ext.getFilename().toString() : "";
		fileNames += (ext.getSecondfilename() >0 ) ? "/" + ext.getSecondfilename().toString() : "";
		
		if (act.getArlentrytype().equalsIgnoreCase("DELETE"))
		{
			actName = act.getName();
			rsiUsage = ext.getRecorderdelta().toString();
			//rsiAvail = String.valueOf(actManDao.getAvailCapacity(ext) - ext.getRecorderdelta());
			rsiAvail = actManDao.getAvailCapacity(ext,"RSI") - ext.getRecorderdelta();
			isualAvail = actManDao.getAvailCapacity(ext,"ISUAL");
			totAvail = actManDao.getTotalAvail(ext) - ext.getRecorderdelta();
		}	
		else if (act.getArlentrytype().equalsIgnoreCase("ISUAL"))
		{
			actName = "ISUAL PBK";
			//rsiAvail = String.valueOf(actManDao.getAvailCapacity(ext));
			rsiAvail = actManDao.getAvailCapacity(ext,"RSI");
			isualUsage = String.valueOf(ext.getRecorderdelta());
			totAvail = isfirstISUAL ? actManDao.getTotalAvail(ext) - ext.getRecorderdelta() : actManDao.getTotalAvail(ext);
			
			int totalCapinMbits = 0; 
		    Recorderp recp = sessManDao.getRecorderp(ext.getRecorderid(), act.getSessionid());
		    if(recp != null)
		    	totalCapinMbits = recp.getIsualcapacity();
		    int Sectorsize = recDao.get((long)ext.getRecorderid()).getSectorsize();
		    if (Sectorsize != 0)
		    	isualAvail = (totalCapinMbits / Sectorsize) - ext.getRecorderdelta();
		}
		else if (act.getActsubtype().equalsIgnoreCase("DDT"))
		{
			actName = act.getName() + ":" + act.getInfo();
			//rsiAvail = String.valueOf(actManDao.getAvailCapacity(ext));
			rsiAvail = actManDao.getAvailCapacity(ext,"RSI");
			isualAvail = actManDao.getAvailCapacity(ext,"ISUAL");
			totAvail = actManDao.getTotalAvail(ext) - (ext.getRecorderdelta() + ext.getSecondrecorderdelta());
			ddtUsage = String.valueOf(ext.getRecorderdelta() + ext.getSecondrecorderdelta());
		}
		else if (act.getActsubtype().equalsIgnoreCase("REC") || act.getActsubtype().equalsIgnoreCase("PBK"))
		{
			actName = act.getName().replaceAll("_", " ");
			rsiUsage = (ext.getSecondrecorderdelta() > 0) ? 
				ext.getRecorderdelta().toString() + "/" + ext.getSecondrecorderdelta().toString() : 
				ext.getRecorderdelta().toString();
			//rsiAvail = String.valueOf(actManDao.getAvailCapacity(ext) - ext.getRecorderdelta() - ext.getSecondrecorderdelta());
			rsiAvail = actManDao.getAvailCapacity(ext,"RSI") - ext.getRecorderdelta() - ext.getSecondrecorderdelta();
			isualAvail = actManDao.getAvailCapacity(ext,"ISUAL");
			totAvail = actManDao.getTotalAvail(ext) - ext.getRecorderdelta() - ext.getSecondrecorderdelta();
		}

		

		String lineStr = TimeToString.AsLogTime(ext.getStarttime()) + "   ";
		lineStr += String.format("%20s", actName) + "   ";
		lineStr += String.format("%8s", fileNames) + "   ";
		lineStr += String.format("%9s", rsiUsage) + "   ";
		lineStr += String.format("%8.0f", rsiAvail) + "   ";
		lineStr += String.format("%6s", ddtUsage) + "   ";
		lineStr += String.format("%6s", isualUsage) + "   ";
		lineStr += String.format("%8.0f", isualAvail) + "   ";
		lineStr += String.format("%8.0f", totAvail) + "   \n";
		
		return lineStr;
	}
	
	private void WriteRSILog ()
	{
		/// open the log file for output
		logFileName = TimeToString.AsLongDate(rptTimeRange.first) + "_" + section +".html";
		String logFilePath = fileDir + System.getProperty("asrReportLog") + logFileName;
		
		File file = new File(logFilePath);
		if(!file.exists())
		{
		    try {
		        file.createNewFile();
		    } catch (Exception e) {
		        logger.error("ReportCreator Error: Log file can not be create: " + logFileName);
		    }
		}
	    try 
	    {
	    	logFOS = new FileOutputStream(file);
	    	logOSW = new OutputStreamWriter(logFOS);
	    	
	    } catch (Exception e) {
	        logger.error("ReportCreator Error: Log file can not be opened: " + logFileName);
	    }
	    
	    // get the current time
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		try
		{
			logOSW.write("<html>\n<head>\n</head>\n<body>\n<pre>\n");
			logOSW.write("***  Running ASR Report Creator on " +
					TimeToString.AsRptTime(currentTime) + " (" + TimeToString.AsShortDate(currentTime) + ")\n\n");

			
			logOSW.write("***  Log File Name:		" + logFileName + "\n");
			logOSW.write("***  Satellite Name:		" + satName + "\n");
			logOSW.write("***  Report Time Range:	" + rptTimeRange.first.toString() + " - " + rptTimeRange.second.toString()+ "\n");
			logOSW.write("***  Section:				" + section + "\n");
			if (branchId != 0)
			{
				logOSW.write("***  Branch Id:			" + branchId + "\n");
			}
			logOSW.write("\n\n");
		}
		catch (Exception e){
			logger.error(e.getMessage());
		}
	}
	
	private void WriteAIPLog ()
	{
		/// open the log file for output
		logFileName = "AIP_" + TimeToString.AsLongDate(rptTimeRange.first) + ".html";
		String logFilePath = fileDir + System.getProperty("aipReportLog") + logFileName;
		
		File file = new File(logFilePath);
		if(!file.exists())
		{
		    try {
		        file.createNewFile();
		    } catch (Exception e) {
		        logger.error("ReportCreator Error: Log file can not be create: " + logFileName);
		    }
		}
	    try 
	    {
	    	logFOS = new FileOutputStream(file);
	    	logOSW = new OutputStreamWriter(logFOS);
	    	
	    } catch (Exception e) {
	        logger.error("ReportCreator Error: Log file can not be opened: " + logFileName);
	    }
	    
	    // get the current time
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		try
		{
			logOSW.write("<html>\n<head>\n</head>\n<body>\n<pre>\n");
			logOSW.write("***  Running AIP Report Creator on " +
					TimeToString.AsRptTime(currentTime) + " (" + TimeToString.AsShortDate(currentTime) + ")\n\n");
			logOSW.write("***  Log File Name:		" + logFileName + "\n");
			logOSW.write("***  Satellite Name:		" + satName + "\n");
			logOSW.write("***  Report Time Range:	" + rptTimeRange.first.toString() + " - " + rptTimeRange.second.toString()+ "\n");
			
			logOSW.write("\n\n");
		}
		catch (Exception e){
			logger.error(e.getMessage());
		}
	}
}
