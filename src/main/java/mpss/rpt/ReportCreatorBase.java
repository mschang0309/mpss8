package mpss.rpt;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mpss.configFactory;
import mpss.common.dao.ActivityDao;
import mpss.common.dao.ActivityManDao;
import mpss.common.dao.AipprocrequestDao;
import mpss.common.dao.ArlDao;
import mpss.common.dao.ContactDao;
import mpss.common.dao.ExtentDao;
import mpss.common.dao.RecorderDao;
import mpss.common.dao.RevDao;
import mpss.common.dao.RsiimagingrequestDao;
import mpss.common.dao.SatelliteDao;
import mpss.common.dao.SessionManDao;
import mpss.common.dao.SessionSectionDao;
import mpss.common.dao.SiteDao;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Satellite;
import mpss.util.timeformat.TimeRange;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportCreatorBase implements ReportCreator{
	
    @Autowired
    ActivityDao actDao;
    @Autowired
    ExtentDao extentDao;
    @Autowired
    ContactDao contactDao;
    @Autowired
    ActivityManDao actManDao;
    @Autowired
    RsiimagingrequestDao rsiimgreqDao;
    @Autowired
    SessionSectionDao SesSecDao;
    @Autowired
    RevDao revDao;
    @Autowired
    SatelliteDao satDao;
    @Autowired
    ContactDao contDao;
    @Autowired
    SiteDao siteDao;
    @Autowired
    SessionManDao sessManDao;
    @Autowired
    RecorderDao recDao;
    @Autowired
    AipprocrequestDao aipreqDao;
    @Autowired
    ArlDao arlDao;
    
    public String returnMsg;
    protected TimeRange rptTimeRange;
	protected boolean selRSI;
	protected boolean selSSR;
	protected boolean isBranch;
	protected int branchId;
	protected String logFileName;
	protected String rsiFileName;
	protected String ssrFileName;

	protected String fileDir;
	protected String satName;
	protected String satShortName;
	protected int satId;
	protected String section;
	protected Map<String, List<Extent>> sitersiExtsMap;
	protected Map<String, List<Extent>> sitePbkExtsMap;
	protected Map<String, List<String>> siteOrbitMap;
	protected Map<String, TimeRange> siteorbitRangeMap;//<"site"+","+"orbit" ,TimeRange>
	protected Map<Integer,String> fileNamePbkStr;
	protected FileOutputStream logFOS;
	protected OutputStreamWriter logOSW;
	
	protected Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public String getLogFileName()
	{
	  return logFileName;
	}
	
	public String getReturnMsg(){
		return this.returnMsg;		
	}
	
	public void setSection (int sectionId){
		this.section = SesSecDao.get((long) sectionId).getSectionname();
	}
	
	public void setBranch (boolean isBranch, int branchId){
		this.isBranch = isBranch;
		this.branchId = this.isBranch ? branchId : 0;
	}

	public void setSelRSI (boolean selRSI){
		this.selRSI = selRSI;
	}
	
	public void setSelSSR (boolean selSSR){
		this.selSSR = selSSR;
	}
	
	public void setRSIFileName (String rsiFileName){
		this.rsiFileName = rsiFileName;
	}
	
	public void setSSRFileName (String ssrFileName){
		this.ssrFileName = ssrFileName;
	}

	public void setFileDir (String fileDir){
		this.fileDir = fileDir;
	}
	
	public void setRptTimeRange (Date startTime, Date endTime){
		this.rptTimeRange = new TimeRange(new Timestamp(startTime.getTime()), new Timestamp(endTime.getTime()));
	}
	
	public void setSatName (String satName){
		this.satName = satName;
		Satellite Satellite = satDao.getByName(satName);
		this.satId = Satellite.getId().intValue();
		this.satShortName = Satellite.getShortname();
	}

	@Override
	public boolean GenReport() {		
		return false;
	}

	@Override
	public boolean GenAIP(int sessionid) {		
		return false;
	}	
	
}
