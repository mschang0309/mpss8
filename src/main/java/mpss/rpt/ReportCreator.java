package mpss.rpt;

import java.util.Date;

import org.springframework.stereotype.Service;

@Service
public interface ReportCreator {    
	
	public String getLogFileName();	
	public String getReturnMsg();
	public void setSection (int sectionId);	
	public void setBranch (boolean isBranch, int branchId);
	public void setSelRSI (boolean selRSI);	
	public void setSelSSR (boolean selSSR);	
	public void setRSIFileName (String rsiFileName);	
	public void setSSRFileName (String ssrFileName);
	public void setFileDir (String fileDir);	
	public void setRptTimeRange (Date startTime, Date endTime);	
	public void setSatName (String satName);	
	public boolean GenReport ();	
	public boolean GenAIP (int sessionid);		
}
