package mpss.common.jpa;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement
public class PsSatelliteParameterSet implements Serializable {

	private static final long serialVersionUID = 1L;
  // timing parameter set
    private Satellitep satellitep;
  
  // child recorderps
	@XmlElement(required = true)	
	private List<Recorderp> recorderps;
	
  // child duty cycles
	@XmlElement(required = true)	
	private List<Dutycycle> dutycycles;
	
	public PsSatelliteParameterSet() {
	}

	public PsSatelliteParameterSet(Satellitep satellitep) {
		this.satellitep = satellitep;
	}

  // Satellitep
	public Satellitep getSatellitep() {
		return this.satellitep;
	}

	public void setSatellitep(Satellitep satellitep) {
		this.satellitep = satellitep;
	}

  // Recorderp
  public List<Recorderp> getRecorderps() {
  	    return this.recorderps;
  }

  public void setRecorderps(List<Recorderp> recorderp) {
  	    this.recorderps = recorderp;
  }

  // Dutycycle
  public List<Dutycycle> getDutycycles() {
  	    return this.dutycycles;
  }

  public void setDutycycles(List<Dutycycle> dutycycles) {
  	    this.dutycycles = dutycycles;
  }
}