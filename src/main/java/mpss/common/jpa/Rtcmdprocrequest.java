package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the rtcmdprocrequests database table.
 * 
 */
@Entity
@XmlRootElement
@Table(name="rtcmdprocrequests")
public class Rtcmdprocrequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RTCMDPROCREQUESTS_ID_GENERATOR", sequenceName="RTCMDPROCREQUESTS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RTCMDPROCREQUESTS_ID_GENERATOR")
	private Long id;

	private Integer arlid;

	private String comments;

	private Integer day;

	private Integer duration;

	private String requestor;

	private String rtcmdproc;

	private Integer starttime;

	private Integer year;

	public Rtcmdprocrequest() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getArlid() {
		return this.arlid;
	}

	public void setArlid(Integer arlid) {
		this.arlid = arlid;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getDay() {
		return this.day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getRequestor() {
		return this.requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getRtcmdproc() {
		return this.rtcmdproc;
	}

	public void setRtcmdproc(String rtcmdproc) {
		this.rtcmdproc = rtcmdproc;
	}

	public Integer getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Integer starttime) {
		this.starttime = starttime;
	}

	public Integer getYear() {
		return this.year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

}