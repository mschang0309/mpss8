package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the datapath database table.
 * 
 */
@Entity
public class Datapath implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DATAPATH_ID_GENERATOR", sequenceName="DATAPATH_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DATAPATH_ID_GENERATOR")
	private Long id;

	private Integer ccid;

	private String name;

	public Datapath() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCcid() {
		return this.ccid;
	}

	public void setCcid(Integer ccid) {
		this.ccid = ccid;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}