package mpss.common.jpa;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement
public class PsOrbitEvents implements Serializable {

	private static final long serialVersionUID = 1L;
    private Rev rev;
  // child eclipses
	@XmlElement(required = true)	
	private List<Eclipse> eclipses;
	
  // child contacts
	@XmlElement(required = true)	
	private List<Contact> contacts;
	
	public PsOrbitEvents() {
	}

	public PsOrbitEvents(Rev rev) {
		this.rev = rev;
	}

  // Rev
	public Rev getRev() {
		return this.rev;
	}

	public void setRev(Rev rev) {
		this.rev = rev;
	}

  // Eclipses
  public List<Eclipse> getEclipses() {
  	    return this.eclipses;
  }

  public void setEclipses(List<Eclipse> eclipses) {
  	    this.eclipses = eclipses;
  }

  // Contact
  public List<Contact> getContacts() {
  	    return this.contacts;
  }

  public void setContacts(List<Contact> contacts) {
  	    this.contacts = contacts;
  }
}