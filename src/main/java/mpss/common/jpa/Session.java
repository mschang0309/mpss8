package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the sessions database table.
 * 
 */
@Entity
@Table(name="sessions")
@XmlRootElement
public class Session implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SESSIONS_ID_GENERATOR", sequenceName="SESSIONS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SESSIONS_ID_GENERATOR")
	private Long id;

	private Integer acqsumloaded;

	private Integer coldscheduled;

	private String description;

	private Timestamp endtime;

	private Integer isbranch;

	private Timestamp lastsave;

	private String name;

	private Integer needsvalidation;

	private Integer nominalapplied;

	private Integer psid;

	private Timestamp starttime;

  // handcrafted entities
	@OneToMany(mappedBy="session", cascade={CascadeType.ALL})
	private Set<Sessionsatarlxref> satArlXrefs;
	@OneToMany(mappedBy="session", cascade={CascadeType.ALL})
	private Set<Sessionsatxref> satXrefs;

	@OneToMany(mappedBy="session", cascade={CascadeType.ALL})
	private Set<Sessionsitexref> siteXrefs;
	
	public Session() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAcqsumloaded() {
		return this.acqsumloaded;
	}

	public void setAcqsumloaded(Integer acqsumloaded) {
		this.acqsumloaded = acqsumloaded;
	}

	public Integer getColdscheduled() {
		return this.coldscheduled;
	}

	public void setColdscheduled(Integer coldscheduled) {
		this.coldscheduled = coldscheduled;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEndtime() {
		return this.endtime;
	}

	public void setEndtime(Timestamp endtime) {
		this.endtime = endtime;
	}

	public Integer getIsbranch() {
		return this.isbranch;
	}

	public void setIsbranch(Integer isbranch) {
		this.isbranch = isbranch;
	}

	public Timestamp getLastsave() {
		return this.lastsave;
	}

	public void setLastsave(Timestamp lastsave) {
		this.lastsave = lastsave;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNeedsvalidation() {
		return this.needsvalidation;
	}

	public void setNeedsvalidation(Integer needsvalidation) {
		this.needsvalidation = needsvalidation;
	}

	public Integer getNominalapplied() {
		return this.nominalapplied;
	}

	public void setNominalapplied(Integer nominalapplied) {
		this.nominalapplied = nominalapplied;
	}

	public Integer getPsid() {
		return this.psid;
	}

	public void setPsid(Integer psid) {
		this.psid = psid;
	}

	public Timestamp getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

  // handcrafted entities
  // Session satellite arl xref
  public Set<Sessionsatarlxref> getSatArlXrefs() {
  	    return this.satArlXrefs;
  }

  public void setSatArlXrefs(Set<Sessionsatarlxref> satArlXrefs) {
  	    this.satArlXrefs = satArlXrefs;
  }
  // Session satellite xref
  public Set<Sessionsatxref> getSatXrefs() {
	    return this.satXrefs;
  }

  public void setSatXrefs(Set<Sessionsatxref> satXrefs) {
	    this.satXrefs = satXrefs;
  }
  // Session site xref
  public Set<Sessionsitexref> getSiteXrefs() {
	    return this.siteXrefs;
  }

  public void setSiteXrefs(Set<Sessionsitexref> siteXrefs) {
	    this.siteXrefs = siteXrefs;
  }
}