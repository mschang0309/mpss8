package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the imagingmode database table.
 * 
 */
@Entity
@XmlRootElement
public class Imagingmode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="IMAGINGMODE_ID_GENERATOR", sequenceName="IMAGINGMODE_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IMAGINGMODE_ID_GENERATOR")
	private Long id;

	private float compressionratio;

	private String instrument;

	private Integer modeid;

	private String modename;

	private String modeshort;

	public Imagingmode() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public float getCompressionratio() {
		return this.compressionratio;
	}

	public void setCompressionratio(float compressionratio) {
		this.compressionratio = compressionratio;
	}

	public String getInstrument() {
		return this.instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public Integer getModeid() {
		return this.modeid;
	}

	public void setModeid(Integer modeid) {
		this.modeid = modeid;
	}

	public String getModename() {
		return this.modename;
	}

	public void setModename(String modename) {
		this.modename = modename;
	}

	public String getModeshort() {
		return this.modeshort;
	}

	public void setModeshort(String modeshort) {
		this.modeshort = modeshort;
	}

}