package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.sql.Timestamp;


/**
 * The persistent class for the externalactivityrequestinfo database table.
 * 
 */
@Entity
@XmlRootElement
public class Externalactivityrequestinfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EXTERNALACTIVITYREQUESTINFO_ID_GENERATOR", sequenceName="EXTERNALACTIVITYREQUESTINFO_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTERNALACTIVITYREQUESTINFO_ID_GENERATOR")
	private Long id;

	private Integer arlid;

	private Integer branchid;

	private Timestamp endtime;

	private String instrument;

	private String satellite;

	private Timestamp starttime;

	public Externalactivityrequestinfo() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getArlid() {
		return this.arlid;
	}

	public void setArlid(Integer arlid) {
		this.arlid = arlid;
	}

	public Integer getBranchid() {
		return this.branchid;
	}

	public void setBranchid(Integer branchid) {
		this.branchid = branchid;
	}

	public Timestamp getEndtime() {
		return this.endtime;
	}

	public void setEndtime(Timestamp endtime) {
		this.endtime = endtime;
	}

	public String getInstrument() {
		return this.instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}

	public String getSatellite() {
		return this.satellite;
	}

	public void setSatellite(String satellite) {
		this.satellite = satellite;
	}

	public Timestamp getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

}