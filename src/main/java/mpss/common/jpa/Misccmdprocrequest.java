package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the misccmdprocrequests database table.
 * 
 */
@Entity
@XmlRootElement
@Table(name="misccmdprocrequests")
public class Misccmdprocrequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MISCCMDPROCREQUESTS_ID_GENERATOR", sequenceName="MISCCMDPROCREQUESTS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MISCCMDPROCREQUESTS_ID_GENERATOR")
	private Long id;

	private Integer arlid;

	private String comments;

	private Integer contactid;

	private Integer day;

	private Integer duration;

	private Integer _offset;

	private Integer offsetnegative;

	private Integer revid;

	private Integer time;

	private Integer trig;

	private String userproc;

	private Integer year;

	public Misccmdprocrequest() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getArlid() {
		return this.arlid;
	}

	public void setArlid(Integer arlid) {
		this.arlid = arlid;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getContactid() {
		return this.contactid;
	}

	public void setContactid(Integer contactid) {
		this.contactid = contactid;
	}

	public Integer getDay() {
		return this.day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getOffset() {
		return this._offset;
	}

	public void setOffset(Integer offset) {
		this._offset = offset;
	}

	public Integer getOffsetnegative() {
		return this.offsetnegative;
	}

	public void setOffsetnegative(Integer offsetnegative) {
		this.offsetnegative = offsetnegative;
	}

	public Integer getRevid() {
		return this.revid;
	}

	public void setRevid(Integer revid) {
		this.revid = revid;
	}

	public Integer getTime() {
		return this.time;
	}

	public void setTime(Integer time) {
		this.time = time;
	}

	public Integer getTrig() {
		return this.trig;
	}

	public void setTrig(Integer trig) {
		this.trig = trig;
	}

	public String getUserproc() {
		return this.userproc;
	}

	public void setUserproc(String userproc) {
		this.userproc = userproc;
	}

	public Integer getYear() {
		return this.year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

}