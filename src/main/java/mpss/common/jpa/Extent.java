package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.sql.Timestamp;


/**
 * The persistent class for the extent database table.
 * 
 */
@Entity
@XmlRootElement
public class Extent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EXTENT_ID_GENERATOR", sequenceName="EXTENT_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTENT_ID_GENERATOR")
	private Long id;

	private Integer activityid;

	private Integer antennaid;

	private Integer branchid;

	private Integer contactid;

	private Timestamp endtime;

	private Integer filename;

	private Integer hardconstraintviolated;

	private Integer recorderdelta;

	private Integer recorderid;

	private Integer secondfilename;

	private Integer secondrecorderdelta;

	private Integer softconstraintviolated;

	private Timestamp starttime;

	private Integer transmitterid;

	private Integer userlocked;

	private Integer usermodified;

	private Integer isaip;

	public Extent() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getActivityid() {
		return this.activityid;
	}

	public void setActivityid(Integer activityid) {
		this.activityid = activityid;
	}

	public Integer getAntennaid() {
		return this.antennaid;
	}

	public void setAntennaid(Integer antennaid) {
		this.antennaid = antennaid;
	}

	public Integer getBranchid() {
		return this.branchid;
	}

	public void setBranchid(Integer branchid) {
		this.branchid = branchid;
	}

	public Integer getContactid() {
		return this.contactid;
	}

	public void setContactid(Integer contactid) {
		this.contactid = contactid;
	}

	public Timestamp getEndtime() {
		return this.endtime;
	}

	public void setEndtime(Timestamp endtime) {
		this.endtime = endtime;
	}

	public Integer getFilename() {
		return this.filename;
	}

	public void setFilename(Integer filename) {
		this.filename = filename;
	}

	public Integer getHardconstraintviolated() {
		return this.hardconstraintviolated;
	}

	public void setHardconstraintviolated(Integer hardconstraintviolated) {
		this.hardconstraintviolated = hardconstraintviolated;
	}

	public Integer getRecorderdelta() {
		return this.recorderdelta;
	}

	public void setRecorderdelta(Integer recorderdelta) {
		this.recorderdelta = recorderdelta;
	}

	public Integer getRecorderid() {
		return this.recorderid;
	}

	public void setRecorderid(Integer recorderid) {
		this.recorderid = recorderid;
	}

	public Integer getSecondfilename() {
		return this.secondfilename;
	}

	public void setSecondfilename(Integer secondfilename) {
		this.secondfilename = secondfilename;
	}

	public Integer getSecondrecorderdelta() {
		return this.secondrecorderdelta;
	}

	public void setSecondrecorderdelta(Integer secondrecorderdelta) {
		this.secondrecorderdelta = secondrecorderdelta;
	}

	public Integer getSoftconstraintviolated() {
		return this.softconstraintviolated;
	}

	public void setSoftconstraintviolated(Integer softconstraintviolated) {
		this.softconstraintviolated = softconstraintviolated;
	}

	public Timestamp getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

	public Integer getTransmitterid() {
		return this.transmitterid;
	}

	public void setTransmitterid(Integer transmitterid) {
		this.transmitterid = transmitterid;
	}

	public Integer getUserlocked() {
		return this.userlocked;
	}

	public void setUserlocked(Integer userlocked) {
		this.userlocked = userlocked;
	}

	public Integer getUsermodified() {
		return this.usermodified;
	}

	public void setUsermodified(Integer usermodified) {
		this.usermodified = usermodified;
	}

	public Integer getIsaip() {
		return this.isaip;
	}

	public void setIsaip(Integer isaip) {
		this.isaip = isaip;
	}

}