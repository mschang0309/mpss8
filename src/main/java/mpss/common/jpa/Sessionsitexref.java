package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * The persistent class for the sessionsitexref database table.
 * 
 */
@Entity
@XmlRootElement
public class Sessionsitexref implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SESSIONSITEXREF_ID_GENERATOR", sequenceName="SESSIONSITEXREF_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SESSIONSITEXREF_ID_GENERATOR")
	private Integer id;

	private Integer sessionid;

	private Integer siteid;

  // handcrafted entities
	@ManyToOne
	@JoinColumn(name = "sessionid", insertable=false, updatable=false)
	private Session session;
	
	public Sessionsitexref() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSessionid() {
		return this.sessionid;
	}

	public void setSessionid(Integer sessionid) {
		this.sessionid = sessionid;
	}

	public Integer getSiteid() {
		return this.siteid;
	}

	public void setSiteid(Integer siteid) {
		this.siteid = siteid;
	}

  // handcrafted 
  @XmlInverseReference(mappedBy = "siteXrefs")
  public Session getSession() {
  	return this.session;
  }


  public void setSession(Session session) {
  	this.session = session;
  }
}