package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the uplinkrequests database table.
 * 
 */
@Entity
@XmlRootElement
@Table(name="uplinkrequests")
public class Uplinkrequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="UPLINKREQUESTS_ID_GENERATOR", sequenceName="UPLINKREQUESTS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="UPLINKREQUESTS_ID_GENERATOR")
	private Long id;

	private Integer arlid;

	private String comments;

	private Integer day;

	private Integer duration;

	private String loadsrcproc;

	private String loadtype;

	private Integer starttime;

	private Integer year;

	public Uplinkrequest() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getArlid() {
		return this.arlid;
	}

	public void setArlid(Integer arlid) {
		this.arlid = arlid;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getDay() {
		return this.day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getLoadsrcproc() {
		return this.loadsrcproc;
	}

	public void setLoadsrcproc(String loadsrcproc) {
		this.loadsrcproc = loadsrcproc;
	}

	public String getLoadtype() {
		return this.loadtype;
	}

	public void setLoadtype(String loadtype) {
		this.loadtype = loadtype;
	}

	public Integer getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Integer starttime) {
		this.starttime = starttime;
	}

	public Integer getYear() {
		return this.year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

}