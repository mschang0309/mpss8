package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the userdata database table.
 * 
 */
@Entity
@XmlRootElement
public class Userdata implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="USERDATA_ID_GENERATOR", sequenceName="USERDATA_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USERDATA_ID_GENERATOR")
	private Long id;

	private String username;

	private String userpw;

	private String prevtype;


	public Userdata() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserpw() {
		return this.userpw;
	}

	public void setUserpw(String userpw) {
		this.userpw = userpw;
	}

	public String getPrevtype() {
		return this.prevtype;
	}

	public void setPrevtype(String prevtype) {
		this.prevtype = prevtype;
	}



}