package mpss.common.jpa;

//import java.sql.Timestamp;
import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the rsiimagingrequests database table.
 * 
 */
@Entity
@XmlRootElement
@Table(name="rsiimagingrequests")
public class Rsiimagingrequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RSIIMAGINGREQUESTS_ID_GENERATOR", sequenceName="RSIIMAGINGREQUESTS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RSIIMAGINGREQUESTS_ID_GENERATOR")
	private Long id;

	private Integer arlid;

	private String azimuth;
	//private float azimuth;

	private String comments;

	private Integer day;

	private Integer duration;

	private String elevation;
	//private float elevation;

	@Column(name="grid_je")
	private String gridJe;
	//private Integer gridJe;

	@Column(name="grid_js")
	private String gridJs;
	//private Integer gridJs;

	@Column(name="grid_k")
	private String gridK;
	//private Integer gridK;

	private Integer imagingmode;

	private Integer imagingtype;

	private String mscompressionratio;
	
	private float mslinecompressionratio;

	private Integer orbit;

	private String pancompressionratio;
	
	private float panlinecompressionratio;

	private String ssrstatus;

	private float startlatitude;

	private Integer starttime;
	//private Timestamp starttime;

	private String station;

	private float stoplatitude;

	@Column(name="track_gi")
	private String trackGi;
	//private Integer trackGi;

	private String videomb1gain;

	private String videomb2gain;

	private String videomb3gain;

	private String videomb4gain;

	private String videopangain;

	private Integer year;

	public Rsiimagingrequest() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getArlid() {
		return this.arlid;
	}

	public void setArlid(Integer arlid) {
		this.arlid = arlid;
	}

	public String getAzimuth() {
		return this.azimuth;
	}
  
	public void setAzimuth(String azimuth) {
		this.azimuth = azimuth;
	}

//	public float getAzimuth() {
//		return this.azimuth;
//	}
//
//	public void setAzimuth(float azimuth) {
//		this.azimuth = azimuth;
//	}
	
	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getDay() {
		return this.day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getElevation() {
		return this.elevation;
	}
  
	public void setElevation(String elevation) {
		this.elevation = elevation;
	}

//	public float getElevation() {
//		return this.elevation;
//	}
//
//	public void setElevation(float elevation) {
//		this.elevation = elevation;
//	}

	public String getGridJe() {
		return this.gridJe;
	}
  
	public void setGridJe(String gridJe) {
		this.gridJe = gridJe;
	}

//	public Integer getGridJe() {
//		return this.gridJe;
//	}
//
//	public void setGridJe(Integer gridJe) {
//		this.gridJe = gridJe;
//	}
	
	public String getGridJs() {
		return this.gridJs;
	}
  
	public void setGridJs(String gridJs) {
		this.gridJs = gridJs;
	}

//	public Integer getGridJs() {
//		return this.gridJs;
//	}
//
//	public void setGridJs(Integer gridJs) {
//		this.gridJs = gridJs;
//	}

	public String getGridK() {
		return this.gridK;
	}
  
	public void setGridK(String gridK) {
		this.gridK = gridK;
	}

//	public Integer getGridK() {
//		return this.gridK;
//	}
//
//	public void setGridK(Integer gridK) {
//		this.gridK = gridK;
//	}

	public Integer getImagingmode() {
		return this.imagingmode;
	}

	public void setImagingmode(Integer imagingmode) {
		this.imagingmode = imagingmode;
	}

	public Integer getImagingtype() {
		return this.imagingtype;
	}

	public void setImagingtype(Integer imagingtype) {
		this.imagingtype = imagingtype;
	}

	public String getMscompressionratio() {
		return this.mscompressionratio;
	}

	public void setMscompressionratio(String mscompressionratio) {
		this.mscompressionratio = mscompressionratio;
	}

	public float getMslinecompressionratio() {
		return this.mslinecompressionratio;
	}

	public void setMslinecompressionratio(float mslinecompressionratio) {
		this.mslinecompressionratio = mslinecompressionratio;
	}
	
	public Integer getOrbit() {
		return this.orbit;
	}

	public void setOrbit(Integer orbit) {
		this.orbit = orbit;
	}

	public String getPancompressionratio() {
		return this.pancompressionratio;
	}

	public void setPancompressionratio(String pancompressionratio) {
		this.pancompressionratio = pancompressionratio;
	}

	public float getPanlinecompressionratio() {
		return this.panlinecompressionratio;
	}

	public void setPanlinecompressionratio(float panlinecompressionratio) {
		this.panlinecompressionratio = panlinecompressionratio;
	}
	
	public String getSsrstatus() {
		return this.ssrstatus;
	}

	public void setSsrstatus(String ssrstatus) {
		this.ssrstatus = ssrstatus;
	}

	public float getStartlatitude() {
		return this.startlatitude;
	}

	public void setStartlatitude(float startlatitude) {
		this.startlatitude = startlatitude;
	}

	public Integer getStarttime() {
		return this.starttime;
	}
  
	public void setStarttime(Integer starttime) {
		this.starttime = starttime;
	}

	//public Timestamp getStarttime() {
	//	return this.starttime;
	//}
  //
	//public void setStarttime(Timestamp starttime) {
	//	this.starttime = starttime;
	//}
	
	public String getStation() {
		return this.station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public float getStoplatitude() {
		return this.stoplatitude;
	}

	public void setStoplatitude(float stoplatitude) {
		this.stoplatitude = stoplatitude;
	}

	public String getTrackGi() {
		return this.trackGi;
	}
  
	public void setTrackGi(String trackGi) {
		this.trackGi = trackGi;
	}

//	public Integer getTrackGi() {
//		return this.trackGi;
//	}
//
//	public void setTrackGi(Integer trackGi) {
//		this.trackGi = trackGi;
//	}
	
	public String getVideomb1gain() {
		return this.videomb1gain;
	}

	public void setVideomb1gain(String videomb1gain) {
		this.videomb1gain = videomb1gain;
	}

	public String getVideomb2gain() {
		return this.videomb2gain;
	}

	public void setVideomb2gain(String videomb2gain) {
		this.videomb2gain = videomb2gain;
	}

	public String getVideomb3gain() {
		return this.videomb3gain;
	}

	public void setVideomb3gain(String videomb3gain) {
		this.videomb3gain = videomb3gain;
	}

	public String getVideomb4gain() {
		return this.videomb4gain;
	}

	public void setVideomb4gain(String videomb4gain) {
		this.videomb4gain = videomb4gain;
	}

	public String getVideopangain() {
		return this.videopangain;
	}

	public void setVideopangain(String videopangain) {
		this.videopangain = videopangain;
	}

	public Integer getYear() {
		return this.year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

}