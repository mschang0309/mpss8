package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the satelliteps database table.
 * 
 */
@Entity
@XmlRootElement
@Table(name="satelliteps")
public class Satellitep implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SATELLITEPS_ID_GENERATOR", sequenceName="SATELLITEPS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SATELLITEPS_ID_GENERATOR")
	private Long id;

	private Integer isualpbcleanupdelay;

	private Integer isualpbsetupdelay;

	private Integer isualreccleanupdelay;

	private Integer isualrecsetupdelay;

	private Integer minrfpainterval;

	private float postddtdelay;

	private Integer priority;

	private Integer psid;

	private Integer rsifocalplanedelay;

	private Integer rsiimagerpreheatdelay;

	private Integer rsipbcleanupdelay;

	private Integer rsipbkshift;

	private Integer rsipbsetupdelay;

	private Integer rsireccleanupdelay;

	private Integer rsirecsetupdelay;

	private Integer rsisecondarypreheatdelay;

	private Integer satelliteid;

	private Integer slewmaneuverdelay;

	private Integer twtaheatdelay;

	private Integer xbandswitchdelay;

	private Integer eclipseenterdelay;

	private Integer eclipseexitdelay;

	public Satellitep() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getIsualpbcleanupdelay() {
		return this.isualpbcleanupdelay;
	}

	public void setIsualpbcleanupdelay(Integer isualpbcleanupdelay) {
		this.isualpbcleanupdelay = isualpbcleanupdelay;
	}

	public Integer getIsualpbsetupdelay() {
		return this.isualpbsetupdelay;
	}

	public void setIsualpbsetupdelay(Integer isualpbsetupdelay) {
		this.isualpbsetupdelay = isualpbsetupdelay;
	}

	public Integer getIsualreccleanupdelay() {
		return this.isualreccleanupdelay;
	}

	public void setIsualreccleanupdelay(Integer isualreccleanupdelay) {
		this.isualreccleanupdelay = isualreccleanupdelay;
	}

	public Integer getIsualrecsetupdelay() {
		return this.isualrecsetupdelay;
	}

	public void setIsualrecsetupdelay(Integer isualrecsetupdelay) {
		this.isualrecsetupdelay = isualrecsetupdelay;
	}

	public Integer getMinrfpainterval() {
		return this.minrfpainterval;
	}

	public void setMinrfpainterval(Integer minrfpainterval) {
		this.minrfpainterval = minrfpainterval;
	}

	public float getPostddtdelay() {
		return this.postddtdelay;
	}

	public void setPostddtdelay(float postddtdelay) {
		this.postddtdelay = postddtdelay;
	}

	public Integer getPriority() {
		return this.priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Integer getPsid() {
		return this.psid;
	}

	public void setPsid(Integer psid) {
		this.psid = psid;
	}

	public Integer getRsifocalplanedelay() {
		return this.rsifocalplanedelay;
	}

	public void setRsifocalplanedelay(Integer rsifocalplanedelay) {
		this.rsifocalplanedelay = rsifocalplanedelay;
	}

	public Integer getRsiimagerpreheatdelay() {
		return this.rsiimagerpreheatdelay;
	}

	public void setRsiimagerpreheatdelay(Integer rsiimagerpreheatdelay) {
		this.rsiimagerpreheatdelay = rsiimagerpreheatdelay;
	}

	public Integer getRsipbcleanupdelay() {
		return this.rsipbcleanupdelay;
	}

	public void setRsipbcleanupdelay(Integer rsipbcleanupdelay) {
		this.rsipbcleanupdelay = rsipbcleanupdelay;
	}

	public Integer getRsipbkshift() {
		return this.rsipbkshift;
	}

	public void setRsipbkshift(Integer rsipbkshift) {
		this.rsipbkshift = rsipbkshift;
	}

	public Integer getRsipbsetupdelay() {
		return this.rsipbsetupdelay;
	}

	public void setRsipbsetupdelay(Integer rsipbsetupdelay) {
		this.rsipbsetupdelay = rsipbsetupdelay;
	}

	public Integer getRsireccleanupdelay() {
		return this.rsireccleanupdelay;
	}

	public void setRsireccleanupdelay(Integer rsireccleanupdelay) {
		this.rsireccleanupdelay = rsireccleanupdelay;
	}

	public Integer getRsirecsetupdelay() {
		return this.rsirecsetupdelay;
	}

	public void setRsirecsetupdelay(Integer rsirecsetupdelay) {
		this.rsirecsetupdelay = rsirecsetupdelay;
	}

	public Integer getRsisecondarypreheatdelay() {
		return this.rsisecondarypreheatdelay;
	}

	public void setRsisecondarypreheatdelay(Integer rsisecondarypreheatdelay) {
		this.rsisecondarypreheatdelay = rsisecondarypreheatdelay;
	}

	public Integer getSatelliteid() {
		return this.satelliteid;
	}

	public void setSatelliteid(Integer satelliteid) {
		this.satelliteid = satelliteid;
	}

	public Integer getSlewmaneuverdelay() {
		return this.slewmaneuverdelay;
	}

	public void setSlewmaneuverdelay(Integer slewmaneuverdelay) {
		this.slewmaneuverdelay = slewmaneuverdelay;
	}

	public Integer getTwtaheatdelay() {
		return this.twtaheatdelay;
	}

	public void setTwtaheatdelay(Integer twtaheatdelay) {
		this.twtaheatdelay = twtaheatdelay;
	}

	public Integer getXbandswitchdelay() {
		return this.xbandswitchdelay;
	}

	public void setXbandswitchdelay(Integer xbandswitchdelay) {
		this.xbandswitchdelay = xbandswitchdelay;
	}

	public Integer getEclipseenterdelay() {
		return this.eclipseenterdelay;
	}

	public void setEclipseenterdelay(Integer eclipseenterdelay) {
		this.eclipseenterdelay = eclipseenterdelay;
	}

	public Integer getEclipseexitdelay() {
		return this.eclipseexitdelay;
	}

	public void setEclipseexitdelay(Integer eclipseexitdelay) {
		this.eclipseexitdelay = eclipseexitdelay;
	}

}