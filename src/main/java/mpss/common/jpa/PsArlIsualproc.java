package mpss.common.jpa;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement
public class PsArlIsualproc implements Serializable {

	private static final long serialVersionUID = 1L;
    private Arl arl;
  // child eclipses
	@XmlElement(required = true)	
	private List<Isualprocrequest> isualprocrequests;
	
	
	public PsArlIsualproc() {
	}

	public PsArlIsualproc(Arl arl) {
		  this.arl = arl;
	}

  // Arl
	public Arl getArl() {
		  return this.arl;
	}

	public void setArl(Arl arl) {
		  this.arl = arl;
	}

  // Eclipses
  public List<Isualprocrequest> getIsualimagingrequests() {
  	  return this.isualprocrequests;
  }

  public void setIsualprocrequests(List<Isualprocrequest> isualprocrequests) {
  	  this.isualprocrequests = isualprocrequests;
  }

}