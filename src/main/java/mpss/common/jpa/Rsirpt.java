package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.sql.Timestamp;


/**
 * The persistent class for the rsirpt database table.
 * 
 */
@Entity
@XmlRootElement
public class Rsirpt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RSIRPT_ID_GENERATOR", sequenceName="RSIRPT_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RSIRPT_ID_GENERATOR")
	private Long id;

	private Timestamp sessionday;

	private String sectionname;

	private Integer ttq;

	private Integer aocs;

	private Integer pdm;

	private Integer man;

	private Integer gohome;

	private Integer rsi;

	private Integer pbk;

	private Integer ddt;

	private String filename;

	private Timestamp gendate;

	private Integer orbitstart;

	private Integer orbitend;

	private Integer tx;

	private Integer rsidel;
	
	private Integer aip=0;



	public Rsirpt() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getSessionday() {
		return this.sessionday;
	}

	public void setSessionday(Timestamp sessionday) {
		this.sessionday = sessionday;
	}

	public String getSectionname() {
		return this.sectionname;
	}

	public void setSectionname(String sectionname) {
		this.sectionname = sectionname;
	}

	public Integer getTtq() {
		return this.ttq;
	}

	public void setTtq(Integer ttq) {
		this.ttq = ttq;
	}

	public Integer getAocs() {
		return this.aocs;
	}

	public void setAocs(Integer aocs) {
		this.aocs = aocs;
	}

	public Integer getPdm() {
		return this.pdm;
	}

	public void setPdm(Integer pdm) {
		this.pdm = pdm;
	}

	public Integer getMan() {
		return this.man;
	}

	public void setMan(Integer man) {
		this.man = man;
	}

	public Integer getGohome() {
		return this.gohome;
	}

	public void setGohome(Integer gohome) {
		this.gohome = gohome;
	}

	public Integer getRsi() {
		return this.rsi;
	}

	public void setRsi(Integer rsi) {
		this.rsi = rsi;
	}

	public Integer getPbk() {
		return this.pbk;
	}

	public void setPbk(Integer pbk) {
		this.pbk = pbk;
	}

	public Integer getDdt() {
		return this.ddt;
	}

	public void setDdt(Integer ddt) {
		this.ddt = ddt;
	}

	public Integer getRrbitstart() {
		return this.orbitstart;
	}

	public void setOrbitstart(Integer orbitstart) {
		this.orbitstart = orbitstart;
	}

	public Integer getOrbitend() {
		return this.orbitend;
	}

	public void setOrbitend(Integer orbitend) {
		this.orbitend = orbitend;
	}

	public Integer getTx() {
		return this.tx;
	}

	public void setTx(Integer tx) {
		this.tx = tx;
	}

	public Integer getRsidel() {
		return this.rsidel;
	}

	public void setRsidel(Integer rsidel) {
		this.rsidel = rsidel;
	}

	public Timestamp getGendate() {
		return this.gendate;
	}

	public void setGendate(Timestamp gendate) {
		this.gendate = gendate;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public Integer getAip() {
		return this.aip;
	}

	public void setAip(Integer aip) {
		this.aip = aip;
	}


}