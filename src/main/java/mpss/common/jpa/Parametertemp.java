package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the parametertemp database table.
 * 
 */
@Entity
public class Parametertemp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PARAMETERTEMP_ID_GENERATOR", sequenceName="PARAMETERTEMP_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARAMETERTEMP_ID_GENERATOR")
	private Long id;

	private Integer alignplaybacks;

	private Integer coldschedulinglimit;

	private Integer commandpulselength;

	private Integer commenttorange;

	private Integer ephemerisprotectionfactor;

	private String equipgroupid;

	private Integer firstavailable;

	private Integer hotschedulinglimit;

	private Integer mincoldduration;

	private float mincoldmaxel;

	private Integer minimumpasslength;

	private Integer orbminduration;

	private float orbminmaxel;

	private Integer passstarttimelag;

	private Integer passstarttimelead;

	private Integer pbduration;

	private Integer pbfacilityid;

	private Integer pbstarttimelag;

	private Integer pbstarttimelead;

	private Integer percentageunscheduled;

	private Integer prepassqualifier;

	private Integer prepbduration;

	private Integer standardpbcommentid;

	private String usernameid;

	public Parametertemp() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAlignplaybacks() {
		return this.alignplaybacks;
	}

	public void setAlignplaybacks(Integer alignplaybacks) {
		this.alignplaybacks = alignplaybacks;
	}

	public Integer getColdschedulinglimit() {
		return this.coldschedulinglimit;
	}

	public void setColdschedulinglimit(Integer coldschedulinglimit) {
		this.coldschedulinglimit = coldschedulinglimit;
	}

	public Integer getCommandpulselength() {
		return this.commandpulselength;
	}

	public void setCommandpulselength(Integer commandpulselength) {
		this.commandpulselength = commandpulselength;
	}

	public Integer getCommenttorange() {
		return this.commenttorange;
	}

	public void setCommenttorange(Integer commenttorange) {
		this.commenttorange = commenttorange;
	}

	public Integer getEphemerisprotectionfactor() {
		return this.ephemerisprotectionfactor;
	}

	public void setEphemerisprotectionfactor(Integer ephemerisprotectionfactor) {
		this.ephemerisprotectionfactor = ephemerisprotectionfactor;
	}

	public String getEquipgroupid() {
		return this.equipgroupid;
	}

	public void setEquipgroupid(String equipgroupid) {
		this.equipgroupid = equipgroupid;
	}

	public Integer getFirstavailable() {
		return this.firstavailable;
	}

	public void setFirstavailable(Integer firstavailable) {
		this.firstavailable = firstavailable;
	}

	public Integer getHotschedulinglimit() {
		return this.hotschedulinglimit;
	}

	public void setHotschedulinglimit(Integer hotschedulinglimit) {
		this.hotschedulinglimit = hotschedulinglimit;
	}

	public Integer getMincoldduration() {
		return this.mincoldduration;
	}

	public void setMincoldduration(Integer mincoldduration) {
		this.mincoldduration = mincoldduration;
	}

	public float getMincoldmaxel() {
		return this.mincoldmaxel;
	}

	public void setMincoldmaxel(float mincoldmaxel) {
		this.mincoldmaxel = mincoldmaxel;
	}

	public Integer getMinimumpasslength() {
		return this.minimumpasslength;
	}

	public void setMinimumpasslength(Integer minimumpasslength) {
		this.minimumpasslength = minimumpasslength;
	}

	public Integer getOrbminduration() {
		return this.orbminduration;
	}

	public void setOrbminduration(Integer orbminduration) {
		this.orbminduration = orbminduration;
	}

	public float getOrbminmaxel() {
		return this.orbminmaxel;
	}

	public void setOrbminmaxel(float orbminmaxel) {
		this.orbminmaxel = orbminmaxel;
	}

	public Integer getPassstarttimelag() {
		return this.passstarttimelag;
	}

	public void setPassstarttimelag(Integer passstarttimelag) {
		this.passstarttimelag = passstarttimelag;
	}

	public Integer getPassstarttimelead() {
		return this.passstarttimelead;
	}

	public void setPassstarttimelead(Integer passstarttimelead) {
		this.passstarttimelead = passstarttimelead;
	}

	public Integer getPbduration() {
		return this.pbduration;
	}

	public void setPbduration(Integer pbduration) {
		this.pbduration = pbduration;
	}

	public Integer getPbfacilityid() {
		return this.pbfacilityid;
	}

	public void setPbfacilityid(Integer pbfacilityid) {
		this.pbfacilityid = pbfacilityid;
	}

	public Integer getPbstarttimelag() {
		return this.pbstarttimelag;
	}

	public void setPbstarttimelag(Integer pbstarttimelag) {
		this.pbstarttimelag = pbstarttimelag;
	}

	public Integer getPbstarttimelead() {
		return this.pbstarttimelead;
	}

	public void setPbstarttimelead(Integer pbstarttimelead) {
		this.pbstarttimelead = pbstarttimelead;
	}

	public Integer getPercentageunscheduled() {
		return this.percentageunscheduled;
	}

	public void setPercentageunscheduled(Integer percentageunscheduled) {
		this.percentageunscheduled = percentageunscheduled;
	}

	public Integer getPrepassqualifier() {
		return this.prepassqualifier;
	}

	public void setPrepassqualifier(Integer prepassqualifier) {
		this.prepassqualifier = prepassqualifier;
	}

	public Integer getPrepbduration() {
		return this.prepbduration;
	}

	public void setPrepbduration(Integer prepbduration) {
		this.prepbduration = prepbduration;
	}

	public Integer getStandardpbcommentid() {
		return this.standardpbcommentid;
	}

	public void setStandardpbcommentid(Integer standardpbcommentid) {
		this.standardpbcommentid = standardpbcommentid;
	}

	public String getUsernameid() {
		return this.usernameid;
	}

	public void setUsernameid(String usernameid) {
		this.usernameid = usernameid;
	}

}