package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the aipcommand database table.
 * 
 */
@Entity
@XmlRootElement
@Table(name="aipcommand")
public class Aipcommand implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="AIPCOMMAND_ID_GENERATOR", sequenceName="AIPCOMMAND_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AIPCOMMAND_ID_GENERATOR")
	private Long id;

	private String cmd;

	private String aipcmd;

	public Aipcommand() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCmd() {
		return this.cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getAipcmd() {
		return this.aipcmd;
	}

	public void setAipcmd(String aipcmd) {
		this.aipcmd = aipcmd;
	}

}