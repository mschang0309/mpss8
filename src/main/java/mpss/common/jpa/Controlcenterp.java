package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the controlcenterps database table.
 * 
 */
@Entity
@Table(name="controlcenterps")
public class Controlcenterp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CONTROLCENTERPS_ID_GENERATOR", sequenceName="CONTROLCENTERPS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CONTROLCENTERPS_ID_GENERATOR")
	private Long id;

	private Integer acslimit;

	private Integer ccid;

	private Integer psid;

	private Integer rank;

	public Controlcenterp() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAcslimit() {
		return this.acslimit;
	}

	public void setAcslimit(Integer acslimit) {
		this.acslimit = acslimit;
	}

	public Integer getCcid() {
		return this.ccid;
	}

	public void setCcid(Integer ccid) {
		this.ccid = ccid;
	}

	public Integer getPsid() {
		return this.psid;
	}

	public void setPsid(Integer psid) {
		this.psid = psid;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

}