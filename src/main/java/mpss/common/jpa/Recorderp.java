package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the recorderps database table.
 * 
 */
@Entity
@XmlRootElement
@Table(name="recorderps")
public class Recorderp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RECORDERPS_ID_GENERATOR", sequenceName="RECORDERPS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RECORDERPS_ID_GENERATOR")
	private Long id;

	private float avgisualwritesector;

	private Integer capacity;

	private Integer compressionratiocount;

	private Integer ddtcapacity;

	private Integer filesectormargin;

	private Integer isualcapacity;

	private Integer isualfilesize;

	private float msimagingmode;

	private float msplusimagingmode;

	private float outputdatarate;

	private float panimagingmode;

	private float panplusimagingmode;

	private Integer psid;

	private Integer recorderid;

	private Integer rsicapacity;

	private float timetofilloneddtsector;

	private float timetoreadoneisualsector;

	private float timetoreadonersisector;

	private Integer vc1ratio;

	private Integer vc2ratio;
	
	private float pan1;
	
	private float pan1_5;
	
	private float pan3_75;
	
	private float pan7_5;
	
	private float ms1;
	
	private float ms1_5;
	
	private float ms3_75;
	
	private float ms7_5;
	
	private float ddt_filesectormargin;
	
	// add column 20160602
	private float ddt_outputdatarate;
	private Integer ddt_vc1ratio;
	private Integer ddt_vc2ratio;
	private float ddt_pan1;
	private float ddt_pan1_5;
	private float ddt_pan3_75;
	private float ddt_pan7_5;
	private float ddt_ms1;
	private float ddt_ms1_5;
	private float ddt_ms3_75;
	private float ddt_ms7_5;

	public Recorderp() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public float getAvgisualwritesector() {
		return this.avgisualwritesector;
	}

	public void setAvgisualwritesector(float avgisualwritesector) {
		this.avgisualwritesector = avgisualwritesector;
	}

	public Integer getCapacity() {
		return this.capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Integer getCompressionratiocount() {
		return this.compressionratiocount;
	}

	public void setCompressionratiocount(Integer compressionratiocount) {
		this.compressionratiocount = compressionratiocount;
	}

	public Integer getDdtcapacity() {
		return this.ddtcapacity;
	}

	public void setDdtcapacity(Integer ddtcapacity) {
		this.ddtcapacity = ddtcapacity;
	}

	public Integer getFilesectormargin() {
		return this.filesectormargin;
	}

	public void setFilesectormargin(Integer filesectormargin) {
		this.filesectormargin = filesectormargin;
	}

	public Integer getIsualcapacity() {
		return this.isualcapacity;
	}

	public void setIsualcapacity(Integer isualcapacity) {
		this.isualcapacity = isualcapacity;
	}

	public Integer getIsualfilesize() {
		return this.isualfilesize;
	}

	public void setIsualfilesize(Integer isualfilesize) {
		this.isualfilesize = isualfilesize;
	}

	public float getMsimagingmode() {
		return this.msimagingmode;
	}

	public void setMsimagingmode(float msimagingmode) {
		this.msimagingmode = msimagingmode;
	}

	public float getMsplusimagingmode() {
		return this.msplusimagingmode;
	}

	public void setMsplusimagingmode(float msplusimagingmode) {
		this.msplusimagingmode = msplusimagingmode;
	}

	public float getOutputdatarate() {
		return this.outputdatarate;
	}

	public void setOutputdatarate(float outputdatarate) {
		this.outputdatarate = outputdatarate;
	}

	public float getPanimagingmode() {
		return this.panimagingmode;
	}

	public void setPanimagingmode(float panimagingmode) {
		this.panimagingmode = panimagingmode;
	}

	public float getPanplusimagingmode() {
		return this.panplusimagingmode;
	}

	public void setPanplusimagingmode(float panplusimagingmode) {
		this.panplusimagingmode = panplusimagingmode;
	}

	public Integer getPsid() {
		return this.psid;
	}

	public void setPsid(Integer psid) {
		this.psid = psid;
	}

	public Integer getRecorderid() {
		return this.recorderid;
	}

	public void setRecorderid(Integer recorderid) {
		this.recorderid = recorderid;
	}

	public Integer getRsicapacity() {
		return this.rsicapacity;
	}

	public void setRsicapacity(Integer rsicapacity) {
		this.rsicapacity = rsicapacity;
	}

	public float getTimetofilloneddtsector() {
		return this.timetofilloneddtsector;
	}

	public void setTimetofilloneddtsector(float timetofilloneddtsector) {
		this.timetofilloneddtsector = timetofilloneddtsector;
	}

	public float getTimetoreadoneisualsector() {
		return this.timetoreadoneisualsector;
	}

	public void setTimetoreadoneisualsector(float timetoreadoneisualsector) {
		this.timetoreadoneisualsector = timetoreadoneisualsector;
	}

	public float getTimetoreadonersisector() {
		return this.timetoreadonersisector;
	}

	public void setTimetoreadonersisector(float timetoreadonersisector) {
		this.timetoreadonersisector = timetoreadonersisector;
	}

	public Integer getVc1ratio() {
		return this.vc1ratio;
	}

	public void setVc1ratio(Integer vc1ratio) {
		this.vc1ratio = vc1ratio;
	}

	public Integer getVc2ratio() {
		return this.vc2ratio;
	}

	public void setVc2ratio(Integer vc2ratio) {
		this.vc2ratio = vc2ratio;
	}
	
	public float getPan1() {
		return this.pan1;
	}

	public void setPan1(float pan1) {
		this.pan1 = pan1;
	}
	
	public float getPan1_5() {
		return this.pan1_5;
	}

	public void setPan1_5(float pan1_5) {
		this.pan1_5 = pan1_5;
	}
	
	public float getPan3_75() {
		return this.pan3_75;
	}

	public void setPan3_75(float pan3_75) {
		this.pan3_75 = pan3_75;
	}
	
	public float getPan7_5() {
		return this.pan7_5;
	}

	public void setPan7_5(float pan7_5) {
		this.pan7_5 = pan7_5;
	}
	
	public float getMs1() {
		return this.ms1;
	}

	public void setMs1(float ms1) {
		this.ms1 = ms1;
	}
	
	public float getMs1_5() {
		return this.ms1_5;
	}

	public void setMs1_5(float ms1_5) {
		this.ms1_5 = ms1_5;
	}
	
	public float getMs3_75() {
		return this.ms3_75;
	}

	public void setMs3_75(float ms3_75) {
		this.ms3_75 = ms3_75;
	}
	
	public float getMs7_5() {
		return this.ms7_5;
	}

	public void setMs7_5(float ms7_5) {
		this.ms7_5 = ms7_5;
	}
	
	public float getDDTFileMargin() {
		return this.ddt_filesectormargin;
	}

	public void setDDTFileMargin(float ddtfilemargin) {
		this.ddt_filesectormargin = ddtfilemargin;
	}
	
	public float getDDTOutputdatarate() {
		return this.ddt_outputdatarate;
	}

	public void setDDTOutputdatarate(float ddtoutputdatarate) {
		this.ddt_outputdatarate = ddtoutputdatarate;
	}
	
	public Integer getDDTVc1ratio() {
		return this.ddt_vc1ratio;
	}

	public void setDDTVc1ratio(Integer ddtvc1ratio) {
		this.ddt_vc1ratio = ddtvc1ratio;
	}

	public Integer getDDTVc2ratio() {
		return this.ddt_vc2ratio;
	}

	public void setDDTVc2ratio(Integer ddtvc2ratio) {
		this.ddt_vc2ratio = ddtvc2ratio;
	}
	
	//
	public float getDDTPan1() {
		return this.ddt_pan1;
	}

	public void setDDTPan1(float ddtpan1) {
		this.ddt_pan1 = ddtpan1;
	}
	
	public float getDDTPan1_5() {
		return this.ddt_pan1_5;
	}

	public void setDDTPan1_5(float ddtpan1_5) {
		this.ddt_pan1_5 = ddtpan1_5;
	}
	
	public float getDDTPan3_75() {
		return this.ddt_pan3_75;
	}

	public void setDDTPan3_75(float ddtpan3_75) {
		this.ddt_pan3_75 = ddtpan3_75;
	}
	
	public float getDDTPan7_5() {
		return this.ddt_pan7_5;
	}

	public void setDDTPan7_5(float ddtpan7_5) {
		this.ddt_pan7_5 = ddtpan7_5;
	}
	
	public float getDDTMs1() {
		return this.ddt_ms1;
	}

	public void setDDTMs1(float ddtms1) {
		this.ddt_ms1 = ddtms1;
	}
	
	public float getDDTMs1_5() {
		return this.ddt_ms1_5;
	}

	public void setDDTMs1_5(float ddtms1_5) {
		this.ddt_ms1_5 = ddtms1_5;
	}
	
	public float getDDTMs3_75() {
		return this.ddt_ms3_75;
	}

	public void setDDTMs3_75(float ddtms3_75) {
		this.ddt_ms3_75 = ddtms3_75;
	}
	
	public float getDDTMs7_5() {
		return this.ddt_ms7_5;
	}

	public void setDDTMs7_5(float ddtms7_5) {
		this.ddt_ms7_5 = ddtms7_5;
	}


}