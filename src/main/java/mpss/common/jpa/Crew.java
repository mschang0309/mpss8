package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the crews database table.
 * 
 */
@Entity
@Table(name="crews")
public class Crew implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CREWS_ID_GENERATOR", sequenceName="CREWS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CREWS_ID_GENERATOR")
	private Long id;

	public Crew() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}