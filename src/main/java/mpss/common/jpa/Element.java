package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the elements database table.
 * 
 */
@Entity
@XmlRootElement
@Table(name="elements")
public class Element implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ELEMENTS_ID_GENERATOR", sequenceName="ELEMENTS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ELEMENTS_ID_GENERATOR")
	private Long id;

	private float argperigee;

	private Integer branchid;

	private float decayrate;

	private float eccentricity;

	private float elementtimeinsecs;

	private Integer elementyear;

	private Integer epochrev;

	private float firstderiv;

	private float inclination;

	private float meananomaly;

	private float meanmotion;

	private float orbitalperiod;

	private float rightasc;

	private Integer satelliteid;

	private float secondderiv;

	private float semimajoraxis;

	public Element() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public float getArgperigee() {
		return this.argperigee;
	}

	public void setArgperigee(float argperigee) {
		this.argperigee = argperigee;
	}

	public Integer getBranchid() {
		return this.branchid;
	}

	public void setBranchid(Integer branchid) {
		this.branchid = branchid;
	}

	public float getDecayrate() {
		return this.decayrate;
	}

	public void setDecayrate(float decayrate) {
		this.decayrate = decayrate;
	}

	public float getEccentricity() {
		return this.eccentricity;
	}

	public void setEccentricity(float eccentricity) {
		this.eccentricity = eccentricity;
	}

	public float getElementtimeinsecs() {
		return this.elementtimeinsecs;
	}

	public void setElementtimeinsecs(float elementtimeinsecs) {
		this.elementtimeinsecs = elementtimeinsecs;
	}

	public Integer getElementyear() {
		return this.elementyear;
	}

	public void setElementyear(Integer elementyear) {
		this.elementyear = elementyear;
	}

	public Integer getEpochrev() {
		return this.epochrev;
	}

	public void setEpochrev(Integer epochrev) {
		this.epochrev = epochrev;
	}

	public float getFirstderiv() {
		return this.firstderiv;
	}

	public void setFirstderiv(float firstderiv) {
		this.firstderiv = firstderiv;
	}

	public float getInclination() {
		return this.inclination;
	}

	public void setInclination(float inclination) {
		this.inclination = inclination;
	}

	public float getMeananomaly() {
		return this.meananomaly;
	}

	public void setMeananomaly(float meananomaly) {
		this.meananomaly = meananomaly;
	}

	public float getMeanmotion() {
		return this.meanmotion;
	}

	public void setMeanmotion(float meanmotion) {
		this.meanmotion = meanmotion;
	}

	public float getOrbitalperiod() {
		return this.orbitalperiod;
	}

	public void setOrbitalperiod(float orbitalperiod) {
		this.orbitalperiod = orbitalperiod;
	}

	public float getRightasc() {
		return this.rightasc;
	}

	public void setRightasc(float rightasc) {
		this.rightasc = rightasc;
	}

	public Integer getSatelliteid() {
		return this.satelliteid;
	}

	public void setSatelliteid(Integer satelliteid) {
		this.satelliteid = satelliteid;
	}

	public float getSecondderiv() {
		return this.secondderiv;
	}

	public void setSecondderiv(float secondderiv) {
		this.secondderiv = secondderiv;
	}

	public float getSemimajoraxis() {
		return this.semimajoraxis;
	}

	public void setSemimajoraxis(float semimajoraxis) {
		this.semimajoraxis = semimajoraxis;
	}

}