package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * The persistent class for the recorder database table.
 * 
 */
@Entity
@XmlRootElement
public class Recorder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RECORDER_ID_GENERATOR", sequenceName="RECORDER_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="RECORDER_ID_GENERATOR")
	private Long id;

	private Integer maxddtfiles;

	private Integer maxisualfiles;

	private Integer maxrsifiles;
	
	private Integer userfiles;

	private String name;

	private Integer satelliteid;

	private Integer sectorsize;

	private Integer totalfiles;

  // handcrafted entities
	@ManyToOne
	@JoinColumn(name = "satelliteid", insertable=false, updatable=false)
	private Satellite satellite;
	
	public Recorder() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getMaxddtfiles() {
		return this.maxddtfiles;
	}

	public void setMaxddtfiles(Integer maxddtfiles) {
		this.maxddtfiles = maxddtfiles;
	}

	public Integer getMaxisualfiles() {
		return this.maxisualfiles;
	}

	public void setMaxisualfiles(Integer maxisualfiles) {
		this.maxisualfiles = maxisualfiles;
	}

	public Integer getMaxrsifiles() {
		return this.maxrsifiles;
	}

	public void setMaxrsifiles(Integer maxrsifiles) {
		this.maxrsifiles = maxrsifiles;
	}
	
	public Integer getUserfiles() {
		return this.userfiles;
	}

	public void setUserfiles(Integer userfiles) {
		this.userfiles = userfiles;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSatelliteid() {
		return this.satelliteid;
	}

	public void setSatelliteid(Integer satelliteid) {
		this.satelliteid = satelliteid;
	}

	public Integer getSectorsize() {
		return this.sectorsize;
	}

	public void setSectorsize(Integer sectorsize) {
		this.sectorsize = sectorsize;
	}

	public Integer getTotalfiles() {
		return this.totalfiles;
	}

	public void setTotalfiles(Integer totalfiles) {
		this.totalfiles = totalfiles;
	}

  // handcrafted 
  @XmlInverseReference(mappedBy = "recorders")
  public Satellite getSatellite() {
  	return this.satellite;
  }

  public void setSatellite(Satellite satellite) {
  	this.satellite = satellite;
  }
  
}