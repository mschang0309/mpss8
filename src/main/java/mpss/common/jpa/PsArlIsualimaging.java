package mpss.common.jpa;

import java.io.Serializable;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement
public class PsArlIsualimaging implements Serializable {

	private static final long serialVersionUID = 1L;
    private Arl arl;
  // child eclipses
	@XmlElement(required = true)	
	private List<Isualimagingrequest> isualimagingrequests;
	
	
	public PsArlIsualimaging() {
	}

	public PsArlIsualimaging(Arl arl) {
		  this.arl = arl;
	}

  // Arl
	public Arl getArl() {
		  return this.arl;
	}

	public void setArl(Arl arl) {
		  this.arl = arl;
	}

  // Eclipses
  public List<Isualimagingrequest> getIsualimagingrequests() {
  	  return this.isualimagingrequests;
  }

  public void setIsualimagingrequests(List<Isualimagingrequest> isualimagingrequests) {
  	  this.isualimagingrequests = isualimagingrequests;
  }

}