package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * The persistent class for the antenna database table.
 * 
 */
@Entity
public class Antenna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ANTENNA_ID_GENERATOR", sequenceName="ANTENNA_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ANTENNA_ID_GENERATOR")
	private Long id;

	private String anttype;

	private String name;

	private Integer satelliteid;

  // handcrafted entities
	@ManyToOne
	@JoinColumn(name = "satelliteid", insertable=false, updatable=false)
	private Satellite satellite;
	
	public Antenna() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAnttype() {
		return this.anttype;
	}

	public void setAnttype(String anttype) {
		this.anttype = anttype;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSatelliteid() {
		return this.satelliteid;
	}

	public void setSatelliteid(Integer satelliteid) {
		this.satelliteid = satelliteid;
	}

  // handcrafted 
  @XmlInverseReference(mappedBy = "antennas")
  public Satellite getSatellite() {
  	return this.satellite;
  }

  public void setSatellite(Satellite satellite) {
  	this.satellite = satellite;
  }
}