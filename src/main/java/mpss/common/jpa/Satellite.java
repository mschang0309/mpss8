package mpss.common.jpa;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * The persistent class for the satellite database table.
 * 
 */
@Entity
@XmlRootElement
public class Satellite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SATELLITE_ID_GENERATOR", sequenceName="SATELLITE_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SATELLITE_ID_GENERATOR")
	private Long id;

	private String commanddbscenario;

	private Integer ephpropduration;

	private Integer includeinpap;

	private Integer maxandrifttime;

	private String name;

	private Integer nominalorbit;

	private String ops4number;

	private String opsnumber;

	private String project;

	private String sattype;

	private Integer scenarioversion;

	private String videogain;
	
	private String shortname;
	
	private Integer asyncrec_durlimit;
	
	private float rrate_upperbound;
	
	private float rrate_lowerbound;
	
	private float prate_upperbound;
	
	private float prate_lowerbound;
	
	private float yrate_upperbound;
	
	private float yrate_lowerbound;
	
  // handcrafted entities
	@OneToMany(mappedBy="satellite", cascade={CascadeType.ALL})
	@XmlElement(required = true)	
	private Set<Recorder> recorders;

	@OneToMany(mappedBy="satellite", cascade={CascadeType.ALL})
	private Set<Antenna> antennas;

	@OneToMany(mappedBy="satellite", cascade={CascadeType.ALL})
	private Set<Transmitter> transmitters;

	public Satellite() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCommanddbscenario() {
		return this.commanddbscenario;
	}

	public void setCommanddbscenario(String commanddbscenario) {
		this.commanddbscenario = commanddbscenario;
	}

	public Integer getEphpropduration() {
		return this.ephpropduration;
	}

	public void setEphpropduration(Integer ephpropduration) {
		this.ephpropduration = ephpropduration;
	}

	public Integer getIncludeinpap() {
		return this.includeinpap;
	}

	public void setIncludeinpap(Integer includeinpap) {
		this.includeinpap = includeinpap;
	}

	public Integer getMaxandrifttime() {
		return this.maxandrifttime;
	}

	public void setMaxandrifttime(Integer maxandrifttime) {
		this.maxandrifttime = maxandrifttime;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNominalorbit() {
		return this.nominalorbit;
	}

	public void setNominalorbit(Integer nominalorbit) {
		this.nominalorbit = nominalorbit;
	}

	public String getOps4number() {
		return this.ops4number;
	}

	public void setOps4number(String ops4number) {
		this.ops4number = ops4number;
	}

	public String getOpsnumber() {
		return this.opsnumber;
	}

	public void setOpsnumber(String opsnumber) {
		this.opsnumber = opsnumber;
	}

	public String getProject() {
		return this.project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getSattype() {
		return this.sattype;
	}

	public void setSattype(String sattype) {
		this.sattype = sattype;
	}

	public Integer getScenarioversion() {
		return this.scenarioversion;
	}

	public void setScenarioversion(Integer scenarioversion) {
		this.scenarioversion = scenarioversion;
	}

	public String getVideogain() {
		return this.videogain;
	}

	public void setVideogain(String videogain) {
		this.videogain = videogain;
	}
	
	public String getShortname() {
		return this.shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}
	
	public Integer getAsyncrec_durlimit() {
		return this.asyncrec_durlimit;
	}

	public void setAsyncrec_durlimit(Integer asyncrec_durlimit) {
		this.asyncrec_durlimit = asyncrec_durlimit;
	}
	
	public float getRrate_upperbound() {
		return this.rrate_upperbound;
	}

	public void setRrate_upperbound(float rrate_upperbound) {
		this.rrate_upperbound = rrate_upperbound;
	}
	
	public float getRrate_lowerbound() {
		return this.rrate_lowerbound;
	}

	public void setRrate_lowerbound(float rrate_lowerbound) {
		this.rrate_lowerbound = rrate_lowerbound;
	}
	
	public float getPrate_upperbound() {
		return this.prate_upperbound;
	}

	public void setPrate_upperbound(float prate_upperbound) {
		this.prate_upperbound = prate_upperbound;
	}
	
	public float getPrate_lowerbound() {
		return this.prate_lowerbound;
	}

	public void setPrate_lowerbound(float prate_lowerbound) {
		this.prate_lowerbound = prate_lowerbound;
	}
	
	public float getYrate_upperbound() {
		return this.yrate_upperbound;
	}

	public void setYrate_upperbound(float yrate_upperbound) {
		this.yrate_upperbound = yrate_upperbound;
	}
	
	public float getYrate_lowerbound() {
		return this.yrate_lowerbound;
	}

	public void setYrate_lowerbound(float yrate_lowerbound) {
		this.yrate_lowerbound = yrate_lowerbound;
	}

  // handcrafted entities
  // Antennas
  public Set<Antenna> getAntennas() {
  	    return this.antennas;
  }

  public void setAntennas(Set<Antenna> antennas) {
  	    this.antennas = antennas;
  }
  // Recorders
  public Set<Recorder> getRecorders() {
	    return this.recorders;
  }

  public void setRecorders(Set<Recorder> recorders) {
	    this.recorders = recorders;
  }
  // Transmitters
  public Set<Transmitter> getTransmitters() {
	    return this.transmitters;
  }

  public void setTransmitters(Set<Transmitter> transmitters) {
	    this.transmitters = transmitters;
  }
}