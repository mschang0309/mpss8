package mpss.common.jpa;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import mpss.util.timeformat.TimeRange;

import java.sql.Timestamp;


/**
 * The persistent class for the contact database table.
 * 
 */
@Entity
@XmlRootElement
public class Contact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CONTACT_ID_GENERATOR", sequenceName="CONTACT_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CONTACT_ID_GENERATOR")
	private Long id;

	private Integer available;

	private Integer backbias;

	private Integer backexp;

	private Integer branchid;

	private Timestamp cmdfade;

	private Timestamp cmdrise;

	private Timestamp enduot;

	private Timestamp fade;

	private Integer frontbias;

	private Integer frontexp;

	private Integer maxel;

	private Timestamp maxeltime;

	private Integer revid;

	private Timestamp rise;

	private Integer satelliteid;

	private Integer siteid;

	private Timestamp startuot;

	private Integer uotcnt;

	private Timestamp xbandfade;

	private Timestamp xbandrise;

	public Contact() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAvailable() {
		return this.available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

	public Integer getBackbias() {
		return this.backbias;
	}

	public void setBackbias(Integer backbias) {
		this.backbias = backbias;
	}

	public Integer getBackexp() {
		return this.backexp;
	}

	public void setBackexp(Integer backexp) {
		this.backexp = backexp;
	}

	public Integer getBranchid() {
		return this.branchid;
	}

	public void setBranchid(Integer branchid) {
		this.branchid = branchid;
	}

	public Timestamp getCmdfade() {
		return this.cmdfade;
	}

	public void setCmdfade(Timestamp cmdfade) {
		this.cmdfade = cmdfade;
	}

	public Timestamp getCmdrise() {
		return this.cmdrise;
	}

	public void setCmdrise(Timestamp cmdrise) {
		this.cmdrise = cmdrise;
	}

	public Timestamp getEnduot() {
		return this.enduot;
	}

	public void setEnduot(Timestamp enduot) {
		this.enduot = enduot;
	}

	public Timestamp getFade() {
		return this.fade;
	}

	public void setFade(Timestamp fade) {
		this.fade = fade;
	}

	public Integer getFrontbias() {
		return this.frontbias;
	}

	public void setFrontbias(Integer frontbias) {
		this.frontbias = frontbias;
	}

	public Integer getFrontexp() {
		return this.frontexp;
	}

	public void setFrontexp(Integer frontexp) {
		this.frontexp = frontexp;
	}

	public Integer getMaxel() {
		return this.maxel;
	}

	public void setMaxel(Integer maxel) {
		this.maxel = maxel;
	}

	public Timestamp getMaxeltime() {
		return this.maxeltime;
	}

	public void setMaxeltime(Timestamp maxeltime) {
		this.maxeltime = maxeltime;
	}

	public Integer getRevid() {
		return this.revid;
	}

	public void setRevid(Integer revid) {
		this.revid = revid;
	}

	public Timestamp getRise() {
		return this.rise;
	}

	public void setRise(Timestamp rise) {
		this.rise = rise;
	}

	public Integer getSatelliteid() {
		return this.satelliteid;
	}

	public void setSatelliteid(Integer satelliteid) {
		this.satelliteid = satelliteid;
	}

	public Integer getSiteid() {
		return this.siteid;
	}

	public void setSiteid(Integer siteid) {
		this.siteid = siteid;
	}

	public Timestamp getStartuot() {
		return this.startuot;
	}

	public void setStartuot(Timestamp startuot) {
		this.startuot = startuot;
	}

	public Integer getUotcnt() {
		return this.uotcnt;
	}

	public void setUotcnt(Integer uotcnt) {
		this.uotcnt = uotcnt;
	}

	public Timestamp getXbandfade() {
		return this.xbandfade;
	}

	public void setXbandfade(Timestamp xbandfade) {
		this.xbandfade = xbandfade;
	}

	public Timestamp getXbandrise() {
		return this.xbandrise;
	}

	public void setXbandrise(Timestamp xbandrise) {
		this.xbandrise = xbandrise;
	}

	public TimeRange getTimeRange() {
		return new TimeRange(this.rise,this.fade);
	}
}