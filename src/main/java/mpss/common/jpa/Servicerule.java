package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the servicerule database table.
 * 
 */
@Entity
public class Servicerule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SERVICERULE_ID_GENERATOR", sequenceName="SERVICERULE_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SERVICERULE_ID_GENERATOR")
	private Long id;

	private String servicerule;

	private String servicetyperunning;

	private String servicetypewantstorun;

	public Servicerule() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServicerule() {
		return this.servicerule;
	}

	public void setServicerule(String servicerule) {
		this.servicerule = servicerule;
	}

	public String getServicetyperunning() {
		return this.servicetyperunning;
	}

	public void setServicetyperunning(String servicetyperunning) {
		this.servicetyperunning = servicetyperunning;
	}

	public String getServicetypewantstorun() {
		return this.servicetypewantstorun;
	}

	public void setServicetypewantstorun(String servicetypewantstorun) {
		this.servicetypewantstorun = servicetypewantstorun;
	}

}