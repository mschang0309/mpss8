package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.sql.Timestamp;


/**
 * The persistent class for the eclipse database table.
 * 
 */
@Entity
@XmlRootElement
public class Latitude implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="LATITUDE_ID_GENERATOR", sequenceName="LATITUDE_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="LATITUDE_ID_GENERATOR")
	private Long id;

	private Integer branchid;	

	private Integer revid;

	private Integer satelliteid;

	private Timestamp latitude70time;

	private Timestamp latitude40time;

	public Latitude() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBranchid() {
		return this.branchid;
	}

	public void setBranchid(Integer branchid) {
		this.branchid = branchid;
	}
	

	public Timestamp getLatitude70time() {
		return this.latitude70time;
	}

	public void setLatitude70time(Timestamp latitude70time) {
		this.latitude70time = latitude70time;
	}

	public Integer getRevid() {
		return this.revid;
	}

	public void setRevid(Integer revid) {
		this.revid = revid;
	}

	public Integer getSatelliteid() {
		return this.satelliteid;
	}

	public void setSatelliteid(Integer satelliteid) {
		this.satelliteid = satelliteid;
	}

	public Timestamp getLatitude40time() {
		return this.latitude40time;
	}

	public void setLatitude40time(Timestamp latitude40time) {
		this.latitude40time = latitude40time;
	}

	

}