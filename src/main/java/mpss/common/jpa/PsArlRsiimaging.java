package mpss.common.jpa;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement
public class PsArlRsiimaging implements Serializable {

	private static final long serialVersionUID = 1L;
    private Arl arl;
  // child eclipses
	@XmlElement(required = true)	
	private List<Rsiimagingrequest> rsiimagingrequests;
	
	
	public PsArlRsiimaging() {
	}

	public PsArlRsiimaging(Arl arl) {
		this.arl = arl;
	}

  // Arl
	public Arl getArl() {
		return this.arl;
	}

	public void setArl(Arl arl) {
		this.arl = arl;
	}

  // Eclipses
  public List<Rsiimagingrequest> getRsiimagingrequests() {
  	    return this.rsiimagingrequests;
  }

  public void setManeuverrequests(List<Rsiimagingrequest> rsiimagingrequests) {
  	    this.rsiimagingrequests = rsiimagingrequests;
  }

}