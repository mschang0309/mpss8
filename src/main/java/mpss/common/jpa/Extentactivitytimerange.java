package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.sql.Timestamp;


/**
 * The persistent class for the extentactivitytimerange database table.
 * 
 */
@Entity
@XmlRootElement
public class Extentactivitytimerange implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="EXTENTACTIVITYTIMERANGE_ID_GENERATOR", sequenceName="EXTENTACTIVITYTIMERANGE_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EXTENTACTIVITYTIMERANGE_ID_GENERATOR")
	private Long id;

	private Timestamp endtime;

	private Integer parentid;

	private Timestamp starttime;

	private String stringdata;

	public Extentactivitytimerange() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getEndtime() {
		return this.endtime;
	}

	public void setEndtime(Timestamp endtime) {
		this.endtime = endtime;
	}

	public Integer getParentid() {
		return this.parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public Timestamp getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

	public String getStringdata() {
		return this.stringdata;
	}

	public void setStringdata(String stringdata) {
		this.stringdata = stringdata;
	}

}