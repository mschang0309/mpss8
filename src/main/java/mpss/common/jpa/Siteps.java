package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the siteps database table.
 * 
 */
@Entity
@XmlRootElement
//@Table(name="siteps")
public class Siteps implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SITEPS_ID_GENERATOR", sequenceName="SITEPS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SITEPS_ID_GENERATOR")
	private Long id;

	private Integer datacollect;

	private Integer postpasscleanuptime;

	private Integer prepasssetuptime;

	private Integer psid;

	private Integer siteid;

	private Integer turnaroundtime;

	private Integer uplinkavailable;

	public Siteps() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDatacollect() {
		return this.datacollect;
	}

	public void setDatacollect(Integer datacollect) {
		this.datacollect = datacollect;
	}

	public Integer getPostpasscleanuptime() {
		return this.postpasscleanuptime;
	}

	public void setPostpasscleanuptime(Integer postpasscleanuptime) {
		this.postpasscleanuptime = postpasscleanuptime;
	}

	public Integer getPrepasssetuptime() {
		return this.prepasssetuptime;
	}

	public void setPrepasssetuptime(Integer prepasssetuptime) {
		this.prepasssetuptime = prepasssetuptime;
	}

	public Integer getPsid() {
		return this.psid;
	}

	public void setPsid(Integer psid) {
		this.psid = psid;
	}

	public Integer getSiteid() {
		return this.siteid;
	}

	public void setSiteid(Integer siteid) {
		this.siteid = siteid;
	}

	public Integer getTurnaroundtime() {
		return this.turnaroundtime;
	}

	public void setTurnaroundtime(Integer turnaroundtime) {
		this.turnaroundtime = turnaroundtime;
	}

	public Integer getUplinkavailable() {
		return this.uplinkavailable;
	}

	public void setUplinkavailable(Integer uplinkavailable) {
		this.uplinkavailable = uplinkavailable;
	}

}