package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the isualimagingrequests database table.
 * 
 */
@Entity
@XmlRootElement
@Table(name="isualimagingrequests")
public class Isualimagingrequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ISUALIMAGINGREQUESTS_ID_GENERATOR", sequenceName="ISUALIMAGINGREQUESTS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ISUALIMAGINGREQUESTS_ID_GENERATOR")
	private Long id;

	private Integer arlid;

	private String comments;

	private Integer contactoccurrence;

	private Integer day;

	private String filename;

	private Integer remotetrackingflag;

	private Integer year;

	public Isualimagingrequest() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getArlid() {
		return this.arlid;
	}

	public void setArlid(Integer arlid) {
		this.arlid = arlid;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getContactoccurrence() {
		return this.contactoccurrence;
	}

	public void setContactoccurrence(Integer contactoccurrence) {
		this.contactoccurrence = contactoccurrence;
	}

	public Integer getDay() {
		return this.day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Integer getRemotetrackingflag() {
		return this.remotetrackingflag;
	}

	public void setRemotetrackingflag(Integer remotetrackingflag) {
		this.remotetrackingflag = remotetrackingflag;
	}

	public Integer getYear() {
		return this.year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

}