package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the globalparameters database table.
 * 
 */
@Entity
@XmlRootElement
@Table(name="globalparameters")
public class Globalparameter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="GLOBALPARAMETERS_ID_GENERATOR", sequenceName="GLOBALPARAMETERS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GLOBALPARAMETERS_ID_GENERATOR")
	private Long id;

	private Integer coldschedulinglimit;

	private String commenttorange;

	private String equipgroupid;

	private Integer hotschedulinglimit;

	private Integer orbminduration;

	private Integer orbminmaxel;

	private Integer passstarttimelag;

	private Integer passstarttimelead;

	private Integer pbduration;

	private String pbfacilityid;

	private Integer pbstarttimelag;

	private Integer pbstarttimelead;

	private Integer prepassqualifier;

	private Integer prepbduration;

	private Integer sessionduration;

	private String standardpbcommentid;

	private String usernameid;

	public Globalparameter() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getColdschedulinglimit() {
		return this.coldschedulinglimit;
	}

	public void setColdschedulinglimit(Integer coldschedulinglimit) {
		this.coldschedulinglimit = coldschedulinglimit;
	}

	public String getCommenttorange() {
		return this.commenttorange;
	}

	public void setCommenttorange(String commenttorange) {
		this.commenttorange = commenttorange;
	}

	public String getEquipgroupid() {
		return this.equipgroupid;
	}

	public void setEquipgroupid(String equipgroupid) {
		this.equipgroupid = equipgroupid;
	}

	public Integer getHotschedulinglimit() {
		return this.hotschedulinglimit;
	}

	public void setHotschedulinglimit(Integer hotschedulinglimit) {
		this.hotschedulinglimit = hotschedulinglimit;
	}

	public Integer getOrbminduration() {
		return this.orbminduration;
	}

	public void setOrbminduration(Integer orbminduration) {
		this.orbminduration = orbminduration;
	}

	public Integer getOrbminmaxel() {
		return this.orbminmaxel;
	}

	public void setOrbminmaxel(Integer orbminmaxel) {
		this.orbminmaxel = orbminmaxel;
	}

	public Integer getPassstarttimelag() {
		return this.passstarttimelag;
	}

	public void setPassstarttimelag(Integer passstarttimelag) {
		this.passstarttimelag = passstarttimelag;
	}

	public Integer getPassstarttimelead() {
		return this.passstarttimelead;
	}

	public void setPassstarttimelead(Integer passstarttimelead) {
		this.passstarttimelead = passstarttimelead;
	}

	public Integer getPbduration() {
		return this.pbduration;
	}

	public void setPbduration(Integer pbduration) {
		this.pbduration = pbduration;
	}

	public String getPbfacilityid() {
		return this.pbfacilityid;
	}

	public void setPbfacilityid(String pbfacilityid) {
		this.pbfacilityid = pbfacilityid;
	}

	public Integer getPbstarttimelag() {
		return this.pbstarttimelag;
	}

	public void setPbstarttimelag(Integer pbstarttimelag) {
		this.pbstarttimelag = pbstarttimelag;
	}

	public Integer getPbstarttimelead() {
		return this.pbstarttimelead;
	}

	public void setPbstarttimelead(Integer pbstarttimelead) {
		this.pbstarttimelead = pbstarttimelead;
	}

	public Integer getPrepassqualifier() {
		return this.prepassqualifier;
	}

	public void setPrepassqualifier(Integer prepassqualifier) {
		this.prepassqualifier = prepassqualifier;
	}

	public Integer getPrepbduration() {
		return this.prepbduration;
	}

	public void setPrepbduration(Integer prepbduration) {
		this.prepbduration = prepbduration;
	}

	public Integer getSessionduration() {
		return this.sessionduration;
	}

	public void setSessionduration(Integer sessionduration) {
		this.sessionduration = sessionduration;
	}

	public String getStandardpbcommentid() {
		return this.standardpbcommentid;
	}

	public void setStandardpbcommentid(String standardpbcommentid) {
		this.standardpbcommentid = standardpbcommentid;
	}

	public String getUsernameid() {
		return this.usernameid;
	}

	public void setUsernameid(String usernameid) {
		this.usernameid = usernameid;
	}

}