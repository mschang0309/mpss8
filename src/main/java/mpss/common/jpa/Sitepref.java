package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the sitepref database table.
 * 
 */
@Entity
@XmlRootElement
public class Sitepref implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SITEPREF_ID_GENERATOR", sequenceName="SITEPREF_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SITEPREF_ID_GENERATOR")
	private Long id;

	private Integer rank;

	private Integer rplus;

	private Integer satpsid;

	private Integer siteid;

	public Sitepref() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getRank() {
		return this.rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Integer getRplus() {
		return this.rplus;
	}

	public void setRplus(Integer rplus) {
		this.rplus = rplus;
	}

	public Integer getSatpsid() {
		return this.satpsid;
	}

	public void setSatpsid(Integer satpsid) {
		this.satpsid = satpsid;
	}

	public Integer getSiteid() {
		return this.siteid;
	}

	public void setSiteid(Integer siteid) {
		this.siteid = siteid;
	}

}