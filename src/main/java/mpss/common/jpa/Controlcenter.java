package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the controlcenter database table.
 * 
 */
@Entity
public class Controlcenter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CONTROLCENTER_ID_GENERATOR", sequenceName="CONTROLCENTER_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CONTROLCENTER_ID_GENERATOR")
	private Long id;

	private String name;

	private String name1;

	public Controlcenter() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName1() {
		return this.name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

}