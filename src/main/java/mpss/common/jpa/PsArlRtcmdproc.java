package mpss.common.jpa;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement
public class PsArlRtcmdproc implements Serializable {

	private static final long serialVersionUID = 1L;
    private Arl arl;
  // child eclipses
	@XmlElement(required = true)	
	private List<Rtcmdprocrequest> rtcmdprocrequests;
	
	
	public PsArlRtcmdproc() {
	}

	public PsArlRtcmdproc(Arl arl) {
		this.arl = arl;
	}

  // Arl
	public Arl getArl() {
		return this.arl;
	}

	public void setArl(Arl arl) {
		this.arl = arl;
	}

  // Eclipses
  public List<Rtcmdprocrequest> getRtcmdprocrequests() {
  	    return this.rtcmdprocrequests;
  }

  public void setRtcmdprocrequests(List<Rtcmdprocrequest> rtcmdprocrequests) {
  	    this.rtcmdprocrequests = rtcmdprocrequests;
  }

}