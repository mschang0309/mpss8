package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.sql.Timestamp;


/**
 * The persistent class for the passplan database table.
 * 
 */
@Entity
@XmlRootElement
public class Passplan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PASSPLAN_ID_GENERATOR", sequenceName="PASSPLAN_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PASSPLAN_ID_GENERATOR")
	private Long id;

	private Integer sessionid;

	private String sessionname;

	private Integer passno;

	private String passtype;

	private Timestamp starttime;

	private Timestamp stoptime;

	private Integer siteid;

	private String sitename;

	private Integer check16;


	public Passplan() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

	public Timestamp getStoptime() {
		return this.stoptime;
	}

	public void setStoptime(Timestamp stoptime) {
		this.stoptime = stoptime;
	}

	public String getSessionname() {
		return this.sessionname;
	}

	public void setSessionname(String sessionname) {
		this.sessionname = sessionname;
	}

	public String getPasstype() {
		return this.passtype;
	}

	public void setPasstype(String passtype) {
		this.passtype = passtype;
	}

	public String getSitename() {
		return this.sitename;
	}

	public void setSitename(String sitename) {
		this.sitename = sitename;
	}

	public Integer getSessionid() {
		return this.sessionid;
	}

	public void setSessionid(Integer sessionid) {
		this.sessionid = sessionid;
	}

	public Integer getPassno() {
		return this.passno;
	}

	public void setPassno(Integer passno) {
		this.passno = passno;
	}

	public Integer getSiteid() {
		return this.siteid;
	}

	public void setSiteid(Integer siteid) {
		this.siteid = siteid;
	}

	public Integer getCheck16() {
		return this.check16;
	}

	public void setCheck16(Integer check16) {
		this.check16 = check16;
	}


}