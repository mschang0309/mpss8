package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the idtable database table.
 * 
 */
@Entity
public class Idtable implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="IDTABLE_NAME_GENERATOR", sequenceName="IDTABLE_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="IDTABLE_NAME_GENERATOR")
	private String name;

	private Integer lastnumber;

	public Idtable() {
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLastnumber() {
		return this.lastnumber;
	}

	public void setLastnumber(Integer lastnumber) {
		this.lastnumber = lastnumber;
	}

}