package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the crewprofile database table.
 * 
 */
@Entity
public class Crewprofile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CREWPROFILE_ID_GENERATOR", sequenceName="CREWPROFILE_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CREWPROFILE_ID_GENERATOR")
	private Long id;

	private Integer crewid;

	private Integer day;

	private Integer hour;

	private Integer numcrews;

	public Crewprofile() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCrewid() {
		return this.crewid;
	}

	public void setCrewid(Integer crewid) {
		this.crewid = crewid;
	}

	public Integer getDay() {
		return this.day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getHour() {
		return this.hour;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

	public Integer getNumcrews() {
		return this.numcrews;
	}

	public void setNumcrews(Integer numcrews) {
		this.numcrews = numcrews;
	}

}