package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the arl database table.
 * 
 */
@Entity
@XmlRootElement
public class Arl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ARL_ID_GENERATOR", sequenceName="ARL_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ARL_ID_GENERATOR")
	private Long id;

	private String arltype;

	private String name;

	private Integer shadow;

	public Arl() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getArltype() {
		return this.arltype;
	}

	public void setArltype(String arltype) {
		this.arltype = arltype;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getShadow() {
		return this.shadow;
	}

	public void setShadow(Integer shadow) {
		this.shadow = shadow;
	}

}