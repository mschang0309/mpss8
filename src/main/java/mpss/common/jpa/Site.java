package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * The persistent class for the site database table.
 * 
 */
@Entity
@XmlRootElement
public class Site implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SITE_ID_GENERATOR", sequenceName="SITE_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SITE_ID_GENERATOR")
	private Long id;

	private String antennasideid;

	private String ccsname;

	private String ccstype;

	private Integer isrts;

	private String name;

	private String name1;

	private String name2;

	private String name4;

	private Integer sitenumber;

	private String sitetype;

	private String usnname;
	
	public Site() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAntennasideid() {
		return this.antennasideid;
	}

	public void setAntennasideid(String antennasideid) {
		this.antennasideid = antennasideid;
	}

	public String getCcsname() {
		return this.ccsname;
	}

	public void setCcsname(String ccsname) {
		this.ccsname = ccsname;
	}

	public String getCcstype() {
		return this.ccstype;
	}

	public void setCcstype(String ccstype) {
		this.ccstype = ccstype;
	}

	public Integer getIsrts() {
		return this.isrts;
	}

	public void setIsrts(Integer isrts) {
		this.isrts = isrts;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName1() {
		return this.name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return this.name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getName4() {
		return this.name4;
	}

	public void setName4(String name4) {
		this.name4 = name4;
	}

	public Integer getSitenumber() {
		return this.sitenumber;
	}

	public void setSitenumber(Integer sitenumber) {
		this.sitenumber = sitenumber;
	}

	public String getSitetype() {
		return this.sitetype;
	}

	public void setSitetype(String sitetype) {
		this.sitetype = sitetype;
	}

	public String getUsnname() {
		return this.usnname;
	}

	public void setUsnname(String usnname) {
		this.usnname = usnname;
	}
}