package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlSchemaType;

import java.sql.Timestamp;


/**
 * The persistent class for the rev database table.
 * 
 */
@Entity
@XmlRootElement
public class Rev implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="REV_ID_GENERATOR", sequenceName="REV_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="REV_ID_GENERATOR")
	private Long id;

	@XmlSchemaType(name = "date")
	private Timestamp antime;

	private Integer branchid;

	private Timestamp dntime;

	private Timestamp revendtime;

	private Integer revno;

	private Integer scid;

	public Rev() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getAntime() {
		return this.antime;
	}

	public void setAntime(Timestamp antime) {
		this.antime = antime;
	}

	public Integer getBranchid() {
		return this.branchid;
	}

	public void setBranchid(Integer branchid) {
		this.branchid = branchid;
	}

	public Timestamp getDntime() {
		return this.dntime;
	}

	public void setDntime(Timestamp dntime) {
		this.dntime = dntime;
	}

	public Timestamp getRevendtime() {
		return this.revendtime;
	}

	public void setRevendtime(Timestamp revendtime) {
		this.revendtime = revendtime;
	}

	public Integer getRevno() {
		return this.revno;
	}

	public void setRevno(Integer revno) {
		this.revno = revno;
	}

	public Integer getScid() {
		return this.scid;
	}

	public void setScid(Integer scid) {
		this.scid = scid;
	}

}