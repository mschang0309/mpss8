package mpss.common.jpa;

//import java.sql.Timestamp;
import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the maneuverrequests database table.
 * 
 */
@Entity
@XmlRootElement
@Table(name="maneuverrequests")
public class Maneuverrequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MANEUVERREQUESTS_ID_GENERATOR", sequenceName="MANEUVERREQUESTS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MANEUVERREQUESTS_ID_GENERATOR")
	private Long id;

	private Integer arlid;

	private String azimuth;
	//private float azimuth;

	private String comments;

	private Integer day;

	private Integer duration;

	private String elevation;
	//private float elevation;

	private float maneuverduration;

	private String maneuverproc;

	private Integer orbit;

	private String orbitmanplan;

	private float q1;

	private float q2;

	private float q3;

	private float q4;
	
	private float r_rate;
	
	private float p_rate;
	
	private float y_rate;
	
	private Integer asyncflag;

	private Integer starttime;
	//private Timestamp starttime;

	private String station;

	private String type;

	private Integer year;
	
	private String ratefit1_1="0" ;
	private String ratefit1_2="0" ;
	private String ratefit1_3="0" ;
	private String ratefit2_1="0" ;
	private String ratefit2_2="0" ;
	private String ratefit2_3="0" ;
	private String ratefit3_1="0" ;
	private String ratefit3_2="0" ;
	private String ratefit3_3="0" ;

	public Maneuverrequest() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getArlid() {
		return this.arlid;
	}

	public void setArlid(Integer arlid) {
		this.arlid = arlid;
	}

	public String getAzimuth() {
		return this.azimuth;
	}
  
	public void setAzimuth(String azimuth) {
		this.azimuth = azimuth;
	}

//	public float getAzimuth() {
//		return this.azimuth;
//	}
//
//	public void setAzimuth(float azimuth) {
//		this.azimuth = azimuth;
//	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getDay() {
		return this.day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getElevation() {
		return this.elevation;
	}
  
	public void setElevation(String elevation) {
		this.elevation = elevation;
	}
  
//	public float getElevation() {
//		return this.elevation;
//	}
//
//	public void setElevation(float elevation) {
//		this.elevation = elevation;
//	}

	public float getManeuverduration() {
		return this.maneuverduration;
	}

	public void setManeuverduration(float maneuverduration) {
		this.maneuverduration = maneuverduration;
	}

	public String getManeuverproc() {
		return this.maneuverproc;
	}

	public void setManeuverproc(String maneuverproc) {
		this.maneuverproc = maneuverproc;
	}

	public Integer getOrbit() {
		return this.orbit;
	}

	public void setOrbit(Integer orbit) {
		this.orbit = orbit;
	}

	public String getOrbitmanplan() {
		return this.orbitmanplan;
	}

	public void setOrbitmanplan(String orbitmanplan) {
		this.orbitmanplan = orbitmanplan;
	}

	public float getQ1() {
		return this.q1;
	}

	public void setQ1(float q1) {
		this.q1 = q1;
	}

	public float getQ2() {
		return this.q2;
	}

	public void setQ2(float q2) {
		this.q2 = q2;
	}

	public float getQ3() {
		return this.q3;
	}

	public void setQ3(float q3) {
		this.q3 = q3;
	}

	public float getQ4() {
		return this.q4;
	}

	public void setQ4(float q4) {
		this.q4 = q4;
	}

	public float getRRate() {
		return this.r_rate;
	}

	public void setRRate(float r_rate) {
		this.r_rate = r_rate;
	}

	public float getPRate() {
		return this.p_rate;
	}

	public void setPRate(float p_rate) {
		this.p_rate = p_rate;
	}
	
	public float getYRate() {
		return this.y_rate;
	}

	public void setYRate(float y_rate) {
		this.y_rate = y_rate;
	}
	
	public Integer getAsyncflag() {
		return this.asyncflag;
	}

	public void setAsyncflag(Integer asyncflag) {
		this.asyncflag = asyncflag;
	}
	
	public Integer getStarttime() {
		return this.starttime;
	}
  
	public void setStarttime(Integer starttime) {
		this.starttime = starttime;
	}

	//public Timestamp getStarttime() {
	//	return this.starttime;
	//}
  //
	//public void setStarttime(Timestamp starttime) {
	//	this.starttime = starttime;
	//}
	public String getStation() {
		return this.station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getYear() {
		return this.year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	public String getRatefit1_1() {
		return ratefit1_1;
	}

	public void setRatefit1_1(String ratefit1_1) {
		this.ratefit1_1 = ratefit1_1;
	}

	public String getRatefit1_2() {
		return ratefit1_2;
	}

	public void setRatefit1_2(String ratefit1_2) {
		this.ratefit1_2 = ratefit1_2;
	}

	public String getRatefit1_3() {
		return ratefit1_3;
	}

	public void setRatefit1_3(String ratefit1_3) {
		this.ratefit1_3 = ratefit1_3;
	}

	public String getRatefit2_1() {
		return ratefit2_1;
	}

	public void setRatefit2_1(String ratefit2_1) {
		this.ratefit2_1 = ratefit2_1;
	}

	public String getRatefit2_2() {
		return ratefit2_2;
	}

	public void setRatefit2_2(String ratefit2_2) {
		this.ratefit2_2 = ratefit2_2;
	}

	public String getRatefit2_3() {
		return ratefit2_3;
	}

	public void setRatefit2_3(String ratefit2_3) {
		this.ratefit2_3 = ratefit2_3;
	}

	public String getRatefit3_1() {
		return ratefit3_1;
	}

	public void setRatefit3_1(String ratefit3_1) {
		this.ratefit3_1 = ratefit3_1;
	}

	public String getRatefit3_2() {
		return ratefit3_2;
	}

	public void setRatefit3_2(String ratefit3_2) {
		this.ratefit3_2 = ratefit3_2;
	}

	public String getRatefit3_3() {
		return ratefit3_3;
	}

	public void setRatefit3_3(String ratefit3_3) {
		this.ratefit3_3 = ratefit3_3;
	}

}