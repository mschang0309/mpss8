package mpss.common.jpa;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement
public class PsArlXcross implements Serializable {
	private static final long serialVersionUID = 1L;
    private Arl arl;
  // child eclipses
	@XmlElement(required = true)	
	private List<Xcrossrequest> xcrossrequests;
	
	
	public PsArlXcross() {
	}

	public PsArlXcross(Arl arl) {
		  this.arl = arl;
	}

  // Arl
	public Arl getArl() {
		  return this.arl;
	}

	public void setArl(Arl arl) {
		this.arl = arl;
	}

  // Eclipses
  public List<Xcrossrequest> getXcrossrequests() {
      return this.xcrossrequests;
  }

  public void setXcrossrequests(List<Xcrossrequest> xcrossrequests) {
  	  this.xcrossrequests = xcrossrequests;
  }

}