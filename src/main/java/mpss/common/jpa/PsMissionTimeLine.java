package mpss.common.jpa;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement
public class PsMissionTimeLine implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
  private Arl arl;
	@XmlElement(required = true)	
	private List<Maneuverrequest> maneuverrequests;
	@XmlElement(required = true)	
	private List<Rsiimagingrequest> rsiimagingrequests;
	
	
	public PsMissionTimeLine() {
	}

	public PsMissionTimeLine(Arl arl) {
		this.arl = arl;
	}

  // Arl
	public Arl getArl() {
		return this.arl;
	}

	public void setArl(Arl arl) {
		this.arl = arl;
	}

  // Maneuver requests
  public List<Maneuverrequest> getManeuverrequests() {
  	    return this.maneuverrequests;
  }

  public void setManeuverrequests(List<Maneuverrequest> maneuverrequests) {
  	    this.maneuverrequests = maneuverrequests;
  }

  // Rsi imaging requests
  public List<Rsiimagingrequest> getRsiimagingrequests() {
  	    return this.rsiimagingrequests;
  }

  public void setRsiimagingrequests(List<Rsiimagingrequest> rsiimagingrequests) {
  	    this.rsiimagingrequests = rsiimagingrequests;
  }
}