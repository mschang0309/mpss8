package mpss.common.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The persistent class for the SESSIONSECTION database table.
 * 
 */
@Entity
@XmlRootElement
public class SessionSection implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SESSIONSECTION_ID_GENERATOR", sequenceName="SESSIONSECTION_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SESSIONSECTION_ID_GENERATOR")
	private Long id;

	private String sectionname;

	private Timestamp starttime;
	
	private Timestamp stoptime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSectionname() {
		return sectionname;
	}

	public void setSectionname(String sectionname) {
		this.sectionname = sectionname;
	}

	public Timestamp getStarttime() {
		return starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

	public Timestamp getStoptime() {
		return stoptime;
	}

	public void setStoptime(Timestamp stoptime) {
		this.stoptime = stoptime;
	}
	
	
	
}