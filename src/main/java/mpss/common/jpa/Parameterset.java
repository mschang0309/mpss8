package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the parameterset database table.
 * 
 */
@Entity
@XmlRootElement
public class Parameterset implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PARAMETERSET_ID_GENERATOR", sequenceName="PARAMETERSET_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARAMETERSET_ID_GENERATOR")
	private Long id;

	private String comments;

	private String fromps;

	private int hasbeencopied;

	private String name;

	public Parameterset() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getFromps() {
		return this.fromps;
	}

	public void setFromps(String fromps) {
		this.fromps = fromps;
	}

	public int getHasbeencopied() {
		return this.hasbeencopied;
	}

	public void setHasbeencopied(int hasbeencopied) {
		this.hasbeencopied = hasbeencopied;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}