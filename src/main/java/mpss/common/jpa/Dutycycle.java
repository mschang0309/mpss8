package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the dutycycle database table.
 * 
 */
@Entity
@XmlRootElement
public class Dutycycle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DUTYCYCLE_ID_GENERATOR", sequenceName="DUTYCYCLE_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DUTYCYCLE_ID_GENERATOR")
	private Long id;

	private String activitysubtype;

	private String activitytype;

	private Integer dutycycles;

	private Integer satpsid;

	public Dutycycle() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActivitysubtype() {
		return this.activitysubtype;
	}

	public void setActivitysubtype(String activitysubtype) {
		this.activitysubtype = activitysubtype;
	}

	public String getActivitytype() {
		return this.activitytype;
	}

	public void setActivitytype(String activitytype) {
		this.activitytype = activitytype;
	}

	public Integer getDutycycles() {
		return this.dutycycles;
	}

	public void setDutycycles(Integer dutycycles) {
		this.dutycycles = dutycycles;
	}

	public Integer getSatpsid() {
		return this.satpsid;
	}

	public void setSatpsid(Integer satpsid) {
		this.satpsid = satpsid;
	}

}