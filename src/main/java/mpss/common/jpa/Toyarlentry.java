package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the toyarlentries database table.
 * 
 */
@Entity
@Table(name="toyarlentries")
public class Toyarlentry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TOYARLENTRIES_ID_GENERATOR", sequenceName="TOYARLENTRIES_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TOYARLENTRIES_ID_GENERATOR")
	private Long id;

	private Integer arlid;

	private String comments;

	private Integer duration;

	private Timestamp endtime;

	private Integer rplus;

	private Timestamp starttime;

	private Integer toytype;

	public Toyarlentry() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getArlid() {
		return this.arlid;
	}

	public void setArlid(Integer arlid) {
		this.arlid = arlid;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Timestamp getEndtime() {
		return this.endtime;
	}

	public void setEndtime(Timestamp endtime) {
		this.endtime = endtime;
	}

	public Integer getRplus() {
		return this.rplus;
	}

	public void setRplus(Integer rplus) {
		this.rplus = rplus;
	}

	public Timestamp getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

	public Integer getToytype() {
		return this.toytype;
	}

	public void setToytype(Integer toytype) {
		this.toytype = toytype;
	}

}