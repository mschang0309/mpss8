package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the compressionratio database table.
 * 
 */
@Entity
@XmlRootElement
public class Compressionratio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="COMPRESSIONRATIO_ID_GENERATOR", sequenceName="COMPRESSIONRATIO_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="COMPRESSIONRATIO_ID_GENERATOR")
	private Long id;

	private Integer imagingmodeid;

	private float ratio;

	private Integer rs2ssrpsid;

	public Compressionratio() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getImagingmodeid() {
		return this.imagingmodeid;
	}

	public void setImagingmodeid(Integer imagingmodeid) {
		this.imagingmodeid = imagingmodeid;
	}

	public float getRatio() {
		return this.ratio;
	}

	public void setRatio(float ratio) {
		this.ratio = ratio;
	}

	public Integer getRs2ssrpsid() {
		return this.rs2ssrpsid;
	}

	public void setRs2ssrpsid(Integer rs2ssrpsid) {
		this.rs2ssrpsid = rs2ssrpsid;
	}

}