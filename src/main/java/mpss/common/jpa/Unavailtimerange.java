package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.sql.Timestamp;


/**
 * The persistent class for the unavailtimerange database table.
 * 
 */
@Entity
@XmlRootElement
public class Unavailtimerange implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="UNAVAILTIMERANGE_ID_GENERATOR", sequenceName="UNAVAILTIMERANGE_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="UNAVAILTIMERANGE_ID_GENERATOR")
	private Long id;

	private Integer branchid;

	private Timestamp endtime;

	private Integer numdata;

	private Integer parentid;

	private Timestamp starttime;

	private String uttype;

	public Unavailtimerange() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBranchid() {
		return this.branchid;
	}

	public void setBranchid(Integer branchid) {
		this.branchid = branchid;
	}

	public Timestamp getEndtime() {
		return this.endtime;
	}

	public void setEndtime(Timestamp endtime) {
		this.endtime = endtime;
	}

	public Integer getNumdata() {
		return this.numdata;
	}

	public void setNumdata(Integer numdata) {
		this.numdata = numdata;
	}

	public Integer getParentid() {
		return this.parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public Timestamp getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

	public String getUttype() {
		return this.uttype;
	}

	public void setUttype(String uttype) {
		this.uttype = uttype;
	}

}