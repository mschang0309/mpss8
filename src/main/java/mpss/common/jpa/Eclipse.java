package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.sql.Timestamp;


/**
 * The persistent class for the eclipse database table.
 * 
 */
@Entity
@XmlRootElement
public class Eclipse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ECLIPSE_ID_GENERATOR", sequenceName="ECLIPSE_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ECLIPSE_ID_GENERATOR")
	private Long id;

	private Integer branchid;

	private String eclipsetype;

	private Timestamp penumbraentrancetime;

	private Timestamp penumbraexittime;

	private Integer revid;

	private Integer satelliteid;

	private Timestamp umbraentrancetime;

	private Timestamp umbraexittime;

	public Eclipse() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBranchid() {
		return this.branchid;
	}

	public void setBranchid(Integer branchid) {
		this.branchid = branchid;
	}

	public String getEclipsetype() {
		return this.eclipsetype;
	}

	public void setEclipsetype(String eclipsetype) {
		this.eclipsetype = eclipsetype;
	}

	public Timestamp getPenumbraentrancetime() {
		return this.penumbraentrancetime;
	}

	public void setPenumbraentrancetime(Timestamp penumbraentrancetime) {
		this.penumbraentrancetime = penumbraentrancetime;
	}

	public Timestamp getPenumbraexittime() {
		return this.penumbraexittime;
	}

	public void setPenumbraexittime(Timestamp penumbraexittime) {
		this.penumbraexittime = penumbraexittime;
	}

	public Integer getRevid() {
		return this.revid;
	}

	public void setRevid(Integer revid) {
		this.revid = revid;
	}

	public Integer getSatelliteid() {
		return this.satelliteid;
	}

	public void setSatelliteid(Integer satelliteid) {
		this.satelliteid = satelliteid;
	}

	public Timestamp getUmbraentrancetime() {
		return this.umbraentrancetime;
	}

	public void setUmbraentrancetime(Timestamp umbraentrancetime) {
		this.umbraentrancetime = umbraentrancetime;
	}

	public Timestamp getUmbraexittime() {
		return this.umbraexittime;
	}

	public void setUmbraexittime(Timestamp umbraexittime) {
		this.umbraexittime = umbraexittime;
	}

}