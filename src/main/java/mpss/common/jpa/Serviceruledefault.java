package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the serviceruledefaults database table.
 * 
 */
@Entity
@Table(name="serviceruledefaults")
public class Serviceruledefault implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SERVICERULEDEFAULTS_ID_GENERATOR", sequenceName="SERVICERULEDEFAULTS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SERVICERULEDEFAULTS_ID_GENERATOR")
	private Long id;

	private String servicerule;

	private String servicetyperunning;

	private String servicetypewantstorun;

	public Serviceruledefault() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServicerule() {
		return this.servicerule;
	}

	public void setServicerule(String servicerule) {
		this.servicerule = servicerule;
	}

	public String getServicetyperunning() {
		return this.servicetyperunning;
	}

	public void setServicetyperunning(String servicetyperunning) {
		this.servicetyperunning = servicetyperunning;
	}

	public String getServicetypewantstorun() {
		return this.servicetypewantstorun;
	}

	public void setServicetypewantstorun(String servicetypewantstorun) {
		this.servicetypewantstorun = servicetypewantstorun;
	}

}