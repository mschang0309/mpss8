package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the acqsumfiles database table.
 * 
 */
@Entity
@Table(name="acqsumfiles")
public class Acqsumfile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACQSUMFILES_ID_GENERATOR", sequenceName="ACQSUMFILES_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACQSUMFILES_ID_GENERATOR")
	private Long id;

	private String branch;

	private String filename;

	private Timestamp starttime;

	private Timestamp stop;
	
	private int startrevid;
	
	private int endrevid;

	public Acqsumfile() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBranch() {
		return this.branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getFilename() {
		return this.filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Timestamp getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

	public Timestamp getStop() {
		return this.stop;
	}

	public void setStop(Timestamp stop) {
		this.stop = stop;
	}

	public int getStartrevid() {
		return this.startrevid;
	}

	public void setStartrevid(int startrevid) {
		this.startrevid = startrevid;
	}

	public int getEndrevid() {
		return this.endrevid;
	}

	public void setEndrevid(int endrevid) {
		this.endrevid = endrevid;
	}
}