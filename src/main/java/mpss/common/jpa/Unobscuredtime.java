package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.sql.Timestamp;


/**
 * The persistent class for the unobscuredtime database table.
 * 
 */
@Entity
@XmlRootElement
public class Unobscuredtime implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="UNOBSCUREDTIME_ID_GENERATOR", sequenceName="UNOBSCUREDTIME_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="UNOBSCUREDTIME_ID_GENERATOR")
	private Long id;

	private Integer contactid;

	private Timestamp unobscuredbegintime;

	private Timestamp unobscuredendtime;

	public Unobscuredtime() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getContactid() {
		return this.contactid;
	}

	public void setContactid(Integer contactid) {
		this.contactid = contactid;
	}

	public Timestamp getUnobscuredbegintime() {
		return this.unobscuredbegintime;
	}

	public void setUnobscuredbegintime(Timestamp unobscuredbegintime) {
		this.unobscuredbegintime = unobscuredbegintime;
	}

	public Timestamp getUnobscuredendtime() {
		return this.unobscuredendtime;
	}

	public void setUnobscuredendtime(Timestamp unobscuredendtime) {
		this.unobscuredendtime = unobscuredendtime;
	}

}