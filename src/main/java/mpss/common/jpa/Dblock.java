package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the dblock database table.
 * 
 */
@Entity
public class Dblock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DBLOCK_APPLOCK_GENERATOR", sequenceName="DBLOCK_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DBLOCK_APPLOCK_GENERATOR")
	private Integer applock;

	public Dblock() {
	}

	public Integer getApplock() {
		return this.applock;
	}

	public void setApplock(Integer applock) {
		this.applock = applock;
	}

}