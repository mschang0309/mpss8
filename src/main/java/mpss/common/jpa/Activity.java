package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the activity database table.
 * 
 */
@Entity
public class Activity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ACTIVITY_ID_GENERATOR", sequenceName="ACTIVITY_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ACTIVITY_ID_GENERATOR")
	private Long id;

	private String actsubtype;

	private Integer arlentryid;

	private String arlentrytype;

	private Integer duration;

	private Timestamp endtime;

	private Integer extentid;

	private String info;

	private String name;

	private Integer nextactid;

	private Integer prevactid;

	private Integer satelliteid;

	private Integer scheduled;

	private Integer sessionid;

	private Timestamp starttime;

	private String toytype;

	private Integer usermodified;

	private Integer valid;

	public Activity() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActsubtype() {
		return this.actsubtype;
	}

	public void setActsubtype(String actsubtype) {
		this.actsubtype = actsubtype;
	}

	public Integer getArlentryid() {
		return this.arlentryid;
	}

	public void setArlentryid(Integer arlentryid) {
		this.arlentryid = arlentryid;
	}

	public String getArlentrytype() {
		return this.arlentrytype;
	}

	public void setArlentrytype(String arlentrytype) {
		this.arlentrytype = arlentrytype;
	}

	public Integer getDuration() {
		return this.duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Timestamp getEndtime() {
		return this.endtime;
	}

	public void setEndtime(Timestamp endtime) {
		this.endtime = endtime;
	}

	public Integer getExtentid() {
		return this.extentid;
	}

	public void setExtentid(Integer extentid) {
		this.extentid = extentid;
	}

	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNextactid() {
		return this.nextactid;
	}

	public void setNextactid(Integer nextactid) {
		this.nextactid = nextactid;
	}

	public Integer getPrevactid() {
		return this.prevactid;
	}

	public void setPrevactid(Integer prevactid) {
		this.prevactid = prevactid;
	}

	public Integer getSatelliteid() {
		return this.satelliteid;
	}

	public void setSatelliteid(Integer satelliteid) {
		this.satelliteid = satelliteid;
	}

	public Integer getScheduled() {
		return this.scheduled;
	}

	public void setScheduled(Integer scheduled) {
		this.scheduled = scheduled;
	}

	public Integer getSessionid() {
		return this.sessionid;
	}

	public void setSessionid(Integer sessionid) {
		this.sessionid = sessionid;
	}

	public Timestamp getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Timestamp starttime) {
		this.starttime = starttime;
	}

	public String getToytype() {
		return this.toytype;
	}

	public void setToytype(String toytype) {
		this.toytype = toytype;
	}

	public Integer getUsermodified() {
		return this.usermodified;
	}

	public void setUsermodified(Integer usermodified) {
		this.usermodified = usermodified;
	}

	public Integer getValid() {
		return this.valid;
	}

	public void setValid(Integer valid) {
		this.valid = valid;
	}

}