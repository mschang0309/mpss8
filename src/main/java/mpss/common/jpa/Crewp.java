package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the crewps database table.
 * 
 */
@Entity
@Table(name="crewps")
public class Crewp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CREWPS_ID_GENERATOR", sequenceName="CREWPS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CREWPS_ID_GENERATOR")
	private Long id;

	private Integer crewid;

	private Integer psid;

	private Integer turnarounddifferentsite;

	private Integer turnaroundsamesite;

	public Crewp() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCrewid() {
		return this.crewid;
	}

	public void setCrewid(Integer crewid) {
		this.crewid = crewid;
	}

	public Integer getPsid() {
		return this.psid;
	}

	public void setPsid(Integer psid) {
		this.psid = psid;
	}

	public Integer getTurnarounddifferentsite() {
		return this.turnarounddifferentsite;
	}

	public void setTurnarounddifferentsite(Integer turnarounddifferentsite) {
		this.turnarounddifferentsite = turnarounddifferentsite;
	}

	public Integer getTurnaroundsamesite() {
		return this.turnaroundsamesite;
	}

	public void setTurnaroundsamesite(Integer turnaroundsamesite) {
		this.turnaroundsamesite = turnaroundsamesite;
	}

}