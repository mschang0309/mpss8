package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the parametersps database table.
 * 
 */
@Entity
@XmlRootElement
@Table(name="parametersps")
public class Parametersp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PARAMETERSPS_ID_GENERATOR", sequenceName="PARAMETERSPS_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PARAMETERSPS_ID_GENERATOR")
	private Long id;

	private Integer adjacentsdsgap;

	private Integer alignplaybacks;

	private Integer commandpulselength;

	private Integer ephemerisprotectionfactor;

	private Integer mincoldduration;

	private Integer mincoldmaxel;

	private Integer minimumpasslength;

	private Integer parametersid;

	private String passcleanup;

	private String passsetup;

	private Integer psid;

	public Parametersp() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAdjacentsdsgap() {
		return this.adjacentsdsgap;
	}

	public void setAdjacentsdsgap(Integer adjacentsdsgap) {
		this.adjacentsdsgap = adjacentsdsgap;
	}

	public Integer getAlignplaybacks() {
		return this.alignplaybacks;
	}

	public void setAlignplaybacks(Integer alignplaybacks) {
		this.alignplaybacks = alignplaybacks;
	}

	public Integer getCommandpulselength() {
		return this.commandpulselength;
	}

	public void setCommandpulselength(Integer commandpulselength) {
		this.commandpulselength = commandpulselength;
	}

	public Integer getEphemerisprotectionfactor() {
		return this.ephemerisprotectionfactor;
	}

	public void setEphemerisprotectionfactor(Integer ephemerisprotectionfactor) {
		this.ephemerisprotectionfactor = ephemerisprotectionfactor;
	}

	public Integer getMincoldduration() {
		return this.mincoldduration;
	}

	public void setMincoldduration(Integer mincoldduration) {
		this.mincoldduration = mincoldduration;
	}

	public Integer getMincoldmaxel() {
		return this.mincoldmaxel;
	}

	public void setMincoldmaxel(Integer mincoldmaxel) {
		this.mincoldmaxel = mincoldmaxel;
	}

	public Integer getMinimumpasslength() {
		return this.minimumpasslength;
	}

	public void setMinimumpasslength(Integer minimumpasslength) {
		this.minimumpasslength = minimumpasslength;
	}

	public Integer getParametersid() {
		return this.parametersid;
	}

	public void setParametersid(Integer parametersid) {
		this.parametersid = parametersid;
	}

	public String getPasscleanup() {
		return this.passcleanup;
	}

	public void setPasscleanup(String passcleanup) {
		this.passcleanup = passcleanup;
	}

	public String getPasssetup() {
		return this.passsetup;
	}

	public void setPasssetup(String passsetup) {
		this.passsetup = passsetup;
	}

	public Integer getPsid() {
		return this.psid;
	}

	public void setPsid(Integer psid) {
		this.psid = psid;
	}

}