package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * The persistent class for the transmitter database table.
 * 
 */
@Entity
public class Transmitter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TRANSMITTER_ID_GENERATOR", sequenceName="TRANSMITTER_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRANSMITTER_ID_GENERATOR")
	private Long id;

	private String name;

	private Integer satelliteid;

  // handcrafted entities
	@ManyToOne
	@JoinColumn(name = "satelliteid", insertable=false, updatable=false)
	private Satellite satellite;
	
	public Transmitter() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSatelliteid() {
		return this.satelliteid;
	}

	public void setSatelliteid(Integer satelliteid) {
		this.satelliteid = satelliteid;
	}

  // handcrafted 
  @XmlInverseReference(mappedBy = "transmitters")
  public Satellite getSatellite() {
  	return this.satellite;
  }


  public void setSatellite(Satellite satellite) {
  	this.satellite = satellite;
  }
}