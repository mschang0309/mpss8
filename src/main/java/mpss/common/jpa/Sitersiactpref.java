package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * The persistent class for the sitersiactpref database table.
 * 
 */
@Entity
@XmlRootElement
public class Sitersiactpref implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SITERSIACTPREF_ID_GENERATOR", sequenceName="SITERSIACTPREF_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SITERSIACTPREF_ID_GENERATOR")
	private Long id;

	private String rsiacttype;

	private Integer satpsid;

	private Integer siteid;

	public Sitersiactpref() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRsiacttype() {
		return this.rsiacttype;
	}

	public void setRsiacttype(String rsiacttype) {
		this.rsiacttype = rsiacttype;
	}

	public Integer getSatpsid() {
		return this.satpsid;
	}

	public void setSatpsid(Integer satpsid) {
		this.satpsid = satpsid;
	}

	public Integer getSiteid() {
		return this.siteid;
	}

	public void setSiteid(Integer siteid) {
		this.siteid = siteid;
	}

}