package mpss.common.jpa;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * The persistent class for the sessionsatxref database table.
 * 
 */
@Entity
@XmlRootElement
public class Sessionsatxref implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SESSIONSATXREF_ID_GENERATOR", sequenceName="SESSIONSATXREF_OID_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SESSIONSATXREF_ID_GENERATOR")
	private Long id;

	private Integer satelliteid;

	private Integer sessionid;

  // handcrafted entities
	@ManyToOne
	@JoinColumn(name = "sessionid", insertable=false, updatable=false)
	private Session session;
	
	public Sessionsatxref() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getSatelliteid() {
		return this.satelliteid;
	}

	public void setSatelliteid(Integer satelliteid) {
		this.satelliteid = satelliteid;
	}

	public Integer getSessionid() {
		return this.sessionid;
	}

	public void setSessionid(Integer sessionid) {
		this.sessionid = sessionid;
	}

  // handcrafted 
  @XmlInverseReference(mappedBy = "satXrefs")
  public Session getSession() {
  	return this.session;
  }


  public void setSession(Session session) {
  	this.session = session;
  }
}