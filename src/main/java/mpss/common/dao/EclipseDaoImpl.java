package mpss.common.dao;


import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Eclipse;
import mpss.util.timeformat.TimeRange;

@Repository("eclipseDao")
@SuppressWarnings("unchecked")
public class EclipseDaoImpl extends GenericDaoImpl<Eclipse> implements EclipseDao {

    public EclipseDaoImpl() {
        super(Eclipse.class);
    }
    
    @Override
	public List<Eclipse> getByBranchNTimerange(int branch,String starttime, String endtime) {    	
    	String q = "select a.* from eclipse a where  a.penumbraentrancetime>='" + starttime + "' and a.penumbraentrancetime<'" + endtime + "' and a.branchid='"+branch+"' order by a.penumbraentrancetime";
    	return entityManager.createNativeQuery(q, Eclipse.class).getResultList();		
	}
    
    public List<Eclipse> getEclipsesByRevid(int revid){
    	String q = "select e from Eclipse e where e.revid = :revid";
      	return entityManager.createQuery(q, Eclipse.class)
      			  .setParameter("revid", revid).getResultList();
    }
    
    public List<Eclipse> getSatNameNTimerange(int branchId,String satName,String type,TimeRange tr) {    	
    	String q = "select a.* from eclipse a where a.branchid = " + branchId + " and eclipsetype = '" + type +
    			"' and a.satelliteid=(select s.id from satellite s where s.name = '" +
    			satName + "') and a.penumbraentrancetime <= '" + tr.second +
    			"' and a.penumbraexittime >= '" + tr.first + "' order by a.penumbraentrancetime";
    	return entityManager.createNativeQuery(q, Eclipse.class).getResultList();		
	}
    
    public Eclipse getByManeuver(int branchId,Date manTime){
		try{
			String q = "select e.* from eclipse e where e.eclipsetype='Earth/Sun' and e.branchid= " + branchId + " and '" +
				manTime + "' between penumbraentrancetime and penumbraexittime order by penumbraentrancetime limit 1";
		  return (Eclipse)entityManager.createNativeQuery(q, Eclipse.class).getSingleResult();
		}catch(NoResultException e){
			logger.info("DutycycleDaoImpl getByManeuver() NoResultException");
			return null;
		}	
    }
    
    public List<Eclipse> getByColumnNSatNRange(String cName,int satId,TimeRange tr,int branchId) { 
    	try{
        	String q = "select e.* from eclipse e where e." + cName + " between '" + tr.first +
        			"' and '" + tr.second + "' and e.satelliteid = " + satId + " and e.branchid = " + branchId;
        	return entityManager.createNativeQuery(q, Eclipse.class).getResultList();
		}catch(Exception ex){
			logger.info("EclipseDaoImpl getByColumnNSatNRange() ex="+ex.getMessage());
			return null;
		}
    }
    
    public List<Eclipse> getByColumnNSatNRangeType(String cName,String type,int satId,TimeRange tr,int branchId) { 
    	try{
        	String q = "select e.* from eclipse e where e." + cName + " between '" + tr.first +
        			"' and '" + tr.second + "' and e.satelliteid = " + satId + " and e.branchid = " + branchId +" and e.eclipsetype='"+type+"' order by e."+cName +" asc";
        	return entityManager.createNativeQuery(q, Eclipse.class).getResultList();
		}catch(Exception ex){
			logger.info("EclipseDaoImpl getByColumnNSatNRange() ex="+ex.getMessage());
			return null;
		}
    }
}
