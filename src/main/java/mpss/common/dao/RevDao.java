package mpss.common.dao;

import java.sql.Timestamp;
import java.util.List;

import mpss.common.jpa.Rev;
import mpss.util.timeformat.TimeRange;

public interface RevDao extends GenericDao<Rev> {

	public List<Rev> getRevbyRange(TimeRange range,int branchid);
	public Rev getMaxRev(int scid,int branchid);
	public Rev getMaxTimeRev(int scid,int branchid, Timestamp ts);
	public Rev getByRevno(int revno,int scid,int branchid);
	public List<Rev> getByBranchIdNTimeRange(int branchId, String trStart, String trEnd);
	public Rev getRevbyID(int id);
	public Rev getRevbyTime(Timestamp time,int branchId);
	public Rev getByStartTime(int branchId, Timestamp trStart);
}
