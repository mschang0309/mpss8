package mpss.common.dao;

import java.util.*;
import mpss.common.jpa.Recorder;

public interface RecorderDao extends GenericDao<Recorder> {

    public List<Recorder> listRecorders(int satelliteid);
    public Recorder getRecorder(int satelliteid, String name);
}
