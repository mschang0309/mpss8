package mpss.common.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Parameterset;


@Repository("parametersetDao")
public class ParametersetDaoImpl extends GenericDaoImpl<Parameterset> implements ParametersetDao {
	
	

	public ParametersetDaoImpl() {
		super(Parameterset.class);
	}

	@Override
	public Parameterset create(Parameterset t) {
		logger.info(" ParametersetDaoImpl.class , return super.create(t); no imp");
		return super.create(t);
		
	}

	@Override
	public Parameterset get(Long id) {
		logger.info(" ParametersetDaoImpl.class , return super.get(id); no imp");
		return super.get(id);
		
	}

	
	@Override
	public boolean update(Parameterset t) throws DataAccessException {
		logger.info(" ParametersetDaoImpl.class , super.update(t); no imp");
		return super.update(t);
		
		
	}

	@Override
	public boolean delete(Parameterset t) throws DataAccessException {
		logger.info(" ParametersetDaoImpl.class , super.delete(t); no imp");
		return super.delete(t);
		
	}

	public Parameterset getByID(long id) {
		try{
			String q = "select s from Parameterset s where s.id = :id";
			return entityManager.createQuery(q, Parameterset.class).setParameter("id", id).getSingleResult();
		}catch(Exception ex){
			logger.info("ParametersetDaoImpl getByID() ex id="+String.valueOf(id));
			return null;
		}
		

	}
}
