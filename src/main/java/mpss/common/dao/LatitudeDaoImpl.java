package mpss.common.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Latitude;
import mpss.util.timeformat.TimeRange;

@Repository("latitudeDao")
@SuppressWarnings("unchecked")
public class LatitudeDaoImpl extends GenericDaoImpl<Latitude> implements LatitudeDao {

    public LatitudeDaoImpl() {
        super(Latitude.class);
    }
    
    @Override
    public List<Latitude> getLatitudesByRevid(int revid){
    	String q = "select e.* from latitude e where e.revid = "+revid;    	
      	return entityManager.createNativeQuery(q, Latitude.class).getResultList();
    }

	@Override
	public Latitude getByStartTime(int branchId, Timestamp trStart) {
		String q = "select a.* from latitude a where a.latitude70time <'" + trStart + "' and a.branchid=" + branchId +" order by a.latitude70time desc limit 1";	
		try{
			return (Latitude) entityManager.createNativeQuery(q, Latitude.class).getSingleResult();
		}catch(Exception ex){
			logger.info("LatitudeDaoImpl getByStartTime() ex="+ex.getMessage());
			return null;
		}
		
	}
	
	@Override
	public List<Latitude> getSatNameNTimerange(int branchId,String satName,TimeRange tr) {    	
    	String q = "select a.* from latitude a where a.branchid = " + branchId + " and  a.satelliteid=(select s.id from satellite s where s.name = '" +
    			satName + "') and a.latitude70time <= '" + tr.second +
    			"' and a.latitude70time >= '" + tr.first + "' order by a.latitude70time";
    	return entityManager.createNativeQuery(q, Latitude.class).getResultList();		
	}
}
