package mpss.common.dao;


import java.util.List;
import mpss.common.jpa.Satellitep;

public interface SatellitepDao extends GenericDao<Satellitep> {
	
	public List<Satellitep> listSatelliteps(int Satellitepid);
	public Satellitep getByPsId(long psId);
	public Satellitep getBySatIdNPsId(int satId, int psId);
}
