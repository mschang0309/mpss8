package mpss.common.dao;

import java.util.List;
import mpss.common.jpa.Recorderp;

public interface RecorderpDao extends GenericDao<Recorderp> {
	
	public List<Recorderp> listRecorderps(int recorderp);
    public Recorderp getRecorderp(int satelliteid, String name);
    public Recorderp getBySessionAndSat(int sessionId, int SatId);

}
