package mpss.common.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Sessionsitexref;

@Repository("sessionsitexrefDao")
public class SessionsitexrefDaoImpl extends GenericDaoImpl<Sessionsitexref> implements SessionsitexrefDao {

    public SessionsitexrefDaoImpl() {
        super(Sessionsitexref.class);
    }

    @Override
	public Sessionsitexref getById(int id) {
    	try{
    		Sessionsitexref site = entityManager.find(Sessionsitexref.class, id);
    		return site;
		}catch(Exception ex){
			logger.info("SessionsitexrefDaoImpl getById() ex="+ex.getMessage());
			return null;
		}
    	
	}

	@Override
	public long getNextId() {
		try{
			return (Long) entityManager.createNativeQuery("select nextval('sessionsitexref_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("SessionsitexrefDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}

	@Override
	public List<Sessionsitexref> getBySessionId(int sessionId) {
		String q = "select s from Sessionsitexref s where s.sessionid = :sessionId ";
		return entityManager.createQuery(q, Sessionsitexref.class).setParameter("sessionId", sessionId).getResultList();
	}
    
}
