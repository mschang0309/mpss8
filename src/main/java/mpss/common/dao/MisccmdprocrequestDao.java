package mpss.common.dao;

import java.util.List;

import mpss.common.jpa.Misccmdprocrequest;

public interface MisccmdprocrequestDao extends GenericDao<Misccmdprocrequest> {

	public List<Misccmdprocrequest> getByArlId(int arlId);
	public Misccmdprocrequest getbyId(int Id);
}
