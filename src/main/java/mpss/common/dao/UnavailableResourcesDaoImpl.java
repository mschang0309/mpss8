package mpss.common.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Date;

import mpss.common.jpa.Unavailtimerange;

@Repository("unavailableResourcesDao")
@SuppressWarnings("unchecked")
public class UnavailableResourcesDaoImpl implements UnavailableResourcesDao {

    private EntityManager entityManager;
    
    public UnavailableResourcesDaoImpl() {
    }
    
    // for Spring injection
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

	  public List<Unavailtimerange> getSitesUnavailableTimes(Date startTime, Date endTime)
	  {
	  	  String uttype = "SITE";
        String q = "select u from Unavailtimerange u where u.uttype = :uttype and " +
    	            "u.starttime >= :starttime and " +
    	            "u.endtime <= :endtime " +
    	            "order by u.starttime";
  	    return entityManager.createQuery(q, Unavailtimerange.class).setParameter("uttype", uttype)
 	                                                    .setParameter("starttime", startTime)
  	                                                    .setParameter("endtime", endTime)
  	                                                    .getResultList();
	  }
	  
	  public List<Unavailtimerange> getSitesUnavailableTimes(Timestamp startTime, Timestamp endTime, int branchId)
	  {
	  	  String uttype = "SITE";
        String q = "select u from Unavailtimerange u where u.uttype = :uttype and " +
    	            "u.endtime >= :starttime and " +
    	            "u.starttime <= :endtime and " +
    	            "u.branchid = :branchId " +
    	            "order by u.starttime";
  	    return entityManager.createQuery(q, Unavailtimerange.class).setParameter("uttype", uttype)
 	                                                    .setParameter("starttime", startTime)
  	                                                    .setParameter("endtime", endTime)
  	                                                    .setParameter("branchId", branchId)
  	                                                    .getResultList();
	  }
	  
	  public List<Unavailtimerange> getSiteUnavailableTimes(int siteid, Date startTime, Date endTime)
	  {
	  	  String uttype = "SITE";
        String q = "select u from Unavailtimerange u where u.uttype = :uttype and " +
    	            "u.parentid = :siteid and " +
    	            "u.starttime >= :starttime and " +
    	            "u.endtime <= :endtime " +
    	            "order by u.starttime";
  	    return entityManager.createQuery(q, Unavailtimerange.class).setParameter("uttype", uttype)
  	                                                    .setParameter("siteid", siteid)
  	                                                    .setParameter("starttime", startTime)
  	                                                    .setParameter("endtime", endTime)
  	                                                    .getResultList();
	  }
	  public List<Unavailtimerange> getSiteUnavailableTimes(String siteName, Date startTime, Date endTime)
	  {
	  	  String uttype = "SITE";
        String q = "select u from Unavailtimerange u where u.uttype = :uttype and " +
    	            "u.parentid = ANY(select s.id from Site s where s.name = :siteName) and " +
    	            "u.starttime >= :starttime and " +
    	            "u.endtime <= :endtime " +
    	            "order by u.starttime";
  	    return entityManager.createQuery(q, Unavailtimerange.class).setParameter("uttype", uttype)
  	                                                    .setParameter("siteName", siteName)
  	                                                    .setParameter("starttime", startTime)
  	                                                    .setParameter("endtime", endTime)
  	                                                    .getResultList();
	  }
	  
	  public boolean isUnavailableTime(int sessionId,String siteName ,Timestamp startTime, Timestamp endTime, int branchId)
	  {
	  	String q = "select * from Unavailtimerange u where u.endtime >= '" + startTime + "' and " +
    	            "u.starttime <= '" + endTime + "' and u.branchid = " + branchId + " and u.uttype = '" +
    	            siteName + "' and u.parentid in (select siteid from sessionsitexref where sessionid= '" +
    	            sessionId + "') " ;
	  	List<Unavailtimerange> unTrs = entityManager.createNativeQuery(q, Unavailtimerange.class).getResultList();
  	    
  	    return (unTrs.size() > 0);
	  }
	  
	  public boolean isUnavailableTimeBySite(int sessionId,int siteId ,Timestamp startTime, Timestamp endTime, int branchId)
	  {
		String siteName = "SITE";
	  	String q = "select * from Unavailtimerange u where u.endtime >= '" + startTime + "' and " +
    	            "u.starttime <= '" + endTime + "' and u.branchid = " + branchId + " and u.uttype = '" +
    	            siteName + "' and u.parentid = " + siteId ;
	  	List<Unavailtimerange> unTrs = entityManager.createNativeQuery(q, Unavailtimerange.class).getResultList();
  	    
  	    return (unTrs.size() > 0);
	  }
	  
}
