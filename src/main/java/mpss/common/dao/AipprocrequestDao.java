package mpss.common.dao;

import java.util.List;

import mpss.common.jpa.Aipprocrequest;

public interface AipprocrequestDao extends GenericDao<Aipprocrequest> {
	public List<Aipprocrequest> getByArlId(int arlId);
	public Aipprocrequest getById(int Id);

}
