package mpss.common.dao;


import mpss.common.jpa.Aipcommand;

public interface AipcommandDao extends GenericDao<Aipcommand> {
	
	public Aipcommand getByCmd(String cmd);
}
