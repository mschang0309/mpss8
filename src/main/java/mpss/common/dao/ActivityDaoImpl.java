package mpss.common.dao;

import java.sql.Timestamp;
import java.util.*;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Activity;
import mpss.schedule.Schedule;
import mpss.schedule.Schedule.SessionType;
import mpss.util.timeformat.TimeRange;

@Repository("activityDao")
@SuppressWarnings("unchecked")
public class ActivityDaoImpl extends GenericDaoImpl<Activity> implements ActivityDao {

    public ActivityDaoImpl() {
        super(Activity.class);
    }

	@Override
	public Activity getActivityById(long id) {
		Activity act = entityManager.find(Activity.class, id);
		return act;
	}

	@Override
	public long getMaxId() {
		try{
			return (Long)entityManager.createQuery("select max(u.id) id from Activity u").getSingleResult();
		}catch(Exception ex){
			logger.info("ActivityDaoImpl getMaxId() ex="+ex.getMessage());  
			return 0;
		}
		
	}

	@Override
	public Collection<Activity> getAssociatedAct(long id) {
		ArrayList<Activity> al = new ArrayList<Activity>();
        Activity act = null;
        try {
            act = this.getActivityById(id);
            String imgMode = act.getInfo();
            //System.out.println("actId=" + act.getId());

            if (imgMode == null || imgMode.equalsIgnoreCase("")) {
                //Delete will be the sequence 4
                int prevActId1 = act.getPrevactid();
                //System.out.println("prevActId1=" + prevActId1);
                Activity voPrevAct1 = this.getActivityById(prevActId1);
                if (voPrevAct1 != null) {
                    al.add(voPrevAct1);

                    int prevActId2 = voPrevAct1.getPrevactid();
                    Activity voPrevAct2 = this.getActivityById(prevActId2);
                    if (voPrevAct2 != null) {
                        al.add(voPrevAct2);

                        int prevActId3 = voPrevAct2.getPrevactid();
                        Activity voPrevAct3 = this.getActivityById(prevActId3);
                        if (voPrevAct3 != null) {
                            al.add(voPrevAct3);
                        }
                    }
                }

            } else if (imgMode.equalsIgnoreCase("PAN+MS")) {
                //PAN+MS will be the sequence 1
                int nextActId1 = act.getNextactid();
                Activity voNextAct1 = this.getActivityById(nextActId1);
                if (voNextAct1 != null) {
                    al.add(voNextAct1);

                    int nextActId2 = voNextAct1.getNextactid();
                    Activity voNextAct2 = this.getActivityById(nextActId2);
                    if (voNextAct2 != null) {
                        al.add(voNextAct2);

                        int nextActId3 = voNextAct2.getNextactid();
                        Activity voNextAct3 = this.getActivityById(nextActId3);
                        if (voNextAct3 != null) {
                            al.add(voNextAct3);
                        }
                    }
                }

            } else if (imgMode.equalsIgnoreCase("MS")) {
                //MS will be the sequence 2
                int prevActId1 = act.getPrevactid();
                Activity voPrevAct1 = this.getActivityById(prevActId1);
                if (voPrevAct1 != null) {
                    al.add(voPrevAct1);
                }

                int nextActId1 = act.getNextactid();
                Activity voNextAct1 = this.getActivityById(nextActId1);
                if (voNextAct1 != null) {
                    al.add(voNextAct1);

                    int nextActId2 = voNextAct1.getNextactid();
                    Activity voNextAct2 = this.getActivityById(nextActId2);
                    if (voNextAct2 != null) {
                        al.add(voNextAct2);
                    }
                }
            } else if (imgMode.equalsIgnoreCase("PAN")) {
                //PAN will be the sequence 3
                int prevActId1 = act.getPrevactid();
                Activity voPrevAct1 = this.getActivityById(prevActId1);
                if (voPrevAct1 != null) {
                    al.add(voPrevAct1);

                    int prevActId2 = voPrevAct1.getPrevactid();
                    Activity voPrevAct2 = this.getActivityById(prevActId2);
                    if (voPrevAct2 != null) {
                        al.add(voPrevAct2);
                    }
                }

                int nextActId1 = act.getNextactid();
                Activity voNextAct1 = this.getActivityById(nextActId1);
                if (voNextAct1 != null) {
                    al.add(voNextAct1);
                }
            }


        } catch (Exception ex) {
        	logger.info("getAssociatedAct error="+ex.getMessage());            
        }
        
		return al;
	}    
	
	public long getNextId() {
		try{
			return (Long)entityManager.createNativeQuery("select nextval('activity_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("ActivityDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
		//return 0 ;
	}

	@Override
	public Collection<Activity> getBySessionId(int sessionid) {
		String q = "select a from Activity a where a.sessionid = :sessionid order by a.starttime";
  	  	return entityManager.createQuery(q, Activity.class).setParameter("sessionid", sessionid).getResultList();
		
	}
	
	@Override
	public Collection<Activity> getBySessionId(int sessionid,boolean schflag) {
		String q = "select a from Activity a where a.sessionid = :sessionid and a.scheduled=0 order by a.id";
  	  	return entityManager.createQuery(q, Activity.class).setParameter("sessionid", sessionid).getResultList();
		
	}

	@Override
	public Collection<Activity> getByEntryIdNType(int arlentryid, String arlentrytype) {
		String q = "select a from Activity a where a.arlentryid = :arlentryid and a.arlentrytype = :arlentrytype order by a.starttime";
  	  	return entityManager.createQuery(q, Activity.class).setParameter("arlentryid", arlentryid).setParameter("arlentrytype", arlentrytype).getResultList();
	}

	@Override
	public Collection<Activity> getBySessionIdNTimerange(int sessionid, String starttime, String endtime) {
		
		if(Schedule.s_type== SessionType.BRANCH){
			if(sessionid!=0){
				String q = "select a.* from Activity a where  sessionid='"+sessionid+"' order by a.starttime";					
				logger.info("act 1 BRANCH sql ="+q);
				return entityManager.createNativeQuery(q, Activity.class).getResultList();
			}else{
				String q = "select a.* from Activity a where  extentid = 0 and sessionid='"+sessionid+"' order by a.starttime";
				logger.info("act 2 BRANCH sql ="+q);
				return entityManager.createNativeQuery(q, Activity.class).getResultList();
			}
		}else{			
			if(sessionid!=0){
				String q = "select a.* from Activity a where  a.endtime>'" + starttime + "' and a.starttime<'" + endtime + "' and extentid <> 0 and sessionid='"+sessionid+"' order by a.starttime";
				logger.info("act 1 sql ="+q);
				//String q = "select a.* from Activity a where  sessionid='"+sessionid+"' order by a.starttime";
				return entityManager.createNativeQuery(q, Activity.class).getResultList();
			}else{
				String q = "select a.* from Activity a where  a.endtime>'" + starttime + "' and a.starttime<'" + endtime + "' and extentid <> 0 order by a.starttime";			
				logger.info("act 2 sql ="+q);
				return entityManager.createNativeQuery(q, Activity.class).getResultList();
			}
		}		
		
	}

	@Override
	public Collection<Activity> getUnschBySessionIdNTimerange(int sessionid, String starttime, String endtime) {
		String q = "select a.* from Activity a where a.sessionid = " + sessionid + " and a.endtime>'" + starttime + "' and a.starttime<'" + endtime + "' and a.scheduled=0 order by a.starttime";
  	  	
		return entityManager.createNativeQuery(q, Activity.class).getResultList();
	}
	
	public List<Activity> getActBySessionIdNStartTime(int sessionid, Timestamp starttime) {
		String q = "select a from Activity a where a.sessionid = :sessionid and a.starttime = :starttime order by a.starttime";
  	  	return entityManager.createQuery(q, Activity.class).setParameter("sessionid", sessionid).setParameter("starttime", starttime).getResultList();
	}
	
	public List<Activity> getByEntryId(int arlentryid) {
		String q = "select a from Activity a where a.arlentryid = :arlentryid";
		try{
			return entityManager.createQuery(q, Activity.class).setParameter("arlentryid", arlentryid).getResultList();
		}catch(Exception ex){
			System.out.println("ActivityDaoImpl getByEntryId() ex");			
			return null;
		}
  	  	
	}
	
	public List<Activity> getUnschByRangeNBranchId(int sessionid, int branchid, TimeRange tr) {
		try{
			if (branchid == 0 )
			{
				String q = "select a.* from activity a,sessions s  where a.sessionid = s.id and a.scheduled = 0 and a.sessionid=" + sessionid + " and a.starttime < '" + tr.second + "' and a.endtime > '" + tr.first + "' and s.isbranch = 0 order by a.starttime";
				return entityManager.createNativeQuery(q, Activity.class).getResultList();
			}
			else
			{
				String q = "select a from Activity a where a.scheduled = 0 and a.starttime < :second and a.endtime > :first and a.sessionid = :branchid order by a.starttime";
				return entityManager.createQuery(q, Activity.class).setParameter("branchid", branchid).setParameter("first", tr.first).setParameter("second", tr.second).getResultList();
			}
		}catch(Exception ex){
			logger.info("ActivityDaoImpl getUnschByRangeNBranchId() ex="+ex.getMessage());
			return null;
		}
	}
	
	public List<Activity> getAIPDownlinkFile(Timestamp sTime, Timestamp eTime) {
		try
		{
			String q = "select a.* from activity a where a.arlentrytype='AIPPROC' and a.info <>''" +
					" and a.starttime between '" + sTime + "' and '" + eTime + "' order by a.starttime";
			return entityManager.createNativeQuery(q, Activity.class).getResultList();
		}catch(Exception ex){
			logger.info("ActivityDaoImpl getAIPDownlinkFile() ex="+ex.getMessage());
			return null;
		}
		
	}
	
	public List<Activity> getAIPBySession(int sessionid) {
		try
		{
			String q = "select a.* from (select * from activity where sessionid = " +
				sessionid + ") a left join aipprocrequests b on a.arlentryid = b.id" + 
				" where b.comments='' union (select * from activity where sessionid = " +
				sessionid + " and arlentrytype = 'AIPPBK') order by id";
			return entityManager.createNativeQuery(q, Activity.class).getResultList();
		}catch(Exception ex){
			logger.info("ActivityDaoImpl getAIPBySession() ex="+ex.getMessage());
			return null;
		}
	}
	
	public List<Activity> getBySessionNType(int sessionid, String arlentrytype) {
		try
		{
			String q = "select a from Activity a where a.sessionid = :sessionid " + 
					"and a.arlentrytype = :arlentrytype order by a.id";		
			TypedQuery<Activity> query =entityManager.createQuery(q, Activity.class);
			query.setParameter("sessionid", sessionid);
			query.setParameter("arlentrytype", arlentrytype);		
		  	return query.getResultList();
		}catch(Exception ex){
			logger.info("ActivityDaoImpl getBySessionNType() ex="+ex.getMessage());
			return null;
		}
	}
	
	public Collection<Activity> getBySessionExtentSchType(int sessionid,int extentid,int scheduled,String arlentrytype) {
		String q = "select a from Activity a where a.sessionid = :sessionid and a.scheduled= :scheduled and a.extentid =:extentid and a.arlentrytype=:arlentrytype  order by a.id";		
		TypedQuery<Activity> query =entityManager.createQuery(q, Activity.class);
		query.setParameter("sessionid", sessionid);
		query.setParameter("scheduled", scheduled);
		query.setParameter("extentid", extentid);
		query.setParameter("arlentrytype", arlentrytype);		
  	  	return query.getResultList();
		
	}
	
	public Collection<Activity> getBySessionSchType(int sessionid,int scheduled,String arlentrytype) {
		String q = "select a from Activity a where a.sessionid = :sessionid and a.scheduled= :scheduled  and a.arlentrytype=:arlentrytype  order by a.id";		
		TypedQuery<Activity> query =entityManager.createQuery(q, Activity.class);
		query.setParameter("sessionid", sessionid);
		query.setParameter("scheduled", scheduled);		
		query.setParameter("arlentrytype", arlentrytype);		
  	  	return query.getResultList();
		
	}
	
	public List<Integer> getExtentIdNotinArlid(String arlid, TimeRange tr) {
		List<Integer> extentids = new ArrayList<Integer>();
		try{			
			// MANEUVER
			String q = "select A.extentid from activity A ,( select id,arlid from maneuverrequests where arlid not in ("+ arlid+") ) E " +
				" where A.arlentrytype <>'MON' and A.extentid > 0 and A.starttime between '" +
				tr.first + "' and '" + tr.second + "' and A.arlentrytype ='MANEUVER' and A.arlentryid=E.id ";
			
			List<Integer> temps =(List<Integer>) entityManager.createNativeQuery(q).getResultList();
			for(Integer temp : temps){
				extentids.add(temp);
			}
			
			// RSI
			q = "select A.extentid from activity A ,( select id,arlid from rsiimagingrequests where arlid not in ("+ arlid+") ) E " +
					" where A.arlentrytype <>'MON' and A.extentid > 0 and A.starttime between '" +
					tr.first + "' and '" + tr.second + "' and A.arlentrytype ='RSI' and A.arlentryid=E.id ";
			
			
			temps =(List<Integer>) entityManager.createNativeQuery(q).getResultList();
			for(Integer temp : temps){
				extentids.add(temp);
			}
			
			//XCROSS
			q = "select A.extentid from activity A ,( select id,arlid from xcrossrequests where arlid not in ("+ arlid+") ) E " +
					" where A.arlentrytype <>'MON' and A.extentid > 0 and A.starttime between '" +
					tr.first + "' and '" + tr.second + "' and A.arlentrytype ='XCROSS' and A.arlentryid=E.id ";
			
			
			temps =(List<Integer>) entityManager.createNativeQuery(q).getResultList();
			for(Integer temp : temps){
				extentids.add(temp);
			}
			
			return extentids;
			
		}catch(Exception ex){
			logger.info("ActivityDaoImpl getExtentIdNotinArlid() ex="+ex.getMessage());
			return extentids;
		}
		
	}
	
	public List<Long> getActivityIdNotinArlid(String arlid, TimeRange tr) {
		List<Long> actids = new ArrayList<Long>();
		try{			
			// MANEUVER
			String q = "select A.id  from activity A ,( select id,arlid from maneuverrequests where arlid not in ("+ arlid+") ) E " +
				" where A.arlentrytype <>'MON' and A.extentid > 0 and A.starttime between '" +
				tr.first + "' and '" + tr.second + "' and A.arlentrytype ='MANEUVER' and A.arlentryid=E.id ";
			
			List<Long> temps =(List<Long>) entityManager.createNativeQuery(q).getResultList();
			for(Long temp : temps){
				actids.add(temp);
			}
			
			// RSI
			q = "select A.id  from activity A ,( select id,arlid from rsiimagingrequests where arlid not in ("+ arlid+") ) E " +
					" where A.arlentrytype <>'MON' and A.extentid > 0 and A.starttime between '" +
					tr.first + "' and '" + tr.second + "' and A.arlentrytype ='RSI' and A.arlentryid=E.id ";
			
			
			temps =(List<Long>) entityManager.createNativeQuery(q).getResultList();
			for(Long temp : temps){
				actids.add(temp);
			}
			
			//XCROSS
			q = "select A.id  from activity A ,( select id,arlid from xcrossrequests where arlid not in ("+ arlid+") ) E " +
					" where A.arlentrytype <>'MON' and A.extentid > 0 and A.starttime between '" +
					tr.first + "' and '" + tr.second + "' and A.arlentrytype ='XCROSS' and A.arlentryid=E.id ";
			
			
			temps =(List<Long>) entityManager.createNativeQuery(q).getResultList();
			for(Long temp : temps){
				actids.add(temp);
			}
			
			return actids;
			
		}catch(Exception ex){
			logger.info("ActivityDaoImpl getActivityIdNotinArlid() ex="+ex.getMessage());
			return actids;
		}
		
	}
	
	public Collection<Activity> getActivityByRange(Timestamp starttime,Timestamp  endtime){
		try{
			String q = "select  a from Activity a where a.scheduled=1 and a.extentid<>0 and a.arlentrytype in('RSI','DELETE') and a.starttime > :starttime and a.starttime < :endtime  order by a.id";		
			TypedQuery<Activity> query =entityManager.createQuery(q, Activity.class);
			query.setParameter("starttime", starttime);		
			query.setParameter("endtime", endtime);
	  	  	return query.getResultList();

		}catch(Exception ex){
			logger.info("ActivityDaoImpl getActivityByRange() ex="+ex.getMessage());
			return null;
		}
		
	}
	
	public Activity getStopRec(Timestamp pbkTime, String filename) {
		try{
			
			String q = "select * from activity where arlentrytype = 'AIPPROC' and starttime < '" +
				pbkTime + "' and info = '" + filename + "' order by starttime desc limit 1";
	        return (Activity) entityManager.createNativeQuery(q, Activity.class).getSingleResult();
		}catch(Exception ex){
			logger.info("ActivityDaoImpl getStopRec() ex="+ex.getMessage());
			return null;
		}		
		
	}
	
	public Collection<Activity> getBySessionIdEndtime(int sessionid ,String endtime){
		String q = "select a.* from Activity a where a.sessionid = " + sessionid + " and  a.starttime<'" + endtime + "' and a.arlentrytype in ('MON','RSI','DELETE','AIPPROC','AIPPBK') order by a.starttime";  	  	
		return entityManager.createNativeQuery(q, Activity.class).getResultList();
	}
	
	public Collection<Activity> getMonActBySessionIdEndtime(int sessionid ,String endtime){
		String q = "select a.* from Activity a where a.sessionid = " + sessionid + " and  a.starttime >'" + endtime + "' and a.arlentrytype in ('MON','AIPPROC','AIPPBK') order by a.starttime";  	  	
		return entityManager.createNativeQuery(q, Activity.class).getResultList();
	}
	
	public Collection<Activity> getMonActForPassPlan(Timestamp starttime,Timestamp endtime){
		String q = "select a.* from Activity a where   a.starttime > '"+starttime.toString()+"' and a.starttime < '"+endtime.toString()+"' and a.arlentrytype in ('MON') and a.scheduled=1 order by a.starttime";  
		return entityManager.createNativeQuery(q, Activity.class).getResultList();
	}
	
	
	public List<Integer> getGroupSessionbyActId(String acts) {
		try{
			String q = "select sessionid from activity where id in ("+acts+") GROUP BY sessionid ";
			return (List<Integer>) entityManager.createNativeQuery(q).getResultList();
			
		}catch(Exception ex){
			logger.info("ActivityDaoImpl getGroupSessionbyActId() ex="+ex.getMessage());
			return null;
		}
		
	}

}
