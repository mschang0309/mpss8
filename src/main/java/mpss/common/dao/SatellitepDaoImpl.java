package mpss.common.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;
import mpss.common.jpa.Satellitep;


@Repository("satellitepDao")
public class SatellitepDaoImpl extends GenericDaoImpl<Satellitep> implements SatellitepDao {

    public SatellitepDaoImpl() {
        super(Satellitep.class);
    }
	
	
    @Override
	public List<Satellitep> listSatelliteps(int Satellitepid) {
    	 String q = "select r from Satellitep r where r.satelliteid = :satelliteid";
      	  return entityManager.createQuery(q, Satellitep.class).setParameter("satelliteid", Satellitepid).getResultList();
   			
	}

	
    
    public Satellitep getByPsId(long psId)
    {
    	try{
    		 String q = "select ps from Satellitep ps where ps.psid = :psid" ;
    		 	
    		    return entityManager.createQuery(q, Satellitep.class).setParameter("psid", psId).getSingleResult();
    	
    	}catch(Exception ex){
    		logger.info("SatellitepDaoImpl getByPsId() ex="+ex.getMessage());
    		return null;
    	}
       	
    	
    }
    
    public Satellitep getBySatIdNPsId(int satId, int psId)
    {
        String q = "select ps from Satellitep ps where ps.psid = :psid and ps.satelliteid = :satId" ;
        try
        {
        	return entityManager.createQuery(q, Satellitep.class).setParameter("psid", psId).setParameter("satId", satId)
	                                                    .getSingleResult();	
		} catch(NoResultException ex) {
			logger.info("SatellitepDaoImpl getBySatIdNPsId() ex="+ex.getMessage());
		    return null;
		}
    }
    
}
