package mpss.common.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Misccmdprocrequest;

@Repository("misccmdprocrequestDao")
public class MisccmdprocrequestDaoImpl extends GenericDaoImpl<Misccmdprocrequest> implements MisccmdprocrequestDao {

    public MisccmdprocrequestDaoImpl() {
        super(Misccmdprocrequest.class);
    }
    
    public List<Misccmdprocrequest> getByArlId(int arlId)
    {
    	try{
    		String q = "select isu from Misccmdprocrequest isu where isu.arlid = :arlid";
    	  	return entityManager.createQuery(q, Misccmdprocrequest.class).setParameter("arlid", arlId).getResultList();
    	}catch(Exception ex){	
    		logger.info("MisccmdprocrequestDaoImpl getByArlId() ex="+ex.getMessage());
			return null;
		}
  	  
    }
        
    public Misccmdprocrequest getbyId(int Id) {
    	try{
    		String q = "select id, arlid, comments, contactid, day, duration, 'offset', offsetnegative" +
    				", revid, time, trig, userproc, year from misccmdprocrequests where id = " + Id;
    		return (Misccmdprocrequest) entityManager.createNativeQuery(q, Misccmdprocrequest.class).getSingleResult();
		}catch(Exception ex){	
			logger.info("MisccmdprocrequestDaoImpl getbyId() ex="+ex.getMessage());
			return null;
		}
    }    
}
