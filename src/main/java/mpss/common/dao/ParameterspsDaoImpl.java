package mpss.common.dao;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Parametersp;


@Repository("parameterspsDao")
public class ParameterspsDaoImpl extends GenericDaoImpl<Parametersp> implements ParameterspsDao {

    public ParameterspsDaoImpl() {
        super(Parametersp.class);
    }    
}
