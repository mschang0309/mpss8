package mpss.common.dao;

import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Dutycycle;


@Repository("dutycycleDao")
public class DutycycleDaoImpl extends GenericDaoImpl<Dutycycle> implements
		DutycycleDao {

	public DutycycleDaoImpl() {
		super(Dutycycle.class);
	}

	@Override
	public List<Dutycycle> getBySatPsId(int satPsId) {
		String q = "select s from Dutycycle s where s.satpsid = :satPsId";

		return entityManager.createQuery(q, Dutycycle.class)
				.setParameter("satPsId", satPsId).getResultList();
	}

	@Override
	public Dutycycle getById(int id) {
		try{
			String q = "select s from Dutycycle s where s.id = :id";
			//String q = "select s from " + Dutycycle.class.getName() + " s where s.id = :id";
			
			//String q = "select s from dutycycle s where s.activitytype= 'ISUALPROC' ";
			
			//Query test = entityManager.createQuery(q);
			//test.setParameter("id", id);
			//return (Dutycycle)test.getSingleResult();
			
			return entityManager.createQuery(q, Dutycycle.class)
					.setParameter("id", id).getSingleResult();
		}catch(Exception ex){
			logger.info("DutycycleDaoImpl getById() ex="+ex.getMessage());
			return null;
		}
		
	}
	
	public Dutycycle getByPsIdNTypeNSubtype(int satPsId,String actType,String actSubType) {
		try
		{
			String q = "select s from Dutycycle s where s.satpsid = :satPsId "
					+ "and s.activitytype = :actType and s.activitysubtype = :actSubType";
	
			return entityManager.createQuery(q, Dutycycle.class)
					.setParameter("satPsId", satPsId)
					.setParameter("actType", actType)
					.setParameter("actSubType", actSubType).getSingleResult();
		} catch(NoResultException e) {
			logger.info("DutycycleDaoImpl getByPsIdNTypeNSubtype() NoResultException");
	    	return null;
	    }
	}

}
