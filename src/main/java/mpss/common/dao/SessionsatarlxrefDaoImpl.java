package mpss.common.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Sessionsatarlxref;

@Repository("sessionsatarlxrefDao")
public class SessionsatarlxrefDaoImpl extends GenericDaoImpl<Sessionsatarlxref> implements SessionsatarlxrefDao {

    public SessionsatarlxrefDaoImpl() {
        super(Sessionsatarlxref.class);
    }

	@Override
	public Sessionsatarlxref getById(long id) {
		try{
			Sessionsatarlxref s = entityManager.find(Sessionsatarlxref.class, id);
			return s;
		}catch(Exception ex){
			logger.info("SessionsatarlxrefDaoImpl getById() ex="+ex.getMessage());
			return null;
		}
		
	}

	@Override
	public long getNextId() {
		try{
			return (Long)entityManager.createNativeQuery("select nextval('sessionsatarlxref_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("SessionsatarlxrefDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}

	@Override
	public List<Sessionsatarlxref> getBySessionId(int sessionId) {
		String q = "select s from Sessionsatarlxref s where s.sessionid = :sessionid";
  	  return entityManager.createQuery(q, Sessionsatarlxref.class).setParameter("sessionid", sessionId).getResultList();
	}
    
}
