package mpss.common.dao;

import java.util.*;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Antenna;


@Repository("antennaDao")
public class AntennaDaoImpl extends GenericDaoImpl<Antenna> implements AntennaDao {

	
    public AntennaDaoImpl() {
        super(Antenna.class);
    }   
    
    public Antenna getAntenna(int satelliteid, String name) {
    	  String q = "select r from Antenna r where r.name = :name and r.satelliteid = :satelliteid";
    	  return entityManager.createQuery(q, Antenna.class).setParameter("satelliteid", satelliteid).setParameter("name", name).getSingleResult();
    }

	@Override
	public List<Antenna> listAntennas(int Antenna) {
		 String q = "select r from Antenna r where r.satelliteid = :satelliteid";
   	  return entityManager.createQuery(q, Antenna.class).setParameter("satelliteid", Antenna).getResultList();
		
	}
}
