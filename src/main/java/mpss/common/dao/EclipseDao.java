package mpss.common.dao;

import java.util.Date;
import java.util.List;
import mpss.common.jpa.Eclipse;
import mpss.util.timeformat.TimeRange;

public interface EclipseDao extends GenericDao<Eclipse> {
	
	public List<Eclipse> getByBranchNTimerange(int branch, String starttime, String endtime);
	public List<Eclipse> getEclipsesByRevid(int revid);
	public List<Eclipse> getSatNameNTimerange(int branchId,String satName,String type,TimeRange tr);
	public Eclipse getByManeuver(int branchId,Date manTime);
	public List<Eclipse> getByColumnNSatNRange(String cName,int satId,TimeRange tr,int branchId);
	public List<Eclipse> getByColumnNSatNRangeType(String cName,String type,int satId,TimeRange tr,int branchId);

}
