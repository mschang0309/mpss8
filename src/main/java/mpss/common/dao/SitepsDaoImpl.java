package mpss.common.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Siteps;

@Repository("sitepsDao")
@SuppressWarnings("unchecked")
public class SitepsDaoImpl extends GenericDaoImpl<Siteps> implements SitepsDao {

    public SitepsDaoImpl() {
        super(Siteps.class);
    }

	@Override
	public long getNextId() {
		try{
			return (Long)entityManager.createNativeQuery("select nextval('siteps_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("SitepsDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}

	@Override
	public Siteps getById(long id) {
		try{
			Siteps s = entityManager.find(Siteps.class, id);
			return s;
		}catch(Exception ex){
			logger.info("SitepsDaoImpl getById() ex="+ex.getMessage());
			return null;
		}
		
	}

	@Override
	public List<Siteps> getBySiteId(int siteid) {
		String q = "select a.* from siteps a where a.siteid=" + siteid ;
		
		return entityManager.createNativeQuery(q, Siteps.class).getResultList();
	}
	
	public Siteps getBySiteIdNPsId(int siteId,int psId) {
		try{
			String q = "select s from Siteps s where s.siteid= :siteId and s.psid= :psId";
			return entityManager.createQuery(q, Siteps.class).setParameter("siteId", siteId)
      			.setParameter("psId", psId).getSingleResult();
		} catch(NoResultException ex) {
			logger.info("SitepsDaoImpl getBySiteIdNPsId() ex="+ex.getMessage());
	    	return null;
	    }
	}
    
}
