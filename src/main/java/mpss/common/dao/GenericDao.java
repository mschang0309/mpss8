package mpss.common.dao;

import java.util.List;

public interface GenericDao<T> {

    public T create(T t);
    
    public T get(Long id);

    public List<T> getAll();

    public boolean update(T object);

    public boolean delete(T object); 
    
    public boolean doSql(String jpsql);  
    
    public void reset();

 }
