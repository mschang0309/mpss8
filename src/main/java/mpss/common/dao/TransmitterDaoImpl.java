package mpss.common.dao;

import java.util.*;

import org.springframework.stereotype.Repository;
import mpss.common.jpa.Transmitter;


@Repository("transmitterDao")
public class TransmitterDaoImpl extends GenericDaoImpl<Transmitter> implements TransmitterDao {

    public TransmitterDaoImpl() {
        super(Transmitter.class);
    }   
    
    public Transmitter getAntenna(int satelliteid, String name) {
    	  String q = "select r from Transmitter r where r.name = :name and r.satelliteid = :satelliteid";
    	  return entityManager.createQuery(q, Transmitter.class).setParameter("satelliteid", satelliteid).setParameter("name", name).getSingleResult();
    }

	@Override
	public List<Transmitter> listAntennas(int transmitter) {
		 String q = "select r from Transmitter r where r.satelliteid = :satelliteid";
   	  return entityManager.createQuery(q, Transmitter.class).setParameter("satelliteid", transmitter).getResultList();
		
	}
}
