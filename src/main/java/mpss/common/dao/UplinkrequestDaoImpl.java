package mpss.common.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Uplinkrequest;

@Repository("uplinkrequestDao")
public class UplinkrequestDaoImpl extends GenericDaoImpl<Uplinkrequest> implements UplinkrequestDao {

    public UplinkrequestDaoImpl() {
        super(Uplinkrequest.class);
    }  
    
    public List<Uplinkrequest> getByArlId(int arlId)
    {
  	  String q = "select isu from Uplinkrequest isu where isu.arlid = :arlid";
  	  return entityManager.createQuery(q, Uplinkrequest.class).setParameter("arlid", arlId).getResultList();
    }
}
