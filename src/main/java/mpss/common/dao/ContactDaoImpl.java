package mpss.common.dao;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Contact;
import mpss.util.timeformat.DateUtil;
import mpss.util.timeformat.TimeRange;

@Repository("contactDao")
@SuppressWarnings("unchecked")
public class ContactDaoImpl extends GenericDaoImpl<Contact> implements ContactDao {

    public ContactDaoImpl() {
        super(Contact.class);
    }
    
    public List<Contact> getByRangeNSat(TimeRange range,int satId,int branchId){
    	try{
        	String q = "select c from Contact c where c.rise between :lowerrange and :uprange " +
        			"and c.satelliteid = :satId and c.branchid = :branchId and c.available=1";
          	return entityManager.createQuery(q, Contact.class)
          			  .setParameter("lowerrange", range.first)
          			  .setParameter("uprange", range.second)
          			  .setParameter("satId", satId)
          			  .setParameter("branchId", branchId).getResultList();
		}catch(Exception ex){
			logger.info("ContactDaoImpl getByRangeNSat() ex="+ex.getMessage());
			return null;
		}

    }
    
    public List<Contact> getConsbyRangeNSatNSite(TimeRange range,int satId,int siteId,int branchId){
    	try{
        	String q = "select c from Contact c where c.rise between :lowerrange and :uprange" +
        			" and c.satelliteid = :satId and c.siteid = :siteId and c.branchid = :branchId and c.available=1";
          	return entityManager.createQuery(q, Contact.class)
          			  .setParameter("lowerrange", range.first)
          			  .setParameter("uprange", range.second)
          			  .setParameter("satId", satId)
          			  .setParameter("siteId", siteId)
          			  .setParameter("branchId", branchId).getResultList();
		}catch(Exception ex){
			logger.info("ContactDaoImpl getByRangeNSat() ex="+ex.getMessage());
			return null;
		}

    }

	@Override
	public long getNextId() {
		try{
			return (Long)entityManager.createNativeQuery("select nextval('contact_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("ContactDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}

	@Override
	public Contact getContactById(int id) {
		try{
			
			String q = "select a.* from Contact a where a.id='" + String.valueOf(id) + "' " ;
	        return (Contact) entityManager.createNativeQuery(q, Contact.class).getSingleResult();
		}catch(Exception ex){
			logger.info("ContactDaoImpl getContactById() id ="+ex.getMessage());
			logger.info("ContactDaoImpl getContactById() ex="+ex.getMessage());
			return null;
		}
		
		
	}

	@Override
	public Contact getContactByParams(long sat, long site, long ar, long branch) {	
		try{
			String q = "select a.* from contact a where a.satelliteid='" + String.valueOf(sat) + "' and a.siteid='" + String.valueOf(site) + "' and a.revid='" + String.valueOf(ar) + "' and a.branchid='" + String.valueOf(branch)+"'" ;
	        System.out.println("query contact sql="+q);
			return (Contact) entityManager.createNativeQuery(q, Contact.class).getSingleResult();
		}catch(Exception ex){
			logger.info("ContactDaoImpl getContactByParams() ex="+ex.getMessage());
			return null;
		}
			
	}

	@Override
	public Collection<Contact> getByBranchId(int branchid) {
		String q = "select a from Contact a where a.branchid = :branchid ";
  	  	return entityManager.createQuery(q, Contact.class).setParameter("branchid", branchid).getResultList();
	}
	
	@Override
	public Collection<Contact> getByStatId(int stat) {
		String q = "select a from Contact a where a.satelliteid = :satelliteid ";
  	  	return entityManager.createQuery(q, Contact.class).setParameter("satelliteid", stat).getResultList();
	}
	
	public Collection<Contact> getBySiteId(int site) {
		String q = "select a from Contact a where a.siteid = :siteid ";
  	  	return entityManager.createQuery(q, Contact.class).setParameter("siteid", site).getResultList();
	}
	
	public Collection<Contact> getContactByParams(int sat,int siteid,int  br){
		String q = "select a.* from Contact a where a.satelliteid='" + String.valueOf(sat) + "' and a.siteid='" + String.valueOf(siteid) + "'  and a.branchid='" + String.valueOf(br)+"'" ;
        return entityManager.createQuery(q, Contact.class).getResultList();
	}

	@Override
	public List<Contact> getByBranchIdNTimeRange(int branchId, String trStart,String trEnd) {
		if(branchId!=0){
			String q = "select a.* from Contact a where a.rise<'" + trEnd + "' and a.fade>'" + trStart + "' and a.branchid=" + branchId + " and available=1 order by a.rise";
			System.out.println("sql="+q);
			//String q = "select a.* from Contact a where  a.branchid=" + branchId + " and available=1 order by a.rise";
			return entityManager.createNativeQuery(q, Contact.class).getResultList();
		}else{
			String q = "select a.* from Contact a where a.rise<'" + trEnd + "' and a.fade>'" + trStart + "' and a.branchid=" + branchId + " and available=1 order by a.rise";
			return entityManager.createNativeQuery(q, Contact.class).getResultList();
		}
		
	}
	
	public List<Contact> getBySiteTypeNSatNBranchNRange(String sitetype,int satId, int branchId, Timestamp trStart,Timestamp trEnd,int sessId) {
		try{
			String q = "select a.* from contact a,site b where a.siteid=b.id and " +
					"b.sitetype='" + sitetype + "' and " +
					"a.satelliteid = " + satId + " and " +
					"a.branchid = " + branchId + " and " +
					"a.rise between '" + trStart + "' and '" + trEnd + "' and " +
					"siteid in(select siteid from sessionsitexref where sessionid = " + sessId +
					") and available=1 order by a.rise";
	  	  	return entityManager.createNativeQuery(q, Contact.class).getResultList();
		}catch(Exception ex){
			logger.info("ContactDaoImpl getBySiteTypeNSatNBranchNRange() ex="+ex.getMessage());
			return null;
		}

	}
	
	public List<Contact> getBySessionNBranch(int branchId, Timestamp trStart,Timestamp trEnd,int sessId) {
		try{
			String q = "select a.* from contact a,site b where a.siteid=b.id and " +
					"a.branchid = " + branchId + " and " +
					"a.rise between '" + trStart + "' and '" + trEnd + "' and " +
					"siteid in(select siteid from sessionsitexref where sessionid = " + sessId +
					") and available=1 order by a.rise";
	  	  	return entityManager.createNativeQuery(q, Contact.class).getResultList();
		}catch(Exception ex){
			logger.info("ContactDaoImpl getBySessionNBranch() ex="+ex.getMessage());
			return null;
		}
	}
	
	public List<Contact> getBySessionNAIP(int branchId, Timestamp trStart,Timestamp trEnd) {
		try{
			String q = "select a.* from contact a,site b where a.siteid=b.id and " +
					"a.branchid = " + branchId + " and " +
					"a.rise between '" + trStart + "' and '" + trEnd + "' and " +
					"siteid = (select id from site where aipdump = 1) and available=1 order by a.rise";
	  	  	return entityManager.createNativeQuery(q, Contact.class).getResultList();
		}catch(Exception ex){
			logger.info("ContactDaoImpl getBySessionNBranch() ex="+ex.getMessage());
			return null;
		}
	}
	
	public List<Contact> getBySessionExtent(int sessionId) {
		try{
			String q = "select * from contact where id in (" +
					"select contactid from extent where id in (" +
					"select extentid from activity where sessionid=" +
					sessionId + " and arlentrytype = 'MON')) order by rise";
	  	  	return entityManager.createNativeQuery(q, Contact.class).getResultList();
		}catch(Exception ex){
			logger.info("ContactDaoImpl getBySessionExtent() ex="+ex.getMessage());
			return null;
		}
	}
	public List<Contact> getContactsByRevid(int revid){
    	String q = "select c from Contact c where c.revid = :revid";
      	return entityManager.createQuery(q, Contact.class)
      			  .setParameter("revid", revid).getResultList();
    }
    
    public Contact getContactByMaxeltime( Date maxeltime) {
    	Date maxeltime1 = DateUtil.DateAddSec(maxeltime,1);
    	String q = null;
  	  try{
  		  	q = "select a.* from contact a where a.maxeltime between '" + DateUtil.dateToString(maxeltime,"yyyy-MM-dd HH:mm:ss") + "' and   '" + DateUtil.dateToString(maxeltime1,"yyyy-MM-dd HH:mm:ss") + "'";
  	  		return (Contact)entityManager.createNativeQuery(q, Contact.class).getSingleResult();
		}catch(Exception ex){
			logger.info("ContactDaoImpl getContactByMaxeltime(), ex = "+ex.getMessage());
			logger.info("ContactDaoImpl sql = "+q);			
			return null;
		}
	}
    
	public Contact getBySatNBranchNotInSite(int siteId,int satId, int branchId, TimeRange tr) {
		try{
			String q = "select a.* from contact a where a.satelliteid = " + satId + 
					" and a.branchid = " + branchId + " and a.siteid <> " + siteId + 
					" and (a.rise between '" + tr.first + "' and '" + tr.second + "'" +
					" or a.fade between '" + tr.first + "' and '" + tr.second + "')" +
					" and available=1 order by a.rise limit 1";
	  	  	return (Contact)entityManager.createNativeQuery(q, Contact.class).getSingleResult();
		}catch(NoResultException e){
			logger.info("ContactDaoImpl getBySatNBranchNotInSite() NoResultException");
			return null;
		}catch(Exception e){
			logger.info("ContactDaoImpl getBySatNBranchNotInSite() ex="+e.getMessage());	
			return null;
		}
	}
	
	public List<Contact> getConsbyRangeSBandSite(TimeRange tr,int satId,int branchId){
    	try{
        	String q = "select * from Contact where rise between '" + tr.first + "' and '" + tr.second + "'" +
        			" and satelliteid = " + satId+" and siteid in (select id from site where sitetype='SBAND' ) and branchid = " + branchId+ " and available=1 order by rise";
               	
        	return entityManager.createNativeQuery(q, Contact.class).getResultList();
		}catch(Exception ex){
			logger.info("ContactDaoImpl getConsbyRangeSBandSite() ex="+ex.getMessage());
			return null;
		}

    }
	
	public Contact getConbySiteRev(long satId,int branchId,int siteId,String revid) {
		try{
			String q = "select a.* from contact a where a.satelliteid = " + satId + 
					" and a.branchid = " + branchId + " and a.siteid = " + siteId + " and a.revid = " + revid + 
					" and available=1 order by a.rise limit 1";
	  	  	return (Contact)entityManager.createNativeQuery(q, Contact.class).getSingleResult();
		}catch(NoResultException e){
			logger.info("ContactDaoImpl getConbySiteRev() NoResultException");
			return null;
		}catch(Exception e){
			logger.info("ContactDaoImpl getConbySiteRev() ex="+e.getMessage());	
			return null;
		}
	}
}
