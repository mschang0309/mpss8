package mpss.common.dao;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import mpss.common.jpa.Activity;
import mpss.util.timeformat.TimeRange;

public interface ActivityDao extends GenericDao<Activity> {
	public Activity getActivityById(long id);
	public Collection<Activity> getAssociatedAct(long id);
	public Collection<Activity> getBySessionId(int sessionid);
	public Collection<Activity> getBySessionId(int sessionid,boolean schflag);
	public Collection<Activity> getByEntryIdNType(int arlentryid, String arlentrytype);
	public Collection<Activity> getBySessionIdNTimerange(int sessionid, String starttime, String endtime);
	public Collection<Activity> getUnschBySessionIdNTimerange(int sessionid, String starttime, String endtime);
	public long getMaxId();
	public long getNextId();
	public List<Activity> getActBySessionIdNStartTime(int sessionid, Timestamp starttime);
	public List<Activity> getByEntryId(int arlentryid);
	public List<Activity> getUnschByRangeNBranchId(int sessionid,int branchid, TimeRange tr);
	public List<Activity> getAIPDownlinkFile(Timestamp sTime, Timestamp eTime);
	public List<Activity> getAIPBySession(int sessionid);
	public List<Activity> getBySessionNType(int sessionid, String arlentrytype);
	public Collection<Activity> getBySessionExtentSchType(int sessionid,int extentid,int scheduled,String arlentrytype);
	public Collection<Activity> getBySessionSchType(int sessionid,int scheduled,String arlentrytype);
	public List<Integer> getExtentIdNotinArlid(String arlid, TimeRange tr);
	public List<Long> getActivityIdNotinArlid(String arlid, TimeRange tr);
	public Collection<Activity> getActivityByRange(Timestamp starttime, Timestamp endtime);
	public Activity getStopRec(Timestamp pbkTime, String filename);
	
	public Collection<Activity> getBySessionIdEndtime(int sessionid ,String endtime);
	public Collection<Activity> getMonActBySessionIdEndtime(int sessionid ,String endtime);
	public Collection<Activity> getMonActForPassPlan(Timestamp starttime,Timestamp endtime);
	
	public List<Integer> getGroupSessionbyActId(String acts);

}
