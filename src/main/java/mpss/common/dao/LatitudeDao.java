package mpss.common.dao;

import java.sql.Timestamp;
import java.util.List;

import mpss.common.jpa.Latitude;
import mpss.util.timeformat.TimeRange;


public interface LatitudeDao extends GenericDao<Latitude> {
	
	public List<Latitude> getLatitudesByRevid(int revid);
	public Latitude getByStartTime(int branchId, Timestamp trStart);
	public List<Latitude> getSatNameNTimerange(int branchId,String satName,TimeRange tr);

}
