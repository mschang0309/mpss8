package mpss.common.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Rev;
import mpss.util.timeformat.TimeRange;

@Repository("revDao")
@SuppressWarnings("unchecked")
public class RevDaoImpl extends GenericDaoImpl<Rev> implements RevDao {

    public RevDaoImpl() {
        super(Rev.class);
    }
    
    public List<Rev> getRevbyRange(TimeRange range,int branchid){
    	String q = "select r from Rev r where r.antime between :lowerrange and :uprange and r.branchid = :branchid order by r.revno";
      	return entityManager.createQuery(q, Rev.class)
      			  .setParameter("lowerrange", range.first)
      			  .setParameter("uprange", range.second)
      			  .setParameter("branchid", branchid).getResultList();
    }
    
	public Rev getMaxRev(int scid,int branchid) {	
	    try{
			String q = "select r from Rev r where r.revno = (select max(r.revno) from Rev r where r.scid = :scid and r.branchid = :branchid)" + 
				" and r.scid = :scid and r.branchid = :branchid";
			return entityManager.createQuery(q, Rev.class)
					.setParameter("scid", scid)
		      		.setParameter("branchid", branchid).getSingleResult();
	    } catch(NoResultException ex) {
	    	logger.info("RevDaoImpl getMaxRev() ex="+ex.getMessage());
	        return null;
	    }
	}
	
	public Rev getMaxTimeRev(int scid,int branchid, Timestamp ts) {	
	    try{
			String q = "select r.* from rev r where r.scid = " + scid + " and r.branchid = " + branchid +
				" and r.antime <= '" + ts + "' order by r.antime desc limit 1";
			return (Rev) entityManager.createNativeQuery(q, Rev.class).getSingleResult();
	    } catch(NoResultException ex) {
	    	logger.info("RevDaoImpl getMaxTimeRev() ex="+ex.getMessage());
	        return null;
	    }
	}
	
	public Rev getByRevno(int revno,int scid,int branchid) {	
	    try{
			String q = "select r from Rev r where r.revno = :revno and r.scid = :scid and r.branchid = :branchid";
			return entityManager.createQuery(q, Rev.class)
					.setParameter("revno", revno)
					.setParameter("scid", scid)
		      		.setParameter("branchid", branchid).getSingleResult();
	    } catch(NoResultException ex) {
	    	logger.info("RevDaoImpl getByRevno() ex="+ex.getMessage());
	        return null;
	    }
	}
	
	@Override
	public Rev getRevbyID(int id) {
		try {

			String q = "select a.* from Rev a where a.id='"+ String.valueOf(id) + "' ";
			return (Rev) entityManager.createNativeQuery(q, Rev.class)
					.getSingleResult();
		} catch (Exception ex) {
			logger.info("RevDaoImpl getRevbyID() id ="+ String.valueOf(id));
			logger.info("RevDaoImpl getRevbyID() ex="+ex.getMessage());
			return null;
		}

	}

	@Override
	public List<Rev> getByBranchIdNTimeRange(int branchId, String trStart,
			String trEnd) {
		if(branchId!=0){
			String q = "select a.* from rev a where a.antime<'" + trEnd + "' and a.dntime>'" + trStart + "' and a.branchid=" + branchId ;
			//String q = "select a.* from rev a where  a.branchid=" + branchId ;
			return entityManager.createNativeQuery(q, Rev.class).getResultList();
		}else{
			String q = "select a.* from rev a where a.antime<'" + trEnd + "' and a.dntime>'" + trStart + "' and a.branchid=" + branchId ;
			return entityManager.createNativeQuery(q, Rev.class).getResultList();
		}
		
	}
	
    public Rev getRevbyTime(Timestamp time,int branchId) {
    	try{
    		String q = "select r.* from rev r where '" + time + "' between r.antime and r.revendtime and r.branchid = " + branchId ;
//    		System.out.println("SQL:" + q);
    		return (Rev) entityManager.createNativeQuery(q, Rev.class).getSingleResult();
    	} catch(NoResultException ex) {
    		logger.info("RevDaoImpl getRevbyTime() ex="+ex.getMessage());
	        return null;
	    }
    }
    
    public Rev getByStartTime(int branchId, Timestamp trStart) {		
			String q = "select a.* from rev a where a.dntime<'" + trStart + "' and a.branchid=" + branchId +" order by a.antime desc limit 1";			
			return (Rev) entityManager.createNativeQuery(q, Rev.class).getSingleResult();	
	}
}
