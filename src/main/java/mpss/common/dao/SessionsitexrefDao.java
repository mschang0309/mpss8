package mpss.common.dao;

import java.util.List;
import mpss.common.jpa.Sessionsitexref;

public interface SessionsitexrefDao extends GenericDao<Sessionsitexref> {
	public Sessionsitexref getById(int id);
	public long getNextId();
	public List<Sessionsitexref> getBySessionId(int sessionId);

}
