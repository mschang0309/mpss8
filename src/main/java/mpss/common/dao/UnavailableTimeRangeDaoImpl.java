package mpss.common.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import java.util.List;

import mpss.common.jpa.Unavailtimerange;

@Repository("unavailableTimeRangeDao")
@SuppressWarnings("unchecked")
public class UnavailableTimeRangeDaoImpl extends GenericDaoImpl<Unavailtimerange> implements UnavailableTimeRangeDao {

    //private EntityManager entityManager;
    
    public UnavailableTimeRangeDaoImpl() {
    	 super(Unavailtimerange.class);
    }
    
    // for Spring injection
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

	@Override
	public List<Unavailtimerange> getUnavailableTimeRangeByBid(int branchid) {
		 String q = "select r from Unavailtimerange r where r.branchid = :branchid";
	   	  return entityManager.createQuery(q, Unavailtimerange.class).setParameter("branchid", branchid).getResultList();
		
	}
	
	public List<Unavailtimerange> getAllUnavailableTimeRange() {
		 String q = "select r from Unavailtimerange r ";
	   	  return entityManager.createQuery(q, Unavailtimerange.class).getResultList();
		
	}
	
	//starttime   endtime 
	public List<Unavailtimerange> getUnavailtimerangeByTime(int branchId, String trStart, String trEnd){		
		if(branchId!=0){
			String q = "select a.* from Unavailtimerange a where a.starttime<'" + trEnd + "' and a.endtime>'" + trStart + "' and a.branchid=" + branchId + "  order by a.id";
			System.out.println("sql="+q);
			logger.info("SessionDaoImpl getUnavailtimerangeByTime() "+" sql="+q);
			return entityManager.createNativeQuery(q, Unavailtimerange.class).getResultList();
		}else{
			String q = "select a.* from Unavailtimerange a where a.starttime<'" + trEnd + "' and a.endtime>'" + trStart + "' and a.branchid=" + branchId + " order by a.id";
			return entityManager.createNativeQuery(q, Unavailtimerange.class).getResultList();
		}
	}

	public List<Unavailtimerange> getSiteUnavailtimerangeByTime(int branchId,int siteid, String trStart, String trEnd){
		if(branchId!=0){
			String q = "select a.* from Unavailtimerange a where a.starttime<'" + trEnd + "' and a.endtime>'" + trStart + "' and a.branchid=" + branchId + " and a.parentid="+ siteid + "  order by a.id";
			System.out.println("sql="+q);
			return entityManager.createNativeQuery(q, Unavailtimerange.class).getResultList();
		}else{
			String q = "select a.* from Unavailtimerange a where a.starttime<'" + trEnd + "' and a.endtime>'" + trStart + "' and a.branchid=" + branchId + " and a.parentid="+ siteid + " order by a.id";
			return entityManager.createNativeQuery(q, Unavailtimerange.class).getResultList();
		}
	}
	  
}
