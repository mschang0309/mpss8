package mpss.common.dao;

import java.util.*;
import mpss.common.jpa.Antenna;

public interface AntennaDao extends GenericDao<Antenna> {

    public List<Antenna> listAntennas(int Antenna);
    public Antenna getAntenna(int satelliteid, String name);
}
