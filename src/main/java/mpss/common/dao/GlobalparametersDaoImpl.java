package mpss.common.dao;

import org.springframework.stereotype.Repository;
import mpss.common.jpa.Globalparameter;


@Repository("globalparametersDao")
public class GlobalparametersDaoImpl extends GenericDaoImpl<Globalparameter> implements GlobalparametersDao {

	public GlobalparametersDaoImpl() {
		super(Globalparameter.class);
	}

	@Override
	public long getNextId() {
		try{
			return (Long) entityManager.createNativeQuery("select nextval('globalparameters_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("GlobalparametersDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}

	@Override
	public Globalparameter getById(long id) {
		try{
			Globalparameter gp = entityManager.find(Globalparameter.class, id);
			return gp;
		}catch(Exception ex){
			logger.info("GlobalparametersDaoImpl Globalparameter() ex="+ex.getMessage());
			return null;
		}
		
	}	
	

}
