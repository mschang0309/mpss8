package mpss.common.dao;

import java.util.List;
import mpss.common.jpa.Parameterset;
import mpss.common.jpa.Recorderp;
import mpss.common.jpa.Satellitep;
import mpss.common.jpa.Siteps;
import mpss.common.jpa.Dutycycle;

public interface PsDao {
	// Master Parameterset
	public List<Parameterset> listParametersets();
	public Parameterset getParameterset(String psName);
	public String getParameterSetName(int psId);
	
    // Satellite Parameterset
	public Satellitep getSatellitep(String psName, String satName);
	public List<Recorderp> listRecorderps(String psName);
	public Recorderp getRecorderp(String psName, String satName, String recName);
	public Recorderp getRecorderp(int recId, int sessionId);
	public List<Dutycycle> listDutycycles(String psName, String satName);
	public Dutycycle getDutycycle(String psName, String satName, String actType, String actSubType);
	public Dutycycle getDutycycle(int sessionId, int satId, String actType, String actSubType);
	//public List<PsSatelliteParameterSet> listSatelliteParametersets(String psName);
    //public PsSatelliteParameterSet getSatelliteParameterset(String psName, String satName);
	
    // Site Parameterset
    public List<Siteps> listSiteps(String psName);
    public Siteps getSitep(String psName, String siteName);
}
