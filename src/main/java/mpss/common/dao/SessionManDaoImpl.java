package mpss.common.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;

import mpss.configFactory;
import mpss.common.jpa.*;

@Repository("sessionManDao")
@SuppressWarnings("unchecked")
public class SessionManDaoImpl implements SessionManDao {

    private EntityManager entityManager;
    private Logger logger = Logger.getLogger(configFactory.getLogName());
    
    @Autowired 
    SessionDao sessionDao;
    
    @Autowired
    PsDao psDao;
    
    @Autowired
    SatellitepDao satpDao;
    
    public SessionManDaoImpl() {
    }
    
    // for Spring injection
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    //= Session
    public Session getSessionByName(String sessionName) {
    	try{
    		String q = "select s from Session s where s.name = :name";
      	  return entityManager.createQuery(q, Session.class).setParameter("name", sessionName).getSingleResult();
		}catch(Exception ex){
			logger.info("SessionManDaoImpl getSessionByName() ex="+ex.getMessage());
			return null;
		}
    	  
    }
    
    public Session getSessionById(int sessionId) {
  	  //String q = "select s from Session s where s.id = :sessionId";
  	  //return entityManager.createQuery(q, Session.class).setParameter("sessionId", (long)sessionId).getSingleResult();
    	return sessionDao.get((long)sessionId);
    }
    
    //= Satellites
    public List<Satellite> getRefSatellites(String sessionName) {
        String q = "select sat from Satellite sat where sat.id IN (" +
                   "select sxref.satelliteid from Sessionsatxref sxref where sxref.sessionid =  " +
    	            "ANY(select sess.id from Session sess where sess.name = :name))";
  	    return entityManager.createQuery(q, Satellite.class).setParameter("name", sessionName).getResultList();
    }
    
    //public List<Integer> getSatelliteXrefs(String sessionName) {
    //    String q = "select sxref.satelliteid from Sessionsatxref sxref where sxref.sessionid =  " +
    //	            "ANY(select sess.id from Session sess where sess.name = :name)";
  	//    return entityManager.createQuery(q, Integer.class).setParameter("name", sessionName).getResultList();
    //}
    
    //= Satellite ARLs
    public List<Arl> getRefSatelliteARLsByName(String sessionName, String satName) {
    	  Session session = getSessionByName(sessionName);
    	  if (session == null)
    	  {
    	  	  // empty list
    	      return new ArrayList<Arl>();
    	  }
    	  Satellite satellite = getSatelliteByName(satName);
    	  if (satellite == null)
    	  {
    	  	  // empty list
    	      return new ArrayList<Arl>();
    	  }
  	    return getRefSatelliteARLsById(session.getId().intValue(), satellite.getId().intValue());
    }
   
    public List<Arl> getRefSatelliteARLsById(int sessionId, int satelliteId) {
        String q = "select arl from Arl arl where arl.id = " +
                   "ANY(select sxref.arlid from Sessionsatarlxref sxref where sxref.sessionid = :sessionid and sxref.satelliteid = :satelliteid)";
  	    return entityManager.createQuery(q, Arl.class).setParameter("sessionid", sessionId).setParameter("satelliteid", satelliteId).getResultList();
    }
    
    public Satellite getSatelliteByName(String satName)
    {
        String q = "select sat from Satellite sat where sat.name = :name";
  	    return entityManager.createQuery(q, Satellite.class).setParameter("name", satName).getSingleResult();
    }
    
    //= Sites
    public List<Site> getRefSites(String sessionName) {
        String q = "select site from Site site where site.id IN (" +
                   "select sxref.siteid from Sessionsitexref sxref where sxref.sessionid =  " +
    	            "ANY(select sess.id from Session sess where sess.name = :name))";
  	    return entityManager.createQuery(q, Site.class).setParameter("name", sessionName).getResultList();
    }
    
    public List<Site> getRefSitesById(int sessionId) {
        String q = "select site from Site site where site.id IN (" +
                   "select sxref.siteid from Sessionsitexref sxref where sxref.sessionid = :sessionId)";
  	    return entityManager.createQuery(q, Site.class).setParameter("sessionId", sessionId).getResultList();
    }
    
    // parameter sets used by session
    public Parameterset getParameterset(String sessionName)
    {
    	try{
    		String q = "select ps from Parameterset ps where ps.id = " +
 	               "ANY(select sess.psid from Session sess where sess.name = :name)";
 	    return entityManager.createQuery(q, Parameterset.class).setParameter("name", sessionName).getSingleResult();
		}catch(Exception ex){
			logger.info("SessionManDaoImpl getParameterset() ex="+ex.getMessage());
			return null;
		}
           
	    
    }
    
	public Satellitep getSatellitep(String psName, String satName)
	{
		return psDao.getSatellitep(psName, satName);
	}

	public Recorderp getRecorderp(String psName, String satName, String recName)
	{
		return psDao.getRecorderp(psName, satName, recName);
	}
	
	public Recorderp getRecorderp(int recId, int sessionId)
	{
		return psDao.getRecorderp(recId, sessionId);
	}

	public Dutycycle getDutycycle(int sessionId, int satId, String actType, String actSubType)
	{
		return psDao.getDutycycle(sessionId, satId, actType, actSubType);
	}

	public Dutycycle getDutycycle(String psName, String satName, String actType, String actSubType)
	{
		return psDao.getDutycycle(psName, satName, actType, actSubType);
	}
	
	public Siteps getSitep(String psName, String siteName)
    {
    	return psDao.getSitep(psName, siteName);
    }
    
    public Satellite getSatellite(int sessionId)
    {
    	try{
    		String q = "select s.* from Satellite s where s.id = " +
 	             "(select xref.satelliteid from Sessionsatxref xref where xref.sessionid = " + sessionId + ")";
    		return (Satellite)entityManager.createNativeQuery(q, Satellite.class).getSingleResult();
		}catch(Exception ex){
			logger.info("SessionManDaoImpl getSatellite() ex="+ex.getMessage());
			return null;
		}
    }
    
	public Satellitep getSatellitep(int sessionId,int satId)
	{
		try{
			int psId = sessionDao.get((long)sessionId).getPsid();
			return satpDao.getBySatIdNPsId(satId, psId);
		}catch(Exception ex){
			logger.info("SessionManDaoImpl getSatellitep() ex="+ex.getMessage());
			return null;
		}
	}
	
	public List<Extent> getDutyCycleExtents(int scid,int branchid, Timestamp ts) {	
	    try{
			String q ="select e.* from (select * from extent where branchid = " + branchid + " and starttime < '"
				+ ts + "' and endtime >= (select r.antime from rev r where r.scid = " + scid + " and r.branchid ="
				+ branchid + " and r.antime <= '" + ts + "' order by r.antime desc limit 1)) e"
				+ " left join activity a on a.id = e.activityid where a.arlentrytype='RSI' and"
				+ " (a.actsubtype='REC' or a.actsubtype='DDT') order by starttime";
			return entityManager.createNativeQuery(q, Extent.class).getResultList();
	    } catch(Exception ex) {
	    	logger.info("SessionManDaoImpl getDutyCycleExtents() ex="+ex.getMessage());
	        return null;
	    }
	}
 }
