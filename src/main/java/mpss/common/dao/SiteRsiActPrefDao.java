package mpss.common.dao;

import java.util.List;

import mpss.common.jpa.Sitersiactpref;

public interface SiteRsiActPrefDao extends GenericDao<Sitersiactpref> {

	public List<Sitersiactpref> getSiteRsiActPrefsByPsid(int satpsid);
}
