package mpss.common.dao;

import java.util.*;
import mpss.common.jpa.Transmitter;

public interface TransmitterDao extends GenericDao<Transmitter> {

    public List<Transmitter> listAntennas(int Transmitter);
    public Transmitter getAntenna(int satelliteid, String name);
}
