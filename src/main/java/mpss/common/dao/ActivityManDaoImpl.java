package mpss.common.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;

import mpss.common.jpa.*;
import mpss.util.timeformat.RangeUtil;
import mpss.util.timeformat.TimeRange;

@Repository("activityManDao")
public class ActivityManDaoImpl implements ActivityManDao {

    @SuppressWarnings("unused")
	private EntityManager entityManager;
    
    @Autowired 
    SessionManDao sessManDao;
    
    @Autowired
    ActivityDao actDao;
    
    @Autowired
	RecorderDao recDao;
    
    @Autowired
    ExtentDao extentDao;
    
    public ActivityManDaoImpl() {
    }
    
    // for Spring injection
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    public TimeRange getOperationalPeriod(Extent extent)
	{
		Timestamp startTime = extent.getStarttime();
		Timestamp endtime = extent.getEndtime();
		Activity act = actDao.get((long)extent.getActivityid());
		Satellitep satps = sessManDao.getSatellitep(act.getSessionid(), act.getSatelliteid());
		
		int setupDelay,cleanupDelay;
		switch (act.getActsubtype())
		{
			case "PBK" :
				if (act.getArlentrytype().equalsIgnoreCase("RSI"))
				{
					setupDelay = satps.getRsipbsetupdelay();
					cleanupDelay = satps.getRsipbcleanupdelay();
				}
				else
				{
					setupDelay = satps.getIsualpbsetupdelay();
					cleanupDelay = satps.getIsualpbcleanupdelay();
				}
				break;
				
			case "REC" :
				setupDelay = satps.getRsirecsetupdelay();
				cleanupDelay = satps.getRsireccleanupdelay();
				break;
			
			case "DDT" :
				setupDelay = satps.getRsirecsetupdelay();
				cleanupDelay = (int)Math.ceil((extent.getRecorderdelta() + extent.getSecondrecorderdelta()) * satps.getPostddtdelay());
				break;
			
			default:
				setupDelay = 0;
				cleanupDelay = 0;
				break;
		}

		// calculate start/stop times buy considering ssr setup/cleanup times
	    TimeRange TR = new TimeRange(startTime, endtime);
	    TimeRange tRange = RangeUtil.rangeAppendSec(TR , setupDelay, - cleanupDelay);

		return tRange;
	}
 
    public double calculateSectors(int recorderId,int sessionid ,String type)
	{
    	int totalCapinMbits = 0; 
	    Recorderp recp = sessManDao.getRecorderp(recorderId, sessionid);
	    if(recp != null)
	    	totalCapinMbits = type.equalsIgnoreCase("ISUAL") ? recp.getIsualcapacity() : recp.getRsicapacity();
		
		// convert megabits to sectors
	    int Sectorsize = recDao.get((long)recorderId).getSectorsize();
	    double totalCap = 0;
	    if (Sectorsize != 0)
	    	totalCap = totalCapinMbits / Sectorsize;
		
		return totalCap;
		
	}
    
    public double getAvailCapacity(Extent extent,String type)
	{
    	Activity act = actDao.get((long)extent.getActivityid());
    	
		int totalCapinMbits = 0; 
	    Recorderp recp = sessManDao.getRecorderp(extent.getRecorderid(), act.getSessionid());
	    if(recp != null)
	    	totalCapinMbits = type.equalsIgnoreCase("ISUAL") ? recp.getIsualcapacity() : recp.getRsicapacity();
		
		// convert megabits to sectors
	    int Sectorsize = recDao.get((long)extent.getRecorderid()).getSectorsize();
	    double totalCap = 0;
	    if (Sectorsize != 0)
	    	totalCap = totalCapinMbits / Sectorsize;
		
		// get the available capacity of the RSI reservoir at the start of this extent
		int availCap;
		if (type.equalsIgnoreCase("ISUAL"))
			availCap = getISUALCapacity(extent.getStarttime(),extent.getRecorderid(),act.getSessionid());
		else
			availCap = getRSICapacity(extent.getStarttime(),extent.getRecorderid(),act.getSessionid());
		
		return (totalCap - availCap);
		
	}
    
    public double getAvailCapacity(Extent extent,String type,Timestamp tval)
	{
    	Activity act = actDao.get((long)extent.getActivityid());
    	
		int totalCapinMbits = 0; 
	    Recorderp recp = sessManDao.getRecorderp(extent.getRecorderid(), act.getSessionid());
	    if(recp != null)
	    	totalCapinMbits = type.equalsIgnoreCase("ISUAL") ? recp.getIsualcapacity() : recp.getRsicapacity();
		
		// convert megabits to sectors
	    int Sectorsize = recDao.get((long)extent.getRecorderid()).getSectorsize();
	    double totalCap = 0;
	    if (Sectorsize != 0)
	    	totalCap = totalCapinMbits / Sectorsize;
		
		// get the available capacity of the RSI reservoir at the start of this extent
		int availCap;
		if (type.equalsIgnoreCase("ISUAL"))
			availCap = getISUALCapacity(tval,extent.getRecorderid(),act.getSessionid());
		else
			availCap = getRSICapacity(tval,extent.getRecorderid(),act.getSessionid());
		
		return (totalCap - availCap);
		
	}
    
    public double getTotalAvail(Extent extent)
	{
    	Activity act = actDao.get((long)extent.getActivityid());
		
		Timestamp tval = extent.getStarttime();
		// now at this point, get the idividual capacities for each type (DDT, ISUAL, RSI)
		int ddtU = getDDTCapacity(tval,extent.getRecorderid(),act.getSessionid());
		int rsiU = getRSICapacity(tval,extent.getRecorderid(),act.getSessionid());
		int islU = getISUALCapacity(tval,extent.getRecorderid(),act.getSessionid());
		
		// add the total amount of SSR used by adding each individual capacity
		int totalUsed = ddtU + rsiU + islU;

		int totalCapinMbits = 0; 
	    Recorderp recp = sessManDao.getRecorderp(extent.getRecorderid(), act.getSessionid());
	    if(recp != null)
	    	totalCapinMbits = recp.getCapacity();
		
	    // convert megabits to sectors
	    int Sectorsize = recDao.get((long)extent.getRecorderid()).getSectorsize();
	    double totalCap = 0;
	    if (Sectorsize != 0)
	    	totalCap = totalCapinMbits / Sectorsize;
		
		// total available = total capacity minus the total amount used by ISUAL, DDT, RSI
		return (totalCap - totalUsed);
	}
    
    public int getRSICapacity(Timestamp tval,int recorderId,int sessId)
	{
		int capacity = 0;

		// now need to filter extents by:
		//  - prior to passed time
		//  - associated with specified recorder
		//  - associated to RSI related activities
		// and put in rsiExtents vector
		List<Extent> rsiExtents = new ArrayList<Extent>();
		List<Extent> delExtents = new ArrayList<Extent>();
		rsiExtents = extentDao.getExtentsByRSICapacity("RSI", tval, recorderId, sessId);
		delExtents = extentDao.getExtentsByRSICapacity("DELETE", tval, recorderId, sessId);
		//System.out.println("rsiExtents size : " + rsiExtents.size());
		for (Extent ex : delExtents)
		{

			capacity += ex.getRecorderdelta();
		}
		// total up capacities of all extents on rsiExtents vector and return
		for(Extent rsiEx :rsiExtents)
		{
			capacity += rsiEx.getRecorderdelta();
			// if RSI is performing PAN+MS (requiring 2 filenames) then add in the second recorder delta
			if(rsiEx.getSecondfilename() > 0)
				capacity += rsiEx.getSecondrecorderdelta();
		}
		
		return capacity;
	}
	
    public int getISUALCapacity(Timestamp tval,int recorderId,int sessId)
	{
		int capacity = 0;

		List<Extent> isualExtents = new ArrayList<Extent>();
		isualExtents = extentDao.getExtentsByRSICapacity("ISUAL", tval, recorderId, sessId);
		//System.out.println("rsiExtents size : " + rsiExtents.size());
		if (isualExtents.size() > 0)
		{
			capacity += isualExtents.get(0).getRecorderdelta();
		}
		
		return capacity;
	}
	
    public int getDDTCapacity(Timestamp tval,int recorderId,int sessId)
	{
		int capacity = 0;

		List<Extent> ddtExtents = new ArrayList<Extent>();
		ddtExtents = extentDao.getExtentsByRSICapacity("DDT", tval, recorderId, sessId);
		
		for(Extent ddtEx :ddtExtents)
		{
			capacity += ddtEx.getRecorderdelta();
			capacity += ddtEx.getSecondrecorderdelta();
		}
		
		return capacity;
	}	

 }
