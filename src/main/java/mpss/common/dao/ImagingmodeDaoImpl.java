package mpss.common.dao;


import java.util.List;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Imagingmode;

//@Component("imagingmodeDao")
@Repository("imagingmodeDao")
public class ImagingmodeDaoImpl extends GenericDaoImpl<Imagingmode> implements ImagingmodeDao {

    public ImagingmodeDaoImpl() {
        super(Imagingmode.class);
    }
    
    public Imagingmode getImagingModeByInstNModeid(String instrument, int modeid)
    {
    	try{
    		String q = "select s from Imagingmode s where s.instrument = :instrument " +
   	             "and s.modeid = :modeid";
   	  //String q = "select s from " + Imagingmode.class.getName() + " s where s.instrument = :instrument " +
//	             "and s.modeid = :modeid";
	      
   	  return entityManager.createQuery(q, Imagingmode.class).setParameter("instrument", instrument).setParameter("modeid", modeid).getSingleResult();
		}catch(Exception ex){
			logger.info("ImagingmodeDaoImpl Imagingmode() ex="+ex.getMessage());
			return null;
		}
    	  
    }
    
    @Override
	public long getMaxId() {

    	try{	      
    		return (Long)entityManager.createQuery("select max(u.id) id from Imagingmode u").getSingleResult();
		}catch(Exception ex){
			logger.info("ImagingmodeDaoImpl getMaxId() ex="+ex.getMessage());
			return 0;
		}
  	  
  	  	

	}

	@Override
	public long getNextId() {
		try{	      
			return (Long)entityManager.createNativeQuery("select nextval('imagingmode_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("ImagingmodeDaoImpl getMaxId() ex="+ex.getMessage());
			return 0;
		}
		
		//return 0 ;
	}

	@Override
	public List<Imagingmode> getImagingModeByInst(String instrument) {
		String q = "select s from Imagingmode s where s.instrument = :instrument " ;
		return entityManager.createQuery(q, Imagingmode.class).setParameter("instrument", instrument).getResultList();
	}

	@Override
	public Imagingmode getImagingModeById(long id) {
		try{	      
			Imagingmode img = entityManager.find(Imagingmode.class, id);
			return img;
		}catch(Exception ex){
			logger.info("ImagingmodeDaoImpl getImagingModeById() ex="+ex.getMessage());
			return null;
		}
		
	}

	
}