package mpss.common.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Rtcmdprocrequest;

@Repository("rtcmdprocrequestDao")
public class RtcmdprocrequestDaoImpl extends GenericDaoImpl<Rtcmdprocrequest> implements RtcmdprocrequestDao {

    public RtcmdprocrequestDaoImpl() {
        super(Rtcmdprocrequest.class);
    }    
    
    public List<Rtcmdprocrequest> getByArlId(int arlId)
    {
  	  String q = "select isu from Rtcmdprocrequest isu where isu.arlid = :arlid";
  	  return entityManager.createQuery(q, Rtcmdprocrequest.class).setParameter("arlid", arlId).getResultList();
    }
}
