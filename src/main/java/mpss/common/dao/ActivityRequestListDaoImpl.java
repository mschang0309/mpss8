package mpss.common.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import mpss.configFactory;
import mpss.common.jpa.*;

@Repository("activityRequestListDao")
public class ActivityRequestListDaoImpl implements ActivityRequestListDao {

    private EntityManager entityManager;
    
    private Logger logger = Logger.getLogger(configFactory.getLogName());
    
    public ActivityRequestListDaoImpl() {
    }
    
    // for Spring injection
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Arl> listArls() {
    	  String q = "select a from Arl a";
    	  return entityManager.createQuery(q, Arl.class).getResultList();
      }

    public List<String> listExternalFileNames() {
    	  String q = "select a.name from Arl a where a.arltype = :arlType";
    	  return entityManager.createQuery(q, String.class).setParameter("arlType", "EXTFILE").getResultList();
      }


  	public Arl getArl(String arlName)
  	{
  		try{
  			String q = "select a from Arl a where a.name = :arlName";
      	  return entityManager.createQuery(q, Arl.class).setParameter("arlName", arlName).getSingleResult();
		}catch(Exception ex){
			logger.info("ActivityRequestListDaoImpl getArl() ex="+ex.getMessage());  
			return null;
		}
    	  	
  	}

  	public String getArlName(long id)
  	{
  		try{
  			String q = "select a from Arl a where a.id = :id";
      	  return entityManager.createQuery(q, Arl.class).setParameter("id", id).getSingleResult().getName();	
		}catch(Exception ex){
			logger.info("ActivityRequestListDaoImpl getArlName() ex="+ex.getMessage());
			return "";
		}
    	  
  	}

    // Isualimagingrequest
    public List<Isualimagingrequest> listIsualimagingrequests(String arlName)
    {
        String q = "select r from Isualimagingrequest r where r.arlid = " +
                   "ANY(select a.id from Arl a where a.name = :arlname)";
    	
  	    return entityManager.createQuery(q, Isualimagingrequest.class).setParameter("arlname", arlName)
  	                                                    .getResultList();	
    }

    // Isualprocrequest
    public List<Isualprocrequest> listIsualprocrequests(String arlName)
    {
        String q = "select r from Isualprocrequest r where r.arlid = " +
                   "ANY(select a.id from Arl a where a.name = :arlname)";
    	
  	    return entityManager.createQuery(q, Isualprocrequest.class).setParameter("arlname", arlName)
  	                                                    .getResultList();	
    }

    // Maneuverrequest
    public List<Maneuverrequest> listManeuverrequests(String arlName)
    {
        String q = "select r from Maneuverrequest r where r.arlid = " +
                "ANY(select a.id from Arl a where a.name = :arlname)";
    	
  	    return entityManager.createQuery(q, Maneuverrequest.class).setParameter("arlname", arlName)
  	                                                    .getResultList();	
    }

    // Mission Time Line
    public PsMissionTimeLine getMissionTimeLines(String arlName)
    {
    	  Arl arl = getArl(arlName);
    	  if (arl == null)
    	  {
    	  	return null;
    	  }
    	  
    	  PsMissionTimeLine mtl = new PsMissionTimeLine(arl);
    	  
    	  List<Maneuverrequest> mans = listManeuverrequests(arlName);
    	  mtl.setManeuverrequests(mans);

  		  List<Rsiimagingrequest> rsis = listRsiimagingrequests(arlName);
    	  mtl.setRsiimagingrequests(rsis);
    	
  	    return mtl;	
    }

    // Misccmdprocrequest
    public List<Misccmdprocrequest> listMisccmdprocrequests(String arlName)
    {
        String q = "select r from Misccmdprocrequest r where r.arlid = " +
                   "ANY(select a.id from Arl a where a.name = :arlname)";
    	
  	    return entityManager.createQuery(q, Misccmdprocrequest.class).setParameter("arlname", arlName)
  	                                                    .getResultList();	
    }

    // Rsiimagingrequest
    public List<Rsiimagingrequest> listRsiimagingrequests(String arlName)
    {
        String q = "select r from Rsiimagingrequest r where r.arlid = " +
                   "ANY(select a.id from Arl a where a.name = :arlname)";
    	
  	    return entityManager.createQuery(q, Rsiimagingrequest.class).setParameter("arlname", arlName)
  	                                                    .getResultList();	
    }

    // Rtcmdprocrequest
    public List<Rtcmdprocrequest> listRtcmdprocrequests(String arlName)
    {
        String q = "select r from Rtcmdprocrequest r where r.arlid = " +
                   "ANY(select a.id from Arl a where a.name = :arlname)";
    	
  	    return entityManager.createQuery(q, Rtcmdprocrequest.class).setParameter("arlname", arlName)
  	                                                    .getResultList();	
    }

    // Uplinkrequest
    public List<Uplinkrequest> listUplinkrequests(String arlName)
    {
        String q = "select r from Uplinkrequest r where r.arlid = " +
                   "ANY(select a.id from Arl a where a.name = :arlname)";
    	
  	    return entityManager.createQuery(q, Uplinkrequest.class).setParameter("arlname", arlName)
  	                                                    .getResultList();	
    }

    // Xcrossrequest
    public List<Xcrossrequest> listXcrossrequests(String arlName)
    {
        String q = "select r from Xcrossrequest r where r.arlid = " +
                   "ANY(select a.id from Arl a where a.name = :arlname)";
    	
  	    return entityManager.createQuery(q, Xcrossrequest.class).setParameter("arlname", arlName)
  	                                                    .getResultList();	
    }

}
