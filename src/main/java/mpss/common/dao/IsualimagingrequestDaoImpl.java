package mpss.common.dao;

import org.springframework.stereotype.Repository;
import mpss.common.jpa.Isualimagingrequest;

@Repository("isualimagingrequestDao")
public class IsualimagingrequestDaoImpl extends GenericDaoImpl<Isualimagingrequest> implements IsualimagingrequestDao {

    public IsualimagingrequestDaoImpl() {
        super(Isualimagingrequest.class);
    }    
}
