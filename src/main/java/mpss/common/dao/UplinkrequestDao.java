package mpss.common.dao;


import java.util.List;
import mpss.common.jpa.Uplinkrequest;

public interface UplinkrequestDao extends GenericDao<Uplinkrequest> {
	public List<Uplinkrequest> getByArlId(int arlId);
}
