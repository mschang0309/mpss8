package mpss.common.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.*;

@Repository("schedulerDao")
@SuppressWarnings("unchecked")
public class SchedulerDaoImpl implements SchedulerDao {

    private EntityManager entityManager;
    
    public SchedulerDaoImpl() {
    }
    
    // for Spring injection
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    // get session by name
    public Session getSession(String name)
    {
    	  String q = "select s from Session s where s.name = :name";
    	  return entityManager.createQuery(q, Session.class).setParameter("name", name).getSingleResult();
    }
    
    // list all activities
    public List<Activity> listActivities() {
    	  String q = "select a from Activity a order by a.starttime";
    	  return entityManager.createQuery(q, Activity.class).getResultList();
    }

    // list all activities within a session
    public List<Activity> listSessionActivities(Session s) {
    	  String q = "select a from Activity a where a.sessionid = :sessionid order by a.starttime";
    	  return entityManager.createQuery(q, Activity.class).setParameter("sessionid", s.getId()).getResultList();
    }

    // list all extents within a session
    public List<Extent> listSessionExtents(Session s) {
    	  String q = "select ext from Extent ext where ext.activityid = ANY(select a.id from Activity a where a.sessionid = :sessionid) ";
          q += "order by ext.starttime";    	  
          return entityManager.createQuery(q, Extent.class).setParameter("sessionid", s.getId()).getResultList();
    }
    
    // list all contacts within a session's extents(MON) and type='SBAND'  , modify by jeff 20170821 add " and a.arlentrytype='MON' "
    public List<Contact> listSessioncontacts(Session s) {
    	  String q = "select c.* from contact c left join site s on s.id=c.siteid where c.id in" +
    			  " (select distinct contactid from extent e left join activity a on e.id=a.extentid and a.arlentrytype='MON' " +
    			  " where e.contactid <> 0 and a.sessionid =" + s.getId().intValue() + ") and s.sitetype ='SBAND'";
          return entityManager.createNativeQuery(q, Contact.class).getResultList();
    }
}
