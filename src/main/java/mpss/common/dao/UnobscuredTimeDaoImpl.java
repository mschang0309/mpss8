package mpss.common.dao;

import org.springframework.stereotype.Repository;
import mpss.common.jpa.Unobscuredtime;


@Repository("unobscuredTimeDao")
public class UnobscuredTimeDaoImpl extends GenericDaoImpl<Unobscuredtime> implements UnobscuredTimeDao {

    public UnobscuredTimeDaoImpl() {
        super(Unobscuredtime.class);
    }    
}
