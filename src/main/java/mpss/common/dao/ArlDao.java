package mpss.common.dao;

import java.util.List;
import mpss.common.jpa.Arl;

public interface ArlDao extends GenericDao<Arl> {
   public Arl getByName(String name);
   public Arl getById(int id);
   public Arl getArlById(long id);
   public long getNextId();
   public List<Arl> getByType(String arltype);
   
}
