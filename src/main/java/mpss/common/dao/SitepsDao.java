package mpss.common.dao;

import java.util.List;

import mpss.common.jpa.Siteps;

public interface SitepsDao extends GenericDao<Siteps> {

	public long getNextId();
	public Siteps getById(long id);
	public List<Siteps> getBySiteId(int siteid);
	public Siteps getBySiteIdNPsId(int siteId,int psId);
}
