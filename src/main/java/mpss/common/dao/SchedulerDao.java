package mpss.common.dao;

import java.util.List;

//import mpss.common.jpa.Arl;
//import mpss.common.jpa.Isualimagingrequest;
//import mpss.common.jpa.Isualprocrequest;
//import mpss.common.jpa.Maneuverrequest;
//import mpss.common.jpa.Misccmdprocrequest;
//import mpss.common.jpa.PsMissionTimeLine;
//import mpss.common.jpa.Rsiimagingrequest;
//import mpss.common.jpa.Rtcmdprocrequest;
//import mpss.common.jpa.Uplinkrequest;
//import mpss.common.jpa.Xcrossrequest;
import mpss.common.jpa.*;

public interface SchedulerDao {
	  // Sessions
    public Session getSession(String name);
    
	  // Activity
	  public List<Activity> listActivities();  
    public List<Activity> listSessionActivities(Session s);
    public List<Extent> listSessionExtents(Session s);
    public List<Contact> listSessioncontacts(Session s);
}