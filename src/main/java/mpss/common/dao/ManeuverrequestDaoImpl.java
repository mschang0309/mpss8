package mpss.common.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Maneuverrequest;

@Repository("maneuverrequestDao")
public class ManeuverrequestDaoImpl extends GenericDaoImpl<Maneuverrequest> implements ManeuverrequestDao {

    public ManeuverrequestDaoImpl() {
        super(Maneuverrequest.class);
    }    
    
    public Maneuverrequest getManeuverrequest(int id){
    	try{
			String q = "select s from Maneuverrequest s where s.id = :id";			
			
			return entityManager.createQuery(q, Maneuverrequest.class)
					.setParameter("id", id).getSingleResult();
		}catch(Exception ex){
			logger.info("Maneuverrequest getManeuverrequest() ex="+ex.getMessage());
			return null;
		}
    }
    
    public List<Maneuverrequest> getByArlId(int arlId)
    {
  	  String q = "select man from Maneuverrequest man where man.arlid = :arlid";
  	  return entityManager.createQuery(q, Maneuverrequest.class).setParameter("arlid", arlId).getResultList();
    }
    
    @Override
	public long getNextId() {
		try{
			return (Long)entityManager.createNativeQuery("select nextval('maneuverrequests_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("ManeuverrequestDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}
}
