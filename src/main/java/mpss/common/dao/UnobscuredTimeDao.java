package mpss.common.dao;

import mpss.common.jpa.Unobscuredtime;

public interface UnobscuredTimeDao extends GenericDao<Unobscuredtime> {

}
