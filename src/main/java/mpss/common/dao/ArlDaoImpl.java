package mpss.common.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Arl;

@Repository("arlDao")
public class ArlDaoImpl extends GenericDaoImpl<Arl> implements ArlDao {

	public ArlDaoImpl() {
		super(Arl.class);
	}

	public Arl getByName(String name) {
		try{
			String q = "select a from Arl a where a.name = :name";
			return entityManager.createQuery(q, Arl.class).setParameter("name", name).getSingleResult();
		}catch(Exception ex){
			logger.info("ArlDaoImpl getByName() , name="+name+", ex="+ex.getMessage());
			return null;
		}
	}

	@Override
	public Arl getById(int id) {
		try{
			String q = "select a from Arl a where a.id = :id";
			return entityManager.createQuery(q, Arl.class).setParameter("id", id).getSingleResult();
		} catch(NoResultException e) {
			logger.info("ArlDaoImpl getById() , id="+id+"+, ex="+e.getMessage());
	        return null;
	    }
	}

	@Override
	public Arl getArlById(long id) {
		Arl s = entityManager.find(Arl.class, id);
		return s;
	}

	@Override
	public long getNextId() {
		try{
			return (Long)entityManager.createNativeQuery("select nextval('arl_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("ArlDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}

	@Override
	public List<Arl> getByType(String arltype) {
		String q = "select a from Arl a where a.arltype = :arltype";
		return entityManager.createQuery(q, Arl.class).setParameter("arltype", arltype).getResultList();
	}

	
}
