package mpss.common.dao;

import java.util.List;

import mpss.common.jpa.Extentactivitytimerange;


public interface ExtentActivityTimerangeDao extends GenericDao<Extentactivitytimerange> {

	public long getNextId();
	
	public Extentactivitytimerange getExtentById(long id);
	
	public List<Extentactivitytimerange> getExtentByparentid(long id);
	
	public List<Extentactivitytimerange> getExtentByparentid4Copy(long id) ;
	
}
