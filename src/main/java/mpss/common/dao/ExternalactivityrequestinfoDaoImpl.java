package mpss.common.dao;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Externalactivityrequestinfo;

@Repository("externalactivityrequestinfoDao")
@SuppressWarnings("unchecked")
public class ExternalactivityrequestinfoDaoImpl extends GenericDaoImpl<Externalactivityrequestinfo> implements ExternalactivityrequestinfoDao {

    public ExternalactivityrequestinfoDaoImpl() {
        super(Externalactivityrequestinfo.class);
    }
    
    @Override
	public long getNextId() {
    	try{
    		return (Long)entityManager.createNativeQuery("select nextval('externalactivityrequestinfo_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("ExternalactivityrequestinfoDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}
    
    public Externalactivityrequestinfo getByArlId(int arlId)
    {
    	try{
    		String q = "select ear from Externalactivityrequestinfo ear where ear.arlid = :arlid";
    	  	return entityManager.createQuery(q, Externalactivityrequestinfo.class).setParameter("arlid", arlId).getSingleResult();
    	}catch(Exception ex){
			logger.info("ExternalactivityrequestinfoDaoImpl getByArlId() ex,arlId ="+arlId);
			return null;
		}
  	  
    }
    
    public List<Externalactivityrequestinfo> getBySatNameNBranch(String satName, boolean isbranch, String instrument)
    {
    	String strBr = isbranch ? "ear.branchid > 0" : "ear.branchid = 0";
    	String q = "select ear from Externalactivityrequestinfo ear where ear.satellite =" +
    		":satName and ear.instrument = :instrument and " + strBr + " order by ear.arlid";
    	return entityManager.createQuery(q, Externalactivityrequestinfo.class)
    		.setParameter("satName", satName)
    		.setParameter("instrument", instrument).getResultList();
    }
    
	public long getAfterTimeMTL(Date time, int sessionId, boolean isBranch, String instrument) {
		try{
			int branchId = isBranch ? sessionId : 0;
			String q ="select count(*) from externalactivityrequestinfo" +
					" right join (select E.arlid from activity A left join (" +
					" select min(id) as arlentryid,arlid from maneuverrequests group by arlid" +
					" union select min(id) as arlentryid,arlid from rsiimagingrequests group by arlid" +
					" union select min(id) as arlentryid,arlid from xcrossrequests group by arlid" +
					" union select min(id) as arlentryid,arlid from isualprocrequests group by arlid" +
					" union select min(id) as arlentryid,arlid from aipprocrequests group by arlid" +
					" ) E using(arlentryid) where arlid is not null and sessionid=" + sessionId +
					" group by arlid ) B using (arlid) where starttime > '" + time + "'" +
					" and instrument = '" + instrument + "' and branchid =" + branchId;
			return (long)entityManager.createNativeQuery(q).getSingleResult();
		}catch(Exception ex){
			logger.info("ExternalactivityrequestinfoDaoImpl getAfterTimeMTL() ex="+ex.getMessage());
			return -1;
		}
	}
	
	
	public List<Long> getArlIdByTime(String starttime, String endtime){
		List<Long> alrs = new ArrayList<Long>();
		try{
			//String q ="Select arlid from externalactivityrequestinfo where endtime > '" + starttime + "' and branchid=0 ";
			String q ="Select arlid from externalactivityrequestinfo where starttime < '"+starttime+"' and  endtime < '" + endtime + "' and branchid=0 ";
			System.out.println("getArlIdByTime sql q = "+q);
			List<Integer> datas =entityManager.createNativeQuery(q).getResultList();
			for(Integer myl: datas){
				alrs.add(new Long(myl.intValue()));				
			}
			return alrs;
			//return ((Integer) entityManager.createNativeQuery(q).getResultList()).longValue();
		}catch(Exception ex){
			logger.info("ExternalactivityrequestinfoDaoImpl getArlIdByTime() ex="+ex.getMessage());
			return alrs;
		}
		
	}
}
