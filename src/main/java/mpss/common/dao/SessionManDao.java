package mpss.common.dao;

import java.sql.Timestamp;
import java.util.List;
import mpss.common.jpa.Dutycycle;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Parameterset;
import mpss.common.jpa.Recorderp;
import mpss.common.jpa.Satellite;
import mpss.common.jpa.Satellitep;
import mpss.common.jpa.Session;
import mpss.common.jpa.Site;
import mpss.common.jpa.Siteps;
import mpss.common.jpa.Arl;

public interface SessionManDao {
     //= session
	 public Session getSessionByName(String name);
	 public Session getSessionById(int sessionId);
	   
	 //= cross references
	 // satellite
	 public List<Satellite> getRefSatellites(String sessname);
     public Satellite getSatelliteByName(String satName);
     public Satellite getSatellite(int sessinId);
     //public List<Integer> getSatelliteXrefs(String name);
	   // satellite ARLs
     public List<Arl> getRefSatelliteARLsByName(String sessionName, String satName);
     public List<Arl> getRefSatelliteARLsById(int sessionid, int satelliteid);
     // sites
     public List<Site> getRefSites(String sessname);
     public List<Site> getRefSitesById(int sessionId);
       
     //= parameter sets
     public Parameterset getParameterset(String sessname);
     public Satellitep getSatellitep(String psName, String satName);
     public Dutycycle getDutycycle(int sessionId, int satId, String actType, String actSubType);
 	 public Dutycycle getDutycycle(String psName, String satName, String actType, String actSubType);
     public Siteps getSitep(String psName, String siteName);
     public Satellitep getSatellitep(int sessionId,int satId);
     
     //= extent
     public List<Extent> getDutyCycleExtents(int scid,int branchid, Timestamp ts);

     //= recorderp
     public Recorderp getRecorderp(String psName, String satName, String recName);
     public Recorderp getRecorderp(int recId, int sessionId);
}
