package mpss.common.dao;

import java.sql.Timestamp;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.SessionSection;


@Repository("sessionSectionDao")
public class SessionSectionDaoImpl extends GenericDaoImpl<SessionSection> implements SessionSectionDao {

    public SessionSectionDaoImpl() {
        super(SessionSection.class);
    }
    
    public SessionSection getSectionbyTime(Timestamp time) {
    	try{
    		String q = "select s.* from sessionsection s where '" + time + "' >= s.starttime and '" +
    			time + "' < s.stoptime";
    		return (SessionSection) entityManager.createNativeQuery(q, SessionSection.class).getSingleResult();
    	} catch(NoResultException ex) {
    		logger.info("SessionSectionDaoImpl getSectionbyTime() ex="+ex.getMessage());
	        return null;
	    }
    }
}
