package mpss.common.dao;


import mpss.common.jpa.Globalparameter;



public interface GlobalparametersDao extends GenericDao<Globalparameter>{
	
	public long getNextId();
	public Globalparameter getById(long id);	

}
