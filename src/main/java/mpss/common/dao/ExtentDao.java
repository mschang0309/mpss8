package mpss.common.dao;

import java.sql.Timestamp;
import java.util.List;
import mpss.common.jpa.Extent;
import mpss.util.timeformat.TimeRange;

public interface ExtentDao  extends GenericDao<Extent> {
	public long getNextId();
	public Extent getExtentById(long id);
	public Extent getExtentByParams(String starttime, String endtime, int branchID, int transmitterID, int antennaID, int recorderID, int recorderDelta, int secRecorderDelta, int filename, int secFilename);
	public List<Extent> getLastRecord(int recorderID, int branchID, String time);
	public List<Extent> getLastRecordByType(int recorderID, int branchID, String time, String type);
	public List<Extent> getByContactId(int contactId);
	public List<Extent> getByBranchId(int branchId);
	public List<Extent> getBySatelliteId(int satelliteId);
	public List<Extent> getBySiteId(int siteId);
	public List<Extent> getByTransmitterId(int transmitterId);
	public List<Extent> getByRecorderId(int recorderid);
	public List<Extent> getByAntennaId(int antennaid);
	public List<Extent> getByBranchIdNTimeRange(int branchId, String trStart, String trEnd);
	public Extent getByActId(int actId);
	public List<Extent> getStartWithinTimeRange(TimeRange tr, int branchid);
	public List<Extent> getExtentsByRSICapacity(String entryType,Timestamp tval,int recId, int sessId);
	public boolean getContactHasExtent(long contactid);
}
