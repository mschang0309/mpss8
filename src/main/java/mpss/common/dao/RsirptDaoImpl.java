package mpss.common.dao;


import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Rsirpt;
@Repository("rsirptDao")
public class RsirptDaoImpl extends GenericDaoImpl<Rsirpt> implements RsirptDao {

    public RsirptDaoImpl() {
        super(Rsirpt.class);
    }

	public Rsirpt getByName(String name) {
		try{
			String q = "select r from Rsirpt r where r.filename = :name";
			return entityManager.createQuery(q, Rsirpt.class).setParameter("name", name).getSingleResult();
    	} catch(NoResultException ex) {
    		logger.info("RsirptDaoImpl getByName() ex="+ex.getMessage());
	        return null;
	    }
	}
}
