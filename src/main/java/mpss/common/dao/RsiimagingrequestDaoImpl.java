package mpss.common.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Rsiimagingrequest;

@Repository("rsiimagingrequestDao")
public class RsiimagingrequestDaoImpl extends GenericDaoImpl<Rsiimagingrequest> implements RsiimagingrequestDao {

	public RsiimagingrequestDaoImpl() {
        super(Rsiimagingrequest.class);
    } 
	
    @Override
	public Rsiimagingrequest getRsiImagingrequest(int id) {
    	try{
			String q = "select s from Rsiimagingrequest s where s.id = :id";			
			
			return entityManager.createQuery(q, Rsiimagingrequest.class)
					.setParameter("id", id).getSingleResult();
		}catch(Exception ex){
			logger.info("Rsiimagingrequest getRsiImagingrequest() ex="+ex.getMessage());
			return null;
		}
		
	}

    public List<Rsiimagingrequest> getByArlId(int arlId)
    {
  	  String q = "select rsi from Rsiimagingrequest rsi where rsi.arlid = :arlid";
  	  return entityManager.createQuery(q, Rsiimagingrequest.class).setParameter("arlid", arlId).getResultList();
    }
    
    public Rsiimagingrequest getById(int Id)
    {
  	  String q = "select rsi from Rsiimagingrequest rsi where rsi.id = :id";
  	  return entityManager.createQuery(q, Rsiimagingrequest.class).setParameter("id", Id).getSingleResult();
    }
    
    @Override
	public long getNextId() {
		try{
			return (Long)entityManager.createNativeQuery("select nextval('rsiimagingrequests_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("RsiimagingrequestDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}
}
