package mpss.common.dao;

 

import mpss.common.jpa.Parameterset;

public interface ParametersetDao extends GenericDao<Parameterset> {
	
 
	public Parameterset getByID(long id);
	 
}
