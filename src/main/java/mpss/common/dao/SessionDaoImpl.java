package mpss.common.dao;

import org.springframework.stereotype.Repository;
import com.google.common.collect.Range;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import mpss.common.jpa.Session;
import mpss.util.timeformat.TimeRange;

@Repository("sessionDao")
@SuppressWarnings("unchecked")
public class SessionDaoImpl extends GenericDaoImpl<Session> implements SessionDao {

    public SessionDaoImpl() {
        super(Session.class);
    }

    public List<Session> listSessions() {
    	  String q = "select s from Session s";
    	  return entityManager.createQuery(q, Session.class).getResultList();
    }

    public Session getSession(String name) {
    	try{
    		
    		String q = "select s from Session s where s.name = :name";
      	  return entityManager.createQuery(q, Session.class).setParameter("name", name).getSingleResult();
		}catch(Exception ex){
			logger.info("SessionDaoImpl getSession() ex="+ex.getMessage());
			return null;
		}
    	  
    }
    
    public List<String> getSatelliteNames(String name)
    {
    	  String q = "select s.name from Satellite s where s.id = " +
    	             "ANY(select xref.satelliteid from Sessionsatxref xref where xref.sessionid = " +
			             "ANY(select s.id from Session s where s.name = :name))";
	      return entityManager.createQuery(q, String.class).setParameter("name", name).getResultList();
    }

    public List<String> getSiteNames(String name)
    {
    	  String q = "select s.name from Site s where s.id = " +
    	             "ANY(select xref.siteid from Sessionsitexref xref where xref.sessionid = " +
			             "ANY(select s.id from Session s where s.name = :name))";
	      return entityManager.createQuery(q, String.class).setParameter("name", name).getResultList();
    }

    public List<String> getSatelliteArlNames(String name, String satName)
    {
    	  String q = "select arl.name from Arl arl where arl.id = " +
    	             "ANY(select xref.arlid from Sessionsatarlxref xref " +
    	             "where xref.sessionid = ANY(select sess.id from Session sess where sess.name = :name) " +
    	             "and xref.satelliteid = ANY(select sat.id from Satellite sat where sat.name = :satName))";
	      return entityManager.createQuery(q, String.class).setParameter("name", name).getResultList();
    }
    
    public List<String> getSessionNamesbyRange(Range<Timestamp>range, int isbranch) {
  	  String q = "select s.name from Session s where s.starttime between :lowerrange and :uprange and s.isbranch = :isbranch";
  	  return entityManager.createQuery(q, String.class)
  			  .setParameter("lowerrange", range.lowerEndpoint())
  			  .setParameter("uprange", range.upperEndpoint())
  			  .setParameter("isbranch", isbranch).getResultList();
  }

	@Override
	public Session getSessionById(long id) {
		try{
		Session s = entityManager.find(Session.class, id);
		return s;
		}catch(Exception ex){
			logger.info("SessionDaoImpl getSessionById() ex="+ex.getMessage());
			return null;
		}

	}

	@Override
	public long getMaxId() {

		try{
			return (Long)entityManager.createQuery("select max(u.id) id from Session u").getSingleResult();
		}catch(Exception ex){
			logger.info("SessionDaoImpl getMaxId() ex="+ex.getMessage());
			return 0;
		}
  	  
  	  	

	}

	@Override
	public long getNextId() {
		try{
			return (Long)entityManager.createNativeQuery("select nextval('sessions_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("SessionDaoImpl getMaxId() ex="+ex.getMessage());
			return 0;
		}
		
	}

	@Override
	public Collection<Session> getSessionByRangeNBranch(String starttime, String endtime, int isbranch) {
		String q = "select a.* from sessions a where a.starttime>='" + starttime + "' and a.endtime<='" + endtime + "' and a.isbranch=" + isbranch + " order by a.starttime";
  	  	
		return entityManager.createNativeQuery(q, Session.class).getResultList();
	}

	@Override
	public Collection<Session> getSessionByTimeNBranch(String time, int isbranch) {
		String q = "select a.* from sessions a where a.starttime<='" + time + "' and a.endtime>='" + time + "' and a.isbranch=" + isbranch + " order by a.starttime";
  	  	
		return entityManager.createNativeQuery(q, Session.class).getResultList();
	}

	@Override
	public Date getMinStarttime() {
		try{
			String q = "select min(s.starttime) from Session s";
	  	  	return entityManager.createQuery(q, Date.class).getSingleResult();
		}catch(Exception ex){
			logger.info("SessionDaoImpl getMinStarttime() ex="+ex.getMessage());
			return null;
		}
		
	}

	@Override
	public Date getMaxEndtime() {
		try{
			String q = "select max(s.endtime) from Session s";
	  	  	return entityManager.createQuery(q, Date.class).getSingleResult();
		}catch(Exception ex){
			logger.info("SessionDaoImpl getMaxEndtime() ex="+ex.getMessage());
			return null;
		}
		
	}

    public List<Session> getSessionsbyRange(TimeRange range, int isbranch) {
  	  String q = "select s from Session s where s.starttime < :uprange and s.endtime > :lowerrange" + 
  			  " and s.needsvalidation = 0 and s.isbranch = :isbranch order by s.starttime";
  	  return entityManager.createQuery(q, Session.class)
  			  .setParameter("lowerrange", range.first)
  			  .setParameter("uprange", range.second)
  			  .setParameter("isbranch", isbranch).getResultList();
  }
    
    public List<Session> getSessionsbyRangeForRTS(TimeRange range, int isbranch){
    	String q = "select s from Session s where s.starttime < :uprange and s.endtime > :lowerrange" + 
    			  " and s.isbranch = :isbranch order by s.starttime";
    	  return entityManager.createQuery(q, Session.class)
    			  .setParameter("lowerrange", range.first)
    			  .setParameter("uprange", range.second)
    			  .setParameter("isbranch", isbranch).getResultList();
    }
    
    public Session getSessionbyTime(Timestamp time, int isbranch, int satId) {
    	try{
    		
//    		String q = "select s from Session s where starttime <= :time and endtime > :time and isbranch = :isbranch";
//    		return entityManager.createQuery(q, Session.class).setParameter("time", time).setParameter("isbranch", isbranch).getSingleResult();
    		String q = "select s.* from sessions s right join sessionsatxref sr on s.id=sr.sessionid where starttime <= '"
    			+ time + "' and endtime > '" + time + "' and isbranch =" + isbranch + " and satelliteid =" + satId;
    		return (Session) entityManager.createNativeQuery(q, Session.class).getSingleResult();
		}catch(Exception ex){
			logger.info("SessionDaoImpl getSessionbyTime() ex="+ex.getMessage());
			return null;
		}
    	  
    }
    
	public List<String> getScheduledCount(int sessionId, int isbranch, int satId) {
		try{
			String q = "select name from sessions s right join sessionsatxref sref on s.id=sref.sessionid where"
				+ " satelliteid =" + satId + " and starttime > (select starttime from sessions where id="
				+ sessionId + ") and isbranch = " + isbranch + " and coldscheduled = 1";
			return (List<String>)entityManager.createNativeQuery(q).getResultList();
		}catch(Exception ex){
			logger.info("SessionDaoImpl getScheduledCount() ex="+ex.getMessage());
			return null;
		}
		
	}
	
    public Session getSessionContainTime(Timestamp time, int isBranch) {
    	try{
    		String q = "select s from Session s where :time between s.starttime and s.endtime and s.isbranch = :isBranch";
      	  return entityManager.createQuery(q, Session.class).setParameter("time", time)
      			  .setParameter("isBranch", isBranch).getSingleResult();
    	}catch(NoResultException e) {
    		logger.info("SessionDaoImpl NoResultException() e="+e.getMessage());			
	    	return null;
	    }catch(Exception ex){
			logger.info("SessionDaoImpl NoResultException() ex="+ex.getMessage());	
			return null;
		} 
    }

	

	
}
