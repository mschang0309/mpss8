package mpss.common.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Sessionsatxref;



@Repository("sessionsatxrefDao")
public class SessionsatxrefDaoImpl extends GenericDaoImpl<Sessionsatxref> implements SessionsatxrefDao {

    public SessionsatxrefDaoImpl() {
        super(Sessionsatxref.class);
    }

	@Override
	public Sessionsatxref getById(long id) {
		try{
			Sessionsatxref site = entityManager.find(Sessionsatxref.class, id);
			return site;
		}catch(Exception ex){
			logger.info("SessionsatxrefDaoImpl getById() ex="+ex.getMessage());
			return null;
		}
		
	}

	@Override
	public long getNextId() {
		try{
			return (Long) entityManager.createNativeQuery("select nextval('sessionsatxref_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("SessionsatxrefDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}

	@Override
	public List<Sessionsatxref> getBySessionId(int sessionId) {
		String q = "select s from Sessionsatxref s where s.sessionid = :sessionId ";
		return entityManager.createQuery(q, Sessionsatxref.class).setParameter("sessionId", sessionId).getResultList();
	}
    
}
