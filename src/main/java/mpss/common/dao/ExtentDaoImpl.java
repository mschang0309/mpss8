package mpss.common.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Extent;
import mpss.util.timeformat.TimeRange;

@Repository("extentDao")
@SuppressWarnings("unchecked")
public class ExtentDaoImpl extends GenericDaoImpl<Extent> implements ExtentDao {

    public ExtentDaoImpl() {
        super(Extent.class);
    }

    @Override
	public long getNextId() {
    	try{
    		return (Long)entityManager.createNativeQuery("select nextval('extent_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("ExtentDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}

	@Override
	public Extent getExtentById(long id) {
		try{
			Extent ext = entityManager.find(Extent.class, id);
			return ext;
		}catch(Exception ex){
			logger.info("ExtentDaoImpl getExtentById() ex="+ex.getMessage());
			return null;
		}
		
	}
	
	public Extent getByActId(int actId) {
		String q = "select e from Extent e where e.activityid = :actId";
		try{
			return entityManager.createQuery(q, Extent.class).setParameter("actId", actId).getSingleResult();
		}catch(Exception ex){
			logger.info("ExtentDaoImpl getByActId() ex="+ex.getMessage());
			return null;
		}
		
	}

	@Override
	public Extent getExtentByParams(String starttime, String endtime, int branchID, int transmitterID, int antennaID, int recorderID, int recorderDelta, int secRecorderDelta, int filename,
			int secFilename) {
		Extent extent =null;
		try{
			String q = "select a.* from extent a where a.starttime='" + starttime + "' and a.endtime='" + endtime + "' and a.branchid=" + branchID + " and a.transmitterid=" + transmitterID + " and a.antennaid=" + antennaID + " and a.recorderid=" + recorderID + " and a.recorderdelta=" + recorderDelta + " and a.secondrecorderdelta=" + secRecorderDelta + " and a.filename=" + filename + "  and a.secondfilename=" + secFilename + " order by a.starttime";
			extent =(Extent) entityManager.createNativeQuery(q, Extent.class).getSingleResult();
			return extent;
		}catch(Exception ex){
			logger.info("extentDaoImpl getExtentByParams ex="+ex.getMessage());
			return null;
		}
		
	}

	@Override
	public List<Extent> getLastRecord(int recorderID, int branchID, String time) {
		String q = "select a.* from extent a where a.endtime<'" + time + "' and a.branchid=" + branchID + "  and a.recorderid=" + recorderID + " order by a.starttime desc";
		
		return entityManager.createNativeQuery(q, Extent.class).getResultList();
	}

	@Override
	public List<Extent> getLastRecordByType(int recorderID, int branchID, String time, String type) {
		String q = "select a.* from extent a, activity b where a.endtime<'" + time + "' and a.branchid=" + branchID + "  and a.recorderid=" + recorderID + "  and a.activityid=b.id and b.arlentrytype='" + type + "' order by a.starttime desc";
		
		return entityManager.createNativeQuery(q, Extent.class).getResultList();
	}

	@Override
	public List<Extent> getByContactId(int contactId) {
		String q = "select a.* from extent a where a.contactid=" + contactId + " order by a.starttime";
		
		return entityManager.createNativeQuery(q, Extent.class).getResultList();
	}

	@Override
	public List<Extent> getByBranchId(int branchId) {
		String q = "select a.* from extent a where a.branchid=" + branchId + " order by a.starttime";
		
		return entityManager.createNativeQuery(q, Extent.class).getResultList();
	}

	@Override
	public List<Extent> getBySatelliteId(int satelliteId) {
		String q = "select a.* from extent a, contact b where a.contactid=b.id and b.satelliteid=" + satelliteId + " order by a.starttime";
		
		return entityManager.createNativeQuery(q, Extent.class).getResultList();
	}

	@Override
	public List<Extent> getBySiteId(int siteId) {
		String q = "select a.* from extent a, contact b where a.contactid=b.id and b.siteid=" + siteId + " order by a.starttime";
		
		return entityManager.createNativeQuery(q, Extent.class).getResultList();
	}

	@Override
	public List<Extent> getByTransmitterId(int transmitterId) {
		String q = "select a.* from extent a where a.transmitterid=" + transmitterId + " order by a.starttime";
		
		return entityManager.createNativeQuery(q, Extent.class).getResultList();
	}

	@Override
	public List<Extent> getByRecorderId(int recorderid) {
		String q = "select a.* from extent a where a.recorderid=" + recorderid + " order by a.starttime";
		
		return entityManager.createNativeQuery(q, Extent.class).getResultList();
	}

	@Override
	public List<Extent> getByAntennaId(int antennaid) {
		String q = "select a.* from extent a where a.antennaid=" + antennaid + " order by a.starttime";
		
		return entityManager.createNativeQuery(q, Extent.class).getResultList();
	}

	@Override
	public List<Extent> getByBranchIdNTimeRange(int branchId, String trStart, String trEnd) {
       String q = "select a.* from extent a where a.starttime>'" + trStart + "' and a.endtime<'" + trEnd + "' and a.branchid=" + branchId + " order by a.starttime";        
		return entityManager.createNativeQuery(q, Extent.class).getResultList();
	}
	
	public List<Extent> getStartWithinTimeRange(TimeRange tr, int branchid) {
        String q = "select a.* from extent a where a.starttime between '" + tr.first + "' and '" + tr.second + "' and a.branchid=" + branchid + " order by a.starttime";
  	  	
		return entityManager.createNativeQuery(q, Extent.class).getResultList();
	}
	
	public List<Extent> getExtentsByRSICapacity(String entryType,Timestamp tval,int recId, int sessId) {
        String typeStr;
        if (entryType == "DELETE")
        	typeStr = "'DELETE' order by e.starttime";
        else if (entryType == "ISUAL")
        	typeStr = "'ISUAL' and (a.actsubtype='REC' or a.actsubtype='PBK') order by e.starttime";
        else if (entryType == "DDT")
        	typeStr = "'RSI' and (a.actsubtype='DDT') order by e.starttime";
        else
        	typeStr = "'RSI' and (a.actsubtype='REC' or a.actsubtype='PBK') order by e.starttime";
        
		String q = "select e.* from (select * from extent where starttime < '" + tval + "' and recorderid = " +
        		recId + ") e left join activity a on a.id=e.activityid where a.sessionid=" +
				sessId + " and a.arlentrytype=" + typeStr;
         
		return entityManager.createNativeQuery(q, Extent.class).getResultList();
	}
	
	public boolean getContactHasExtent(long contactid){
		 String q = "select a.* from extent a where a.contactid='" + contactid + "' ";
		 List<Extent> list =entityManager.createNativeQuery(q, Extent.class).getResultList();
	  	 if(list.size()>0){	  		
	  		 return true;
	  	 }else{	  		
	  		return false;
	  	 }		
		
	}
}