package mpss.common.dao;

import java.util.*;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Recorder;

@Repository("recorderDao")
public class RecorderDaoImpl extends GenericDaoImpl<Recorder> implements RecorderDao {

    public RecorderDaoImpl() {
        super(Recorder.class);
    }
    
    public List<Recorder> listRecorders(int satelliteid) {
    	  String q = "select r from Recorder r where r.satelliteid = :satelliteid";
    	  return entityManager.createQuery(q, Recorder.class).setParameter("satelliteid", satelliteid).getResultList();
    }

    public Recorder getRecorder(int satelliteid, String name) {
    	  String q = "select r from Recorder r where r.name = :name and r.satelliteid = :satelliteid";
    	  return entityManager.createQuery(q, Recorder.class).setParameter("satelliteid", satelliteid).setParameter("name", name).getSingleResult();
    }
}
