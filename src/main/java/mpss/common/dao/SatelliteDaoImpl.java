package mpss.common.dao;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.HashSet;
import java.util.Set;

import mpss.common.jpa.Satellite;
import mpss.common.jpa.Recorder;

@Repository("satelliteDao")
public class SatelliteDaoImpl extends GenericDaoImpl<Satellite> implements SatelliteDao {

	public SatelliteDaoImpl() {
		super(Satellite.class);
	}

	public Satellite getByName(String name) {
		String q = "select s from Satellite s where s.name = :name";
		return entityManager.createQuery(q, Satellite.class).setParameter("name", name).getSingleResult();
	}

	public Satellite getByOps4number(String ops4number) {
		String q = "select s from Satellite s where s.ops4number = :ops4number";
		return entityManager.createQuery(q, Satellite.class).setParameter("ops4number", ops4number).getSingleResult();
	}

	public Satellite getByOpsnumber(String opsnumber) {
		String q = "select s from Satellite s where s.opsnumber = :opsnumber";
		return entityManager.createQuery(q, Satellite.class).setParameter("opsnumber", opsnumber).getSingleResult();
	}

	public Set<Recorder> getRecorders(Long satId) {
		Satellite sat = entityManager.find(Satellite.class, satId);
		return (sat == null) ? new HashSet<Recorder>() : sat.getRecorders();
	}

	@Override
	public Satellite getSatelliteById(long id) {
		Satellite sat = entityManager.find(Satellite.class, id);
		return sat;
	}

	@Override
	public long getNextId() {
		return (Long) entityManager.createNativeQuery("select nextval('satellite_oid_seq')").getSingleResult();
	}

	@Override
	public List<Satellite> getAllSatellites() {
		String q = "select s from Satellite s";
  	  	return entityManager.createQuery(q, Satellite.class).getResultList();
	}
}
