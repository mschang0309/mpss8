package mpss.common.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import mpss.configFactory;
import mpss.common.jpa.*;

@Repository("psDao")
public class PsDaoImpl implements PsDao {

    private EntityManager entityManager;
    private Logger logger = Logger.getLogger(configFactory.getLogName());
    
    public PsDaoImpl() {
    }
    
    // for Spring injection
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    

    public List<Parameterset> listParametersets() {
  	  String q = "select ps from Parameterset ps";
  	  return entityManager.createQuery(q, Parameterset.class).getResultList();
    }

	public Parameterset getParameterset(String psName)
	{
		try{
			String q = "select ps from Parameterset ps where ps.name = :psname";
		  	  return entityManager.createQuery(q, Parameterset.class).setParameter("psname", psName).getSingleResult();	
		}catch(Exception ex){
			logger.info("PsDaoImpl getParameterset() ex="+ex.getMessage());
			return null;
		}
  	  
	}

	public String getParameterSetName(int psId)
	{
		try{
			 String q = "select ps.name from Parameterset ps where ps.id = :psid";
		  	  return entityManager.createQuery(q, String.class).setParameter("psid", psId).getSingleResult();	
		}catch(Exception ex){
			logger.info("PsDaoImpl getParameterSetName() ex="+ex.getMessage());
			return null;
		}
	  	 
		
	}
	
	public Satellitep getSatellitep(String psName, String satName)
	{
		try{
			 String q = "select sps from Satellitep sps where sps.psid = " + 
				     "ANY(select ps.id from Parameterset ps where ps.name = :psName) ";
		  return entityManager.createQuery(q, Satellitep.class).setParameter("psName", psName).getSingleResult();
		}catch(Exception ex){
			logger.info("PsDaoImpl getSatellitep() ex="+ex.getMessage());
			return null;
		}
	  	 
		
	}
	
	public List<Recorderp> listRecorderps(String psName)
	{
  	  String q = "select rps from Recorderp rps where rps.psid = " + 
			     "ANY(select ps.id from Parameterset ps where ps.name = :psName) ";
	  return entityManager.createQuery(q, Recorderp.class).setParameter("psName", psName).getResultList();
	}
	
	public Recorderp getRecorderp(String psName, String satName, String recName)
	{
		try{
			String q = "select rps from Recorderp rps where rps.psid = " + 
				     "ANY(select ps.id from Parameterset ps where ps.name = :psName) " +
	  			     "and rps.recorderid = " +
				     "ANY(select r.id from Recorder r where r.name = :recName and r.satelliteid = " + "" +
				     "ANY(select s.id from Satellite s where s.name = :satName))";
		  return entityManager.createQuery(q, Recorderp.class).setParameter("psName", psName).setParameter("satName", satName).setParameter("recName", recName).getSingleResult();
		}catch(Exception ex){
			logger.info("PsDaoImpl getRecorderp() ex="+ex.getMessage());
			return null;
		}	  
	}
	
	public Recorderp getRecorderp(int recId, int sessionId)
	{
		try{
			String q = "select rps from Recorderp rps where rps.psid = " + 
				     "(select s.psid from Session s where s.id = :sessionId) " +
	  			     "and rps.recorderid = :recId";
		  return entityManager.createQuery(q, Recorderp.class).setParameter("recId", recId).setParameter("sessionId", sessionId).getSingleResult();
		}catch(NoResultException ex){
			logger.info("PsDaoImpl getRecorderp() ex="+ex.getMessage());
			return null;
		}	  
	}

	public List<Dutycycle> listDutycycles(String psName, String satName)
	{
	  	  String q = "select dc from Dutycycle dc where dc.satpsid = " + 
				     "ANY(select sps.id from Satellitep sps where sps.psid =  " +
				     "ANY(select ps.id from Parameterset ps where ps.name = :psName) " +
	  			     "and sps.satelliteid = " +
		             "ANY(select s.id from Satellite s where s.name = :satName))";
	      return entityManager.createQuery(q, Dutycycle.class).setParameter("psName", psName).setParameter("satName", satName).getResultList();
	}
	
	public Dutycycle getDutycycle(String psName, String satName, String actType, String actSubType)
	{
  	  String q = "select dc from Dutycycle dc where dc.satpsid = " + 
			     "ANY(select sps.id from Satellitep sps where sps.psid =  " +
			     "ANY(select ps.id from Parameterset ps where ps.name = :psName) " +
  			     "and sps.satelliteid = " +
	             "ANY(select s.id from Satellite s where s.name = :satName)) " +
	             "and dc.activitytype = :actType " +
                 "and dc.activitysubtype = :actSubType";
	  return entityManager.createQuery(q, Dutycycle.class).setParameter("psName", psName).setParameter("satName", satName).setParameter("actType", actType).setParameter("actSubType", actSubType).getSingleResult();
	}
	
	public Dutycycle getDutycycle(int sessionId, int satId, String actType, String actSubType)
	{
		try
		{
		  	 String q = "select dc from Dutycycle dc where dc.satpsid = " + 
					     "(select sps.id from Satellitep sps where sps.psid =  " +
					     "(select se.psid from Session se where se.id = :sessionId) " +
		  			     "and sps.satelliteid = :satId) " +
			             "and dc.activitytype = :actType " +
		                 "and dc.activitysubtype = :actSubType";
		  	 return entityManager.createQuery(q, Dutycycle.class)
					  .setParameter("sessionId", sessionId)
					  .setParameter("satId", satId)
					  .setParameter("actType", actType)
					  .setParameter("actSubType", actSubType).getSingleResult();
		} catch(NoResultException ex) {
			logger.info("PsDaoImpl getDutycycle() ex="+ex.getMessage());
	    	return null;
	    }
	}
	
    public List<Siteps> listSiteps(String psName) {
    	  String q = "select sps from Sitep sps where sps.psid = " + 
    			     "ANY(select ps.id from Parameterset ps where ps.name = :psName)" ;
    	  return entityManager.createQuery(q, Siteps.class).setParameter("psName", psName).getResultList();
    }

  	public Siteps getSitep(String psName, String siteName)
  	{
  		try{
  			String q = "select sps from Sitep sps where sps.psid = " + 
  				     "ANY(select ps.id from Parameterset ps where ps.name = :psName) " +
  	  	    		 "and sps.siteid = " +
  				     "ANY(select s.id from Site s where s.name = :siteName)";
  	    	  return entityManager.createQuery(q, Siteps.class).setParameter("psName", psName).setParameter("siteName", siteName).getSingleResult();	
		}catch(Exception ex){
			logger.info("PsDaoImpl getSitep() ex="+ex.getMessage());
			return null;
		}
  	      
  	}
}
