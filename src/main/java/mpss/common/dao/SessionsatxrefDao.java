package mpss.common.dao;

import java.util.List;
import mpss.common.jpa.Sessionsatxref;


public interface SessionsatxrefDao extends GenericDao<Sessionsatxref> {
	public Sessionsatxref getById(long id);
	public long getNextId();
	public List<Sessionsatxref> getBySessionId(int sessionId);

}
