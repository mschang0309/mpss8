package mpss.common.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.Date;
import mpss.common.jpa.Unavailtimerange;

public interface UnavailableResourcesDao {

	   public List<Unavailtimerange> getSitesUnavailableTimes(Date startTime, Date endTime);
	   public List<Unavailtimerange> getSiteUnavailableTimes(int siteid, Date startTime, Date endTime);
	   public List<Unavailtimerange> getSiteUnavailableTimes(String siteName, Date startTime, Date endTime);
	   public List<Unavailtimerange> getSitesUnavailableTimes(Timestamp startTime, Timestamp endTime, int branchId);
	   public boolean isUnavailableTime(int sessionId,String siteName, Timestamp startTime, Timestamp endTime, int branchId);
	   public boolean isUnavailableTimeBySite(int sessionId,int siteId ,Timestamp startTime, Timestamp endTime, int branchId);
}
