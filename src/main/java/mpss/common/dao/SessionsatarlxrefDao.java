package mpss.common.dao;

import java.util.List;
import mpss.common.jpa.Sessionsatarlxref;

public interface SessionsatarlxrefDao extends GenericDao<Sessionsatarlxref> {
	public Sessionsatarlxref getById(long id);

	public long getNextId();
	public List<Sessionsatarlxref> getBySessionId(int sessionId);

}
