package mpss.common.dao;


import java.util.List;

import mpss.common.jpa.Maneuverrequest;


public interface ManeuverrequestDao extends GenericDao<Maneuverrequest> {
	
	public Maneuverrequest getManeuverrequest(int id);
	public List<Maneuverrequest> getByArlId(int arlId);
	public long getNextId();
}
