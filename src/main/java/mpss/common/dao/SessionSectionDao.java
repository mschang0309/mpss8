package mpss.common.dao;

import java.sql.Timestamp;

import mpss.common.jpa.SessionSection;

public interface SessionSectionDao extends GenericDao<SessionSection> {

	public SessionSection getSectionbyTime(Timestamp time);
}
