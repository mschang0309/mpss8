package mpss.common.dao;

import mpss.common.jpa.Rsirpt;


public interface RsirptDao extends GenericDao<Rsirpt> {
	public Rsirpt getByName(String name);

}
