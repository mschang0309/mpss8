package mpss.common.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import mpss.common.jpa.Site;

@Repository("siteDao")
public class SiteDaoImpl extends GenericDaoImpl<Site> implements SiteDao {

    public SiteDaoImpl() {
        super(Site.class);
    }
    
    public Site getByName(String name) {
    	try{
    		String q = "select s from Site s where s.name = :name";
      	  return entityManager.createQuery(q, Site.class).setParameter("name", name).getSingleResult();
		}catch(Exception ex){
			logger.info("SiteDaoImpl getNextId() ex="+ex.getMessage());
			return null;
		}
    	  
    }

	@Override
	public Site getSiteById(long id) {
		try{
			Site site = entityManager.find(Site.class, id);
			return site;
		}catch(Exception ex){
			logger.info("SiteDaoImpl getSiteById() ex="+ex.getMessage());
			return null;
		}
		
	}

	@Override
	public long getNextId() {
		try{
			return (Long) entityManager.createNativeQuery("select nextval('site_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("SiteDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}

	@Override
	public List<Site> getAllSites() {
		String q = "select s from Site s";
  	  	return entityManager.createQuery(q, Site.class).getResultList();
	}

	@Override
	public List<Site> getByAliasType(String aliasType, String name) {
		String q = "select s from Site s where s."+ aliasType +" = :name order by s.name";
		return entityManager.createQuery(q, Site.class).setParameter("name", name).getResultList();
	}
}
