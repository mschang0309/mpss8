package mpss.common.dao;

import java.util.List;

import mpss.common.jpa.Site;

public interface SiteDao extends GenericDao<Site> {

   public Site getByName(String name);
   public List<Site> getByAliasType(String aliasType, String name);
   public Site getSiteById(long id);
   public long getNextId();
   public List<Site> getAllSites();
}
