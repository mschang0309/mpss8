package mpss.common.dao;

import java.util.List;
import mpss.common.jpa.Xcrossrequest;

public interface XcrossrequestDao extends GenericDao<Xcrossrequest> {
	public List<Xcrossrequest> getByArlId(int arlId);
	public Xcrossrequest getXcrossById(int Id);
	public long getNextId();

}
