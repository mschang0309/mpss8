package mpss.common.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Aipprocrequest;

@Repository("aipprocrequestDao")
public class AipprocrequestDaoImpl extends GenericDaoImpl<Aipprocrequest> implements AipprocrequestDao {

    public AipprocrequestDaoImpl() {
        super(Aipprocrequest.class);
    }
    
    public List<Aipprocrequest> getByArlId(int arlId)
    {
  	  String q = "select aip from Aipprocrequest aip where aip.arlid = :arlid";
  	  return entityManager.createQuery(q, Aipprocrequest.class).setParameter("arlid", arlId).getResultList();
    }
    
    public Aipprocrequest getById(int Id)
    {
  	  String q = "select aip from Aipprocrequest aip where aip.id = :id";
  	  return entityManager.createQuery(q, Aipprocrequest.class).setParameter("id", Id).getSingleResult();
    }
}
