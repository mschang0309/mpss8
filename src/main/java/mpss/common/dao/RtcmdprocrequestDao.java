package mpss.common.dao;

import java.util.List;
import mpss.common.jpa.Rtcmdprocrequest;

public interface RtcmdprocrequestDao extends GenericDao<Rtcmdprocrequest> {
	
	public List<Rtcmdprocrequest> getByArlId(int arlId);

}
