package mpss.common.dao;

import java.util.Date;
import java.util.List;

import mpss.common.jpa.Externalactivityrequestinfo;

public interface ExternalactivityrequestinfoDao extends GenericDao<Externalactivityrequestinfo> {
	
	public long getNextId();
	public Externalactivityrequestinfo getByArlId(int arlId);
	public List<Externalactivityrequestinfo> getBySatNameNBranch(String satName, boolean isbranch, String instrument);
	public long getAfterTimeMTL(Date time, int sessionId, boolean isBranch, String instrument);
	public List<Long> getArlIdByTime(String starttime, String endtime);
	
}
