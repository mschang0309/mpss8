package mpss.common.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Xcrossrequest;

@Repository("xcrossrequestDao")
public class XcrossrequestDaoImpl extends GenericDaoImpl<Xcrossrequest> implements XcrossrequestDao {

    public XcrossrequestDaoImpl() {
        super(Xcrossrequest.class);
    }
    
    public List<Xcrossrequest> getByArlId(int arlId)
    {
  	  String q = "select x from Xcrossrequest x where x.arlid = :arlid";
  	  return entityManager.createQuery(q, Xcrossrequest.class).setParameter("arlid", arlId).getResultList();
    }
    
    public Xcrossrequest getXcrossById(int Id)
    {
  	  String q = "select x from Xcrossrequest x where x.id = :id";
  	  return entityManager.createQuery(q, Xcrossrequest.class).setParameter("id", Id).getSingleResult();
    }
    
    @Override
	public long getNextId() {
		try{
			return (Long)entityManager.createNativeQuery("select nextval('xcrossrequest_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("XcrossrequestDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}
}
