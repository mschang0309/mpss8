package mpss.common.dao;

import java.util.List;
import mpss.common.jpa.Dutycycle;

public interface DutycycleDao extends GenericDao<Dutycycle> {
	public List<Dutycycle> getBySatPsId(int satPsId);
	public Dutycycle getById(int id);
	public Dutycycle getByPsIdNTypeNSubtype(int satPsId,String actType,String actSubType);
}
