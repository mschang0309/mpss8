package mpss.common.dao;

import java.util.List;
import mpss.common.jpa.Imagingmode;

public interface ImagingmodeDao extends GenericDao<Imagingmode> {
	 public Imagingmode getImagingModeByInstNModeid(String instrument, int modeid);
	 public Imagingmode getImagingModeById(long id);
	 public List<Imagingmode> getImagingModeByInst(String instrument);
	 public long getMaxId();
	 public long getNextId();
	 
}

