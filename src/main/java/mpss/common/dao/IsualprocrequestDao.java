package mpss.common.dao;

import java.util.List;

import mpss.common.jpa.Isualprocrequest;

public interface IsualprocrequestDao extends GenericDao<Isualprocrequest> {
	public List<Isualprocrequest> getByArlId(int arlId);
	public long getNextId();

}
