package mpss.common.dao;

import java.util.List;
import mpss.common.jpa.Unavailtimerange;

public interface UnavailableTimeRangeDao extends GenericDao<Unavailtimerange> {

	   public List<Unavailtimerange> getUnavailableTimeRangeByBid(int bid);
	   public List<Unavailtimerange> getAllUnavailableTimeRange();	   
	   public List<Unavailtimerange> getUnavailtimerangeByTime(int branchId, String trStart, String trEnd);
	   public List<Unavailtimerange> getSiteUnavailtimerangeByTime(int branchId,int siteid, String trStart, String trEnd);
	   
}
