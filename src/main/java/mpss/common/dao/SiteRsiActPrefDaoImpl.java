package mpss.common.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import mpss.common.jpa.Sitersiactpref;

@Repository("siteRsiActPrefDao")
public class SiteRsiActPrefDaoImpl extends GenericDaoImpl<Sitersiactpref> implements SiteRsiActPrefDao {

    public SiteRsiActPrefDaoImpl() {
        super(Sitersiactpref.class);
    }

	@Override
	public List<Sitersiactpref> getSiteRsiActPrefsByPsid(int satpsid) {
		String q = "select r from Sitersiactpref r where r.satpsid = :satpsid";
    	return entityManager.createQuery(q, Sitersiactpref.class).setParameter("satpsid", satpsid).getResultList();
		
	}

	
    
}
