package mpss.common.dao;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Aipcommand;

@Repository("aipcommandDao")
public class AipcommandDaoImpl extends GenericDaoImpl<Aipcommand> implements AipcommandDao {

    public AipcommandDaoImpl() {
        super(Aipcommand.class);
    }
    
	public Aipcommand getByCmd(String cmd) {
		try{
			String q = "select a from Aipcommand a where a.cmd = :cmd";
			return entityManager.createQuery(q, Aipcommand.class).setParameter("cmd", cmd).getSingleResult();
		}catch(Exception ex){
			logger.info("AipcommandDaoImpl getByCmd() ex="+ex.getMessage());
			return null;
		}
	}

}
