package mpss.common.dao;

import java.util.List;

import mpss.common.jpa.Rsiimagingrequest;

public interface RsiimagingrequestDao extends GenericDao<Rsiimagingrequest> {
	
	public Rsiimagingrequest getRsiImagingrequest(int id);
	public List<Rsiimagingrequest> getByArlId(int arlId);
	public Rsiimagingrequest getById(int Id);
	public long getNextId();

}
