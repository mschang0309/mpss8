package mpss.common.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mpss.configFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings("unchecked")
public class GenericDaoImpl<T> implements GenericDao<T> {

    private Class<T> type;

    protected EntityManager entityManager;
    
    protected Logger logger = Logger.getLogger(configFactory.getLogName());

    public GenericDaoImpl(Class<T> type) {
        super();
        this.type = type;
    }

    @PersistenceContext(unitName="puMpss4j")
    @Qualifier(value="emfMpss4j")
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public T create(T t) {
    	try
    	{
          entityManager.persist(t);          
          return t;
    	}
    	catch (Exception e)
    	{
    		logger.info("create exception: " + e.getMessage());    		
    		return null;
    	}
    }
    
    public T get(Long id) {
        return (T) entityManager.find(type, id);
    }

    public List<T> getAll() {
        return entityManager.createQuery("select obj from " + type.getName() + " obj").getResultList();
    }

    @Transactional
    public boolean update(T t) {
    	try{
    		entityManager.merge(t);
    		return true;
    	}catch(Exception e){
    		return false;    		
    	}
        
    }

    @Transactional
    public boolean delete(T t)  {
    	try{
    		//entityManager.remove(t);
            entityManager.remove(entityManager.merge(t));
    		return true;
    	}catch(Exception e){
    		return false;    		
    	}
        
    }
    
    @Transactional
    public boolean doSql(String  jpsql) {    
    	try{
    		entityManager.createNativeQuery(jpsql).executeUpdate();       		
    		return true;
    	}catch(Exception e){
    		logger.info("UI DO SQL EX,SQL="+jpsql);  
    		return false;    		
    	}
        
    }
    
    public void reset(){
    	if(entityManager!=null){
    		entityManager.getEntityManagerFactory().getCache().evictAll();  
    	}
    		
    	
    }

}
