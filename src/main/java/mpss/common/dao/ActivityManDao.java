package mpss.common.dao;

import java.sql.Timestamp;

import mpss.common.jpa.Extent;
import mpss.util.timeformat.TimeRange;


public interface ActivityManDao {
	public TimeRange getOperationalPeriod(Extent extent);
	public double calculateSectors(int recorderId,int sessionid ,String type);
	public double getAvailCapacity(Extent extent,String type);
	public double getAvailCapacity(Extent extent,String type,Timestamp tval);
	public double getTotalAvail(Extent extent);
	public int getRSICapacity(Timestamp tval,int recorderId,int sessId);
	public int getISUALCapacity(Timestamp tval,int recorderId,int sessId);
	public int getDDTCapacity(Timestamp tval,int recorderId,int sessId);
}
