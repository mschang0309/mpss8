package mpss.common.dao;

import java.util.List;
import java.util.Set;
import mpss.common.jpa.Recorder;
import mpss.common.jpa.Satellite;

public interface SatelliteDao extends GenericDao<Satellite> {

   public Satellite getByName(String name);
   public Satellite getSatelliteById(long id);
   public Set<Recorder> getRecorders(Long satId);
   public Satellite getByOps4number(String ops4number);
   public Satellite getByOpsnumber(String opsnumber);
   public List<Satellite> getAllSatellites();
   public long getNextId();
}
