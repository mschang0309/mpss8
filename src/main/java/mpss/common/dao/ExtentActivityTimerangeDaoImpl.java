package mpss.common.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Extentactivitytimerange;

@Repository("extentActivityTimerangeDao")
@SuppressWarnings("unchecked")
public class ExtentActivityTimerangeDaoImpl extends	GenericDaoImpl<Extentactivitytimerange> implements ExtentActivityTimerangeDao {

	public ExtentActivityTimerangeDaoImpl() {
		super(Extentactivitytimerange.class);
	}

	@Override
	public long getNextId() {
		try {
			return (Long) entityManager.createNativeQuery(
					"select nextval('extentactivitytimerange_oid_seq')")
					.getSingleResult();
		} catch (Exception ex) {
			logger.info("ExtentActivityTimerangeDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}

	}

	@Override
	public Extentactivitytimerange getExtentById(long id) {
		try {

			String q = "select a.* from Extentactivitytimerange a where a.id='"+ String.valueOf(id) + "' ";
			return (Extentactivitytimerange) entityManager.createNativeQuery(q, Extentactivitytimerange.class)
					.getSingleResult();
		} catch (Exception ex) {
			logger.info("ExtentActivityTimerangeDaoImpl getExtentById() id ="+ String.valueOf(id));
			logger.info("ExtentActivityTimerangeDaoImpl getExtentById() ex="+ex.getMessage());
			return null;
		}
		

	}
	
	@Override
	public List<Extentactivitytimerange> getExtentByparentid(long id) {
		try {
			String q = "select a.* from Extentactivitytimerange a where a.parentid='"+ String.valueOf(id) + "' order by id";
			return  entityManager.createNativeQuery(q, Extentactivitytimerange.class).getResultList();
		} catch (Exception ex) {
			logger.info("ExtentActivityTimerangeDaoImpl getExtentByparentid() id ="+ String.valueOf(id));
			logger.info("ExtentActivityTimerangeDaoImpl getExtentByparentid() ex="+ex.getMessage());
			return null;
		}		

	}
	
	public List<Extentactivitytimerange> getExtentByparentid4Copy(long id) {
		try {
			String q = "select a.* from Extentactivitytimerange a where a.parentid='"+ String.valueOf(id) + "' order by id";
			return  entityManager.createNativeQuery(q, Extentactivitytimerange.class).getResultList();
		} catch (Exception ex) {
			logger.info("ExtentActivityTimerangeDaoImpl getExtentByparentid() id ="+ String.valueOf(id));
			logger.info("ExtentActivityTimerangeDaoImpl getExtentByparentid() ex="+ex.getMessage());
			return null;
		}		

	}

}
