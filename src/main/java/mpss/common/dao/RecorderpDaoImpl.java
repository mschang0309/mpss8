package mpss.common.dao;

import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import mpss.common.jpa.Recorderp;

@Repository("recorderpDao")
public class RecorderpDaoImpl extends GenericDaoImpl<Recorderp> implements RecorderpDao {

    public RecorderpDaoImpl() {
        super(Recorderp.class);
    }

	@Override
	public List<Recorderp> listRecorderps(int recorderid) {
		
		String q = "select r from Recorderp r where r.recorderid = :recorderid";
	   	return entityManager.createQuery(q, Recorderp.class).setParameter("recorderid", recorderid).getResultList();
		
	}

	@Override
	public Recorderp getRecorderp(int satelliteid, String name) {
		logger.info(" RecorderpDaoImpl.class , return null; no imp");
		return null;
		
	}
	
	public Recorderp getBySessionAndSat(int sessionId, int SatId) {	
	    try{
			String q = "select rp from Recorderp rp where rp.recorderid = (select max(r.id) from Recorder r where r.satelliteid = :SatId)" + 
					" and rp.psid=(select s.psid from Session s where s.id= :sessionId)";
				return entityManager.createQuery(q, Recorderp.class).setParameter("SatId", SatId).setParameter("sessionId", sessionId).getSingleResult();
//				String q = "select rp.* from recorderps rp where rp.recorderid = (select max(r.id) from recorder r where r.satelliteid = " + SatId + 
//						") and rp.psid=(select s.psid from sessions s where s.id= " + sessionId + ")";
//					return (Recorderp) entityManager.createNativeQuery(q, Recorderp.class).getSingleResult();
	    } catch(NoResultException ex) {
	    	logger.info("RecorderpDaoImpl getBySessionAndSat ex="+ex.getMessage());
	        return null;
	    }
	}
}
