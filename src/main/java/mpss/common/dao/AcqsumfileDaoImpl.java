package mpss.common.dao;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Acqsumfile;

@Repository("acqsumfileDao")
public class AcqsumfileDaoImpl extends GenericDaoImpl<Acqsumfile> implements AcqsumfileDao {

    public AcqsumfileDaoImpl() {
        super(Acqsumfile.class);
    }    
}
