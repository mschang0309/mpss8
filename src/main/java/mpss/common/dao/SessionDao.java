package mpss.common.dao;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import com.google.common.collect.Range;
import mpss.common.jpa.Session;
import mpss.util.timeformat.TimeRange;

public interface SessionDao extends GenericDao<Session> {

   public List<Session> listSessions();
   public Session getSession(String name);
   public Session getSessionById(long id);
   public Session getSessionbyTime(Timestamp time, int isbranch, int satId);
   public List<String> getSatelliteNames(String name);
   public List<String> getSiteNames(String name);
   public List<String> getSatelliteArlNames(String name, String satName);
   public List<String> getSessionNamesbyRange(Range<Timestamp>range, int isbranch);
   public List<Session> getSessionsbyRange(TimeRange range, int isbranch);
   public Collection<Session> getSessionByRangeNBranch(String starttime, String endtime, int isbranch);
   public Collection<Session> getSessionByTimeNBranch(String time, int isbranch);
   public Date getMinStarttime();
   public Date getMaxEndtime();
   public long getMaxId();
   public long getNextId();
   public List<String> getScheduledCount(int sessionId, int isbranch, int satId);
   public Session getSessionContainTime(Timestamp time, int isBranch);
   
   public List<Session> getSessionsbyRangeForRTS(TimeRange range, int isbranch);
   
}
