package mpss.common.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import mpss.common.jpa.Isualprocrequest;

@Repository("isualprocrequestDao")
public class IsualprocrequestDaoImpl extends GenericDaoImpl<Isualprocrequest> implements IsualprocrequestDao {

    public IsualprocrequestDaoImpl() {
        super(Isualprocrequest.class);
    }
    
    public List<Isualprocrequest> getByArlId(int arlId)
    {
  	  String q = "select isu from Isualprocrequest isu where isu.arlid = :arlid";
  	  return entityManager.createQuery(q, Isualprocrequest.class).setParameter("arlid", arlId).getResultList();
    }
    
    @Override
	public long getNextId() {
		try{
			return (Long)entityManager.createNativeQuery("select nextval('isualprocrequests_oid_seq')").getSingleResult();
		}catch(Exception ex){
			logger.info("IsualprocrequestDaoImpl getNextId() ex="+ex.getMessage());
			return 0;
		}
		
	}
}
