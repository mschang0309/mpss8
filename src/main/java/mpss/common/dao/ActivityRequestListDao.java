package mpss.common.dao;

import java.util.List;

//import mpss.common.jpa.Arl;
//import mpss.common.jpa.Isualimagingrequest;
//import mpss.common.jpa.Isualprocrequest;
//import mpss.common.jpa.Maneuverrequest;
//import mpss.common.jpa.Misccmdprocrequest;
//import mpss.common.jpa.PsMissionTimeLine;
//import mpss.common.jpa.Rsiimagingrequest;
//import mpss.common.jpa.Rtcmdprocrequest;
//import mpss.common.jpa.Uplinkrequest;
//import mpss.common.jpa.Xcrossrequest;
import mpss.common.jpa.*;

public interface ActivityRequestListDao {
	  // Arl
	  public List<Arl> listArls();
	  public Arl getArl(String arlName);
    // Isualimagingrequest
    public List<Isualimagingrequest> listIsualimagingrequests(String arlName);
    //public List<Isualimagingrequest> getIsualimagingrequests(String arlName, Date from, Date to);
    // Isualprocrequest
    public List<Isualprocrequest> listIsualprocrequests(String arlName);
    //public List<Isualprocrequest> getIsualprocrequests(String arlName, Date from, Date to);
    // Maneuverrequest
    public List<Maneuverrequest> listManeuverrequests(String arlName);
    //public List<Isualprocrequest> getManeuverrequests(String arlName, Date from, Date to);
    // Misccmdprocrequest
    public List<Misccmdprocrequest> listMisccmdprocrequests(String arlName);
    //public List<Isualprocrequest> getMisccmdprocrequests(String arlName, Date from, Date to);
    // Rsiimagingrequest
    public List<Rsiimagingrequest> listRsiimagingrequests(String arlName);
    //public List<Rsiimagingrequest> getRsiimagingrequests(String arlName, Date from, Date to);
    // Rtcmdprocrequest
    public List<Rtcmdprocrequest> listRtcmdprocrequests(String arlName);
    //public List<Rtcmdprocrequest> getRtcmdprocrequests(String arlName, Date from, Date to);
    // Uplinkrequest
    public List<Uplinkrequest> listUplinkrequests(String arlName);
    //public List<Uplinkrequest> getUplinkrequests(String arlName, Date from, Date to);
    // Xcrossrequest
    public List<Xcrossrequest> listXcrossrequests(String arlName);
    //public List<Xcrossrequest> getXcrossrequests(String arlName, Date from, Date to);
    public List<String> listExternalFileNames();
    public PsMissionTimeLine getMissionTimeLines(String arlName);
}
