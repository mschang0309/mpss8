package mpss.common.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import mpss.configFactory;
import mpss.common.jpa.*;

@Repository("orbitEventsDao")
public class OrbitEventsDaoImpl implements OrbitEventsDao {

    private EntityManager entityManager;
    private Logger logger = Logger.getLogger(configFactory.getLogName());
    
    public OrbitEventsDaoImpl() {
    }
    
    // for Spring injection
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    //= Revolutions
    public Rev getRevByPass(int scId, int revNo) {
    	try{
    		 String q = "select r from Rev r where r.scid = :scid and r.revno = :revno";
       	  return entityManager.createQuery(q, Rev.class).setParameter("scid", scId).setParameter("revno", revNo).getSingleResult();
		}catch(Exception ex){
			logger.info("OrbitEventsDaoImpl getRevByPass() ex="+ex.getMessage());
			return null;
		}
    	 
    }

    public List<Rev> getRevsByTimeRange(int scId, Date startTime, Date endTime) {
    	  String q = "select r from Rev r where r.scid = :scid and r.antime between :startTime and :endTime order by r.antime";
    	  return entityManager.createQuery(q, Rev.class).setParameter("scid", scId)
                                                      .setParameter("startTime", startTime)
                                                      .setParameter("endTime", endTime)
    			                                            .getResultList();
    }
    
    //= Eclipses
    public List<Eclipse> getEclipsesByPass(int scId, int revNo) {
        String q = "select e from Eclipse e where e.satelliteid = :scid and " +
    	            "e.revid = ANY(select r.id from Rev r where r.scid= :scid and r.revno = :revno) " +
    	            "order by e.penumbraentrancetime";
  	    return entityManager.createQuery(q, Eclipse.class).setParameter("scid", scId)
  	                                                    .setParameter("revno", revNo)
  	                                                    .getResultList();
    }

    public List<Eclipse> getEclipsesByRev(Rev rev) {
        String q = "select e from Eclipse e where e.satelliteid = :scid and " +
    	            "e.revid = ANY(select r.id from Rev r where r.id = :revid) " +
    	            "order by e.penumbraentrancetime";
  	    return entityManager.createQuery(q, Eclipse.class).setParameter("scid", rev.getScid())
  	                                                    .setParameter("revid", rev.getId())
  	                                                    .getResultList();
    }

    public List<Eclipse> getEclipsesByTimeRange(int scId, Date startTime, Date endTime) {
        String q = "select e from Eclipse e where e.satelliteid = :scid and " +
    	            "e.revid = ANY(select r.id from Rev r where r.antime between :startTime and :endTime) " +
    	            "order by e.penumbraentrancetime";
  	    return entityManager.createQuery(q, Eclipse.class).setParameter("scid", scId)
  	                                                    .setParameter("startTime", startTime)
  	                                                    .setParameter("endTime", endTime)
  	                                                    .getResultList();
    }
    
    //= Contacts
    public List<Contact> getContactsByPass(int scId, int revNo) {
        String q = "select c from Contact c where c.satelliteid = :scid and " +
    	            "c.revid = ANY(select r.id from Rev r where r.scid= :scid and r.revno = :revno) " +
    	            "order by c.rise";
  	    return entityManager.createQuery(q, Contact.class).setParameter("scid", scId)
  	                                                    .setParameter("revno", revNo)
  	                                                    .getResultList();
    }
    
    public List<Contact> getContactsByRev(Rev rev) {
        String q = "select c from Contact c where c.satelliteid = :scid and " +
    	            "c.revid = ANY(select r.id from Rev r where r.id = :revid) " +
    	            "order by c.rise";
  	    return entityManager.createQuery(q, Contact.class).setParameter("scid", rev.getScid())
  	                                                    .setParameter("revid", rev.getId())
  	                                                    .getResultList();
    }

    public List<Contact> getContactsByTimeRange(int scId, Date startTime, Date endTime) {
        String q = "select c from Contact c where c.satelliteid = :scid and " +
    	            "c.revid = ANY(select r.id from Rev r where r.scid = :scid and r.antime between :startTime and :endTime) " +
    	            "order by c.rise";
  	    return entityManager.createQuery(q, Contact.class).setParameter("scid", scId)
  	                                                    .setParameter("startTime", startTime)
  	                                                    .setParameter("endTime", endTime)
  	                                                    .getResultList();
    }
    
    public List<Contact> getContactsByTimeRangeSiteType(int scId, Date startTime, Date endTime, String siteType) {
    	List<Contact> res = null;
        //String q = "select c from Contact c where c.satelliteid = :scid and " +
    	//            "c.revid = ANY(select r.id from Rev r where r.scid = :scid and r.antime between :startTime and :endTime) and " +
    	//            "c.siteid = ANY(select s.id from Site s where s.sitetype = :siteType) " +
    	//            "order by c.rise";
        String q = "select c from Contact c where c.satelliteid = :scid and " +
	            "c.revid IN (select r.id from Rev r where r.scid = :scid and r.antime between :startTime and :endTime) and " +
	            "c.siteid IN (select s.id from Site s where s.sitetype = :siteType) " +
	            "order by c.rise";
  	    res = entityManager.createQuery(q, Contact.class).setParameter("scid", scId)
  	                                                    .setParameter("startTime", startTime)
  	                                                    .setParameter("endTime", endTime)
  	                                                    .setParameter("siteType", siteType)
  	                                                    .getResultList();
  	    if (res == null) {
  	    	logger.info("getContactsByTimeRangeSiteType: null results");  	    	
  	    }
  	    else if (res.size() == 0) {
  	    	logger.info("getContactsByTimeRangeSiteType: empty results");  	    	    	
  	    }
  	    return res;
    }
    //= Orbit events (rev + eclipses + contacts)
    public PsOrbitEvents getOrbitEventsByPass(int scId, int revNo) {
    	  PsOrbitEvents oe = null;
    	  
    	  // get Rev
        Rev rev = getRevByPass(scId, revNo);   	  
     	  if (rev == null) {
    	  	return oe;
    	  }
    	  
    	  oe = new PsOrbitEvents(rev);
    	  
    	  // get eclipses
    	  List<Eclipse> eclipses = getEclipsesByRev(rev);
    	  oe.setEclipses(eclipses);
    	  
    	  // get contacts
    	  List<Contact> contacts = getContactsByRev(rev);
    	  oe.setContacts(contacts);
    	  
    	  return oe;
    }

    public List<PsOrbitEvents> getOrbitEventsByTimeRange(int scId, Date startTime, Date endTime) {
    	  
    	  List<PsOrbitEvents> oes = new ArrayList<PsOrbitEvents>();
    	  
    	  // get Revs
        List<Rev> revs = getRevsByTimeRange(scId, startTime, endTime);  
         
        for (Rev rev : revs)
        {	  
        	PsOrbitEvents oe = new PsOrbitEvents(rev);
    	      
    	      // get eclipses
    	      List<Eclipse> eclipses = getEclipsesByRev(rev);
    	      oe.setEclipses(eclipses);
    	      
    	      // get contacts
    	      List<Contact> contacts = getContactsByRev(rev);
    	      oe.setContacts(contacts);
    	      
    	      oes.add(oe);
    	  }
    	  
    	  return oes;
    }
    
    public List<Timestamp> getAdjacentPenumbraExits(long scId,Timestamp targetDt)
    {
    	
    	List<Timestamp> res = null;
    			String q = "select max(e.penumbraexittime) from Eclipse e " +
	            "where e.satelliteid = :scid and e.eclipsetype = 'Earth/Sun' and e.penumbraexittime <= :targetDt " +
	            "union select min(e.penumbraexittime) from Eclipse e " +
	            "where e.satelliteid = :scid and e.eclipsetype = 'Earth/Sun' and e.penumbraexittime >= :targetDt ";
	            //") as U";
	            
    	/*
    	String q = "select penumbraexit_time from " +
	            "(select max(penumbraexittime) penumbraexit_time from eclipse e " +
	            "where e.satelliteid = 1 and eclipsetype = 'Earth/Sun' and penumbraexittime <= '2009-06-02 02:55:00' " +
	            "union select min(penumbraexittime) penumbraexit_time from eclipse e " +
	            "where e.satelliteid = 1 and eclipsetype = 'Earth/Sun' and penumbraexittime >= '2009-06-02 02:55:00' " +
	            ") as U";
    	*/
  	    res = entityManager.createQuery(q, Timestamp.class).setParameter("scid", scId)
  	                                                    .setParameter("targetDt", targetDt)
  	                                                    .getResultList();
  	    if (res == null) {
  	    	logger.info("getAdjacentPenumbraExits: null results"); 
  	    }
  	    else if (res.size() == 0) {
  	    	logger.info("getAdjacentPenumbraExits: empty results"); 
  	    }
  	    return res;
    }
    
    public List<Timestamp> getAdjacentPenumbraentrance(long scId,Timestamp targetDt)
    {
    	
    	List<Timestamp> res = null;
    			String q = "select max(e.penumbraentrancetime) from Eclipse e " +
	            "where e.satelliteid = :scid and e.eclipsetype = 'Earth/Sun' and e.penumbraentrancetime <= :targetDt " +
	            "union select min(e.penumbraentrancetime) from Eclipse e " +
	            "where e.satelliteid = :scid and e.eclipsetype = 'Earth/Sun' and e.penumbraentrancetime >= :targetDt ";     
	            
    	
  	    res = entityManager.createQuery(q, Timestamp.class).setParameter("scid", scId)
  	                                                    .setParameter("targetDt", targetDt)
  	                                                    .getResultList();
  	    if (res == null) {
  	    	logger.info("getAdjacentPenumbraentrance: null results"); 
  	    }
  	    else if (res.size() == 0) {
  	    	logger.info("getAdjacentPenumbraentrance: empty results"); 
  	    }
  	    return res;
    }
}
