package mpss.common.dao;

import java.util.List;
import java.util.Date;
import java.sql.Timestamp;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Rev;
import mpss.common.jpa.Eclipse;
import mpss.common.jpa.PsOrbitEvents;

public interface OrbitEventsDao {

	   public Rev getRevByPass(int scId, int revNo);
	   public List<Rev> getRevsByTimeRange(int scId, Date startTime, Date endTimeo);
	   public List<Eclipse> getEclipsesByPass(int scId, int revNo);
	   public List<Eclipse> getEclipsesByRev(Rev rev);
	   public List<Eclipse> getEclipsesByTimeRange(int scId, Date startTime, Date endTime);
	   public List<Contact> getContactsByPass(int scId, int revNo);
	   public List<Contact> getContactsByRev(Rev rev);
	   public List<Contact> getContactsByTimeRange(int scId, Date startTime, Date endTime);
	   public List<Contact> getContactsByTimeRangeSiteType(int scId, Date startTime, Date endTime, String siteType);
	   public PsOrbitEvents getOrbitEventsByPass(int scId, int revNo);
	   public List<PsOrbitEvents> getOrbitEventsByTimeRange(int scId, Date startTime, Date endTime);
	   public List<Timestamp> getAdjacentPenumbraExits(long scId,Timestamp targetDt);
	   public List<Timestamp> getAdjacentPenumbraentrance(long scId,Timestamp targetDt);
}
