package mpss.common.dao;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import mpss.common.jpa.Contact;
import mpss.util.timeformat.TimeRange;
public interface ContactDao extends GenericDao<Contact> {
	
	public List<Contact> getByRangeNSat(TimeRange range,int satId,int branchId);
	public List<Contact> getConsbyRangeNSatNSite(TimeRange range,int satId,int siteId,int branchId);
	public long getNextId();
	public Contact getContactById(int id);
	public Contact getContactByParams(long sat,long site,long ar,long branch);
	public Collection<Contact> getByBranchId(int branchid);
	public Collection<Contact> getByStatId(int stat);
	public Collection<Contact> getBySiteId(int site);
	public Collection<Contact> getContactByParams(int sat,int siteid,int  br);
	public List<Contact> getByBranchIdNTimeRange(int branchId, String trStart, String trEnd);
	public List<Contact> getBySiteTypeNSatNBranchNRange(String sitetype, int satId, int branchId, Timestamp trStart,Timestamp trEnd,int sessId);
	public List<Contact> getBySessionNBranch(int branchId, Timestamp trStart,Timestamp trEnd,int sessId);
	public List<Contact> getBySessionNAIP(int branchId, Timestamp trStart,Timestamp trEnd);
	public List<Contact> getBySessionExtent(int sessionId);
	public List<Contact> getContactsByRevid(int revid);
	public Contact getContactByMaxeltime( Date maxeltime);
	public Contact getBySatNBranchNotInSite(int siteId,int satId, int branchId, TimeRange tr);
	public List<Contact> getConsbyRangeSBandSite(TimeRange range,int satId,int branchId);
	
	public Contact getConbySiteRev(long satId,int branchId,int siteId,String revid);

}
