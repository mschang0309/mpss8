package mpss.resources;


import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Range;
import mpss.common.jpa.*;
import mpss.common.dao.*;

@Service
public class AvailableContacts 
{
	protected Satellite satellite;
	protected Session session;
	
	  @Autowired
    UnavailableResourcesDao unavailableResourcesDao;
	
	  @Autowired
    OrbitEventsDao oeDao;
	
	 public Satellite setSatellite()
	 {
			 return this.satellite;
	 }
	 public void setSatellite(Satellite satellite)
	 {
		 this.satellite = satellite;
	 }
	 
	 public Session setSession()
	 {
			 return this.session;
	 }
	 public void setSession(Session session)
	 {
		 this.session = session;
	 }
	 
	// need default constructor for Spring Ioc
	public AvailableContacts()
	{		
	}
  
	public AvailableContacts(Satellite satellite, Session session)
	{
		this.satellite = satellite;
		this.session = session;
	}
	
	public List<Contact> getAvailableContactsBySiteType(String siteType)
	{
		if (oeDao == null || unavailableResourcesDao == null) {
			System.out.println("Dao not injected");
			return null;
		}
		
		List<Contact> res = new ArrayList<Contact>();
		// get S-band contacts within session period
		int scid = satellite.getId().intValue();
		Date starttime = new Date(session.getStarttime().getTime());
		Date endtime = new Date(session.getEndtime().getTime());
		List<Contact> contacts = oeDao.getContactsByTimeRangeSiteType(scid, starttime, endtime, siteType);
			
		// remove unavailable contacts
		if (siteType == "SBAND")
		{
		    for (Contact c : contacts)
		    {
			    if (isSbandContactAvailable(c))
			    {
				    res.add(c);
			    }
		    }
		}
		else if (siteType == "XBAND")
		{
		    for (Contact c : contacts)
		    {
			    if (isXbandContactAvailable(c))
			    {
				    res.add(c);
			    }
		    }
		}
		return res;
	}
	
	public boolean isSbandContactAvailable(Contact contact)
	{
		// get unavailable time ranges within session period
		List<Unavailtimerange>  unavailables = unavailableResourcesDao.getSiteUnavailableTimes(contact.getSiteid(), new Date(session.getStarttime().getTime()), new Date(session.getEndtime().getTime()));
		// if conatct period overlaps with any of the unavailable period of the contacted site, this contact is deemed unavailable
		for (Unavailtimerange utr : unavailables)
		{
			if (Range.closed(contact.getCmdrise(), contact.getCmdfade()).isConnected(Range.closed(utr.getStarttime(), utr.getEndtime())))
			{
				return false;
			}
		}
		return true;
	}

	public boolean isXbandContactAvailable(Contact contact)
	{
		// get unavailable time ranges within session period
		List<Unavailtimerange>  unavailables = unavailableResourcesDao.getSiteUnavailableTimes(contact.getSiteid(), new Date(session.getStarttime().getTime()), new Date(session.getEndtime().getTime()));
		// if conatct period overlaps with any of the unavailable period of the contacted site, this contact is deemed unavailable
		for (Unavailtimerange utr : unavailables)
		{
			if (Range.closed(contact.getXbandrise(), contact.getXbandfade()).isConnected(Range.closed(utr.getStarttime(), utr.getEndtime())))
			{
				return false;
			}
		}
		return true;
	}

}