package mpss.resources;

import mpss.common.jpa.*;

public class RecorderResources 
{
	protected Recorderp recorderPS;
	
	public RecorderResources(Recorderp recorderPS)
	{
		this.recorderPS = recorderPS;
	}
	
	// get number of sectors reserved for the duration of the imaging activity and imaging mode
	// number of sectors used is dependent on the record duration and the data rate for MS and PAN imaging
  public int calculateRecorderDelta(int recDuration, double rate)
  {
  	// assumed rate is sectors/sec round down to nearest whole number
  	double d = Math.floor(recDuration*rate);
  	
  	// add file sector margin
  	int delta = (int)d + recorderPS.getFilesectormargin();
  	
  	return delta;
  }
  
  public int calculatePlaybackDuration(int sectors)
  {
  	int dur = (int)Math.ceil(sectors / recorderPS.getOutputdatarate());
  	return dur;
  }

  public int calculatePlaybackDuration(int sectors, double bandwidthRatio)
  {
  	int dur = (int)Math.ceil(sectors / (recorderPS.getOutputdatarate() * bandwidthRatio));
  	return dur;
  }


  public int calculateISUALDuration(Recorder rec)
  {
  	int capacity = (int) (recorderPS.getIsualfilesize() * recorderPS.getTimetoreadoneisualsector());
  	// capacity is in total bits. for RS2 sector size = 8 MBytes = 64  MBits = 64000 kbits = 64000000 bits
  	if (rec.getSectorsize() != 0) {
  		return capacity/rec.getSectorsize();
  	}
  	return 0;
  }
}