package mpss.rts;


import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mpss.configFactory;
import mpss.common.dao.ContactDao;
import mpss.common.dao.SatelliteDao;
import mpss.common.dao.SessionDao;
import mpss.common.dao.SiteDao;
import mpss.common.dao.UnavailableTimeRangeDao;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Session;
import mpss.common.jpa.Site;
import mpss.common.jpa.Unavailtimerange;
import mpss.util.timeformat.DateUtil;
import mpss.util.timeformat.TimeRange;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public abstract class RTSCreatorBase implements RTSCreator {
	
    @Autowired
    SiteDao siteDao;
    @Autowired
    ContactDao contDao;
    @Autowired
    SatelliteDao satDao;
    @Autowired
    UnavailableTimeRangeDao UnTRDao;
    @Autowired
    SessionDao sessionDao;

    
    public String returnMsg;
    protected String satName;
	protected Date startTime;
	protected Date stopTime;
	protected String fileName;
	protected Map<Integer,String> contactList;
	protected Map<Integer,String> nocheckList;
	protected String logFileDir;
	protected String rtsFileDir;
	protected boolean isBranch;
	protected int branchId;
	protected FileOutputStream logFOS;
	protected OutputStreamWriter logOSW;
	
	protected Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public String getLogFileName()
	{
		return fileName+ ".html";
	}
	
	public String getReturnMsg()
	{
		return this.returnMsg;
	}
	
	public void setBranch (boolean isBranch, int branchId){
		this.isBranch = isBranch;
		this.branchId = this.isBranch ? branchId : 0;
	}
	
	public void setStartTime (Date startTime)
	{
		this.startTime = startTime;
	}
	
	public void setStopTime (Date stopTime)
	{
		this.stopTime = stopTime;
	}
	
	public void setContactList (Map<Integer,String> contactList)
	{
		 
		this.contactList = contactList;
	}
	
	public void setNoCheckList (Map<Integer,String> nocheckList)
	{
		this.nocheckList = nocheckList;
	}

	public boolean GenReport () throws Exception
	{
		satName = System.getProperty("satName");
		
		logger.info("Start "+satName+" RTS Creator");		
		
		if (contactList == null)
		{
			returnMsg = "No contact selected.";
			return false;
		}
		
		// if session is schedule can't modify , by jeff 20170725			
		TimeRange tr = new TimeRange(DateUtil.dateToTimestamp(startTime),DateUtil.dateToTimestamp(stopTime));
		List<Session> sessions = sessionDao.getSessionsbyRangeForRTS(tr,0);
		for(Session session:sessions){
			if(session.getColdscheduled()==1){
				returnMsg = "Session is scheduled in this timerange !";
				return false;
			}
		}				
		
		logFileDir = System.getProperty("reportFileDir") + System.getProperty("rtsReport");
		rtsFileDir = System.getProperty("reportFileDir") + System.getProperty("rtsRequest");		
		
	
		if (!GenXML())
			return false;

		try 
	    {
		     // write log
	        logOSW.write("<a href=\"http://" + System.getProperty("mqIP") + ":8080/" + System.getProperty("web.path","mpssweb") + "/RtsSchRepGen/rtsRpt.xml\""
				+ " target=\"_blank\">" + rtsFileDir + fileName + ".xml</a>\n");
	        
	    	logOSW.write("</body>\n</html>\n");
	        logOSW.close();	 

	    } catch (Exception e) {
	    	logger.error("RTSCreator Error: Log File create Error");	        
	    }
	
		// clear Unavailtime between start time and end time, by jeff 20170725	
        ClearUnavailtimerange();
		
		SetUnavailtimerange();
		
		
		return true;
	}
	
	public boolean ClearUnavailtimerange() throws Exception
	{		
		Map<String,String> rts_sites = getRtsSite();		
		List<Unavailtimerange> unavailtimeranges = UnTRDao.getUnavailtimerangeByTime(0, DateUtil.dateToString(startTime,"yyyy-MM-dd HH:mm:ss"), DateUtil.dateToString(stopTime,"yyyy-MM-dd HH:mm:ss"));
		for(Unavailtimerange un:unavailtimeranges){
			for(String siteid :rts_sites.keySet()){
				if(un.getParentid().toString().equalsIgnoreCase(siteid)){
					UnTRDao.delete(un);
				}
			}			
		}		
		return true;
	}
	
	public boolean SetUnavailtimerange() throws Exception
	{
		Map<String,String> rts_sites = getRtsSite();
		
		List<Unavailtimerange> unTRList = new ArrayList<Unavailtimerange>();
		for (int i :nocheckList.keySet())
        {
			Contact cont = contDao.get((long)i);
			Site site = siteDao.get((long) cont.getSiteid());
			
			for (String j :rts_sites.keySet()){				
				Unavailtimerange UnTR = new Unavailtimerange();				
				UnTR.setUttype("SITE");
				UnTR.setParentid(Integer.valueOf(j).intValue());
				if (site.getSitetype().equalsIgnoreCase("XBAND"))
				{
					//UnTR.setStarttime(cont.getXbandrise());
					//UnTR.setEndtime(cont.getXbandfade());
					UnTR.setStarttime(cont.getRise());
					UnTR.setEndtime(cont.getFade());
				}
				else
				{
					UnTR.setStarttime(cont.getRise());
					UnTR.setEndtime(cont.getFade());
				}
				
				UnTR.setNumdata(0);
				UnTR.setBranchid(branchId);
				unTRList.add(UnTR);
			}			
        }
		
		//contactList
		for (Integer key : contactList.keySet()) {            
            Contact cont = contDao.get((long)key);
			Site site = siteDao.get((long) cont.getSiteid());
			
            if(contactList.get(key).equalsIgnoreCase("X")){
            	// add s band unavailtimerange
            	for (String j :rts_sites.keySet()){	
            		if(rts_sites.get(j).equalsIgnoreCase("S")){
            			Unavailtimerange UnTR = new Unavailtimerange();				
        				UnTR.setUttype("SITE");
        				UnTR.setParentid(Integer.valueOf(j).intValue());
        				if (site.getSitetype().equalsIgnoreCase("XBAND"))
        				{
        					//UnTR.setStarttime(cont.getXbandrise());
        					//UnTR.setEndtime(cont.getXbandfade());
        					UnTR.setStarttime(cont.getRise());
        					UnTR.setEndtime(cont.getFade());
        				}
        				else
        				{
        					UnTR.setStarttime(cont.getRise());
        					UnTR.setEndtime(cont.getFade());
        				}
        				
        				UnTR.setNumdata(0);
        				UnTR.setBranchid(branchId);
        				unTRList.add(UnTR);
            		}
            	}
            }else if(contactList.get(key).equalsIgnoreCase("S")){
            	// add x band unavailtimerange
            	for (String j :rts_sites.keySet()){	
            		if(rts_sites.get(j).equalsIgnoreCase("X")){
            			Unavailtimerange UnTR = new Unavailtimerange();				
        				UnTR.setUttype("SITE");
        				UnTR.setParentid(Integer.valueOf(j).intValue());
        				if (site.getSitetype().equalsIgnoreCase("XBAND"))
        				{
        					//UnTR.setStarttime(cont.getXbandrise());
        					//UnTR.setEndtime(cont.getXbandfade());
        					UnTR.setStarttime(cont.getRise());
        					UnTR.setEndtime(cont.getFade());
        				}
        				else
        				{
        					UnTR.setStarttime(cont.getRise());
        					UnTR.setEndtime(cont.getFade());
        				}
        				
        				UnTR.setNumdata(0);
        				UnTR.setBranchid(branchId);
        				unTRList.add(UnTR);
            		}
            	}
            }
            
        }
		
		
		// generate unavailtimerange
		for(Unavailtimerange tr : unTRList)
		{
			createUnavailtimerange(tr);
		}
			
		return true;
	}
	
	private Unavailtimerange createUnavailtimerange(Unavailtimerange untr)
	{	
		Unavailtimerange UnTR = new Unavailtimerange();
		try
		{
			UnTR = UnTRDao.create(untr);			
			logger.info("Unavailtimerange created: " + UnTR.getId());
		}
		catch (Exception e)
		{
			logger.info("Create Unavailtimerange failed :"+e.getMessage());
			UnTR = null;
		}
		
		return UnTR;
	}
	
	public void openLogFile ()
	{
		/// open the log file for output
		String logFilePath = logFileDir + fileName + ".html";
		
		File file = new File(logFilePath);
		if(!file.exists())
		{
		    try {
		        file.createNewFile();
		    } catch (Exception e) {
		        logger.error("RTSCreator Error: Log file can not be create: " + logFilePath);
		    }
		}
	    try 
	    {
	    	logFOS = new FileOutputStream(file);
	    	logOSW = new OutputStreamWriter(logFOS);
	    	
	    } catch (Exception e) {
	        logger.error("RTSCreatorBase Error: Log file can not be opened: " + logFilePath);
	    }
	}
	
}
