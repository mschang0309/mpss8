package mpss.rts;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import mpss.config;
import mpss.configFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class RTSConsole 
{
	public static void main(String args[])
	{
		Map<Integer,String> contactList = new HashMap<Integer,String>();
		Map<Integer,String> nocheckList = new HashMap<Integer,String>();
		contactList.put(1919, "X");
		contactList.put(1923, "S");
		contactList.put(1930, "XS");
		
		
		nocheckList.put(1935, "X");
		nocheckList.put(1937, "X");
		nocheckList.put(1940, "X");	
		
				
		//System.setProperty("satName", "FORMOSAT8");
		System.setProperty("satName", "FORMOSAT5");
		
		config.load();   
		try 
		{
			// Spring application context
			ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf",configFactory.getSatelliteName()}));
			org.apache.log4j.xml.DOMConfigurator.configure(javae.AppDomain.getPath("log4j.xml", new String[]{"conf",configFactory.getSatelliteName()}));
			
			RTSCreator rtsCreator = (RTSCreator)context.getBean(configFactory.getRTSCreator());
			if (rtsCreator == null) {
				System.err.println("Load RTSCreator bean failed");
				return;
			}
			
			rtsCreator.setStartTime(new Date (Timestamp.valueOf("2018-11-1 00:00:00").getTime())); 
        	rtsCreator.setStopTime(new Date (Timestamp.valueOf("2018-11-8 00:00:00").getTime()));
        	rtsCreator.setBranch(false, 0);
        	rtsCreator.setContactList (contactList);
        	rtsCreator.setNoCheckList (nocheckList);
        	
			if (rtsCreator.GenReport())
			{
				System.out.println("Report generation Success.");
			}
			else
			{
				String msg = (rtsCreator.getReturnMsg() == null) ? "Report generation failed" : rtsCreator.getReturnMsg();
				System.out.println(msg);
				return;
			}
    	
      } catch (Exception ex) {
    		ex.printStackTrace();
      }
      
	    
	}
}
