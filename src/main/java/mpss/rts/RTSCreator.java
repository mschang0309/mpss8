package mpss.rts;


import java.util.Date;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public interface RTSCreator {
	
	public String getLogFileName();	
	public void setBranch (boolean isBranch, int branchId);	
	public void setStartTime (Date startTime);	
	public void setStopTime (Date stopTime);	
	public void setContactList (Map<Integer,String> contactList);	
	public void setNoCheckList (Map<Integer,String> nocheckList);
	public boolean GenReport () throws Exception;	
	public String getReturnMsg();
	public boolean ClearUnavailtimerange() throws Exception;
	public boolean SetUnavailtimerange() throws Exception;
	public Map<String,String> getRtsSite();
	public void openLogFile ();
	public boolean GenXML() throws Exception;
}
