package mpss.rts;


import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import mpss.common.jpa.Contact;
import mpss.common.jpa.Site;
import mpss.util.timeformat.TimeToString;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.springframework.stereotype.Component;

@Component("FS8RtsCreator")
public class FS8RTSCreator extends RTSCreatorBase { 	

	
	@Override
	public Map<String, String> getRtsSite() {
		List<Site> sites =siteDao.getAllSites();
		Map<String,String> rts_sites = new HashMap<String,String>();
		for(Site site:sites){
			if(site.getName4().equalsIgnoreCase("SVAX") || site.getName4().equalsIgnoreCase("SVAL")){
				rts_sites.put(site.getId().toString(), site.getName1());
			}
		}		
		return rts_sites;
	}

	@Override
	public boolean GenXML() throws Exception
	{
		logger.info("FS8RTSCreator GenXML()");
		
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat df = new SimpleDateFormat("DDD");    
    	String julianDAY  = df.format(currentTime); 
    	SimpleDateFormat year_df = new SimpleDateFormat("yyyy");    
    	String year  = year_df.format(currentTime);
		
    	// request_reference
		File folder = new File(rtsFileDir);
		File[] listOfFiles = folder.listFiles();
		
		int num = 1;
		for (int i = 0; i < listOfFiles.length; i++) 
		{
			if (listOfFiles[i].isFile()) 
			{
				String name = listOfFiles[i].getName();
				if (name.endsWith(".xml") || name.endsWith(".XML"))
				{
					if (name.split("_").length > 6)
					{
						String reqRef = name.split("_")[5];
						String year_ref = name.split("_")[6];
						if (reqRef.startsWith("MPSS" + julianDAY + "N") && year_ref.startsWith(year))
						{
							int oldnum = Integer.valueOf(reqRef.substring(8, 9));
							if (oldnum >= num)
								num = oldnum + 1;
						}
					}
					
				}
			}
		}
		
		String request_reference = "MPSS" + julianDAY + "N" + String.valueOf(num);
		
		//get file name
		fileName = "ksat_schedule_request_KSAT_" + satName + "_"
	        + request_reference+ "_" + TimeToString.AsRTSNameTime(currentTime);
		
		// open the log file for output
		openLogFile ();
	    
		
		//create xml document
		Document doc = DocumentHelper.createDocument();

        Element req = doc.addElement("ksat_schedule_request");
        req.addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        req.addAttribute("xsi:noNamespaceSchemaLocation", "http://cweb.ksat.no/cweb/schema/schedule/schema_schedule_request.xsd");

        //header
        Element req_header= req.addElement("header");
        Element req_header_originator = req_header.addElement("originator");
        req_header_originator.addText("NSPO");
        Element req_header_recipient = req_header.addElement("recipient");
        req_header_recipient.addText("KSAT");
        Element req_header_schfrom = req_header.addElement("schedule_valid_from");
        req_header_schfrom.addText(TimeToString.AsRTSXMLTime(startTime));
        Element req_header_schto = req_header.addElement("schedule_valid_to");
        req_header_schto.addText(TimeToString.AsRTSXMLTime(stopTime));
        Element req_header_ref = req_header.addElement("request_reference");
        req_header_ref.addText(request_reference);
        Element req_header_gentime = req_header.addElement("generation_time");
        req_header_gentime.addText(TimeToString.AsRTSXMLTime(currentTime));
        //log header
		logOSW.write("<html>\n<head>\n</head>\n<body>\n<pre>\n");
		logOSW.write("Header :\n");
        logOSW.write("<table border=\"1\">");
		logOSW.write("<tr><td>originator</td><td>NSPO</td></tr>");
		logOSW.write("<tr><td>recipient</td><td>KSAT</td></tr>");
		logOSW.write("<tr><td>schedule_valid_from</td><td>" + TimeToString.AsRTSXMLTime(startTime) + "</td></tr>");
		logOSW.write("<tr><td>schedule_valid_to</td><td>" + TimeToString.AsRTSXMLTime(stopTime) + "</td></tr>");
		logOSW.write("<tr><td>request_reference</td><td>" + request_reference + "</td></tr>");
		logOSW.write("<tr><td>generation_time</td><td>" + TimeToString.AsRTSXMLTime(currentTime) + "</td></tr>");
        logOSW.write("</table>\n\n");
		
        //body
        Element req_body = req.addElement("body");
        //log body header
        logOSW.write("Body :\n");
        logOSW.write("<table border=\"1\">");
		logOSW.write("<tr><td>action</td><td>start_time</td><td>end_time</td><td>satellite_name</td>" +
			"<td>requested_antenna</td><td>bands</td><td>uplink</td></tr>");

		//sort by contact id by jeff 20170815
		TreeSet<Integer> ts = new TreeSet<Integer>(new Comparator<Object>(){
			@Override
			public int compare(Object o1, Object o2) {
				Integer a=(Integer) o1;
				Integer b=(Integer) o2;
				return a.intValue()-b.intValue();
			}
        	
        });
        ts.addAll(contactList.keySet());
        for (int i :ts)        
        {
        	Contact cont = contDao.get((long)i);
        	if(cont==null){
        		logger.info("FS5RTSCreator contact is null, contact id :"+i);	
        	}        	
        	
    		Timestamp sTime,eTime;
    		String band,uplink;    		
    		sTime = cont.getRise();
    		eTime = cont.getFade(); 		
    		
    		band = contactList.get(i);
    		if (band.endsWith("S"))
    			uplink = "YES";
    		else
    			uplink = "NO";
    		
            Element req_body_req = req_body.addElement("schedule_request");
            Element req_body_req_act = req_body_req.addElement("action");
            req_body_req_act.addText("ADD");
            Element req_body_req_stime = req_body_req.addElement("start_time");
            req_body_req_stime.addText(TimeToString.AsRTSXMLTime(sTime));
            Element req_body_req_etime = req_body_req.addElement("end_time");
            req_body_req_etime.addText(TimeToString.AsRTSXMLTime(eTime));
            Element req_body_req_satname = req_body_req.addElement("satellite_name");
            
            req_body_req_satname.addText(satName);
            
            Element req_body_req_ant = req_body_req.addElement("requested_antenna");
            req_body_req_ant.addText(System.getProperty("reqAntenna"));
            Element req_body_req_band = req_body_req.addElement("bands");
            req_body_req_band.addText(band);
            Element req_body_req_uplink = req_body_req.addElement("uplink");
            req_body_req_uplink.addText(uplink);

    		logOSW.write("<tr><td>ADD</td><td>" + TimeToString.AsRTSXMLTime(sTime) + "</td><td>" + 
    			TimeToString.AsRTSXMLTime(eTime) + "</td><td>" + satName + "</td>" +
    			"<td>" + System.getProperty("reqAntenna") +"</td><td>" + band + "</td><td>" + uplink + "</td></tr>");
    	}

        logOSW.write("</table>\n\n");

        // save XML file
        String xmlfile = rtsFileDir + fileName + ".xml";
        FileWriter fw = new FileWriter(xmlfile);        
        OutputFormat of = new OutputFormat(); 
        of.setIndentSize(4); // set Tab is 4 spac
        of.setNewlines(true);
        XMLWriter xw = new XMLWriter(fw, of);
        xw.write(doc);
        xw.close();        
        
        String tomcatXmlfile = System.getProperty("tomcatXml");
        FileWriter tomcatfw = new FileWriter(tomcatXmlfile);
        XMLWriter tomcatxw = new XMLWriter(tomcatfw, of);
        tomcatxw.write(doc);
        tomcatxw.close();        
		return true;
	}
	
}
