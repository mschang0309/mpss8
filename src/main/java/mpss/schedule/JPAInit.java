package mpss.schedule;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import mpss.schedule.imp.FactoryBuilder;

public class JPAInit {
	
	private static FactoryBuilder facBuilder;
	
	static {
		ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf"}));
		JPAInit.facBuilder = (FactoryBuilder) context
				.getBean(FactoryBuilder.class);		
	}
	
	public static FactoryBuilder getBuilder(){
		return JPAInit.facBuilder;
	}

}
