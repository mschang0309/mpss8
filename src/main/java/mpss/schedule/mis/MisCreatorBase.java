package mpss.schedule.mis;

import mpss.configFactory;
import mpss.common.dao.ActivityDao;
import mpss.common.dao.ArlDao;
import mpss.common.dao.EclipseDao;
import mpss.common.dao.ExtentDao;
import mpss.common.dao.ManeuverrequestDao;
import mpss.common.dao.SessionManDao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MisCreatorBase implements MisCreator {

	@Autowired
    SessionManDao sessManDao;
	@Autowired
    EclipseDao eclipseDao;
	@Autowired
    ArlDao arlDao;
	@Autowired
	ManeuverrequestDao manrequestDao;
	@Autowired
    ActivityDao actDao;
    @Autowired
    ExtentDao extentDao;
	
    protected Logger logger = Logger.getLogger(configFactory.getLogName());
	
	@Override
	public int create(int sessionId,int satId) {		
		return 0;
	}

}
