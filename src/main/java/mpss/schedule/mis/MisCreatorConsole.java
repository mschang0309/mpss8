package mpss.schedule.mis;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import mpss.config;
import mpss.configFactory;


public class MisCreatorConsole {

	public static void main(String[] args) {

		System.setProperty("satName", "FORMOSAT5");
		config.load(); 
		try {
			ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml",new String[] { "conf" }));
			org.apache.log4j.xml.DOMConfigurator.configure(javae.AppDomain.getPath("log4j.xml", new String[] { "conf",configFactory.getSatelliteName() }));			
			MisCreator misCreator = (MisCreator) context.getBean("FS5MisCreator");
			if (misCreator == null) {
				System.err.println("Load MisCreator bean failed");
				return;
			}
			
			misCreator.create(50109,1);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
