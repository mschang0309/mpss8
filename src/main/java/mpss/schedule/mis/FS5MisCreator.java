package mpss.schedule.mis;

import java.sql.Timestamp;
import java.util.List;

import mpss.common.jpa.Activity;
import mpss.common.jpa.Arl;
import mpss.common.jpa.Eclipse;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Maneuverrequest;
import mpss.common.jpa.Session;
import mpss.util.timeformat.DateUtil;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeUtil;

import org.springframework.stereotype.Component;


@Component("FS5MisCreator")
public class FS5MisCreator extends MisCreatorBase {
	
	@Override
	public int create(int sessionId,int satId) {	
		Session session = sessManDao.getSessionById(sessionId);
		TimeRange session_tr = new TimeRange(session.getStarttime(),session.getEndtime());		
		Timestamp starttime =TimeUtil.timeAppendSec(DateUtil.timestampAddSec(session.getStarttime(), "yyyy-MM-dd HH:mm:ss", 0),-6000);		
		Timestamp endtime =TimeUtil.timeAppendSec(DateUtil.timestampAddSec(session.getEndtime(), "yyyy-MM-dd HH:mm:ss", 0),6000);
		TimeRange tr = new TimeRange(starttime,endtime);
		
		int count = 0;
		 
		List<Eclipse> eclipses = eclipseDao.getByColumnNSatNRangeType("penumbraentrancetime", "Earth/Sun", satId, tr, 0);			
		for(Eclipse eclipse:eclipses){
			Timestamp time = TimeUtil.timeAppendSec(DateUtil.timestampAddSec(eclipse.getPenumbraentrancetime(), "yyyy-MM-dd HH:mm:ss", 0),Integer.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.exit","-1")).intValue());	
			if(session_tr.contains(time)){
				// add mic act
				float q1 = Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.entrance.Q1","0"));
				float q2 = Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.entrance.Q2","0"));
				float q3 = Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.entrance.Q3","0"));
				float q4 = Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.entrance.Q4","0"));
				float rrate=Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.entrance.RRate","0"));
				float prate=Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.entrance.PRate","0"));
				float yrate=Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.entrance.YRate","0"));
				int asyncflag=Integer.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.entrance.Asyncflag","0"));
				addManActivity(satId,session,0,time,Integer.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.entrance.duration","12")).intValue(),q1,q2,q3,q4,rrate,prate,yrate,asyncflag);
				count=count+1;
			}
		}
		
		eclipses = eclipseDao.getByColumnNSatNRangeType("penumbraexittime", "Earth/Sun", satId, tr, 0);			
		for(Eclipse eclipse:eclipses){
			Timestamp time = TimeUtil.timeAppendSec(eclipse.getPenumbraexittime(),Integer.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.exit","-1")).intValue());	
			if(session_tr.contains(time)){
				// add mic act
				float q1 = Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.exit.Q1","0"));
				float q2 = Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.exit.Q2","0"));
				float q3 = Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.exit.Q3","0"));
				float q4 = Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.exit.Q4","0"));
				float rrate=Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.exit.RRate","0"));
				float prate=Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.exit.PRate","0"));
				float yrate=Float.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.exit.YRate","0"));
				int asyncflag=Integer.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.exit.Asyncflag","0"));
				addManActivity(satId,session,1,time,Integer.valueOf(System.getProperty("aocs.maneuver.mis.eclipse.exit.duration","150")).intValue(),q1,q2,q3,q4,rrate,prate,yrate,asyncflag);
				count=count+1;
			}
		}
		
		
		
		return count;
	}
	
	@SuppressWarnings("deprecation")
	private void addManActivity(int satId,Session session,int type,Timestamp time,int duration,float q1,float q2,float q3,float q4,float rrate,float prate,float yrate,int asyncflag){		
		
		// 1. add Arl , one session one arl
		String arl_name = "shadowMANEUVER"+session.getName();
		Arl arl ;
		if(arlDao.getByName(arl_name)==null){
			arl = new Arl();
			arl.setArltype("MANEUVER");
			arl.setName(arl_name);
			arl.setShadow(1);
			arl = arlDao.create(arl);
		}else{
			arl = arlDao.getByName(arl_name);
		}
		
		// 2. add Maneuverrequest 		
        Maneuverrequest mancVo = new Maneuverrequest();        
        mancVo.setArlid(Long.valueOf(arl.getId()).intValue());
        mancVo.setDay(time.getDay());
        mancVo.setYear(time.getYear());
        mancVo.setStarttime(DateUtil.parseHmsToSeconds(time));
        mancVo.setDuration(duration);
        mancVo.setQ1(q1);
        mancVo.setQ2(q2);
        mancVo.setQ3(q3);
        mancVo.setQ4(q4);
        mancVo.setStation("NA");
        mancVo.setElevation("NA");
        mancVo.setAzimuth("NA");
        mancVo.setOrbit(0);
        mancVo.setManeuverproc("NA");
        mancVo.setOrbitmanplan("NA");
        mancVo.setType("ATTITUDE");        
        mancVo.setComments("");
        mancVo.setManeuverduration(duration);
        mancVo.setRRate(rrate);
        mancVo.setPRate(prate);
        mancVo.setYRate(yrate);
        mancVo.setAsyncflag(asyncflag);
                
        mancVo = manrequestDao.create(mancVo);
        
       // 3. add act
        Activity act_vo = new Activity();        
        act_vo.setDuration(1);
        act_vo.setStarttime(time);
        act_vo.setEndtime(TimeUtil.timeAppendSec(time,duration));
        act_vo.setSatelliteid(satId);
        act_vo.setSessionid(Long.valueOf(session.getId()).intValue());
        act_vo.setExtentid(0);
        act_vo.setScheduled(0);
        act_vo.setArlentrytype("MANEUVER");
        act_vo.setArlentryid(Long.valueOf(mancVo.getId()).intValue());
        act_vo.setName("Attitude_Maneuver");
        act_vo.setActsubtype("NO");
        act_vo.setToytype("NO");
        act_vo.setPrevactid(0);
        act_vo.setNextactid(0);
        act_vo.setUsermodified(0);
        act_vo.setValid(1);
        act_vo.setInfo("");        
        act_vo = actDao.create(act_vo);
        
       // 4. add extent
        Extent extent_vo = new Extent();   
        extent_vo.setActivityid(Long.valueOf(act_vo.getId()).intValue());
        extent_vo.setContactid(0);
        extent_vo.setBranchid(0);
        extent_vo.setTransmitterid(0);
        extent_vo.setRecorderid(0);
        extent_vo.setAntennaid(0);
        extent_vo.setStarttime(time);
        extent_vo.setEndtime(TimeUtil.timeAppendSec(time,duration));
        extent_vo.setRecorderdelta(0);
        extent_vo.setSecondrecorderdelta(0);
        extent_vo.setFilename(0);
        extent_vo.setSecondfilename(0);
        extent_vo.setUsermodified(0);
        extent_vo.setUserlocked(0);
        extent_vo.setSoftconstraintviolated(0);
        extent_vo.setHardconstraintviolated(0);
        extent_vo.setIsaip(0);
        extent_vo = extentDao.create(extent_vo);
        
        
       // 5. update act  
        act_vo.setScheduled(1);
        act_vo.setExtentid(Long.valueOf(extent_vo.getId()).intValue());
        actDao.update(act_vo);
	}

}
