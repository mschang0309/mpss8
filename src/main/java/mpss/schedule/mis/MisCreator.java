package mpss.schedule.mis;

import org.springframework.stereotype.Service;

@Service
public interface MisCreator {
	
	public int create(int sessionId,int satId);
	
}
