package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.MisccmdprocrequestDao;
import mpss.common.jpa.Misccmdprocrequest;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseARLEntryImp;
import mpss.schedule.base.FwBaseARLEntryImpVector;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwMiscCmdProcARLEntryImp;
import mpss.schedule.base.FwMiscCmdProcARLEntryImpFactory;
import mpss.schedule.base.GlARL.Trigger;
import mpss.schedule.base.GlDaySpec;


public class MiscCmdProcImpFactory extends FwMiscCmdProcARLEntryImpFactory implements UIDB<Misccmdprocrequest>{
	
    private static MisccmdprocrequestDao misccmdprocDao=JPAInit.getBuilder().getMisccmdprocrequest();
    private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	
	public boolean create(Misccmdprocrequest object){
		if(misccmdprocDao.create(object)==null){ 
			return false;
		}else{
			return true; 
		} 
	 }
	
	public Long createGetId(Misccmdprocrequest object){
		Misccmdprocrequest data  =misccmdprocDao.create(object);		
		return data.getId();		
	 }

	 public boolean update(Misccmdprocrequest object){
		 return misccmdprocDao.update(object);
	 }

	 public boolean delete(Misccmdprocrequest object){
		 return misccmdprocDao.delete(object);	
	 }

	 public boolean doSql(String jpsql){
		 return misccmdprocDao.doSql(jpsql);	
	 }
	
	 public void releaseFwImp(){
		 misccmdprocDao.reset();
	 }

	@Override
	public synchronized void delete() {
		logger.info(" MiscCmdProcImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwMiscCmdProcARLEntryImp GetOne(int parentID, GlDaySpec Date,long time, Trigger Trigger, boolean offsetNeg, long offset,long duration, String UserProc, int ContactID, int RevID,String Comments) {
		logger.info(" MiscCmdProcImpFactory.class , return super.GetOne(parentID, Date, time, Trigger, offsetNeg, offset, duration,UserProc, ContactID, RevID, Comments); no imp");
		return super.GetOne(parentID, Date, time, Trigger, offsetNeg, offset, duration,UserProc, ContactID, RevID, Comments);
		
	}

	@Override
	protected FwMiscCmdProcARLEntryImp CreateOne(int parent, GlDaySpec Date,long time, Trigger Trigger, boolean offsetNeg, long offset,long duration, String UserProc, int contactID, int RevID,String Comments) {
		logger.info(" MiscCmdProcImpFactory.class , return super.CreateOne(parent, Date, time, Trigger, offsetNeg, offset,duration, UserProc, contactID, RevID, Comments); no imp");
		return super.CreateOne(parent, Date, time, Trigger, offsetNeg, offset,duration, UserProc, contactID, RevID, Comments);
		
	}

	@Override
	public FwBaseARLEntryImp GetID(int arg0) {
		logger.info(" MiscCmdProcImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);
		
	}

	@Override
	public void GetAll(FwBaseARLEntryImpVector arg0, int parentID) {
		logger.info(" MiscCmdProcImpFactory.class , super.GetAll(arg0, parentID); no imp");
		super.GetAll(arg0, parentID);
		
	}

	@Override
	public void Delete(FwBaseARLEntryImp arg0) {
		logger.info(" MiscCmdProcImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwBaseARLEntryImpVector arg0) {
		logger.info(" MiscCmdProcImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void CancelAll() {
		logger.info(" MiscCmdProcImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public boolean Store(FwBaseARLEntryImpVector imps,FwBaseARLEntryImpVector delimps) {
		logger.info(" MiscCmdProcImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwBaseARLEntryImp imp) {
		logger.info(" MiscCmdProcImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" MiscCmdProcImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" MiscCmdProcImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" MiscCmdProcImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}
	 
	
	public HashMap<Long,Long> copyArl(int arlid,int new_arlid) {
		HashMap<Long,Long> misccmdprocmaps = new HashMap<Long,Long>();
		List<Misccmdprocrequest> lists = misccmdprocDao.getByArlId(arlid);
		for(Misccmdprocrequest info:lists){
			Long id  = info.getId();
			info.setId(null);			
			info.setArlid(new_arlid);			
			misccmdprocmaps.put(id, createGetId(info));	
		}
		return misccmdprocmaps;
	}
	 
	 

}
