package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.IsualprocrequestDao;
import mpss.common.jpa.Isualprocrequest;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseARLEntryImp;
import mpss.schedule.base.FwBaseARLEntryImpVector;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwISUALProcARLEntryImp;
import mpss.schedule.base.FwISUALProcARLEntryImpFactory;
import mpss.schedule.base.GlDaySpec;


public class IsualProcImpFactory extends FwISUALProcARLEntryImpFactory implements UIDB<Isualprocrequest>{
	
    private static IsualprocrequestDao isualprocDao=JPAInit.getBuilder().getIsualrequest();	
    private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public boolean create(Isualprocrequest object){
		if(isualprocDao.create(object)==null){ 
			return false;
		}else{
			return true; 
		} 
	 }
	
	public Long createGetId(Isualprocrequest object){
		Isualprocrequest data  =isualprocDao.create(object);		
		return data.getId();		
	 }

	 public boolean update(Isualprocrequest object){
		 return isualprocDao.update(object);
	 }

	 public boolean delete(Isualprocrequest object){
		 return isualprocDao.delete(object);	
	 }

	 public boolean doSql(String jpsql){
		 return isualprocDao.doSql(jpsql);	
	 }
	
	 public void releaseFwImp(){
		 isualprocDao.reset();
	 }

	@Override
	public synchronized void delete() {
		logger.info(" IsualProcImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwISUALProcARLEntryImp GetOne(int parentID, GlDaySpec Date,long startTime, long Duration, String ISUALProc, String Comments) {
		logger.info(" IsualProcImpFactory.class , return super.GetOne(parentID, Date, startTime, Duration, ISUALProc, Comments); no imp");
		return super.GetOne(parentID, Date, startTime, Duration, ISUALProc, Comments);
		
	}

	@Override
	protected FwISUALProcARLEntryImp CreateOne(int parent, GlDaySpec Date,
			long startTime, long Duration, String ISUALProc, String Comments) {
		logger.info(" IsualProcImpFactory.class , return super.CreateOne(parent, Date, startTime, Duration, ISUALProc, Comments); no imp");
		return super.CreateOne(parent, Date, startTime, Duration, ISUALProc, Comments);
		
	}

	@Override
	public FwBaseARLEntryImp GetID(int arg0) {
		logger.info(" IsualProcImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);
		
	}

	@Override
	public void GetAll(FwBaseARLEntryImpVector arg0, int parentID) {
		logger.info(" IsualProcImpFactory.class , super.GetAll(arg0, parentID); no imp");
		super.GetAll(arg0, parentID);
		
	}

	@Override
	public void Delete(FwBaseARLEntryImp arg0) {
		logger.info(" IsualProcImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwBaseARLEntryImpVector arg0) {
		logger.info(" IsualProcImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void CancelAll() {
		logger.info(" IsualProcImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public boolean Store(FwBaseARLEntryImpVector imps,FwBaseARLEntryImpVector delimps) {
		logger.info(" IsualProcImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwBaseARLEntryImp imp) {
		logger.info(" IsualProcImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" IsualProcImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" IsualProcImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" IsualProcImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);		
	}

	public HashMap<Long,Long> copyArl(int arlid,int new_arlid) {
		HashMap<Long,Long> isualprocmaps = new HashMap<Long,Long>();
		List<Isualprocrequest> lists = isualprocDao.getByArlId(arlid);
		for(Isualprocrequest info:lists){
			Long id  = info.getId();
			info.setId(null);			
			info.setArlid(new_arlid);			
			isualprocmaps.put(id, createGetId(info));	
		}
		return isualprocmaps;
	}
	 

}
