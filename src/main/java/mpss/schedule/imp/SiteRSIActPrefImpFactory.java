package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.SiteRsiActPrefDao;
import mpss.common.jpa.Sitersiactpref;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwFwSiteRSIActPrefImpVector;
import mpss.schedule.base.FwSiteRSIActPrefImp;
import mpss.schedule.base.FwSiteRSIActPrefImpFactory;
import mpss.schedule.base.GlRSI;
import mpss.schedule.base.GlRSI.Type;

public class SiteRSIActPrefImpFactory extends FwSiteRSIActPrefImpFactory implements UIDB<Sitersiactpref>{
	
	private Map<Long, FwSiteRSIActPrefImp> instances = new HashMap<Long, FwSiteRSIActPrefImp>();
	private static SiteRsiActPrefDao sitersiactprefDao=JPAInit.getBuilder().getSiteRsiActPrefDao();	
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	
	public boolean create(Sitersiactpref object){
		if(sitersiactprefDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Sitersiactpref object){
		 return sitersiactprefDao.update(object);
	 }

	 public boolean delete(Sitersiactpref object){
		 return sitersiactprefDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return sitersiactprefDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		this.instances.clear();
		sitersiactprefDao.reset();
	}

	@Override
	public synchronized void delete() {
		logger.info(" SiteRSIActPrefImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwSiteRSIActPrefImp GetOne(int parentID, int siteID, Type type) {
		logger.info(" SiteRSIActPrefImpFactory.class , return super.GetOne(parentID, siteID, type); no imp");
		return super.GetOne(parentID, siteID, type);
		
	}

	@Override
	protected FwSiteRSIActPrefImp CreateOne(int parentID, int siteID, Type type) {
		logger.info(" SiteRSIActPrefImpFactory.class , return super.CreateOne(parentID, siteID, type); no imp");
		return super.CreateOne(parentID, siteID, type);		
	}

	@Override
	public FwSiteRSIActPrefImp GetID(int arg0) {
		logger.info(" SiteRSIActPrefImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);		
	}

	@Override
	public void GetAll(FwFwSiteRSIActPrefImpVector arg0, int parentID) {
		logger.info(" SiteRSIActPrefImpFactory.class , super.GetAll(arg0, parentID); parentID="+parentID);
		List<Sitersiactpref> lists =sitersiactprefDao.getSiteRsiActPrefsByPsid(parentID);
		for (Sitersiactpref sitersiactpref : lists){
			FwSiteRSIActPrefImp imp = new FwSiteRSIActPrefImp();
			long lid = sitersiactpref.getId();
			int id = (int) lid;
			imp.SetID(id);
			imp.SetSatPS(parentID);
			imp.SetSite(sitersiactpref.getSiteid());
			imp.SetRSIActType(getRsiActType(sitersiactpref.getRsiacttype()));
			
			this.instances.put(lid, imp);			
			arg0.add(imp);
		}
	}
	
	
	public GlRSI.Type getRsiActType(String type){
		if(type.equals("FIRST_RSI")){
			return GlRSI.Type.FIRST_RSI;
		}else if(type.equals("REC")){
			return GlRSI.Type.REC;
		}else if(type.equals("REC_XMIT")){
			return GlRSI.Type.REC_XMIT;
		}else if(type.equals("PBK")){
			return GlRSI.Type.PBK;
		}else if(type.equals("DDT")){
			return GlRSI.Type.DDT;
		}else if(type.equals("LAST_RSI")){
			return GlRSI.Type.LAST_RSI;
		}else{
			return GlRSI.Type.LAST_RSI;
		}
		
	}

	@Override
	public boolean Store(FwFwSiteRSIActPrefImpVector imps,FwFwSiteRSIActPrefImpVector delimps) {
		logger.info(" SiteRSIActPrefImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwSiteRSIActPrefImp imp) {
		logger.info(" SiteRSIActPrefImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public void Delete(FwSiteRSIActPrefImp arg0) {
		logger.info(" SiteRSIActPrefImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwFwSiteRSIActPrefImpVector arg0) {
		logger.info(" SiteRSIActPrefImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" SiteRSIActPrefImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" SiteRSIActPrefImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" SiteRSIActPrefImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}

	@Override
	public void CancelAll() {
		logger.info(" SiteRSIActPrefImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	

	

}
