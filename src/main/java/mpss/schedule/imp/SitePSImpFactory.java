package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.SitepsDao;
import mpss.common.jpa.Siteps;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwSitePSImp;
import mpss.schedule.base.FwSitePSImpFactory;
import mpss.schedule.base.FwSitePSImpVector;

public class SitePSImpFactory extends FwSitePSImpFactory implements UIDB<Siteps>{

	private Map<Long, FwSitePSImp> instances = new HashMap<Long, FwSitePSImp>();

	private static SitepsDao sitepsDao=JPAInit.getBuilder().getSitepsDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	
	public boolean create(Siteps object){
		if(sitepsDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Siteps object){
		return sitepsDao.update(object);
	 }

	 public boolean delete(Siteps object){
		 return sitepsDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return sitepsDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		this.instances.clear();
		sitepsDao.reset();
	}

	@Override
	public FwSitePSImp GetOne(int psid, int satid, FwSitePSImp fromImp) {
		logger.info("call SitePSImpFactory.GetOne(.,.,.)");
		return null;
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" SitePSImpFactory.class , super.GetAll(arg0); no imp");
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" SitePSImpFactory.class , super.GetAll(arg0, arg1); no imp");
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" SitePSImpFactory.class , return super.Delete(arg0); no imp");
		return true;

	}

	@Override
	public void CancelAll() {
		logger.info(" SitePSImpFactory.class , super.CancelAll(); no imp");
	}

	@Override
	public FwSitePSImp GetOne(int parentID, int psID, long ta, long ps,long pc, boolean uplink, boolean datacollect) {
		logger.info("call SitePSImpFactory.GetOne(...)");
		return null;
	}

	@Override
	protected FwSitePSImp CreateOne(int parentID, int psid, long ta, long ps,long pc, boolean uplink, boolean collect) {
		logger.info("call SitePSImpFactory.CreateOne(...)");
		long newid = sitepsDao.getNextId();
		FwSitePSImp imp = new FwSitePSImp((int) newid, parentID, psid, ta, ps,pc, uplink, collect, true);		
		this.instances.put(newid, imp);		
		return imp;
	}

	@Override
	protected FwSitePSImp CreateOne(int psid, int siteid, FwSitePSImp fromImp) {
		logger.info("call SitePSImpFactory.CreateOne(.,.,.)");
		long newid = sitepsDao.getNextId();
		FwSitePSImp imp = new FwSitePSImp((int) newid, siteid, psid,
				fromImp.GetTurnaroundTime(), fromImp.GetPrepassSetupTime(),
				fromImp.GetPostpassCleanupTime(), fromImp.GetUplinkAvailable(),
				fromImp.GetDataCollect(), true);

		
		this.instances.put(newid, imp);		
		return imp;
	}

	@Override
	public FwSitePSImp GetID(int arg0) {
		logger.info("call SitePSImpFactory.GetID(.)");

		if (instances.containsKey(Long.valueOf(arg0))) {
			return (FwSitePSImp) instances.get(Long.valueOf(arg0));
		} else {			
			Siteps s = sitepsDao.getById(arg0);			
			if(s==null){
				logger.info("SitePSImpFactory GetID(" + arg0 + ") is null");
				return null;
			}else{
				FwSitePSImp imp = new FwSitePSImp();
				long lid = s.getId();
				int id = (int) lid;

				imp.SetID(id);
				imp.SetSite(s.getSiteid());
				imp.SetPS(s.getPsid());
				imp.SetTurnaroundTime(s.getTurnaroundtime());
				imp.SetPrepassSetupTime(s.getPrepasssetuptime());
				imp.SetPostpassCleanupTime(s.getPostpasscleanuptime());
				imp.SetUplinkAvailable(s.getUplinkavailable() == 1 ? true : false);
				imp.SetDataCollect(s.getDatacollect() == 1 ? true : false);				
				this.instances.put(lid, imp);	
				return imp;
			}	
		}			
		
	}

	@Override
	public void GetAll(FwSitePSImpVector arg0, int parentID) {
		//System.out.println("call SitePSImpFactory.GetAll(.,.) id="+parentID); //OK
		List<Siteps> dList = sitepsDao.getBySiteId(parentID);

		for (Siteps s : dList) {
			long lid = s.getId();
			int id = (int) lid;

//			if (instances.containsKey(Long.valueOf(lid))) {				
//				arg0.add((FwSitePSImp) instances.get(Long.valueOf(lid)));
//			} else { }
				FwSitePSImp imp = new FwSitePSImp();
				imp.SetID(id);
				imp.SetSite(s.getSiteid());
				imp.SetPS(s.getPsid());
				imp.SetTurnaroundTime(s.getTurnaroundtime());
				imp.SetPrepassSetupTime(s.getPrepasssetuptime());
				imp.SetPostpassCleanupTime(s.getPostpasscleanuptime());
				imp.SetUplinkAvailable(s.getUplinkavailable() == 1 ? true : false);
				imp.SetDataCollect(s.getDatacollect() == 1 ? true : false);

				instances.put(lid, imp);
				arg0.add(imp);
			}
				
				

	}

	@Override
	public synchronized void delete() {
		logger.info(" SitePSImpFactory.class , super.delete(); no imp");

	}

	@Override
	public boolean Store(FwSitePSImpVector imps, FwSitePSImpVector delimps) {
		logger.info(" SitePSImpFactory.class , return super.Store(imps, delimps); no imp");
		return true;

	}

	@Override
	public boolean Store(FwSitePSImp imp) {
		logger.info(" SitePSImpFactory.class , return super.Store(imp); no imp");
		return true;

	}

	@Override
	public void Delete(FwSitePSImp arg0) {
		logger.info(" SitePSImpFactory.class , super.Delete(arg0); no imp");

	}

	@Override
	public void Delete(FwSitePSImpVector arg0) {
		logger.info(" SitePSImpFactory.class , super.Delete(arg0); no imp");
	}

}
