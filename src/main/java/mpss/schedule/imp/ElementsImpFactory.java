package mpss.schedule.imp;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.jpa.Element;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwElementsImp;
import mpss.schedule.base.FwElementsImpFactory;
import mpss.schedule.base.FwElementsImp_Vector;


public class ElementsImpFactory extends FwElementsImpFactory implements UIDB<Element>{
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public boolean create(Element object){
		return true;
	 }

	 public boolean update(Element object){
		 return true;
	 }

	 public boolean delete(Element object){
		 return true;
	 }
	
	 
	 public boolean doSql(String jpsql){
		 return true;
	 }
	 
	public void releaseFwImp(){
		
	}
	
	

	@Override
	public synchronized void delete() {
		logger.info(" ElementsImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwElementsImp GetOne(int arg0, int arg1) {
		logger.info(" ElementsImpFactory.class , return super.GetOne(arg0, arg1); no imp");
		return super.GetOne(arg0, arg1);
		
	}

	@Override
	public FwElementsImp GetOne(int arg0, int arg1, double arg2, double sma,
			double ecc, double peri, double decay, double inc, double ra,
			double mm, double deriv1, double deriv2, double ma, double period,
			int yr, int branchid) {
		logger.info(" ElementsImpFactory.class , return super.GetOne(...); no imp");
		return super.GetOne(arg0, arg1, arg2, sma, ecc, peri, decay, inc, ra, mm,
				deriv1, deriv2, ma, period, yr, branchid);
		
	}

	@Override
	public void GetAll(FwElementsImp_Vector arg0, int branchID) {
		logger.info(" ElementsImpFactory.class , super.GetAll(arg0, branchID); no imp");
		//super.GetAll(arg0, branchID);
		
	}

	@Override
	public void GetAllSats(FwElementsImp_Vector arg0, int arg1) {
		logger.info(" ElementsImpFactory.class , super.GetAllSats(arg0, arg1); no imp");
		super.GetAllSats(arg0, arg1);
		
	}

	@Override
	public boolean CheckSatelliteDependency(int satelliteID) {
		logger.info(" ElementsImpFactory.class , return super.CheckSatelliteDependency(satelliteID); no imp");
		return super.CheckSatelliteDependency(satelliteID);
		
	}

	@Override
	public void MoveToNominal(FwElementsImp arg0) {
		logger.info(" ElementsImpFactory.class , super.MoveToNominal(arg0); no imp");
		super.MoveToNominal(arg0);
		
	}

	@Override
	protected void AddNew(FwElementsImp arg0) {
		logger.info(" ElementsImpFactory.class , super.AddNew(arg0); no imp");
		super.AddNew(arg0);
		
	}

	@Override
	protected void AddDeleted(FwElementsImp arg0) {
		logger.info(" ElementsImpFactory.class , super.AddDeleted(arg0); no imp");
		super.AddDeleted(arg0);
		
	}

	@Override
	protected void DoCancel(FwElementsImp arg0) {
		logger.info(" ElementsImpFactory.class , super.DoCancel(arg0); no imp");
		super.DoCancel(arg0);
		
	}

	@Override
	protected void DoCancelAll() {
		logger.info(" ElementsImpFactory.class , super.DoCancelAll(); no imp");
		super.DoCancelAll();
		
	}

	@Override
	protected FwElementsImp CreateOne(int arg0, int arg1, double arg2,
			double sma, double ecc, double peri, double decay, double inc,
			double ra, double mm, double deriv1, double deriv2, double ma,
			double period, int yr, int branch) {
		logger.info(" ElementsImpFactory.class , return super.CreateOne(...); no imp");
		return super.CreateOne(arg0, arg1, arg2, sma, ecc, peri, decay, inc, ra, mm,
				deriv1, deriv2, ma, period, yr, branch);
		
	}

	@Override
	protected void DoCleanup() {
		logger.info(" ElementsImpFactory.class , super.DoCleanup(); no imp");
		super.DoCleanup();
		
	}

	@Override
	protected boolean CheckSatelliteInDatabase(int satelliteID) {
		logger.info(" ElementsImpFactory.class , return super.CheckSatelliteInDatabase(satelliteID); no imp");
		return super.CheckSatelliteInDatabase(satelliteID);
		
	}

	@Override
	protected void LoadAll(FwElementsImp_Vector arg0, int arg1) {
		logger.info(" ElementsImpFactory.class , super.LoadAll(arg0, arg1); no imp");
		//super.LoadAll(arg0, arg1);
		
	}

	@Override
	protected void LoadAllSats(FwElementsImp_Vector arg0, int satid) {
		logger.info(" ElementsImpFactory.class , super.LoadAllSats(arg0, satid); no imp");
		super.LoadAllSats(arg0, satid);
		
	}

	@Override
	public FwElementsImp GetID(int arg0) {
		logger.info(" ElementsImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);
		
	}

	@Override
	public void GetAll(FwElementsImp_Vector arg0) {
		logger.info(" ElementsImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void Delete(FwElementsImp_Vector arg0) {
		logger.info(" ElementsImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwElementsImp arg0) {
		logger.info(" ElementsImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public boolean Store(FwElementsImp_Vector vec) {
		logger.info(" ElementsImpFactory.class , return super.Store(vec); no imp,vec size="+vec.size());
		return true;
		
	}

	@Override
	public boolean Store(FwElementsImp_Vector savem,FwElementsImp_Vector deleteem) {
		logger.info(" ElementsImpFactory.class , return super.Store(savem, deleteem); no imp");
		return super.Store(savem, deleteem);
		
	}

	@Override
	public boolean Store(FwElementsImp imp) {
		logger.info(" ElementsImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public boolean Store() {
		logger.info(" ElementsImpFactory.class , return super.Store(); no imp");
		return super.Store();
		
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" ElementsImpFactory.class , return super.DeleteAll(); no imp");
		return super.DeleteAll();
		
	}

	@Override
	public void CancelAll() {
		logger.info(" ElementsImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public void Cancel(FwElementsImp arg0) {
		logger.info(" ElementsImpFactory.class , super.Cancel(arg0); no imp");
		super.Cancel(arg0);
		
	}

	@Override
	public void AddID(FwElementsImp arg0) {
		logger.info(" ElementsImpFactory.class , super.AddID(arg0); no imp");
		super.AddID(arg0);
		
	}

	@Override
	public void Cleanup() {
		logger.info(" ElementsImpFactory.class , super.Cleanup(); no imp");
		super.Cleanup();
		
	}

	@Override
	protected void DoGetAll(FwElementsImp_Vector arg0) {
		logger.info(" ElementsImpFactory.class , super.DoGetAll(arg0); no imp");
		super.DoGetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" ElementsImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" ElementsImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" ElementsImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}

	
	

}
