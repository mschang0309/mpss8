package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.ArlDao;
import mpss.common.jpa.Arl;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwARLImp;
import mpss.schedule.base.FwARLImpFactory;
import mpss.schedule.base.FwARLImpVector;
import mpss.schedule.base.GlARL;
import mpss.schedule.base.GlARL.Type;


public class ArlImpFactory extends FwARLImpFactory implements UIDB<Arl> {
	
	
	private Map<Long, FwARLImp> instances = new HashMap<Long, FwARLImp>();

	private static ArlDao arlDao =JPAInit.getBuilder().getArlDao();
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
		
	public boolean create(Arl object){
		if(arlDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Arl object){
		 return arlDao.update(object);
	 }

	 public boolean delete(Arl object){
		return arlDao.delete(object);	
	 }
	 
	 public boolean doSql(String object){
		 return arlDao.doSql(object);	
	 }
	
	public void releaseFwImp(){
		this.instances.clear();
		arlDao.reset();
	}
	
	@Override
	protected FwARLImp CreateOne(String arg0, Type arg1) {
		logger.info(" ArlImpFactory.class , return super.CreateOne(arg0, arg1); no imp");
		return super.CreateOne(arg0, arg1);
		
	}

	@Override
	protected void DoCleanup() {
		logger.info(" ArlImpFactory.class , super.DoCleanup(); no imp");			
	}

	@Override
	public boolean Store() {
		logger.info(" ArlImpFactory.class , return super.Store(); no imp");
		return true;		
	}
	

	@Override
	public synchronized void delete() {
		logger.info(" ArlImpFactory.class , super.delete(); no imp");
	}

	@Override
	public FwARLImp GetNewOne(String arg0, Type arg1, boolean isShadow) {
		logger.info(" ArlImpFactory.class , return super.GetNewOne(arg0, arg1, isShadow);");
		long newid = arlDao.getNextId();
		FwARLImp arl_imp = new FwARLImp();
		arl_imp.SetID((int) newid);
		arl_imp.SetName(arg0);
		arl_imp.SetARLType(arg1);
		arl_imp.SetShadow(isShadow);
		return arl_imp;
	    //return this.AllocateImp((int) newid, arg0, arg1, isShadow, true);
		//return super.GetNewOne(arg0, arg1,isShadow);
		
		
	}

	@Override
	public boolean CheckName(String arg0) {
		logger.info(" ArlImpFactory.class , return super.CheckName(arg0); no imp");
		return true;
		
	}

	@Override
	public boolean Rename(String arg0, String arg1) {
		logger.info(" ArlImpFactory.class , return super.Rename(arg0, arg1); no imp");
		return true;
		
	}

	@Override
	public void AddNew(FwARLImp arg0) {
		System.out.println(" ArlImpFactory.class , super.AddNew(arg0); no imp");
		logger.info(" ArlImpFactory.class , return super.CreateOne(arg0, arg1); no imp");
	}

	@Override
	public void AddDeleted(FwARLImp arg0) {
		logger.info(" ArlImpFactory.class , super.AddDeleted(arg0); no imp");
	}

	@Override
	public void DoCancel(FwARLImp arg0) {
		logger.info(" ArlImpFactory.class , super.DoCancel(arg0); ");	
	}

	@Override
	public void DoCancelAll() {
		logger.info(" ArlImpFactory.class , super.DoCancelAll(); no imp");	
	}

	@Override
	public void GetAll(FwARLImpVector arg0) {
		logger.info(" ArlImpFactory.class , super.GetAll(arg0); no imp");	
	}

	@Override
	public void Delete(FwARLImpVector arg0) {
		logger.info(" ArlImpFactory.class , super.Delete(arg0); no imp");
	}

	@Override
	public void Delete(FwARLImp arg0) {
		logger.info(" ArlImpFactory.class , super.Delete(arg0); no imp");	
	}

	@Override
	public boolean Store(FwARLImpVector vec) {
		logger.info(" ArlImpFactory.class , return super.Store(vec); no imp");
		return true;
		
	}

	@Override
	public boolean Store(FwARLImpVector savem, FwARLImpVector deleteem) {
		logger.info(" ArlImpFactory.class , return super.Store(savem, deleteem); no imp");
		return true;
		
	}

	@Override
	public boolean Store(FwARLImp imp) {
		logger.info(" ArlImpFactory.class , return super.Store(imp); arlid = "+imp.GetID());
		Arl arl = new Arl();
		arl.setId(Long.valueOf(imp.GetID()));
		arl.setName(imp.GetName());
		arl.setArltype(getArlType(imp.GetARLType()));
		arl.setShadow(imp.GetShadow()==true ? 1 : 0);
		
		return arlDao.update(arl);
		
		//return true;
		
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" ArlImpFactory.class , return super.DeleteAll(); no imp");
		return true;
		
	}

	@Override
	public void CancelAll() {
		logger.info(" ArlImpFactory.class , super.CancelAll(); no imp");		
	}

	@Override
	public void Cancel(FwARLImp arg0) {
		logger.info(" ArlImpFactory.class , super.Cancel(arg0); no imp");		
	}

	@Override
	public void AddID(FwARLImp arg0) {
		logger.info(" ArlImpFactory.class , super.AddID(arg0); no imp");			
	}

	@Override
	public void Cleanup() {
		logger.info(" ArlImpFactory.class , super.Cleanup(); no imp");			
	}

	@Override
	public void DoGetAll(FwARLImpVector arg0, Type arg1) {
		logger.info("call ArlImpFactory.DoGetAll(.,.)");
		String arltype = getArlType(arg1);
		List<Arl> dList = arlDao.getByType(arltype);

		for (Arl s : dList) {
			long lid = s.getId();
			int id = (int) lid;
			FwARLImp imp = new FwARLImp();
			
			imp.SetID(id);
			imp.SetName(s.getName());
			this.SetMyARLType(imp, s.getArltype());
			imp.SetShadow(s.getShadow() == 1 ? true : false);
						
			
			arg0.add(imp);
		}

	}

	@Override
	public FwARLImp CreateOne(String arg0, Type arg1, boolean isShadow) {		
		long newid = arlDao.getNextId();
		FwARLImp imp = new FwARLImp();
		imp.SetID((int) newid);
		imp.SetName(arg0);
		imp.SetARLType(arg1);
		imp.SetShadow(isShadow);
		
		if (imp != null) {
			this.instances.put(newid, imp);
		}		
		return imp;
	}

	@Override
	public void DoGetAll(FwARLImpVector arg0) {
		logger.info("call ArlImpFactory.DoGetAll(.)");
		List<Arl> dList = arlDao.getAll();

		for (Arl s : dList) {
			long lid = s.getId();
			int id = (int) lid;
			FwARLImp imp = new FwARLImp();
			imp.SetID(id);
			imp.SetName(s.getName());
			this.SetMyARLType(imp, s.getArltype());
			imp.SetShadow(s.getShadow() == 1 ? true : false);			
			arg0.add(imp);
		}
	}

	@Override
	public FwARLImp GetOne(String arg0) {
		logger.info("call ArlImpFactory.GetOne(.)");
		Arl s = arlDao.getByName(arg0);
		long lid = s.getId();
		int id = (int) lid;
		FwARLImp imp = new FwARLImp();
		
		imp.SetID(id);
		imp.SetName(s.getName());
		this.SetMyARLType(imp, s.getArltype());
		imp.SetShadow(s.getShadow() == 1 ? true : false);

		return imp;
	}

	@Override
	public void DoLoadAll() {
		logger.info("call ArlImpFactory.DoLoadAll()");
		List<Arl> dList = arlDao.getAll();

		for (Arl s : dList) {
			long lid = s.getId();
			int id = (int) lid;
			FwARLImp imp = new FwARLImp();
			imp.SetID(id);
			imp.SetName(s.getName());
			this.SetMyARLType(imp, s.getArltype());
			imp.SetShadow(s.getShadow() == 1 ? true : false);

			if (!this.instances.containsKey(id)) {
				this.instances.put((long) id, imp);
			}
		}

	}

	@Override
	public FwARLImp GetID(int arg0) {
		FwARLImp imp = null;
		
		if (instances.containsKey(Long.valueOf(arg0))) {
			imp = (FwARLImp) instances.get(Long.valueOf(arg0));
		} else {
			Arl s = arlDao.getArlById(arg0);
			if (s != null) {

				imp = new FwARLImp();
				long lid = s.getId();
				int id = (int) lid;

				imp.SetID(id);
				imp.SetName(s.getName());
				this.SetMyARLType(imp, s.getArltype());
				imp.SetShadow(s.getShadow() == 1 ? true : false);

				if (imp != null) {
					this.instances.put(lid, imp);
				}
			}
		}

		if (imp == null) {
			logger.info("ArlImpFactory GetID(" + arg0 + ") is null");

		}
//		 System.out.println("instance2 size=" + instances.size());
		return imp;
	}
	
    public String getArlType(Type arg1){
    	String arltype ="";
    	if (arg1.equals(GlARL.Type.DEL)) {
			arltype = "DELETE";
		} else if (arg1.equals(GlARL.Type.EXTFILE)) {
			arltype = "EXTFILE";
		} else if (arg1.equals(GlARL.Type.FTYPE)) {
			arltype = "FTYPE";
		} else if (arg1.equals(GlARL.Type.MON)) {
			arltype = "MON";
		} else if (arg1.equals(GlARL.Type.UPLINK)) {
			arltype = "UPLINK";
		} else if (arg1.equals(GlARL.Type.RTCMD)) {
			arltype = "RTCMD";
		} else if (arg1.equals(GlARL.Type.MISCPROC)) {
			arltype = "MISCPROC";
		} else if (arg1.equals(GlARL.Type.RSI)) {
			arltype = "RSI";
		} else if (arg1.equals(GlARL.Type.ISUAL)) {
			arltype = "ISUAL";
		} else if (arg1.equals(GlARL.Type.ISUALPROC)) {
			arltype = "ISUALPROC";
		} else if (arg1.equals(GlARL.Type.MANEUVER)) {
			arltype = "MANEUVER";
		} else if (arg1.equals(GlARL.Type.XCROSS)) {
			arltype = "XCROSS";
		} else if (arg1.equals(GlARL.Type.TOY)) {
			arltype = "TOY";
		} else if (arg1.equals(GlARL.Type.LTYPE)) {
			arltype = "LTYPE";
		}
    	return arltype;
    }
	
	public void SetMyARLType(FwARLImp imp, String type) {		
		if (type.equalsIgnoreCase("DELETE")) {
			imp.SetARLType(GlARL.Type.DEL);
		} else if (type.equalsIgnoreCase("EXTFILE")) {
			imp.SetARLType(GlARL.Type.EXTFILE);
		} else if (type.equalsIgnoreCase("FTYPE")) {
			imp.SetARLType(GlARL.Type.FTYPE);
		} else if (type.equalsIgnoreCase("MON")) {
			imp.SetARLType(GlARL.Type.MON);
		} else if (type.equalsIgnoreCase("UPLINK")) {
			imp.SetARLType(GlARL.Type.UPLINK);
		} else if (type.equalsIgnoreCase("RTCMD")) {
			imp.SetARLType(GlARL.Type.RTCMD);
		} else if (type.equalsIgnoreCase("MISCPROC")) {
			imp.SetARLType(GlARL.Type.MISCPROC);
		} else if (type.equalsIgnoreCase("RSI")) {
			imp.SetARLType(GlARL.Type.RSI);
		} else if (type.equalsIgnoreCase("ISUAL")) {
			imp.SetARLType(GlARL.Type.ISUAL);
		} else if (type.equalsIgnoreCase("ISUALPROC")) {
			imp.SetARLType(GlARL.Type.ISUALPROC);
		} else if (type.equalsIgnoreCase("MANEUVER")) {
			imp.SetARLType(GlARL.Type.MANEUVER);
		} else if (type.equalsIgnoreCase("XCROSS")) {
			imp.SetARLType(GlARL.Type.XCROSS);
		} else if (type.equalsIgnoreCase("TOY")) {
			imp.SetARLType(GlARL.Type.TOY);
		} else if (type.equalsIgnoreCase("LTYPE")) {
			imp.SetARLType(GlARL.Type.LTYPE);
		}else{
			logger.info(" ");
		}

	}
	
	public int addArlByID(int arlid,String name) {
		Arl s = arlDao.getArlById(arlid);
		String s_name = s.getName();
		s.setName(s_name+"_("+name+")");
		if(arlDao.getByName(s.getName())==null){
            long new_arlid = arlDao.getNextId();
            s.setId(new_arlid); 
            arlDao.create(s);
            return Long.valueOf(new_arlid).intValue();
       }else{
            return arlDao.getByName(s.getName()).getId().intValue();
       }
	}
	
	public int addArlByIDBranch(int arlid) {
		Arl s = arlDao.getArlById(arlid);
		String s_name = s.getName();
		s.setName(s_name+"_br");		
        long new_arlid = arlDao.getNextId();
        s.setId(new_arlid); 
        arlDao.create(s);
        return Long.valueOf(new_arlid).intValue();
      
	}
	

}
