package mpss.schedule.imp;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.SessionsatxrefDao;
import mpss.common.jpa.Sessionsatxref;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwSessionSatImp;
import mpss.schedule.base.FwSessionSatImpFactory;
import mpss.schedule.base.FwSessionSatImpVector;

public class SessionSatImpFactory  extends FwSessionSatImpFactory  implements UIDB<Sessionsatxref>{
	
	private Map<Long, FwSessionSatImp> instances = new HashMap<Long, FwSessionSatImp>();

	private static SessionsatxrefDao sessionsatxrefDao=JPAInit.getBuilder().getSessionsatxrefDao();
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	@Override
	public synchronized void delete() {
		logger.info(" SessionSatImpFactory.class , super.delete(); no imp");
		super.delete();		
	}
	
	public boolean create(Sessionsatxref object){
		if(sessionsatxrefDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Sessionsatxref object){
		return sessionsatxrefDao.update(object);
	 }

	 public boolean delete(Sessionsatxref object){
		return sessionsatxrefDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		 return sessionsatxrefDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		this.instances.clear();
		sessionsatxrefDao.reset();
	}

	@Override
	public void Delete(FwSessionSatImp arg0) {
		logger.info(" SessionSatImpFactory.class , super.Delete(arg0); no imp...");
		
	}

	@Override
	public void Delete(FwSessionSatImpVector arg0) {
		logger.info(" SessionSatImpFactory.class , super.Delete(arg0); no imp...");
		
	}
	

	@Override
	public FwSessionSatImp GetOne(int ParentID, int SatId) {
		
		return this.CreateOne(ParentID, SatId);
	}

	@Override
	protected FwSessionSatImp CreateOne(int SessionId, int SatId) {
		logger.info("call SessionSatImpFactory.CreateOne(.,.)");
		long newid = sessionsatxrefDao.getNextId();
		FwSessionSatImp imp = new FwSessionSatImp();
		imp.SetID((int)newid);
		imp.SetSessionID(SessionId);
		imp.SetSatID(SatId);
		if (imp != null) {
			this.instances.put(newid, imp);
		}
		
		return imp;
	}

	@Override
	public FwSessionSatImp GetID(int arg0) {
		logger.info("call SessionSatImpFactory.GetID(.)");
		FwSessionSatImp imp= null;
		
		if (instances.containsKey(Long.valueOf(arg0))){
			imp = (FwSessionSatImp)instances.get(Long.valueOf(arg0));
		} else {
			imp = new FwSessionSatImp();
			Sessionsatxref s = sessionsatxrefDao.getById(arg0);
			
			long lid = s.getId();
			int id = (int) lid;
			
					
			imp.SetID(id);
			imp.SetSessionID(s.getSessionid());
			imp.SetSatID(s.getSatelliteid());
			
			
			if (imp != null) {
				this.instances.put(lid, imp);
			}
		}
		
		
		if(imp==null){
			logger.info("SessionSatImpFactory GetID(" + arg0 +") is null");			
		}
		
		return imp;
	}
	
	public List<Sessionsatxref> getSessionsatxrefBySessionID(int sessionid){
		return sessionsatxrefDao.getBySessionId(sessionid);
	}

	@Override
	public void GetAll(FwSessionSatImpVector arg0, int parentID) {
		//System.out.println("call SessionSatImpFactory.GetAll(.,.)"); OK
		List<Sessionsatxref> dList = sessionsatxrefDao.getBySessionId(parentID);
		for (Sessionsatxref s : dList){
			long lid = s.getId();
			int id = (int) lid;
			FwSessionSatImp imp = new FwSessionSatImp();
			imp.SetID(id);
			imp.SetSessionID(s.getSessionid());
			imp.SetSatID(s.getSatelliteid());
			instances.put(lid, imp);			
			arg0.add(imp);
		}
	}

	
}
