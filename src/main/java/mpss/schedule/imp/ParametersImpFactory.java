package mpss.schedule.imp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.GlobalparametersDao;
import mpss.common.jpa.Globalparameter;
import mpss.schedule.JPAInit;
import mpss.schedule.base.FwParametersImp;
import mpss.schedule.base.FwParametersImpFactory;
import mpss.schedule.base.FwParametersImpVector;

public class ParametersImpFactory extends FwParametersImpFactory {

	@SuppressWarnings("unused")
	private FwParametersImp instances;
	private Map<Long, FwParametersImp> instancesByID = new HashMap<Long, FwParametersImp>();
	private FwParametersImpVector myPmVec = new FwParametersImpVector();
	private static GlobalparametersDao globalparametersDao=JPAInit.getBuilder().getGlobalparametersDao();	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	

	public ParametersImpFactory() {
		super();

		List<Globalparameter> dList = globalparametersDao.getAll();
		if (dList.size() > 0) {
			Globalparameter gp = dList.get(0);
			long lid = gp.getId();
			int id = (int) lid;
			int iprepassqualifier = gp.getPrepassqualifier();
			short prepassqualifier = (short) iprepassqualifier;
			instances = new FwParametersImp(id, Long.valueOf(gp.getColdschedulinglimit()), Long.valueOf(gp.getHotschedulinglimit()), Double.valueOf(gp.getOrbminmaxel()), Long.valueOf(gp
					.getOrbminduration()), Long.valueOf(gp.getPbduration()), prepassqualifier, Long.valueOf(gp.getPassstarttimelead()), Long.valueOf(gp.getPbstarttimelead()), Long.valueOf(gp
					.getPassstarttimelag()), Long.valueOf(gp.getPbstarttimelag()), gp.getCommenttorange(), gp.getUsernameid(), gp.getPbfacilityid(), gp.getEquipgroupid(), gp.getStandardpbcommentid(),
					gp.getPrepbduration(), gp.getSessionduration(), true);
			
		}

	}

	@Override
	public FwParametersImp GetID(int arg0) {
		FwParametersImp imp = null;

		if (instancesByID.containsKey(Long.valueOf(arg0))) {
			imp = (FwParametersImp) instancesByID.get(Long.valueOf(arg0));
		} else {

			Globalparameter gp = globalparametersDao.getById(arg0);
			long lid = gp.getId();
			int id = (int) lid;
			int iprepassqualifier = gp.getPrepassqualifier();
			short prepassqualifier = (short) iprepassqualifier;

			imp = new FwParametersImp(id, Long.valueOf(gp.getColdschedulinglimit()), Long.valueOf(gp.getHotschedulinglimit()), Double.valueOf(gp.getOrbminmaxel()),
					Long.valueOf(gp.getOrbminduration()), Long.valueOf(gp.getPbduration()), prepassqualifier, Long.valueOf(gp.getPassstarttimelead()), Long.valueOf(gp.getPbstarttimelead()),
					Long.valueOf(gp.getPassstarttimelag()), Long.valueOf(gp.getPbstarttimelag()), gp.getCommenttorange(), gp.getUsernameid(), gp.getPbfacilityid(), gp.getEquipgroupid(),
					gp.getStandardpbcommentid(), gp.getPrepbduration(), gp.getSessionduration());

			if (imp != null) {
				this.instancesByID.put(lid, imp);
			}
		}

		if (imp == null) {
			logger.info("ParametersImpFactory GetID(" + arg0 + ") is null");
		}
		
		return imp;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public FwParametersImp GetOne() {
		
		FwParametersImp imp = null;
		this.GetAll(myPmVec);

		if (this.instancesByID.size() == 0) {
			long newid = globalparametersDao.getNextId();
			int id = (int) newid;
			imp = new FwParametersImp(id, Long.valueOf(0), Long.valueOf(0), Double.valueOf(0), Long.valueOf(0), Long.valueOf(0), Short.valueOf("0"), Long.valueOf(0), Long.valueOf(0), Long.valueOf(0), Long.valueOf(0), "",        "",           "",           "",            "",       Long.valueOf(0), 0, true);
													//(int arg0, long arg1,  long arg2,       double arg3,       long arg4,       long arg5,        short arg6,     long arg7,       long arg8,       long arg9,       long arg10,  String arg11, String arg12, String arg13, String arg14, String arg15, long arg16, int arg17, boolean isNew) {

			this.instancesByID.put(newid, imp);
			this.myPmVec.add(imp);
			return imp;
		} else {
			
			Iterator keyItor = this.instancesByID.keySet().iterator();
			while(keyItor.hasNext()){
				long key = (Long) keyItor.next();				
				if (key != -1){
					imp = this.instancesByID.get(key);
					
				}
			}			
		}		
		return imp;

	}

	@Override
	public void DoLoadAll() {
		this.GetAll(myPmVec);
	}

	@Override
	public void GetAll(FwParametersImpVector arg0) {		
		List<Globalparameter> sList = globalparametersDao.getAll();
		for (Globalparameter gp : sList) {

			long lid = gp.getId();
			int id = (int) lid;
			int iprepassqualifier = gp.getPrepassqualifier();
			short prepassqualifier = (short) iprepassqualifier;

			FwParametersImp imp = new FwParametersImp(id, Long.valueOf(gp.getColdschedulinglimit()), Long.valueOf(gp.getHotschedulinglimit()), Double.valueOf(gp.getOrbminmaxel()), Long.valueOf(gp
					.getOrbminduration()), Long.valueOf(gp.getPbduration()), prepassqualifier, Long.valueOf(gp.getPassstarttimelead()), Long.valueOf(gp.getPbstarttimelead()), Long.valueOf(gp
					.getPassstarttimelag()), Long.valueOf(gp.getPbstarttimelag()), gp.getCommenttorange(), gp.getUsernameid(), gp.getPbfacilityid(), gp.getEquipgroupid(), gp.getStandardpbcommentid(),
					gp.getPrepbduration(), gp.getSessionduration());

			if (!this.instancesByID.containsKey(id)) {
				this.instancesByID.put((long) id, imp);
			}			
			arg0.add(imp);
		}		

	}

}
