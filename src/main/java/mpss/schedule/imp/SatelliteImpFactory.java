package mpss.schedule.imp;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.SatelliteDao;
import mpss.common.jpa.Satellite;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwSatelliteImp;
import mpss.schedule.base.FwSatelliteImpFactory;
import mpss.schedule.base.FwSatelliteImpVector;
import mpss.schedule.base.GlSatellite;
import mpss.schedule.base.GlSatellite.AliasType;
import mpss.schedule.base.GlSatellite.Type;


public class SatelliteImpFactory extends FwSatelliteImpFactory implements UIDB<Satellite>{

	private Map<Long, FwSatelliteImp> instancesByID = new HashMap<Long, FwSatelliteImp>();
	private Map<String, FwSatelliteImp> instancesByName = new HashMap<String, FwSatelliteImp>();

	private static SatelliteDao satelliteDao=JPAInit.getBuilder().getSatelliteDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());

		
	public boolean create(Satellite object){
		if(satelliteDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Satellite object){
		return  satelliteDao.update(object);
	 }

	 public boolean delete(Satellite object){
		 return satelliteDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		 return satelliteDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		this.instancesByID.clear();
		this.instancesByName.clear();
		satelliteDao.reset();
	}

	@Override
	public FwSatelliteImp GetOne(String arg0) {		
		DoLoadAll();
		if (this.instancesByName.containsKey(arg0) == false) {
			logger.info("Satellite name:" + arg0 + " is not in database.");
			return null;
		}

		return this.instancesByName.get(arg0);
	}

	@Override
	public void DoLoadAll() {		
		List<Satellite> list = satelliteDao.getAll();
		for (Satellite s : list){
			long lid = s.getId();
			int id = (int) lid;
			
			int nominal = s.getNominalorbit() == null ? 0: s.getNominalorbit();
			FwSatelliteImp imp = new FwSatelliteImp(id, s.getName(), GlSatellite.Type.NoType, s.getOpsnumber(), s.getOps4number(), Long.valueOf(nominal), Long.valueOf(s.getMaxandrifttime()), Long.valueOf(s
					.getEphpropduration()), s.getCommanddbscenario(), s.getScenarioversion(), s.getProject(), s.getVideogain());
			
			imp.SetType(configFactory.getSetlliteType());
			
			if (!this.instancesByID.containsKey(id)) {
				this.instancesByID.put((long) id, imp);
			}

			if (!this.instancesByName.containsKey(s.getName())) {
				this.instancesByName.put(s.getName(), imp);
			}
			
		}
	}

	@Override
	public FwSatelliteImp DoGetOne(String arg0, AliasType arg1) {
		logger.info("call SatelliteImpFactory.DoGetOne(.,.)");
		Satellite s = null;
		if (arg1.equals(AliasType.OPSAlias)) {
			s = satelliteDao.getByOpsnumber(arg0);
		} else if (arg1.equals(AliasType.OPS4Alias)) {
			s = satelliteDao.getByOps4number(arg0);
		}
		FwSatelliteImp imp = null;
		if (s != null) {
			long lid = s.getId();
			int id = (int) lid;
			
			int nominal = s.getNominalorbit() == null ? 0: s.getNominalorbit();
			imp = new FwSatelliteImp(id, s.getName(), GlSatellite.Type.NoType, s.getOpsnumber(), s.getOps4number(), Long.valueOf(nominal), Long.valueOf(s.getMaxandrifttime()), Long.valueOf(s
					.getEphpropduration()), s.getCommanddbscenario(), s.getScenarioversion(), s.getProject(), s.getVideogain());
			
			imp.SetType(configFactory.getSetlliteType());
		}
		return imp;
	}

	@Override
	public FwSatelliteImp GetNewOne(String name, Type type, String opsnum, String ops4num, long nominalOrbit, long maxANDriftTime, long epd, String videoGain) {
		logger.info("call SatelliteImpFactory.GetNewOne(...)");
		long newid = satelliteDao.getNextId();

		FwSatelliteImp imp = new FwSatelliteImp((int) newid, name, type, opsnum, ops4num, nominalOrbit, maxANDriftTime, epd, "", 0, "", videoGain);
		
		if (imp != null) {
			this.instancesByID.put(newid, imp);
			this.instancesByName.put(name, imp);
		}
		return imp;
	}

	@Override
	public FwSatelliteImp GetID(int arg0) {		
		FwSatelliteImp imp= null;
		
		if (instancesByID.containsKey(Long.valueOf(arg0))){
			imp = (FwSatelliteImp)instancesByID.get(Long.valueOf(arg0));
		} else {			
			
			Satellite s = satelliteDao.getSatelliteById(arg0);			
			
			long lid = s.getId();
			int id = (int) lid;			
			int nominal = s.getNominalorbit() == null ? 0: s.getNominalorbit();
			imp = new FwSatelliteImp(id, s.getName(), GlSatellite.Type.NoType, s.getOpsnumber(), s.getOps4number(), Long.valueOf(nominal), Long.valueOf(s.getMaxandrifttime()), Long.valueOf(s
					.getEphpropduration()), s.getCommanddbscenario(), s.getScenarioversion(), s.getProject(), s.getVideogain());
			
			imp.SetType(configFactory.getSetlliteType());			
			
			if (imp != null) {
				this.instancesByID.put(lid, imp);
				this.instancesByName.put(s.getName(), imp);
			}
			
		}
		
		
		if(imp==null){
			logger.info("SatelliteImpFactory GetID(" + arg0 +") is null");			
		}
		
		return imp;
	}

	@Override
	public void GetAll(FwSatelliteImpVector arg0) {		
		this.DoGetAll(arg0);
	}

	@Override
	public void DoGetAll(FwSatelliteImpVector arg0) {		
		List<Satellite> list = satelliteDao.getAll();
		for (Satellite s : list){
			long lid = s.getId();
			int id = (int) lid;
			
			int nominal = s.getNominalorbit() == null ? 0: s.getNominalorbit();
			FwSatelliteImp imp = new FwSatelliteImp(id, s.getName(), GlSatellite.Type.NoType, s.getOpsnumber(), s.getOps4number(), Long.valueOf(nominal), Long.valueOf(s.getMaxandrifttime()), Long.valueOf(s
					.getEphpropduration()), s.getCommanddbscenario(), s.getScenarioversion(), s.getProject(), s.getVideogain());
			
			imp.SetType(configFactory.getSetlliteType());
			
			
			if (!this.instancesByID.containsKey(id)) {
				this.instancesByID.put((long) id, imp);
			}

			if (!this.instancesByName.containsKey(s.getName())) {
				this.instancesByName.put(s.getName(), imp);
			}
			
			arg0.add(imp);
		}
		
	}

	@Override
	public synchronized void delete() {
		logger.info(" SatelliteImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	protected FwSatelliteImp CreateOne(String name, Type type, String opsnum,String ops4num, long nominalOrbit, long maxANDriftTime, long epd,String videoGain) {
		logger.info(" SatelliteImpFactory.class , return super.CreateOne(...); no imp");
		return super.CreateOne(name, type, opsnum, ops4num, nominalOrbit,maxANDriftTime, epd, videoGain);
		
	}

	@Override
	protected void DoCleanup() {
		logger.info(" SatelliteImpFactory.class , super.DoCleanup(); no imp");
		super.DoCleanup();
		
	}

	@Override
	public boolean CheckName(String arg0) {
		logger.info(" SatelliteImpFactory.class , return super.CheckName(arg0); no imp");
		return super.CheckName(arg0);
		
	}

	@Override
	public boolean Rename(String arg0, String arg1) {
		logger.info(" SatelliteImpFactory.class , return super.Rename(arg0, arg1); no imp");
		return super.Rename(arg0, arg1);
		
	}

	@Override
	public void AddNew(FwSatelliteImp arg0) {
		logger.info(" SatelliteImpFactory.class , super.AddNew(arg0); no imp");
		super.AddNew(arg0);
		
	}

	@Override
	public void AddDeleted(FwSatelliteImp arg0) {
		logger.info(" SatelliteImpFactory.class , super.AddDeleted(arg0); no imp");
		super.AddDeleted(arg0);
		
	}

	@Override
	public void DoCancel(FwSatelliteImp arg0) {
		logger.info(" SatelliteImpFactory.class , super.DoCancel(arg0); no imp");
		super.DoCancel(arg0);
		
	}

	@Override
	public void DoCancelAll() {
		logger.info(" SatelliteImpFactory.class , super.DoCancelAll(); no imp");
		super.DoCancelAll();
		
	}

	@Override
	public void Delete(FwSatelliteImpVector arg0) {
		logger.info(" SatelliteImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwSatelliteImp arg0) {
		logger.info(" SatelliteImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public boolean Store(FwSatelliteImpVector vec) {
		logger.info(" SatelliteImpFactory.class , return super.Store(vec); no imp");
		return super.Store(vec);
		
	}

	@Override
	public boolean Store(FwSatelliteImpVector savem,FwSatelliteImpVector deleteem) {
		logger.info(" SatelliteImpFactory.class , return super.Store(savem, deleteem); no imp");
		return super.Store(savem, deleteem);
		
	}

	@Override
	public boolean Store(FwSatelliteImp imp) {
		logger.info(" SatelliteImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public boolean Store() {
		logger.info(" SatelliteImpFactory.class , return super.Store(); no imp");
		return super.Store();
		
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" SatelliteImpFactory.class , return super.DeleteAll(); no imp");
		return super.DeleteAll();
		
	}

	@Override
	public void CancelAll() {
		logger.info(" SatelliteImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public void Cancel(FwSatelliteImp arg0) {
		logger.info(" SatelliteImpFactory.class , super.Cancel(arg0); no imp");
		super.Cancel(arg0);
		
	}

	@Override
	public void AddID(FwSatelliteImp arg0) {
		logger.info(" SatelliteImpFactory.class , super.AddID(arg0); no imp");
		super.AddID(arg0);
		
	}

	@Override
	public void Cleanup() {
		logger.info(" SatelliteImpFactory.class , super.Cleanup(); no imp");
		super.Cleanup();
		
	}

	

}
