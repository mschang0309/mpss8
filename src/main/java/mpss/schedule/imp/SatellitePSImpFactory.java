package mpss.schedule.imp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.SatellitepDao;
import mpss.common.jpa.Satellitep;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.*;


public class SatellitePSImpFactory extends FwSatellitePSImpFactory implements UIDB<Satellitep>  {
		
	private Map<Long, FwSatellitePSImp> instances = new HashMap<Long, FwSatellitePSImp>();
	private static SatellitepDao satellitepDao=JPAInit.getBuilder().getSatellitepDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public boolean create(Satellitep object){
		if(satellitepDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Satellitep object){
		 return satellitepDao.update(object);
	 }

	 public boolean delete(Satellitep object){
		 return satellitepDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		 return satellitepDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		instances.clear();
		satellitepDao.reset();
	}

	@Override
	public synchronized void delete() {
		logger.info(" SatellitePSImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwSatellitePSImp GetOne(int satid, int psid,FwSatellitePSImp fromImp, boolean copyKids) {
		logger.info(" SatellitePSImpFactory.class , return super.GetOne(satid, psid, fromImp, copyKids); no imp");
		return super.GetOne(satid, psid, fromImp, copyKids);
		
	}

	@Override
	public FwSatellitePSImp GetOne(int parentID, int psID, int prio, long thd,long slew, long preheat, long focal, long rrs, long rrc, long rps,long rpc, long irs, long irc, long ips, long ipc, double pdd,long xsd, long mri, int rpsd, long secPreheat) {
		logger.info(" SatellitePSImpFactory.class , return super.GetOne(...); no imp");
		return super.GetOne(parentID, psID, prio, thd, slew, preheat, focal, rrs, rrc,rps, rpc, irs, irc, ips, ipc, pdd, xsd, mri, rpsd, secPreheat);
		
	}

	@Override
	protected FwSatellitePSImp CreateOne(int satelliteID, int psid, int prio,long thd, long slew, long preheat, long focal, long rrs, long rrc,long rps, long rpc, long irs, long irc, long ips, long ipc,double pdd, long xsd, long mri, int rpsd, long secPreheat) {
		logger.info(" SatellitePSImpFactory.class , return super.CreateOne(...); no imp");
		return super.CreateOne(satelliteID, psid, prio, thd, slew, preheat, focal, rrs,rrc, rps, rpc, irs, irc, ips, ipc, pdd, xsd, mri, rpsd, secPreheat);
		
	}

	@Override
	protected FwSatellitePSImp CreateOne(int satid, int psid,FwSatellitePSImp fromImp, boolean copykids) {
		logger.info(" SatellitePSImpFactory.class , return super.CreateOne(satid, psid, fromImp, copykids); no imp");
		return super.CreateOne(satid, psid, fromImp, copykids);
		
	}

	@Override
	public FwSatellitePSImp GetID(int arg0) {
		logger.info(" SatellitePSImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);
		
	}

	@Override
	public void GetAll(FwSatellitePSImpVector arg0, int parentID) {
		logger.info("cal SatellitePSImpFactory.class , super.GetAll(arg0, parentID); OK but object some par is error =" + parentID);
		List<Satellitep> list = satellitepDao.listSatelliteps(parentID);
		Iterator<Satellitep> item = list.iterator();
		while (item.hasNext()) {
			Satellitep con = (Satellitep) item.next();			
			FwSatellitePSImp fwrev = new FwSatellitePSImp(
					con.getId().intValue(),
					parentID, 
					con.getPsid().intValue(),
					con.getPriority().intValue(),
					Long.valueOf(String.valueOf(con.getTwtaheatdelay())).longValue(),					
					Long.valueOf(String.valueOf(con.getSlewmaneuverdelay())).longValue(),					
					Long.valueOf(String.valueOf(con.getRsiimagerpreheatdelay())).longValue(),
					Long.valueOf(String.valueOf(con.getRsifocalplanedelay())).longValue(),
					Long.valueOf(String.valueOf(con.getRsirecsetupdelay())).longValue(),
					Long.valueOf(String.valueOf(con.getRsireccleanupdelay())).longValue(),
					Long.valueOf(String.valueOf(con.getRsipbsetupdelay())).longValue(),
					Long.valueOf(String.valueOf(con.getRsipbcleanupdelay())).longValue(),
					Long.valueOf(String.valueOf(con.getIsualrecsetupdelay())).longValue(),
					Long.valueOf(String.valueOf(con.getIsualreccleanupdelay())).longValue(),
					Long.valueOf(String.valueOf(con.getIsualpbsetupdelay())).longValue(),
					Long.valueOf(String.valueOf(con.getIsualpbcleanupdelay())).longValue(),									
					Double.valueOf(String.valueOf(con.getPostddtdelay())).doubleValue(),					
					Long.valueOf(String.valueOf(con.getXbandswitchdelay())).longValue(),
					Long.valueOf(String.valueOf(con.getMinrfpainterval())).longValue(),
					con.getRsipbkshift().intValue(),
					Long.valueOf(String.valueOf(con.getRsisecondarypreheatdelay())).longValue(),  
					false);
			arg0.add(fwrev);
			this.instances.put(con.getId().longValue(), fwrev);
		}
		
	}

	@Override
	public boolean Store(FwSatellitePSImpVector imps,FwSatellitePSImpVector delimps) {
		logger.info(" SatellitePSImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwSatellitePSImp imp) {
		logger.info(" SatellitePSImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public void Delete(FwSatellitePSImp arg0) {
		logger.info(" SatellitePSImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwSatellitePSImpVector arg0) {
		logger.info(" SatellitePSImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	
	public Satellitep getBySatIdNPsId(int satId, int psId) {
		return satellitepDao.getBySatIdNPsId(satId, psId);		
	}
	
	
}
