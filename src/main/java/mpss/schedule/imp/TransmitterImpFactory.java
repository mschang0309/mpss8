package mpss.schedule.imp;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.TransmitterDao;
import mpss.common.jpa.Transmitter;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.*;

public class TransmitterImpFactory extends FwTransmitterImpFactory  implements UIDB<Transmitter>{
			
	private Map<Long, FwTransmitterImp> instances = new HashMap<Long ,FwTransmitterImp>();
	private static TransmitterDao transmitterDao=JPAInit.getBuilder().getTransmitterDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	public boolean create(Transmitter object){
		if(transmitterDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Transmitter object){
		 return transmitterDao.update(object);
	 }

	 public boolean delete(Transmitter object){
		 return transmitterDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		 return transmitterDao.doSql(jpsql);	
	 }

	public void releaseFwImp(){
		
		this.instances.clear();
		transmitterDao.reset();
	}
	
	@Override
	public synchronized void delete() {
		logger.info(" TransmitterImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwTransmitterImp GetOne(int parentID, String name) {
		logger.info(" TransmitterImpFactory.class , return super.GetOne(parentID, name); no imp");
		return super.GetOne(parentID, name);
		
	}

	@Override
	protected FwTransmitterImp CreateOne(int parentID, String name) {
		logger.info(" TransmitterImpFactory.class , return super.CreateOne(parentID, name); no imp");
		return super.CreateOne(parentID, name);
		
	}

	@Override
	public FwTransmitterImp GetID(int arg0) {
		logger.info(" TransmitterImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);
		
	}

	@Override
	public void GetAll(FwTransmitterImpVector arg0, int parentID) {
		//System.out.println(" TransmitterImpFactory.class , super.GetAll(arg0, parentID); no imp");
		List<Transmitter> list = transmitterDao.listAntennas(parentID);
		Iterator<Transmitter> item = list.iterator();
		while (item.hasNext()) {
			Transmitter con = (Transmitter) item.next();
			FwTransmitterImp fwrev = new FwTransmitterImp(con.getId().intValue(),
					parentID, con.getName(),false);
			arg0.add(fwrev);
			this.instances.put(con.getId().longValue(), fwrev);
		}
		
	}

	@Override
	public void Delete(FwTransmitterImp arg0) {
		logger.info(" TransmitterImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwTransmitterImpVector arg0) {
		logger.info(" TransmitterImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" TransmitterImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" TransmitterImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" TransmitterImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}

	@Override
	public void CancelAll() {
		logger.info(" TransmitterImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();		
	}
	
}
