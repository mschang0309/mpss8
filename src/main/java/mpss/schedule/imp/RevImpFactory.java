package mpss.schedule.imp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.RevDao;
import mpss.common.jpa.Rev;
import mpss.schedule.JPAInit;
import mpss.schedule.Schedule;
import mpss.schedule.UIDB;
import mpss.schedule.Schedule.SessionType;
import mpss.schedule.base.*;
import mpss.util.timeformat.DateUtil;

public class RevImpFactory extends FwRevImpFactory implements UIDB<Rev> {

	public static Map<Long, FwRevImp> instances = new HashMap<Long, FwRevImp>();
	private static RevDao revDao=JPAInit.getBuilder().getRevDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public boolean create(Rev object){
		if(revDao.create(object)==null){			
			return false;
		}else{
			return true;
		}
	 }
	
	public Long createGetId(Rev object){
		Rev data  =revDao.create(object);
		
		return data.getId();		
	 }

	 public boolean update(Rev object){
		 return revDao.update(object);
	 }

	 public boolean delete(Rev object){
		return  revDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return revDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		RevImpFactory.instances.clear();
		revDao.reset();
	}

	@Override
	public synchronized void delete() {
		logger.info(" RevImpFactory.class , super.delete(); no imp");
		super.delete();

	}

	@Override
	public FwRevImp GetOne(int sat, int ar, GlTime an, GlTime dn, GlTime end,int branch) {
		logger.info(" RevImpFactory.class , return super.GetOne(sat, ar, an, dn, end, branch); no imp");
		return super.GetOne(sat, ar, an, dn, end, branch);

	}

	@Override
	public FwRevImp GetOne(int sat, int ar, int branch) {
		logger.info(" RevImpFactory.class , return super.GetOne(sat, ar, branch); no imp");
		return super.GetOne(sat, ar, branch);

	}

	@Override
	public FwRevImp GetOne(int sat, GlTime time, int bid) {
		logger.info(" RevImpFactory.class , return super.GetOne(sat, time, bid); no imp");
		return super.GetOne(sat, time, bid);

	}

	@Override
	public void GetAll(FwRevImpVector arg0, GlTimeRange arg1, int arg2) {
		String starttime = arg1.GetStartTime().asString("%Y-%m-%d %H:%M:%S");
		String endtime = arg1.GetEndTime().asString("%Y-%m-%d %H:%M:%S");
		if(Schedule.s_type== SessionType.BRANCH){
			arg2 = Schedule.branch_type_id;
			if(arg2==0){				
				logger.info("rev getall ========================== branch="+arg2);
				return;
			}
		}
		List<Rev> list = revDao.getByBranchIdNTimeRange(arg2, starttime,
				endtime);

		logger.info("FwRevImpFactory::GetAll.  Loading(" + starttime + "- "+ endtime +")" + " bid: " + arg2 + " #loaded: " + list.size());
		
		Iterator<Rev> item = list.iterator();
		while (item.hasNext()) {
			Rev con = (Rev) item.next();
			FwRevImp fwrev = new FwRevImp(con.getId().intValue(), con.getScid()
					.intValue(), con.getRevno().intValue(),
					DateUtil.timestampToGlTIme(con.getAntime()),
					DateUtil.timestampToGlTIme(con.getDntime()),
					DateUtil.timestampToGlTIme(con.getRevendtime()), 0);
			
			arg0.add(fwrev);
			RevImpFactory.instances.put(con.getId().longValue(), fwrev);
		}

	}

	@Override
	public void GetAll(FwRevImpVector arg0, int arg1) {
		logger.info(" RevImpFactory.class , super.GetAll(arg0, arg1); no imp");
		
	}

	@Override
	public void GetAllSats(FwRevImpVector arg0, int arg1) {
		logger.info(" RevImpFactory.class , super.GetAllSats(arg0, arg1); no imp");
		
	}

	@Override
	public boolean CheckSatelliteDependency(int satelliteID) {
		logger.info(" RevImpFactory.class , return super.CheckSatelliteDependency(satelliteID); no imp");
		return true;

	}

	@Override
	protected void AddNew(FwRevImp arg0) {
		logger.info(" RevImpFactory.class , super.AddNew(arg0); no imp");
		super.AddNew(arg0);

	}

	@Override
	protected void AddDeleted(FwRevImp arg0) {
		logger.info(" RevImpFactory.class , super.AddDeleted(arg0); no imp");
	}

	@Override
	protected void DoCancel(FwRevImp arg0) {
		logger.info(" RevImpFactory.class , super.DoCancel(arg0); no imp");
		
	}

	@Override
	protected void DoCancelAll() {
		logger.info(" RevImpFactory.class , super.DoCancelAll(); no imp");
	}

	@Override
	protected void DoCleanup() {
		logger.info(" RevImpFactory.class , super.DoCleanup(); no imp");
	}

	@Override
	protected FwRevImp CreateOne(int sat, int ar, GlTime an, GlTime dn,GlTime end, int branch) {
		logger.info(" RevImpFactory.class , return super.CreateOne(sat, ar, an, dn, end, branch); no imp");
		return super.CreateOne(sat, ar, an, dn, end, branch);

	}

	@Override
	protected void LoadAll(FwRevImpVector arg0, GlTimeRange arg1, int branch) {
		logger.info(" RevImpFactory.class , super.LoadAll(arg0, arg1, branch); no imp");

	}

	@Override
	protected void LoadAll(FwRevImpVector arg0, int branch) {
		logger.info(" RevImpFactory.class , super.LoadAll(arg0, branch); no imp");
		super.LoadAll(arg0, branch);

	}

	@Override
	protected void LoadAllSats(FwRevImpVector arg0, int satid) {
		logger.info(" RevImpFactory.class , super.LoadAllSats(arg0, satid); no imp");
		super.LoadAllSats(arg0, satid);

	}

	@Override
	protected FwRevImp LoadOne(int sat, int ar, int branch) {
		logger.info(" RevImpFactory.class , return super.LoadOne(sat, ar, branch); no imp");
		return super.LoadOne(sat, ar, branch);

	}

	@Override
	protected FwRevImp LoadOne(int sat, GlTime t, int branch) {
		logger.info(" RevImpFactory.class , return super.LoadOne(sat, t, branch); no imp");
		return super.LoadOne(sat, t, branch);

	}

	@Override
	protected boolean CheckSatelliteInDatabase(int satelliteID) {
		logger.info(" RevImpFactory.class , return super.CheckSatelliteInDatabase(satelliteID); no imp");
		return true;

	}

	@Override
	public FwRevImp GetID(int arg0) {
		Rev con = revDao.getRevbyID(arg0);
		FwRevImp fwrev = new FwRevImp(con.getId().intValue(), con.getScid()
				.intValue(), con.getRevno().intValue(),
				DateUtil.timestampToGlTIme(con.getAntime()),
				DateUtil.timestampToGlTIme(con.getDntime()),
				DateUtil.timestampToGlTIme(con.getRevendtime()), 0);
		return fwrev;

	}

	@Override
	public void GetAll(FwRevImpVector arg0) {
		logger.info(" RevImpFactory.class , super.GetAll(arg0); no imp");
	}

	@Override
	public void Delete(FwRevImpVector arg0) {
		logger.info(" RevImpFactory.class , super.Delete(arg0); no imp");

	}

	@Override
	public void Delete(FwRevImp arg0) {
		logger.info(" RevImpFactory.class , super.Delete(arg0); no imp");	

	}

	@Override
	public boolean Store(FwRevImpVector vec) {
		logger.info(" RevImpFactory.class , return super.Store(vec); no imp,vec size="+vec.size());
		return true;

	}

	@Override
	public boolean Store(FwRevImpVector savem, FwRevImpVector deleteem) {
		logger.info(" RevImpFactory.class , return super.Store(savem, deleteem); no imp");
		return true;

	}

	@Override
	public boolean Store(FwRevImp imp) {
		logger.info(" RevImpFactory.class , return super.Store(imp); no imp");
		return true;

	}

	@Override
	public boolean Store() {
		logger.info(" RevImpFactory.class , return super.Store(); no imp");
		return true;

	}

	@Override
	public boolean DeleteAll() {
		logger.info(" RevImpFactory.class , return super.DeleteAll(); no imp");
		return true;

	}

	@Override
	public void CancelAll() {
		logger.info(" RevImpFactory.class , super.CancelAll(); no imp");	

	}

	@Override
	public void Cancel(FwRevImp arg0) {
		logger.info(" RevImpFactory.class , super.Cancel(arg0); no imp");
		super.Cancel(arg0);

	}

	@Override
	public void AddID(FwRevImp arg0) {
		logger.info(" RevImpFactory.class , super.AddID(arg0); no imp");
		super.AddID(arg0);

	}

	@Override
	public void Cleanup() {
		logger.info(" RevImpFactory.class , super.Cleanup(); no imp");
	}

	@Override
	protected void DoGetAll(FwRevImpVector arg0) {
		logger.info(" RevImpFactory.class , super.DoGetAll(arg0); no imp");

	}

	@Override
	protected void GetSession(GlTime arg0, int arg1) {
		logger.info(" RevImpFactory.class , super.GetSession(arg0, arg1); no imp");

	}

	@Override
	protected void GetBranch(int arg0) {
		logger.info(" RevImpFactory.class , super.GetBranch(arg0); no imp");

	}

	@Override
	protected FwRevImp FindOne(int sat, int arg1, int branch) {
		logger.info(" RevImpFactory.class , return super.FindOne(sat, arg1, branch); no imp");
		return null;

	}
	
	public  List<Rev> getRevByTime(String starttime,String endtime , int branch) {		
		return  revDao.getByBranchIdNTimeRange(branch, starttime, endtime);			
	}
	

}
