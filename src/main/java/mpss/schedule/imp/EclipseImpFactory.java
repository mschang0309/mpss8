package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.EclipseDao;
import mpss.common.jpa.Eclipse;
import mpss.schedule.JPAInit;
import mpss.schedule.Schedule;
import mpss.schedule.UIDB;
import mpss.schedule.Schedule.SessionType;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwEclipseImp;
import mpss.schedule.base.FwEclipseImpFactory;
import mpss.schedule.base.FwEclipseImpVector;
import mpss.schedule.base.GlEclipse;
import mpss.schedule.base.GlEclipse.Type;
import mpss.schedule.base.GlTime;
import mpss.schedule.base.GlTimeRange;
import mpss.util.timeformat.DateUtil;


public class EclipseImpFactory extends FwEclipseImpFactory implements UIDB<Eclipse> {
	
	public static Map<Long, FwEclipseImp> instances = new HashMap<Long, FwEclipseImp>();

	private static EclipseDao eclipseDao=JPAInit.getBuilder().getEclipseDao();
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	public boolean create(Eclipse object){
		if(eclipseDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Eclipse object){
		 return eclipseDao.update(object);
	 }

	 public boolean delete(Eclipse object){
		 return eclipseDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		 return eclipseDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		EclipseImpFactory.instances.clear();
		eclipseDao.reset();
	}
	
	@Override
	public synchronized void delete() {
		logger.info(" EclipseImpFactory.class , super.delete(); no imp");				
	}

	@Override
	public FwEclipseImp GetOne(int sat, int rev, Type type, GlTime penter,
			GlTime uenter, GlTime uexit, GlTime pexit, int branch) {
		logger.info(" EclipseImpFactory.class , return super.GetOne(sat, rev, type, penter, uenter, uexit, pexit, branch); no imp");
		return super.GetOne(sat, rev, type, penter, uenter, uexit, pexit, branch);
		
	}

	@Override
	public FwEclipseImp GetOne(int sat, int rev, Type type, int branch) {
		logger.info(" EclipseImpFactory.class , return super.GetOne(sat, rev, type, branch); no imp");
		return super.GetOne(sat, rev, type, branch);
		
	}

	@Override
	public void GetAll(FwEclipseImpVector imps, GlTimeRange tr, int branch) {
		//System.out.println(" EclipseImpFactory.class , super.GetAll(imps, tr, branch); branch="+branch);
		String starttime = tr.GetStartTime().asString("%Y-%m-%d %H:%M:%S");
		String endtime = tr.GetEndTime().asString("%Y-%m-%d %H:%M:%S");
		if(Schedule.s_type== SessionType.BRANCH){
			branch = Schedule.branch_type_id;
			if(branch==0){
				logger.info("eclipse getall ========================== branch="+branch);	
				return;
			}
		}
		List<Eclipse> lists=eclipseDao.getByBranchNTimerange(branch, starttime, endtime);
		logger.info("FwEclipseImpFactory::GetAll.  Loading("+starttime+" - "+endtime+") bid: "+branch+" #loaded: "+lists.size());
		for (Eclipse eclipse : lists){	
			long lid = eclipse.getId();
			int id = (int) lid;
//			FwEclipseImp imp = new FwEclipseImp(id,eclipse.getSatelliteid(),eclipse.getRevid(),getEclipseType(eclipse.getEclipsetype()),
//					DateUtil.timestampToGlTIme(eclipse.getPenumbraentrancetime()),
//					DateUtil.timestampToGlTIme(eclipse.getUmbraentrancetime()),
//					DateUtil.timestampToGlTIme(eclipse.getUmbraexittime()),
//					DateUtil.timestampToGlTIme(eclipse.getPenumbraexittime()),eclipse.getBranchid(),false);
			
			FwEclipseImp imp = new FwEclipseImp(id,eclipse.getSatelliteid(),eclipse.getRevid(),getEclipseType(eclipse.getEclipsetype()),
					DateUtil.timestampToGlTIme(eclipse.getPenumbraentrancetime()),
					DateUtil.timestampToGlTIme(eclipse.getUmbraentrancetime()),
					DateUtil.timestampToGlTIme(eclipse.getUmbraexittime()),
					DateUtil.timestampToGlTIme(eclipse.getPenumbraexittime()),0,false);
			
			
			EclipseImpFactory.instances.put((long)imp.GetID(), imp);			
			imps.add(imp);
		}
		
		
	}
	
	public GlEclipse.Type getEclipseType(String type){
		if(type.equals("FTYPE")){
			return GlEclipse.Type.FTYPE;
		}else if(type.equals("Earth/Sun")){
			return GlEclipse.Type.EARTH_SUN;
		}else if(type.equals("MOON/SUN")){
			return GlEclipse.Type.MOON_SUN;
		}else if(type.equals("EARTH/MOON")){
			return GlEclipse.Type.EARTH_MOON;
		}else if(type.equals("LTYPE")){
			return GlEclipse.Type.LTYPE;
		}else{
			return GlEclipse.Type.LTYPE;
		}
		 
	}

	@Override
	public void GetAll(FwEclipseImpVector arg0, int arg1) {
		logger.info(" EclipseImpFactory.class , super.GetAll(arg0, arg1); no imp");				
	}

	@Override
	public void GetAllSats(FwEclipseImpVector arg0, int arg1) {
		logger.info(" EclipseImpFactory.class , super.GetAllSats(arg0, arg1); no imp");					
	}

	@Override
	public void GetAllRevs(FwEclipseImpVector arg0, int arg1) {
		logger.info(" EclipseImpFactory.class , super.GetAllRevs(arg0, arg1); no imp");					
	}

	@Override
	public void GetAllTypes(FwEclipseImpVector arg0, Type arg1) {
		logger.info(" EclipseImpFactory.class , super.GetAllTypes(arg0, arg1); no imp");
			
	}

	@Override
	public boolean CheckSatelliteDependency(int satelliteID) {
		logger.info(" EclipseImpFactory.class , return super.CheckSatelliteDependency(satelliteID); no imp");
		return true;		
	}

	@Override
	protected void AddNew(FwEclipseImp arg0) {
		logger.info(" EclipseImpFactory.class , super.AddNew(arg0); no imp");				
	}

	@Override
	protected void AddDeleted(FwEclipseImp arg0) {
		logger.info(" EclipseImpFactory.class , super.AddDeleted(arg0); no imp");		
	}

	@Override
	protected void DoCancel(FwEclipseImp arg0) {
		logger.info(" EclipseImpFactory.class , super.DoCancel(arg0); no imp");			
	}

	@Override
	protected void DoCancelAll() {
		logger.info(" EclipseImpFactory.class , super.DoCancelAll(); no imp");		
	}

	@Override
	protected void DoCleanup() {
		logger.info(" EclipseImpFactory.class , super.DoCleanup(); no imp");				
	}

	@Override
	protected FwEclipseImp CreateOne(int sat, int rev, Type type,GlTime penter, GlTime uenter, GlTime uexit, GlTime pexit, int branch) {
		logger.info(" EclipseImpFactory.class , return super.CreateOne(sat, rev, type, penter, uenter, uexit, pexit, branch); no imp");
		return super.CreateOne(sat, rev, type, penter, uenter, uexit, pexit, branch);
		
	}

	@Override
	protected void LoadAll(FwEclipseImpVector arg0, GlTimeRange arg1, int branch) {
		logger.info(" EclipseImpFactory.class , super.LoadAll(arg0, arg1, branch); no imp");	
		GetAll(arg0,arg1,branch);
		
	}

	@Override
	protected void LoadAll(FwEclipseImpVector arg0, int branch) {
		logger.info(" EclipseImpFactory.class , super.LoadAll(arg0, branch); no imp");
		super.LoadAll(arg0, branch);
		
	}

	@Override
	protected void LoadAllRevs(FwEclipseImpVector arg0, int rev) {
		logger.info(" EclipseImpFactory.class , super.LoadAllRevs(arg0, rev); no imp");	
		super.LoadAllRevs(arg0, rev);
		
	}

	@Override
	protected void LoadAllSats(FwEclipseImpVector arg0, int satid) {
		logger.info(" EclipseImpFactory.class , super.LoadAllSats(arg0, satid); no imp");
		super.LoadAllSats(arg0, satid);
		
	}

	@Override
	protected void LoadAllTypes(FwEclipseImpVector arg0, Type type) {
		logger.info(" EclipseImpFactory.class , super.LoadAllTypes(arg0, type); no imp");
		super.LoadAllTypes(arg0, type);
		
	}

	@Override
	protected FwEclipseImp LoadOne(int sat, int rev, Type type, int branch) {
		logger.info(" EclipseImpFactory.class , return super.LoadOne(sat, rev, type, branch); no imp");
		return super.LoadOne(sat, rev, type, branch);
		
	}

	@Override
	protected boolean CheckSatelliteInDatabase(int satelliteID) {
		logger.info(" EclipseImpFactory.class , return super.CheckSatelliteInDatabase(satelliteID); no imp");
		return true;
		
	}

	@Override
	public FwEclipseImp GetID(int arg0) {
		logger.info(" EclipseImpFactory.class , return super.GetID(arg0); no imp");	
		return super.GetID(arg0);
		
	}

	@Override
	public void GetAll(FwEclipseImpVector arg0) {
		logger.info(" EclipseImpFactory.class , super.GetAll(arg0); no imp");			
	}

	@Override
	public void Delete(FwEclipseImpVector arg0) {
		logger.info(" EclipseImpFactory.class , super.Delete(arg0); no imp");					
	}

	@Override
	public void Delete(FwEclipseImp arg0) {
		logger.info(" EclipseImpFactory.class , super.Delete(arg0); no imp");	
	}

	@Override
	public boolean Store(FwEclipseImpVector vec) {
		System.out.println(" EclipseImpFactory.class , return super.Store(vec); no imp,vec size="+vec.size());
		logger.info(" EclipseImpFactory.class , super.delete(); no imp");	
		return true;		
	}

	@Override
	public boolean Store(FwEclipseImpVector savem, FwEclipseImpVector deleteem) {
		logger.info(" EclipseImpFactory.class , return super.Store(savem, deleteem); no imp");
		return true;
		
	}

	@Override
	public boolean Store(FwEclipseImp imp) {
		logger.info(" EclipseImpFactory.class , return super.Store(imp); no imp");	
		return true;		
	}

	@Override
	public boolean Store() {
		System.out.println(" EclipseImpFactory.class , return super.Store(); no imp");
		logger.info(" EclipseImpFactory.class , super.delete(); no imp");	
		return true;		
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" EclipseImpFactory.class , return super.DeleteAll(); no imp");	
		return true;
		
	}

	@Override
	public void CancelAll() {
		logger.info(" EclipseImpFactory.class , super.CancelAll(); no imp");				
	}

	@Override
	public void Cancel(FwEclipseImp arg0) {
		logger.info(" EclipseImpFactory.class , super.Cancel(arg0); no imp");					
	}

	@Override
	public void AddID(FwEclipseImp arg0) {
		logger.info(" EclipseImpFactory.class , super.AddID(arg0); no imp");				
	}

	@Override
	public void Cleanup() {
		logger.info(" EclipseImpFactory.class , super.Cleanup(); no imp");					
	}

	@Override
	protected void DoGetAll(FwEclipseImpVector arg0) {
		logger.info(" EclipseImpFactory.class , super.DoGetAll(arg0); no imp");
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" EclipseImpFactory.class , super.GetAll(arg0); no imp");				
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" EclipseImpFactory.class , super.GetAll(arg0, arg1); no imp");	
			
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" EclipseImpFactory.class , return super.Delete(arg0); no imp");
		return true;
		
	}
	
	public  List<Eclipse> getEclipseByTime(String starttime,String endtime , int branch) {		
		return  eclipseDao.getByBranchNTimerange(branch, starttime, endtime);			
	}

}
