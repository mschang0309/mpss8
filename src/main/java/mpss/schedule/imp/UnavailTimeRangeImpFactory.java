package mpss.schedule.imp;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.UnavailableTimeRangeDao;
import mpss.common.jpa.Unavailtimerange;
import mpss.schedule.JPAInit;
import mpss.schedule.Schedule;
import mpss.schedule.UIDB;
import mpss.schedule.Schedule.SessionType;
import mpss.schedule.base.*;
import mpss.schedule.base.GlUnavailTime.Type;
import mpss.util.timeformat.DateUtil;

public class UnavailTimeRangeImpFactory extends FwUnavailTimeRangeImpFactory implements UIDB<Unavailtimerange>{
		
	private Map<Long, FwUnavailTimeRangeImp> instances = new HashMap<Long, FwUnavailTimeRangeImp>();
	private static UnavailableTimeRangeDao unavailabletimerangeDao=JPAInit.getBuilder().getUnavailableTimeRangeDao();
	private boolean isloadfalg=false;
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	
	public boolean create(Unavailtimerange object){
		if(unavailabletimerangeDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Unavailtimerange object){
		 return unavailabletimerangeDao.update(object);
	 }

	 public boolean delete(Unavailtimerange object){
		 return unavailabletimerangeDao.delete(object);	
	 }

	 public boolean doSql(String jpsql){
		 return unavailabletimerangeDao.doSql(jpsql);	
	 }
	 
	public void releaseFwImp(){
		this.instances.clear();
		unavailabletimerangeDao.reset();
		isloadfalg=false;
	}
	
	@Override
	public synchronized void delete() {
		System.out.println();
		logger.info(" UnavailTimeRangeImpFactory.class , super.delete(); no imp");
		
	}

	@Override
	public FwUnavailTimeRangeImp GetOne(Type type, int parentID,GlTimeRange unavailtr, int numdata, int bid) {
		logger.info(" UnavailTimeRangeImpFactory.class , return super.GetOne(type, parentID, unavailtr, numdata, bid); no imp");
		return null;
		
	}

	@Override
	public void DoGetAll(FwUnavailTimeRangeImpVector arg0, int branchID) {
		if(isloadfalg==false){
			if(Schedule.s_type== SessionType.BRANCH){
				branchID = Schedule.branch_type_id;
				if(branchID==0){
					logger.info("UnavailTimeRange getall branch=0");
					return;
				}else{

				}
			}
			List<Unavailtimerange> list = unavailabletimerangeDao.getUnavailableTimeRangeByBid(branchID);
			logger.info("call UnavailTimeRangeImpFactory.class , super.DoGetAll(arg0, branchID); branchID ="+branchID+",size="+list.size());
			Iterator<Unavailtimerange> item = list.iterator();
			while (item.hasNext()) {
				Unavailtimerange con = (Unavailtimerange) item.next();			
				FwUnavailTimeRangeImp fwrev = new FwUnavailTimeRangeImp(con.getId().intValue(),	getTypeByString(con.getUttype()),con.getParentid().intValue(),DateUtil.getGlTimeRange(con.getStarttime(), con.getEndtime()), con.getNumdata().intValue(),branchID,false);
				arg0.add(fwrev);
				this.instances.put(con.getId().longValue(), fwrev);
			}
			isloadfalg = true;
		}		
		
	}
	
	public Type getTypeByString(String type){		
		if(type.equals("FTYPE")){
			return GlUnavailTime.Type.FTYPE;
		}else if(type.equals("CHAN")){
			return GlUnavailTime.Type.CHAN;
		}else if(type.equals("CC")){
			return GlUnavailTime.Type.CC;
		}else if(type.equals("CREW")){
			return GlUnavailTime.Type.CREW;
		}else if(type.equals("DC")){
			return GlUnavailTime.Type.DC;
		}else if(type.equals("EP")){
			return GlUnavailTime.Type.EP;
		}else if(type.equals("REC")){
			return GlUnavailTime.Type.REC;
		}else if(type.equals("UPL")){
			return GlUnavailTime.Type.UPL;
		}else if(type.equals("ANT")){
			return GlUnavailTime.Type.ANT;
		}else if(type.equals("XMIT")){
			return GlUnavailTime.Type.XMIT;
		}else if(type.equals("LTYPE")){
			return GlUnavailTime.Type.LTYPE;
		}else if(type.equals("DP")){
			return GlUnavailTime.Type.DP;
		}else if(type.equals("SITE")){
			return GlUnavailTime.Type.SITE;
		}
		return GlUnavailTime.Type.FTYPE;
	}

	@Override
	public void DoGetAll(FwUnavailTimeRangeImpVector arg0, Type arg1, int rscID) {
		logger.info(" UnavailTimeRangeImpFactory.class , super.DoGetAll(arg0, arg1, rscID); no imp...");		
	}

	@Override
	public void DoGetAll(FwUnavailTimeRangeImpVector arg0, Type arg1,int rscID, int bid) {
		logger.info(" UnavailTimeRangeImpFactory.class , super.DoGetAll(arg0, arg1, rscID, bid); no imp");		
		
	}

	@Override
	public boolean CheckResourceDependency(Type arg0, int rscID) {
		logger.info(" UnavailTimeRangeImpFactory.class , return super.CheckResourceDependency(arg0, rscID); no imp");
		return true;
		
	}

	@Override
	protected void DoCancelAll() {
		logger.info(" UnavailTimeRangeImpFactory.class , super.DoCancelAll(); no imp");
		
	}

	@Override
	protected FwUnavailTimeRangeImp CreateOne(Type type, int parentID,GlTimeRange uat, int numdata, int bid) {
		logger.info(" UnavailTimeRangeImpFactory.class , return super.CreateOne(type, parentID, uat, numdata, bid); no imp");
		return null;
		
	}

	@Override
	protected void DoLoadAll() {
		logger.info(" UnavailTimeRangeImpFactory.class , super.DoLoadAll(); no imp");		
		
	}

	@Override
	protected void DoCleanup() {
		logger.info(" UnavailTimeRangeImpFactory.class , super.DoCleanup(); no imp");	
		
	}

	@Override
	protected boolean CheckResourceInDatabase(Type arg0, int rscID) {
		logger.info(" UnavailTimeRangeImpFactory.class , return super.CheckResourceInDatabase(arg0, rscID); no imp");
		return true;
		
	}

	@Override
	protected void LoadAll(FwUnavailTimeRangeImpVector arg0, int arg1) {	
		logger.info(" UnavailTimeRangeImpFactory.class , return super.LoadAll(arg0, arg1); no imp");
					
	}

	@Override
	protected void LoadAll(FwUnavailTimeRangeImpVector arg0, Type arg1,int rscID) {
		logger.info(" UnavailTimeRangeImpFactory.class , super.LoadAll(arg0, arg1, rscID); no imp");
		
	}

	@Override
	public FwUnavailTimeRangeImp GetID(int arg0) {
		logger.info(" UnavailTimeRangeImpFactory.class , return super.GetID(arg0); no imp");
		return null;
		
	}

	@Override
	public void GetAll(FwUnavailTimeRangeImpVector arg0) {
		logger.info(" UnavailTimeRangeImpFactory.class , super.GetAll(arg0); for type");
		List<Unavailtimerange> list = unavailabletimerangeDao.getAllUnavailableTimeRange();
		Iterator<Unavailtimerange> item = list.iterator();
		while (item.hasNext()) {
			Unavailtimerange con = (Unavailtimerange) item.next();			
			FwUnavailTimeRangeImp fwrev = new FwUnavailTimeRangeImp(con.getId().intValue(),
					GlUnavailTime.Type.SITE,con.getParentid().intValue(),DateUtil.getGlTimeRange(con.getStarttime(), con.getEndtime()), con.getNumdata().intValue(),con.getBranchid().intValue(),false);
			arg0.add(fwrev);
			this.instances.put(con.getId().longValue(), fwrev);
		}
		
	}

	@Override
	public void Delete(FwUnavailTimeRangeImpVector arg0) {
		logger.info(" UnavailTimeRangeImpFactory.class , super.Delete(arg0); no imp");		
	}

	@Override
	public void Delete(FwUnavailTimeRangeImp arg0) {
		logger.info(" UnavailTimeRangeImpFactory.class , super.Delete(arg0); no imp");				
	}

	@Override
	public boolean Store(FwUnavailTimeRangeImpVector vec) {
		logger.info(" UnavailTimeRangeImpFactory.class , return super.Store(vec); no imp");
		return true;
		
	}

	@Override
	public boolean Store(FwUnavailTimeRangeImpVector savem,	FwUnavailTimeRangeImpVector deleteem) {
		logger.info(" UnavailTimeRangeImpFactory.class , return super.Store(savem, deleteem); no imp");
		return true;		
	}

	@Override
	public boolean Store(FwUnavailTimeRangeImp imp) {
		logger.info(" UnavailTimeRangeImpFactory.class , return super.Store(imp); no imp");
		return true;		
	}

	@Override
	public boolean Store() {
		logger.info(" UnavailTimeRangeImpFactory.class , return super.Store(); no imp");
		return true;		
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" UnavailTimeRangeImpFactory.class , return super.DeleteAll(); no imp");
		return true;		
	}

	@Override
	public void CancelAll() {
		logger.info(" UnavailTimeRangeImpFactory.class , super.CancelAll(); no imp");
	}

	@Override
	public void Cancel(FwUnavailTimeRangeImp arg0) {
		logger.info(" UnavailTimeRangeImpFactory.class , super.Cancel(arg0); no imp");		
	}

	@Override
	public void AddID(FwUnavailTimeRangeImp arg0) {		
		logger.info(" UnavailTimeRangeImpFactory.class , super.AddID(arg0); no imp");
		
	}

	@Override
	public void Cleanup() {
		logger.info("UnavailTimeRangeImpFactory.class , super.Cleanup(); no imp");			
	}

	@Override
	protected void AddNew(FwUnavailTimeRangeImp arg0) {
		logger.info(" UnavailTimeRangeImpFactory.class , super.AddNew(arg0); no imp");			
	}

	@Override
	protected void AddDeleted(FwUnavailTimeRangeImp arg0) {
		logger.info(" UnavailTimeRangeImpFactory.class , super.AddDeleted(arg0); no imp");			
	}

	@Override
	protected void DoCancel(FwUnavailTimeRangeImp arg0) {
		logger.info(" UnavailTimeRangeImpFactory.class , super.DoCancel(arg0); no imp");		
	}

	@Override
	protected void DoGetAll(FwUnavailTimeRangeImpVector arg0) {
		logger.info(" UnavailTimeRangeImpFactory.class , super.DoGetAll(arg0); no imp");		
	}

	

	@Override
	protected void DoLoadAll(int arg0) {
		logger.info("FwUnavailTimeRangeImpFactory::GetAll.  Loading all for branch " +String.valueOf(arg0));		
	}

	
	public  List<Unavailtimerange> getUnavailtimerangeByTime(String starttime,String endtime , int branch) {		
		return  unavailabletimerangeDao.getUnavailtimerangeByTime(branch, starttime, endtime);			
	}
	

	
}
