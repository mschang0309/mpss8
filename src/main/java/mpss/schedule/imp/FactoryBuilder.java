package mpss.schedule.imp;
import mpss.common.dao.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component("factoryBuilder")
public class FactoryBuilder {
	
	@Autowired
	protected ImagingmodeDao imagingmodeDao;
	
	@Autowired
	protected ActivityDao activityDao;
	
	@Autowired
	protected SessionDao sessionDao;
	
	@Autowired
	protected ExtentDao extentDao;
	
	@Autowired
	protected ContactDao contactDao;
	
	@Autowired
	protected SatelliteDao satelliteDao;
	
	@Autowired
	protected SiteDao siteDao;
	
	@Autowired
	protected SessionsitexrefDao sessionsitexrefDao;
	
	@Autowired
	protected SessionsatxrefDao sessionsatxrefDao;
	
	@Autowired
	protected SessionsatarlxrefDao sessionsatarlxrefDao;
	
	@Autowired
	protected ArlDao arlDao;
	
	@Autowired
	protected GlobalparametersDao globalparametersDao;
	
	@Autowired
	protected ParametersetDao parametersetDao;
	
	@Autowired
	protected SitepsDao sitepsDao;
	
	@Autowired
	protected UnavailableTimeRangeDao unavailabletimerangeDao;
	
	@Autowired
	protected RevDao revDao;
	
	@Autowired
	protected RecorderDao recorderDao;
	
	@Autowired
	protected RecorderpDao recorderpDao;
	
	@Autowired
	protected AntennaDao antennaDao;
	
	@Autowired
	protected TransmitterDao transmitterDao;
	
	@Autowired
	protected SatellitepDao satellitepDao;
	
	@Autowired
	protected DutycycleDao dutycycleDao;
	
	@Autowired
	protected ManeuverrequestDao maneuverDao;
	
	@Autowired
	protected RsiimagingrequestDao rsiimagingDao;
	
	@Autowired
	protected SiteRsiActPrefDao sitersiactprefDao;
	
	@Autowired
	protected EclipseDao eclipseDao;
	
	@Autowired
	protected ExtentActivityTimerangeDao extentactivitytimerangeDao;
	
	@Autowired
	protected UnobscuredTimeDao unobscuredtimeDao;
	
	@Autowired
	protected MisccmdprocrequestDao misccmdprocrequestDao;
	
	@Autowired
	protected RtcmdprocrequestDao rtcmdprocrequestDao;
	
	@Autowired
	protected UplinkrequestDao uplinkrequestDao;
	
	@Autowired
	protected XcrossrequestDao xcrossrequestDao;
	
	@Autowired
	protected IsualprocrequestDao isualrequestDao;
	
	@Autowired
	protected AipprocrequestDao aiprequestDao;
	
	@Autowired
	protected ExternalactivityrequestinfoDao externalactivityDao;
	
	public FactoryBuilder() {
	}
	
	public ImagingmodeDao getImagingmodeDao() {
		return imagingmodeDao;
	}
	
	public ActivityDao getActivityDao() {
		return activityDao;
	}
	
	public SessionDao getSessionDao() {
		return sessionDao;
	}
	
	public ExtentDao getExtentDao() {
		return extentDao;
	}
	
	public ContactDao getContactDao() {
		return contactDao;
	}
	
	public SatelliteDao getSatelliteDao() {
		return satelliteDao;
	}
	
	public SiteDao getSiteDao() {
		return siteDao;
	}
	
	public SessionsitexrefDao getSessionsitexrefDao() {
		return sessionsitexrefDao;
	}
	
	public SessionsatxrefDao getSessionsatxrefDao() {
		return sessionsatxrefDao;
	}
	
	public SessionsatarlxrefDao getSessionsatarlxrefDao() {
		return sessionsatarlxrefDao;
	}
	
	public ArlDao getArlDao() {
		return arlDao;
	}
	
	public GlobalparametersDao getGlobalparametersDao() {
		return globalparametersDao;
	}
	
	public ParametersetDao getParameterSetDao() {
		return parametersetDao;
	}
	
	public SitepsDao getSitepsDao() {
		return sitepsDao;
	}
	
	public UnavailableTimeRangeDao getUnavailableTimeRangeDao() {
		return unavailabletimerangeDao;
	}
	
	public RevDao getRevDao() {
		return revDao;
	}
	
	public RecorderDao getRecorderDao() {
		return recorderDao;
	}
	
	public RecorderpDao getRecorderpDao() {
		return recorderpDao;
	}
	
	public AntennaDao getAntennaDao() {
		return antennaDao;
	}
	
	public TransmitterDao getTransmitterDao() {
		return transmitterDao;
	}
	
	public SatellitepDao getSatellitepDao() {
		return satellitepDao;
	}
	
	public DutycycleDao getDutycycleDao() {
		return dutycycleDao;
	}
	
	public ManeuverrequestDao getManeuverDao() {
		return this.maneuverDao;
	}
	
	public RsiimagingrequestDao getRsiImagingDao() {
		return this.rsiimagingDao;
	}
	
	public SiteRsiActPrefDao getSiteRsiActPrefDao() {
		return this.sitersiactprefDao;
	}
	
	public EclipseDao getEclipseDao() {
		return this.eclipseDao;
	}	
	
	
	public ExtentActivityTimerangeDao getExtentActivityTimerange(){
		return this.extentactivitytimerangeDao;
	}
	
	public UnobscuredTimeDao getUnobscuredTime(){
		return this.unobscuredtimeDao;
	}
	
	
	public MisccmdprocrequestDao getMisccmdprocrequest(){
		return this.misccmdprocrequestDao;
	}
	
	
	public RtcmdprocrequestDao getRtcmdprocrequest(){
		return this.rtcmdprocrequestDao;
	}
	
	
	public UplinkrequestDao getUplinkrequest(){
		return this.uplinkrequestDao;
	}
	
	
	public XcrossrequestDao getXcrossrequest(){
		return this.xcrossrequestDao;
	}
	  
	public IsualprocrequestDao getIsualrequest(){
		return this.isualrequestDao;
	}
	
	public AipprocrequestDao getAiprequest(){
		return this.aiprequestDao;
	}
	
	public ExternalactivityrequestinfoDao getExternalActivityrequest(){
		return this.externalactivityDao;
	}

	

}
