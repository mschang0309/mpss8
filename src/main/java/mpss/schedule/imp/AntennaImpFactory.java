package mpss.schedule.imp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.AntennaDao;
import mpss.common.jpa.Antenna;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwAntennaImp;
import mpss.schedule.base.FwAntennaImpFactory;
import mpss.schedule.base.FwAntennaImpVector;
import mpss.schedule.base.FwBaseImpVector;

public class AntennaImpFactory extends FwAntennaImpFactory implements UIDB<Antenna>{
	
	private Map<Long, FwAntennaImp> instances = new HashMap<Long, FwAntennaImp>();
	private static AntennaDao antennaDao =JPAInit.getBuilder().getAntennaDao();	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public boolean create(Antenna object){
		if(antennaDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Antenna object){
		 return antennaDao.update(object);
	 }

	 public boolean delete(Antenna object){
		return antennaDao.delete(object);	
	 } 
	 
	 public boolean doSql(String jpsql){
		 return antennaDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		instances.clear();
		antennaDao.reset();
	}

	@Override
	public FwAntennaImp GetOne(int parentID, String name, String type) {
		logger.info(" AntennaImpFactory.class , return super.GetOne(parentID, name, type); no imp");
		return super.GetOne(parentID, name, type);
		
	}

	@Override
	public FwAntennaImp GetID(int arg0) {
		logger.info(" AntennaImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);
		
	}

	@Override
	public void GetAll(FwAntennaImpVector arg0, int parentID) {		
		List<Antenna> list = antennaDao.listAntennas(parentID);
		Iterator<Antenna> item = list.iterator();
		while (item.hasNext()) {
			Antenna con = (Antenna) item.next();
			FwAntennaImp fwrev = new FwAntennaImp(con.getId().intValue(),
					parentID, con.getName(), con.getAnttype(),false);
			arg0.add(fwrev);
			
			this.instances.put(con.getId().longValue(), fwrev);
		}
		
	}

	@Override
	public void Delete(FwAntennaImp arg0) {
		logger.info(" AntennaImpFactory.class , super.Delete(arg0); no imp");			
	}

	@Override
	public void Delete(FwAntennaImpVector arg0) {
		logger.info(" AntennaImpFactory.class , super.Delete(arg0); no imp");		
	}

	@Override
	public synchronized void delete() {
		logger.info(" AntennaImpFactory.class , super.delete(); no imp");
	}

	@Override
	protected FwAntennaImp CreateOne(int parentID, String name, String type) {
		logger.info(" AntennaImpFactory.class , return super.CreateOne(parentID, name, type); no imp");
		return super.CreateOne(parentID, name, type);		
	}

	@Override
	public boolean Store(FwAntennaImpVector imps, FwAntennaImpVector delimps) {
		logger.info(" AntennaImpFactory.class , return super.Store(imps, delimps); no imp");
		return true;
	}

	@Override
	public boolean Store(FwAntennaImp imp) {
		logger.info(" AntennaImpFactory.class , return super.Store(imp); no imp");
		return true;		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" AntennaImpFactory.class , super.GetAll(arg0); no imp");
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" AntennaImpFactory.class , super.GetAll(arg0, arg1); no imp");		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" AntennaImpFactory.class , return super.Delete(arg0); no imp");
		return true;		
	}

	@Override
	public void CancelAll() {
		logger.info(" AntennaImpFactory.class , super.CancelAll(); no imp");		
	}	
}
