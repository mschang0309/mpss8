package mpss.schedule.imp;

import java.util.HashMap;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.ParametersetDao;
import mpss.schedule.JPAInit;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwParametersPSImp;
import mpss.schedule.base.FwParametersPSImpFactory;
import mpss.schedule.base.FwParametersPSImpVector;
import mpss.schedule.base.GlSecondsVector;

public class ParamSetImpFactory extends FwParametersPSImpFactory {
	
	@SuppressWarnings("unused")
	private HashMap<Long, FwParametersPSImp> instances = new HashMap<Long, FwParametersPSImp>();

	@SuppressWarnings("unused")
	private static ParametersetDao parametersetDao=JPAInit.getBuilder().getParameterSetDao();
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public ParamSetImpFactory() {
		super();
		
	}

	@Override
	public FwParametersPSImp GetOne(int ParamSetID, int ParametersID, long EPF,
			long CPL, int AlignPlaybacks, long MinimumPassLength,
			double MinColdMaxEl, long MinColdDuration,
			GlSecondsVector passSetup, GlSecondsVector passCleanup,
			long AdjacentSDSGap) {
		logger.info(" ParamSetImpFactory.class , return super.GetOne(ParamSetID, ParametersID, EPF, CPL, AlignPlaybacks,	MinimumPassLength, MinColdMaxEl, MinColdDuration, passSetup,passCleanup, AdjacentSDSGap); no imp");
		return super.GetOne(ParamSetID, ParametersID, EPF, CPL, AlignPlaybacks,
				MinimumPassLength, MinColdMaxEl, MinColdDuration, passSetup,
				passCleanup, AdjacentSDSGap);
		
	}

	@Override
	protected FwParametersPSImp CreateOne(int ParamSetID, int Parameters,
			long EPF, long CPL, int AlignPlaybacks, long MinimumPassLength,
			double MinColdMaxEl, long MinColdDuration,
			GlSecondsVector passSetup, GlSecondsVector passCleanup,
			long AdjacentSDSGap) {
		logger.info(" ParamSetImpFactory.class , return super.CreateOne(ParamSetID, Parameters, EPF, CPL, AlignPlaybacks,MinimumPassLength, MinColdMaxEl, MinColdDuration, passSetup,passCleanup, AdjacentSDSGap); no imp");
		return super.CreateOne(ParamSetID, Parameters, EPF, CPL, AlignPlaybacks,
				MinimumPassLength, MinColdMaxEl, MinColdDuration, passSetup,
				passCleanup, AdjacentSDSGap);
		
	}

	@Override
	public FwParametersPSImp GetID(int arg0) {
		logger.info(" ParamSetImpFactory.class , return super.GetID(arg0); no imp");		
		return super.GetID(arg0);
		
	}

	@Override
	public void GetAll(FwParametersPSImpVector arg0, int parentID) {
		logger.info(" ParamSetImpFactory.class , super.GetAll(arg0, parentID); no imp");		
		super.GetAll(arg0, parentID);
		
	}

	@Override
	public boolean Store(FwParametersPSImpVector imps,FwParametersPSImpVector delimps) {
		logger.info(" ParamSetImpFactory.class , return super.Store(imps, delimps); no imp");		
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwParametersPSImp imp) {
		logger.info(" ParamSetImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public void Delete(FwParametersPSImp arg0) {
		logger.info(" ParamSetImpFactory.class , super.Delete(arg0); no imp");		
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwParametersPSImpVector arg0) {
		logger.info(" ParamSetImpFactory.class , super.Delete(arg0); no imp");	
		super.Delete(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" ParamSetImpFactory.class , super.GetAll(arg0); no imp");		
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" ParamSetImpFactory.class , super.GetAll(arg0, arg1); no imp");		
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" ParamSetImpFactory.class , return super.Delete(arg0); no imp");		
		return super.Delete(arg0);
		
	}

	@Override
	public void CancelAll() {
		logger.info(" ParamSetImpFactory.class , super.CancelAll(); no imp");	
		super.CancelAll();
		
	}

	

	

}
