package mpss.schedule.imp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.RecorderDao;
import mpss.common.jpa.Recorder;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.*;


public class RecorderImpFactory extends FwRecorderImpFactory  implements UIDB<Recorder> {

	private Map<Long, FwRecorderImp> instances = new HashMap<Long, FwRecorderImp>();
	private static RecorderDao recorderDao=JPAInit.getBuilder().getRecorderDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());
		
	public boolean create(Recorder object){
		if(recorderDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Recorder object){
		 return recorderDao.update(object);
	 }

	 public boolean delete(Recorder object){
		 return recorderDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return  recorderDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		this.instances.clear();
		recorderDao.reset();
	}

	@Override
	public synchronized void delete() {
		logger.info(" RecorderImpFactory.class , super.delete(); no imp");
		super.delete();

	}

	@Override
	public FwRecorderImp GetOne(int parentID, String name, int SectorSize,int TotalFiles, int MaxRsiFiles, int MaxIsualFiles, int MaxDdtFiles) {
		logger.info(" RecorderImpFactory.class , return super.GetOne(...); no imp");
		return super.GetOne(parentID, name, SectorSize, TotalFiles,	MaxRsiFiles, MaxIsualFiles, MaxDdtFiles);

	}

	@Override
	protected FwRecorderImp CreateOne(int parentID, String name,int SectorSize, int TotalFiles, int MaxRsiFiles, int MaxIsualFiles,	int MaxDdtFiles) {
		logger.info(" RecorderImpFactory.class , return super.CreateOne(...); no imp");
		return super.CreateOne(parentID, name, SectorSize, TotalFiles,
				MaxRsiFiles, MaxIsualFiles, MaxDdtFiles);

	}

	@Override
	public FwRecorderImp GetID(int arg0) {
		logger.info(" RecorderImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);

	}

	@Override
	public void GetAll(FwRecorderImpVector arg0, int parentID) {		
		List<Recorder> list = recorderDao.listRecorders(parentID);
		Iterator<Recorder> item = list.iterator();
		while (item.hasNext()) {
			Recorder con = (Recorder) item.next();
			FwRecorderImp fwrev = new FwRecorderImp(con.getId().intValue(),
					parentID, con.getName(), con.getSectorsize().intValue(),
					con.getTotalfiles().intValue(), con.getMaxrsifiles()
							.intValue(), con.getMaxisualfiles().intValue(), con
							.getMaxddtfiles().intValue());
			fwrev.SetMaxUserFiles(con.getUserfiles());
			arg0.add(fwrev);
			this.instances.put(con.getId().longValue(), fwrev);
		}

	}

	@Override
	public boolean Store(FwRecorderImpVector imps, FwRecorderImpVector delimps) {
		logger.info(" RecorderImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwRecorderImp imp) {
		logger.info(" RecorderImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public void Delete(FwRecorderImp arg0) {
		logger.info(" RecorderImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);

	}

	@Override
	public void Delete(FwRecorderImpVector arg0) {
		logger.info(" RecorderImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);

	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" RecorderImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);

	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" RecorderImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);

	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" RecorderImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);

	}

	@Override
	public void CancelAll() {
		logger.info(" RecorderImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();

	}

}
