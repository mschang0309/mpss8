package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.XcrossrequestDao;
import mpss.common.jpa.Xcrossrequest;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseARLEntryImp;
import mpss.schedule.base.FwBaseARLEntryImpVector;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwXCrossARLEntryImp;
import mpss.schedule.base.FwXCrossARLEntryImpFactory;
import mpss.schedule.base.GlDaySpec;

public class XcrossProcImpFactory extends FwXCrossARLEntryImpFactory implements	UIDB<Xcrossrequest> {

	private static XcrossrequestDao xcrossprocDao = JPAInit.getBuilder().getXcrossrequest();
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	public boolean create(Xcrossrequest object) {
		if (xcrossprocDao.create(object) == null) {
			return false;
		} else {
			return true;
		}
	}

	public Long createGetId(Xcrossrequest object) {
		Xcrossrequest data = xcrossprocDao.create(object);
		return Long.valueOf(data.getId().longValue());
	}

	public boolean update(Xcrossrequest object) {
		return xcrossprocDao.update(object);
	}

	public boolean delete(Xcrossrequest object) {
		return xcrossprocDao.delete(object);
	}

	public boolean doSql(String jpsql) {
		return xcrossprocDao.doSql(jpsql);
	}

	public void releaseFwImp() {
		xcrossprocDao.reset();
	}

	public Xcrossrequest getXcrossrequestByID(Long id) {
		try {
			return xcrossprocDao.getXcrossById(id.intValue());
		} catch (Exception ex) {
			logger.info("XcrossProcImpFactory getXcrossrequestByID ex="	+ ex.getMessage());
			return null;
		}
	}

	@Override
	public synchronized void delete() {
		logger.info(" XcrossProcImpFactory.class , super.delete(); no imp");
		super.delete();

	}

	@Override
	public FwXCrossARLEntryImp GetOne(int parentID, GlDaySpec date,long StartTime, long Duration, String station, String elev,String azim, int orbit, String Comments) {
		logger.info(" XcrossProcImpFactory.class , return super.GetOne(parentID, date, StartTime, Duration, station, elev, azim,	orbit, Comments); no imp");
		return super.GetOne(parentID, date, StartTime, Duration, station, elev,azim, orbit, Comments);

	}

	@Override
	protected FwXCrossARLEntryImp CreateOne(int parent, GlDaySpec date,long StartTime, long Duration, String station, String elev,String azim, int orbit, String Comments) {
		logger.info(" XcrossProcImpFactory.class , return super.CreateOne(parent, date, StartTime, Duration, station, elev, azim,orbit, Comments); no imp");
		return super.CreateOne(parent, date, StartTime, Duration, station,elev, azim, orbit, Comments);

	}

	@Override
	public FwBaseARLEntryImp GetID(int arg0) {
		logger.info(" XcrossProcImpFactory.class , return super.GetID(arg0); no imp");		
		return super.GetID(arg0);

	}

	@Override
	public void GetAll(FwBaseARLEntryImpVector arg0, int parentID) {
		logger.info(" XcrossProcImpFactory.class , super.GetAll(arg0, parentID); no imp");		
		super.GetAll(arg0, parentID);

	}

	@Override
	public void Delete(FwBaseARLEntryImp arg0) {
		logger.info(" XcrossProcImpFactory.class , super.Delete(arg0); no imp");	
		super.Delete(arg0);

	}

	@Override
	public void Delete(FwBaseARLEntryImpVector arg0) {
		logger.info(" XcrossProcImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);

	}

	@Override
	public void CancelAll() {
		logger.info(" XcrossProcImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();

	}

	@Override
	public boolean Store(FwBaseARLEntryImpVector imps,FwBaseARLEntryImpVector delimps) {
		logger.info(" XcrossProcImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);

	}

	@Override
	public boolean Store(FwBaseARLEntryImp imp) {
		logger.info(" XcrossProcImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);

	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" XcrossProcImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);

	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" XcrossProcImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);

	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" XcrossProcImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);

	}

	public HashMap<Long, Long> copyArl(int arlid, int new_arlid) {
		HashMap<Long, Long> xcrossmaps = new HashMap<Long, Long>();
		List<Xcrossrequest> lists = xcrossprocDao.getByArlId(arlid);
		for (Xcrossrequest info : lists) {
			Long id = Long.valueOf(info.getId().longValue());
			info.setId(null);
			info.setArlid(new_arlid);
			xcrossmaps.put(id, createGetId(info));
		}
		return xcrossmaps;
	}

}
