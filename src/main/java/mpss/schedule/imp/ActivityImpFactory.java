package mpss.schedule.imp;

import java.sql.Timestamp;
import java.util.*;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.*;
import mpss.schedule.JPAInit;
import mpss.schedule.Schedule;
import mpss.schedule.UIDB;
import mpss.schedule.Schedule.SchType;
import mpss.schedule.Schedule.SessionType;
import mpss.schedule.base.FwActivityImp;
import mpss.schedule.base.FwActivityImpFactory;
import mpss.schedule.base.FwActivityImpVector;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwTimeRangeTracker;
import mpss.schedule.base.GlARL;
import mpss.schedule.base.GlTime;
import mpss.schedule.base.GlTimeRange;
import mpss.schedule.base.GlTimeRangeVector;
import mpss.schedule.base.GlARL.SubType;
import mpss.schedule.base.GlARL.ToyType;
import mpss.schedule.base.GlARL.Type;
import mpss.common.jpa.*;
import mpss.util.timeformat.DateUtil;

public class ActivityImpFactory extends FwActivityImpFactory implements UIDB<Activity>{

	public static Map<Long, FwActivityImp> instances = new HashMap<Long, FwActivityImp>();
	FwTimeRangeTracker ourTRTracker = new FwTimeRangeTracker();	
	private static ActivityDao activityDao=JPAInit.getBuilder().getActivityDao();	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public boolean create(Activity object){
		if(activityDao.create(object)==null){
			return false;
		}else{
			return true;
		}		
	 }
	
	public Long createGetId(Activity object){
		Activity data  =activityDao.create(object);		
		return data.getId();		
	 }

	 public boolean update(Activity object){
		return activityDao.update(object);
	 }

	 public boolean delete(Activity object){
		return  activityDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return activityDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		instances.clear();
		activityDao.reset();
		ourTRTracker = null;
		ourTRTracker = new FwTimeRangeTracker();
	}
	
	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" ActivityImpFactory.class , super.GetAll(arg0); no imp");
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" ActivityImpFactory.class , super.GetAll(arg0, arg1); no imp");			
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" ActivityImpFactory.class , return super.Delete(arg0); no imp");
		return true;		
	}
	
	@Override
	public synchronized void delete() {
		logger.info(" ActivityImpFactory.class , super.delete(); no imp");	
	}

	@Override
	public boolean CheckSatelliteDependency(int satelliteID) {
		logger.info(" ActivityImpFactory.class , return super.CheckSatelliteDependency(satelliteID); no imp");
		return true;		
	}

	@Override
	public boolean CheckSessionDependency(int sessionID) {
		logger.info(" ActivityImpFactory.class , return super.CheckSessionDependency(sessionID); no imp");
		return true;		
	}

	@Override
	public void GetAll(FwActivityImpVector arg0) {
		logger.info(" ActivityImpFactory.class , super.GetAll(arg0); no imp");	
	}

	@Override
	protected FwActivityImp CreateOne(long duration, GlTimeRange timeWindow,
			int satID, int sessID, int extentID, boolean scheduled,
			Type arlType, int eid, String name, SubType subType,
			ToyType toyType, String info, int prevID, int nextID, boolean valid) {
		logger.info(" ActivityImpFactory.class , return super.CreateOne(...); no imp");
		return null;		
	}

	@Override
	protected void LoadAll(FwActivityImpVector arg0, int sessionID) {
		logger.info(" ActivityImpFactory.class , super.LoadAll(arg0, sessionID); no imp");			
	}

	@Override
	protected void LoadAll(FwActivityImpVector arg0, int entryID, Type entryType) {
		logger.info(" ActivityImpFactory.class , super.LoadAll(arg0, entryID, entryType); no imp");				
	}

	@Override
	protected void LoadUnscheduled(FwActivityImpVector arg0, GlTimeRange tr,int sessID) {
		logger.info(" ActivityImpFactory.class , super.LoadUnscheduled(arg0, tr, sessID); no imp");
	}

	@Override
	protected void AddOutside(FwActivityImpVector arg0, GlTimeRange arg1,
			FwActivityImp arg2) {
		logger.info(" ActivityImpFactory.class , super.AddOutside(arg0, arg1, arg2); no imp");	
	}

	@Override
	protected void DoCleanup() {
		logger.info(" ActivityImpFactory.class , super.DoCleanup(); no imp");	
	}

	@Override
	protected boolean CheckSatelliteInDatabase(int satelliteID) {
		logger.info(" ActivityImpFactory.class , return super.CheckSatelliteInDatabase(satelliteID); no imp");
		return true;		
	}

	@Override
	protected boolean CheckSessionInDatabase(int sessionID) {
		logger.info(" ActivityImpFactory.class , return super.CheckSessionInDatabase(sessionID); no imp");
		return true;		
	}

	@Override
	protected void DoCancelAll() {
		logger.info(" ActivityImpFactory.class , super.DoCancelAll(); no imp");	
	}

	@Override
	protected void AddNew(FwActivityImp arg0) {
		logger.info(" ActivityImpFactory.class , super.AddNew(arg0); no imp");	
	}

	@Override
	public void Delete(FwActivityImpVector arg0) {
		logger.info(" ActivityImpFactory.class , super.Delete(arg0); no imp,size="+arg0.size());	
	}

	@Override
	public void Delete(FwActivityImp arg0) {
		
//		System.out.println("call ActivityImpFactory.class , super.Delete(arg0); activity id ="+arg0.GetID());
//		if(Schedule.type==SchType.HOT){
//			Schedule.hotrun2 = Schedule.hotrun2+1;
//		}
//		try{
//			
//			doSql("delete from activity where id="+arg0.GetID());
//			
//		}catch(Exception ex){
//			System.out.println("ActivityImpFactory Delete ex= "+ex.getMessage());
//		}
		
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" ActivityImpFactory.class , return super.DeleteAll(); no imp");	
		return true;
	}

	@Override
	public void CancelAll() {
		logger.info(" ActivityImpFactory.class , super.CancelAll(); no imp");	
	}

	@Override
	public void Cancel(FwActivityImp arg0) {
		logger.info(" ActivityImpFactory.class , super.Cancel(arg0); no imp");	
	}

	@Override
	public void AddID(FwActivityImp arg0) {
		logger.info(" ActivityImpFactory.class , super.AddID(arg0); no imp");	
	}

	@Override
	public void Cleanup() {
		logger.info(" ActivityImpFactory.class , super.Cleanup(); no imp");	
	}

	@Override
	protected void AddDeleted(FwActivityImp arg0) {
		logger.info(" ActivityImpFactory.class , super.AddDeleted(arg0); no imp");
	}

	@Override
	protected void DoCancel(FwActivityImp arg0) {
		logger.info(" ActivityImpFactory.class , super.DoCancel(arg0); no imp");
	}
	


	@Override
	public FwActivityImp GetOne(long duration, GlTimeRange timeWindow, int satID, int sessID, int extentID, boolean scheduled, Type arlType, int eid, String name, SubType subType, ToyType toyType,
			String info, int prevID, int nextID, boolean valid) {
		long newid = activityDao.getNextId();
		FwActivityImp imp = new FwActivityImp((int) newid, duration, timeWindow, satID, sessID, extentID, scheduled, arlType, eid, name, subType, toyType, false, info, prevID, nextID, valid, true);
		
		ActivityImpFactory.instances.put(newid, imp);		
		return imp;

	}

	@Override
	public void GetAll(FwActivityImpVector arg0, int sessionID) {
		try{
			if(Schedule.type==SchType.HOT){	
				Collection<Activity> col =activityDao.getBySessionId(sessionID,false);
				System.out.println("activity size="+col.size());
				Schedule.hotrun1 = col.size();
				this.AddFwActivityImpVector(arg0,col );
			}else if(Schedule.type==SchType.COLD){
				this.AddFwActivityImpVector(arg0, activityDao.getBySessionId(sessionID));
			}else if(Schedule.type==SchType.RESCH){
				this.AddFwActivityImpVector(arg0, activityDao.getBySessionId(sessionID));
			}else if(Schedule.type==SchType.RESCH_FILTER){
				this.AddFwActivityImpVector(arg0, activityDao.getBySessionId(sessionID));
			}else{				
				logger.info("ActivityImpFactory GetAll() switch case no imp data");
			}
						
		}catch(Exception ex){
			logger.info("ActivityImpFactory GetAll() 1 ex");
		}				

	}

	
	@Override
	public void DoGetAll(FwActivityImpVector arg0) {
		List<Activity> list = activityDao.getAll();
		for (Activity act : list){
			FwActivityImp imp = new FwActivityImp();
			long lid = act.getId();
			int id = (int) lid;
			
			if(ActivityImpFactory.instances.containsKey(Long.valueOf(lid))){
				arg0.add(ActivityImpFactory.instances.get(Long.valueOf(lid)));
			}else{
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date(act.getStarttime().getTime()));			
				long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) , cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
				cal.setTime(new Date(act.getEndtime().getTime()));			
				long sec2 =  DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) , cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
				
				imp.SetID(id);
				imp.SetARLEntryID(act.getArlentryid());
				this.SetMyARLEntryType(imp, act.getArlentrytype());
				this.SetMySubType(imp, act.getActsubtype());
				this.SetMyToyType(imp, act.getToytype());
				imp.SetDuration(act.getDuration());
				imp.SetExtent(act.getExtentid());
				imp.SetInfo(act.getInfo()==null? "" : act.getInfo());
				imp.SetName(act.getName());
				imp.SetNextActivity(act.getNextactid());
				imp.SetPreviousActivity(act.getPrevactid());
				imp.SetSatellite(act.getSatelliteid());
				imp.SetScheduled(act.getScheduled() == 1 ? true : false);
				imp.SetSession(act.getSessionid());
				imp.SetTimeWindow(new GlTimeRange(new GlTime(sec1), new GlTime(sec2)));
				imp.SetUserModified(act.getUsermodified() == 1 ? true : false);
				imp.SetValid(act.getValid() == 1 ? true : false);
				
				ActivityImpFactory.instances.put(lid, imp);				
				arg0.add(imp);				
			}			
		}
	}

	@Override
	public void GetAll(FwActivityImpVector arg0, int entryID, Type entryType) {
		try{
			String arlentrytype = null;
			if (entryType.equals(GlARL.Type.DEL)){
				arlentrytype = "DELETE";
			} else if (entryType.equals(GlARL.Type.EXTFILE)){
				arlentrytype = "EXTFILE";
			} else if (entryType.equals(GlARL.Type.FTYPE)) {
				arlentrytype = "FTYPE";
			} else if (entryType.equals(GlARL.Type.MON)) {
				arlentrytype = "MON";
			} else if (entryType.equals(GlARL.Type.UPLINK)) {
				arlentrytype = "UPLINK";
			} else if (entryType.equals(GlARL.Type.RTCMD)) {
				arlentrytype = "RTCMD";
			} else if (entryType.equals(GlARL.Type.MISCPROC)) {
				arlentrytype = "MISC";
			} else if (entryType.equals(GlARL.Type.RSI)) {
				arlentrytype = "RSI";
			} else if (entryType.equals(GlARL.Type.ISUAL)) {
				arlentrytype = "ISUAL";
			} else if (entryType.equals(GlARL.Type.ISUALPROC)) {
				arlentrytype = "ISUALPROC";
			} else if (entryType.equals(GlARL.Type.MANEUVER)) {
				arlentrytype = "MANEUVER";
			} else if (entryType.equals(GlARL.Type.XCROSS)) {
				arlentrytype = "XCROSS";
			}else if (entryType.equals(GlARL.Type.AIPPROC)) {
				arlentrytype = "AIPPROC";
			}else if (entryType.equals(GlARL.Type.AIPPBK)) {
				arlentrytype = "AIPPBK";
			} else if (entryType.equals(GlARL.Type.TOY)) {
				arlentrytype = "TOY";
			} else if (entryType.equals(GlARL.Type.LTYPE)) {
				arlentrytype = "LTYPE";
			}			

			this.AddFwActivityImpVector(arg0, activityDao.getByEntryIdNType(entryID, arlentrytype));
		}catch(Exception ex){
			logger.info("ActivityImpFactory GetAll() 3 ex");
		}
		
	}

	@Override
	public void GetAll(FwActivityImpVector arg0, GlTimeRange tr, int branchID) {
		try{
			
			logger.info("call ActivityImpFactory.GetAll() 3 branchid =" +branchID);	
			
			if(Schedule.s_type== SessionType.BRANCH){
				branchID = Schedule.branch_type_id;
				if(branchID==0){
					logger.info("activity getall branch=0");
					return;
				}else{
//					LoadAll(arg0, tr, branchID);
//					return;
				}
			}
			
			
			
			if(branchID==0){
				LoadAll(arg0, tr, branchID);
				return ;
			}
			
			
			GlTimeRangeVector needTRs=new GlTimeRangeVector();
		    //FwActivityImpVector localImps=new FwActivityImpVector();
		    if (ourTRTracker.NeedOtherTRs(branchID, tr, needTRs))	    {
		        for (int i = 0; i < needTRs.size(); i++)	        {            
		            LoadAll(arg0, needTRs.get(i), branchID);
		            	            
		            logger.info("FwActivityImpFactory::GetAll.  Loading("
			                + needTRs.get(i).AsString()+") #loaded: " + arg0.size());
		            ourTRTracker.UpdateTRs(branchID, needTRs.get(i));
		        }
		    }  
			// must do something by jeff view source code
		}catch(Exception ex){
			logger.info("ActivityImpFactory GetAll() 3 ex");
		}
		

	}
	
	@Override
	protected void LoadAll(FwActivityImpVector arg0, GlTimeRange arg1, int branchID) {
		if(Schedule.s_type== SessionType.BRANCH){
			branchID = Schedule.branch_type_id;
			if(branchID==0){
				logger.info("activity getall branch=0");
				return;
			}

		}
		String starttime = arg1.GetStartTime().asString("%Y-%m-%d %H:%M:%S");
		String endtime = arg1.GetEndTime().asString("%Y-%m-%d %H:%M:%S");
		this.AddFwActivityImpVector(arg0, activityDao.getBySessionIdNTimerange(branchID, starttime, endtime));
	}

	@Override
	public void GetAllLinkedOutside(FwActivityImpVector arg0, GlTimeRange arg1) {		
		logger.info("call ActivityImpFactory.GetAllLinkedOutside(.,.)");	
	}

	@Override
	public void GetUnscheduled(FwActivityImpVector arg0, GlTimeRange tr, int branchID) {
		if(Schedule.s_type== SessionType.BRANCH){
			branchID = Schedule.branch_type_id;
			if(branchID==0){
				logger.info("activity getall branch=0");
				return;
			}

		}
		logger.info("call ActivityImpFactory.GetUnscheduled(.,.,.)");
		String starttime = tr.GetStartTime().asString("%Y-%m-%d %H:%M:%S");
		String endtime = tr.GetEndTime().asString("%Y-%m-%d %H:%M:%S");			
		this.AddFwActivityImpVector(arg0, activityDao.getUnschBySessionIdNTimerange(branchID, starttime, endtime));
		this.SyncList(arg0);		
	}
	
	@Override
	public FwActivityImp GetID(int arg0) {
				
		if (instances.containsKey(Long.valueOf(arg0))) {
			return (FwActivityImp) instances.get(Long.valueOf(arg0));
		} else {
			
			Activity act = activityDao.getActivityById(Long.valueOf(arg0));
			FwActivityImp imp =new FwActivityImp();
			long lid = act.getId();
			int id = (int) lid;
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date(act.getStarttime().getTime()));			
			long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) , cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
			cal.setTime(new Date(act.getEndtime().getTime()));			
			long sec2 =  DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) , cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

			
			imp.SetID(id);
			imp.SetARLEntryID(act.getArlentryid());
			this.SetMyARLEntryType(imp, act.getArlentrytype());
			this.SetMySubType(imp, act.getActsubtype());
			this.SetMyToyType(imp, act.getToytype());
			imp.SetDuration(act.getDuration());
			imp.SetExtent(act.getExtentid());
			imp.SetInfo(act.getInfo()==null? "" : act.getInfo());
			imp.SetName(act.getName());
			imp.SetNextActivity(act.getNextactid());
			imp.SetPreviousActivity(act.getPrevactid());
			imp.SetSatellite(act.getSatelliteid());
			imp.SetScheduled(act.getScheduled() == 1 ? true : false);
			imp.SetSession(act.getSessionid());
			imp.SetTimeWindow(new GlTimeRange(new GlTime(sec1), new GlTime(sec2)));
			imp.SetUserModified(act.getUsermodified() == 1 ? true : false);
			imp.SetValid(act.getValid() == 1 ? true : false);			
			
			ActivityImpFactory.instances.put(lid, imp);
			return imp;
			
		}
		
	}
	
	public String getSubType(int arg0){		
			Activity act = activityDao.getActivityById(Long.valueOf(arg0));			
			return act.getActsubtype();			
	}	

	@Override
	public boolean Store(FwActivityImpVector arg0) {	
		try {				
			logger.info("ActivityImpFactory Store=======modify some,size="+arg0.size());
			int size = (int)arg0.size();
			
			if(Schedule.type==SchType.COLD){
				Schedule.coldrun1 =(int) size ;
			}
			
			for (int i = 0; i <  size; i++) {
				FwActivityImp imp = arg0.get(i);
				Activity act = new Activity();		
				
				String starttime = imp.GetTimeWindow().GetStartTime().asString("%Y-%m-%d %H:%M:%S");
				String endtime = imp.GetTimeWindow().GetEndTime().asString("%Y-%m-%d %H:%M:%S");
				Date dStart = DateUtil.stringToDate(starttime, "yyyy-MM-dd HH:mm:ss");
				Date dEnd = DateUtil.stringToDate(endtime, "yyyy-MM-dd HH:mm:ss");
				Timestamp tsStart = DateUtil.dateToTimestamp(dStart);
				Timestamp tsEnd =  DateUtil.dateToTimestamp(dEnd);
				
				
				act.setId(Long.valueOf(imp.GetID()));
				act.setArlentryid(imp.GetARLEntryID());				
				act.setArlentrytype(getARLEntryTypetoString(imp.GetARLEntryType()));
				act.setActsubtype(getSubTypetoString(imp.GetSubType()));
				act.setToytype(getToyTypetoString(imp.GetToyType()));						
				act.setDuration((int)imp.GetDuration());
				act.setExtentid(imp.GetExtent());
				act.setInfo(imp.GetInfo());
				act.setName(imp.GetName());
				act.setNextactid(imp.GetNextActivity());
				act.setPrevactid(imp.GetPreviousActivity());
				act.setSatelliteid(imp.GetSatellite());
				act.setScheduled(imp.GetScheduled() == true? 1 : 0);
				act.setSessionid(imp.GetSession());
				act.setStarttime(tsStart);
				act.setEndtime(tsEnd);
				act.setUsermodified(imp.GetUserModified() == true ? 1 : 0);
				act.setValid(imp.GetValid() == true ? 1 : 0);			
				
				if(Schedule.type==SchType.COLD){
					if (imp.IsChanged()) {
						activityDao.update(act);
					} else if (imp.IsNew()) {	
						if(activityDao.getActivityById(imp.GetID())==null){
							activityDao.create(act);
						}												
					}
				}else if(Schedule.type==SchType.HOT){					
					if(Schedule.updateDB==true){
						activityDao.update(act);
					}
					
				}else if(Schedule.type==SchType.RESCH){
					if(Schedule.updateDB==true){
						activityDao.update(act);
					}
				}else if(Schedule.type==SchType.ADJUST){
					if(Schedule.updateDB==true){
						activityDao.update(act);
					}
				}else if(Schedule.type==SchType.RESCH_FILTER){
					if(Schedule.updateDB==true){
						activityDao.update(act);
					}
				}
				
				
			}
		} catch (Exception ex) {
			logger.info("Store(FwActivityImpVector arg0) ex:" + ex.toString());
			return true;
		}

		return true;
	}

	@Override
	public boolean Store(FwActivityImpVector savem, FwActivityImpVector deleteem) {
		logger.info("activetyImpFactory store() 3 no imp");
		return true;
	}

	@Override
	public boolean Store(FwActivityImp arg0) {
		logger.info("activetyImpFactory store()"); 
        FwActivityImpVector vec = new FwActivityImpVector();
        vec.add(arg0);
        return Store(vec); 

	}

	@Override
	public boolean Store() {
		logger.info("activetyImpFactory store() 1 no imp");
		return true;
	}
	
	public String getARLEntryTypetoString(Type type){
		if (type==GlARL.Type.DEL) {
			return "DELETE";
		} else if (type==GlARL.Type.EXTFILE) {
			return "EXTFILE";
		} else if (type==GlARL.Type.FTYPE) {
			return "MON";
		} else if (type==GlARL.Type.MON) {
				return "MON";
		} else if (type==GlARL.Type.UPLINK) {
				return "UPLINK";
		} else if (type==GlARL.Type.RTCMD) {
				return "RTCMD";
		} else if (type==GlARL.Type.MISCPROC) {
				return "MISC";
		} else if (type==GlARL.Type.RSI) {
				return "RSI";
		} else if (type==GlARL.Type.ISUAL) {
				return "ISUAL";
		} else if (type==GlARL.Type.ISUALPROC) {
				return "ISUALPROC";
		} else if (type==GlARL.Type.MANEUVER) {
				return "MANEUVER";
		} else if (type==GlARL.Type.XCROSS) {
				return "XCROSS";
		}else if (type==GlARL.Type.AIPPROC) {
				return "AIPPROC";
	    }else if (type==GlARL.Type.AIPPBK) {
	    		return "AIPPBK";
        } else if (type==GlARL.Type.TOY) {
				return "TOY";
		} else if (type==GlARL.Type.LTYPE) {
				return "LTYPE";
		}
		return "";
		
	}
	
	
	public void SetMyARLEntryType(FwActivityImp imp, String type) {		
		if (type.equalsIgnoreCase("DELETE")) {
			imp.SetARLEntryType(GlARL.Type.DEL);
		} else if (type.equalsIgnoreCase("EXTFILE")) {
			imp.SetARLEntryType(GlARL.Type.EXTFILE);
		} else if (type.equalsIgnoreCase("FTYPE")) {
			imp.SetARLEntryType(GlARL.Type.FTYPE);
		} else if (type.equalsIgnoreCase("MON")) {
			imp.SetARLEntryType(GlARL.Type.MON);
		} else if (type.equalsIgnoreCase("UPLINK")) {
			imp.SetARLEntryType(GlARL.Type.UPLINK);
		} else if (type.equalsIgnoreCase("RTCMD")) {
			imp.SetARLEntryType(GlARL.Type.RTCMD);
		} else if (type.equalsIgnoreCase("MISC")) {
			imp.SetARLEntryType(GlARL.Type.MISCPROC);
		} else if (type.equalsIgnoreCase("RSI")) {
			imp.SetARLEntryType(GlARL.Type.RSI);
		} else if (type.equalsIgnoreCase("ISUAL")) {
			imp.SetARLEntryType(GlARL.Type.ISUAL);
		} else if (type.equalsIgnoreCase("ISUALPROC")) {
			imp.SetARLEntryType(GlARL.Type.ISUALPROC);
		} else if (type.equalsIgnoreCase("MANEUVER")) {
			imp.SetARLEntryType(GlARL.Type.MANEUVER);
		} else if (type.equalsIgnoreCase("XCROSS")) {
			imp.SetARLEntryType(GlARL.Type.XCROSS);
		}else if (type.equalsIgnoreCase("AIPPROC")) {
			imp.SetARLEntryType(GlARL.Type.AIPPROC);
		}else if (type.equalsIgnoreCase("AIPPBK")) {
			imp.SetARLEntryType(GlARL.Type.AIPPBK);
		} else if (type.equalsIgnoreCase("TOY")) {
			imp.SetARLEntryType(GlARL.Type.TOY);
		} else if (type.equalsIgnoreCase("LTYPE")) {
			imp.SetARLEntryType(GlARL.Type.LTYPE);
		}else{
			//imp.SetARLEntryType(GlARL.Type.DEL);
		}

	}

	public void SetMySubType(FwActivityImp imp, String type) {
		//System.out.println("call ActivityImpFactory.SetMySubType(.,.)"); //OK
		if (type.equalsIgnoreCase("FSUBTYPE")) {
			imp.SetSubType(GlARL.SubType.FSUBTYPE);
		} else if (type.equalsIgnoreCase("NO")) {
			imp.SetSubType(GlARL.SubType.NOSUBTYPE);
		} else if (type.equalsIgnoreCase("REC")) {
			imp.SetSubType(GlARL.SubType.RECORD);
		} else if (type.equalsIgnoreCase("PBK")) {
			imp.SetSubType(GlARL.SubType.PLAYBACK);
		} else if (type.equalsIgnoreCase("DDT")) {
			imp.SetSubType(GlARL.SubType.DDT);
		} else if (type.equalsIgnoreCase("POSTSHIP")) {
			imp.SetSubType(GlARL.SubType.POSTSHIP);
		} else if (type.equalsIgnoreCase("LSUBTYPE")) {
			imp.SetSubType(GlARL.SubType.LSUBTYPE);
		}

	}
	
	public String getSubTypetoString(SubType type){
		if (type==GlARL.SubType.FSUBTYPE) {
			//return "FSUBTYPE";
			return "NO";
		} else if (type==GlARL.SubType.NOSUBTYPE) {
			return "NO";
		} else if (type==GlARL.SubType.RECORD) {
			return "REC";
		} else if (type==GlARL.SubType.PLAYBACK) {
				return "PBK";
		} else if (type==GlARL.SubType.DDT) {
				return "DDT";
		} else if (type==GlARL.SubType.POSTSHIP) {
				return "POSTSHIP";
		} else if (type==GlARL.SubType.LSUBTYPE) {
				return "LSUBTYPE";
		} 
		return "";
		
	}


	public void SetMyToyType(FwActivityImp imp, String type) {
		//System.out.println("call ActivityImpFactory.SetMyToyType(.,.)"); //OK
		if (type.equalsIgnoreCase("FTOYTYPE")) {
			imp.SetToyType(GlARL.ToyType.FTOYTYPE);
		} else if (type.equalsIgnoreCase("NO")) {
			imp.SetToyType(GlARL.ToyType.NOTOY);
		} else if (type.equalsIgnoreCase("CONTACT")) {
			imp.SetToyType(GlARL.ToyType.CONTACT);
		} else if (type.equalsIgnoreCase("TREC")) {
			imp.SetToyType(GlARL.ToyType.TREC);
		} else if (type.equalsIgnoreCase("TPBK")) {
			imp.SetToyType(GlARL.ToyType.TPBK);
		} else if (type.equalsIgnoreCase("TPS")) {
			imp.SetToyType(GlARL.ToyType.TPS);
		} else if (type.equalsIgnoreCase("TRT")) {
			imp.SetToyType(GlARL.ToyType.TRT);
		} else if (type.equalsIgnoreCase("TMON")) {
			imp.SetToyType(GlARL.ToyType.TMON);
		} else if (type.equalsIgnoreCase("TUPLINK")) {
			imp.SetToyType(GlARL.ToyType.TUPLINK);
		} else if (type.equalsIgnoreCase("TRTCMD")) {
			imp.SetToyType(GlARL.ToyType.TRTCMD);
		} else if (type.equalsIgnoreCase("TMISC")) {
			imp.SetToyType(GlARL.ToyType.TMISC);
		} else if (type.equalsIgnoreCase("TTAPE")) {
			imp.SetToyType(GlARL.ToyType.TTAPE);
		} else if (type.equalsIgnoreCase("TCLK")) {
			imp.SetToyType(GlARL.ToyType.TCLK);
		}
	}
	
	public String getToyTypetoString(ToyType type) {
		
		if (type==GlARL.ToyType.FTOYTYPE) {
			//return "FTOYTYPE";
			return "NO";
		} else if (type==GlARL.ToyType.NOTOY) {
			return "NO";
		} else if (type==GlARL.ToyType.CONTACT) {
			return "CONTACT";
		} else if (type==GlARL.ToyType.TREC) {
			return "TREC";
		} else if (type==GlARL.ToyType.TPBK) {
			return "TPBK";
		} else if (type==GlARL.ToyType.TPS) {
			return "TPS";
		} else if (type==GlARL.ToyType.TRT) {
			return "TRT";
		} else if (type==GlARL.ToyType.TMON) {
			return "TMON";
		} else if (type==GlARL.ToyType.TUPLINK) {
			return "TUPLINK";
		} else if (type==GlARL.ToyType.TRTCMD) {
			return "TRTCMD";
		} else if (type==GlARL.ToyType.TMISC) {
			return "TMISC";
		} else if (type==GlARL.ToyType.TTAPE) {
			return "TTAPE";
		} else if (type==GlARL.ToyType.TCLK) {
			return "TCLK";
		}
		return "";
	}
	
	public void AddFwActivityImpVector(FwActivityImpVector arg0, Collection<Activity> col){
		logger.info("call ActivityImpFactory.AddFwActivityImpVector(.,.)");
		ArrayList<Activity> al = new ArrayList<Activity>(col);		

		for (int i = 0; i < al.size(); i++) {
			Activity act = (Activity) al.get(i);
			long lid = act.getId();
			int id = (int) lid;
			
			if(ActivityImpFactory.instances.containsKey(Long.valueOf(lid))){
				arg0.add(ActivityImpFactory.instances.get(Long.valueOf(lid)));
			}else{
				FwActivityImp imp = new FwActivityImp();
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date(act.getStarttime().getTime()));			
				long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) , cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
				cal.setTime(new Date(act.getEndtime().getTime()));			
				long sec2 =  DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) , cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

				imp.SetID(id);
				imp.SetARLEntryID(act.getArlentryid());
				this.SetMyARLEntryType(imp, act.getArlentrytype());
				this.SetMySubType(imp, act.getActsubtype());
				this.SetMyToyType(imp, act.getToytype());
				imp.SetDuration(act.getDuration());
				imp.SetExtent(act.getExtentid());
				imp.SetInfo(act.getInfo()==null? "" : act.getInfo());
				imp.SetName(act.getName());
				imp.SetNextActivity(act.getNextactid());
				imp.SetPreviousActivity(act.getPrevactid());
				imp.SetSatellite(act.getSatelliteid());
				imp.SetScheduled(act.getScheduled() == 1 ? true : false);
				imp.SetSession(act.getSessionid());
				imp.SetTimeWindow(new GlTimeRange(new GlTime(sec1), new GlTime(sec2)));
				imp.SetUserModified(act.getUsermodified() == 1 ? true : false);
				imp.SetValid(act.getValid() == 1 ? true : false);
				
				
				ActivityImpFactory.instances.put(lid, imp);
				arg0.add(imp);				
			}			
		}	
		logger.info("activity db size = " + ActivityImpFactory.instances.size());
	}
	
	public void DeleteNoXcross(int sessionid){
		try{
			Collection<Activity> cols =activityDao.getBySessionExtentSchType(sessionid, 0, 0, "XCROSS");
			for(Activity act : cols){		
				logger.info("delete XCROSS not in sch ,xcrossrequests id ="+String.valueOf(act.getArlentryid()));
				doSql("delete from xcrossrequests where id="+String.valueOf(act.getArlentryid()));
				Schedule.hotrun1= Schedule.hotrun1-1;
			}
			doSql("delete from activity where sessionid="+String.valueOf(sessionid) + " and extentid=0 and scheduled=0 and arlentrytype='XCROSS' ");				
			
		}catch(Exception ex){
			logger.info("delete ActivityImpFactory DeleteNoXcross ex="+ex.getMessage());
		}
		
	}
	
	public void DeleteNoXcross(Activity act){
		try{
						
			doSql("delete from xcrossrequests where id="+String.valueOf(act.getArlentryid()));
			Schedule.hotrun1= Schedule.hotrun1-1;			
			doSql("delete from activity where id='" +act.getId()+"'");
			doSql("delete from extent where activityid='" +act.getId()+"'");
			
		}catch(Exception ex){
			logger.info("delete ActivityImpFactory DeleteNoXcross by act ex="+ex.getMessage());
		}		
	}
	
	@SuppressWarnings("unused")
	public void DeleteMon(int sessionid){
		try{
			Collection<Activity> cols =activityDao.getBySessionExtentSchType(sessionid, 0, 0, "MON");
			for(Activity act : cols){
				Schedule.coldrun1= Schedule.coldrun1-1;
			}
			doSql("delete from activity where sessionid="+String.valueOf(sessionid) + " and extentid=0 and scheduled=0 and arlentrytype='MON' ");
			
		}catch(Exception ex){
			logger.info("delete ActivityImpFactory DeleteMon ex="+ex.getMessage());
		}
		
	}
	
	
	public int DeleteSchActivity(int activityid){
		try{
			Activity activity =activityDao.getActivityById(activityid);
			if(activity.getScheduled().intValue()==1){
				doSql("update activity set extentid=0 , scheduled=0  where  id="+String.valueOf(activityid));
				return 1;
			}
			
			return 0;
		}catch(Exception ex){
			logger.info("delete ActivityImpFactory DeleteSchActivity ex="+ex.getMessage());
			return 0;
		}
		
	}
	
	
	public Collection<Activity> getSchXcross(int sessionid,int scheduled){
		try{
			return activityDao.getBySessionSchType(sessionid,scheduled, "XCROSS");			
		}catch(Exception ex){
			logger.info("delete ActivityImpFactory DeleteNoXcross ex="+ex.getMessage());
			return new ArrayList<Activity>();
		}
		
	}
	
	public Collection<Activity> getSchAip(int sessionid,int scheduled){
		try{
			return activityDao.getBySessionSchType(sessionid,scheduled, "AIPPROC");			
		}catch(Exception ex){
			logger.info("ActivityImpFactory get sch aip  ex="+ex.getMessage());
			return new ArrayList<Activity>();
		}
		
	}
	
	
	public void AddFwActivityImpVector(FwActivityImpVector arg0, Collection<Activity> col,int sessionid){
		if(Schedule.s_type== SessionType.BRANCH){
			sessionid = Schedule.branch_type_id;
			if(sessionid==0){				
				logger.info("activity ========================== branch="+sessionid);
				return;
			}
		}
		logger.info("call ActivityImpFactory.AddFwActivityImpVector(.,.,.) size="+col.size());
		ArrayList<Activity> al = new ArrayList<Activity>(col);		

		for (int i = 0; i < al.size(); i++) {
			Activity act = (Activity) al.get(i);
			long lid = act.getId();
			int id = (int) lid;
			if(ActivityImpFactory.instances.containsKey(Long.valueOf(lid))){
				arg0.add(ActivityImpFactory.instances.get(Long.valueOf(lid)));
			}else{
				FwActivityImp imp = new FwActivityImp();
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date(act.getStarttime().getTime()));			
				long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) , cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
				cal.setTime(new Date(act.getEndtime().getTime()));			
				long sec2 =  DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) , cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

				
				imp.SetID(id);
				imp.SetARLEntryID(act.getArlentryid());
				this.SetMyARLEntryType(imp, act.getArlentrytype());
				this.SetMySubType(imp, act.getActsubtype());
				this.SetMyToyType(imp, act.getToytype());
				imp.SetDuration(act.getDuration());
				imp.SetExtent(act.getExtentid());
				imp.SetInfo(act.getInfo()==null? "" : act.getInfo());
				imp.SetName(act.getName());
				imp.SetNextActivity(act.getNextactid());
				imp.SetPreviousActivity(act.getPrevactid());
				imp.SetSatellite(act.getSatelliteid());
				imp.SetScheduled(act.getScheduled() == 1 ? true : false);
				imp.SetSession(sessionid);
				imp.SetTimeWindow(new GlTimeRange(new GlTime(sec1), new GlTime(sec2)));
				imp.SetUserModified(act.getUsermodified() == 1 ? true : false);
				imp.SetValid(act.getValid() == 1 ? true : false);
				
				ActivityImpFactory.instances.put(lid, imp);
				arg0.add(imp);
			}
			
		}		
	}

	
	
	public Collection<Activity> getSQL(Timestamp starttime, Timestamp endtime){
		try{
			return activityDao.getActivityByRange(starttime,endtime);			
		}catch(Exception ex){
			logger.info("delete ActivityImpFactory getSQL ex="+ex.getMessage());
			return new ArrayList<Activity>();
		}
	}
	
	public Collection<Activity> getSQL(int sessionid){
		try{
			return activityDao.getBySessionId(sessionid);			
		}catch(Exception ex){
			logger.info("delete ActivityImpFactory getSQL(int ) ex="+ex.getMessage());
			return new ArrayList<Activity>();
		}
	}
	
	public Activity getById(int activityid){
		try{
			return activityDao.getActivityById(activityid);			
		}catch(Exception ex){
			logger.info("ActivityImpFactory getById ex="+ex.getMessage());
			return null;
		}
	}
	
	
	public Collection<Activity> getNorSession(int sessionid,String endtime){
		Collection<Activity> acts = new ArrayList<Activity>();
		Collection<Activity> temp =activityDao.getBySessionIdEndtime(sessionid,endtime);
		for(Activity act : temp){
			acts.add(act);			
		}
		temp =activityDao.getMonActBySessionIdEndtime(sessionid,endtime);
		for(Activity act : temp){
			acts.add(act);			
		}		
		
		return acts;
		
	}
	
}
