package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.ParametersetDao;
import mpss.common.jpa.Parameterset;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwParamSetImp;
import mpss.schedule.base.FwParamSetImpFactory;
import mpss.schedule.base.FwParamSetImpVector;


public class ParameterSetImpFactory extends FwParamSetImpFactory implements UIDB<Parameterset>{
	
	private HashMap<Long, FwParamSetImp> instances = new HashMap<Long, FwParamSetImp>();
	private static ParametersetDao parametersetDao=JPAInit.getBuilder().getParameterSetDao();	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public boolean create(Parameterset object){
		if(parametersetDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Parameterset object){
		return  parametersetDao.update(object);
	 }

	 public boolean delete(Parameterset object){
		return parametersetDao.delete(object);	
	 } 
	 
	 public boolean doSql(String jpsql){
		 return parametersetDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		instances.clear();
		parametersetDao.reset();
	}

	public ParameterSetImpFactory() {		
		List<Parameterset> dList = parametersetDao.getAll();
		for (Parameterset ps : dList) {
			long lid = ps.getId();
			int id = (int) lid;

			boolean copied = ps.getHasbeencopied() == 0 ? false : true;
			FwParamSetImp ins = new FwParamSetImp(id, ps.getName(), ps.getComments() == null ? "" : ps.getComments(), ps.getFromps() == null ? "" : ps.getFromps(), copied, true);
			instances.put(lid, ins);

		}
	}

	@Override
	protected FwParamSetImp CreateOne(String Name, String Comment, String fromps) {
		logger.info(" ParameterSetImpFactory.class , return super.CreateOne(Name, Comment, fromps); no imp");
		return super.CreateOne(Name, Comment, fromps);
		
	}

	@Override
	protected void DoCleanup() {
		logger.info(" ParameterSetImpFactory.class , super.DoCleanup(); no imp");
		super.DoCleanup();
		
	}

	

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" ParameterSetImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" ParameterSetImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" ParameterSetImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}

	@Override
	public FwParamSetImp GetID(int arg0) {		
		FwParamSetImp imp= null;
		
		if (instances.containsKey(Long.valueOf(arg0))){
			imp = (FwParamSetImp)instances.get(Long.valueOf(arg0));
		} else {
			imp = new FwParamSetImp();
			Parameterset s = parametersetDao.getByID(arg0);
			
			long lid = s.getId();
			int id = (int) lid;
								
			imp.SetID(id);
			imp.SetName(s.getName()==null? "" :s.getName());
			imp.SetComment(s.getComments()==null? "" :s.getComments());
			imp.SetFromPS(s.getFromps()==null? "" :s.getFromps());
			imp.SetHasBeenCopied(s.getHasbeencopied() == 1 ? true: false);
			
			this.instances.put(lid, imp);
			
		}	
		
		return imp;

	}

	@Override
	public synchronized void delete() {
		logger.info(" ParameterSetImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwParamSetImp GetNewOne(String Name, String Comment, String FromPS) {
		logger.info(" ParameterSetImpFactory.class , return super.GetNewOne(Name, Comment, FromPS); no imp");
		return super.GetNewOne(Name, Comment, FromPS);
		
	}

	@Override
	public FwParamSetImp DoGetOne(String Name) {
		logger.info(" ParameterSetImpFactory.class , return super.DoGetOne(Name); no imp");
		return super.DoGetOne(Name);
		
	}

	@Override
	public void DoGetAll(FwParamSetImpVector arg0) {
		List<Parameterset> dList = parametersetDao.getAll();
		for (Parameterset ps : dList) {
			long lid = ps.getId();
			int id = (int) lid;

			boolean copied = ps.getHasbeencopied() == 0 ? false : true;

			FwParamSetImp ins = new FwParamSetImp(id, ps.getName(), ps.getComments() == null ? "" : ps.getComments(), ps.getFromps() == null ? "" : ps.getFromps(), copied, true);
			arg0.add(ins);
			instances.put(lid, ins);

		}
		
	}

	@Override
	public FwParamSetImp GetOne(String arg0) {
		List<Parameterset> dList = parametersetDao.getAll();
		for (Parameterset ps : dList) {
			if(ps.getName().equalsIgnoreCase(arg0)){
				long lid = ps.getId();
				int id = (int) lid;

				boolean copied = ps.getHasbeencopied() == 0 ? false : true;

				return new FwParamSetImp(id, ps.getName(), ps.getComments() == null ? "" : ps.getComments(), ps.getFromps() == null ? "" : ps.getFromps(), copied, true);
			}
		}
		return null;
		
	}

	@Override
	public boolean CheckName(String arg0) {
		logger.info(" ParameterSetImpFactory.class , return super.CheckName(arg0); no imp");
		return super.CheckName(arg0);
		
	}

	@Override
	public boolean Rename(String arg0, String arg1) {
		logger.info(" ParameterSetImpFactory.class , return super.Rename(arg0, arg1); no imp");
		return super.Rename(arg0, arg1);
		
	}

	@Override
	public void DoLoadAll() {
		List<Parameterset> dList = parametersetDao.getAll();
		for (Parameterset ps : dList) {
			long lid = ps.getId();
			int id = (int) lid;
			boolean copied = ps.getHasbeencopied() == 0 ? false : true;
			FwParamSetImp ins = new FwParamSetImp(id, ps.getName(), ps.getComments() == null ? "" : ps.getComments(), ps.getFromps() == null ? "" : ps.getFromps(), copied, true);
			instances.put(lid, ins);

		}
		
	}

	@Override
	public void AddNew(FwParamSetImp arg0) {
		logger.info(" ParameterSetImpFactory.class , super.AddNew(arg0); no imp");
		super.AddNew(arg0);
		
	}

	@Override
	public void AddDeleted(FwParamSetImp arg0) {
		logger.info(" ParameterSetImpFactory.class , super.AddDeleted(arg0); no imp");
		super.AddDeleted(arg0);
		
	}

	@Override
	public void DoCancel(FwParamSetImp arg0) {
		logger.info(" ParameterSetImpFactory.class , super.DoCancel(arg0); no imp");
		super.DoCancel(arg0);
		
	}

	@Override
	public void DoCancelAll() {
		logger.info(" ParameterSetImpFactory.class , super.DoCancelAll(); no imp");
		super.DoCancelAll();
		
	}

	@Override
	public void GetAll(FwParamSetImpVector arg0) {
		logger.info(" ParameterSetImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void Delete(FwParamSetImpVector arg0) {
		logger.info(" ParameterSetImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwParamSetImp arg0) {
		logger.info(" ParameterSetImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public boolean Store(FwParamSetImpVector vec) {
		logger.info(" ParameterSetImpFactory.class , return super.Store(vec); no imp");
		return super.Store(vec);
		
	}

	@Override
	public boolean Store(FwParamSetImpVector savem, FwParamSetImpVector deleteem) {
		logger.info(" ParameterSetImpFactory.class , return super.Store(savem, deleteem); no imp");
		return super.Store(savem, deleteem);
		
	}

	@Override
	public boolean Store(FwParamSetImp imp) {
		logger.info(" ParameterSetImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public boolean Store() {
		logger.info(" ParameterSetImpFactory.class , return super.Store(); no imp");
		return super.Store();
		
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" ParameterSetImpFactory.class , return super.DeleteAll(); no imp");
		return super.DeleteAll();
		
	}

	@Override
	public void CancelAll() {
		logger.info(" ParameterSetImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public void Cancel(FwParamSetImp arg0) {
		logger.info(" ParameterSetImpFactory.class , super.Cancel(arg0); no imp");
		super.Cancel(arg0);
		
	}

	@Override
	public void AddID(FwParamSetImp arg0) {
		logger.info(" ParameterSetImpFactory.class , super.AddID(arg0); no imp");
		super.AddID(arg0);
		
	}

	@Override
	public void Cleanup() {
		logger.info(" ParameterSetImpFactory.class , super.Cleanup(); no imp");
		super.Cleanup();
		
	}

}
