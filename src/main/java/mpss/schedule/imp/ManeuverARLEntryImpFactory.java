package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.ManeuverrequestDao;
import mpss.common.jpa.Maneuverrequest;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.*;
import mpss.schedule.base.GlManeuver.Type;

public class ManeuverARLEntryImpFactory extends FwManeuverARLEntryImpFactory implements UIDB<Maneuverrequest> {

	private Map<Long, FwManeuverARLEntryImp> instances = new HashMap<Long, FwManeuverARLEntryImp>();
	private static ManeuverrequestDao maneuverDao=JPAInit.getBuilder().getManeuverDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	public boolean create(Maneuverrequest object){
		if(maneuverDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }
	
	public Long createGetId(Maneuverrequest object){
		Maneuverrequest data  =maneuverDao.create(object);		
		return data.getId();		
	 }

	 public boolean update(Maneuverrequest object){
		 return maneuverDao.update(object);
	 }

	 public boolean delete(Maneuverrequest object){
		return maneuverDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return  maneuverDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		this.instances.clear();
		maneuverDao.reset();
	}

	@Override
	public synchronized void delete() {
		logger.info(" ManeuverARLEntryImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwManeuverARLEntryImp GetOne(int parentID, GlDaySpec date,
			long StartTime, long Duration, double q1, double q2, double q3,
			double q4, String station, String elev, String azim, int orbit,
			String proc, String plan, Type type, double maneuverDuration,
			String Comments) {
		logger.info(" ManeuverARLEntryImpFactory.class , return super.GetOne(...); no imp");
		return super.GetOne(parentID, date, StartTime, Duration, q1, q2, q3, q4,
				station, elev, azim, orbit, proc, plan, type, maneuverDuration,
				Comments);
		
	}

	@Override
	protected FwManeuverARLEntryImp CreateOne(int parent, GlDaySpec date,
			long StartTime, long Duration, double q1, double q2, double q3,
			double q4, String station, String elev, String azim, int orbit,
			String proc, String plan, Type type, double maneuverDuration,
			String Comments) {
		logger.info(" ManeuverARLEntryImpFactory.class , return super.CreateOne(...); no imp");
		return super.CreateOne(parent, date, StartTime, Duration, q1, q2, q3, q4,
				station, elev, azim, orbit, proc, plan, type, maneuverDuration,
				Comments);
		
	}

	@Override
	public FwBaseARLEntryImp GetID(int arg0) {		
		Maneuverrequest item =maneuverDao.getManeuverrequest(arg0);		
		
		FwManeuverARLEntryImp fwrev = new FwManeuverARLEntryImp();
		fwrev.SetID(arg0);
		fwrev.SetARLID(item.getArlid().intValue());
		GlDaySpec ss = new GlDaySpec();
		ss.setDay(item.getDay());
		ss.setYear(item.getYear());
		fwrev.SetDate(ss); 
		fwrev.SetStartTime(item.getStarttime().intValue());
		fwrev.SetDuration(item.getDuration().intValue());
		fwrev.SetQ1(item.getQ1());
		fwrev.SetQ2(item.getQ2());
		fwrev.SetQ3(item.getQ3());
		fwrev.SetQ4(item.getQ4());
		fwrev.SetStation(item.getStation());
		fwrev.SetElevation(String.valueOf(item.getElevation()));
		fwrev.SetAzimuth(String.valueOf(item.getAzimuth()));
		fwrev.SetOrbit(item.getOrbit().intValue());
		fwrev.SetManeuverProc(item.getManeuverproc()==null? "":item.getManeuverproc());
		fwrev.SetOrbitManPlan(item.getOrbitmanplan()==null? "":item.getOrbitmanplan());
		fwrev.SetType(getManeuverType(item.getType()));
		fwrev.SetComments(item.getComments()==null? "":item.getComments());
		fwrev.SetManeuverDuration(item.getManeuverduration());		
		instances.put((long)arg0,fwrev);
		return fwrev;
		
	}
	
	public GlManeuver.Type getManeuverType(String type){
		if(type.equals("FTYPE")){
			return GlManeuver.Type.FTYPE;
			
		}else if(type.equals("ATTITUDE")){
			return GlManeuver.Type.ATTITUDE;
			
		}else if(type.equals("GOHOME")){
			return GlManeuver.Type.GOHOME;
			
		}else if(type.equals("ORBIT")){			
			return GlManeuver.Type.ORBIT;
		}else if(type.equals("LTYPE")){
			return GlManeuver.Type.LTYPE;			
		}else{
			return GlManeuver.Type.LTYPE;	
		}
		
	}

	@Override
	public void GetAll(FwBaseARLEntryImpVector arg0, int parentID) {
		logger.info(" ManeuverARLEntryImpFactory.class , super.GetAll(arg0, parentID); no imp");
		super.GetAll(arg0, parentID);
		
	}

	@Override
	public void Delete(FwBaseARLEntryImp arg0) {
		logger.info(" ManeuverARLEntryImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwBaseARLEntryImpVector arg0) {
		logger.info(" ManeuverARLEntryImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void CancelAll() {
		logger.info(" ManeuverARLEntryImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public boolean Store(FwBaseARLEntryImpVector imps,
			FwBaseARLEntryImpVector delimps) {
		logger.info(" ManeuverARLEntryImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwBaseARLEntryImp imp) {
		logger.info(" ManeuverARLEntryImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" ManeuverARLEntryImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" ManeuverARLEntryImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" ManeuverARLEntryImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}

	public HashMap<Long,Long> copyArl(int arlid,int new_arlid) {
		HashMap<Long,Long> maneuvermaps = new HashMap<Long,Long>();
		List<Maneuverrequest> lists = maneuverDao.getByArlId(arlid);
		for(Maneuverrequest info:lists){
			Long id  = info.getId();
			info.setId(null);			
			info.setArlid(new_arlid);			
			maneuvermaps.put(id, createGetId(info));	
		}
		return maneuvermaps;
	}

}
