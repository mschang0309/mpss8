package mpss.schedule.imp;

import mpss.configFactory;
import mpss.common.dao.ExtentDao;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Extentactivitytimerange;
import mpss.schedule.JPAInit;
import mpss.schedule.Schedule;
import mpss.schedule.UIDB;
import mpss.schedule.Schedule.SchType;
import mpss.schedule.Schedule.SessionType;
import mpss.schedule.base.*;
import mpss.schedule.base.GlARL.Type;
import mpss.util.ExtentActivityTimeRangeUtils;
import mpss.util.ExtentActivityTimeRangeUtils.ActivityType;
import mpss.util.timeformat.DateUtil;

import java.sql.Timestamp;
import java.util.*;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

public class ExtentImpFactory extends FwExtentImpFactory implements UIDB<Extent> {

	public static Map<Long, FwExtentImp> instances = new HashMap<Long, FwExtentImp>();
	FwTimeRangeTracker ourTRTracker = new FwTimeRangeTracker();
	
	private static ExtentDao extentDao=JPAInit.getBuilder().getExtentDao();	
	private ExtentActivityTimeRangeImpFactory eatrfact;
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	
	public void setEatrFactory(ExtentActivityTimeRangeImpFactory eatrfact){
		this.eatrfact= eatrfact;
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" ExtentImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" ExtentImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" ExtentImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}
	
	public boolean create(Extent object){
		if(extentDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }
	
	public Long createGetId(Extent object){
		Extent data  =extentDao.create(object);		
		return data.getId();		
	 }

	 public boolean update(Extent object){
		 return extentDao.update(object);
	 }

	 public boolean delete(Extent object){
		return extentDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return extentDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		ExtentImpFactory.instances.clear();
		extentDao.reset();
		ourTRTracker = null;
		ourTRTracker = new FwTimeRangeTracker();
	}

	@Override
	public FwExtentImp GetOne(int actID, int contID, int branchid, int transmitterID, int recorderID, int antennaID, GlTimeRange tr, int recorderDelta, int secondRecorderDelta, int filename,
			int secondFilename) {		
		return this.CreateOne(actID, contID, branchid, transmitterID, recorderID, antennaID, tr, recorderDelta, secondRecorderDelta, filename, secondFilename, false, false, false, false);
	}

	@Override
	public FwExtentImp FindOne(GlTimeRange tr, int branchID, int transmitterID, int antennaID, int recorderID, int recorderDelta, int secRecorderDelta, int filename, int secFilename) {
		logger.info("call ExtentImpFactory.FindOne(...)");
		FwExtentImp imp = null;
		String starttime = tr.GetStartTime().asString("%Y-%m-%d %H:%M:%S");
		String endtime = tr.GetEndTime().asString("%Y-%m-%d %H:%M:%S");
		Extent ext = extentDao.getExtentByParams(starttime, endtime, branchID, transmitterID, antennaID, recorderID, recorderDelta, secRecorderDelta, filename, secFilename);

		if (ext != null) {
			imp = this.SetFwExtentImp(ext);
		}
		return imp;

	}

	@Override
	public FwExtentImp GetLastRecord(int recorderid, GlTime t, int branch) {
		logger.info("call ExtentImpFactory.GetLastRecord(.,.,.)");
		return this.LoadLastRecord(recorderid, t, branch);
	}

	@Override
	public FwExtentImp GetLastByType(int recorderid, GlTime t, int branch, Type type) {
		logger.info("call ExtentImpFactory.GetLastByType(.,.,.)");
		return this.LoadLastByType(recorderid, t, branch, type);
	}

	@Override
	public void DoGetAll(FwExtentImpVector arg0, GlTimeRange arg1, boolean GetOverlap, int branchid) {
		
		if(Schedule.s_type== SessionType.BRANCH){
			branchid = Schedule.branch_type_id;
			if(branchid==0){
				logger.info("extend getall branch=0");
				return;
			}else{
			}
		}
		
		GlTimeRangeVector needTRs=new GlTimeRangeVector();
		FwExtentImpVector localImps=new FwExtentImpVector();
	
		if (ourTRTracker.NeedOtherTRs(branchid, arg1, needTRs))	    {
	        for (int i = 0; i < needTRs.size(); i++)	        {            
	        	LoadAll(localImps, needTRs.get(i), branchid);	            
	            logger.info("FwExtentImpFactory::DoGetAll.  Loading("
		                + needTRs.get(i).AsString() + ")" + " #loaded: " + localImps.size() + " bid: " + branchid );
	                
	            ourTRTracker.UpdateTRs(branchid, needTRs.get(i));
	        }
	    }   
		
//		Date d1 =DateUtil.stringToDate("2004/01/01 00:00:00", "yyyy/MM/dd HH:mm:ss");
//		Date d2 =DateUtil.stringToDate("2014/01/19 13:40:00", "yyyy/MM/dd HH:mm:ss");				
//		GlTimeRange range1 =new GlTimeRange();		
//		range1.SetStartTime(DateUtil.timestampToGlTIme(DateUtil.dateToTimestamp(d1)));
//		range1.SetEndTime(DateUtil.timestampToGlTIme(DateUtil.dateToTimestamp(d2)));
//		needTRs.add(range1);
//		Date d3 =DateUtil.stringToDate("2014/01/20 13:40:00", "yyyy/MM/dd HH:mm:ss");
//		Date d4 =DateUtil.stringToDate("2020/01/01 00:00:00", "yyyy/MM/dd HH:mm:ss");		
//		GlTimeRange range2 =new GlTimeRange();		
//		range2.SetStartTime(DateUtil.timestampToGlTIme(DateUtil.dateToTimestamp(d3)));
//		range2.SetEndTime(DateUtil.timestampToGlTIme(DateUtil.dateToTimestamp(d4)));
//		needTRs.add(range2);
//		
//	        for (int i = 0; i < needTRs.size(); i++)	        {            
//	        	LoadAll(localImps, needTRs.get(i), branchid);
//	            System.out.println("FwExtentImpFactory::DoGetAll.  Loading("
//	                + needTRs.get(i).AsString() + ")" + " #loaded: " + localImps.size()+" branchid: " + branchid  );
//	                
//	            ourTRTracker.UpdateTRs(branchid, needTRs.get(i));
//	        }
	    
		// for branch start
		branchid =0;
		// for branch end
		for(Entry<Long, FwExtentImp> entry : instances.entrySet()) {	        
	    	FwExtentImp imp = entry.getValue();
	    	
	    	
	    	if (imp.GetBranch() == branchid)
	        {
	            GlTimeRange    nexttr = imp.GetTimeRange();
	            
//	            if (nexttr.first > tr.second)
//	                break;	          
	            
	            
	            if (GetOverlap)
	            {
	                if (nexttr.Overlaps(arg1)){
	                	arg0.add(imp);
	                }	                    
	            }else if (nexttr.StartsWithin(arg1)){
	            	arg0.add(imp);
	            }	            	
	        }	
	    }
		
	}

	@Override
	public void DoGetAll(FwExtentImpVector arg0, int contactID) {
		logger.info("call extentImpFacory.DoGetAll(.,.)");
		this.GetExtents(arg0, contactID);
	}

	@Override
	public void GetAllBranch(FwExtentImpVector arg0, int branchID) {
		logger.info("call extentImpFacory.GetAllBranch(.,.)");
		FwExtentImp imp = null;
		if(Schedule.s_type== SessionType.BRANCH){
			branchID = Schedule.branch_type_id;
			if(branchID==0){
				logger.info("extent getall branch=0");
				return;
			}

		}
		List<Extent> list = extentDao.getByBranchId(branchID);
		for (int i = 0; i < list.size(); i++) {
			Extent ext = list.get(i);
			imp = this.SetFwExtentImp(ext);
			
			arg0.add(imp);
		}
	}

	@Override
	public void GetExtents(FwExtentImpVector arg0, int contactID) {
		//System.out.println("call extentImpFacory.GetExtents(.,.)"); //OK
		FwExtentImp imp = null;
		// in FwExtentImpFactory.cpp, GetExtents has a sub function
		// GetSession(contactID), must be confirm
		List<Extent> list = extentDao.getByContactId(contactID);
		for (int i = 0; i < list.size(); i++) {
			Extent ext = list.get(i);			
			imp = this.SetFwExtentImp(ext);
			
			arg0.add(imp);
		}
	}

	@Override
	public void GetAllSats(FwExtentImpVector arg0, int arg1) {
		logger.info("call extentImpFacory.GetAllSats(.,.)");
		FwExtentImp imp = null;

		List<Extent> list = extentDao.getBySatelliteId(arg1);
		for (int i = 0; i < list.size(); i++) {
			Extent ext = list.get(i);
			imp = this.SetFwExtentImp(ext);			
			arg0.add(imp);
		}
	}

	@Override
	public void GetAllSites(FwExtentImpVector arg0, int arg1) {
		logger.info("call extentImpFacory.GetAllSites(.,.)");
		FwExtentImp imp = null;

		List<Extent> list = extentDao.getBySiteId(arg1);
		for (int i = 0; i < list.size(); i++) {
			Extent ext = list.get(i);
			imp = this.SetFwExtentImp(ext);
			arg0.add(imp);
		}
	}

	@Override
	public void GetAllTx(FwExtentImpVector arg0, int arg1) {
		logger.info("call extentImpFacory.GetAllTx(.,.)");
		FwExtentImp imp = null;

		List<Extent> list = extentDao.getByTransmitterId(arg1);
		for (int i = 0; i < list.size(); i++) {
			Extent ext = list.get(i);
			imp = this.SetFwExtentImp(ext);
			arg0.add(imp);
		}
	}

	@Override
	public void GetAllRec(FwExtentImpVector arg0, int arg1) {
		logger.info("call extentImpFacory.GetAllRec(.,.)");
		FwExtentImp imp = null;

		List<Extent> list = extentDao.getByRecorderId(arg1);
		for (int i = 0; i < list.size(); i++) {
			Extent ext = list.get(i);
			imp = this.SetFwExtentImp(ext);
			arg0.add(imp);
		}
	}

	@Override
	public void GetAllAnt(FwExtentImpVector arg0, int arg1) {
		logger.info("call extentImpFacory.GetAllAnt(.,.)");
		FwExtentImp imp = null;

		List<Extent> list = extentDao.getByAntennaId(arg1);
		for (int i = 0; i < list.size(); i++) {
			Extent ext = list.get(i);
			imp = this.SetFwExtentImp(ext);
			arg0.add(imp);
		}
	}

	

	@Override
	public boolean CheckActivityDependency(int activityID) {
		logger.info("call extentImpFacory.CheckActivityDependency(.)");
		boolean flag = false;
		for (Long key : instances.keySet()) {
			FwExtentImp imp = instances.get(key);
			if (imp.GetActivity() == activityID)
				flag = true;
		}

		return flag;
	}

	@Override
	public boolean CheckTransmitterDependency(int ID) {
		logger.info("call extentImpFacory.CheckTransmitterDependency(.)");
		boolean flag = false;
		for (Long key : instances.keySet()) {
			FwExtentImp imp = instances.get(key);
			if (imp.GetTransmitter() == ID)
				flag = true;
		}

		return flag;
	}

	@Override
	public boolean CheckRecorderDependency(int ID) {
		logger.info("call extentImpFacory.CheckRecorderDependency(.)");
		boolean flag = false;
		for (Long key : instances.keySet()) {
			FwExtentImp imp = instances.get(key);
			if (imp.GetRecorder() == ID)
				flag = true;
		}

		return flag;
	}

	@Override
	public boolean CheckAntennaDependency(int ID) {
		logger.info("call extentImpFacory.CheckAntennaDependency(.)");
		boolean flag = false;
		for (Long key : instances.keySet()) {
			FwExtentImp imp = instances.get(key);
			
			if (imp.GetAntenna() == ID)
				flag = true;
		}

		return flag;
	}

	@Override
	public FwExtentImp GetID(int arg0) {
		//System.out.println("call extentImpFacory.GetID(.) id="+arg0); // OK
		FwExtentImp imp = null;
		
		if (instances.containsKey(Long.valueOf(arg0))) {
			imp = (FwExtentImp) instances.get(Long.valueOf(arg0));
		} else {
			Extent ext = extentDao.getExtentById(arg0);
			imp = this.SetFwExtentImp(ext);			

			if (imp != null) {
				ExtentImpFactory.instances.put(ext.getId(), imp);
			}
		}

		if (imp == null) {
			logger.info("ExtentImpFactory GetID(" + arg0 + ") is null");

		}
		
		return imp;
	}

	@Override
	public synchronized void delete() {
		logger.info(" ExtentImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	protected void LoadAll(FwExtentImpVector arg0, int contactID) {
		logger.info(" ExtentImpFactory.class , super.LoadAll(arg0, contactID); no imp");
		super.LoadAll(arg0, contactID);
		
	}

	@Override
	protected void LoadAllBranch(FwExtentImpVector arg0, int branch) {
		logger.info(" ExtentImpFactory.class , super.LoadAllBranch(arg0, branch); no imp");
		super.LoadAllBranch(arg0, branch);
		
	}

	@Override
	protected void LoadAllSats(FwExtentImpVector arg0, int satid) {
		logger.info(" ExtentImpFactory.class , super.LoadAllSats(arg0, satid); no imp");
		super.LoadAllSats(arg0, satid);
		
	}

	@Override
	protected void LoadAllSites(FwExtentImpVector arg0, int siteid) {
		logger.info(" ExtentImpFactory.class , super.LoadAllSites(arg0, siteid); no imp");
		super.LoadAllSites(arg0, siteid);
		
	}

	@Override
	protected void LoadAllTx(FwExtentImpVector arg0, int id) {
		logger.info(" ExtentImpFactory.class , super.LoadAllTx(arg0, id); no imp");
		super.LoadAllTx(arg0, id);
		
	}

	@Override
	protected void LoadAllRec(FwExtentImpVector arg0, int id) {
		logger.info(" ExtentImpFactory.class , super.LoadAllRec(arg0, id); no imp");
		super.LoadAllRec(arg0, id);
		
	}

	@Override
	protected void LoadAllAnt(FwExtentImpVector arg0, int id) {
		logger.info(" ExtentImpFactory.class , super.LoadAllAnt(arg0, id); no imp");
		super.LoadAllAnt(arg0, id);
		
	}

	@Override
	protected void DoCleanup() {
		logger.info(" ExtentImpFactory.class , super.DoCleanup(); no imp");
		super.DoCleanup();
		
	}

	@Override
	protected void AddNew(FwExtentImp arg0) {
		logger.info(" ExtentImpFactory.class , super.AddNew(arg0); no imp");
		super.AddNew(arg0);
		
	}

	@Override
	protected void AddDeleted(FwExtentImp arg0) {
		logger.info(" ExtentImpFactory.class , super.AddDeleted(arg0);"); //by jeff ddt need
        extentDao.doSql("DELETE FROM extent WHERE id='"+arg0.GetID()+"'");  
        //super.AddDeleted(arg0); 	
	}

	@Override
	protected void DoCancel(FwExtentImp arg0) {
		logger.info(" ExtentImpFactory.class , super.DoCancel(arg0); no imp");
		super.DoCancel(arg0);
		
	}

	@Override
	protected void DoCancelAll() {
		logger.info(" ExtentImpFactory.class , super.DoCancelAll(); no imp");
		super.DoCancelAll();
		
	}

	@Override
	protected boolean CheckActivityInDatabase(int activityID) {
		logger.info(" ExtentImpFactory.class , return super.CheckActivityInDatabase(activityID); no imp");
		return super.CheckActivityInDatabase(activityID);
		
	}

	@Override
	protected boolean CheckTransmitterInDatabase(int ID) {
		logger.info(" ExtentImpFactory.class , return super.CheckTransmitterInDatabase(ID); no imp");
		return super.CheckTransmitterInDatabase(ID);
		
	}

	@Override
	protected boolean CheckRecorderInDatabase(int ID) {
		logger.info(" ExtentImpFactory.class , return super.CheckRecorderInDatabase(ID); no imp");
		return super.CheckRecorderInDatabase(ID);
		
	}

	@Override
	protected boolean CheckAntennaInDatabase(int ID) {
		logger.info(" ExtentImpFactory.class , return super.CheckAntennaInDatabase(ID); no imp");
		return super.CheckAntennaInDatabase(ID);
		
	}

	@Override
	public void Delete(FwExtentImpVector arg0) {
		logger.info(" ExtentImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwExtentImp arg0) {
		logger.info(" ExtentImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" ExtentImpFactory.class , return super.DeleteAll(); no imp");
		return super.DeleteAll();
		
	}

	@Override
	public void CancelAll() {
		logger.info(" ExtentImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public void Cancel(FwExtentImp arg0) {
		logger.info(" ExtentImpFactory.class , super.Cancel(arg0); no imp");
		super.Cancel(arg0);
		
	}

	@Override
	public void AddID(FwExtentImp arg0) {
		logger.info(" ExtentImpFactory.class , super.AddID(arg0); no imp");
		super.AddID(arg0);		
	}

	@Override
	public void Cleanup() {
		logger.info(" ExtentImpFactory.class , super.Cleanup(); no imp");
		super.Cleanup();
		
	}

	@Override
	public void GetAll(FwExtentImpVector arg0) {
		logger.info("call ExtentImpFactory.GetAll(.)");
		this.DoGetAll(arg0);
	}

	@Override
	public void DoGetAll(FwExtentImpVector arg0) {
		logger.info("call ExtentImpFactory.DoGetAll(.)");
		List<Extent> list = extentDao.getAll();
		for (Extent ext : list){
			FwExtentImp imp = new FwExtentImp();
			imp = this.SetFwExtentImp(ext);			
			arg0.add(imp);
		}
	}

	

	@Override
	protected FwExtentImp CreateOne(int actID, int contactID, int branchID, int transmitterID, int recorderID, int antennaID, GlTimeRange tr, int recorderDelta, int secondRecorderDelta, int filename,
			int secondFilename, boolean UserModified, boolean UserLocked, boolean SoftConstraintViolated, boolean HardConstraintViolated) {
		long newid = extentDao.getNextId();
		FwExtentImp imp = new FwExtentImp();
		imp.SetID((int) newid);
		imp.SetActivity(actID);
		imp.SetContact(contactID);
		imp.SetBranch(branchID);
		imp.SetTransmitter(transmitterID);
		imp.SetRecorder(recorderID);
		imp.SetAntenna(antennaID);
		imp.SetTimeRange(tr);
		imp.SetRecorderDelta(recorderDelta);
		imp.SetSecondRecorderDelta(secondRecorderDelta);
		imp.SetFilename(filename);
		imp.SetSecondFilename(secondFilename);
		imp.SetUserModified(UserModified);
		imp.SetUserLocked(UserLocked);
		imp.SetSoftConstraintViolated(SoftConstraintViolated);
		imp.SetHardConstraintViolated(HardConstraintViolated);
				

		if (imp != null) {
			ExtentImpFactory.instances.put(newid, imp);
		}
		
		return imp;
	}

	@Override
	protected FwExtentImp LoadLastRecord(int recorderid, GlTime arg1, int branch) {
		logger.info("call ExtentImpFactory.LoadLastRecord(...)");
		FwExtentImp imp = null;
		String time = arg1.asString("%Y-%m-%d %H:%M:%S");
		List<Extent> list = extentDao.getLastRecord(recorderid, branch, time);
		if (list.size() > 0) {
			imp = new FwExtentImp();
			Extent ext = list.get(0); // extentDao.GetLastRecord's sql is order
										// by startime desc, so we use get(0) to
										// get the first record.

			imp = this.SetFwExtentImp(ext);
		}
		return imp;
	}

	@Override
	protected FwExtentImp LoadLastByType(int recorderid, GlTime arg1, int branch, Type arg3) {
		System.out.println("call ExtentImpFactory.LoadLastByType(...)");
		logger.info("call ExtentImpFactory.LoadLastByType(...)");
		FwExtentImp imp = null;
		String time = arg1.asString("%Y-%m-%d %H:%M:%S");
		String arltype = this.GetArlTypeString(arg3);
		List<Extent> list = extentDao.getLastRecordByType(recorderid, branch, time, arltype);
		if (list.size() > 0) {
			imp = new FwExtentImp();
			Extent ext = list.get(0); // extentDao.GetLastRecord's sql is order
										// by startime desc, so we use get(0) to
										// get the first record.

			imp = this.SetFwExtentImp(ext);
		}
		return imp;
	}

	@Override
	protected void LoadAll(FwExtentImpVector arg0, GlTimeRange arg1, int branch) {
		logger.info("call ExtentImpFactory.LoadAll(...) branch="+branch); //OK
		String starttime = arg1.GetStartTime().asString("%Y-%m-%d %H:%M:%S");
		String endtime = arg1.GetEndTime().asString("%Y-%m-%d %H:%M:%S");		
		List<Extent> list = extentDao.getByBranchIdNTimeRange(branch, starttime, endtime);
		for (Extent ext : list){
			FwExtentImp imp = new FwExtentImp();
			
			imp = this.SetFwExtentImp(ext);				
			ExtentImpFactory.instances.put(Long.valueOf(imp.GetID()), imp);
			arg0.add(imp);
		}
	}

	

	protected FwExtentImp SetFwExtentImp(Extent ext) {		
		FwExtentImp imp = new FwExtentImp();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(ext.getStarttime().getTime()));
		// System.out.println("cal1="+sdf.format(cal.getTime()));
		long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

		cal.setTime(new Date(ext.getEndtime().getTime()));
		// System.out.println("cal2="+sdf.format(cal.getTime()));
		long sec2 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

		long lid = ext.getId();
		int id = (int) lid;
		imp.SetID(id);
		imp.SetActivity(ext.getActivityid());
		imp.SetContact(ext.getContactid());
		//imp.SetBranch(ext.getBranchid());
		imp.SetBranch(0);
		imp.SetTransmitter(ext.getTransmitterid());
		imp.SetRecorder(ext.getRecorderid());
		imp.SetAntenna(ext.getAntennaid());
		imp.SetTimeRange(new GlTimeRange(new GlTime(sec1), new GlTime(sec2)));
		imp.SetRecorderDelta(ext.getRecorderdelta());
		imp.SetSecondRecorderDelta(ext.getSecondrecorderdelta());
		imp.SetFilename(ext.getFilename());
		imp.SetSecondFilename(ext.getSecondfilename());
		imp.SetUserModified(ext.getUsermodified() == 1 ? true : false);
		imp.SetUserLocked(ext.getUserlocked() == 1 ? true : false);
		imp.SetSoftConstraintViolated(ext.getSoftconstraintviolated() == 1 ? true : false);
		imp.SetHardConstraintViolated(ext.getHardconstraintviolated() == 1 ? true : false);

		return imp;

	}

	protected String GetArlTypeString(Type type) {
		logger.info("call ExtentImpFactory.GetArlTypeString(.)");
		String arltype = null;
		if (type.equals(GlARL.Type.DEL)) {
			arltype = "DELETE";
		} else if (type.equals(GlARL.Type.EXTFILE)) {
			arltype = "EXTFILE";
		} else if (type.equals(GlARL.Type.FTYPE)) {
			arltype = "FTYPE";
		} else if (type.equals(GlARL.Type.MON)) {
			arltype = "MON";
		} else if (type.equals(GlARL.Type.UPLINK)) {
			arltype = "UPLINK";
		} else if (type.equals(GlARL.Type.RTCMD)) {
			arltype = "RTCMD";
		} else if (type.equals(GlARL.Type.MISCPROC)) {
			arltype = "MISCPROC";
		} else if (type.equals(GlARL.Type.RSI)) {
			arltype = "RSI";
		} else if (type.equals(GlARL.Type.ISUAL)) {
			arltype = "ISUAL";
		} else if (type.equals(GlARL.Type.ISUALPROC)) {
			arltype = "ISUALPROC";
		} else if (type.equals(GlARL.Type.MANEUVER)) {
			arltype = "MANEUVER";
		} else if (type.equals(GlARL.Type.XCROSS)) {
			arltype = "XCROSS";
		}else if (type.equals(GlARL.Type.AIPPROC)) {
			arltype = "AIPPROC";
		}else if (type.equals(GlARL.Type.AIPPBK)) {
			arltype = "AIPPBK";
		} else if (type.equals(GlARL.Type.TOY)) {
			arltype = "TOY";
		} else if (type.equals(GlARL.Type.LTYPE)) {
			arltype = "LTYPE";
		}
		return arltype;
	}

	@Override
	public void GetAllRec(FwExtentImpVector arg0, GlTimeRange tr, int recid,int bid) {
		logger.info(" ExtentImpFactory.class , super.GetAllRec(arg0, tr, recid, bid); no imp");
		super.GetAllRec(arg0, tr, recid, bid);
		
	}

	@Override
	public void AddExtent(FwExtentImp arg0, int contactID) {
		logger.info(" ExtentImpFactory.class , super.AddExtent(arg0, contactID); no imp");
		super.AddExtent(arg0, contactID);
		
	}

	@Override
	public void RemoveExtent(FwExtentImp arg0) {
		logger.info(" ExtentImpFactory.class , super.RemoveExtent(arg0);");  //by jeff ddt need  

        extentDao.doSql("DELETE FROM extent WHERE id='"+arg0.GetID()+"'");   		
	}

	@Override
	public void SetExtents(FwExtentImpVector arg0, int contactID) {
		System.out.println(" ExtentImpFactory.class , super.SetExtents(arg0, contactID); no imp");
		logger.info(" ExtentImpFactory.class , super.GetAll(arg0); no imp");
		super.SetExtents(arg0, contactID);
		
	}	

	@Override
	public boolean Store(FwExtentImpVector vec) {
		
		//GlTimeRangeVector TimeRanges ;
		try {					
			int size = (int)vec.size();	
			for (int i = 0; i < size; i++) {				
				FwExtentImp imp = vec.get(i);				
				Extent extent = new Extent();								
				String starttime = imp.GetTimeRange().GetStartTime().asString("%Y-%m-%d %H:%M:%S");
				String endtime = imp.GetTimeRange().GetEndTime().asString("%Y-%m-%d %H:%M:%S");
				Date dStart = DateUtil.stringToDate(starttime, "yyyy-MM-dd HH:mm:ss");
				Date dEnd = DateUtil.stringToDate(endtime, "yyyy-MM-dd HH:mm:ss");
				Timestamp tsStart = DateUtil.dateToTimestamp(dStart);
				Timestamp tsEnd =  DateUtil.dateToTimestamp(dEnd);
				
				extent.setId(Long.valueOf(imp.GetID()));
				extent.setActivityid(Integer.valueOf(imp.GetActivity()));
				extent.setContactid(Integer.valueOf(imp.GetContact()));
				extent.setBranchid(Integer.valueOf(imp.GetBranch()));
				extent.setTransmitterid(Integer.valueOf(imp.GetTransmitter()));
				extent.setRecorderid(Integer.valueOf(imp.GetRecorder()));
				extent.setAntennaid(Integer.valueOf(imp.GetAntenna()));
				extent.setStarttime(tsStart);
				extent.setEndtime(tsEnd);
				extent.setRecorderdelta(Integer.valueOf(imp.GetRecorderDelta()));
				extent.setSecondrecorderdelta(Integer.valueOf(imp.GetSecondRecorderDelta()));
				extent.setFilename(Integer.valueOf(imp.GetFilename()));
				extent.setSecondfilename(Integer.valueOf(imp.GetSecondFilename()));
				extent.setUsermodified(imp.GetUserModified() == true ? 1 : 0);
				extent.setUserlocked(imp.GetUserLocked()==true ? 1:0);
				extent.setSoftconstraintviolated(imp.GetSoftConstraintViolated()==true?1:0);
				extent.setHardconstraintviolated(imp.GetHardConstraintViolated()==true ?1:0);	
				extent.setIsaip(0);

				if(Schedule.type==SchType.COLD){	
					if(Schedule.updateDB==true){
						Schedule.coldrun2 =Schedule.coldrun2+1;
						if (imp.IsChanged()) {
							extentDao.update(extent);
						} else if (imp.IsNew()) {					
							extentDao.create(extent);
						}
					}
					
				}else if(Schedule.type==SchType.HOT){
					if(Schedule.s_type== SessionType.BRANCH){
						int branchid = Schedule.branch_type_id;
						extent.setBranchid(branchid);
					}
					if(Schedule.updateDB==true){
						if (imp.IsChanged()) {
							extentDao.update(extent);							
						} else if (imp.IsNew()) {					
							extentDao.create(extent);		
							
						}
					}
										
				}else if(Schedule.type==SchType.RESCH){
					if(Schedule.s_type== SessionType.BRANCH){
						int branchid = Schedule.branch_type_id;
						extent.setBranchid(branchid);
					}
					if(Schedule.updateDB==true){
						if (imp.IsChanged()) {
							extentDao.update(extent);							
						} else if (imp.IsNew()) {					
							extentDao.create(extent);
							
						}						
					}
				}else if(Schedule.type==SchType.MOVE){
					if(Schedule.s_type== SessionType.BRANCH){
						int branchid = Schedule.branch_type_id;
						extent.setBranchid(branchid);
					}
					if(Schedule.updateDB==true){
						if (imp.IsChanged()) {
							extentDao.update(extent);							
						} else if (imp.IsNew()) {					
							extentDao.create(extent);								
						}						
					}
				}else if(Schedule.type==SchType.ADJUST){
					if(Schedule.s_type== SessionType.BRANCH){
						int branchid = Schedule.branch_type_id;
						extent.setBranchid(branchid);
					}
					if(Schedule.updateDB==true){
						if (imp.IsChanged()) {
							extentDao.update(extent);							
						} else if (imp.IsNew()) {					
							extentDao.create(extent);								
						}						
					}
				}else if(Schedule.type==SchType.RESCH_FILTER){
					if(Schedule.s_type== SessionType.BRANCH){
						int branchid = Schedule.branch_type_id;
						extent.setBranchid(branchid);
					}
					if(Schedule.updateDB==true){
						if (imp.IsChanged()) {
							extentDao.update(extent);							
						} else if (imp.IsNew()) {					
							extentDao.create(extent);								
						}						
					}
				}
				
				// db Extentactivitytimerange
				ActivityImpFactory fac=Schedule.getActivityImpFactory();				
				
				if(fac!=null){	
					String Type = fac.getSubType(Integer.valueOf(imp.GetActivity()));
					if(Type.equals("REC")){
						eatrfact.doSql("delete FROM  extentactivitytimerange where parentid ="+imp.GetID());
						ArrayList<Extentactivitytimerange> ranges =ExtentActivityTimeRangeUtils.getExtentActivityTimeRanges(imp.GetID(), ActivityType.RSI_REC, dStart, dEnd);
						for(Extentactivitytimerange range:ranges){
							eatrfact.insertExtentActivityTimeRange(range);
						}						
					}else if(Type.equals("PBK")){
						eatrfact.doSql("delete FROM  extentactivitytimerange where parentid ="+imp.GetID());
						ArrayList<Extentactivitytimerange> ranges =ExtentActivityTimeRangeUtils.getExtentActivityTimeRanges(imp.GetID(), ActivityType.RSI_PBK, dStart, dEnd);
						for(Extentactivitytimerange range:ranges){
							eatrfact.insertExtentActivityTimeRange(range);
						}						
					}else if(Type.equals("DDT")){						
						eatrfact.doSql("delete FROM  extentactivitytimerange where parentid ="+imp.GetID());
						ArrayList<Extentactivitytimerange> ranges =ExtentActivityTimeRangeUtils.getExtentActivityTimeRanges(imp.GetID(), ActivityType.DDT, dStart, dEnd);
						for(Extentactivitytimerange range:ranges){
							eatrfact.insertExtentActivityTimeRange(range);
						}						
					}					
				}				
				
			}				
			
		} catch (Exception ex) {
			logger.info("Store(FwExtentImpVector arg0) ex:" + ex.toString());
			return false;
		}

		return true;
		
	}

	@Override
	public boolean Store(FwExtentImpVector savem, FwExtentImpVector deleteem) {
		logger.info(" ExtentImpFactory.class , return super.Store(savem, deleteem); no imp");
		return true;
		
	}

	@Override
	public boolean Store(FwExtentImp imp) {
		logger.info(" ExtentImpFactory.class , return super.Store(imp); ");
        FwExtentImpVector vec = new FwExtentImpVector();
        vec.add(imp);  
        return Store(vec);
		
	}

	@Override
	public boolean Store() {
		logger.info(" ExtentImpFactory.class , return super.Store(); no imp");
		return true;
		
	}
	
	
	public Extent getExtentByActivityId(int activityid){
		return  extentDao.getByActId(activityid);
	}
	
	public Extent getExtentById(int id){
		return  extentDao.getExtentById(id);
	}
	
	public boolean getContactHasExtent(long contactid){
		return extentDao.getContactHasExtent(contactid);		
	}
	
	
	public int getExtentTotalCount(){
		return extentDao.getAll().size();			
	}
	

}
