package mpss.schedule.imp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.RecorderpDao;
import mpss.common.jpa.Recorderp;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.*;


public class RecorderPSImpFactory extends FwRecorderPSImpFactory  implements UIDB<Recorderp> {

	private Map<Long, FwRecorderPSImp> instances = new HashMap<Long, FwRecorderPSImp>();
	private static RecorderpDao recorderpsDao=JPAInit.getBuilder().getRecorderpDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	public boolean create(Recorderp object){
		if(recorderpsDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Recorderp object){
		 return recorderpsDao.update(object);
	 }

	 public boolean delete(Recorderp object){
		 return recorderpsDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return recorderpsDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		this.instances.clear();
		recorderpsDao.reset();
	}

	@Override
	public synchronized void delete() {
		logger.info(" RecorderPSImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwRecorderPSImp GetOne(int psid, int satid, FwRecorderPSImp fromImp) {
		logger.info(" RecorderPSImpFactory.class , return super.GetOne(psid, satid, fromImp); no imp");
		return super.GetOne(psid, satid, fromImp);		
	}

	@Override
	public FwRecorderPSImp GetOne(int parentID, int psID, int cap, int rsi,int isual, int ddt) {
		logger.info(" RecorderPSImpFactory.class , return super.GetOne(parentID, psID, cap, rsi, isual, ddt); no imp");
		return super.GetOne(parentID, psID, cap, rsi, isual, ddt);
		
	}

	@Override
	protected FwRecorderPSImp CreateOne(int parentID, int psid, int cap,int rsi, int isual, int ddt) {
		logger.info(" RecorderPSImpFactory.class , return super.CreateOne(parentID, psid, cap, rsi, isual, ddt); no imp");
		return super.CreateOne(parentID, psid, cap, rsi, isual, ddt);
		
	}

	@Override
	protected FwRecorderPSImp CreateOne(int psid, int satid,FwRecorderPSImp fromImp) {
		logger.info(" RecorderPSImpFactory.class , return super.CreateOne(psid, satid, fromImp); no imp");
		return super.CreateOne(psid, satid, fromImp);
		
	}

	@Override
	public FwRecorderPSImp GetID(int arg0) {
		logger.info(" RecorderPSImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);		
	}

	@Override
	public void GetAll(FwRecorderPSImpVector arg0, int parentID) {
		logger.info(" RecorderPSImpFactory.class , super.GetAll(arg0, parentID); id= "+parentID);
		List<Recorderp> list = recorderpsDao.listRecorderps(parentID);
		Iterator<Recorderp> item = list.iterator();
		while (item.hasNext()) {
			Recorderp con = (Recorderp) item.next();	
			
			if(System.getProperty("satName").equalsIgnoreCase("ROCSAT2")){
				FwRecorderPSImp fwrev =new FwRecorderPSImp(con.getId().intValue()
						,con.getRecorderid().intValue()
						,con.getPsid().intValue()
						,con.getCapacity().intValue()
						,con.getRsicapacity().intValue()
						,con.getIsualcapacity().intValue()
						,con.getDdtcapacity().intValue()
						,con.getCompressionratiocount().intValue()
						,Double.parseDouble(String.valueOf(con.getPanimagingmode()))
						,Double.parseDouble(String.valueOf(con.getMsimagingmode()))
						,Double.parseDouble(String.valueOf(con.getPanplusimagingmode()))
						,Double.parseDouble(String.valueOf(con.getMsplusimagingmode()))
						,Double.parseDouble(String.valueOf(con.getTimetoreadonersisector()))					
						,Double.parseDouble(String.valueOf(con.getTimetofilloneddtsector()))
						,Double.parseDouble(String.valueOf(con.getAvgisualwritesector()))
						,Double.parseDouble(String.valueOf(con.getTimetoreadoneisualsector()))
						,con.getIsualfilesize().intValue()
						,con.getFilesectormargin().intValue()
						,Double.parseDouble(String.valueOf(con.getOutputdatarate()))
						,con.getVc1ratio().intValue()
						,con.getVc2ratio().intValue(),false);
				arg0.add(fwrev);
				this.instances.put(con.getId().longValue(), fwrev);
			}else{
				//System.out.println("============ROCSAT5");
				FwRecorderPSImp fwrev =new FwRecorderPSImp(con.getId().intValue()
						,con.getRecorderid().intValue()
						,con.getPsid().intValue()
						,con.getCapacity().intValue()
						,con.getRsicapacity().intValue()
						,con.getIsualcapacity().intValue()
						,con.getDdtcapacity().intValue()
						,con.getCompressionratiocount().intValue()
						,Double.parseDouble(String.valueOf(con.getPan1_5()))
						,Double.parseDouble(String.valueOf(con.getMs1_5()))
						,Double.parseDouble(String.valueOf(con.getPan7_5()))
						,Double.parseDouble(String.valueOf(con.getMs7_5()))
						,Double.parseDouble(String.valueOf(con.getTimetoreadonersisector()))					
						,Double.parseDouble(String.valueOf(con.getTimetofilloneddtsector()))
						,Double.parseDouble(String.valueOf(con.getAvgisualwritesector()))
						,Double.parseDouble(String.valueOf(con.getTimetoreadoneisualsector()))
						,con.getIsualfilesize().intValue()
						,con.getFilesectormargin().intValue()
						,Double.parseDouble(String.valueOf(con.getOutputdatarate()))
						,con.getVc1ratio().intValue()
						,con.getVc2ratio().intValue(),false);
				
				fwrev.SetMSMediumImageModeTime(Double.parseDouble(String.valueOf(con.getMs3_75())));
				fwrev.SetPanMediumImageModeTime(Double.parseDouble(String.valueOf(con.getPan3_75())));
				fwrev.SetPanNoneImageModeTime(Double.parseDouble(String.valueOf(con.getPan1())));
				fwrev.SetMSNoneImageModeTime(Double.parseDouble(String.valueOf(con.getMs1())));
				fwrev.SetDDTFileMargin(Double.parseDouble(String.valueOf(con.getDDTFileMargin())));
				
				fwrev.SetDDTOutputDataRate(Double.parseDouble(String.valueOf(con.getDDTOutputdatarate())));
				fwrev.SetDDTVC1Ratio(con.getDDTVc1ratio().intValue());
				fwrev.SetDDTVC2Ratio(con.getDDTVc2ratio().intValue());
				fwrev.SetDDTMSNoneImageModeTime(Double.parseDouble(String.valueOf(con.getDDTMs1())));
				fwrev.SetDDTMSMediumImageModeTime(Double.parseDouble(String.valueOf(con.getDDTMs3_75())));
				fwrev.SetDDTMSLowImageModeTime(Double.parseDouble(String.valueOf(con.getDDTMs1_5())));
				fwrev.SetDDTMSPlusImageModeTime(Double.parseDouble(String.valueOf(con.getDDTMs7_5())));
				
				fwrev.SetDDTPanNoneImageModeTime(Double.parseDouble(String.valueOf(con.getDDTPan1())));
				fwrev.SetDDTPanMediumImageModeTime(Double.parseDouble(String.valueOf(con.getDDTPan3_75())));
				fwrev.SetDDTPanLowImageModeTime(Double.parseDouble(String.valueOf(con.getDDTPan1_5())));
				fwrev.SetDDTPANPlusImageModeTime(Double.parseDouble(String.valueOf(con.getDDTPan7_5())));
				
				
				arg0.add(fwrev);
				this.instances.put(con.getId().longValue(), fwrev);
			}	
			
		}
		
	}

	@Override
	public boolean Store(FwRecorderPSImpVector imps,FwRecorderPSImpVector delimps) {
		logger.info(" RecorderPSImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwRecorderPSImp imp) {
		logger.info(" RecorderPSImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);		
	}

	@Override
	public void Delete(FwRecorderPSImp arg0) {
		logger.info(" RecorderPSImpFactory.class , super.Delete(arg0); no imp...");
		
		
	}

	@Override
	public void Delete(FwRecorderPSImpVector arg0) {
		logger.info(" RecorderPSImpFactory.class , super.Delete(arg0); no imp...");
				
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" RecorderPSImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" RecorderPSImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" RecorderPSImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}

	@Override
	public void CancelAll() {
		logger.info(" RecorderPSImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	

}
