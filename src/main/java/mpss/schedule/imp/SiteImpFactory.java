package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.SiteDao;
import mpss.common.jpa.Site;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwSiteImp;
import mpss.schedule.base.FwSiteImpFactory;
import mpss.schedule.base.FwSiteImpVector;
import mpss.schedule.base.GlSite;
import mpss.schedule.base.GlSite.AliasType;
import mpss.schedule.base.GlSite.CCSType;
import mpss.schedule.base.GlSite.Type;

public class SiteImpFactory extends FwSiteImpFactory implements UIDB<Site> {

	private Map<Long, FwSiteImp> instancesByID = new HashMap<Long, FwSiteImp>();
	private Map<String, FwSiteImp> instancesByName = new HashMap<String, FwSiteImp>();		
	private long myID;
	private static SiteDao siteDao=JPAInit.getBuilder().getSiteDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	
	public boolean create(Site object){
		if(siteDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Site object){
		 return siteDao.update(object);
	 }

	 public boolean delete(Site object){
		 return siteDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return siteDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		this.instancesByID.clear();
		this.instancesByName.clear();
		siteDao.reset();
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" SiteImpFactory.class , super.GetAll(arg0); no imp");
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" SiteImpFactory.class , super.GetAll(arg0, arg1); no imp");
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" SiteImpFactory.class , return super.Delete(arg0); no imp");
		return true;
		
	}

	

	@Override
	public FwSiteImp GetOne(String arg0) {
		//System.out.println("call SiteImpFactory.GetOne(.) , name="+arg0); //ok
		
		if (!this.instancesByName.containsKey(arg0)) {
			logger.info("Site name:" + arg0 + " is not in database.");
			return null;
		}

		myID = this.instancesByName.get(arg0).GetID();		
		return this.instancesByName.get(arg0);
	}

	@Override
	public void DoLoadAll() {
		//System.out.println("call SiteImpFactory.DoLoadAll()");
		List<Site> sList = siteDao.getAll();
		for (Site s : sList) {
			
			long lid = s.getId();
			int id = (int) lid;

			
			FwSiteImp imp = this.getMyImp(s);

			if (!this.instancesByID.containsKey(id)) {
				this.instancesByID.put((long) id, imp);
			}

			if (!this.instancesByName.containsKey(s.getName())) {
				this.instancesByName.put(s.getName(), imp);
			}

		}
	}

	@Override
	public FwSiteImp DoGetOne(String arg0, AliasType arg1) {
		//System.out.println("call SiteImpFactory.DoGetOne(.,.)");
		FwSiteImp imp =null;

		String aliasType = "";
		if (arg1.equals(AliasType.CCSName))
			aliasType = "ccsname";
		else if (arg1.equals(AliasType.Name1Alias))
			aliasType = "name1";
		else if (arg1.equals(AliasType.Name2Alias))
			aliasType = "name2";
		else if (arg1.equals(AliasType.Name4Alias))
			aliasType = "name4";

		List<Site> ls = siteDao.getByAliasType(aliasType, arg0);
		if (ls.size() > 0) {

			Site s = ls.get(0);
			imp = this.getMyImp(s);
		}
		
		if (!this.instancesByID.containsKey((long)imp.GetID())) {
			this.instancesByID.put((long)imp.GetID(), imp);
		}

		if (!this.instancesByName.containsKey(imp.GetName())) {
			this.instancesByName.put(imp.GetName(), imp);
		}
		
		this.myID = imp.GetID();
		return imp;
	}



	@Override
	public FwSiteImp GetNewOne(String name, short arg1, String name4, String name2, String name1, String ccsname, String antenna, CCSType ccsType, Type type, boolean isRTS) {
		//System.out.println("call SiteImpFactory.GetNewOne(...)");
		this.myID = siteDao.getNextId();

		FwSiteImp imp = new FwSiteImp((int) myID, name, arg1, name4, name2, name1, ccsname, antenna, ccsType, type, isRTS, true);
		this.instancesByID.put(myID, imp);
		this.instancesByName.put(name, imp);		
		return imp;
	}

	@Override
	public void GetAll(FwSiteImpVector arg0) {
		//System.out.println("call SiteImpFactory.GetAll(.)");
		List<Site> sList = siteDao.getAllSites();
		
		for (Site s : sList) {
			long lid = s.getId();
			int id = (int) lid;
			if (!this.instancesByID.containsKey(id)) {
				FwSiteImp imp = this.getMyImp(s);
				arg0.add(imp);
				this.instancesByID.put((long) id, imp);
				this.instancesByName.put(s.getName(), imp);
			}else{
				FwSiteImp imp =this.instancesByID.get(id);
				arg0.add(imp);
			}

		}
	}

	@Override
	public void DoGetAll(FwSiteImpVector imps, boolean GetDeleted) {
		//System.out.println("call SiteImpFactory.DoGetAll(.,.)");
		List<Site> sList = siteDao.getAll();
		for (Site s : sList) {
			long lid = s.getId();
			int id = (int) lid;
			
			if (!this.instancesByID.containsKey(id)) {
				FwSiteImp imp = this.getMyImp(s);
				imps.add(imp);
				this.instancesByID.put((long) id, imp);
				this.instancesByName.put(s.getName(), imp);
			}else{
				FwSiteImp imp =this.instancesByID.get(id);
				imps.add(imp);
			}
		}

	}

	@Override
	public void DoGetAll(FwSiteImpVector imps) {
		//System.out.println("call SiteImpFactory.DoGetAll(.)"); //OK
		List<Site> sList = siteDao.getAllSites();
		
		for (Site s : sList) {
			long lid = s.getId();
			int id = (int) lid;	
			
			if (!this.instancesByID.containsKey(id)) {
				FwSiteImp imp = this.getMyImp(s);
				imps.add(imp);
				this.instancesByID.put((long) id, imp);
				this.instancesByName.put(s.getName(), imp);
			}else{
				FwSiteImp imp =this.instancesByID.get(id);
				imps.add(imp);
			}
			
		}
		
	}

	@Override
	public FwSiteImp GetID(int arg0) {
		//System.out.println("call SiteImpFactory.GetID(.) id="+arg0); 
		if (instancesByID.containsKey(Long.valueOf(arg0))) {
			return  (FwSiteImp) instancesByID.get(Long.valueOf(arg0));
		} else {			
			Site s = siteDao.getSiteById(arg0);
			if(s==null){
				logger.info("SiteImpFactory GetID(" + arg0 + ") is null");
				return null;
			}else{
				FwSiteImp imp = this.getMyImp(s);
				this.instancesByID.put(s.getId(), imp);	
				return imp;
			}
		}				
		
	}

	
	
	
	
	@Override
	public synchronized void delete() {
		logger.info(" SiteImpFactory.class , super.delete(); no imp");		
	}

	

	@Override
	protected FwSiteImp AllocateImp(int arg0, String name, short arg2,String name4, String name2, String name1, String ccsname,	String antenna, CCSType ccsType, Type type, boolean isRTS) {
		logger.info(" SiteImpFactory.class , return super.AllocateImp(....); no imp");
		return super.AllocateImp(arg0, name, arg2, name4, name2, name1, ccsname,antenna, ccsType, type, isRTS);
		
	}

	@Override
	protected FwSiteImp CreateOne(String name, short arg1, String name4,String name2, String name1, String ccsname, String antenna,CCSType ccsType, Type type, boolean isRTS) {
		logger.info(" SiteImpFactory.class , return super.CreateOne(...); no imp");
		return super.CreateOne(name, arg1, name4, name2, name1, ccsname, antenna,ccsType, type, isRTS);
		
	}

	@Override
	protected FwSiteImp AllocateImp(int arg0, String name, short arg2,String name4, String name2, String name1, String ccsname,String antenna, CCSType ccsType, Type type, boolean isRTS,boolean newflag) {
		logger.info(" SiteImpFactory.class , return super.AllocateImp(...); no imp");
		return super.AllocateImp(arg0, name, arg2, name4, name2, name1, ccsname,antenna, ccsType, type, isRTS, newflag);
		
	}

	@Override
	protected void DoCleanup() {
		logger.info(" SiteImpFactory.class , super.DoCleanup(); no imp");
		
	}

	@Override
	public boolean CheckName(String arg0) {
		logger.info(" SiteImpFactory.class , return super.CheckName(arg0); no imp");
		return true;
		
	}

	@Override
	public boolean Rename(String arg0, String arg1) {
		logger.info(" SiteImpFactory.class , return super.Rename(arg0, arg1); no imp");
		return true;
		
	}

	@Override
	public void AddNew(FwSiteImp arg0) {
		logger.info(" SiteImpFactory.class , super.AddNew(arg0); no imp");
		
	}

	@Override
	public void AddDeleted(FwSiteImp arg0) {
		logger.info(" SiteImpFactory.class , super.AddDeleted(arg0); no imp");
	}

	@Override
	public void DoCancel(FwSiteImp arg0) {
		logger.info(" SiteImpFactory.class , super.DoCancel(arg0); no imp");
	}

	@Override
	public void DoCancelAll() {
		logger.info(" SiteImpFactory.class , super.DoCancelAll(); no imp");			
	}

	@Override
	public void Delete(FwSiteImpVector arg0) {
		logger.info(" SiteImpFactory.class , super.Delete(arg0); no imp");
	}

	@Override
	public void Delete(FwSiteImp arg0) {
		logger.info(" SiteImpFactory.class , super.Delete(arg0); no imp");		
	}

	@Override
	public boolean Store(FwSiteImpVector vec) {
		logger.info(" SiteImpFactory.class , return super.Store(vec); no imp");
		return true;		
	}

	@Override
	public boolean Store(FwSiteImpVector savem, FwSiteImpVector deleteem) {
		logger.info(" SiteImpFactory.class , return super.Store(savem, deleteem); no imp");
		return true;		
	}

	@Override
	public boolean Store(FwSiteImp imp) {
		logger.info(" SiteImpFactory.class , return super.Store(imp); no imp");
		return true;
		
	}

	@Override
	public boolean Store() {
		logger.info(" SiteImpFactory.class , return super.Store(); no imp");
		return true;		
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" SiteImpFactory.class , return super.DeleteAll(); no imp");
		return true;		
	}

	@Override
	public void CancelAll() {
		logger.info(" SiteImpFactory.class , super.CancelAll(); no imp");			
	}

	@Override
	public void Cancel(FwSiteImp arg0) {
		logger.info(" SiteImpFactory.class , super.Cancel(arg0); no imp");				
	}

	@Override
	public void AddID(FwSiteImp arg0) {
		logger.info(" SiteImpFactory.class , super.AddID(arg0); no imp");				
	}

	@Override
	public void Cleanup() {
		logger.info(" SiteImpFactory.class , super.Cleanup(); no imp");			
	}

	private FwSiteImp getMyImp(Site s){
		//System.out.println("call SiteImpFactory.getMyImp(.),site id="+s.getId() + "site name="+s.getName());		
		long lid = s.getId();
		int id = (int) lid;

		int siteNo = s.getSitenumber();
		short ssiteNo = (short) siteNo;

		String ccstype = s.getCcstype();
		GlSite.CCSType ccsTypeEnum = null;
		if (ccstype.equalsIgnoreCase("AFSCN")){			
			ccsTypeEnum = CCSType.AFSCN;
		}else if (ccstype.equalsIgnoreCase("FCCSTYPE")){			
			ccsTypeEnum = CCSType.FCCSTYPE;
		}else if (ccstype.equalsIgnoreCase("LCCSTYPE")){			
			ccsTypeEnum = CCSType.LCCSTYPE;
		}else if (ccstype.equalsIgnoreCase("NOAA")){			
			ccsTypeEnum = CCSType.NOAA;
		}else if (ccstype.equalsIgnoreCase("OTHER")){			
			ccsTypeEnum = CCSType.OTHER;
		}

		String type = s.getSitetype();
		GlSite.Type typeEnum = null;
		if (type.equalsIgnoreCase("FTYPE")){			
			typeEnum = GlSite.Type.FTYPE;
		}else if (type.equalsIgnoreCase("LTYPE")){			
			typeEnum = Type.LTYPE;
		}else if (type.equalsIgnoreCase("SBAND")){			
			typeEnum = Type.SBAND;
		}else if (type.equalsIgnoreCase("XBAND")){			
			typeEnum = Type.XBAND;
		}
		
		return  new FwSiteImp(id, s.getName(), ssiteNo, s.getName4(), s.getName2(), s.getName1(), s.getCcsname() == null ? "" : s.getCcsname(), s.getAntennasideid() == null ? "" : s.getAntennasideid(), ccsTypeEnum, typeEnum, s.getIsrts() == 1 ? true : false, false);
		
		
	}

}
