package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.SessionsatarlxrefDao;
import mpss.common.jpa.Sessionsatarlxref;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwSessionSatARLImp;
import mpss.schedule.base.FwSessionSatARLImpFactory;
import mpss.schedule.base.FwSessionSatARLImpVector;
import mpss.schedule.base.GlARL;
import mpss.schedule.base.GlARL.Type;

public class SessionSatARLImpFactory  extends FwSessionSatARLImpFactory implements UIDB<Sessionsatarlxref>{

	private Map<Long, FwSessionSatARLImp> instances = new HashMap<Long, FwSessionSatARLImp>();

	private static SessionsatarlxrefDao sessionsatarlxrefDao=JPAInit.getBuilder().getSessionsatarlxrefDao();
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
		
	public boolean create(Sessionsatarlxref object){
		if(sessionsatarlxrefDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Sessionsatarlxref object){
		 return sessionsatarlxrefDao.update(object);
	 }

	 public boolean delete(Sessionsatarlxref object){
		 return sessionsatarlxrefDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return sessionsatarlxrefDao.doSql(jpsql);	
	 }
	
	@Override
	public synchronized void delete() {
		logger.info(" SessionSatARLImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}
	
	public void releaseFwImp(){
		instances.clear();
		sessionsatarlxrefDao.reset();
		
	}

	@Override
	public boolean Store(FwSessionSatARLImpVector imps,FwSessionSatARLImpVector delimps) {
		logger.info(" SessionSatARLImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwSessionSatARLImp imp) {
		logger.info(" SessionSatARLImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public void Delete(FwSessionSatARLImp arg0) {
		logger.info(" SessionSatARLImpFactory.class , super.Delete(arg0); no imp...");	
	}

	@Override
	public void Delete(FwSessionSatARLImpVector arg0) {
		logger.info(" SessionSatARLImpFactory.class , super.Delete(arg0); no imp...");		
	}
	

	@Override
	public FwSessionSatARLImp GetOne(int SessionId, int SatelliteId, int ARLId, Type Type) {
		logger.info("call SessionSatARLImpFactory.GetOne(.,.,.,.)");
		return this.CreateOne(SessionId, SatelliteId, ARLId, Type);
	}

	@Override
	public FwSessionSatARLImp CreateOne(int SessionId, int SatelliteId, int ARLID, Type Type) {
		logger.info("call SessionSatARLImpFactory.CreateOne(.,.,.,.)");
		long newid = sessionsatarlxrefDao.getNextId();
		FwSessionSatARLImp imp = new FwSessionSatARLImp();
		imp.SetID((int) newid);
		imp.SetSessionID(SessionId);
		imp.SetSatelliteID(SatelliteId);
		imp.SetARLID(ARLID);
		imp.SetARLType(Type);
		
		if (imp != null) {
			this.instances.put(newid, imp);
		}

		return imp;
	}

	@Override
	public FwSessionSatARLImp GetID(int arg0) {
		logger.info("call SessionSatARLImpFactory.GetID()");
		FwSessionSatARLImp imp = null;
		
		if (instances.containsKey(Long.valueOf(arg0))) {
			imp = (FwSessionSatARLImp) instances.get(Long.valueOf(arg0));
		} else {
			Sessionsatarlxref s = sessionsatarlxrefDao.getById(arg0);
			if (s != null) {

				imp = new FwSessionSatARLImp();
				long lid = s.getId();
				int id = (int) lid;

				imp.SetID(id);
				imp.SetARLID(s.getArlid());
				this.SetMyARLType(imp, s.getArltype());
				imp.SetSatelliteID(s.getSatelliteid());
				imp.SetSessionID(s.getSessionid());

				if (imp != null) {
					this.instances.put(lid, imp);
				}
			}
		}

		if (imp == null) {
			logger.info("SessionSatARLImpFactory GetID(" + arg0 + ") is null");
		}

		return imp;
	}

	@Override
	public void GetAll(FwSessionSatARLImpVector arg0, int parentID) {		
		List<Sessionsatarlxref> dList = sessionsatarlxrefDao.getBySessionId(parentID);
		
		for (Sessionsatarlxref s : dList){
			FwSessionSatARLImp imp = new FwSessionSatARLImp();
			long lid = s.getId();
			int id = (int) lid;

			imp.SetID(id);
			imp.SetARLID(s.getArlid());
			this.SetMyARLType(imp, s.getArltype());
			imp.SetSatelliteID(s.getSatelliteid());
			imp.SetSessionID(s.getSessionid());
			
			instances.put(lid, imp);
			arg0.add(imp);
			
		}
	}

	public void SetMyARLType(FwSessionSatARLImp imp, String type) {		
		if (type.equalsIgnoreCase("DELETE")) {
			imp.SetARLType(GlARL.Type.DEL);
		} else if (type.equalsIgnoreCase("EXTFILE")) {
			imp.SetARLType(GlARL.Type.EXTFILE);
		} else if (type.equalsIgnoreCase("FTYPE")) {
			imp.SetARLType(GlARL.Type.FTYPE);
		} else if (type.equalsIgnoreCase("MON")) {
			imp.SetARLType(GlARL.Type.MON);
		} else if (type.equalsIgnoreCase("UPLINK")) {
			imp.SetARLType(GlARL.Type.UPLINK);
		} else if (type.equalsIgnoreCase("RTCMD")) {
			imp.SetARLType(GlARL.Type.RTCMD);
		} else if (type.equalsIgnoreCase("MISCPROC")) {
			imp.SetARLType(GlARL.Type.MISCPROC);
		} else if (type.equalsIgnoreCase("RSI")) {
			imp.SetARLType(GlARL.Type.RSI);
		} else if (type.equalsIgnoreCase("ISUAL")) {
			imp.SetARLType(GlARL.Type.ISUAL);
		} else if (type.equalsIgnoreCase("ISUALPROC")) {
			imp.SetARLType(GlARL.Type.ISUALPROC);
		} else if (type.equalsIgnoreCase("MANEUVER")) {
			imp.SetARLType(GlARL.Type.MANEUVER);
		} else if (type.equalsIgnoreCase("XCROSS")) {
			imp.SetARLType(GlARL.Type.XCROSS);
		} else if (type.equalsIgnoreCase("TOY")) {
			imp.SetARLType(GlARL.Type.TOY);
		} else if (type.equalsIgnoreCase("LTYPE")) {
			imp.SetARLType(GlARL.Type.LTYPE);
		}

	}
	
}
