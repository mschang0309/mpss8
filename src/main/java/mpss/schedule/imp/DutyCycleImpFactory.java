package mpss.schedule.imp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.DutycycleDao;
import mpss.common.jpa.Dutycycle;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwDutyCycleImp;
import mpss.schedule.base.FwDutyCycleImpFactory;
import mpss.schedule.base.FwDutyCycleImpVector;
import mpss.schedule.base.GlARL;
import mpss.schedule.base.GlARL.SubType;
import mpss.schedule.base.GlARL.Type;

public class DutyCycleImpFactory extends FwDutyCycleImpFactory  implements UIDB<Dutycycle>{
	
	private Map<Long, FwDutyCycleImp> instances = new HashMap<Long, FwDutyCycleImp>();
	private static DutycycleDao dutycycleo=JPAInit.getBuilder().getDutycycleDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	public boolean create(Dutycycle object){
		if(dutycycleo.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Dutycycle object){
		 return dutycycleo.update(object);
	 }

	 public boolean delete(Dutycycle object){
		return dutycycleo.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return dutycycleo.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		this.instances.clear();
		dutycycleo.reset();
	}

	@Override
	public synchronized void delete() {
		logger.info(" DutyCycleImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwDutyCycleImp GetOne(int parentID, Type type, SubType subType,long cycles) {
		logger.info(" DutyCycleImpFactory.class , return super.GetOne(parentID, type, subType, cycles); no imp");
		return super.GetOne(parentID, type, subType, cycles);
		
	}

	@Override
	protected FwDutyCycleImp CreateOne(int parentID, Type type,SubType subType, long cycles) {
		logger.info(" DutyCycleImpFactory.class , return super.CreateOne(parentID, type, subType, cycles); no imp");
		return super.CreateOne(parentID, type, subType, cycles);
		
	}

	@Override
	public FwDutyCycleImp GetID(int arg0) {
		logger.info(" DutyCycleImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);
		
	}

	@Override
	public void GetAll(FwDutyCycleImpVector arg0, int parentID) {
		logger.info("call DutyCycleImpFactory.class , super.GetAll(arg0, parentID) ~~~~ ,id="+String.valueOf(parentID));
		
			List<Dutycycle> list = dutycycleo.getBySatPsId(parentID);
			Iterator<Dutycycle> item = list.iterator();
			while (item.hasNext()) {
				Dutycycle con = (Dutycycle) item.next();
				FwDutyCycleImp fwrev = new FwDutyCycleImp(con.getId().intValue(),
						parentID,  getTypeByString(con.getActivitytype()), getSubTypeByString(con.getActivitysubtype()),Long.valueOf(con.getDutycycles()),false);
				arg0.add(fwrev);
				this.instances.put(con.getId().longValue(), fwrev);
			}	
		
	}
	
	protected Type getTypeByString(String type) {		
		if (type.equals("FTYPE")) {
			return GlARL.Type.FTYPE;
		} else if (type.equals("MON")) {
			return GlARL.Type.MON;
		} else if (type.equals("UPLINK")) {
			return GlARL.Type.UPLINK;
		} else if (type.equals("RTCMD")) {
			return GlARL.Type.RTCMD;
		} else if (type.equals("MISCPROC")) {
			return GlARL.Type.MISCPROC;
		} else if (type.equals("EXTFILE")) {
			return GlARL.Type.EXTFILE;
		} else if (type.equals("RSI")) {
			return GlARL.Type.RSI;
		} else if (type.equals("ISUAL")) {
			return GlARL.Type.ISUAL;
		} else if (type.equals("ISUALPROC")) {
			return GlARL.Type.ISUALPROC;
		} else if (type.equals("MANEUVER")) {
			return GlARL.Type.MANEUVER;
		} else if (type.equals("DEL")) {
			return GlARL.Type.DEL;
		} else if (type.equals("XCROSS")) {
			return GlARL.Type.XCROSS;
		} else if (type.equals("TOY")) {
			return GlARL.Type.TOY;
		} else if (type.equals("LTYPE")) {
			return GlARL.Type.LTYPE;
		} else {
			return GlARL.Type.RSI;
		}
		
	}
	
	protected SubType getSubTypeByString(String type) {		
		if (type.equals("FSUBTYPE")) {
			return GlARL.SubType.FSUBTYPE;
		} else if (type.equals("NOSUBTYPE")) {
			return GlARL.SubType.NOSUBTYPE;
		} else if (type.equals("RECORD")) {
			return GlARL.SubType.RECORD;
		} else if (type.equals("PBK")) {
			return GlARL.SubType.PLAYBACK;
		} else if (type.equals("DDT")) {
			return GlARL.SubType.DDT;
		} else if (type.equals("LSUBTYPE")) {
			return GlARL.SubType.LSUBTYPE;
		} else {
			return GlARL.SubType.DDT;
		}
		
	}

	@Override
	public void Delete(FwDutyCycleImp arg0) {
		logger.info(" DutyCycleImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwDutyCycleImpVector arg0) {
		logger.info(" DutyCycleImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" DutyCycleImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" DutyCycleImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" DutyCycleImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}

	@Override
	public void CancelAll() {
		logger.info(" DutyCycleImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	
	
	
}
