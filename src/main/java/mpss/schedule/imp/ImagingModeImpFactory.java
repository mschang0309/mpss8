package mpss.schedule.imp;

import java.util.*;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.*;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwImagingModeImp;
import mpss.schedule.base.FwImagingModeImpFactory;
import mpss.schedule.base.FwImagingModeImpVector;
import mpss.common.jpa.*;

public class ImagingModeImpFactory extends FwImagingModeImpFactory  implements UIDB<Imagingmode> {

	public static int myVal = 100;

	private Map<Long, FwImagingModeImp> instances = new HashMap<Long, FwImagingModeImp>();

	private static ImagingmodeDao imagingmodeDao=JPAInit.getBuilder().getImagingmodeDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	public boolean create(Imagingmode object){
		if(imagingmodeDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Imagingmode object){
		 return imagingmodeDao.update(object);
	 }

	 public boolean delete(Imagingmode object){
		 return imagingmodeDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return imagingmodeDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		this.instances.clear();
		imagingmodeDao.reset();
	}

	@Override
	public FwImagingModeImp GetOne(String instrument, int modeID, String mode, String modeShort, double ratio) {
		logger.info("call ImagingModeImpFactory.GetOne(.,.,.,.)");
		long newid = imagingmodeDao.getNextId();

		FwImagingModeImp imp = new FwImagingModeImp((int) newid, instrument, modeID, mode, modeShort, ratio, true);
		// this.AddID(imp);

		if (imp != null) {
			this.instances.put(newid, imp);
		}

		return imp;
	}

	@Override
	public FwImagingModeImp Get(int modeID, String instrument) {		
		Imagingmode i = imagingmodeDao.getImagingModeByInstNModeid(instrument, modeID);

		FwImagingModeImp imp = new FwImagingModeImp();
		long lid = i.getId();
		int id = (int) lid;
		imp.SetID(id);
		imp.SetCompressionRatio(i.getCompressionratio());
		imp.SetInstrument(i.getInstrument());
		imp.SetMode(i.getModename());
		imp.SetModeID(i.getModeid());
		imp.SetModeShort(i.getModeshort());
		

		return imp;
	}

	@Override
	public void GetAll(FwImagingModeImpVector arg0, String instrument) {
		logger.info("call ImagingModeImpFactory.GetAll(.,.)");
		List<Imagingmode> list = imagingmodeDao.getImagingModeByInst(instrument);

		for (Imagingmode i : list) {
			// System.out.println("Mode Name: " + i.getModename());
			FwImagingModeImp imp = new FwImagingModeImp();

			long lid = i.getId();
			int id = (int) lid;
			imp.SetID(id);
			imp.SetCompressionRatio(i.getCompressionratio());
			imp.SetInstrument(i.getInstrument());
			imp.SetMode(i.getModename());
			imp.SetModeID(i.getModeid());
			imp.SetModeShort(i.getModeshort());
			arg0.add(imp);
		}

	}

	@Override
	public void GetAll(FwImagingModeImpVector arg0) {
		logger.info("call ImagingModeImpFactory.GetAll(.)");
		this.DoGetAll(arg0);
	}

	@Override
	public void DoGetAll(FwImagingModeImpVector arg0) {
		logger.info("call ImagingModeImpFactory.DoGetAll(.)");
		List<Imagingmode> list = imagingmodeDao.getAll();
		
		for (Imagingmode i : list) {
			// System.out.println("Mode Name: " + i.getModename());
			FwImagingModeImp imp = new FwImagingModeImp();

			long lid = i.getId();
			int id = (int) lid;
			imp.SetID(id);
			imp.SetCompressionRatio(i.getCompressionratio());
			imp.SetInstrument(i.getInstrument());
			imp.SetMode(i.getModename());
			imp.SetModeID(i.getModeid());
			imp.SetModeShort(i.getModeshort());
			arg0.add(imp);
		}
		
	}

	public boolean Store(FwImagingModeImpVector savem, FwImagingModeImpVector deleteem) {
		try {
			logger.info("call ImagingModeImpFactory.Store(.,.)");
			long size = deleteem.size();
			for (int i = 0; i < (int) size; i++) {
				FwImagingModeImp imp = deleteem.get(i);
				Imagingmode img = imagingmodeDao.getImagingModeById(Long.valueOf(imp.GetID()));
				imagingmodeDao.delete(img);

			}

			size = savem.size();
			for (int i = 0; i < (int) size; i++) {
				FwImagingModeImp imp = savem.get(i);
				Imagingmode img = new Imagingmode();

				img.setId(Long.valueOf(imp.GetID()));
				img.setCompressionratio((float) imp.GetCompressionRatio());
				img.setInstrument(imp.GetInstrument());
				img.setModeid(imp.GetModeID());
				img.setModename(imp.GetMode());
				img.setModeshort(imp.GetModeShort());

				if (imp.IsChanged()) {
					imagingmodeDao.update(img);

				} else if (imp.IsNew()) {
					imagingmodeDao.create(img);

				}
			}
		} catch (Exception ex) {
			logger.info("Store(FwImagingModeImpVector savem, FwImagingModeImpVector deleteem) ex:" + ex.toString());
			return false;
		}

		return true;
	}

	@Override
	public void Delete(FwImagingModeImpVector arg0) {
		logger.info(" ImagingModeImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwImagingModeImp arg0) {
		logger.info(" ImagingModeImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" ImagingModeImpFactory.class , return super.DeleteAll(); no imp");
		return super.DeleteAll();
		
	}

	@Override
	public void CancelAll() {
		logger.info(" ImagingModeImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public void Cancel(FwImagingModeImp arg0) {
		logger.info(" ImagingModeImpFactory.class , super.Cancel(arg0); no imp");
		super.Cancel(arg0);
		
	}

	@Override
	public void AddID(FwImagingModeImp arg0) {
		logger.info(" ImagingModeImpFactory.class , super.AddID(arg0); no imp");
		super.AddID(arg0);
		
	}

	@Override
	public void Cleanup() {
		logger.info(" ImagingModeImpFactory.class , super.Cleanup(); no imp");
		super.Cleanup();
		
	}

	@Override
	public boolean Store() {

		try {			
			logger.info("call ImagingModeImpFactory.Store()");
			Iterator<Long> itor = instances.keySet().iterator();
			while (itor.hasNext()) {

				long key = (Long) itor.next();
				FwImagingModeImp imp = instances.get(key);

				Imagingmode img = new Imagingmode();

				img.setId(Long.valueOf(imp.GetID()));
				img.setCompressionratio((float) imp.GetCompressionRatio());
				img.setInstrument(imp.GetInstrument());
				img.setModeid(imp.GetModeID());
				img.setModename(imp.GetMode());
				img.setModeshort(imp.GetModeShort());

				if (imp.IsChanged()) {
					imagingmodeDao.update(img);

				} else if (imp.IsNew()) {
					imagingmodeDao.create(img);

				}

			}

		} catch (Exception ex) {
			logger.info("Store() ex=" + ex.toString());
			return false;
		}

		return true;
	}

	@Override
	public FwImagingModeImp GetID(int arg0) {
		logger.info("call ImagingModeImpFactory.GetID(.)");
		FwImagingModeImp imp = null;
		if (instances.containsKey(Long.valueOf(arg0))) {
			imp = (FwImagingModeImp) instances.get(Long.valueOf(arg0));
		} else {
			Imagingmode i = imagingmodeDao.getImagingModeById(Long.valueOf(arg0));
			
			imp = new FwImagingModeImp();
			long lid = i.getId();
			int id = (int) lid;
			imp.SetID(id);
			imp.SetCompressionRatio(i.getCompressionratio());
			imp.SetInstrument(i.getInstrument());
			imp.SetMode(i.getModename());
			imp.SetModeID(i.getModeid());
			imp.SetModeShort(i.getModeshort());
			
			if (imp != null) {
				this.instances.put(lid, imp);
			}
		}

		if (imp == null) {
			logger.info("ImagingModeImpFactory GetID(" + arg0 + ") is null");

		}

		return imp;
	}

	@Override
	public boolean Store(FwImagingModeImp imp) {
		logger.info("call ImagingModeImpFactory.Store(.)");
		try {
			Imagingmode img = new Imagingmode();

			img.setId(Long.valueOf(imp.GetID()));
			img.setCompressionratio((float) imp.GetCompressionRatio());
			img.setInstrument(imp.GetInstrument());
			img.setModeid(imp.GetModeID());
			img.setModename(imp.GetMode());
			img.setModeshort(imp.GetModeShort());

			if (imp.IsChanged()) {
				imagingmodeDao.update(img);

			} else if (imp.IsNew()) {
				imagingmodeDao.create(img);

			}

		} catch (Exception ex) {
			logger.info("Store(FwImagingModeImp arg0) ex:" + ex.toString());
			return false;
		}
		return true;
	}

	@Override
	public boolean Store(FwImagingModeImpVector vec) {
		logger.info("call ImagingModeImpFactory.Store(v)");
		try {
			long size = vec.size();
			for (int i = 0; i < (int) size; i++) {
				FwImagingModeImp imp = vec.get(i);
				Imagingmode img = new Imagingmode();

				img.setId(Long.valueOf(imp.GetID()));
				img.setCompressionratio((float) imp.GetCompressionRatio());
				img.setInstrument(imp.GetInstrument());
				img.setModeid(imp.GetModeID());
				img.setModename(imp.GetMode());
				img.setModeshort(imp.GetModeShort());

				if (imp.IsChanged()) {
					imagingmodeDao.update(img);

				} else if (imp.IsNew()) {
					imagingmodeDao.create(img);

				}
			}
		} catch (Exception ex) {
			logger.info("Store(FwImagingModeImpVector vec) ex:" + ex.toString());
			return false;
		}

		return true;
	}

	@Override
	public synchronized void delete() {
		logger.info(" ImagingModeImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	protected FwImagingModeImp CreateOne(String instrument, int modeID,String mode, String modeShort, double ratio) {
		logger.info(" ImagingModeImpFactory.class , return super.CreateOne(instrument, modeID, mode, modeShort, ratio); no imp");
		return super.CreateOne(instrument, modeID, mode, modeShort, ratio);
		
	}

	@Override
	protected void DoCleanup() {
		logger.info(" ImagingModeImpFactory.class , super.DoCleanup(); no imp");
		super.DoCleanup();
		
	}

	@Override
	protected void LoadAll(FwImagingModeImpVector arg0, String arg1) {
		logger.info("call ImagingModeImpFactory.class , super.LoadAll(arg0, arg1); String="+arg1);
		List<Imagingmode> items =imagingmodeDao.getImagingModeByInst(arg1);
		
		for (Imagingmode i : items) {
			
			FwImagingModeImp imp = new FwImagingModeImp();
			long lid = i.getId();
			int id = (int) lid;
			imp.SetID(id);
			imp.SetCompressionRatio(i.getCompressionratio());
			imp.SetInstrument(i.getInstrument());
			imp.SetMode(i.getModename());
			imp.SetModeID(i.getModeid());
			imp.SetModeShort(i.getModeshort());
			arg0.add(imp);
			this.instances.put(lid, imp);
		}
	}

	@Override
	protected void AddNew(FwImagingModeImp arg0) {
		logger.info(" ImagingModeImpFactory.class , super.AddNew(arg0); ");
		this.instances.put((long)arg0.GetID(),arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" ImagingModeImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" ImagingModeImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" ImagingModeImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}

	@Override
	protected void AddDeleted(FwImagingModeImp arg0) {
		logger.info(" ImagingModeImpFactory.class , super.AddDeleted(arg0); no imp");
		super.AddDeleted(arg0);
		
	}

	@Override
	protected void DoCancel(FwImagingModeImp arg0) {
		logger.info(" ImagingModeImpFactory.class , super.DoCancel(arg0); no imp");
		super.DoCancel(arg0);
		
	}

	@Override
	protected void DoCancelAll() {
		logger.info(" ImagingModeImpFactory.class , super.DoCancelAll(); no imp");
		super.DoCancelAll();
		
	}
	

}
