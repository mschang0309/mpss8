package mpss.schedule.imp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.GlobalparametersDao;
import mpss.common.jpa.Globalparameter;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwParametersImp;
import mpss.schedule.base.FwParametersImpFactory;
import mpss.schedule.base.FwParametersImpVector;


public class GlobalParametersImpFactory extends FwParametersImpFactory implements UIDB<Globalparameter>{

	
	//private FwParametersImp instances;
	private Map<Long, FwParametersImp> instancesByID = new HashMap<Long, FwParametersImp>();
	//private FwParametersImpVector myPmVec = new FwParametersImpVector();

	private static GlobalparametersDao globalparametersDao=JPAInit.getBuilder().getGlobalparametersDao();
	// private static ParametersetDao parametersetDao;
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public boolean create(Globalparameter object){
		if(globalparametersDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Globalparameter object){
		return globalparametersDao.update(object);
	 }

	 public boolean delete(Globalparameter object){
		return globalparametersDao.delete(object);	
	 }
	
	 
	 public boolean doSql(String jpsql){
		return globalparametersDao.doSql(jpsql);	
	 }
	public void releaseFwImp(){
		this.instancesByID.clear();
		globalparametersDao.reset();
	}

	public GlobalParametersImpFactory() {
		//super();
		//System.out.println("call ParametersImpFactory.contractor() get all global parameters"); //OK
		List<Globalparameter> dList = globalparametersDao.getAll();
		for(int i =0 ; i<dList.size();i++){			
			Globalparameter gp = dList.get(i);
			long lid = gp.getId();
			int id = (int) lid;
			int iprepassqualifier = gp.getPrepassqualifier();
			short prepassqualifier = (short) iprepassqualifier;
			FwParametersImp temp =new FwParametersImp(id, Long.valueOf(gp.getColdschedulinglimit()), Long.valueOf(gp.getHotschedulinglimit()), Double.valueOf(gp.getOrbminmaxel()), Long.valueOf(gp
					.getOrbminduration()), Long.valueOf(gp.getPbduration()), prepassqualifier, Long.valueOf(gp.getPassstarttimelead()), Long.valueOf(gp.getPbstarttimelead()), Long.valueOf(gp
					.getPassstarttimelag()), Long.valueOf(gp.getPbstarttimelag()), gp.getCommenttorange(), gp.getUsernameid(), gp.getPbfacilityid(), gp.getEquipgroupid(), gp.getStandardpbcommentid(),
					gp.getPrepbduration(), gp.getSessionduration(), false);
			
			instancesByID.put(lid, temp);
		}		

	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" GlobalParametersImpFactory.class , super.GetAll(arg0); no imp");
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" GlobalParametersImpFactory.class , super.GetAll(arg0, arg1); no imp");	
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" GlobalParametersImpFactory.class , return super.Delete(arg0); no imp");
		return true;		
	}

	@Override
	public FwParametersImp GetID(int arg0) {
		logger.info("call GlobalParametersImpFactory.GetID(.)");
		FwParametersImp imp = null;

		if (instancesByID.containsKey(Long.valueOf(arg0))) {
			imp = (FwParametersImp) instancesByID.get(Long.valueOf(arg0));
		} else {

			Globalparameter gp = globalparametersDao.getById(arg0);
			long lid = gp.getId();
			int id = (int) lid;
			int iprepassqualifier = gp.getPrepassqualifier();
			short prepassqualifier = (short) iprepassqualifier;

			imp = new FwParametersImp(id, Long.valueOf(gp.getColdschedulinglimit()), Long.valueOf(gp.getHotschedulinglimit()), Double.valueOf(gp.getOrbminmaxel()),
					Long.valueOf(gp.getOrbminduration()), Long.valueOf(gp.getPbduration()), prepassqualifier, Long.valueOf(gp.getPassstarttimelead()), Long.valueOf(gp.getPbstarttimelead()),
					Long.valueOf(gp.getPassstarttimelag()), Long.valueOf(gp.getPbstarttimelag()), gp.getCommenttorange(), gp.getUsernameid(), gp.getPbfacilityid(), gp.getEquipgroupid(),
					gp.getStandardpbcommentid(), gp.getPrepbduration(), gp.getSessionduration());

			if (imp != null) {
				this.instancesByID.put(lid, imp);
			}
		}

		if (imp == null) {
			logger.info("GlobalParametersImpFactory GetID(" + arg0 + ") is null");
		}		
		return imp;
	}

	@Override
	public FwParametersImp GetOne() {
		logger.info("call GlobalParametersImpFactory.GetOne()"); //OK
		FwParametersImp imp = null;
		if (this.instancesByID.size() == 0) {
			long newid = globalparametersDao.getNextId();
			int id = (int) newid;
			imp = new FwParametersImp(id, Long.valueOf(0), Long.valueOf(0), Double.valueOf(0), Long.valueOf(0), Long.valueOf(0), Short.valueOf("0"), Long.valueOf(0), Long.valueOf(0), Long.valueOf(0), Long.valueOf(0), "",        "",           "",           "",            "",       Long.valueOf(0), 0, false);
			this.instancesByID.put(newid, imp);			
			return imp;
		} else {			
			Iterator<Long> keyItor = this.instancesByID.keySet().iterator();
			while(keyItor.hasNext()){
				long key = (Long) keyItor.next();
				//return the first entry with an ID != GlDummyID (-1)
				if (key != -1){
					imp = this.instancesByID.get(key);					
				}
			}			
		}		
		return imp;
	}
	
	@Override
	public void GetAll(FwParametersImpVector arg0) {	
		logger.info("call GlobalParametersImpFactory.GetAll()");
		List<Globalparameter> sList = globalparametersDao.getAll();
		for (Globalparameter gp : sList) {

			long lid = gp.getId();
			int id = (int) lid;
			int iprepassqualifier = gp.getPrepassqualifier();
			short prepassqualifier = (short) iprepassqualifier;

			FwParametersImp imp = new FwParametersImp(id, Long.valueOf(gp.getColdschedulinglimit()), Long.valueOf(gp.getHotschedulinglimit()), Double.valueOf(gp.getOrbminmaxel()), Long.valueOf(gp
					.getOrbminduration()), Long.valueOf(gp.getPbduration()), prepassqualifier, Long.valueOf(gp.getPassstarttimelead()), Long.valueOf(gp.getPbstarttimelead()), Long.valueOf(gp
					.getPassstarttimelag()), Long.valueOf(gp.getPbstarttimelag()), gp.getCommenttorange(), gp.getUsernameid(), gp.getPbfacilityid(), gp.getEquipgroupid(), gp.getStandardpbcommentid(),
					gp.getPrepbduration(), gp.getSessionduration());

			if (!this.instancesByID.containsKey(id)) {
				this.instancesByID.put((long) id, imp);
			}

			
			arg0.add(imp);

		}	
	}
	
	@Override
	protected FwParametersImp CreateOne(long ColdSchedulingLimit,
			long HotSchedulingLimit, double orbMinmaxel, long orbMinDuration,
			long PbDuration, short PrePassQualifier, long PassStartTimeLead,
			long PbStartTimeLead, long PassStartTimeLag, long PbStartTimeLag,
			String CommentToRange, String UserNameID, String PbFacilityID,
			String EquipGroupID, String StandardPbCommentID,
			long PrePbDuration, int SessionDuration) {
		logger.info(" GlobalParametersImpFactory.class , return super.CreateOne(...); no imp");
		return super.CreateOne(ColdSchedulingLimit, HotSchedulingLimit, orbMinmaxel,
				orbMinDuration, PbDuration, PrePassQualifier, PassStartTimeLead,
				PbStartTimeLead, PassStartTimeLag, PbStartTimeLag, CommentToRange,
				UserNameID, PbFacilityID, EquipGroupID, StandardPbCommentID,
				PrePbDuration, SessionDuration);
		
	}
	
	@Override
	public synchronized void delete() {
		logger.info(" GlobalParametersImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	

	@Override
	protected void DoCancelAll() {
		logger.info(" GlobalParametersImpFactory.class , super.DoCancelAll(); no imp");
		super.DoCancelAll();
		
	}

	@Override
	protected void DoCleanup() {
		logger.info(" GlobalParametersImpFactory.class , super.DoCleanup(); no imp");
		super.DoCleanup();
		
	}

	@Override
	protected void DoCancel(FwParametersImp arg0) {
		logger.info(" GlobalParametersImpFactory.class , super.DoCancel(arg0); no imp");
		super.DoCancel(arg0);
		
	}

	@Override
	protected void DoGetAll(FwParametersImpVector arg0) {
		logger.info(" GlobalParametersImpFactory.class , super.DoGetAll(arg0); no imp");
		super.DoGetAll(arg0);
		
	}

	

	@Override
	public void Delete(FwParametersImpVector arg0) {
		logger.info(" GlobalParametersImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwParametersImp arg0) {
		logger.info(" GlobalParametersImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public boolean Store(FwParametersImpVector vec) {
		logger.info(" GlobalParametersImpFactory.class , return super.Store(vec); no imp");
		return super.Store(vec);
		
	}

	@Override
	public boolean Store(FwParametersImpVector savem,FwParametersImpVector deleteem) {
		logger.info(" GlobalParametersImpFactory.class , return super.Store(savem, deleteem); no imp");
		return super.Store(savem, deleteem);
		
	}

	@Override
	public boolean Store(FwParametersImp imp) {
		logger.info(" GlobalParametersImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public boolean Store() {
		logger.info(" ParametersImpFactory.class , return super.Store(); no imp");
		return super.Store();
		
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" GlobalParametersImpFactory.class , return super.DeleteAll(); no imp");
		return super.DeleteAll();
		
	}

	@Override
	public void CancelAll() {
		logger.info(" GlobalParametersImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public void Cancel(FwParametersImp arg0) {
		logger.info(" GlobalParametersImpFactory.class , super.Cancel(arg0); no imp");
		super.Cancel(arg0);
		
	}

	@Override
	public void AddID(FwParametersImp arg0) {
		logger.info(" GlobalParametersImpFactory.class , super.AddID(arg0); no imp");
		super.AddID(arg0);
		
	}

	@Override
	public void Cleanup() {
		logger.info(" GlobalParametersImpFactory.class , super.Cleanup(); no imp");
		super.Cleanup();
		
	}

	@Override
	protected void AddNew(FwParametersImp arg0) {
		logger.info(" GlobalParametersImpFactory.class , super.AddNew(arg0); no imp");
		super.AddNew(arg0);
		
	}

	@Override
	protected void AddDeleted(FwParametersImp arg0) {
		logger.info(" GlobalParametersImpFactory.class , super.AddDeleted(arg0); no imp");
		super.AddDeleted(arg0);
		
	}

	@Override
	public void DoLoadAll() {
		logger.info("call GlobalParametersImpFactory.DoLoadAll()");
		//this.GetAll(myPmVec);
	}

	

}
