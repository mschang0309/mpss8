package mpss.schedule.imp;

import mpss.configFactory;
import mpss.common.dao.SessionDao;
import mpss.common.jpa.Session;
import mpss.schedule.JPAInit;
import mpss.schedule.Schedule;
import mpss.schedule.UIDB;
import mpss.schedule.Schedule.SchType;
import mpss.schedule.base.FwSessionImp;
import mpss.schedule.base.FwSessionImpFactory;
import mpss.schedule.base.FwSessionImpVector;
import mpss.schedule.base.GlTime;
import mpss.schedule.base.GlTimeRange;
import mpss.util.timeformat.DateUtil;

import java.sql.Timestamp;
import java.util.*;

import org.apache.log4j.Logger;

public class SessionImpFactory extends FwSessionImpFactory  implements UIDB<Session>{

	private Map<Long, FwSessionImp> instancesByID = new HashMap<Long, FwSessionImp>();
	private Map<String, FwSessionImp> instancesByName = new HashMap<String, FwSessionImp>();
	private static SessionDao sessionDao=JPAInit.getBuilder().getSessionDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());
		
	public boolean create(Session object){
		if(sessionDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Session object){
		 
		return sessionDao.update(object);
	 }

	 public boolean delete(Session object){
		return sessionDao.delete(object);	
	 } 
	
	 public boolean doSql(String jpsql){
		 return sessionDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		instancesByID.clear();		
		instancesByName.clear();
		sessionDao.reset();
	}
	
	public Collection<Session> getSessionByTimeRange(String starttime,String endtime){		
		return sessionDao.getSessionByRangeNBranch(starttime, endtime, 0);
	}

	@Override
	public FwSessionImp GetNewOne(String arg0, GlTimeRange arg1, String arg2) {
		logger.info("call SessionImpFactory.GetNewOne(.,.,.)");
		long newid = sessionDao.getNextId();

		FwSessionImp imp = new FwSessionImp((int) newid, arg0, arg1, arg2,
				new GlTime(), false, false, false, false, 0, false, true);

		if (imp != null) {
			this.instancesByID.put(newid, imp);
			this.instancesByName.put(arg0, imp);
		}

		return imp;
	}

	@SuppressWarnings("unused")
	@Override
	public FwSessionImp DoGetOne(GlTime arg0) {
		logger.info("call SessionImpFactory.DoGetOne(.)");
		String time = arg0.asString("%Y-%m-%d %H:%M:%S");

		FwSessionImp imp = new FwSessionImp();
		Collection<Session> col = sessionDao.getSessionByTimeNBranch(time, 0);
		ArrayList<Session> al = new ArrayList<Session>(col);
		for (int i = 0; i < al.size();i++) {			
			Session s = (Session) al.get(i);
			long lid = s.getId();
			int id = (int) lid;
			
			if(instancesByID.containsKey(Long.valueOf(lid))){
				return (FwSessionImp)instancesByID.get(Long.valueOf(lid));
			}else{
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date(s.getStarttime().getTime()));
				long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

				cal.setTime(new Date(s.getEndtime().getTime()));
				long sec2 = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

				cal.setTime(new Date(s.getLastsave().getTime()));
				long sec3 = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

				imp.SetID(id);
				imp.SetName(s.getName());
				imp.SetPSID(s.getPsid());
				imp.SetBranchFlag(s.getIsbranch() == 1 ? true : false);
				imp.SetTimeRange(new GlTimeRange(new GlTime(sec1), new GlTime(sec2)));
				imp.SetColdScheduled(s.getColdscheduled() == 1 ? true : false);
				imp.SetNeedsValidation(s.getNeedsvalidation() == 1 ? true : false);
				imp.SetDescription(s.getDescription() == null ? "" : s
						.getDescription());
				imp.SetAcqsumLoaded(s.getAcqsumloaded() == 1 ? true : false);
				imp.SetLastSave(new GlTime(sec3));
				imp.SetNominalApplied(s.getNominalapplied() == 1 ? true : false);
				
				this.instancesByID.put(Long.valueOf(imp.GetID()), imp);
				this.instancesByName.put(imp.GetName(), imp);
				return imp;

			}
			
		}
		logger.info("call SessionImpFactory.DoGetOne(.) no session");
		return null;
			
	}

	@Override
	public void DoGetAll(FwSessionImpVector arg0) {		
		List<Session> sessList = sessionDao.getAll();

		for (Session s : sessList) {
			long lid = s.getId();
			int id = (int) lid;
			
			if(instancesByID.containsKey(Long.valueOf(lid))){
				arg0.add((FwSessionImp)instancesByID.get(Long.valueOf(lid)));
			}else{
				FwSessionImp imp = new FwSessionImp();
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date(s.getStarttime().getTime()));
				long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

				cal.setTime(new Date(s.getEndtime().getTime()));
				long sec2 = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

				cal.setTime(new Date(s.getLastsave().getTime()));
				long sec3 = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

				imp.SetID(id);
				imp.SetName(s.getName());
				imp.SetPSID(s.getPsid());
				imp.SetBranchFlag(s.getIsbranch() == 1 ? true : false);
				imp.SetTimeRange(new GlTimeRange(new GlTime(sec1), new GlTime(sec2)));
				imp.SetColdScheduled(s.getColdscheduled() == 1 ? true : false);
				imp.SetNeedsValidation(s.getNeedsvalidation() == 1 ? true : false);
				imp.SetDescription(s.getDescription() == null ? "" : s
						.getDescription());
				imp.SetAcqsumLoaded(s.getAcqsumloaded() == 1 ? true : false);
				imp.SetLastSave(new GlTime(sec3));
				imp.SetNominalApplied(s.getNominalapplied() == 1 ? true : false);
				
                this.instancesByID.put(Long.valueOf(id), imp);				
			    this.instancesByName.put(s.getName(), imp);
				
				arg0.add(imp);
			}
			
		}
	}

	@Override
	public void GetAll(FwSessionImpVector arg0) {		
		this.DoGetAll(arg0);
	}

	@Override
	public GlTime GetFirstSessionStart(String[] INOUT) {
		logger.info("call SessionImpFactory.GetFirstSessionStart(.)");
		Date d = sessionDao.getMinStarttime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		long sec = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

		return new GlTime(sec);
	}

	@Override
	public GlTime GetNextSessionStart(String[] INOUT) {
		logger.info("call SessionImpFactory.GetNextSessionStart(.)");
		Date d = sessionDao.getMaxEndtime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		long sec = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

		return new GlTime(sec);
	}

	@Override
	public GlTime GetFirstSessionStart() {
		logger.info("call SessionImpFactory.GetFirstSessionStart()");
		Date d = sessionDao.getMinStarttime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		long sec = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

		return new GlTime(sec);
	}

	@Override
	public GlTime GetNextSessionStart() {
		logger.info("call SessionImpFactory.GetNextSessionStart()");
		Date d = sessionDao.getMaxEndtime();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		long sec = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

		return new GlTime(sec);
	}

	@Override
	public FwSessionImp GetID(int arg0) {				
		if (instancesByID.containsKey(Long.valueOf(arg0))) {
			return (FwSessionImp) instancesByID.get(Long.valueOf(arg0));
		} else {
			Session s = sessionDao.getSessionById(arg0);
			
			if(s==null){
				logger.info("SessionImpFactory GetID(" + arg0 + ") is null");
				return null;
			}

			FwSessionImp imp = new FwSessionImp();
			long lid = s.getId();
			int id = (int) lid;
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date(s.getStarttime().getTime()));
			long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

			cal.setTime(new Date(s.getEndtime().getTime()));
			long sec2 = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

			cal.setTime(new Date(s.getLastsave().getTime()));
			long sec3 = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

			imp.SetID(id);
			imp.SetName(s.getName());
			imp.SetPSID(s.getPsid());
			imp.SetBranchFlag(s.getIsbranch() == 1 ? true : false);
			imp.SetTimeRange(new GlTimeRange(new GlTime(sec1), new GlTime(sec2)));
			//GlTimeRange gr = new GlTimeRange(new GlTime(sec1), new GlTime(sec2));
			//System.out.println(gr.AsString());
			imp.SetColdScheduled(s.getColdscheduled() == 1 ? true : false);
			imp.SetNeedsValidation(s.getNeedsvalidation() == 1 ? true : false);
			imp.SetDescription(s.getDescription() == null ? "" : s
					.getDescription());
			imp.SetAcqsumLoaded(s.getAcqsumloaded() == 1 ? true : false);
			imp.SetLastSave(new GlTime(sec3));
			imp.SetNominalApplied(s.getNominalapplied() == 1 ? true : false);

			
			this.instancesByID.put(Long.valueOf(lid), imp);
			this.instancesByName.put(s.getName(), imp);
			return imp;
			
		}		
		
	}

	public void DoLoadAll() {
		
		List<Session> sessList = sessionDao.getAll();

		for (Session s : sessList) {
			
			long lid = s.getId();
			int id = (int) lid;
			if (instancesByID.containsKey(Long.valueOf(lid))) {
				
			} else {
				
				FwSessionImp imp = new FwSessionImp();
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date(s.getStarttime().getTime()));
				long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

				cal.setTime(new Date(s.getEndtime().getTime()));
				long sec2 = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

				cal.setTime(new Date(s.getLastsave().getTime()));
				long sec3 = DateUtil.getClSeconds(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DATE),cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));

				imp.SetID(id);
				imp.SetName(s.getName());
				imp.SetPSID(s.getPsid());
				imp.SetBranchFlag(s.getIsbranch() == 1 ? true : false);
				imp.SetTimeRange(new GlTimeRange(new GlTime(sec1), new GlTime(sec2)));
				//GlTimeRange gr = new GlTimeRange(new GlTime(sec1), new GlTime(sec2));
				imp.SetColdScheduled(s.getColdscheduled() == 1 ? true : false);
				imp.SetNeedsValidation(s.getNeedsvalidation() == 1 ? true : false);
				imp.SetDescription(s.getDescription() == null ? "" : s.getDescription());
				imp.SetAcqsumLoaded(s.getAcqsumloaded() == 1 ? true : false);
				imp.SetLastSave(new GlTime(sec3));
				imp.SetNominalApplied(s.getNominalapplied() == 1 ? true : false);

				
				this.instancesByID.put(Long.valueOf(id), imp);
				this.instancesByName.put(s.getName(), imp);
				
			}

			

		}

	}

	@Override
	public FwSessionImp GetOne(String arg0) {
		DoLoadAll();
		if (this.instancesByName.containsKey(arg0) == false) {
			logger.info("Session name:" + arg0 + " is not in database.");
			return null;
		}

		return this.instancesByName.get(arg0);
	}
	
	
	public Session getSessionByID(int sessionid){
		return sessionDao.getSessionById(sessionid);
	}

	@Override
	public synchronized void delete() {
		logger.info(" SessionImpFactory.class , super.delete(); no imp");
	}

	@Override
	protected FwSessionImp CreateOne(String arg0, GlTimeRange arg1, String arg2) {
		logger.info(" SessionImpFactory.class , return super.CreateOne(arg0, arg1, arg2); no imp return null");
		return null;

	}

	@Override
	protected void DoCleanup() {
		logger.info(" SessionImpFactory.class , super.DoCleanup(); no imp");
	}

	@Override
	public boolean CheckName(String arg0) {
		logger.info(" SessionImpFactory.class , return super.CheckName(arg0); no imp");
		return true;

	}

	@Override
	public boolean Rename(String arg0, String arg1) {
		logger.info(" SessionImpFactory.class , return super.Rename(arg0, arg1); no imp");
		return true;

	}

	@Override
	public void AddNew(FwSessionImp arg0) {
		logger.info(" SessionImpFactory.class , super.AddNew(arg0); no imp");
	}

	@Override
	public void AddDeleted(FwSessionImp arg0) {
		logger.info(" SessionImpFactory.class , super.AddDeleted(arg0); no imp");
	}

	@Override
	public void DoCancel(FwSessionImp arg0) {
		logger.info(" SessionImpFactory.class , super.DoCancel(arg0); no imp");
	}

	@Override
	public void DoCancelAll() {
		logger.info(" SessionImpFactory.class , super.DoCancelAll(); no imp");
	}

	@Override
	public void Delete(FwSessionImpVector arg0) {
		logger.info(" SessionImpFactory.class , super.Delete(arg0); no imp");
	}

	@Override
	public void Delete(FwSessionImp arg0) {
		logger.info(" SessionImpFactory.class , super.Delete(arg0); no imp");
	}

	@Override
	public boolean Store(FwSessionImpVector vec) {
		logger.info(" SessionImpFactory.class , return super.Store(vec); no imp");
		return true;
	}

	@Override
	public boolean Store(FwSessionImpVector savem, FwSessionImpVector deleteem) {
		logger.info(" SessionImpFactory.class , return super.Store(savem, deleteem); no imp");
		return true;
	}

	@Override
	public boolean Store(FwSessionImp imp) {		
		try {
			String starttime = imp.GetTimeRange().GetStartTime().asString("%Y-%m-%d %H:%M:%S");
			String endtime = imp.GetTimeRange().GetEndTime().asString("%Y-%m-%d %H:%M:%S");
			Date dStart = DateUtil.stringToDate(starttime,"yyyy-MM-dd HH:mm:ss");
			Date dEnd = DateUtil.stringToDate(endtime, "yyyy-MM-dd HH:mm:ss");
			Timestamp tsStart = DateUtil.dateToTimestamp(dStart);
			Timestamp tsEnd = DateUtil.dateToTimestamp(dEnd);

			Session session = new Session();
			session.setId(Long.valueOf(imp.GetID()));
			session.setName(imp.GetName());
			session.setPsid(Integer.valueOf(imp.GetPSID()));
			session.setIsbranch(imp.GetBranchFlag() == true ? 1 : 0);
			session.setStarttime(tsStart);
			session.setEndtime(tsEnd);
			session.setColdscheduled(imp.GetColdScheduled()==true?1:0);
			session.setNeedsvalidation(imp.GetNeedsValidation()==true?1:0);
			session.setDescription(imp.GetDescription());
			session.setAcqsumloaded(imp.GetAcqsumLoaded()==true?1:0);
			session.setLastsave(DateUtil.dateToTimestamp(new Date()));
			
			session.setNominalapplied(imp.GetNominalApplied()==true?1:0);
			if(Schedule.type==SchType.COLD){				
				sessionDao.update(session);
			}else{
				
				
			}
			logger.info("SessionImpFactory Store=======modify some");

		} catch (Exception ex) {
			logger.info("Store(FwExtentImpVector arg0) ex:"+ ex.toString());
			return false;
		}

		return true;

	}

	@Override
	public void DoGetAll(FwSessionImpVector arg0, boolean arg1) {
		logger.info(" SessionImpFactory.class , super.DoGetAll(arg0, arg1); no imp");		
	}

	@Override
	public boolean Store() {
		logger.info(" SessionImpFactory.class , return super.Store(); no imp");
		return true;
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" SessionImpFactory.class , return super.DeleteAll(); no imp");
		return true;
	}

	@Override
	public void CancelAll() {
		logger.info(" SessionImpFactory.class , super.CancelAll(); no imp");		
	}

	@Override
	public void Cancel(FwSessionImp arg0) {
		logger.info(" SessionImpFactory.class , super.Cancel(arg0); no imp");
	}

	@Override
	public void AddID(FwSessionImp arg0) {
		logger.info(" SessionImpFactory.class , super.AddID(arg0); no imp");
	}

	@Override
	public void Cleanup() {
		logger.info(" SessionImpFactory.class , super.Cleanup(); no imp");
	}
	
	public Session getSessionByName(String name){
		return sessionDao.getSession(name);
	}
	
	public Session getSessionbyTime(Timestamp time, int isbranch, int satId){
		return sessionDao.getSessionbyTime(time, isbranch, satId);
	}

}
