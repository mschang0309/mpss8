package mpss.schedule.imp;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.ExtentActivityTimerangeDao;
import mpss.common.jpa.Extentactivitytimerange;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwBaseTimeRangeImp;
import mpss.schedule.base.FwBaseTimeRangeImpFactory;
import mpss.schedule.base.FwBaseTimeRangeImpVector;
import mpss.schedule.base.GlTime;
import mpss.schedule.base.GlTimeRange;
import mpss.util.timeformat.DateUtil;


public class ExtentActivityTimeRangeImpFactory extends FwBaseTimeRangeImpFactory  implements UIDB<Extentactivitytimerange>{
	
	
	private Map<Long, FwBaseTimeRangeImp> instances = new HashMap<Long, FwBaseTimeRangeImp>();

	private static ExtentActivityTimerangeDao extentactivitytimerangeDao=JPAInit.getBuilder().getExtentActivityTimerange();
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	public boolean create(Extentactivitytimerange object){
		if(extentactivitytimerangeDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Extentactivitytimerange object){
		 return extentactivitytimerangeDao.update(object);
	 }

	 public boolean delete(Extentactivitytimerange object){
		 return extentactivitytimerangeDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return extentactivitytimerangeDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		this.instances.clear();
		extentactivitytimerangeDao.reset();
	}

	@Override
	public synchronized void delete() {
		logger.info(" ExtentActivityTimeRangeImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public void GetAll(FwBaseTimeRangeImpVector arg0, int parentID) {
		//System.out.println(" ExtentActivityTimeRangeImpFactory.class , super.GetAll(arg0, parentID);");
		FwBaseTimeRangeImp imp = null;
		List<Extentactivitytimerange> lists = extentactivitytimerangeDao.getExtentByparentid(parentID);
		for (int i = 0; i < lists.size(); i++) {
			Extentactivitytimerange ext = lists.get(i);
			imp = new FwBaseTimeRangeImp();
			int lid = Long.valueOf(ext.getId()).intValue();
			imp.SetID(lid);
			imp.SetParentID(parentID);
			imp.SetStringData(ext.getStringdata());
			Calendar cal = Calendar.getInstance();
			
			cal.setTime(new Date(ext.getStarttime().getTime()));			
			long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

			cal.setTime(new Date(ext.getEndtime().getTime()));			
			long sec2 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

			imp.SetTimeRange(new GlTimeRange(new GlTime(sec1), new GlTime(sec2)));
			
			arg0.add(imp);
		}		
	}
	
	public ArrayList<Extentactivitytimerange> GetByExtentId(int parentID) {
		
		ArrayList<Extentactivitytimerange> ranges = new ArrayList<Extentactivitytimerange>();
		List<Extentactivitytimerange> lists = extentactivitytimerangeDao.getExtentByparentid4Copy(parentID);
		for (int i = 0; i < lists.size(); i++) {
			Extentactivitytimerange ext = lists.get(i);			
			ranges.add(ext);
		}	
		return ranges;
	}

	@Override
	public FwBaseTimeRangeImp GetOne(int parentID, GlTimeRange timerange,String StringData) {
		//System.out.println(" ExtentActivityTimeRangeImpFactory.class , return super.GetOne(parentID, TimeRange, StringData); no imp");
		
		return CreateOne(parentID,timerange,StringData);
		
//		Extentactivitytimerange extentactivitytimerange =extentactivitytimerangeDao.getExtentById(parentID);
//		if(extentactivitytimerange==null){
//			return CreateOne(parentID,timerange,StringData);
//		}else{
//			FwBaseTimeRangeImp info = new FwBaseTimeRangeImp();
//			long lid = extentactivitytimerange.getId();
//			int id = (int) lid;
//			info.SetID(id);
//			info.SetParentID(parentID);
//			info.SetTimeRange(timerange);
//			info.SetStringData(StringData);		
//			return info;
//		}
		
		
	}

	@Override
	protected FwBaseTimeRangeImp CreateOne(int parentID, GlTimeRange timerange,String StringData) {
		//System.out.println(" ExtentActivityTimeRangeImpFactory.class , return super.CreateOne(parentID, TimeRange, StringData); no imp");
		long newid = extentactivitytimerangeDao.getNextId();
		FwBaseTimeRangeImp info = new FwBaseTimeRangeImp((int)newid,parentID,timerange,StringData,true);
		return info;
		
	}
	
	public void insertExtentActivityTimeRange(Extentactivitytimerange timerange){
		long newid = extentactivitytimerangeDao.getNextId();
		timerange.setId(newid);
		create(timerange);		
	}

	@Override
	protected void LoadAll(FwBaseTimeRangeImpVector arg0, int parentID) {
		GetAll(arg0,parentID);
		
	}

	@Override
	public FwBaseTimeRangeImp GetID(int arg0) {
		logger.info(" ExtentActivityTimeRangeImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);
		
	}

	
	public boolean Store(FwBaseTimeRangeImpVector imps,FwBaseTimeRangeImpVector delimps) {
		logger.info(" ExtentActivityTimeRangeImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	
	public boolean Store(Extentactivitytimerange imp) {
		try {
		
			extentactivitytimerangeDao.create(imp);		
		return true;
		}catch (Exception ex) {
			logger.info("ExtentActivityTimeRangeImpFactory Store(FwBaseTimeRangeImp arg0) ex:=====================");
			return true;
		}
		
		
		
	}
	
	public boolean Store(FwBaseTimeRangeImp imp) {
		try {
		//System.out.println(" ExtentActivityTimeRangeImpFactory.class , return super.Store(imp); no imp");
		Extentactivitytimerange myimp = new Extentactivitytimerange();
		myimp.setId(Long.valueOf(imp.GetID()));
		myimp.setParentid(imp.GetParentID());
		
		String starttime = imp.GetTimeRange().GetStartTime().asString("%Y-%m-%d %H:%M:%S");
		System.out.println("starttime="+starttime);
		String endtime = imp.GetTimeRange().GetEndTime().asString("%Y-%m-%d %H:%M:%S");
		System.out.println("endtime="+endtime);
		Date dStart = DateUtil.stringToDate(starttime, "yyyy-MM-dd HH:mm:ss");
		Date dEnd = DateUtil.stringToDate(endtime, "yyyy-MM-dd HH:mm:ss");
		Timestamp tsStart = DateUtil.dateToTimestamp(dStart);
		Timestamp tsEnd =  DateUtil.dateToTimestamp(dEnd);
		
		myimp.setStarttime(tsStart);
		myimp.setEndtime(tsEnd);
		myimp.setStringdata(imp.GetStringData());
		
		extentactivitytimerangeDao.create(myimp);
		
		return true;
		}catch (Exception ex) {
			logger.info("ExtentActivityTimeRangeImpFactory Store(FwBaseTimeRangeImp arg0) ex:" + ex.toString());
			return true;
		}
		
		
		
	}

	@Override
	public void Delete(FwBaseTimeRangeImp arg0) {
		logger.info(" ExtentActivityTimeRangeImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwBaseTimeRangeImpVector arg0) {
		logger.info(" ExtentActivityTimeRangeImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" ExtentActivityTimeRangeImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" ExtentActivityTimeRangeImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" ExtentActivityTimeRangeImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}

	@Override
	public void CancelAll() {
		logger.info(" ExtentActivityTimeRangeImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}
	
	
	
	

	

	

}
