package mpss.schedule.imp;


import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.UplinkrequestDao;
import mpss.common.jpa.Uplinkrequest;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseARLEntryImp;
import mpss.schedule.base.FwBaseARLEntryImpVector;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwUplinkRequestsARLEntryImp;
import mpss.schedule.base.FwUplinkRequestsARLEntryImpFactory;
import mpss.schedule.base.GlDaySpec;
import mpss.schedule.base.GlLoad.Type;


public class UplinkProcImpFactory extends FwUplinkRequestsARLEntryImpFactory implements UIDB<Uplinkrequest>{
	
	private static UplinkrequestDao uplinkprocDao=JPAInit.getBuilder().getUplinkrequest();
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
    @Override
	public synchronized void delete() {
    	logger.info(" UplinkProcImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwUplinkRequestsARLEntryImp GetOne(int parentID, GlDaySpec Date,	long StartTime, long Duration, Type LoadType, String LoadSrcProc,String Comments) {
		logger.info(" UplinkProcImpFactory.class , return super.GetOne(parentID, Date, StartTime, Duration, LoadType, LoadSrcProc,Comments); no imp");
		return super.GetOne(parentID, Date, StartTime, Duration, LoadType, LoadSrcProc,	Comments);
		
	}

	@Override
	protected FwUplinkRequestsARLEntryImp CreateOne(int parent, GlDaySpec Date,long StartTime, long Duration, Type LoadType, String LoadSrcProc,String Comments) {
		logger.info(" UplinkProcImpFactory.class , return super.CreateOne(parent, Date, StartTime, Duration, LoadType,LoadSrcProc, Comments); no imp");
		return super.CreateOne(parent, Date, StartTime, Duration, LoadType,LoadSrcProc, Comments);
		
	}

	@Override
	public FwBaseARLEntryImp GetID(int arg0) {
		logger.info(" UplinkProcImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);
		
	}

	@Override
	public void GetAll(FwBaseARLEntryImpVector arg0, int parentID) {
		logger.info(" UplinkProcImpFactory.class , super.GetAll(arg0, parentID); no imp");
		super.GetAll(arg0, parentID);
		
	}

	@Override
	public void Delete(FwBaseARLEntryImp arg0) {
		logger.info(" UplinkProcImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwBaseARLEntryImpVector arg0) {
		logger.info(" UplinkProcImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void CancelAll() {
		logger.info(" UplinkProcImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public boolean Store(FwBaseARLEntryImpVector imps,FwBaseARLEntryImpVector delimps) {
		logger.info(" UplinkProcImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwBaseARLEntryImp imp) {
		logger.info(" UplinkProcImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" UplinkProcImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" UplinkProcImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" UplinkProcImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}
	
	
	public boolean create(Uplinkrequest object){ 
		if(uplinkprocDao.create(object)==null){
			return false;
		}else{
			return true; 
		}
	 }
	
	public Long createGetId(Uplinkrequest object){
		Uplinkrequest data  =uplinkprocDao.create(object);		
		return data.getId();		
	 }

	 public boolean update(Uplinkrequest object){
		 return uplinkprocDao.update(object);
	 }

	 public boolean delete(Uplinkrequest object){
		 return uplinkprocDao.delete(object);	
	 }

	 public boolean doSql(String jpsql){
		 return uplinkprocDao.doSql(jpsql);	
	 }

	
	 public void releaseFwImp(){
		 uplinkprocDao.reset();
	 }
	 
	 public HashMap<Long,Long> copyArl(int arlid,int new_arlid) {
			HashMap<Long,Long> uplinkmaps = new HashMap<Long,Long>();
			List<Uplinkrequest> lists = uplinkprocDao.getByArlId(arlid);
			for(Uplinkrequest info:lists){
				Long id  = info.getId();
				info.setId(null);			
				info.setArlid(new_arlid);			
				uplinkmaps.put(id, createGetId(info));	
			}
			return uplinkmaps;
		}
		

}
