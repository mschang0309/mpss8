package mpss.schedule.imp;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.ExternalactivityrequestinfoDao;
import mpss.common.jpa.Externalactivityrequestinfo;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.*;
import mpss.util.timeformat.DateUtil;
public class ExternalActivityRequestInfoImpFactory extends FwExternalActivityRequestInfoImpFactory implements UIDB<Externalactivityrequestinfo> {
			
	private static ExternalactivityrequestinfoDao externalactivityDao=JPAInit.getBuilder().getExternalActivityrequest();
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public boolean create(Externalactivityrequestinfo object){
		if(externalactivityDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Externalactivityrequestinfo object){
		 return true;
	 }

	 public boolean delete(Externalactivityrequestinfo object){
		 return true;
	 }
	
	public void releaseFwImp(){
		
	}
	
	public boolean doSql(String jpsql){
		return true;
	 }

	@Override
	public synchronized void delete() {
		logger.info(" ExternalActivityRequestInfoImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwExternalActivityRequestInfoImp GetOne(int ARLId, GlTimeRange tr,
			int branchID, String satName,
			mpss.schedule.base.GlInstrument.Type arg4) {
		logger.info(" ExternalActivityRequestInfoImpFactory.class , return super.GetOne(ARLId, tr, branchID, satName, arg4); ");
		
		Externalactivityrequestinfo ear_info=externalactivityDao.getByArlId(ARLId);
		FwExternalActivityRequestInfoImp info = new FwExternalActivityRequestInfoImp();
		if(ear_info==null){			
			return info;
		}
		
		
		if(ear_info!=null){
			
			info.SetID(ear_info.getId().intValue());
			info.SetARLID(ear_info.getArlid().intValue());
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date(ear_info.getStarttime().getTime()));			
			long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) , cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
			cal.setTime(new Date(ear_info.getEndtime().getTime()));			
			long sec2 =  DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) , cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
			
			info.SetTimeRange(new GlTimeRange(new GlTime(sec1), new GlTime(sec2)));			
			info.SetSatellite(ear_info.getSatellite());
			if(ear_info.getInstrument().equals("RSI")){
				info.SetInstrument(GlInstrument.Type.RSI);
			}else{
				info.SetInstrument(GlInstrument.Type.ISUAL);
			}
			
			info.SetBranchID(ear_info.getBranchid().intValue());			
			
		}
		return info;
		
		
	}

	@Override
	protected FwExternalActivityRequestInfoImp CreateOne(int ARLID,
			GlTimeRange tr, int branchID, String satName,
			mpss.schedule.base.GlInstrument.Type arg4) {
		logger.info(" ExternalActivityRequestInfoImpFactory.class , return super.CreateOne(ARLID, tr, branchID, satName, arg4);");
		
		long newid = externalactivityDao.getNextId();
		FwExternalActivityRequestInfoImp info = new FwExternalActivityRequestInfoImp();
		info.SetID((int) newid);
		info.SetARLID(ARLID);
		info.SetTimeRange(tr);
		info.SetSatellite(satName);
		info.SetInstrument(arg4);
		info.SetBranchID(branchID);
		
		return info;
		
	}

	@Override
	public FwExternalActivityRequestInfoImp GetID(int arg0) {
		logger.info(" ExternalActivityRequestInfoImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);
		
	}

	@Override
	public void GetAll(FwExternalActivityRequestInfoImpVector arg0, int parentID) {
		logger.info(" ExternalActivityRequestInfoImpFactory.class , super.GetAll(arg0, parentID); parentID = "+parentID);
		Externalactivityrequestinfo ear_info=externalactivityDao.getByArlId(parentID);
		
		if(ear_info!=null){
			FwExternalActivityRequestInfoImp info = new FwExternalActivityRequestInfoImp();
			info.SetID(ear_info.getId().intValue());
			info.SetARLID(ear_info.getArlid().intValue());
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date(ear_info.getStarttime().getTime()));			
			long sec1 = DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) , cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
			cal.setTime(new Date(ear_info.getEndtime().getTime()));			
			long sec2 =  DateUtil.getClSeconds(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) , cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
			
			info.SetTimeRange(new GlTimeRange(new GlTime(sec1), new GlTime(sec2)));			
			info.SetSatellite(ear_info.getSatellite());
			if(ear_info.getInstrument().equals("RSI")){
				info.SetInstrument(GlInstrument.Type.RSI);
			}else{
				info.SetInstrument(GlInstrument.Type.ISUAL);
			}
			
			info.SetBranchID(ear_info.getBranchid().intValue());
			arg0.add(info);
			
		}
		
		
		
	}

	@Override
	public void Delete(FwExternalActivityRequestInfoImp arg0) {
		logger.info(" ExternalActivityRequestInfoImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwExternalActivityRequestInfoImpVector arg0) {
		logger.info(" ExternalActivityRequestInfoImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}	
	
	public List<Long> getArlId(String starttime,String endtime) {
		return externalactivityDao.getArlIdByTime(starttime, endtime);
	}
	
	public Externalactivityrequestinfo getArlById(long arlid) {		
		return externalactivityDao.getByArlId(Long.valueOf(arlid).intValue());
	}

	
}
