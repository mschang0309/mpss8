package mpss.schedule.imp;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.SessionsitexrefDao;
import mpss.common.jpa.Sessionsitexref;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwSessionSiteImp;
import mpss.schedule.base.FwSessionSiteImpFactory;
import mpss.schedule.base.FwSessionSiteImpVector;


public class SessionSiteImpFactory extends FwSessionSiteImpFactory implements UIDB<Sessionsitexref> {

	private Map<Long, FwSessionSiteImp> instances = new HashMap<Long, FwSessionSiteImp>();

	private static SessionsitexrefDao sessionsitexrefDao=JPAInit.getBuilder().getSessionsitexrefDao();
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	public boolean create(Sessionsitexref object){
		if(sessionsitexrefDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Sessionsitexref object){
		 return sessionsitexrefDao.update(object);
	 }

	 public boolean delete(Sessionsitexref object){
		return sessionsitexrefDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		 return sessionsitexrefDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		this.instances.clear();
		sessionsitexrefDao.reset();
	}

	@Override
	public FwSessionSiteImp GetOne(int SessionId, int SiteId) {
		logger.info("call SessionSiteImpFactory.GetAll(.,.)");
		return this.CreateOne(SessionId, SiteId);
	}

	@Override
	public FwSessionSiteImp GetID(int arg0) {
		logger.info("call SessionSiteImpFactory.GetID(.)");
		FwSessionSiteImp imp= null;
		
		if (instances.containsKey(Long.valueOf(arg0))){
			imp = (FwSessionSiteImp)instances.get(Long.valueOf(arg0));
		} else {
			imp = new FwSessionSiteImp();
			Sessionsitexref s = sessionsitexrefDao.getById(arg0);
			
			long lid = s.getId();
			int id = (int) lid;
			
					
			imp.SetID(id);
			imp.SetSessionID(s.getSessionid());
			imp.SetSiteID(s.getSiteid());
			
			
			if (imp != null) {
				this.instances.put(lid, imp);
			}
		}
		
		
		if(imp==null){
			logger.info("SessionSiteImpFactory GetID(" + arg0 +") is null");
			
		}
		
		return imp;
	}

	@Override
	public void GetAll(FwSessionSiteImpVector arg0, int parentID) {
		//System.out.println("call SessionSiteImpFactory.GetAll(.,.)"); OK
		List<Sessionsitexref> dList = sessionsitexrefDao.getBySessionId(parentID);
		for (Sessionsitexref s : dList){
			long lid = s.getId();
			int id = (int) lid;
			FwSessionSiteImp imp = new FwSessionSiteImp();
			imp.SetID(id);
			imp.SetSessionID(s.getSessionid());
			imp.SetSiteID(s.getSiteid());
			instances.put(lid, imp);	
			arg0.add(imp);
		}
			
	}

	@Override
	protected FwSessionSiteImp CreateOne(int SessionId, int SiteId) {
		logger.info("call SessionSiteImpFactory.CreateOne(.,.)");
		long newid = sessionsitexrefDao.getNextId();
		FwSessionSiteImp imp = new FwSessionSiteImp();
		imp.SetID((int)newid);
		imp.SetSessionID(SessionId);
		imp.SetSiteID(SiteId);
		if (imp != null) {
			this.instances.put(newid, imp);
		}
		
		return imp;
	}

	@Override
	public synchronized void delete() {
		logger.info(" SessionSiteImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public boolean Store(FwSessionSiteImpVector imps,FwSessionSiteImpVector delimps) {
		logger.info(" SessionSiteImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwSessionSiteImp imp) {
		logger.info(" SessionSiteImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public void Delete(FwSessionSiteImp arg0) {
		logger.info(" SessionSiteImpFactory.class , super.Delete(arg0); no imp...");		
	}

	@Override
	public void Delete(FwSessionSiteImpVector arg0) {
		logger.info(" SessionSiteImpFactory.class , super.Delete(arg0); no imp...");		
	}

		

	

}
