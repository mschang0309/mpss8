package mpss.schedule.imp;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.UnobscuredTimeDao;
import mpss.common.jpa.Unobscuredtime;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwUnobscuredTimeImp;
import mpss.schedule.base.FwUnobscuredTimeImpFactory;
import mpss.schedule.base.FwUnobscuredTimeImpVector;
import mpss.schedule.base.GlTimeRange;


public class UnobscuredTimeImpFactory extends FwUnobscuredTimeImpFactory implements UIDB<Unobscuredtime>{
	
    private static UnobscuredTimeDao unobscuredtimeDao=JPAInit.getBuilder().getUnobscuredTime();
    private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	public boolean create(Unobscuredtime object){
		if(unobscuredtimeDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	 public boolean update(Unobscuredtime object){
		 return unobscuredtimeDao.update(object);
	 }

	 public boolean delete(Unobscuredtime object){
		 return unobscuredtimeDao.delete(object);	
	 }

	 public boolean doSql(String jpsql){
		 return unobscuredtimeDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		unobscuredtimeDao.reset();
	}

	@Override
	protected void finalize() {
		logger.info(" UnobscuredTimeImpFactory.class , super.finalize(); no imp");		
	}

	@Override
	public synchronized void delete() {
		logger.info(" UnobscuredTimeImpFactory.class , super.delete(); no imp");		
	}

	@Override
	public FwUnobscuredTimeImp GetOne(int arg0, GlTimeRange tr) {
		logger.info(" UnobscuredTimeImpFactory.class , return super.GetOne(arg0, tr); no imp");
		return null;
		
	}

	@Override
	protected FwUnobscuredTimeImp CreateOne(int arg0, GlTimeRange arg1) {
		logger.info(" UnobscuredTimeImpFactory.class , return super.CreateOne(arg0, arg1); no imp");
		return null;
		
	}

	@Override
	public FwUnobscuredTimeImp GetID(int arg0) {
		logger.info(" UnobscuredTimeImpFactory.class , return super.GetID(arg0); no imp");
		return null;		
	}

	@Override
	public void GetAll(FwUnobscuredTimeImpVector arg0, int parentID) {
		logger.info(" UnobscuredTimeImpFactory.class , super.GetAll(arg0, parentID); no imp");		
		
	}

	@Override
	public boolean Store(FwUnobscuredTimeImpVector imps,FwUnobscuredTimeImpVector delimps) {
		logger.info(" UnobscuredTimeImpFactory.class , return super.Store(imps, delimps); no imp");
		return true;
		
	}

	@Override
	public boolean Store(FwUnobscuredTimeImp imp) {
		logger.info(" UnobscuredTimeImpFactory.class , return super.Store(imp); no imp");
		return true;		
	}

	@Override
	public void Delete(FwUnobscuredTimeImp arg0) {
		logger.info(" UnobscuredTimeImpFactory.class , super.Delete(arg0); no imp...");		
	}

	@Override
	public void Delete(FwUnobscuredTimeImpVector arg0) {
		logger.info(" UnobscuredTimeImpFactory.class , super.Delete(arg0); no imp...");		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" UnobscuredTimeImpFactory.class , super.GetAll(arg0); no imp");
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" UnobscuredTimeImpFactory.class , super.GetAll(arg0, arg1); no imp");
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" UnobscuredTimeImpFactory.class , return super.Delete(arg0); no imp");
		return true;
		
	}

	@Override
	public void CancelAll() {
		logger.info(" UnobscuredTimeImpFactory.class , super.CancelAll(); no imp");
		
	}

	
	
	

}
