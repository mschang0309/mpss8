package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import mpss.configFactory;
import mpss.common.dao.RsiimagingrequestDao;
import mpss.common.jpa.Rsiimagingrequest;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.*;
import mpss.schedule.base.GlARL.SubType;
import mpss.schedule.base.GlRecorder.Rate;


public class RSIImagingARLEntryImpFactory extends FwRSIImagingARLEntryImpFactory implements UIDB<Rsiimagingrequest>{

	private Map<Long, FwRSIImagingARLEntryImp> instances = new HashMap<Long, FwRSIImagingARLEntryImp>();
	private static RsiimagingrequestDao rsiimagingDao=JPAInit.getBuilder().getRsiImagingDao();
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	public boolean create(Rsiimagingrequest object){
		if(rsiimagingDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }

	public Long createGetId(Rsiimagingrequest object){
		Rsiimagingrequest data  =rsiimagingDao.create(object);		
		return data.getId();		
	 }
	
	 public boolean update(Rsiimagingrequest object){
		 return rsiimagingDao.update(object);
	 }

	 public boolean delete(Rsiimagingrequest object){
		 return rsiimagingDao.delete(object);	
	 }
	 
	 public boolean doSql(String jpsql){
		return rsiimagingDao.doSql(jpsql);	
	 }
	

	public void releaseFwImp(){
		this.instances.clear();
		rsiimagingDao.reset();
	}
	
	@Override
	public synchronized void delete() {
		logger.info(" RSIImagingARLEntryImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwRSIImagingARLEntryImp GetOne(int parentID, GlDaySpec date,
			long StartTime, long Duration, int modeID, String station,
			String elevation, String azimuth, int orbit, int trackgi,
			int gridjs, int gridje, int gridk, double startLat, double stopLat,
			int imagingType, SubType ssrStatus, Rate panCompRatio,
			Rate msCompRatio, String videoPanGain, String videoMb1Gain,
			String videoMb2Gain, String videoMb3Gain, String videoMb4Gain,
			String Comments) {
		logger.info(" RSIImagingARLEntryImpFactory.class , return super.GetOne(...); no imp");
		return super.GetOne(parentID, date, StartTime, Duration, modeID, station,
				elevation, azimuth, orbit, trackgi, gridjs, gridje, gridk, startLat,
				stopLat, imagingType, ssrStatus, panCompRatio, msCompRatio,
				videoPanGain, videoMb1Gain, videoMb2Gain, videoMb3Gain, videoMb4Gain,
				Comments);
		
	}

	@Override
	protected FwRSIImagingARLEntryImp CreateOne(int parent, GlDaySpec date,
			long StartTime, long Duration, int modeID, String station,
			String elevation, String azimuth, int orbit, int trackgi,
			int gridjs, int gridje, int gridk, double startLat, double stopLat,
			int imagingType, SubType ssrStatus, Rate panCompRatio,
			Rate msCompRatio, String videoPanGain, String videoMb1Gain,
			String videoMb2Gain, String videoMb3Gain, String videoMb4Gain,
			String Comments) {
		logger.info(" RSIImagingARLEntryImpFactory.class , return super.CreateOne(...); no imp");
		return super.CreateOne(parent, date, StartTime, Duration, modeID, station,
				elevation, azimuth, orbit, trackgi, gridjs, gridje, gridk, startLat,
				stopLat, imagingType, ssrStatus, panCompRatio, msCompRatio,
				videoPanGain, videoMb1Gain, videoMb2Gain, videoMb3Gain, videoMb4Gain,
				Comments);
		
	}
	
	public FwRSIImagingARLEntryImp GetJavaID(int arg0) {
		if(instances.containsKey((long)arg0)){			
			return this.instances.get((long)arg0);
		}else{
			return null;
		}
		
		
	}

	@Override
	public FwBaseARLEntryImp GetID(int arg0) {		
		if(instances.containsKey((long)arg0)){			
			return this.instances.get((long)arg0);
		}
		Rsiimagingrequest item =rsiimagingDao.getRsiImagingrequest(arg0);
		FwRSIImagingARLEntryImp fw= new FwRSIImagingARLEntryImp();
		fw.SetID(arg0);
		fw.SetARLID(item.getArlid().intValue());
		fw.SetStartTime(item.getStarttime().intValue());
		fw.SetDuration(item.getDuration().intValue());
		fw.SetImagingMode(item.getImagingmode().intValue());
		if(item.getElevation()==null){
			fw.SetElevation("0");
		}else{
			fw.SetElevation(item.getElevation());
		}
		
		if(item.getAzimuth()==null){
			fw.SetAzimuth("0");
		}else{
			fw.SetAzimuth(item.getAzimuth());
		}
				
		fw.SetOrbit(item.getOrbit().intValue());
		
		if(item.getTrackGi()==null){
			fw.SetTrack_GI(0);
		}else{
			fw.SetTrack_GI(Integer.valueOf(item.getTrackGi()).intValue());
		}
		
		if(item.getGridJs()==null){
			fw.SetGrid_JS(0);
		}else{
			fw.SetGrid_JS(Integer.valueOf(item.getGridJs()).intValue());
		}
		
		if(item.getGridJe()==null){
			fw.SetGrid_JE(0);
		}else{
			fw.SetGrid_JE(Integer.valueOf(item.getGridJe()).intValue());
		}
		
		if(item.getGridK()==null){
			fw.SetGrid_K(0);
		}else{
			fw.SetGrid_K(Integer.valueOf(item.getGridK()).intValue());
		}		
		
		fw.SetStartLatitude(item.getStartlatitude());
		fw.SetStopLatitude(item.getStoplatitude());
		fw.SetImagingMode(item.getImagingmode().intValue());
		fw.SetSSRStatus(getType(item.getSsrstatus()));
		fw.SetPanCompRatio(getRateType(item.getPancompressionratio()));
		fw.SetMsCompRatio(getRateType(item.getMscompressionratio()));
		fw.SetVideoMb1Gain(item.getVideomb1gain()==null?"":item.getVideomb1gain());
		fw.SetVideoMb2Gain(item.getVideomb2gain()==null?"":item.getVideomb2gain());
		fw.SetVideoMb3Gain(item.getVideomb3gain()==null?"":item.getVideomb3gain());
		fw.SetVideoMb4Gain(item.getVideomb4gain()==null?"":item.getVideomb4gain());
		fw.SetComments(item.getComments()==null?"":item.getComments());
		fw.SetStation(item.getStation());
		  
		this.instances.put((long)arg0, fw);
		return  fw;
		
	}
	
	public GlARL.SubType getType(String type){
		if(type.equals("FSUBTYPE")){
			return GlARL.SubType.FSUBTYPE;
		}else if(type.equals("NOSUBTYPE")){
			return GlARL.SubType.NOSUBTYPE;
			
		}else if(type.equals("RECORD")){
			
			return GlARL.SubType.RECORD;
		}else if(type.equals("PLAYBACK")){
			return GlARL.SubType.PLAYBACK;
			
		}else if(type.equals("DDT")){
			return GlARL.SubType.DDT;
			
		}else if(type.equals("POSTSHIP")){
			return GlARL.SubType.POSTSHIP;
			
		}else if(type.equals("LSUBTYPE")){
			return GlARL.SubType.LSUBTYPE;
			
		}else{			
			return GlARL.SubType.FSUBTYPE;
		}
		
	}
	
	public GlRecorder.Rate getRateType(String type){
		if(type.equals("DEFAULT")){
			return GlRecorder.Rate.DEFAULT;
		}else if(type.equals("LOW")){
			return GlRecorder.Rate.LOW;
			
		}else if(type.equals("HIGH")){			
			return GlRecorder.Rate.HIGH;
		}else if(type.equals("MEDIUM")){			
			return GlRecorder.Rate.MEDIUM;
		}else if(type.equals("NONE")){			
			return GlRecorder.Rate.NONE;
		}else{			
			return GlRecorder.Rate.DEFAULT;
		}		
	}
	
	
	
	@Override
	public void GetAll(FwBaseARLEntryImpVector arg0, int parentID) {
		logger.info(" RSIImagingARLEntryImpFactory.class , super.GetAll(arg0, parentID); no imp");
		super.GetAll(arg0, parentID);
		
	}

	@Override
	public void Delete(FwBaseARLEntryImp arg0) {
		logger.info(" RSIImagingARLEntryImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwBaseARLEntryImpVector arg0) {
		logger.info(" RSIImagingARLEntryImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void CancelAll() {
		logger.info(" RSIImagingARLEntryImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public boolean Store(FwBaseARLEntryImpVector imps,
			FwBaseARLEntryImpVector delimps) {
		logger.info(" RSIImagingARLEntryImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwBaseARLEntryImp imp) {
		logger.info(" RSIImagingARLEntryImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" RSIImagingARLEntryImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" RSIImagingARLEntryImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" RSIImagingARLEntryImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}

	public HashMap<Long,Long> copyArl(int arlid,int new_arlid) {
		HashMap<Long,Long> rsimaps = new HashMap<Long,Long>();
		List<Rsiimagingrequest> lists = rsiimagingDao.getByArlId(arlid);
		for(Rsiimagingrequest info:lists){
			Long id  = info.getId();
			info.setId(rsiimagingDao.getNextId());			
			info.setArlid(new_arlid);
			info.setId(null);
			rsimaps.put(id, createGetId(info));	
		}
		return rsimaps;
	}
	
	
	public Long getArl(int arlentryid) {		
		Rsiimagingrequest obj = rsiimagingDao.getById(arlentryid);
		return new Long(obj.getArlid());
	}

	

}
