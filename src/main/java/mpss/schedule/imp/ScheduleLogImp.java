package mpss.schedule.imp;

import org.apache.log4j.Logger;

import javae.jmx.amq.topic.AmqJmxDriver;
import javae.jmx.topic.JmxConnection;
import javae.jmx.topic.JmxDriverManager;
import javae.jmx.topic.JmxException;
import mpss.configFactory;
import nspo.mpss.amq.IMpssAmqData;
import nspo.mpss.amq.cmd.ScheduleLog;

public class ScheduleLogImp {

	//private AmqService amq = new AmqService();
	//private int amqID;
	private static JmxConnection<IMpssAmqData, IMpssAmqData> conn = null;
	//private static JmxPublisher<IMpssAmqData, IMpssAmqData> pub;	
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	static {
		
		JmxDriverManager.register("ActiveMQ", new AmqJmxDriver());
		try {
			conn = JmxDriverManager.connectObject(IMpssAmqData.class, IMpssAmqData.class, "ActiveMQ","tcp://140.110.229.210:61616");
			conn.start();
			//pub= conn.createPublisher();
		} catch (JmxException ex) {
			System.err.println();			
		}		 
		
	}
	
	public ScheduleLogImp(){
		super();
	}

	
	public void OnMessage(String msg) {
       ScheduleLog log=new ScheduleLog();
	   log.scheduleLog=msg;		
		//pub.publish("nspo.mpss.web", log);
	    logger.info("ScheduleLogImp OnMessage msg="+msg);
		
	}	
	
}
