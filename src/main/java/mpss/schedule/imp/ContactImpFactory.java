package mpss.schedule.imp;

import mpss.configFactory;
import mpss.common.dao.ContactDao;
import mpss.common.jpa.Contact;
import mpss.schedule.JPAInit;
import mpss.schedule.Schedule;
import mpss.schedule.Schedule.SchType;
import mpss.schedule.Schedule.SessionType;
import mpss.schedule.UIDB;
import mpss.schedule.base.*;
import mpss.util.timeformat.DateUtil;

import java.sql.Timestamp;
import java.util.*;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

public class ContactImpFactory extends FwContactImpFactory implements UIDB<Contact> {

	private int GlNullID = 0;
	public static Map<Long, FwContactImp> instances = new HashMap<Long, FwContactImp>();
	FwSessionImpFactory ourSessionFactory;	
	FwTimeRangeTracker ourTRTracker = new FwTimeRangeTracker();	
	private static ContactDao contactDao=JPAInit.getBuilder().getContactDao();
	
	private Logger logger = Logger.getLogger(configFactory.getLogName());

	public boolean create(Contact object){
		 if(contactDao.create(object)==null){
				return false;
			}else{
				return true;
			}
	 }
	
	public long createGetId(Contact object){
		Contact data  =contactDao.create(object);		
		return data.getId();
	 }

	 public boolean update(Contact object){
		 return contactDao.update(object);
	 }

	 public boolean delete(Contact object){
		 return contactDao.delete(object);	
	 } 
	 
	 public boolean doSql(String jpsql){
		 return contactDao.doSql(jpsql);	
	 }
	
	public void releaseFwImp(){
		ContactImpFactory.instances.clear();
		ourTRTracker = null;
		ourTRTracker = new FwTimeRangeTracker();	
		contactDao.reset();

	}

	@SuppressWarnings("null")
	@Override
	public FwContactImp GetOne(int sat, int site, int ar, GlTime zeroRise,
			GlTime zeroFade, GlTime cmdRise, GlTime cmdFade, GlTime xbandRise,
			GlTime xbandFade, GlTime maxElTime, double maxEl, boolean avail,
			long front, long back, long frontbias, long backbias, int branch) {

		logger.info("call ContactImpFactory.GetOne(...)");
		long newid = contactDao.getNextId();
		
		GetSession(zeroRise, branch);
		FwContactImp imp = null;
		
		if (imp == FindOne(sat, site, ar, branch))
	    {
	        if (imp == CreateOne(newid,sat, site, ar, zeroRise, zeroFade, cmdRise, cmdFade, xbandRise,
	                xbandFade, maxElTime, maxEl, avail, front, back, frontbias,
					backbias, branch))
	        {
	            AddID(imp);
	        }
	    }else if (imp.IsNew() || (imp.IsDeleted()))
	    {	        
	       logger.info("Contact(" + sat + "," + site + "," + ar + "," + "," + branch + ") already exists.");
	    }
	    else
	    {
	        logger.info("Contact(id:"+imp.GetID()+ "," + sat + "," + site + "," + ar + "," + "," + branch + ") already exists - data will not be updated.");
	    }			
		return imp;
	}
	
	@SuppressWarnings("rawtypes")
	public FwContactImp FindOne(int sat, int site,int ar, int branch){
		logger.info("call ContactImpFactory.FindOne(.,.,.,.)");
		FwContactImp imp = null;
		
		Iterator<Entry<Long, FwContactImp>> iter = instances.entrySet().iterator(); 
		while (iter.hasNext()) { 
		    Map.Entry entry = (Map.Entry) iter.next(); 		    
		    FwContactImp key = (FwContactImp)entry.getValue(); 
		    if(key.GetSatellite()==sat && key.GetSite()==site && key.GetRev()==ar && key.GetBranch()==branch){
		    	imp = key;
		    }		    
		} 
	    return imp;
		
	}
		
	public FwContactImp CreateOne(long newid ,int sat,int site,int ar,GlTime zeroRise,GlTime zeroFade,GlTime cmdRise,GlTime cmdFade,GlTime xbandRise,GlTime xbandFade,GlTime maxElTime,double maxEl,boolean avail,long front,long back,long frontbias,long backbias,int branch){
		logger.info("call ContactImpFactory.CreateOne(...)");
		return  new FwContactImp((int) newid, sat, site, ar, zeroRise, zeroFade, cmdRise, cmdFade, xbandRise,
                xbandFade, maxElTime, maxEl, avail, front, back, frontbias,	backbias, 0,new GlTimeRange(zeroRise,zeroFade),branch, true);
	}

	@Override
	public FwContactImp GetOne(int sat, int site, int ar, int branch) {
		logger.info("call ContactImpFactory.GetOne(.,.,.,.)");
		Contact con = contactDao.getContactByParams(sat, site, ar, branch);		
		FwContactImp fwipm = new  FwContactImp(con.getId().intValue(), con.getSatelliteid().intValue(), con.getSiteid().intValue(),con.getRevid().intValue(), DateUtil.timestampToGlTIme(con.getRise()), DateUtil.timestampToGlTIme(con.getFade()), DateUtil.timestampToGlTIme(con.getCmdrise()), DateUtil.timestampToGlTIme(con.getCmdfade()),DateUtil.timestampToGlTIme(con.getXbandrise()) ,DateUtil.timestampToGlTIme(con.getXbandfade()), DateUtil.timestampToGlTIme(con.getMaxeltime()),con.getMaxel().doubleValue(), con.getAvailable().equals("0")? false:true, con.getFrontbias().longValue(), con.getBackbias().longValue(), con.getFrontbias().longValue(),con.getBackbias().longValue(), con.getUotcnt().intValue(), new GlTimeRange(DateUtil.timestampToGlTIme(con.getRise()),DateUtil.timestampToGlTIme(con.getFade())), con.getBranchid().intValue());
		return fwipm;		
	}

	@Override
	public void GetAll(FwContactImpVector imps, GlTimeRange tr,
			boolean getOverlaps, int branch) {
		//System.out.println("call ContactImpFactory.GetAll(.,.,.,.)"); //OK
		if(Schedule.s_type== SessionType.BRANCH){
			branch = Schedule.branch_type_id;
			if(branch==0){				
				logger.info("contact getall branch=0");
				return;
			}
		}
		
		GlTimeRangeVector needTRs=new GlTimeRangeVector();
	    //FwContactImpVector localImps=new FwContactImpVector();
	    if (ourTRTracker.NeedOtherTRs(branch, tr, needTRs))	    {
	        for (int i = 0; i < needTRs.size(); i++)	        {            
	            LoadAll(imps, needTRs.get(i), branch);	            
	            logger.info("FwContactImpFactory::GetAll.  Loading("
		                + needTRs.get(i).AsString() + ")" + " bid: " + branch + " #loaded: " + imps.size());	                
	            ourTRTracker.UpdateTRs(branch, needTRs.get(i));
	            
	        }
	    }   
	    
	    // for branch start
	    branch =0;
	 	// for branch end
	    for(Entry<Long, FwContactImp> entry : instances.entrySet()) {	        
	    	FwContactImp imp = entry.getValue();
	    	if (imp.GetBranch() == branch)
	        {
	            GlTimeRange    nexttr = imp.GetTimeRange();
	            
	            //???? by jeff
//	            if (nexttr.first > tr.second)
//	                break;
	            
	            
	            if (getOverlaps)
	            {
	                if (nexttr.Overlaps(tr)){
	                	imps.add(imp);
	                }	                    
	            }else if (nexttr.StartsWithin(tr)){
	            	imps.add(imp);
	            }	            	
	        }	
	    }
	    
//	   for(int i=0 ;i<=instances.size();i++){		   
//		   FwContactImp imp = instances.get(i);
//		   if (imp.GetBranch() == branch)
//	        {
//	            GlTimeRange    nexttr = imp.GetTimeRange();
//	            
//	            //???? by jeff
////	            if (nexttr.first > tr.second)
////	                break;
//	            
//	            
//	            if (getOverlaps)
//	            {
//	                if (nexttr.Overlaps(tr)){
//	                	imps.add(imp);
//	                }	                    
//	            }else if (nexttr.StartsWithin(tr)){
//	            	imps.add(imp);
//	            }	            	
//	        }		   
//	   }	    
	}

	@Override
	public void GetAll(FwContactImpVector arg0, int branchId) {
		//System.out.println("call ContactImpFactory.GetAll(.,.)");
		if(Schedule.s_type== SessionType.BRANCH){
			branchId = Schedule.branch_type_id;
			if(branchId==0){				
				logger.info("contact getall branch=0");
				return;
			}
		}
		Collection<Contact> col =contactDao.getByBranchId(branchId);
		this.AddFwContactImpVector(arg0, col);
	}
	
	public void AddFwContactImpVector(FwContactImpVector arg0, Collection<Contact> col){
		//System.out.println("call ContactImpFactory.AddFwContactImpVector(.,.) collection");
		Iterator<Contact> item=col.iterator();
		while(item.hasNext()){
			Contact con = (Contact)item.next();
			FwContactImp fwipm;
			if(Schedule.s_type == SessionType.BRANCH){
				if(Schedule.type == SchType.COLD){
					fwipm = new  FwContactImp(con.getId().intValue(), con.getSatelliteid().intValue(), con.getSiteid().intValue(),con.getRevid().intValue(), DateUtil.timestampToGlTIme(con.getRise()), DateUtil.timestampToGlTIme(con.getFade()), DateUtil.timestampToGlTIme(con.getCmdrise()), DateUtil.timestampToGlTIme(con.getCmdfade()),DateUtil.timestampToGlTIme(con.getXbandrise()) ,DateUtil.timestampToGlTIme(con.getXbandfade()), DateUtil.timestampToGlTIme(con.getMaxeltime()),con.getMaxel().doubleValue(), con.getAvailable().equals("0")? false:true, con.getFrontbias().longValue(), con.getBackbias().longValue(), con.getFrontbias().longValue(),con.getBackbias().longValue(), con.getUotcnt().intValue(), new GlTimeRange(DateUtil.timestampToGlTIme(con.getRise()),DateUtil.timestampToGlTIme(con.getFade())), 0);
				}else{
					fwipm = new  FwContactImp(con.getId().intValue(), con.getSatelliteid().intValue(), con.getSiteid().intValue(),con.getRevid().intValue(), DateUtil.timestampToGlTIme(con.getRise()), DateUtil.timestampToGlTIme(con.getFade()), DateUtil.timestampToGlTIme(con.getCmdrise()), DateUtil.timestampToGlTIme(con.getCmdfade()),DateUtil.timestampToGlTIme(con.getXbandrise()) ,DateUtil.timestampToGlTIme(con.getXbandfade()), DateUtil.timestampToGlTIme(con.getMaxeltime()),con.getMaxel().doubleValue(), con.getAvailable().equals("0")? false:true, con.getFrontbias().longValue(), con.getBackbias().longValue(), con.getFrontbias().longValue(),con.getBackbias().longValue(), con.getUotcnt().intValue(), new GlTimeRange(DateUtil.timestampToGlTIme(con.getRise()),DateUtil.timestampToGlTIme(con.getFade())), 0);
				}
			}else{
				fwipm = new  FwContactImp(con.getId().intValue(), con.getSatelliteid().intValue(), con.getSiteid().intValue(),con.getRevid().intValue(), DateUtil.timestampToGlTIme(con.getRise()), DateUtil.timestampToGlTIme(con.getFade()), DateUtil.timestampToGlTIme(con.getCmdrise()), DateUtil.timestampToGlTIme(con.getCmdfade()),DateUtil.timestampToGlTIme(con.getXbandrise()) ,DateUtil.timestampToGlTIme(con.getXbandfade()), DateUtil.timestampToGlTIme(con.getMaxeltime()),con.getMaxel().doubleValue(), con.getAvailable().equals("0")? false:true, con.getFrontbias().longValue(), con.getBackbias().longValue(), con.getFrontbias().longValue(),con.getBackbias().longValue(), con.getUotcnt().intValue(), new GlTimeRange(DateUtil.timestampToGlTIme(con.getRise()),DateUtil.timestampToGlTIme(con.getFade())), 0);
			}
			
			arg0.add(fwipm);
			ContactImpFactory.instances.put(con.getId().longValue(), fwipm);
		}		
	}
	
	@Override
	protected void LoadAll(FwContactImpVector arg0, int branch) {
		logger.info(" ContactImpFactory.class , super.LoadAll(arg0, branch); no imp");
		super.LoadAll(arg0, branch);
		
	}

	@Override
	protected void LoadAllSats(FwContactImpVector arg0, int satid) {
		logger.info(" ContactImpFactory.class , super.LoadAllSats(arg0, satid); no imp");
		super.LoadAllSats(arg0, satid);
		
	}

	@Override
	protected void LoadAllSites(FwContactImpVector arg0, int siteid) {
		logger.info(" ContactImpFactory.class , super.LoadAllSites(arg0, siteid); no imp");
		super.LoadAllSites(arg0, siteid);
		
	}

	@Override
	protected void LoadAllSitesWithSat(FwContactImpVector arg0, int sat,int site, int br) {
		logger.info(" ContactImpFactory.class , super.LoadAllSitesWithSat(arg0, sat, site, br); no imp");
		super.LoadAllSitesWithSat(arg0, sat, site, br);
		
	}

	@Override
	protected FwContactImp LoadOne(int arg0, int arg1, int arg2, int arg3) {
		logger.info(" ContactImpFactory.class , return super.LoadOne(arg0, arg1, arg2, arg3); no imp");
		return super.LoadOne(arg0, arg1, arg2, arg3);
		
	}

	@Override
	public void Delete(FwContactImpVector arg0) {
		logger.info(" ContactImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwContactImp arg0) {
		logger.info(" ContactImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public boolean Store(FwContactImpVector vec) {
		logger.info(" ContactImpFactory.class , return super.Store(vec); modify some,vec size="+String.valueOf(vec.size()));
		long size = vec.size();			
		for (int i = 0; i < (int) size; i++) {
			
			FwContactImp imp = vec.get(i);
			String starttime = imp.GetTimeRange().GetStartTime().asString("%Y-%m-%d %H:%M:%S");
			String endtime = imp.GetTimeRange().GetEndTime().asString("%Y-%m-%d %H:%M:%S");	
			String cmdstarttime = imp.GetCmdRise().asString("%Y-%m-%d %H:%M:%S");	
			String cmdendtime = imp.GetCmdFade().asString("%Y-%m-%d %H:%M:%S");				
			String xbandstarttime = imp.GetXbandRise().asString("%Y-%m-%d %H:%M:%S");	
			String xbandendtime = imp.GetXbandFade().asString("%Y-%m-%d %H:%M:%S");	
			String maxeltimes = imp.GetMaxElTime().asString("%Y-%m-%d %H:%M:%S");	
			String uotstarttime = imp.GetUOT().GetStartTime().asString("%Y-%m-%d %H:%M:%S");
			String uotendtime = imp.GetUOT().GetEndTime().asString("%Y-%m-%d %H:%M:%S");			
			Timestamp tsStart = DateUtil.dateToTimestamp(DateUtil.stringToDate(starttime, "yyyy-MM-dd HH:mm:ss"));
			Timestamp tsEnd =  DateUtil.dateToTimestamp(DateUtil.stringToDate(endtime, "yyyy-MM-dd HH:mm:ss"));			
			Timestamp cmdrise = DateUtil.dateToTimestamp(DateUtil.stringToDate(cmdstarttime, "yyyy-MM-dd HH:mm:ss"));
			Timestamp cmdfade =  DateUtil.dateToTimestamp(DateUtil.stringToDate(cmdendtime, "yyyy-MM-dd HH:mm:ss"));			
			Timestamp xbandrise = DateUtil.dateToTimestamp(DateUtil.stringToDate(xbandstarttime, "yyyy-MM-dd HH:mm:ss"));
			Timestamp xbandfade =  DateUtil.dateToTimestamp(DateUtil.stringToDate(xbandendtime, "yyyy-MM-dd HH:mm:ss"));
			Timestamp maxeltime =  DateUtil.dateToTimestamp(DateUtil.stringToDate(maxeltimes, "yyyy-MM-dd HH:mm:ss"));			
			Timestamp uotstart =  DateUtil.dateToTimestamp(DateUtil.stringToDate(uotstarttime, "yyyy-MM-dd HH:mm:ss"));
			Timestamp uotend =  DateUtil.dateToTimestamp(DateUtil.stringToDate(uotendtime, "yyyy-MM-dd HH:mm:ss"));
			
			
			Contact contact = new Contact();			
			contact.setId(Long.valueOf(imp.GetID()));
			//contact.setBranchid(imp.GetBranch());
			contact.setBranchid(0);
			contact.setSiteid(imp.GetSite());
			contact.setSatelliteid(imp.GetSatellite());
			contact.setRevid(imp.GetRev());
			contact.setRise(tsStart);
			contact.setFade(tsEnd);
			contact.setCmdrise(cmdrise);
			contact.setCmdfade(cmdfade);
			contact.setXbandrise(xbandrise);
			contact.setXbandfade(xbandfade);
			contact.setMaxeltime(maxeltime);			
			contact.setMaxel((int)(imp.GetMaxEl()));
			contact.setAvailable(imp.GetAvailable()==true?1:0);
			contact.setFrontexp(Integer.valueOf(String.valueOf(imp.GetFrontExpansion())));
			contact.setBackexp(Integer.valueOf(String.valueOf(imp.GetBackExpansion())));
			contact.setFrontbias(Integer.valueOf(String.valueOf(imp.GetFrontBias())));
			contact.setBackbias(Integer.valueOf(String.valueOf(imp.GetBackBias())));			
			contact.setUotcnt(imp.GetUOTCount());
			contact.setEnduot(uotend);
			contact.setStartuot(uotstart);			
			
			try{
				if(Schedule.type==SchType.COLD){
					if(Schedule.updateDB==true){
						contactDao.update(contact);
					}				
				}else if(Schedule.type==SchType.HOT){
					if(Schedule.s_type== SessionType.BRANCH){
						int branchid = Schedule.branch_type_id;
						contact.setBranchid(branchid);
					}
					if(Schedule.updateDB==true){
						if(Schedule.s_type != SessionType.BRANCH){
							contactDao.update(contact);
						}else{
							contactDao.update(contact);
						}
						
					}				
				}else if(Schedule.type==SchType.RESCH){
					if(Schedule.s_type== SessionType.BRANCH){
						int branchid = Schedule.branch_type_id;
						contact.setBranchid(branchid);
					}
					if(Schedule.updateDB==true){
						if(Schedule.s_type != SessionType.BRANCH){
							contactDao.update(contact);
						}else{
							contactDao.update(contact);
						}
					}				
				}else if(Schedule.type==SchType.RESCH_FILTER){
					if(Schedule.s_type== SessionType.BRANCH){
						int branchid = Schedule.branch_type_id;
						contact.setBranchid(branchid);
					}
					if(Schedule.updateDB==true){
						if(Schedule.s_type != SessionType.BRANCH){
							contactDao.update(contact);
						}else{
							contactDao.update(contact);
						}
					}				
				}
			}catch(Exception ex){
				logger.info("contactimpfactory Store ex="+ex.getMessage());
			}				
		}
		
		return true;
		
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" ContactImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" ContactImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" ContactImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}

	@Override
	public boolean Store(FwContactImpVector savem, FwContactImpVector deleteem) {
		logger.info(" ContactImpFactory.class , return super.Store(savem, deleteem); no imp");
		return true;		
	}

	@Override
	public boolean Store(FwContactImp imp) {
		logger.info(" ContactImpFactory.class , return super.Store(imp); no imp");
		return true;
		
	}

	@Override
	public boolean Store() {
		logger.info(" ContactImpFactory.class , return super.Store(); no imp");
		return true;
		
	}

	@Override
	public boolean DeleteAll() {
		logger.info(" ContactImpFactory.class , return super.DeleteAll(); no imp");
		return super.DeleteAll();
		
	}

	@Override
	public void CancelAll() {
		logger.info(" ContactImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public void Cancel(FwContactImp arg0) {
		logger.info(" ContactImpFactory.class , super.Cancel(arg0); no imp");
		super.Cancel(arg0);
		
	}

	@Override
	public void Cleanup() {
		logger.info(" ContactImpFactory.class , super.Cleanup(); no imp");
		super.Cleanup();
		
	}
	
	public void AddFwContactImpVector(FwContactImpVector arg0, List<Contact> col){
		//System.out.println("call ContactImpFactory.AddFwContactImpVector(.,.) list size="+col.size()); //OK
		Iterator<Contact> item=col.iterator();
		while(item.hasNext()){
			Contact con = (Contact)item.next();		
			// branch col schedul
			FwContactImp fwipm;
			if(Schedule.s_type == SessionType.BRANCH){
				if(Schedule.type == SchType.COLD){
					fwipm = new  FwContactImp(con.getId().intValue(), con.getSatelliteid().intValue(), con.getSiteid().intValue(),con.getRevid().intValue(), DateUtil.timestampToGlTIme(con.getRise()), DateUtil.timestampToGlTIme(con.getFade()), DateUtil.timestampToGlTIme(con.getCmdrise()), DateUtil.timestampToGlTIme(con.getCmdfade()),DateUtil.timestampToGlTIme(con.getXbandrise()) ,DateUtil.timestampToGlTIme(con.getXbandfade()), DateUtil.timestampToGlTIme(con.getMaxeltime()),con.getMaxel().doubleValue(), con.getAvailable().equals("0")? false:true, con.getFrontbias().longValue(), con.getBackbias().longValue(), con.getFrontbias().longValue(),con.getBackbias().longValue(), con.getUotcnt().intValue(), new GlTimeRange(DateUtil.timestampToGlTIme(con.getRise()),DateUtil.timestampToGlTIme(con.getFade())), 0);
				}else{
					fwipm = new  FwContactImp(con.getId().intValue(), con.getSatelliteid().intValue(), con.getSiteid().intValue(),con.getRevid().intValue(), DateUtil.timestampToGlTIme(con.getRise()), DateUtil.timestampToGlTIme(con.getFade()), DateUtil.timestampToGlTIme(con.getCmdrise()), DateUtil.timestampToGlTIme(con.getCmdfade()),DateUtil.timestampToGlTIme(con.getXbandrise()) ,DateUtil.timestampToGlTIme(con.getXbandfade()), DateUtil.timestampToGlTIme(con.getMaxeltime()),con.getMaxel().doubleValue(), con.getAvailable().equals("0")? false:true, con.getFrontbias().longValue(), con.getBackbias().longValue(), con.getFrontbias().longValue(),con.getBackbias().longValue(), con.getUotcnt().intValue(), new GlTimeRange(DateUtil.timestampToGlTIme(con.getRise()),DateUtil.timestampToGlTIme(con.getFade())), 0);
				}
			}else{
				fwipm = new  FwContactImp(con.getId().intValue(), con.getSatelliteid().intValue(), con.getSiteid().intValue(),con.getRevid().intValue(), DateUtil.timestampToGlTIme(con.getRise()), DateUtil.timestampToGlTIme(con.getFade()), DateUtil.timestampToGlTIme(con.getCmdrise()), DateUtil.timestampToGlTIme(con.getCmdfade()),DateUtil.timestampToGlTIme(con.getXbandrise()) ,DateUtil.timestampToGlTIme(con.getXbandfade()), DateUtil.timestampToGlTIme(con.getMaxeltime()),con.getMaxel().doubleValue(), con.getAvailable().equals("0")? false:true, con.getFrontbias().longValue(), con.getBackbias().longValue(), con.getFrontbias().longValue(),con.getBackbias().longValue(), con.getUotcnt().intValue(), new GlTimeRange(DateUtil.timestampToGlTIme(con.getRise()),DateUtil.timestampToGlTIme(con.getFade())), 0);
			}
			 
			
			arg0.add(fwipm);
			ContactImpFactory.instances.put(con.getId().longValue(), fwipm);
		}		
	}

	@Override
	public void GetAllSats(FwContactImpVector arg0, int stat) {
		logger.info("call ContactImpFactory.GetAllSats(.,.)");
		Collection<Contact> col =contactDao.getByStatId(stat);
		this.AddFwContactImpVector(arg0, col);
	}

	@Override
	public void GetAllSites(FwContactImpVector arg0, int site) {
		logger.info("call ContactImpFactory.GetAllSites(.,.)");
		Collection<Contact> col =contactDao.getBySiteId(site);
		this.AddFwContactImpVector(arg0, col);
	}

	@Override
	public void GetAllSitesWithSat(FwContactImpVector imps, int sat,int siteid, int br) {
		logger.info("call ContactImpFactory.GetAllSitesWithSat(.,.,.,.)");
		Collection<Contact> col =contactDao.getContactByParams(sat, siteid, br);
		this.AddFwContactImpVector(imps, col);
	}

	
	
	@Override
	public FwContactImp GetID(int arg0) {
		//System.out.println("call ContactImpFactory.GetID(.) id ="+arg0); //OK
		Contact con =contactDao.getContactById(arg0);
		FwContactImp fwipm = new  FwContactImp(arg0, con.getSatelliteid().intValue(), con.getSiteid().intValue(),con.getRevid().intValue(), DateUtil.timestampToGlTIme(con.getRise()), DateUtil.timestampToGlTIme(con.getFade()), DateUtil.timestampToGlTIme(con.getCmdrise()), DateUtil.timestampToGlTIme(con.getCmdfade()),DateUtil.timestampToGlTIme(con.getXbandrise()) ,DateUtil.timestampToGlTIme(con.getXbandfade()), DateUtil.timestampToGlTIme(con.getMaxeltime()),con.getMaxel().doubleValue(), con.getAvailable().equals("0")? false:true, con.getFrontbias().longValue(), con.getBackbias().longValue(), con.getFrontbias().longValue(),con.getBackbias().longValue(), con.getUotcnt().intValue(), new GlTimeRange(DateUtil.timestampToGlTIme(con.getRise()),DateUtil.timestampToGlTIme(con.getFade())),0);
		return fwipm;	
	}

	@Override
	public void GetAll(FwContactImpVector arg0) {
		logger.info("call ContactImpFactory.GetAll(.)");
		Collection<Contact> col =contactDao.getAll();
		this.AddFwContactImpVector(arg0, col);
	}

	@Override
	public void DoGetAll(FwContactImpVector arg0) {
		logger.info("call ContactImpFactory.DoGetAll(.)");
		Collection<Contact> col =contactDao.getAll();
		this.AddFwContactImpVector(arg0, col);
	}

	@Override
	public void AddID(FwContactImp imp) {
		logger.info("call ContactImpFactory.AddID(.)");
		if (imp != null){
			instances.put((long)imp.GetID(), imp);
		}
	}
	
	@Override
	public synchronized void delete() {
		logger.info(" ContactImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	protected FwContactImp CreateOne(int arg0, int arg1, int arg2,
			GlTime zeroRise, GlTime zeroFade, GlTime cmdRise, GlTime cmdFade,
			GlTime xbandRise, GlTime xbandFade, GlTime maxElTime, double maxEl,
			boolean avail, long front, long back, long frontbias,
			long backbias, int branch) {
		logger.info(" ContactImpFactory.class , return super.CreateOne(...); no imp");
		return super.CreateOne(arg0, arg1, arg2, zeroRise, zeroFade, cmdRise, cmdFade,
				xbandRise, xbandFade, maxElTime, maxEl, avail, front, back, frontbias,
				backbias, branch);
		
	}

	@Override
	protected void DoCleanup() {
		logger.info(" ContactImpFactory.class , super.DoCleanup(); no imp");
		super.DoCleanup();
		
	}

	@Override
	protected boolean CheckSatelliteInDatabase(int satelliteID) {
		logger.info(" ContactImpFactory.class , return super.CheckSatelliteInDatabase(satelliteID); no imp");
		return super.CheckSatelliteInDatabase(satelliteID);
		
	}

	@Override
	protected boolean CheckSiteInDatabase(int siteID) {
		logger.info(" ContactImpFactory.class , return super.CheckSiteInDatabase(siteID); no imp");
		return super.CheckSiteInDatabase(siteID);
		
	}

	@Override
	protected void AddNew(FwContactImp arg0) {
		logger.info(" ContactImpFactory.class , super.AddNew(arg0); no imp");
		super.AddNew(arg0);
		
	}

	@Override
	protected void AddDeleted(FwContactImp arg0) {
		logger.info(" ContactImpFactory.class , super.AddDeleted(arg0); no imp");
		super.AddDeleted(arg0);
		
	}

	@Override
	protected void DoCancel(FwContactImp arg0) {
		logger.info(" ContactImpFactory.class , super.DoCancel(arg0); no imp");
		super.DoCancel(arg0);
		
	}

	@Override
	protected void DoCancelAll() {
		logger.info(" ContactImpFactory.class , super.DoCancelAll(); no imp");
		super.DoCancelAll();
		
	}

	public void GetSession(GlTime  glt, long branch){
		logger.info("call ContactImpFactory.GetSession(.,.)");
		FwContactImpVector localImps=null;		
		int myinterval = 60*60*24*7;		
	    GlTimeRange tr =DateUtil.getGlTimeRange(DateUtil.GlTimeAddSec(glt, myinterval),DateUtil.GlTimeAddSec(glt, -myinterval));;
	    GlTimeRange halfweek =DateUtil.getGlTimeRange(DateUtil.GlTimeAddSec(glt, myinterval/2),DateUtil.GlTimeAddSec(glt, -myinterval/2));
	    
	    if(Schedule.s_type== SessionType.BRANCH){
	    	branch = Schedule.branch_type_id;
			if(branch==0){
				logger.info("contact getsession branch=0");
				return;
			}
		}
	    
	       
	    if (branch != GlNullID)
	    {
	        FwSessionImp sesimp = GetSessionFactory().GetID((int)branch);
	        if (sesimp!=null)
	        {
	            tr = sesimp.GetTimeRange();
	        }
	    }	    
	    if (!ourTRTracker.TrackingTR((int)branch) || !(ourTRTracker.GetTR((int)branch).Contains(halfweek))){
	    	GetAll(localImps, tr, true, (int)branch);  
	    }
	        
	}
	
	public FwSessionImpFactory GetSessionFactory()
	{
		logger.info("call ContactImpFactory.GetSessionFactory()");
	    if (ourSessionFactory==null)
	    {
	        ourSessionFactory = FwImpFactory.Instance().GetSessionImpFactory();
	    }
	    return ourSessionFactory;
	}
	
	public void AddToSorted(FwContactImp imp)
	{	   
		logger.info("call ContactImpFactory.AddToSorted(.)");
		instances.put(Long.valueOf(imp.GetID()),imp);	    
	}

	@Override
	protected void LoadAll(FwContactImpVector arg0, GlTimeRange arg1, int branch) {	
		//System.out.println("call ContactImpFactory.LoadAll(.,.,.)"); //OK
		String starttime = arg1.GetStartTime().asString("%Y-%m-%d %H:%M:%S");
		String endtime = arg1.GetEndTime().asString("%Y-%m-%d %H:%M:%S");
		if(Schedule.s_type== SessionType.BRANCH){
			branch = Schedule.branch_type_id;
			if(branch==0){
				logger.info("contact LoadAll branch=0");
				return;
			}
		}
		List<Contact> list = contactDao.getByBranchIdNTimeRange(branch, starttime, endtime);		
		this.AddFwContactImpVector(arg0, list);
	}

	@Override
	public boolean CheckSatelliteDependency(int satelliteID) {
		logger.info(" ContactImpFactory.class , return super.CheckSatelliteDependency(satelliteID); no imp");
		return super.CheckSatelliteDependency(satelliteID);
		
	}

	@Override
	public boolean CheckSiteDependency(int siteID) {
		logger.info(" ContactImpFactory.class , return super.CheckSiteDependency(siteID); no imp");
		return super.CheckSiteDependency(siteID);
		
	}
	
	
	public long getContactIdByMaxeltime(Date time){
		Contact cont=contactDao.getContactByMaxeltime(time);
		if(cont!=null){
			return cont.getId();
		}
		
		Date time1 =DateUtil.DateAddSec(time, 25);
		cont=contactDao.getContactByMaxeltime(time1);
		if(cont!=null){
			return cont.getId();
		}
		
		Date time2 =DateUtil.DateAddSec(time, -25);
		cont=contactDao.getContactByMaxeltime(time2);
		if(cont!=null){
			return cont.getId();
		}
			
		return 0;		
		
	}
	
	
	public  List<Contact> getContactByTime(String starttime,String endtime , int branch) {		
		return  contactDao.getByBranchIdNTimeRange(branch, starttime, endtime);		
		
	}
	
	
}
