package mpss.schedule.imp;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.dao.RtcmdprocrequestDao;
import mpss.common.jpa.Rtcmdprocrequest;
import mpss.schedule.JPAInit;
import mpss.schedule.UIDB;
import mpss.schedule.base.FwBaseARLEntryImp;
import mpss.schedule.base.FwBaseARLEntryImpVector;
import mpss.schedule.base.FwBaseImpVector;
import mpss.schedule.base.FwRTCmdProcARLEntryImp;
import mpss.schedule.base.FwRTCmdProcARLEntryImpFactory;
import mpss.schedule.base.GlDaySpec;


public class RtcCmdProcImpFactory extends FwRTCmdProcARLEntryImpFactory implements UIDB<Rtcmdprocrequest>{
	
    private static RtcmdprocrequestDao rtcccmdprocDao=JPAInit.getBuilder().getRtcmdprocrequest();
    private Logger logger = Logger.getLogger(configFactory.getLogName());
	
	
	public boolean create(Rtcmdprocrequest object){
		if(rtcccmdprocDao.create(object)==null){
			return false;
		}else{
			return true;
		}
	 }
	
	public Long createGetId(Rtcmdprocrequest object){
		Rtcmdprocrequest data  =rtcccmdprocDao.create(object);		
		return data.getId();		
	 }

	 public boolean update(Rtcmdprocrequest object){
		 return rtcccmdprocDao.update(object);
	 }

	 public boolean delete(Rtcmdprocrequest object){
		 return rtcccmdprocDao.delete(object);	
	 }

	 public boolean doSql(String jpsql){
		 return rtcccmdprocDao.doSql(jpsql);	
	 }

	
	 public void releaseFwImp(){
		 rtcccmdprocDao.reset();
	 }

	@Override
	public synchronized void delete() {
		logger.info(" RtcCmdProcImpFactory.class , super.delete(); no imp");
		super.delete();
		
	}

	@Override
	public FwRTCmdProcARLEntryImp GetOne(int parentID, GlDaySpec Date,long startTime, long Duration, String RTCmdProc, String requestor,String Comments) {
		logger.info(" RtcCmdProcImpFactory.class , return super.GetOne(parentID, Date, startTime, Duration, RTCmdProc, requestor,Comments); no imp");
		return super.GetOne(parentID, Date, startTime, Duration, RTCmdProc, requestor,Comments);
		
	}

	@Override
	protected FwRTCmdProcARLEntryImp CreateOne(int parent, GlDaySpec Date,long startTime, long Duration, String RTCmdProc, String requestor,String Comments) {
		logger.info(" RtcCmdProcImpFactory.class , return super.CreateOne(parent, Date, startTime, Duration, RTCmdProc, requestor,Comments); no imp");
		return super.CreateOne(parent, Date, startTime, Duration, RTCmdProc, requestor,Comments);
		
	}

	@Override
	public FwBaseARLEntryImp GetID(int arg0) {
		logger.info(" RtcCmdProcImpFactory.class , return super.GetID(arg0); no imp");
		return super.GetID(arg0);
		
	}

	@Override
	public void GetAll(FwBaseARLEntryImpVector arg0, int parentID) {
		logger.info(" RtcCmdProcImpFactory.class , super.GetAll(arg0, parentID); no imp");
		super.GetAll(arg0, parentID);
		
	}

	@Override
	public void Delete(FwBaseARLEntryImp arg0) {
		logger.info(" RtcCmdProcImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void Delete(FwBaseARLEntryImpVector arg0) {
		logger.info(" RtcCmdProcImpFactory.class , super.Delete(arg0); no imp");
		super.Delete(arg0);
		
	}

	@Override
	public void CancelAll() {
		logger.info(" RtcCmdProcImpFactory.class , super.CancelAll(); no imp");
		super.CancelAll();
		
	}

	@Override
	public boolean Store(FwBaseARLEntryImpVector imps,FwBaseARLEntryImpVector delimps) {
		logger.info(" RtcCmdProcImpFactory.class , return super.Store(imps, delimps); no imp");
		return super.Store(imps, delimps);
		
	}

	@Override
	public boolean Store(FwBaseARLEntryImp imp) {
		logger.info(" RtcCmdProcImpFactory.class , return super.Store(imp); no imp");
		return super.Store(imp);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0) {
		logger.info(" RtcCmdProcImpFactory.class , super.GetAll(arg0); no imp");
		super.GetAll(arg0);
		
	}

	@Override
	public void GetAll(FwBaseImpVector arg0, int arg1) {
		logger.info(" RtcCmdProcImpFactory.class , super.GetAll(arg0, arg1); no imp");
		super.GetAll(arg0, arg1);
		
	}

	@Override
	public boolean Delete(FwBaseImpVector arg0) {
		logger.info(" RtcCmdProcImpFactory.class , return super.Delete(arg0); no imp");
		return super.Delete(arg0);
		
	}
	
	public HashMap<Long,Long> copyArl(int arlid,int new_arlid) {
		HashMap<Long,Long> rtcmdprocmaps = new HashMap<Long,Long>();
		List<Rtcmdprocrequest> lists = rtcccmdprocDao.getByArlId(arlid);
		for(Rtcmdprocrequest info:lists){
			Long id  = info.getId();
			info.setId(null);			
			info.setArlid(new_arlid);			
			rtcmdprocmaps.put(id, createGetId(info));	
		}
		return rtcmdprocmaps;
	}
		

}
