package mpss.schedule;

public interface UIDB<T> {

	    public boolean create(T t);

	    public boolean update(T object);

	    public boolean delete(T object);  
	    
	    public boolean doSql(String jpsql);  
	    
	    
}
