package mpss.schedule;

import java.util.ArrayList;
import java.util.List;

import mpss.common.jpa.Activity;
import mpss.common.jpa.Contact;

public class AIPGenerate {	


	@SuppressWarnings("deprecation")
	public List<Activity> Generate(int sessionid,List<Contact> contacts){
		List<Activity> acts = initAct();
		for(Contact contact :contacts){
			if(contact.getRise().getHours()<12){
				if(contact.getMaxel()>acts.get(0).getDuration()){
					acts.get(0).setDuration(contact.getMaxel());
					acts.get(0).setStarttime(contact.getCmdrise());
					acts.get(0).setEndtime(contact.getCmdfade());
					acts.get(0).setSatelliteid(contact.getSatelliteid());
					acts.get(0).setSessionid(sessionid);
					//acts.get(0).setExtentid(0);
					acts.get(0).setScheduled(0);
					acts.get(0).setArlentrytype("AIPPBK");
					acts.get(0).setArlentryid(0);
					acts.get(0).setName("AIP(PBK)");
					acts.get(0).setActsubtype("0");
					acts.get(0).setToytype("NO");
					acts.get(0).setPrevactid(0);
					acts.get(0).setNextactid(0);
					acts.get(0).setUsermodified(0);
					acts.get(0).setValid(1);
					acts.get(0).setInfo("");

				}
			}else{
				if(contact.getMaxel()>acts.get(1).getDuration()){
					acts.get(1).setDuration(contact.getMaxel());
					acts.get(1).setStarttime(contact.getCmdrise());
					acts.get(1).setEndtime(contact.getCmdfade());
					acts.get(1).setSatelliteid(contact.getSatelliteid());
					acts.get(1).setSessionid(sessionid);
					//acts.get(1).setExtentid(0);
					acts.get(1).setScheduled(0);
					acts.get(1).setArlentrytype("AIPPBK");
					acts.get(1).setArlentryid(0);
					acts.get(1).setName("AIP(PBK)");
					acts.get(0).setActsubtype("1");
					acts.get(1).setPrevactid(0);
					acts.get(1).setNextactid(0);
					acts.get(1).setUsermodified(0);
					acts.get(1).setValid(1);
					acts.get(1).setInfo("");
					
				}
				
			}
		}
		
		return acts;
	}
	
	private List<Activity> initAct(){
		List<Activity> acts = new ArrayList<Activity>();
		Activity act1 = new Activity();
		act1.setDuration(0);
		Activity act2 = new Activity();	
		act2.setDuration(0);
		acts.add(act1);
		acts.add(act2);		
		return acts;
	}
	
}
