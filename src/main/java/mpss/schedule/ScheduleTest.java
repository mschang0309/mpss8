package mpss.schedule;


import java.util.Date;
import java.util.LinkedList;

import mpss.schedule.base.ScSchedulerType;
import mpss.util.timeformat.DateUtil;

import org.junit.Test;

public class ScheduleTest {

	//@Test
	public void testCold() {
		// cold schedule test
		Schedule sch=new Schedule();
		sch.runCold("day007");	
		System.out.println("asdasd");	
	}
	
	//@Test
	public void testColdBranch() {
		// cold schedule test
		Schedule sch=new Schedule();
		sch.runCold("day008-br");	
		System.out.println("asdasd");	
	}
	
	//@Test
	public void testHotBranch() {
			// hot schedule test
			Schedule sch=new Schedule();
			Schedule.updateDB = true;
			sch.runHot(49640);

	}

	//@Test
	public void testHot() {
		// hot schedule test
		Schedule sch=new Schedule();		
		Schedule.updateDB = true;
		sch.runHot(49677);
	}
	
	//@Test
	public void testReHot() {

			Schedule sch=new Schedule();
			Schedule.updateDB = false;
			sch.runUIHot(47267,1897168,344914);
			
	}	
	
	
	 @Test
	public void test222ReHot() {

			Schedule sch=new Schedule();
			Schedule.updateDB = false;
			LinkedList<Long> activityids = new LinkedList<Long>();
			activityids.add(new Long(2011742));					
			
			LinkedList<Long> deleteactivityids = new LinkedList<Long>();	
			
			sch.runUIReHot(49681,1111,"ANY",activityids,-1,deleteactivityids);
			
			
	}
	
   // @Test
	public void testOneAct2Contact() {

			Schedule sch=new Schedule();
			Schedule.updateDB = true;
			LinkedList<Long> activityids = new LinkedList<Long>();
			activityids.add(new Long(2001504));			
			LinkedList<Long> deleteactivityids = new LinkedList<Long>();			
			sch.runUIReHot(49638,123,"",activityids,377451,deleteactivityids);			
			
	}
	
	
	    //@Test
		public void testMoveActivity() {
	    	
				Schedule sch=new Schedule();
				Schedule.updateDB = true;	
				Date d =DateUtil.stringToDate("2014-07-24 02:10:10", "yyyy-MM-dd HH:mm:ss");
				//sch.AddResourceChange(47267,1919158,new Date());
				sch.AddResourceChange(47267,40,d);
				
		}
		
		
		//@Test
		public void testAdjustActivity() {
			    	
				Schedule sch=new Schedule();
				Schedule.updateDB = true;	
				Date d =DateUtil.stringToDate("2014-10-23 00:18:00", "yyyy-MM-dd HH:mm:ss");
				LinkedList<Long> activityids = new LinkedList<Long>();
				activityids.add(new Long(4112));	
				activityids.add(new Long(4113));				
				sch.AdjustActivity(47277,1876,activityids,d);
						
		}
		
		//@Test
		public void testReschFilterHot() {
			    	
			Schedule sch=new Schedule();
			Schedule.updateDB = true;
			
			LinkedList<Long> sites = new LinkedList<Long>();
			sites.add(new Long(1));
			sites.add(new Long(2));
			sites.add(new Long(3));
			sites.add(new Long(51));
			sites.add(new Long(81));
			sites.add(new Long(82));
			sites.add(new Long(83));
			sites.add(new Long(111));
			sites.add(new Long(121));
			sites.add(new Long(131));
			
			LinkedList<Long> activityids = new LinkedList<Long>();
			activityids.add(new Long(11415));	
			activityids.add(new Long(11431));
			
			LinkedList<Long> deleteactivityids = new LinkedList<Long>();
			Date d1 =DateUtil.stringToDate("2014-10-26 13:00:00", "yyyy-MM-dd HH:mm:ss");
			Date d2 =DateUtil.stringToDate("2014-10-30 00:00:00", "yyyy-MM-dd HH:mm:ss");
			
			sch.runUIReFilterHot(d1,d2,sites,activityids,deleteactivityids);
						
		}
 
		//@Test
	    @SuppressWarnings("unused")
		public void testMS1750A() {
			Schedule sch=new Schedule();	
			int AcsiiCode=1;
	        char Asc2Char= (char) AcsiiCode;  
	        int aa =00;
	        int ab =01;
	        
			char[] a = {Asc2Char,(char) aa,(char) aa,(char) ab};
			
			//String d=new String(a);
			float f =ScSchedulerType.FSI1750(String.valueOf(a));
			System.out.println(f);
					
				}

}
