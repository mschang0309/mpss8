/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwRTCmdProcARLEntryImpFactory extends FwBaseARLEntryImpFactory {
  private long swigCPtr;

  protected FwRTCmdProcARLEntryImpFactory(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwRTCmdProcARLEntryImpFactory_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwRTCmdProcARLEntryImpFactory obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwRTCmdProcARLEntryImpFactory(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FwRTCmdProcARLEntryImpFactory_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FwRTCmdProcARLEntryImpFactory_change_ownership(this, swigCPtr, true);
  }

  public FwRTCmdProcARLEntryImp GetOne(int parentID, GlDaySpec Date, long startTime, long Duration, String RTCmdProc, String requestor, String Comments) {
    long cPtr = (getClass() == FwRTCmdProcARLEntryImpFactory.class) ? ScWrapJNI.FwRTCmdProcARLEntryImpFactory_GetOne(swigCPtr, this, parentID, GlDaySpec.getCPtr(Date), Date, startTime, Duration, RTCmdProc, requestor, Comments) : ScWrapJNI.FwRTCmdProcARLEntryImpFactory_GetOneSwigExplicitFwRTCmdProcARLEntryImpFactory(swigCPtr, this, parentID, GlDaySpec.getCPtr(Date), Date, startTime, Duration, RTCmdProc, requestor, Comments);
    return (cPtr == 0) ? null : new FwRTCmdProcARLEntryImp(cPtr, false);
  }

  public FwRTCmdProcARLEntryImpFactory() {
    this(ScWrapJNI.new_FwRTCmdProcARLEntryImpFactory__SWIG_0(), true);
    ScWrapJNI.FwRTCmdProcARLEntryImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwRTCmdProcARLEntryImpFactory(FwRTCmdProcARLEntryImpFactory arg0) {
    this(ScWrapJNI.new_FwRTCmdProcARLEntryImpFactory__SWIG_1(FwRTCmdProcARLEntryImpFactory.getCPtr(arg0), arg0), true);
    ScWrapJNI.FwRTCmdProcARLEntryImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwRTCmdProcARLEntryImp CreateOne(int parent, GlDaySpec Date, long startTime, long Duration, String RTCmdProc, String requestor, String Comments) {
    long cPtr = (getClass() == FwRTCmdProcARLEntryImpFactory.class) ? ScWrapJNI.FwRTCmdProcARLEntryImpFactory_CreateOne(swigCPtr, this, parent, GlDaySpec.getCPtr(Date), Date, startTime, Duration, RTCmdProc, requestor, Comments) : ScWrapJNI.FwRTCmdProcARLEntryImpFactory_CreateOneSwigExplicitFwRTCmdProcARLEntryImpFactory(swigCPtr, this, parent, GlDaySpec.getCPtr(Date), Date, startTime, Duration, RTCmdProc, requestor, Comments);
    return (cPtr == 0) ? null : new FwRTCmdProcARLEntryImp(cPtr, false);
  }

}
