/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FactoryParent_FwARLImp extends FwBaseImpFactory {
  private long swigCPtr;

  protected FactoryParent_FwARLImp(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FactoryParent_FwARLImp_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FactoryParent_FwARLImp obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        throw new UnsupportedOperationException("C++ destructor does not have public access");
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FactoryParent_FwARLImp_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FactoryParent_FwARLImp_change_ownership(this, swigCPtr, true);
  }

  public FactoryParent_FwARLImp() {
    this(ScWrapJNI.new_FactoryParent_FwARLImp(), true);
    ScWrapJNI.FactoryParent_FwARLImp_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  public FwARLImp GetID(int arg0) {
    long cPtr = ScWrapJNI.FactoryParent_FwARLImp_GetID(swigCPtr, this, arg0);
    return (cPtr == 0) ? null : new FwARLImp(cPtr, false);
  }

  public void GetAll(FwARLImpVector arg0) {
    ScWrapJNI.FactoryParent_FwARLImp_GetAll(swigCPtr, this, FwARLImpVector.getCPtr(arg0), arg0);
  }

  public void Delete(FwARLImpVector arg0) {
    ScWrapJNI.FactoryParent_FwARLImp_Delete__SWIG_0(swigCPtr, this, FwARLImpVector.getCPtr(arg0), arg0);
  }

  public void Delete(FwARLImp arg0) {
    ScWrapJNI.FactoryParent_FwARLImp_Delete__SWIG_1(swigCPtr, this, FwARLImp.getCPtr(arg0), arg0);
  }

  public boolean Store(FwARLImpVector vec) {
    return (getClass() == FactoryParent_FwARLImp.class) ? ScWrapJNI.FactoryParent_FwARLImp_Store__SWIG_0(swigCPtr, this, FwARLImpVector.getCPtr(vec), vec) : ScWrapJNI.FactoryParent_FwARLImp_StoreSwigExplicitFactoryParent_FwARLImp__SWIG_0(swigCPtr, this, FwARLImpVector.getCPtr(vec), vec);
  }

  public boolean Store(FwARLImpVector savem, FwARLImpVector deleteem) {
    return (getClass() == FactoryParent_FwARLImp.class) ? ScWrapJNI.FactoryParent_FwARLImp_Store__SWIG_1(swigCPtr, this, FwARLImpVector.getCPtr(savem), savem, FwARLImpVector.getCPtr(deleteem), deleteem) : ScWrapJNI.FactoryParent_FwARLImp_StoreSwigExplicitFactoryParent_FwARLImp__SWIG_1(swigCPtr, this, FwARLImpVector.getCPtr(savem), savem, FwARLImpVector.getCPtr(deleteem), deleteem);
  }

  public boolean Store(FwARLImp imp) {
    return (getClass() == FactoryParent_FwARLImp.class) ? ScWrapJNI.FactoryParent_FwARLImp_Store__SWIG_2(swigCPtr, this, FwARLImp.getCPtr(imp), imp) : ScWrapJNI.FactoryParent_FwARLImp_StoreSwigExplicitFactoryParent_FwARLImp__SWIG_2(swigCPtr, this, FwARLImp.getCPtr(imp), imp);
  }

  public boolean Store() {
    return ScWrapJNI.FactoryParent_FwARLImp_Store__SWIG_3(swigCPtr, this);
  }

  public boolean DeleteAll() {
    return ScWrapJNI.FactoryParent_FwARLImp_DeleteAll(swigCPtr, this);
  }

  public void CancelAll() {
    if (getClass() == FactoryParent_FwARLImp.class) ScWrapJNI.FactoryParent_FwARLImp_CancelAll(swigCPtr, this); else ScWrapJNI.FactoryParent_FwARLImp_CancelAllSwigExplicitFactoryParent_FwARLImp(swigCPtr, this);
  }

  public void Cancel(FwARLImp arg0) {
    ScWrapJNI.FactoryParent_FwARLImp_Cancel(swigCPtr, this, FwARLImp.getCPtr(arg0), arg0);
  }

  public void SyncList(FwARLImpVector arg0) {
    ScWrapJNI.FactoryParent_FwARLImp_SyncList(swigCPtr, this, FwARLImpVector.getCPtr(arg0), arg0);
  }

  public FwARLImp SyncImp(FwARLImp arg0) {
    long cPtr = ScWrapJNI.FactoryParent_FwARLImp_SyncImp(swigCPtr, this, FwARLImp.getCPtr(arg0), arg0);
    return (cPtr == 0) ? null : new FwARLImp(cPtr, false);
  }

  public void AddID(FwARLImp arg0) {
    ScWrapJNI.FactoryParent_FwARLImp_AddID(swigCPtr, this, FwARLImp.getCPtr(arg0), arg0);
  }

  public void Cleanup() {
    ScWrapJNI.FactoryParent_FwARLImp_Cleanup(swigCPtr, this);
  }

  public void Set_FwParentImpFactory_JavaInstance(FactoryParent_FwARLImp ins) {
    ScWrapJNI.FactoryParent_FwARLImp_Set_FwParentImpFactory_JavaInstance(swigCPtr, this, FactoryParent_FwARLImp.getCPtr(ins), ins);
  }

  protected void AddNew(FwARLImp arg0) {
    if (getClass() == FactoryParent_FwARLImp.class) ScWrapJNI.FactoryParent_FwARLImp_AddNew(swigCPtr, this, FwARLImp.getCPtr(arg0), arg0); else ScWrapJNI.FactoryParent_FwARLImp_AddNewSwigExplicitFactoryParent_FwARLImp(swigCPtr, this, FwARLImp.getCPtr(arg0), arg0);
  }

  protected void AddDeleted(FwARLImp arg0) {
    if (getClass() == FactoryParent_FwARLImp.class) ScWrapJNI.FactoryParent_FwARLImp_AddDeleted(swigCPtr, this, FwARLImp.getCPtr(arg0), arg0); else ScWrapJNI.FactoryParent_FwARLImp_AddDeletedSwigExplicitFactoryParent_FwARLImp(swigCPtr, this, FwARLImp.getCPtr(arg0), arg0);
  }

  protected void DoCancel(FwARLImp arg0) {
    if (getClass() == FactoryParent_FwARLImp.class) ScWrapJNI.FactoryParent_FwARLImp_DoCancel(swigCPtr, this, FwARLImp.getCPtr(arg0), arg0); else ScWrapJNI.FactoryParent_FwARLImp_DoCancelSwigExplicitFactoryParent_FwARLImp(swigCPtr, this, FwARLImp.getCPtr(arg0), arg0);
  }

  protected void DoCancelAll() {
    if (getClass() == FactoryParent_FwARLImp.class) ScWrapJNI.FactoryParent_FwARLImp_DoCancelAll(swigCPtr, this); else ScWrapJNI.FactoryParent_FwARLImp_DoCancelAllSwigExplicitFactoryParent_FwARLImp(swigCPtr, this);
  }

  protected void DoGetAll(FwARLImpVector arg0) {
    if (getClass() == FactoryParent_FwARLImp.class) ScWrapJNI.FactoryParent_FwARLImp_DoGetAll(swigCPtr, this, FwARLImpVector.getCPtr(arg0), arg0); else ScWrapJNI.FactoryParent_FwARLImp_DoGetAllSwigExplicitFactoryParent_FwARLImp(swigCPtr, this, FwARLImpVector.getCPtr(arg0), arg0);
  }

}
