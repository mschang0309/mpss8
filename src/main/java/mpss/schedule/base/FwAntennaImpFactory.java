/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwAntennaImpFactory extends FactoryChild_FwAntennaImp {
  private long swigCPtr;

  protected FwAntennaImpFactory(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwAntennaImpFactory_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwAntennaImpFactory obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwAntennaImpFactory(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FwAntennaImpFactory_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FwAntennaImpFactory_change_ownership(this, swigCPtr, true);
  }

  public FwAntennaImp GetOne(int parentID, String name, String type) {
    long cPtr = (getClass() == FwAntennaImpFactory.class) ? ScWrapJNI.FwAntennaImpFactory_GetOne(swigCPtr, this, parentID, name, type) : ScWrapJNI.FwAntennaImpFactory_GetOneSwigExplicitFwAntennaImpFactory(swigCPtr, this, parentID, name, type);
    return (cPtr == 0) ? null : new FwAntennaImp(cPtr, false);
  }

  public FwAntennaImpFactory() {
    this(ScWrapJNI.new_FwAntennaImpFactory__SWIG_0(), true);
    ScWrapJNI.FwAntennaImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwAntennaImpFactory(FwAntennaImpFactory arg0) {
    this(ScWrapJNI.new_FwAntennaImpFactory__SWIG_1(FwAntennaImpFactory.getCPtr(arg0), arg0), true);
    ScWrapJNI.FwAntennaImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwAntennaImp CreateOne(int parentID, String name, String type) {
    long cPtr = (getClass() == FwAntennaImpFactory.class) ? ScWrapJNI.FwAntennaImpFactory_CreateOne(swigCPtr, this, parentID, name, type) : ScWrapJNI.FwAntennaImpFactory_CreateOneSwigExplicitFwAntennaImpFactory(swigCPtr, this, parentID, name, type);
    return (cPtr == 0) ? null : new FwAntennaImp(cPtr, false);
  }

}
