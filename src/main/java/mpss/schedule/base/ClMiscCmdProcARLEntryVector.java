/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClMiscCmdProcARLEntryVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClMiscCmdProcARLEntryVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClMiscCmdProcARLEntryVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClMiscCmdProcARLEntryVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClMiscCmdProcARLEntryVector() {
    this(ScWrapJNI.new_ClMiscCmdProcARLEntryVector(), true);
  }

  public long size() {
    return ScWrapJNI.ClMiscCmdProcARLEntryVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.ClMiscCmdProcARLEntryVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.ClMiscCmdProcARLEntryVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.ClMiscCmdProcARLEntryVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.ClMiscCmdProcARLEntryVector_clear(swigCPtr, this);
  }

  public void add(ClMiscCmdProcARLEntry x) {
    ScWrapJNI.ClMiscCmdProcARLEntryVector_add(swigCPtr, this, ClMiscCmdProcARLEntry.getCPtr(x), x);
  }

  public ClMiscCmdProcARLEntry get(int i) {
    return new ClMiscCmdProcARLEntry(ScWrapJNI.ClMiscCmdProcARLEntryVector_get(swigCPtr, this, i), false);
  }

  public void set(int i, ClMiscCmdProcARLEntry val) {
    ScWrapJNI.ClMiscCmdProcARLEntryVector_set(swigCPtr, this, i, ClMiscCmdProcARLEntry.getCPtr(val), val);
  }

}
