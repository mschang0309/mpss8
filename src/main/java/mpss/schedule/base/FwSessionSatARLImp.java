/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwSessionSatARLImp extends FwBaseImp {
  private long swigCPtr;

  protected FwSessionSatARLImp(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwSessionSatARLImp_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwSessionSatARLImp obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    //delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwSessionSatARLImp(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public int GetSessionID() {
    return ScWrapJNI.FwSessionSatARLImp_GetSessionID(swigCPtr, this);
  }

  public boolean SetSessionID(int SessionID) {
    return ScWrapJNI.FwSessionSatARLImp_SetSessionID(swigCPtr, this, SessionID);
  }

  public int GetSatelliteID() {
    return ScWrapJNI.FwSessionSatARLImp_GetSatelliteID(swigCPtr, this);
  }

  public boolean SetSatelliteID(int SatelliteID) {
    return ScWrapJNI.FwSessionSatARLImp_SetSatelliteID(swigCPtr, this, SatelliteID);
  }

  public int GetARLID() {
    return ScWrapJNI.FwSessionSatARLImp_GetARLID(swigCPtr, this);
  }

  public boolean SetARLID(int ARLID) {
    return ScWrapJNI.FwSessionSatARLImp_SetARLID(swigCPtr, this, ARLID);
  }

  public GlARL.Type GetARLType() {
    return GlARL.Type.swigToEnum(ScWrapJNI.FwSessionSatARLImp_GetARLType(swigCPtr, this));
  }

  public boolean SetARLType(GlARL.Type ARLType) {
    return ScWrapJNI.FwSessionSatARLImp_SetARLType(swigCPtr, this, ARLType.swigValue());
  }

  public String CheckMeString() {
    return ScWrapJNI.FwSessionSatARLImp_CheckMeString(swigCPtr, this);
  }

  public FwSessionSatARLImp() {
    this(ScWrapJNI.new_FwSessionSatARLImp__SWIG_0(), true);
  }

  public FwSessionSatARLImp(int id, int SessionId, int SatelliteId, int ARLID, GlARL.Type Type, boolean arg5) {
    this(ScWrapJNI.new_FwSessionSatARLImp__SWIG_1(id, SessionId, SatelliteId, ARLID, Type.swigValue(), arg5), true);
  }

}
