/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwMemoryPartitionImpVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected FwMemoryPartitionImpVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwMemoryPartitionImpVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    //delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwMemoryPartitionImpVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public FwMemoryPartitionImpVector() {
    this(ScWrapJNI.new_FwMemoryPartitionImpVector__SWIG_0(), true);
  }

  public FwMemoryPartitionImpVector(long n) {
    this(ScWrapJNI.new_FwMemoryPartitionImpVector__SWIG_1(n), true);
  }

  public long size() {
    return ScWrapJNI.FwMemoryPartitionImpVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.FwMemoryPartitionImpVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.FwMemoryPartitionImpVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.FwMemoryPartitionImpVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.FwMemoryPartitionImpVector_clear(swigCPtr, this);
  }

  public void add(FwMemoryPartitionImp x) {
    ScWrapJNI.FwMemoryPartitionImpVector_add(swigCPtr, this, FwMemoryPartitionImp.getCPtr(x), x);
  }

  public FwMemoryPartitionImp get(int i) {
    long cPtr = ScWrapJNI.FwMemoryPartitionImpVector_get(swigCPtr, this, i);
    return (cPtr == 0) ? null : new FwMemoryPartitionImp(cPtr, false);
  }

  public void set(int i, FwMemoryPartitionImp val) {
    ScWrapJNI.FwMemoryPartitionImpVector_set(swigCPtr, this, i, FwMemoryPartitionImp.getCPtr(val), val);
  }

}
