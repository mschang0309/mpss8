/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClTimeNotInSession extends exception {
  private long swigCPtr;

  protected ClTimeNotInSession(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.ClTimeNotInSession_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClTimeNotInSession obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClTimeNotInSession(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public ClTimeNotInSession(String s) {
    this(ScWrapJNI.new_ClTimeNotInSession(s), true);
  }

}
