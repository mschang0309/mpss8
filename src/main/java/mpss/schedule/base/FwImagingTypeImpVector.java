/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwImagingTypeImpVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected FwImagingTypeImpVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwImagingTypeImpVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    //delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwImagingTypeImpVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public FwImagingTypeImpVector() {
    this(ScWrapJNI.new_FwImagingTypeImpVector__SWIG_0(), true);
  }

  public FwImagingTypeImpVector(long n) {
    this(ScWrapJNI.new_FwImagingTypeImpVector__SWIG_1(n), true);
  }

  public long size() {
    return ScWrapJNI.FwImagingTypeImpVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.FwImagingTypeImpVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.FwImagingTypeImpVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.FwImagingTypeImpVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.FwImagingTypeImpVector_clear(swigCPtr, this);
  }

  public void add(FwImagingTypeImp x) {
    ScWrapJNI.FwImagingTypeImpVector_add(swigCPtr, this, FwImagingTypeImp.getCPtr(x), x);
  }

  public FwImagingTypeImp get(int i) {
    long cPtr = ScWrapJNI.FwImagingTypeImpVector_get(swigCPtr, this, i);
    return (cPtr == 0) ? null : new FwImagingTypeImp(cPtr, false);
  }

  public void set(int i, FwImagingTypeImp val) {
    ScWrapJNI.FwImagingTypeImpVector_set(swigCPtr, this, i, FwImagingTypeImp.getCPtr(val), val);
  }

}
