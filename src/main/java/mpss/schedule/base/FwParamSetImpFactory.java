/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwParamSetImpFactory extends FactoryNamed_FwParamSetImp {
  private long swigCPtr;

  protected FwParamSetImpFactory(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwParamSetImpFactory_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwParamSetImpFactory obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwParamSetImpFactory(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FwParamSetImpFactory_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FwParamSetImpFactory_change_ownership(this, swigCPtr, true);
  }

  public FwParamSetImp GetNewOne(String Name, String Comment, String FromPS) {
    long cPtr = ScWrapJNI.FwParamSetImpFactory_GetNewOne(swigCPtr, this, Name, Comment, FromPS);
    return (cPtr == 0) ? null : new FwParamSetImp(cPtr, false);
  }

  public FwParamSetImp DoGetOne(String Name) {
    long cPtr = (getClass() == FwParamSetImpFactory.class) ? ScWrapJNI.FwParamSetImpFactory_DoGetOne(swigCPtr, this, Name) : ScWrapJNI.FwParamSetImpFactory_DoGetOneSwigExplicitFwParamSetImpFactory(swigCPtr, this, Name);
    return (cPtr == 0) ? null : new FwParamSetImp(cPtr, false);
  }

  public FwParamSetImpFactory() {
    this(ScWrapJNI.new_FwParamSetImpFactory__SWIG_0(), true);
    ScWrapJNI.FwParamSetImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  public FwParamSetImp GetID(int arg0) {
    long cPtr = (getClass() == FwParamSetImpFactory.class) ? ScWrapJNI.FwParamSetImpFactory_GetID(swigCPtr, this, arg0) : ScWrapJNI.FwParamSetImpFactory_GetIDSwigExplicitFwParamSetImpFactory(swigCPtr, this, arg0);
    return (cPtr == 0) ? null : new FwParamSetImp(cPtr, false);
  }

  protected FwParamSetImpFactory(FwParamSetImpFactory arg0) {
    this(ScWrapJNI.new_FwParamSetImpFactory__SWIG_1(FwParamSetImpFactory.getCPtr(arg0), arg0), true);
    ScWrapJNI.FwParamSetImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwParamSetImp CreateOne(String Name, String Comment, String fromps) {
    long cPtr = (getClass() == FwParamSetImpFactory.class) ? ScWrapJNI.FwParamSetImpFactory_CreateOne(swigCPtr, this, Name, Comment, fromps) : ScWrapJNI.FwParamSetImpFactory_CreateOneSwigExplicitFwParamSetImpFactory(swigCPtr, this, Name, Comment, fromps);
    return (cPtr == 0) ? null : new FwParamSetImp(cPtr, false);
  }

  protected void DoCleanup() {
    if (getClass() == FwParamSetImpFactory.class) ScWrapJNI.FwParamSetImpFactory_DoCleanup(swigCPtr, this); else ScWrapJNI.FwParamSetImpFactory_DoCleanupSwigExplicitFwParamSetImpFactory(swigCPtr, this);
  }

}
