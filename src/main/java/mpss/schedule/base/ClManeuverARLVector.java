/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClManeuverARLVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClManeuverARLVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClManeuverARLVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClManeuverARLVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClManeuverARLVector() {
    this(ScWrapJNI.new_ClManeuverARLVector(), true);
  }

  public long size() {
    return ScWrapJNI.ClManeuverARLVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.ClManeuverARLVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.ClManeuverARLVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.ClManeuverARLVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.ClManeuverARLVector_clear(swigCPtr, this);
  }

  public void add(ClManeuverARL x) {
    ScWrapJNI.ClManeuverARLVector_add(swigCPtr, this, ClManeuverARL.getCPtr(x), x);
  }

  public ClManeuverARL get(int i) {
    return new ClManeuverARL(ScWrapJNI.ClManeuverARLVector_get(swigCPtr, this, i), false);
  }

  public void set(int i, ClManeuverARL val) {
    ScWrapJNI.ClManeuverARLVector_set(swigCPtr, this, i, ClManeuverARL.getCPtr(val), val);
  }

}
