/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwBaseARLEntryImpFactory extends FactoryChild_FwBaseARLEntryImp {
  private long swigCPtr;

  protected FwBaseARLEntryImpFactory(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwBaseARLEntryImpFactory_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwBaseARLEntryImpFactory obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwBaseARLEntryImpFactory(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FwBaseARLEntryImpFactory_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FwBaseARLEntryImpFactory_change_ownership(this, swigCPtr, true);
  }

  public FwBaseARLEntryImp GetID(int arg0) {
    long cPtr = (getClass() == FwBaseARLEntryImpFactory.class) ? ScWrapJNI.FwBaseARLEntryImpFactory_GetID(swigCPtr, this, arg0) : ScWrapJNI.FwBaseARLEntryImpFactory_GetIDSwigExplicitFwBaseARLEntryImpFactory(swigCPtr, this, arg0);
    return (cPtr == 0) ? null : new FwBaseARLEntryImp(cPtr, false);
  }

  public void GetAll(FwBaseARLEntryImpVector arg0, int parentID) {
    if (getClass() == FwBaseARLEntryImpFactory.class) ScWrapJNI.FwBaseARLEntryImpFactory_GetAll(swigCPtr, this, FwBaseARLEntryImpVector.getCPtr(arg0), arg0, parentID); else ScWrapJNI.FwBaseARLEntryImpFactory_GetAllSwigExplicitFwBaseARLEntryImpFactory(swigCPtr, this, FwBaseARLEntryImpVector.getCPtr(arg0), arg0, parentID);
  }

  public void Delete(FwBaseARLEntryImp arg0) {
    if (getClass() == FwBaseARLEntryImpFactory.class) ScWrapJNI.FwBaseARLEntryImpFactory_Delete__SWIG_0(swigCPtr, this, FwBaseARLEntryImp.getCPtr(arg0), arg0); else ScWrapJNI.FwBaseARLEntryImpFactory_DeleteSwigExplicitFwBaseARLEntryImpFactory__SWIG_0(swigCPtr, this, FwBaseARLEntryImp.getCPtr(arg0), arg0);
  }

  public void Delete(FwBaseARLEntryImpVector arg0) {
    if (getClass() == FwBaseARLEntryImpFactory.class) ScWrapJNI.FwBaseARLEntryImpFactory_Delete__SWIG_1(swigCPtr, this, FwBaseARLEntryImpVector.getCPtr(arg0), arg0); else ScWrapJNI.FwBaseARLEntryImpFactory_DeleteSwigExplicitFwBaseARLEntryImpFactory__SWIG_1(swigCPtr, this, FwBaseARLEntryImpVector.getCPtr(arg0), arg0);
  }

  public void CancelAll() {
    if (getClass() == FwBaseARLEntryImpFactory.class) ScWrapJNI.FwBaseARLEntryImpFactory_CancelAll(swigCPtr, this); else ScWrapJNI.FwBaseARLEntryImpFactory_CancelAllSwigExplicitFwBaseARLEntryImpFactory(swigCPtr, this);
  }

  protected FwBaseARLEntryImpFactory() {
    this(ScWrapJNI.new_FwBaseARLEntryImpFactory__SWIG_0(), true);
    ScWrapJNI.FwBaseARLEntryImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwBaseARLEntryImpFactory(FwBaseARLEntryImpFactory arg0) {
    this(ScWrapJNI.new_FwBaseARLEntryImpFactory__SWIG_1(FwBaseARLEntryImpFactory.getCPtr(arg0), arg0), true);
    ScWrapJNI.FwBaseARLEntryImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

}
