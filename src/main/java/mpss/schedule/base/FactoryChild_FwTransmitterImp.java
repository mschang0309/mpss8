/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FactoryChild_FwTransmitterImp extends FwBaseImpFactory {
  private long swigCPtr;

  protected FactoryChild_FwTransmitterImp(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FactoryChild_FwTransmitterImp_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FactoryChild_FwTransmitterImp obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FactoryChild_FwTransmitterImp(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FactoryChild_FwTransmitterImp_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FactoryChild_FwTransmitterImp_change_ownership(this, swigCPtr, true);
  }

  public FwTransmitterImp GetID(int arg0) {
    long cPtr = ScWrapJNI.FactoryChild_FwTransmitterImp_GetID(swigCPtr, this, arg0);
    return (cPtr == 0) ? null : new FwTransmitterImp(cPtr, false);
  }

  public void GetAll(FwTransmitterImpVector arg0, int parentID) {
    if (getClass() == FactoryChild_FwTransmitterImp.class) ScWrapJNI.FactoryChild_FwTransmitterImp_GetAll(swigCPtr, this, FwTransmitterImpVector.getCPtr(arg0), arg0, parentID); else ScWrapJNI.FactoryChild_FwTransmitterImp_GetAllSwigExplicitFactoryChild_FwTransmitterImp(swigCPtr, this, FwTransmitterImpVector.getCPtr(arg0), arg0, parentID);
  }

  public boolean Store(FwTransmitterImpVector imps, FwTransmitterImpVector delimps) {
    return (getClass() == FactoryChild_FwTransmitterImp.class) ? ScWrapJNI.FactoryChild_FwTransmitterImp_Store__SWIG_0(swigCPtr, this, FwTransmitterImpVector.getCPtr(imps), imps, FwTransmitterImpVector.getCPtr(delimps), delimps) : ScWrapJNI.FactoryChild_FwTransmitterImp_StoreSwigExplicitFactoryChild_FwTransmitterImp__SWIG_0(swigCPtr, this, FwTransmitterImpVector.getCPtr(imps), imps, FwTransmitterImpVector.getCPtr(delimps), delimps);
  }

  public boolean Store(FwTransmitterImp imp) {
    return (getClass() == FactoryChild_FwTransmitterImp.class) ? ScWrapJNI.FactoryChild_FwTransmitterImp_Store__SWIG_1(swigCPtr, this, FwTransmitterImp.getCPtr(imp), imp) : ScWrapJNI.FactoryChild_FwTransmitterImp_StoreSwigExplicitFactoryChild_FwTransmitterImp__SWIG_1(swigCPtr, this, FwTransmitterImp.getCPtr(imp), imp);
  }

  public void Delete(FwTransmitterImp arg0) {
    if (getClass() == FactoryChild_FwTransmitterImp.class) ScWrapJNI.FactoryChild_FwTransmitterImp_Delete__SWIG_0(swigCPtr, this, FwTransmitterImp.getCPtr(arg0), arg0); else ScWrapJNI.FactoryChild_FwTransmitterImp_DeleteSwigExplicitFactoryChild_FwTransmitterImp__SWIG_0(swigCPtr, this, FwTransmitterImp.getCPtr(arg0), arg0);
  }

  public void Delete(FwTransmitterImpVector arg0) {
    if (getClass() == FactoryChild_FwTransmitterImp.class) ScWrapJNI.FactoryChild_FwTransmitterImp_Delete__SWIG_1(swigCPtr, this, FwTransmitterImpVector.getCPtr(arg0), arg0); else ScWrapJNI.FactoryChild_FwTransmitterImp_DeleteSwigExplicitFactoryChild_FwTransmitterImp__SWIG_1(swigCPtr, this, FwTransmitterImpVector.getCPtr(arg0), arg0);
  }

  public void Set_FwChildImpFactory_JavaInstance(FactoryChild_FwTransmitterImp ins) {
    ScWrapJNI.FactoryChild_FwTransmitterImp_Set_FwChildImpFactory_JavaInstance(swigCPtr, this, FactoryChild_FwTransmitterImp.getCPtr(ins), ins);
  }

  protected FactoryChild_FwTransmitterImp() {
    this(ScWrapJNI.new_FactoryChild_FwTransmitterImp(), true);
    ScWrapJNI.FactoryChild_FwTransmitterImp_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

}
