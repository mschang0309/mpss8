/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwRecorderImpFactory extends FactoryChild_FwRecorderImp {
  private long swigCPtr;

  protected FwRecorderImpFactory(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwRecorderImpFactory_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwRecorderImpFactory obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwRecorderImpFactory(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FwRecorderImpFactory_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FwRecorderImpFactory_change_ownership(this, swigCPtr, true);
  }

  public FwRecorderImp GetOne(int parentID, String name, int SectorSize, int TotalFiles, int MaxRsiFiles, int MaxIsualFiles, int MaxDdtFiles) {
    long cPtr = (getClass() == FwRecorderImpFactory.class) ? ScWrapJNI.FwRecorderImpFactory_GetOne(swigCPtr, this, parentID, name, SectorSize, TotalFiles, MaxRsiFiles, MaxIsualFiles, MaxDdtFiles) : ScWrapJNI.FwRecorderImpFactory_GetOneSwigExplicitFwRecorderImpFactory(swigCPtr, this, parentID, name, SectorSize, TotalFiles, MaxRsiFiles, MaxIsualFiles, MaxDdtFiles);
    return (cPtr == 0) ? null : new FwRecorderImp(cPtr, false);
  }

  public FwRecorderImpFactory() {
    this(ScWrapJNI.new_FwRecorderImpFactory__SWIG_0(), true);
    ScWrapJNI.FwRecorderImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwRecorderImpFactory(FwRecorderImpFactory arg0) {
    this(ScWrapJNI.new_FwRecorderImpFactory__SWIG_1(FwRecorderImpFactory.getCPtr(arg0), arg0), true);
    ScWrapJNI.FwRecorderImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwRecorderImp CreateOne(int parentID, String name, int SectorSize, int TotalFiles, int MaxRsiFiles, int MaxIsualFiles, int MaxDdtFiles) {
    long cPtr = (getClass() == FwRecorderImpFactory.class) ? ScWrapJNI.FwRecorderImpFactory_CreateOne(swigCPtr, this, parentID, name, SectorSize, TotalFiles, MaxRsiFiles, MaxIsualFiles, MaxDdtFiles) : ScWrapJNI.FwRecorderImpFactory_CreateOneSwigExplicitFwRecorderImpFactory(swigCPtr, this, parentID, name, SectorSize, TotalFiles, MaxRsiFiles, MaxIsualFiles, MaxDdtFiles);
    return (cPtr == 0) ? null : new FwRecorderImp(cPtr, false);
  }

}
