/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClTransmitterVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClTransmitterVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClTransmitterVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClTransmitterVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClTransmitterVector() {
    this(ScWrapJNI.new_ClTransmitterVector(), true);
  }

  public long size() {
    return ScWrapJNI.ClTransmitterVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.ClTransmitterVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.ClTransmitterVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.ClTransmitterVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.ClTransmitterVector_clear(swigCPtr, this);
  }

  public void add(ClTransmitter x) {
    ScWrapJNI.ClTransmitterVector_add(swigCPtr, this, ClTransmitter.getCPtr(x), x);
  }

  public ClTransmitter get(int i) {
    return new ClTransmitter(ScWrapJNI.ClTransmitterVector_get(swigCPtr, this, i), false);
  }

  public void set(int i, ClTransmitter val) {
    ScWrapJNI.ClTransmitterVector_set(swigCPtr, this, i, ClTransmitter.getCPtr(val), val);
  }

}
