/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClISUALImagingARLVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClISUALImagingARLVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClISUALImagingARLVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClISUALImagingARLVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClISUALImagingARLVector() {
    this(ScWrapJNI.new_ClISUALImagingARLVector(), true);
  }

  public long size() {
    return ScWrapJNI.ClISUALImagingARLVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.ClISUALImagingARLVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.ClISUALImagingARLVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.ClISUALImagingARLVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.ClISUALImagingARLVector_clear(swigCPtr, this);
  }

  public void add(ClISUALImagingARL x) {
    ScWrapJNI.ClISUALImagingARLVector_add(swigCPtr, this, ClISUALImagingARL.getCPtr(x), x);
  }

  public ClISUALImagingARL get(int i) {
    return new ClISUALImagingARL(ScWrapJNI.ClISUALImagingARLVector_get(swigCPtr, this, i), false);
  }

  public void set(int i, ClISUALImagingARL val) {
    ScWrapJNI.ClISUALImagingARLVector_set(swigCPtr, this, i, ClISUALImagingARL.getCPtr(val), val);
  }

}
