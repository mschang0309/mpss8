/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClUplinkRequestsARLEntryVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClUplinkRequestsARLEntryVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClUplinkRequestsARLEntryVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClUplinkRequestsARLEntryVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClUplinkRequestsARLEntryVector() {
    this(ScWrapJNI.new_ClUplinkRequestsARLEntryVector(), true);
  }

  public long size() {
    return ScWrapJNI.ClUplinkRequestsARLEntryVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.ClUplinkRequestsARLEntryVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.ClUplinkRequestsARLEntryVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.ClUplinkRequestsARLEntryVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.ClUplinkRequestsARLEntryVector_clear(swigCPtr, this);
  }

  public void add(ClUplinkRequestsARLEntry x) {
    ScWrapJNI.ClUplinkRequestsARLEntryVector_add(swigCPtr, this, ClUplinkRequestsARLEntry.getCPtr(x), x);
  }

  public ClUplinkRequestsARLEntry get(int i) {
    return new ClUplinkRequestsARLEntry(ScWrapJNI.ClUplinkRequestsARLEntryVector_get(swigCPtr, this, i), false);
  }

  public void set(int i, ClUplinkRequestsARLEntry val) {
    ScWrapJNI.ClUplinkRequestsARLEntryVector_set(swigCPtr, this, i, ClUplinkRequestsARLEntry.getCPtr(val), val);
  }

}
