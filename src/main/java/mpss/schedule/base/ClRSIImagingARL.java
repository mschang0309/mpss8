/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClRSIImagingARL extends ClARL {
  private long swigCPtr;

  protected ClRSIImagingARL(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.ClRSIImagingARL_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClRSIImagingARL obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClRSIImagingARL(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public static void Get(ClRSIImagingARLVector arg0) {
    ScWrapJNI.ClRSIImagingARL_Get(ClRSIImagingARLVector.getCPtr(arg0), arg0);
  }

  public ClRSIImagingARLEntry AddEntry(GlDaySpec Date, long start, long duration, ClImagingMode imagingMode, String station, String elevation, String azimuth, int orbit, int trackgi, int gridjs, int gridje, int gridk, double startLat, double stopLat, ClImagingType imageType, GlARL.SubType ssrStatus, GlRecorder.Rate panCompRatio, GlRecorder.Rate msCompRatio, String videoPanGain, String videoMb1Gain, String videoMb2Gain, String videoMb3Gain, String videoMb4Gain, String comments) {
    return new ClRSIImagingARLEntry(ScWrapJNI.ClRSIImagingARL_AddEntry(swigCPtr, this, GlDaySpec.getCPtr(Date), Date, start, duration, ClImagingMode.getCPtr(imagingMode), imagingMode, station, elevation, azimuth, orbit, trackgi, gridjs, gridje, gridk, startLat, stopLat, ClImagingType.getCPtr(imageType), imageType, ssrStatus.swigValue(), panCompRatio.swigValue(), msCompRatio.swigValue(), videoPanGain, videoMb1Gain, videoMb2Gain, videoMb3Gain, videoMb4Gain, comments), true);
  }

  public boolean RemoveEntry(ClRSIImagingARLEntry arg0) {
    return ScWrapJNI.ClRSIImagingARL_RemoveEntry(swigCPtr, this, ClRSIImagingARLEntry.getCPtr(arg0), arg0);
  }

  public void GetEntries(ClRSIImagingARLEntryVector arg0) {
    ScWrapJNI.ClRSIImagingARL_GetEntries(swigCPtr, this, ClRSIImagingARLEntryVector.getCPtr(arg0), arg0);
  }

  public ClRSIImagingARL(int arg0) {
    this(ScWrapJNI.new_ClRSIImagingARL__SWIG_0(arg0), true);
  }

  public ClRSIImagingARL(String name, boolean Shadow) {
    this(ScWrapJNI.new_ClRSIImagingARL__SWIG_1(name, Shadow), true);
  }

  public ClRSIImagingARL(String name) {
    this(ScWrapJNI.new_ClRSIImagingARL__SWIG_2(name), true);
  }

}
