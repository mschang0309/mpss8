/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClISUALImagingARLEntry extends ClBaseARLEntry {
  private long swigCPtr;

  protected ClISUALImagingARLEntry(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.ClISUALImagingARLEntry_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClISUALImagingARLEntry obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClISUALImagingARLEntry(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public ClISUALImagingARLEntry(ClActivity activity) {
    this(ScWrapJNI.new_ClISUALImagingARLEntry(ClActivity.getCPtr(activity), activity), true);
  }

  public void PrintOn(SWIGTYPE_p_std__ostream os) {
    ScWrapJNI.ClISUALImagingARLEntry_PrintOn(swigCPtr, this, SWIGTYPE_p_std__ostream.getCPtr(os));
  }

  public boolean _Equal_(ClISUALImagingARLEntry rhs) {
    return ScWrapJNI.ClISUALImagingARLEntry__Equal_(swigCPtr, this, ClISUALImagingARLEntry.getCPtr(rhs), rhs);
  }

  public boolean _NotEqual_(ClISUALImagingARLEntry rhs) {
    return ScWrapJNI.ClISUALImagingARLEntry__NotEqual_(swigCPtr, this, ClISUALImagingARLEntry.getCPtr(rhs), rhs);
  }

  public int GetID() {
    return ScWrapJNI.ClISUALImagingARLEntry_GetID(swigCPtr, this);
  }

  public GlDaySpec GetDate() {
    return new GlDaySpec(ScWrapJNI.ClISUALImagingARLEntry_GetDate(swigCPtr, this), true);
  }

  public void SetDate(GlDaySpec Date) {
    ScWrapJNI.ClISUALImagingARLEntry_SetDate(swigCPtr, this, GlDaySpec.getCPtr(Date), Date);
  }

  public GlARL.SubTrigger GetContactOccurrence() {
    return GlARL.SubTrigger.swigToEnum(ScWrapJNI.ClISUALImagingARLEntry_GetContactOccurrence(swigCPtr, this));
  }

  public void SetContactOccurrence(GlARL.SubTrigger trig) {
    ScWrapJNI.ClISUALImagingARLEntry_SetContactOccurrence(swigCPtr, this, trig.swigValue());
  }

  public String GetFileName() {
    return ScWrapJNI.ClISUALImagingARLEntry_GetFileName(swigCPtr, this);
  }

  public void SetFileName(String pn) {
    ScWrapJNI.ClISUALImagingARLEntry_SetFileName(swigCPtr, this, pn);
  }

  public boolean GetRemoteTrackingStationFlag() {
    return ScWrapJNI.ClISUALImagingARLEntry_GetRemoteTrackingStationFlag(swigCPtr, this);
  }

  public void SetRemoteTrackingStationFlag(boolean rts) {
    ScWrapJNI.ClISUALImagingARLEntry_SetRemoteTrackingStationFlag(swigCPtr, this, rts);
  }

  public String GetComments() {
    return ScWrapJNI.ClISUALImagingARLEntry_GetComments(swigCPtr, this);
  }

  public void SetComments(String Comments) {
    ScWrapJNI.ClISUALImagingARLEntry_SetComments(swigCPtr, this, Comments);
  }

  public int GetARLID() {
    return ScWrapJNI.ClISUALImagingARLEntry_GetARLID(swigCPtr, this);
  }

}
