/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwSatelliteImpFactory extends FactoryNamed_FwSatelliteImp {
  private long swigCPtr;

  protected FwSatelliteImpFactory(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwSatelliteImpFactory_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwSatelliteImpFactory obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwSatelliteImpFactory(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FwSatelliteImpFactory_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FwSatelliteImpFactory_change_ownership(this, swigCPtr, true);
  }

  public FwSatelliteImp DoGetOne(String arg0, GlSatellite.AliasType arg1) {
    long cPtr = (getClass() == FwSatelliteImpFactory.class) ? ScWrapJNI.FwSatelliteImpFactory_DoGetOne(swigCPtr, this, arg0, arg1.swigValue()) : ScWrapJNI.FwSatelliteImpFactory_DoGetOneSwigExplicitFwSatelliteImpFactory(swigCPtr, this, arg0, arg1.swigValue());
    return (cPtr == 0) ? null : new FwSatelliteImp(cPtr, false);
  }

  public FwSatelliteImp GetNewOne(String name, GlSatellite.Type type, String opsnum, String ops4num, long nominalOrbit, long maxANDriftTime, long epd, String videoGain) {
    long cPtr = (getClass() == FwSatelliteImpFactory.class) ? ScWrapJNI.FwSatelliteImpFactory_GetNewOne(swigCPtr, this, name, type.swigValue(), opsnum, ops4num, nominalOrbit, maxANDriftTime, epd, videoGain) : ScWrapJNI.FwSatelliteImpFactory_GetNewOneSwigExplicitFwSatelliteImpFactory(swigCPtr, this, name, type.swigValue(), opsnum, ops4num, nominalOrbit, maxANDriftTime, epd, videoGain);
    return (cPtr == 0) ? null : new FwSatelliteImp(cPtr, false);
  }

  public FwSatelliteImpFactory() {
    this(ScWrapJNI.new_FwSatelliteImpFactory__SWIG_0(), true);
    ScWrapJNI.FwSatelliteImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  public FwSatelliteImp GetID(int arg0) {
    long cPtr = (getClass() == FwSatelliteImpFactory.class) ? ScWrapJNI.FwSatelliteImpFactory_GetID(swigCPtr, this, arg0) : ScWrapJNI.FwSatelliteImpFactory_GetIDSwigExplicitFwSatelliteImpFactory(swigCPtr, this, arg0);
    return (cPtr == 0) ? null : new FwSatelliteImp(cPtr, false);
  }

  public void GetAll(FwSatelliteImpVector arg0) {
    if (getClass() == FwSatelliteImpFactory.class) ScWrapJNI.FwSatelliteImpFactory_GetAll(swigCPtr, this, FwSatelliteImpVector.getCPtr(arg0), arg0); else ScWrapJNI.FwSatelliteImpFactory_GetAllSwigExplicitFwSatelliteImpFactory(swigCPtr, this, FwSatelliteImpVector.getCPtr(arg0), arg0);
  }

  public void DoGetAll(FwSatelliteImpVector arg0) {
    if (getClass() == FwSatelliteImpFactory.class) ScWrapJNI.FwSatelliteImpFactory_DoGetAll(swigCPtr, this, FwSatelliteImpVector.getCPtr(arg0), arg0); else ScWrapJNI.FwSatelliteImpFactory_DoGetAllSwigExplicitFwSatelliteImpFactory(swigCPtr, this, FwSatelliteImpVector.getCPtr(arg0), arg0);
  }

  public FwSatelliteImp GetOne(String arg0) {
    long cPtr = (getClass() == FwSatelliteImpFactory.class) ? ScWrapJNI.FwSatelliteImpFactory_GetOne(swigCPtr, this, arg0) : ScWrapJNI.FwSatelliteImpFactory_GetOneSwigExplicitFwSatelliteImpFactory(swigCPtr, this, arg0);
    return (cPtr == 0) ? null : new FwSatelliteImp(cPtr, false);
  }

  protected FwSatelliteImpFactory(FwSatelliteImpFactory arg0) {
    this(ScWrapJNI.new_FwSatelliteImpFactory__SWIG_1(FwSatelliteImpFactory.getCPtr(arg0), arg0), true);
    ScWrapJNI.FwSatelliteImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwSatelliteImp CreateOne(String name, GlSatellite.Type type, String opsnum, String ops4num, long nominalOrbit, long maxANDriftTime, long epd, String videoGain) {
    long cPtr = (getClass() == FwSatelliteImpFactory.class) ? ScWrapJNI.FwSatelliteImpFactory_CreateOne(swigCPtr, this, name, type.swigValue(), opsnum, ops4num, nominalOrbit, maxANDriftTime, epd, videoGain) : ScWrapJNI.FwSatelliteImpFactory_CreateOneSwigExplicitFwSatelliteImpFactory(swigCPtr, this, name, type.swigValue(), opsnum, ops4num, nominalOrbit, maxANDriftTime, epd, videoGain);
    return (cPtr == 0) ? null : new FwSatelliteImp(cPtr, false);
  }

  protected void DoCleanup() {
    if (getClass() == FwSatelliteImpFactory.class) ScWrapJNI.FwSatelliteImpFactory_DoCleanup(swigCPtr, this); else ScWrapJNI.FwSatelliteImpFactory_DoCleanupSwigExplicitFwSatelliteImpFactory(swigCPtr, this);
  }

}
