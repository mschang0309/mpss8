/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class GlDaylightRule {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected GlDaylightRule(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(GlDaylightRule obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_GlDaylightRule(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public void setNext_(GlDaylightRule value) {
    ScWrapJNI.GlDaylightRule_next__set(swigCPtr, this, GlDaylightRule.getCPtr(value), value);
  }

  public GlDaylightRule getNext_() {
    long cPtr = ScWrapJNI.GlDaylightRule_next__get(swigCPtr, this);
    return (cPtr == 0) ? null : new GlDaylightRule(cPtr, false);
  }

  public void setFirstYear_(short value) {
    ScWrapJNI.GlDaylightRule_firstYear__set(swigCPtr, this, value);
  }

  public short getFirstYear_() {
    return ScWrapJNI.GlDaylightRule_firstYear__get(swigCPtr, this);
  }

  public void setObserved_(char value) {
    ScWrapJNI.GlDaylightRule_observed__set(swigCPtr, this, value);
  }

  public char getObserved_() {
    return ScWrapJNI.GlDaylightRule_observed__get(swigCPtr, this);
  }

  public void setBegin_(GlDaylightBoundary value) {
    ScWrapJNI.GlDaylightRule_begin__set(swigCPtr, this, GlDaylightBoundary.getCPtr(value), value);
  }

  public GlDaylightBoundary getBegin_() {
    long cPtr = ScWrapJNI.GlDaylightRule_begin__get(swigCPtr, this);
    return (cPtr == 0) ? null : new GlDaylightBoundary(cPtr, false);
  }

  public void setEnd_(GlDaylightBoundary value) {
    ScWrapJNI.GlDaylightRule_end__set(swigCPtr, this, GlDaylightBoundary.getCPtr(value), value);
  }

  public GlDaylightBoundary getEnd_() {
    long cPtr = ScWrapJNI.GlDaylightRule_end__get(swigCPtr, this);
    return (cPtr == 0) ? null : new GlDaylightBoundary(cPtr, false);
  }

  public GlDaylightRule _ASIGN_(GlDaylightRule arg0) {
    return new GlDaylightRule(ScWrapJNI.GlDaylightRule__ASIGN_(swigCPtr, this, GlDaylightRule.getCPtr(arg0), arg0), false);
  }

  public boolean isDaylight(SWIGTYPE_p_tm arg0) {
    return ScWrapJNI.GlDaylightRule_isDaylight(swigCPtr, this, SWIGTYPE_p_tm.getCPtr(arg0));
  }

  public GlDaylightRule() {
    this(ScWrapJNI.new_GlDaylightRule(), true);
  }

}
