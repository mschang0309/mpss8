/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClRecorderPtrVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClRecorderPtrVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClRecorderPtrVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClRecorderPtrVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClRecorderPtrVector() {
    this(ScWrapJNI.new_ClRecorderPtrVector__SWIG_0(), true);
  }

  public ClRecorderPtrVector(long n) {
    this(ScWrapJNI.new_ClRecorderPtrVector__SWIG_1(n), true);
  }

  public long size() {
    return ScWrapJNI.ClRecorderPtrVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.ClRecorderPtrVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.ClRecorderPtrVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.ClRecorderPtrVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.ClRecorderPtrVector_clear(swigCPtr, this);
  }

  public void add(ClRecorder x) {
    ScWrapJNI.ClRecorderPtrVector_add(swigCPtr, this, ClRecorder.getCPtr(x), x);
  }

  public ClRecorder get(int i) {
    long cPtr = ScWrapJNI.ClRecorderPtrVector_get(swigCPtr, this, i);
    return (cPtr == 0) ? null : new ClRecorder(cPtr, false);
  }

  public void set(int i, ClRecorder val) {
    ScWrapJNI.ClRecorderPtrVector_set(swigCPtr, this, i, ClRecorder.getCPtr(val), val);
  }

}
