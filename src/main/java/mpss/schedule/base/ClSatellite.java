/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClSatellite {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClSatellite(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClSatellite obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClSatellite(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClSatellite(String name) {
    this(ScWrapJNI.new_ClSatellite__SWIG_0(name), true);
  }

  public ClSatellite(String name, GlSatellite.AliasType arg1) {
    this(ScWrapJNI.new_ClSatellite__SWIG_1(name, arg1.swigValue()), true);
  }

  public ClSatellite(int id) {
    this(ScWrapJNI.new_ClSatellite__SWIG_2(id), true);
  }

  public ClSatellite(String name, GlSatellite.Type type, String opsnum, String ops4num, long nominalOrbit, long maxANDriftTime, long epd, String videoGain) {
    this(ScWrapJNI.new_ClSatellite__SWIG_3(name, type.swigValue(), opsnum, ops4num, nominalOrbit, maxANDriftTime, epd, videoGain), true);
  }

  public ClSatellite(String arg0, String ops4name) {
    this(ScWrapJNI.new_ClSatellite__SWIG_4(arg0, ops4name), true);
  }

  public ClSatellite _Asign_(ClSatellite arg0) {
    return new ClSatellite(ScWrapJNI.ClSatellite__Asign_(swigCPtr, this, ClSatellite.getCPtr(arg0), arg0), false);
  }

  public ClSatellite(ClSatellite arg0) {
    this(ScWrapJNI.new_ClSatellite__SWIG_5(ClSatellite.getCPtr(arg0), arg0), true);
  }

  public boolean _LessThan_(ClSatellite arg0) {
    return ScWrapJNI.ClSatellite__LessThan_(swigCPtr, this, ClSatellite.getCPtr(arg0), arg0);
  }

  public boolean _LessThanOrEqual_(ClSatellite rhs) {
    return ScWrapJNI.ClSatellite__LessThanOrEqual_(swigCPtr, this, ClSatellite.getCPtr(rhs), rhs);
  }

  public boolean _GreaterThan_(ClSatellite rhs) {
    return ScWrapJNI.ClSatellite__GreaterThan_(swigCPtr, this, ClSatellite.getCPtr(rhs), rhs);
  }

  public boolean _GreaterThanOrEqual_(ClSatellite rhs) {
    return ScWrapJNI.ClSatellite__GreaterThanOrEqual_(swigCPtr, this, ClSatellite.getCPtr(rhs), rhs);
  }

  public boolean _Equal_(ClSatellite arg0) {
    return ScWrapJNI.ClSatellite__Equal_(swigCPtr, this, ClSatellite.getCPtr(arg0), arg0);
  }

  public boolean _NotEqual_(ClSatellite rhs) {
    return ScWrapJNI.ClSatellite__NotEqual_(swigCPtr, this, ClSatellite.getCPtr(rhs), rhs);
  }

  public boolean Store() {
    return ScWrapJNI.ClSatellite_Store(swigCPtr, this);
  }

  public void Delete() {
    ScWrapJNI.ClSatellite_Delete(swigCPtr, this);
  }

  public void Cancel() {
    ScWrapJNI.ClSatellite_Cancel(swigCPtr, this);
  }

  public void PrintOn(SWIGTYPE_p_std__ostream os) {
    ScWrapJNI.ClSatellite_PrintOn(swigCPtr, this, SWIGTYPE_p_std__ostream.getCPtr(os));
  }

  public int GetID() {
    return ScWrapJNI.ClSatellite_GetID(swigCPtr, this);
  }

  public String GetName() {
    return ScWrapJNI.ClSatellite_GetName(swigCPtr, this);
  }

  public boolean SetName(String arg0) {
    return ScWrapJNI.ClSatellite_SetName(swigCPtr, this, arg0);
  }

  public GlSatellite.Type GetType() {
    return GlSatellite.Type.swigToEnum(ScWrapJNI.ClSatellite_GetType(swigCPtr, this));
  }

  public void SetType(GlSatellite.Type arg0) {
    ScWrapJNI.ClSatellite_SetType(swigCPtr, this, arg0.swigValue());
  }

  public long GetNominalOrbit() {
    return ScWrapJNI.ClSatellite_GetNominalOrbit(swigCPtr, this);
  }

  public void SetNominalOrbit(long arg0) {
    ScWrapJNI.ClSatellite_SetNominalOrbit(swigCPtr, this, arg0);
  }

  public long GetMaxANDriftTime() {
    return ScWrapJNI.ClSatellite_GetMaxANDriftTime(swigCPtr, this);
  }

  public void SetMaxANDriftTime(long arg0) {
    ScWrapJNI.ClSatellite_SetMaxANDriftTime(swigCPtr, this, arg0);
  }

  public long GetEphPropDuration() {
    return ScWrapJNI.ClSatellite_GetEphPropDuration(swigCPtr, this);
  }

  public void SetEphPropDuration(long arg0) {
    ScWrapJNI.ClSatellite_SetEphPropDuration(swigCPtr, this, arg0);
  }

  public int GetPriority(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetPriority(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetPriority(ClParamSet arg0, int arg1) {
    ScWrapJNI.ClSatellite_SetPriority(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, arg1);
  }

  public String GetOPSNumber() {
    return ScWrapJNI.ClSatellite_GetOPSNumber(swigCPtr, this);
  }

  public void SetOPSNumber(String arg0) {
    ScWrapJNI.ClSatellite_SetOPSNumber(swigCPtr, this, arg0);
  }

  public String GetOPS4Number() {
    return ScWrapJNI.ClSatellite_GetOPS4Number(swigCPtr, this);
  }

  public void SetOPS4Number(String arg0) {
    ScWrapJNI.ClSatellite_SetOPS4Number(swigCPtr, this, arg0);
  }

  public long GetTwtaHeatDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetTwtaHeatDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetTwtaHeatDelay(ClParamSet arg0, long TwtaHeatDelay) {
    ScWrapJNI.ClSatellite_SetTwtaHeatDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, TwtaHeatDelay);
  }

  public long GetSlewManeuverDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetSlewManeuverDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetSlewManeuverDelay(ClParamSet arg0, long SlewManeuverDelay) {
    ScWrapJNI.ClSatellite_SetSlewManeuverDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, SlewManeuverDelay);
  }

  public long GetRsiInitialPreheatDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetRsiInitialPreheatDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetRsiInitialPreheatDelay(ClParamSet arg0, long RsiInitialPreheatDelay) {
    ScWrapJNI.ClSatellite_SetRsiInitialPreheatDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, RsiInitialPreheatDelay);
  }

  public long GetRsiSecondaryPreheatDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetRsiSecondaryPreheatDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetRsiSecondaryPreheatDelay(ClParamSet arg0, long RsiSecondaryPreheatDelay) {
    ScWrapJNI.ClSatellite_SetRsiSecondaryPreheatDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, RsiSecondaryPreheatDelay);
  }

  public long GetRsiFocalPlaneDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetRsiFocalPlaneDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetRsiFocalPlaneDelay(ClParamSet arg0, long RsiFocalPlaneDelay) {
    ScWrapJNI.ClSatellite_SetRsiFocalPlaneDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, RsiFocalPlaneDelay);
  }

  public long GetRsiRecSetupDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetRsiRecSetupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetRsiRecSetupDelay(ClParamSet arg0, long RsiRecSetupDelay) {
    ScWrapJNI.ClSatellite_SetRsiRecSetupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, RsiRecSetupDelay);
  }

  public long GetRsiRecCleanupDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetRsiRecCleanupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetRsiRecCleanupDelay(ClParamSet arg0, long RsiRecCleanupDelay) {
    ScWrapJNI.ClSatellite_SetRsiRecCleanupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, RsiRecCleanupDelay);
  }

  public long GetRsiPBSetupDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetRsiPBSetupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetRsiPBSetupDelay(ClParamSet arg0, long RsiPBSetupDelay) {
    ScWrapJNI.ClSatellite_SetRsiPBSetupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, RsiPBSetupDelay);
  }

  public long GetRsiPBCleanupDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetRsiPBCleanupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetRsiPBCleanupDelay(ClParamSet arg0, long RsiPBCleanupDelay) {
    ScWrapJNI.ClSatellite_SetRsiPBCleanupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, RsiPBCleanupDelay);
  }

  public long GetIsualRecSetupDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetIsualRecSetupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetIsualRecSetupDelay(ClParamSet arg0, long IsualRecSetupDelay) {
    ScWrapJNI.ClSatellite_SetIsualRecSetupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, IsualRecSetupDelay);
  }

  public long GetIsualRecCleanupDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetIsualRecCleanupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetIsualRecCleanupDelay(ClParamSet arg0, long IsualRecCleanupDelay) {
    ScWrapJNI.ClSatellite_SetIsualRecCleanupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, IsualRecCleanupDelay);
  }

  public long GetIsualPBSetupDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetIsualPBSetupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetIsualPBSetupDelay(ClParamSet arg0, long IsualPBSetupDelay) {
    ScWrapJNI.ClSatellite_SetIsualPBSetupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, IsualPBSetupDelay);
  }

  public long GetIsualPBCleanupDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetIsualPBCleanupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetIsualPBCleanupDelay(ClParamSet arg0, long IsualPBCleanupDelay) {
    ScWrapJNI.ClSatellite_SetIsualPBCleanupDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, IsualPBCleanupDelay);
  }

  public double GetPostDDTDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetPostDDTDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetPostDDTDelay(ClParamSet arg0, double PostDDTDelay) {
    ScWrapJNI.ClSatellite_SetPostDDTDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, PostDDTDelay);
  }

  public long GetXBandSwitchDelay(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetXBandSwitchDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetXBandSwitchDelay(ClParamSet arg0, long XBandSwitch) {
    ScWrapJNI.ClSatellite_SetXBandSwitchDelay(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, XBandSwitch);
  }

  public long GetMinRFPAInterval(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetMinRFPAInterval(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetMinRFPAInterval(ClParamSet arg0, long MinRFPAIntervav) {
    ScWrapJNI.ClSatellite_SetMinRFPAInterval(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, MinRFPAIntervav);
  }

  public long GetRsiPbkShift(ClParamSet arg0) {
    return ScWrapJNI.ClSatellite_GetRsiPbkShift(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0);
  }

  public void SetRsiPbkShift(ClParamSet arg0, long RsiPbkShift) {
    ScWrapJNI.ClSatellite_SetRsiPbkShift(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, RsiPbkShift);
  }

  public String GetVideoGain() {
    return ScWrapJNI.ClSatellite_GetVideoGain(swigCPtr, this);
  }

  public void SetVideoGain(String videoGain) {
    ScWrapJNI.ClSatellite_SetVideoGain(swigCPtr, this, videoGain);
  }

  public String GetCommandDBScenario() {
    return ScWrapJNI.ClSatellite_GetCommandDBScenario(swigCPtr, this);
  }

  public void SetCommandDBScenario(String arg0) {
    ScWrapJNI.ClSatellite_SetCommandDBScenario(swigCPtr, this, arg0);
  }

  public int GetScenarioVersion() {
    return ScWrapJNI.ClSatellite_GetScenarioVersion(swigCPtr, this);
  }

  public void SetScenarioVersion(int arg0) {
    ScWrapJNI.ClSatellite_SetScenarioVersion(swigCPtr, this, arg0);
  }

  public String GetProject() {
    return ScWrapJNI.ClSatellite_GetProject(swigCPtr, this);
  }

  public void SetProject(String arg0) {
    ScWrapJNI.ClSatellite_SetProject(swigCPtr, this, arg0);
  }

  public void GetRecorders(ClRecorderVector arg0) {
    ScWrapJNI.ClSatellite_GetRecorders(swigCPtr, this, ClRecorderVector.getCPtr(arg0), arg0);
  }

  public boolean AddRecorder(String name, int sectorSize, int totalFiles, int maxRsiFiles, int maxIsualFiles, int maxDdtFiles) {
    return ScWrapJNI.ClSatellite_AddRecorder__SWIG_0(swigCPtr, this, name, sectorSize, totalFiles, maxRsiFiles, maxIsualFiles, maxDdtFiles);
  }

  public boolean AddRecorder(String name, int sectorSize, int totalFiles, int maxRsiFiles, int maxIsualFiles) {
    return ScWrapJNI.ClSatellite_AddRecorder__SWIG_1(swigCPtr, this, name, sectorSize, totalFiles, maxRsiFiles, maxIsualFiles);
  }

  public boolean AddRecorder(String name, int sectorSize, int totalFiles, int maxRsiFiles) {
    return ScWrapJNI.ClSatellite_AddRecorder__SWIG_2(swigCPtr, this, name, sectorSize, totalFiles, maxRsiFiles);
  }

  public boolean AddRecorder(String name, int sectorSize, int totalFiles) {
    return ScWrapJNI.ClSatellite_AddRecorder__SWIG_3(swigCPtr, this, name, sectorSize, totalFiles);
  }

  public boolean AddRecorder(String name, int sectorSize) {
    return ScWrapJNI.ClSatellite_AddRecorder__SWIG_4(swigCPtr, this, name, sectorSize);
  }

  public boolean AddRecorder(String name) {
    return ScWrapJNI.ClSatellite_AddRecorder__SWIG_5(swigCPtr, this, name);
  }

  public void RemoveRecorder(ClRecorder arg0) {
    ScWrapJNI.ClSatellite_RemoveRecorder(swigCPtr, this, ClRecorder.getCPtr(arg0), arg0);
  }

  public void GetAntennas(ClAntennaVector arg0) {
    ScWrapJNI.ClSatellite_GetAntennas(swigCPtr, this, ClAntennaVector.getCPtr(arg0), arg0);
  }

  public boolean AddAntenna(String name, String type) {
    return ScWrapJNI.ClSatellite_AddAntenna(swigCPtr, this, name, type);
  }

  public void RemoveAntenna(ClAntenna arg0) {
    ScWrapJNI.ClSatellite_RemoveAntenna(swigCPtr, this, ClAntenna.getCPtr(arg0), arg0);
  }

  public void GetTransmitters(ClTransmitterVector arg0) {
    ScWrapJNI.ClSatellite_GetTransmitters(swigCPtr, this, ClTransmitterVector.getCPtr(arg0), arg0);
  }

  public boolean AddTransmitter(String name) {
    return ScWrapJNI.ClSatellite_AddTransmitter(swigCPtr, this, name);
  }

  public void RemoveTransmitter(ClTransmitter arg0) {
    ScWrapJNI.ClSatellite_RemoveTransmitter(swigCPtr, this, ClTransmitter.getCPtr(arg0), arg0);
  }

  public void GetDutyCycles(ClParamSet arg0, ClDutyCycleVector arg1) {
    ScWrapJNI.ClSatellite_GetDutyCycles(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, ClDutyCycleVector.getCPtr(arg1), arg1);
  }

  public ClDutyCycle GetDutyCycle(ClParamSet arg0, GlARL.Type arg1, GlARL.SubType arg2) {
    return new ClDutyCycle(ScWrapJNI.ClSatellite_GetDutyCycle__SWIG_0(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, arg1.swigValue(), arg2.swigValue()), true);
  }

  public ClDutyCycle GetDutyCycle(ClParamSet arg0, GlARL.Type arg1) {
    return new ClDutyCycle(ScWrapJNI.ClSatellite_GetDutyCycle__SWIG_1(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, arg1.swigValue()), true);
  }

  public void AddDutyCycle(ClParamSet arg0, GlARL.Type arg1, long arg2, GlARL.SubType arg3) {
    ScWrapJNI.ClSatellite_AddDutyCycle__SWIG_0(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, arg1.swigValue(), arg2, arg3.swigValue());
  }

  public void AddDutyCycle(ClParamSet arg0, GlARL.Type arg1, long arg2) {
    ScWrapJNI.ClSatellite_AddDutyCycle__SWIG_1(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, arg1.swigValue(), arg2);
  }

  public void RemoveDutyCycle(ClParamSet arg0, GlARL.Type arg1, GlARL.SubType arg2) {
    ScWrapJNI.ClSatellite_RemoveDutyCycle__SWIG_0(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, arg1.swigValue(), arg2.swigValue());
  }

  public void RemoveDutyCycle(ClParamSet arg0, GlARL.Type arg1) {
    ScWrapJNI.ClSatellite_RemoveDutyCycle__SWIG_1(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, arg1.swigValue());
  }

  public boolean HasDutyCycle(ClParamSet arg0, GlARL.Type arg1, GlARL.SubType arg2) {
    return ScWrapJNI.ClSatellite_HasDutyCycle__SWIG_0(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, arg1.swigValue(), arg2.swigValue());
  }

  public boolean HasDutyCycle(ClParamSet arg0, GlARL.Type arg1) {
    return ScWrapJNI.ClSatellite_HasDutyCycle__SWIG_1(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, arg1.swigValue());
  }

  public ClSiteRSIActPref GetSiteRSIActPref(ClParamSet arg0, ClSite arg1) {
    return new ClSiteRSIActPref(ScWrapJNI.ClSatellite_GetSiteRSIActPref(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, ClSite.getCPtr(arg1), arg1), true);
  }

  public void SetSiteRSIActPref(ClParamSet arg0, ClSiteRSIActPref arg1) {
    ScWrapJNI.ClSatellite_SetSiteRSIActPref(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, ClSiteRSIActPref.getCPtr(arg1), arg1);
  }

  public boolean HasSiteRSIActPref(ClSite arg0, java.lang.String[] INOUT) {
    return ScWrapJNI.ClSatellite_HasSiteRSIActPref__SWIG_0(swigCPtr, this, ClSite.getCPtr(arg0), arg0, INOUT);
  }

  public boolean HasSiteRSIActPref(ClParamSet arg0, ClSite arg1) {
    return ScWrapJNI.ClSatellite_HasSiteRSIActPref__SWIG_1(swigCPtr, this, ClParamSet.getCPtr(arg0), arg0, ClSite.getCPtr(arg1), arg1);
  }

  public ClAntenna GetAntenna(String arg0) {
    return new ClAntenna(ScWrapJNI.ClSatellite_GetAntenna(swigCPtr, this, arg0), true);
  }

  public ClRecorder GetRecorder(String arg0) {
    return new ClRecorder(ScWrapJNI.ClSatellite_GetRecorder(swigCPtr, this, arg0), true);
  }

  public ClTransmitter GetTransmitter(String arg0) {
    return new ClTransmitter(ScWrapJNI.ClSatellite_GetTransmitter(swigCPtr, this, arg0), true);
  }

  public boolean RecorderExists(String arg0) {
    return ScWrapJNI.ClSatellite_RecorderExists(swigCPtr, this, arg0);
  }

  public boolean AntennaExists(String arg0) {
    return ScWrapJNI.ClSatellite_AntennaExists(swigCPtr, this, arg0);
  }

  public boolean TransmitterExists(String arg0) {
    return ScWrapJNI.ClSatellite_TransmitterExists(swigCPtr, this, arg0);
  }

  public void CompareSatellitePSs(ClParamSet PS1, ClParamSet PS2, GlKeyValueMap diffPS1Map, GlKeyValueMap diffPS2Map, GlKeyValueMap samePS1Map, GlKeyValueMap samePS2Map) {
    ScWrapJNI.ClSatellite_CompareSatellitePSs(swigCPtr, this, ClParamSet.getCPtr(PS1), PS1, ClParamSet.getCPtr(PS2), PS2, GlKeyValueMap.getCPtr(diffPS1Map), diffPS1Map, GlKeyValueMap.getCPtr(diffPS2Map), diffPS2Map, GlKeyValueMap.getCPtr(samePS1Map), samePS1Map, GlKeyValueMap.getCPtr(samePS2Map), samePS2Map);
  }

  public boolean IsDeleteable(java.lang.String[] INOUT) {
    return ScWrapJNI.ClSatellite_IsDeleteable(swigCPtr, this, INOUT);
  }

}
