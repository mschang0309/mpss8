/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwRSIImagingARLEntryImpFactory extends FwBaseARLEntryImpFactory {
  private long swigCPtr;

  protected FwRSIImagingARLEntryImpFactory(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwRSIImagingARLEntryImpFactory_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwRSIImagingARLEntryImpFactory obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwRSIImagingARLEntryImpFactory(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FwRSIImagingARLEntryImpFactory_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FwRSIImagingARLEntryImpFactory_change_ownership(this, swigCPtr, true);
  }

  public FwRSIImagingARLEntryImp GetOne(int parentID, GlDaySpec date, long StartTime, long Duration, int modeID, String station, String elevation, String azimuth, int orbit, int trackgi, int gridjs, int gridje, int gridk, double startLat, double stopLat, int imagingType, GlARL.SubType ssrStatus, GlRecorder.Rate panCompRatio, GlRecorder.Rate msCompRatio, String videoPanGain, String videoMb1Gain, String videoMb2Gain, String videoMb3Gain, String videoMb4Gain, String Comments) {
    long cPtr = (getClass() == FwRSIImagingARLEntryImpFactory.class) ? ScWrapJNI.FwRSIImagingARLEntryImpFactory_GetOne(swigCPtr, this, parentID, GlDaySpec.getCPtr(date), date, StartTime, Duration, modeID, station, elevation, azimuth, orbit, trackgi, gridjs, gridje, gridk, startLat, stopLat, imagingType, ssrStatus.swigValue(), panCompRatio.swigValue(), msCompRatio.swigValue(), videoPanGain, videoMb1Gain, videoMb2Gain, videoMb3Gain, videoMb4Gain, Comments) : ScWrapJNI.FwRSIImagingARLEntryImpFactory_GetOneSwigExplicitFwRSIImagingARLEntryImpFactory(swigCPtr, this, parentID, GlDaySpec.getCPtr(date), date, StartTime, Duration, modeID, station, elevation, azimuth, orbit, trackgi, gridjs, gridje, gridk, startLat, stopLat, imagingType, ssrStatus.swigValue(), panCompRatio.swigValue(), msCompRatio.swigValue(), videoPanGain, videoMb1Gain, videoMb2Gain, videoMb3Gain, videoMb4Gain, Comments);
    return (cPtr == 0) ? null : new FwRSIImagingARLEntryImp(cPtr, false);
  }

  public FwBaseARLEntryImp GetID(int arg0) {
    long cPtr = (getClass() == FwRSIImagingARLEntryImpFactory.class) ? ScWrapJNI.FwRSIImagingARLEntryImpFactory_GetID(swigCPtr, this, arg0) : ScWrapJNI.FwRSIImagingARLEntryImpFactory_GetIDSwigExplicitFwRSIImagingARLEntryImpFactory(swigCPtr, this, arg0);
    return (cPtr == 0) ? null : new FwBaseARLEntryImp(cPtr, false);
  }

  protected FwRSIImagingARLEntryImpFactory() {
    this(ScWrapJNI.new_FwRSIImagingARLEntryImpFactory__SWIG_0(), true);
    ScWrapJNI.FwRSIImagingARLEntryImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwRSIImagingARLEntryImpFactory(FwRSIImagingARLEntryImpFactory arg0) {
    this(ScWrapJNI.new_FwRSIImagingARLEntryImpFactory__SWIG_1(FwRSIImagingARLEntryImpFactory.getCPtr(arg0), arg0), true);
    ScWrapJNI.FwRSIImagingARLEntryImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwRSIImagingARLEntryImp CreateOne(int parent, GlDaySpec date, long StartTime, long Duration, int modeID, String station, String elevation, String azimuth, int orbit, int trackgi, int gridjs, int gridje, int gridk, double startLat, double stopLat, int imagingType, GlARL.SubType ssrStatus, GlRecorder.Rate panCompRatio, GlRecorder.Rate msCompRatio, String videoPanGain, String videoMb1Gain, String videoMb2Gain, String videoMb3Gain, String videoMb4Gain, String Comments) {
    long cPtr = (getClass() == FwRSIImagingARLEntryImpFactory.class) ? ScWrapJNI.FwRSIImagingARLEntryImpFactory_CreateOne(swigCPtr, this, parent, GlDaySpec.getCPtr(date), date, StartTime, Duration, modeID, station, elevation, azimuth, orbit, trackgi, gridjs, gridje, gridk, startLat, stopLat, imagingType, ssrStatus.swigValue(), panCompRatio.swigValue(), msCompRatio.swigValue(), videoPanGain, videoMb1Gain, videoMb2Gain, videoMb3Gain, videoMb4Gain, Comments) : ScWrapJNI.FwRSIImagingARLEntryImpFactory_CreateOneSwigExplicitFwRSIImagingARLEntryImpFactory(swigCPtr, this, parent, GlDaySpec.getCPtr(date), date, StartTime, Duration, modeID, station, elevation, azimuth, orbit, trackgi, gridjs, gridje, gridk, startLat, stopLat, imagingType, ssrStatus.swigValue(), panCompRatio.swigValue(), msCompRatio.swigValue(), videoPanGain, videoMb1Gain, videoMb2Gain, videoMb3Gain, videoMb4Gain, Comments);
    return (cPtr == 0) ? null : new FwRSIImagingARLEntryImp(cPtr, false);
  }

}
