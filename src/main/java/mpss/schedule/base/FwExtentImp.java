/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwExtentImp extends FwBaseImp {
  private long swigCPtr;

  protected FwExtentImp(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwExtentImp_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwExtentImp obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    //delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwExtentImp(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public GlTimeRange GetTimeRange() {
    return new GlTimeRange(ScWrapJNI.FwExtentImp_GetTimeRange(swigCPtr, this), true);
  }

  public void SetTimeRange(GlTimeRange TimeRange) {
    ScWrapJNI.FwExtentImp_SetTimeRange(swigCPtr, this, GlTimeRange.getCPtr(TimeRange), TimeRange);
  }

  public int GetActivity(boolean arg0) {
    return ScWrapJNI.FwExtentImp_GetActivity__SWIG_0(swigCPtr, this, arg0);
  }

  public int GetActivity() {
    return ScWrapJNI.FwExtentImp_GetActivity__SWIG_1(swigCPtr, this);
  }

  public void SetActivity(int ActivityID) {
    ScWrapJNI.FwExtentImp_SetActivity(swigCPtr, this, ActivityID);
  }

  public int GetContact() {
    return ScWrapJNI.FwExtentImp_GetContact(swigCPtr, this);
  }

  public void SetContact(int ContactID) {
    ScWrapJNI.FwExtentImp_SetContact(swigCPtr, this, ContactID);
  }

  public int GetBranch() {
    return ScWrapJNI.FwExtentImp_GetBranch(swigCPtr, this);
  }

  public void SetBranch(int BranchID) {
    ScWrapJNI.FwExtentImp_SetBranch(swigCPtr, this, BranchID);
  }

  public int GetTransmitter(boolean checkDeleted) {
    return ScWrapJNI.FwExtentImp_GetTransmitter__SWIG_0(swigCPtr, this, checkDeleted);
  }

  public int GetTransmitter() {
    return ScWrapJNI.FwExtentImp_GetTransmitter__SWIG_1(swigCPtr, this);
  }

  public void SetTransmitter(int TransmitterID) {
    ScWrapJNI.FwExtentImp_SetTransmitter(swigCPtr, this, TransmitterID);
  }

  public int GetRecorder(boolean checkDeleted) {
    return ScWrapJNI.FwExtentImp_GetRecorder__SWIG_0(swigCPtr, this, checkDeleted);
  }

  public int GetRecorder() {
    return ScWrapJNI.FwExtentImp_GetRecorder__SWIG_1(swigCPtr, this);
  }

  public void SetRecorder(int RecorderID) {
    ScWrapJNI.FwExtentImp_SetRecorder(swigCPtr, this, RecorderID);
  }

  public int GetAntenna(boolean checkDeleted) {
    return ScWrapJNI.FwExtentImp_GetAntenna__SWIG_0(swigCPtr, this, checkDeleted);
  }

  public int GetAntenna() {
    return ScWrapJNI.FwExtentImp_GetAntenna__SWIG_1(swigCPtr, this);
  }

  public void SetAntenna(int AntennaID) {
    ScWrapJNI.FwExtentImp_SetAntenna(swigCPtr, this, AntennaID);
  }

  public int GetRecorderDelta() {
    return ScWrapJNI.FwExtentImp_GetRecorderDelta(swigCPtr, this);
  }

  public void SetRecorderDelta(int arg0) {
    ScWrapJNI.FwExtentImp_SetRecorderDelta(swigCPtr, this, arg0);
  }

  public int GetSecondRecorderDelta() {
    return ScWrapJNI.FwExtentImp_GetSecondRecorderDelta(swigCPtr, this);
  }

  public void SetSecondRecorderDelta(int arg0) {
    ScWrapJNI.FwExtentImp_SetSecondRecorderDelta(swigCPtr, this, arg0);
  }

  public int GetFilename() {
    return ScWrapJNI.FwExtentImp_GetFilename(swigCPtr, this);
  }

  public void SetFilename(int arg0) {
    ScWrapJNI.FwExtentImp_SetFilename(swigCPtr, this, arg0);
  }

  public int GetSecondFilename() {
    return ScWrapJNI.FwExtentImp_GetSecondFilename(swigCPtr, this);
  }

  public void SetSecondFilename(int arg0) {
    ScWrapJNI.FwExtentImp_SetSecondFilename(swigCPtr, this, arg0);
  }

  public boolean GetUserModified() {
    return ScWrapJNI.FwExtentImp_GetUserModified(swigCPtr, this);
  }

  public void SetUserModified(boolean arg0) {
    ScWrapJNI.FwExtentImp_SetUserModified(swigCPtr, this, arg0);
  }

  public boolean GetUserLocked() {
    return ScWrapJNI.FwExtentImp_GetUserLocked(swigCPtr, this);
  }

  public void SetUserLocked(boolean arg0) {
    ScWrapJNI.FwExtentImp_SetUserLocked(swigCPtr, this, arg0);
  }

  public boolean GetSoftConstraintViolated() {
    return ScWrapJNI.FwExtentImp_GetSoftConstraintViolated(swigCPtr, this);
  }

  public void SetSoftConstraintViolated(boolean arg0) {
    ScWrapJNI.FwExtentImp_SetSoftConstraintViolated(swigCPtr, this, arg0);
  }

  public boolean GetHardConstraintViolated() {
    return ScWrapJNI.FwExtentImp_GetHardConstraintViolated(swigCPtr, this);
  }

  public void SetHardConstraintViolated(boolean arg0) {
    ScWrapJNI.FwExtentImp_SetHardConstraintViolated(swigCPtr, this, arg0);
  }

  public void SetActivityTimeRanges(GlTimeRangeVector TimeRanges) {
    ScWrapJNI.FwExtentImp_SetActivityTimeRanges(swigCPtr, this, GlTimeRangeVector.getCPtr(TimeRanges), TimeRanges);
  }

  public void RemoveActivityTimeRanges() {
    ScWrapJNI.FwExtentImp_RemoveActivityTimeRanges(swigCPtr, this);
  }

  public void GetActivityTimeRanges(GlTimeRangeVector TimeRanges) {
    ScWrapJNI.FwExtentImp_GetActivityTimeRanges(swigCPtr, this, GlTimeRangeVector.getCPtr(TimeRanges), TimeRanges);
  }

  public boolean SaveAssociated() {
    return ScWrapJNI.FwExtentImp_SaveAssociated(swigCPtr, this);
  }

  public void RemoveAssociated() {
    ScWrapJNI.FwExtentImp_RemoveAssociated(swigCPtr, this);
  }

  public void SetAssociatedSaved() {
    ScWrapJNI.FwExtentImp_SetAssociatedSaved(swigCPtr, this);
  }

  public String CheckMeString() {
    return ScWrapJNI.FwExtentImp_CheckMeString(swigCPtr, this);
  }

  public FwExtentImp() {
    this(ScWrapJNI.new_FwExtentImp__SWIG_0(), true);
  }

  public FwExtentImp(int extentId, int activityId, int contactId, int branchID, int transmitterID, int recorderID, int antennaID, GlTimeRange arg7, int recorderDelta, int secondRecorderDelta, int filename, int secondFilename, boolean userModified, boolean userLocked, boolean SoftConstraintViolated, boolean HardConstraintViolated, boolean isNew) {
    this(ScWrapJNI.new_FwExtentImp__SWIG_1(extentId, activityId, contactId, branchID, transmitterID, recorderID, antennaID, GlTimeRange.getCPtr(arg7), arg7, recorderDelta, secondRecorderDelta, filename, secondFilename, userModified, userLocked, SoftConstraintViolated, HardConstraintViolated, isNew), true);
  }

  public FwExtentImp(int extentId, int activityId, int contactId, int branchID, int transmitterID, int recorderID, int antennaID, GlTimeRange arg7, int recorderDelta, int secondRecorderDelta, int filename, int secondFilename, boolean userModified, boolean userLocked, boolean SoftConstraintViolated, boolean HardConstraintViolated) {
    this(ScWrapJNI.new_FwExtentImp__SWIG_2(extentId, activityId, contactId, branchID, transmitterID, recorderID, antennaID, GlTimeRange.getCPtr(arg7), arg7, recorderDelta, secondRecorderDelta, filename, secondFilename, userModified, userLocked, SoftConstraintViolated, HardConstraintViolated), true);
  }

}
