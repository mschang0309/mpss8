/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FactoryChild_FwRecorderImp extends FwBaseImpFactory {
  private long swigCPtr;

  protected FactoryChild_FwRecorderImp(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FactoryChild_FwRecorderImp_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FactoryChild_FwRecorderImp obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FactoryChild_FwRecorderImp(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FactoryChild_FwRecorderImp_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FactoryChild_FwRecorderImp_change_ownership(this, swigCPtr, true);
  }

  public FwRecorderImp GetID(int arg0) {
    long cPtr = ScWrapJNI.FactoryChild_FwRecorderImp_GetID(swigCPtr, this, arg0);
    return (cPtr == 0) ? null : new FwRecorderImp(cPtr, false);
  }

  public void GetAll(FwRecorderImpVector arg0, int parentID) {
    if (getClass() == FactoryChild_FwRecorderImp.class) ScWrapJNI.FactoryChild_FwRecorderImp_GetAll(swigCPtr, this, FwRecorderImpVector.getCPtr(arg0), arg0, parentID); else ScWrapJNI.FactoryChild_FwRecorderImp_GetAllSwigExplicitFactoryChild_FwRecorderImp(swigCPtr, this, FwRecorderImpVector.getCPtr(arg0), arg0, parentID);
  }

  public boolean Store(FwRecorderImpVector imps, FwRecorderImpVector delimps) {
    return (getClass() == FactoryChild_FwRecorderImp.class) ? ScWrapJNI.FactoryChild_FwRecorderImp_Store__SWIG_0(swigCPtr, this, FwRecorderImpVector.getCPtr(imps), imps, FwRecorderImpVector.getCPtr(delimps), delimps) : ScWrapJNI.FactoryChild_FwRecorderImp_StoreSwigExplicitFactoryChild_FwRecorderImp__SWIG_0(swigCPtr, this, FwRecorderImpVector.getCPtr(imps), imps, FwRecorderImpVector.getCPtr(delimps), delimps);
  }

  public boolean Store(FwRecorderImp imp) {
    return (getClass() == FactoryChild_FwRecorderImp.class) ? ScWrapJNI.FactoryChild_FwRecorderImp_Store__SWIG_1(swigCPtr, this, FwRecorderImp.getCPtr(imp), imp) : ScWrapJNI.FactoryChild_FwRecorderImp_StoreSwigExplicitFactoryChild_FwRecorderImp__SWIG_1(swigCPtr, this, FwRecorderImp.getCPtr(imp), imp);
  }

  public void Delete(FwRecorderImp arg0) {
    if (getClass() == FactoryChild_FwRecorderImp.class) ScWrapJNI.FactoryChild_FwRecorderImp_Delete__SWIG_0(swigCPtr, this, FwRecorderImp.getCPtr(arg0), arg0); else ScWrapJNI.FactoryChild_FwRecorderImp_DeleteSwigExplicitFactoryChild_FwRecorderImp__SWIG_0(swigCPtr, this, FwRecorderImp.getCPtr(arg0), arg0);
  }

  public void Delete(FwRecorderImpVector arg0) {
    if (getClass() == FactoryChild_FwRecorderImp.class) ScWrapJNI.FactoryChild_FwRecorderImp_Delete__SWIG_1(swigCPtr, this, FwRecorderImpVector.getCPtr(arg0), arg0); else ScWrapJNI.FactoryChild_FwRecorderImp_DeleteSwigExplicitFactoryChild_FwRecorderImp__SWIG_1(swigCPtr, this, FwRecorderImpVector.getCPtr(arg0), arg0);
  }

  public void Set_FwChildImpFactory_JavaInstance(FactoryChild_FwRecorderImp ins) {
    ScWrapJNI.FactoryChild_FwRecorderImp_Set_FwChildImpFactory_JavaInstance(swigCPtr, this, FactoryChild_FwRecorderImp.getCPtr(ins), ins);
  }

  protected FactoryChild_FwRecorderImp() {
    this(ScWrapJNI.new_FactoryChild_FwRecorderImp(), true);
    ScWrapJNI.FactoryChild_FwRecorderImp_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

}
