/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwActivityImp extends FwBaseImp {
  private long swigCPtr;

  protected FwActivityImp(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwActivityImp_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwActivityImp obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    //delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwActivityImp(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public long GetDuration() {
    return ScWrapJNI.FwActivityImp_GetDuration(swigCPtr, this);
  }

  public void SetDuration(long Duration) {
    ScWrapJNI.FwActivityImp_SetDuration(swigCPtr, this, Duration);
  }

  public GlTimeRange GetTimeWindow() {
    return new GlTimeRange(ScWrapJNI.FwActivityImp_GetTimeWindow(swigCPtr, this), true);
  }

  public void SetTimeWindow(GlTimeRange TimeWindow) {
    ScWrapJNI.FwActivityImp_SetTimeWindow(swigCPtr, this, GlTimeRange.getCPtr(TimeWindow), TimeWindow);
  }

  public int GetSatellite(boolean arg0) {
    return ScWrapJNI.FwActivityImp_GetSatellite__SWIG_0(swigCPtr, this, arg0);
  }

  public int GetSatellite() {
    return ScWrapJNI.FwActivityImp_GetSatellite__SWIG_1(swigCPtr, this);
  }

  public void SetSatellite(int Satellite) {
    ScWrapJNI.FwActivityImp_SetSatellite(swigCPtr, this, Satellite);
  }

  public int GetSession(boolean arg0) {
    return ScWrapJNI.FwActivityImp_GetSession__SWIG_0(swigCPtr, this, arg0);
  }

  public int GetSession() {
    return ScWrapJNI.FwActivityImp_GetSession__SWIG_1(swigCPtr, this);
  }

  public void SetSession(int Session) {
    ScWrapJNI.FwActivityImp_SetSession(swigCPtr, this, Session);
  }

  public boolean GetScheduled() {
    return ScWrapJNI.FwActivityImp_GetScheduled(swigCPtr, this);
  }

  public void SetScheduled(boolean arg0) {
    ScWrapJNI.FwActivityImp_SetScheduled(swigCPtr, this, arg0);
  }

  public void SetUserModified(boolean arg0) {
    ScWrapJNI.FwActivityImp_SetUserModified(swigCPtr, this, arg0);
  }

  public boolean GetUserModified() {
    return ScWrapJNI.FwActivityImp_GetUserModified(swigCPtr, this);
  }

  public void SetValid(boolean arg0) {
    ScWrapJNI.FwActivityImp_SetValid(swigCPtr, this, arg0);
  }

  public boolean GetValid() {
    return ScWrapJNI.FwActivityImp_GetValid(swigCPtr, this);
  }

  public GlARL.Type GetARLEntryType() {
    return GlARL.Type.swigToEnum(ScWrapJNI.FwActivityImp_GetARLEntryType(swigCPtr, this));
  }

  public void SetARLEntryType(GlARL.Type arg0) {
    ScWrapJNI.FwActivityImp_SetARLEntryType(swigCPtr, this, arg0.swigValue());
  }

  public GlARL.SubType GetSubType() {
    return GlARL.SubType.swigToEnum(ScWrapJNI.FwActivityImp_GetSubType(swigCPtr, this));
  }

  public void SetSubType(GlARL.SubType arg0) {
    ScWrapJNI.FwActivityImp_SetSubType(swigCPtr, this, arg0.swigValue());
  }

  public GlARL.ToyType GetToyType() {
    return GlARL.ToyType.swigToEnum(ScWrapJNI.FwActivityImp_GetToyType(swigCPtr, this));
  }

  public void SetToyType(GlARL.ToyType arg0) {
    ScWrapJNI.FwActivityImp_SetToyType(swigCPtr, this, arg0.swigValue());
  }

  public String GetName() {
    return ScWrapJNI.FwActivityImp_GetName(swigCPtr, this);
  }

  public void SetName(String arg0) {
    ScWrapJNI.FwActivityImp_SetName(swigCPtr, this, arg0);
  }

  public int GetARLEntryID() {
    return ScWrapJNI.FwActivityImp_GetARLEntryID(swigCPtr, this);
  }

  public void SetARLEntryID(int arg0) {
    ScWrapJNI.FwActivityImp_SetARLEntryID(swigCPtr, this, arg0);
  }

  public int GetPreviousActivity() {
    return ScWrapJNI.FwActivityImp_GetPreviousActivity(swigCPtr, this);
  }

  public void SetPreviousActivity(int PreviousActivityID) {
    ScWrapJNI.FwActivityImp_SetPreviousActivity(swigCPtr, this, PreviousActivityID);
  }

  public int GetNextActivity() {
    return ScWrapJNI.FwActivityImp_GetNextActivity(swigCPtr, this);
  }

  public void SetNextActivity(int NextActivityID) {
    ScWrapJNI.FwActivityImp_SetNextActivity(swigCPtr, this, NextActivityID);
  }

  public int GetExtent() {
    return ScWrapJNI.FwActivityImp_GetExtent(swigCPtr, this);
  }

  public void SetExtent(int arg0) {
    ScWrapJNI.FwActivityImp_SetExtent(swigCPtr, this, arg0);
  }

  public String GetInfo() {
    return ScWrapJNI.FwActivityImp_GetInfo(swigCPtr, this);
  }

  public void SetInfo(String arg0) {
    ScWrapJNI.FwActivityImp_SetInfo(swigCPtr, this, arg0);
  }

  public void CheckUserAssociations() {
    ScWrapJNI.FwActivityImp_CheckUserAssociations(swigCPtr, this);
  }

  public String CheckMeString() {
    return ScWrapJNI.FwActivityImp_CheckMeString(swigCPtr, this);
  }

  public FwActivityImp() {
    this(ScWrapJNI.new_FwActivityImp__SWIG_0(), true);
  }

  public FwActivityImp(int arg0, long duration, GlTimeRange timeWindow, int satID, int sessionID, int extentID, boolean Scheduled, GlARL.Type arlType, int eid, String name, GlARL.SubType subType, GlARL.ToyType toyType, boolean umod, String info, int prev, int next, boolean valid, boolean isNew) {
    this(ScWrapJNI.new_FwActivityImp__SWIG_1(arg0, duration, GlTimeRange.getCPtr(timeWindow), timeWindow, satID, sessionID, extentID, Scheduled, arlType.swigValue(), eid, name, subType.swigValue(), toyType.swigValue(), umod, info, prev, next, valid, isNew), true);
  }

  public FwActivityImp(int arg0, long duration, GlTimeRange timeWindow, int satID, int sessionID, int extentID, boolean Scheduled, GlARL.Type arlType, int eid, String name, GlARL.SubType subType, GlARL.ToyType toyType, boolean umod, String info, int prev, int next, boolean valid) {
    this(ScWrapJNI.new_FwActivityImp__SWIG_2(arg0, duration, GlTimeRange.getCPtr(timeWindow), timeWindow, satID, sessionID, extentID, Scheduled, arlType.swigValue(), eid, name, subType.swigValue(), toyType.swigValue(), umod, info, prev, next, valid), true);
  }

  public FwActivityImp(int arg0, long duration, GlTimeRange timeWindow, int satID, int sessionID, int extentID, boolean Scheduled, GlARL.Type arlType, int eid, String name, GlARL.SubType subType, GlARL.ToyType toyType, boolean umod, String info, int prev, int next) {
    this(ScWrapJNI.new_FwActivityImp__SWIG_3(arg0, duration, GlTimeRange.getCPtr(timeWindow), timeWindow, satID, sessionID, extentID, Scheduled, arlType.swigValue(), eid, name, subType.swigValue(), toyType.swigValue(), umod, info, prev, next), true);
  }

  public FwActivityImp(int arg0, long duration, GlTimeRange timeWindow, int satID, int sessionID, int extentID, boolean Scheduled, GlARL.Type arlType, int eid, String name, GlARL.SubType subType, GlARL.ToyType toyType, boolean umod, String info, int prev) {
    this(ScWrapJNI.new_FwActivityImp__SWIG_4(arg0, duration, GlTimeRange.getCPtr(timeWindow), timeWindow, satID, sessionID, extentID, Scheduled, arlType.swigValue(), eid, name, subType.swigValue(), toyType.swigValue(), umod, info, prev), true);
  }

  public FwActivityImp(int arg0, long duration, GlTimeRange timeWindow, int satID, int sessionID, int extentID, boolean Scheduled, GlARL.Type arlType, int eid, String name, GlARL.SubType subType, GlARL.ToyType toyType, boolean umod, String info) {
    this(ScWrapJNI.new_FwActivityImp__SWIG_5(arg0, duration, GlTimeRange.getCPtr(timeWindow), timeWindow, satID, sessionID, extentID, Scheduled, arlType.swigValue(), eid, name, subType.swigValue(), toyType.swigValue(), umod, info), true);
  }

  public FwActivityImp(int arg0, long duration, GlTimeRange timeWindow, int satID, int sessionID, int extentID, boolean Scheduled, GlARL.Type arlType, int eid, String name, GlARL.SubType subType, GlARL.ToyType toyType, boolean umod) {
    this(ScWrapJNI.new_FwActivityImp__SWIG_6(arg0, duration, GlTimeRange.getCPtr(timeWindow), timeWindow, satID, sessionID, extentID, Scheduled, arlType.swigValue(), eid, name, subType.swigValue(), toyType.swigValue(), umod), true);
  }

}
