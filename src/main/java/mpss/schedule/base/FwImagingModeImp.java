/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwImagingModeImp extends FwBaseImp {
  private long swigCPtr;

  protected FwImagingModeImp(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwImagingModeImp_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwImagingModeImp obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    //delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwImagingModeImp(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public String GetInstrument() {
    return ScWrapJNI.FwImagingModeImp_GetInstrument(swigCPtr, this);
  }

  public void SetInstrument(String Instrument) {
    ScWrapJNI.FwImagingModeImp_SetInstrument(swigCPtr, this, Instrument);
  }

  public int GetModeID() {
    return ScWrapJNI.FwImagingModeImp_GetModeID(swigCPtr, this);
  }

  public void SetModeID(int ModeID) {
    ScWrapJNI.FwImagingModeImp_SetModeID(swigCPtr, this, ModeID);
  }

  public String GetMode() {
    return ScWrapJNI.FwImagingModeImp_GetMode(swigCPtr, this);
  }

  public void SetMode(String Mode) {
    ScWrapJNI.FwImagingModeImp_SetMode(swigCPtr, this, Mode);
  }

  public String GetModeShort() {
    return ScWrapJNI.FwImagingModeImp_GetModeShort(swigCPtr, this);
  }

  public void SetModeShort(String ModeShort) {
    ScWrapJNI.FwImagingModeImp_SetModeShort(swigCPtr, this, ModeShort);
  }

  public double GetCompressionRatio() {
    return ScWrapJNI.FwImagingModeImp_GetCompressionRatio(swigCPtr, this);
  }

  public void SetCompressionRatio(double ratio) {
    ScWrapJNI.FwImagingModeImp_SetCompressionRatio(swigCPtr, this, ratio);
  }

  public String CheckMeString() {
    return ScWrapJNI.FwImagingModeImp_CheckMeString(swigCPtr, this);
  }

  public FwImagingModeImp() {
    this(ScWrapJNI.new_FwImagingModeImp__SWIG_0(), true);
  }

  public FwImagingModeImp(int arg0, String instrument, int modeID, String mode, String modeShort, double ratio, boolean isNew) {
    this(ScWrapJNI.new_FwImagingModeImp__SWIG_1(arg0, instrument, modeID, mode, modeShort, ratio, isNew), true);
  }

  public FwImagingModeImp(int arg0, String instrument, int modeID, String mode, String modeShort, double ratio) {
    this(ScWrapJNI.new_FwImagingModeImp__SWIG_2(arg0, instrument, modeID, mode, modeShort, ratio), true);
  }

}
