/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClISUALProcARLVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClISUALProcARLVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClISUALProcARLVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClISUALProcARLVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClISUALProcARLVector() {
    this(ScWrapJNI.new_ClISUALProcARLVector(), true);
  }

  public long size() {
    return ScWrapJNI.ClISUALProcARLVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.ClISUALProcARLVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.ClISUALProcARLVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.ClISUALProcARLVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.ClISUALProcARLVector_clear(swigCPtr, this);
  }

  public void add(ClISUALProcARL x) {
    ScWrapJNI.ClISUALProcARLVector_add(swigCPtr, this, ClISUALProcARL.getCPtr(x), x);
  }

  public ClISUALProcARL get(int i) {
    return new ClISUALProcARL(ScWrapJNI.ClISUALProcARLVector_get(swigCPtr, this, i), false);
  }

  public void set(int i, ClISUALProcARL val) {
    ScWrapJNI.ClISUALProcARLVector_set(swigCPtr, this, i, ClISUALProcARL.getCPtr(val), val);
  }

}
