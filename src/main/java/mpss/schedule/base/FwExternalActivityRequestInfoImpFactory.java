/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwExternalActivityRequestInfoImpFactory extends FactoryChild_FwExternalActivityRequestInfoImp {
  private long swigCPtr;

  protected FwExternalActivityRequestInfoImpFactory(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwExternalActivityRequestInfoImpFactory_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwExternalActivityRequestInfoImpFactory obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwExternalActivityRequestInfoImpFactory(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FwExternalActivityRequestInfoImpFactory_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FwExternalActivityRequestInfoImpFactory_change_ownership(this, swigCPtr, true);
  }

  public FwExternalActivityRequestInfoImp GetOne(int ARLId, GlTimeRange tr, int branchID, String satName, GlInstrument.Type arg4) {
    long cPtr = (getClass() == FwExternalActivityRequestInfoImpFactory.class) ? ScWrapJNI.FwExternalActivityRequestInfoImpFactory_GetOne(swigCPtr, this, ARLId, GlTimeRange.getCPtr(tr), tr, branchID, satName, arg4.swigValue()) : ScWrapJNI.FwExternalActivityRequestInfoImpFactory_GetOneSwigExplicitFwExternalActivityRequestInfoImpFactory(swigCPtr, this, ARLId, GlTimeRange.getCPtr(tr), tr, branchID, satName, arg4.swigValue());
    return (cPtr == 0) ? null : new FwExternalActivityRequestInfoImp(cPtr, false);
  }

  public FwExternalActivityRequestInfoImpFactory() {
    this(ScWrapJNI.new_FwExternalActivityRequestInfoImpFactory__SWIG_0(), true);
    ScWrapJNI.FwExternalActivityRequestInfoImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwExternalActivityRequestInfoImpFactory(FwExternalActivityRequestInfoImpFactory arg0) {
    this(ScWrapJNI.new_FwExternalActivityRequestInfoImpFactory__SWIG_1(FwExternalActivityRequestInfoImpFactory.getCPtr(arg0), arg0), true);
    ScWrapJNI.FwExternalActivityRequestInfoImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwExternalActivityRequestInfoImp CreateOne(int ARLID, GlTimeRange tr, int branchID, String satName, GlInstrument.Type arg4) {
    long cPtr = (getClass() == FwExternalActivityRequestInfoImpFactory.class) ? ScWrapJNI.FwExternalActivityRequestInfoImpFactory_CreateOne(swigCPtr, this, ARLID, GlTimeRange.getCPtr(tr), tr, branchID, satName, arg4.swigValue()) : ScWrapJNI.FwExternalActivityRequestInfoImpFactory_CreateOneSwigExplicitFwExternalActivityRequestInfoImpFactory(swigCPtr, this, ARLID, GlTimeRange.getCPtr(tr), tr, branchID, satName, arg4.swigValue());
    return (cPtr == 0) ? null : new FwExternalActivityRequestInfoImp(cPtr, false);
  }

}
