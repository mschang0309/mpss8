/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClSessionVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClSessionVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClSessionVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClSessionVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClSessionVector() {
    this(ScWrapJNI.new_ClSessionVector(), true);
  }

  public long size() {
    return ScWrapJNI.ClSessionVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.ClSessionVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.ClSessionVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.ClSessionVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.ClSessionVector_clear(swigCPtr, this);
  }

  public void add(ClSession x) {
    ScWrapJNI.ClSessionVector_add(swigCPtr, this, ClSession.getCPtr(x), x);
  }

  public ClSession get(int i) {
    return new ClSession(ScWrapJNI.ClSessionVector_get(swigCPtr, this, i), false);
  }

  public void set(int i, ClSession val) {
    ScWrapJNI.ClSessionVector_set(swigCPtr, this, i, ClSession.getCPtr(val), val);
  }

}
