/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClCompressionRatioVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClCompressionRatioVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClCompressionRatioVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClCompressionRatioVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClCompressionRatioVector() {
    this(ScWrapJNI.new_ClCompressionRatioVector(), true);
  }

  public long size() {
    return ScWrapJNI.ClCompressionRatioVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.ClCompressionRatioVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.ClCompressionRatioVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.ClCompressionRatioVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.ClCompressionRatioVector_clear(swigCPtr, this);
  }

  public void add(ClCompressionRatio x) {
    ScWrapJNI.ClCompressionRatioVector_add(swigCPtr, this, ClCompressionRatio.getCPtr(x), x);
  }

  public ClCompressionRatio get(int i) {
    return new ClCompressionRatio(ScWrapJNI.ClCompressionRatioVector_get(swigCPtr, this, i), false);
  }

  public void set(int i, ClCompressionRatio val) {
    ScWrapJNI.ClCompressionRatioVector_set(swigCPtr, this, i, ClCompressionRatio.getCPtr(val), val);
  }

}
