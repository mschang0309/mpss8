/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwSiteImp extends FwTimelinedImp {
  private long swigCPtr;

  protected FwSiteImp(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwSiteImp_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwSiteImp obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    //delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwSiteImp(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public String GetName(boolean arg0) {
    return ScWrapJNI.FwSiteImp_GetName__SWIG_0(swigCPtr, this, arg0);
  }

  public String GetName() {
    return ScWrapJNI.FwSiteImp_GetName__SWIG_1(swigCPtr, this);
  }

  public int GetSiteNumber() {
    return ScWrapJNI.FwSiteImp_GetSiteNumber(swigCPtr, this);
  }

  public long GetTurnaroundTime(int psid) {
    return ScWrapJNI.FwSiteImp_GetTurnaroundTime(swigCPtr, this, psid);
  }

  public long GetPrepassSetupTime(int psid) {
    return ScWrapJNI.FwSiteImp_GetPrepassSetupTime(swigCPtr, this, psid);
  }

  public long GetPostpassCleanupTime(int psid) {
    return ScWrapJNI.FwSiteImp_GetPostpassCleanupTime(swigCPtr, this, psid);
  }

  public boolean SetName(String arg0) {
    return ScWrapJNI.FwSiteImp_SetName(swigCPtr, this, arg0);
  }

  public void SetSiteNumber(short sn) {
    ScWrapJNI.FwSiteImp_SetSiteNumber(swigCPtr, this, sn);
  }

  public void SetTurnaroundTime(int psid, long turnAroundTime) {
    ScWrapJNI.FwSiteImp_SetTurnaroundTime(swigCPtr, this, psid, turnAroundTime);
  }

  public void SetPrepassSetupTime(int psid, long prepassTime) {
    ScWrapJNI.FwSiteImp_SetPrepassSetupTime(swigCPtr, this, psid, prepassTime);
  }

  public void SetPostpassCleanupTime(int psid, long postpassTime) {
    ScWrapJNI.FwSiteImp_SetPostpassCleanupTime(swigCPtr, this, psid, postpassTime);
  }

  public String GetName4() {
    return ScWrapJNI.FwSiteImp_GetName4(swigCPtr, this);
  }

  public void SetName4(String Name4) {
    ScWrapJNI.FwSiteImp_SetName4(swigCPtr, this, Name4);
  }

  public String GetName2() {
    return ScWrapJNI.FwSiteImp_GetName2(swigCPtr, this);
  }

  public void SetName2(String Name2) {
    ScWrapJNI.FwSiteImp_SetName2(swigCPtr, this, Name2);
  }

  public String GetName1() {
    return ScWrapJNI.FwSiteImp_GetName1(swigCPtr, this);
  }

  public void SetName1(String Name1) {
    ScWrapJNI.FwSiteImp_SetName1(swigCPtr, this, Name1);
  }

  public void GetUnavailableTimeRanges(GlUnavailTime.Type type, GlTimeRangeVector arg1, int branchid) {
    ScWrapJNI.FwSiteImp_GetUnavailableTimeRanges(swigCPtr, this, type.swigValue(), GlTimeRangeVector.getCPtr(arg1), arg1, branchid);
  }

  public boolean AddUnavailableTimeRange(GlUnavailTime.Type type, GlTimeRange arg1, int branchid) {
    return ScWrapJNI.FwSiteImp_AddUnavailableTimeRange(swigCPtr, this, type.swigValue(), GlTimeRange.getCPtr(arg1), arg1, branchid);
  }

  public boolean RemoveUnavailableTimeRange(GlUnavailTime.Type type, GlTimeRange arg1, int branchid) {
    return ScWrapJNI.FwSiteImp_RemoveUnavailableTimeRange(swigCPtr, this, type.swigValue(), GlTimeRange.getCPtr(arg1), arg1, branchid);
  }

  public void SetUnavailableTimeRanges(GlUnavailTime.Type type, GlTimeRangeVector trs, int branchid) {
    ScWrapJNI.FwSiteImp_SetUnavailableTimeRanges(swigCPtr, this, type.swigValue(), GlTimeRangeVector.getCPtr(trs), trs, branchid);
  }

  public void RemoveUnavailableTimeRanges(GlUnavailTime.Type type, int branchid) {
    ScWrapJNI.FwSiteImp_RemoveUnavailableTimeRanges(swigCPtr, this, type.swigValue(), branchid);
  }

  public String GetCommandControlSiteName() {
    return ScWrapJNI.FwSiteImp_GetCommandControlSiteName(swigCPtr, this);
  }

  public void SetCommandControlSiteName(String arg0) {
    ScWrapJNI.FwSiteImp_SetCommandControlSiteName(swigCPtr, this, arg0);
  }

  public GlSite.CCSType GetCCSType() {
    return GlSite.CCSType.swigToEnum(ScWrapJNI.FwSiteImp_GetCCSType(swigCPtr, this));
  }

  public void SetCCSType(GlSite.CCSType arg0) {
    ScWrapJNI.FwSiteImp_SetCCSType(swigCPtr, this, arg0.swigValue());
  }

  public GlSite.Type GetType_Site() {
    return GlSite.Type.swigToEnum(ScWrapJNI.FwSiteImp_GetType_Site(swigCPtr, this));
  }

  public void SetType(GlSite.Type arg0) {
    ScWrapJNI.FwSiteImp_SetType(swigCPtr, this, arg0.swigValue());
  }

  public String GetAntennaSideID() {
    return ScWrapJNI.FwSiteImp_GetAntennaSideID(swigCPtr, this);
  }

  public void SetAntennaSideID(String arg0) {
    ScWrapJNI.FwSiteImp_SetAntennaSideID(swigCPtr, this, arg0);
  }

  public boolean GetIsRTS() {
    return ScWrapJNI.FwSiteImp_GetIsRTS(swigCPtr, this);
  }

  public void SetIsRTS(boolean arg0) {
    ScWrapJNI.FwSiteImp_SetIsRTS(swigCPtr, this, arg0);
  }

  public boolean AddPS(int arg0) {
    return ScWrapJNI.FwSiteImp_AddPS__SWIG_0(swigCPtr, this, arg0);
  }

  public boolean AddPS(int newps, int fromps) {
    return ScWrapJNI.FwSiteImp_AddPS__SWIG_1(swigCPtr, this, newps, fromps);
  }

  public boolean RemovePS(int arg0) {
    return ScWrapJNI.FwSiteImp_RemovePS(swigCPtr, this, arg0);
  }

  public boolean SaveAssociated() {
    return ScWrapJNI.FwSiteImp_SaveAssociated(swigCPtr, this);
  }

  public void RemoveAssociated() {
    ScWrapJNI.FwSiteImp_RemoveAssociated(swigCPtr, this);
  }

  public void SetAssociatedSaved() {
    ScWrapJNI.FwSiteImp_SetAssociatedSaved(swigCPtr, this);
  }

  public void CancelAssociated() {
    ScWrapJNI.FwSiteImp_CancelAssociated(swigCPtr, this);
  }

  public void DeleteRelated() {
    ScWrapJNI.FwSiteImp_DeleteRelated(swigCPtr, this);
  }

  public void CheckUserAssociations() {
    ScWrapJNI.FwSiteImp_CheckUserAssociations(swigCPtr, this);
  }

  public FwSiteImp(int arg0, String name, short arg2, String name4, String name2, String name1, String ccsname, String antenna, GlSite.CCSType ccstype, GlSite.Type type, boolean rtsFlag, boolean isNew) {
    this(ScWrapJNI.new_FwSiteImp__SWIG_0(arg0, name, arg2, name4, name2, name1, ccsname, antenna, ccstype.swigValue(), type.swigValue(), rtsFlag, isNew), true);
  }

  public FwSiteImp(int arg0, String name, short arg2, String name4, String name2, String name1, String ccsname, String antenna, GlSite.CCSType ccstype, GlSite.Type type, boolean rtsFlag) {
    this(ScWrapJNI.new_FwSiteImp__SWIG_1(arg0, name, arg2, name4, name2, name1, ccsname, antenna, ccstype.swigValue(), type.swigValue(), rtsFlag), true);
  }

  public String CheckMeString() {
    return ScWrapJNI.FwSiteImp_CheckMeString(swigCPtr, this);
  }

  public void LoadAllAssoc() {
    ScWrapJNI.FwSiteImp_LoadAllAssoc(swigCPtr, this);
  }

  public void LoadPSs() {
    ScWrapJNI.FwSiteImp_LoadPSs(swigCPtr, this);
  }

  public FwSitePSImp GetPS(int arg0) {
    long cPtr = ScWrapJNI.FwSiteImp_GetPS(swigCPtr, this, arg0);
    return (cPtr == 0) ? null : new FwSitePSImp(cPtr, false);
  }

  public static FwSitePSImp GetDefaultPS() {
    long cPtr = ScWrapJNI.FwSiteImp_GetDefaultPS();
    return (cPtr == 0) ? null : new FwSitePSImp(cPtr, false);
  }

  public boolean AddPS(int psid, FwSitePSImp imp) {
    return ScWrapJNI.FwSiteImp_AddPS__SWIG_2(swigCPtr, this, psid, FwSitePSImp.getCPtr(imp), imp);
  }

  public FwTimelinedImp GetTimelinedAttribute(GlUnavailTime.Type type) {
    long cPtr = ScWrapJNI.FwSiteImp_GetTimelinedAttribute__SWIG_0(swigCPtr, this, type.swigValue());
    return (cPtr == 0) ? null : new FwTimelinedImp(cPtr, false);
  }

  public static FwSitePSImpFactory GetPSImpFactory() {
    long cPtr = ScWrapJNI.FwSiteImp_GetPSImpFactory();
    return (cPtr == 0) ? null : new FwSitePSImpFactory(cPtr, false);
  }

  public static void Cleanup() {
    ScWrapJNI.FwSiteImp_Cleanup();
  }

  public void setMyName(String value) {
    ScWrapJNI.FwSiteImp_myName_set(swigCPtr, this, value);
  }

  public String getMyName() {
    return ScWrapJNI.FwSiteImp_myName_get(swigCPtr, this);
  }

  public void setMySiteNumber(int value) {
    ScWrapJNI.FwSiteImp_mySiteNumber_set(swigCPtr, this, value);
  }

  public int getMySiteNumber() {
    return ScWrapJNI.FwSiteImp_mySiteNumber_get(swigCPtr, this);
  }

  public void setMyName4(String value) {
    ScWrapJNI.FwSiteImp_myName4_set(swigCPtr, this, value);
  }

  public String getMyName4() {
    return ScWrapJNI.FwSiteImp_myName4_get(swigCPtr, this);
  }

  public void setMyName2(String value) {
    ScWrapJNI.FwSiteImp_myName2_set(swigCPtr, this, value);
  }

  public String getMyName2() {
    return ScWrapJNI.FwSiteImp_myName2_get(swigCPtr, this);
  }

  public void setMyName1(String value) {
    ScWrapJNI.FwSiteImp_myName1_set(swigCPtr, this, value);
  }

  public String getMyName1() {
    return ScWrapJNI.FwSiteImp_myName1_get(swigCPtr, this);
  }

  public void setMyCCSName(String value) {
    ScWrapJNI.FwSiteImp_myCCSName_set(swigCPtr, this, value);
  }

  public String getMyCCSName() {
    return ScWrapJNI.FwSiteImp_myCCSName_get(swigCPtr, this);
  }

  public void setMyAnt(String value) {
    ScWrapJNI.FwSiteImp_myAnt_set(swigCPtr, this, value);
  }

  public String getMyAnt() {
    return ScWrapJNI.FwSiteImp_myAnt_get(swigCPtr, this);
  }

  public void setMyCCSType(GlSite.CCSType value) {
    ScWrapJNI.FwSiteImp_myCCSType_set(swigCPtr, this, value.swigValue());
  }

  public GlSite.CCSType getMyCCSType() {
    return GlSite.CCSType.swigToEnum(ScWrapJNI.FwSiteImp_myCCSType_get(swigCPtr, this));
  }

  public void setMyType(GlSite.Type value) {
    ScWrapJNI.FwSiteImp_myType_set(swigCPtr, this, value.swigValue());
  }

  public GlSite.Type getMyType() {
    return GlSite.Type.swigToEnum(ScWrapJNI.FwSiteImp_myType_get(swigCPtr, this));
  }

  public void setMyRTSFlag(boolean value) {
    ScWrapJNI.FwSiteImp_myRTSFlag_set(swigCPtr, this, value);
  }

  public boolean getMyRTSFlag() {
    return ScWrapJNI.FwSiteImp_myRTSFlag_get(swigCPtr, this);
  }

  public void setMyDataCollectTimeline(FwTimelinedImp value) {
    ScWrapJNI.FwSiteImp_myDataCollectTimeline_set(swigCPtr, this, FwTimelinedImp.getCPtr(value), value);
  }

  public FwTimelinedImp getMyDataCollectTimeline() {
    long cPtr = ScWrapJNI.FwSiteImp_myDataCollectTimeline_get(swigCPtr, this);
    return (cPtr == 0) ? null : new FwTimelinedImp(cPtr, false);
  }

  public void setMyUplinkAvailableTimeline(FwTimelinedImp value) {
    ScWrapJNI.FwSiteImp_myUplinkAvailableTimeline_set(swigCPtr, this, FwTimelinedImp.getCPtr(value), value);
  }

  public FwTimelinedImp getMyUplinkAvailableTimeline() {
    long cPtr = ScWrapJNI.FwSiteImp_myUplinkAvailableTimeline_get(swigCPtr, this);
    return (cPtr == 0) ? null : new FwTimelinedImp(cPtr, false);
  }

  public void setMyPSsLoaded(boolean value) {
    ScWrapJNI.FwSiteImp_myPSsLoaded_set(swigCPtr, this, value);
  }

  public boolean getMyPSsLoaded() {
    return ScWrapJNI.FwSiteImp_myPSsLoaded_get(swigCPtr, this);
  }

  public void setMyPSs(FwSitePSImpVector value) {
    ScWrapJNI.FwSiteImp_myPSs_set(swigCPtr, this, FwSitePSImpVector.getCPtr(value), value);
  }

  public FwSitePSImpVector getMyPSs() {
    long cPtr = ScWrapJNI.FwSiteImp_myPSs_get(swigCPtr, this);
    return (cPtr == 0) ? null : new FwSitePSImpVector(cPtr, false);
  }

  public void setMyDeletedPSs(FwSitePSImpVector value) {
    ScWrapJNI.FwSiteImp_myDeletedPSs_set(swigCPtr, this, FwSitePSImpVector.getCPtr(value), value);
  }

  public FwSitePSImpVector getMyDeletedPSs() {
    long cPtr = ScWrapJNI.FwSiteImp_myDeletedPSs_get(swigCPtr, this);
    return (cPtr == 0) ? null : new FwSitePSImpVector(cPtr, false);
  }

  public static void setOurPSImpFactory(FwSitePSImpFactory value) {
    ScWrapJNI.FwSiteImp_ourPSImpFactory_set(FwSitePSImpFactory.getCPtr(value), value);
  }

  public static FwSitePSImpFactory getOurPSImpFactory() {
    long cPtr = ScWrapJNI.FwSiteImp_ourPSImpFactory_get();
    return (cPtr == 0) ? null : new FwSitePSImpFactory(cPtr, false);
  }

  public static void setOurDefaultPS(FwSitePSImp value) {
    ScWrapJNI.FwSiteImp_ourDefaultPS_set(FwSitePSImp.getCPtr(value), value);
  }

  public static FwSitePSImp getOurDefaultPS() {
    long cPtr = ScWrapJNI.FwSiteImp_ourDefaultPS_get();
    return (cPtr == 0) ? null : new FwSitePSImp(cPtr, false);
  }

}
