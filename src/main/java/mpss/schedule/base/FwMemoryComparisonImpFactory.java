/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwMemoryComparisonImpFactory extends FactoryChild_FwMemoryComparisonImp {
  private long swigCPtr;

  protected FwMemoryComparisonImpFactory(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwMemoryComparisonImpFactory_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwMemoryComparisonImpFactory obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwMemoryComparisonImpFactory(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FwMemoryComparisonImpFactory_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FwMemoryComparisonImpFactory_change_ownership(this, swigCPtr, true);
  }

  public FwMemoryComparisonImp GetOne(int parentID, String mn, int first, int last, boolean load, boolean dump) {
    long cPtr = (getClass() == FwMemoryComparisonImpFactory.class) ? ScWrapJNI.FwMemoryComparisonImpFactory_GetOne(swigCPtr, this, parentID, mn, first, last, load, dump) : ScWrapJNI.FwMemoryComparisonImpFactory_GetOneSwigExplicitFwMemoryComparisonImpFactory(swigCPtr, this, parentID, mn, first, last, load, dump);
    return (cPtr == 0) ? null : new FwMemoryComparisonImp(cPtr, false);
  }

  public FwMemoryComparisonImpFactory() {
    this(ScWrapJNI.new_FwMemoryComparisonImpFactory__SWIG_0(), true);
    ScWrapJNI.FwMemoryComparisonImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwMemoryComparisonImpFactory(FwMemoryComparisonImpFactory arg0) {
    this(ScWrapJNI.new_FwMemoryComparisonImpFactory__SWIG_1(FwMemoryComparisonImpFactory.getCPtr(arg0), arg0), true);
    ScWrapJNI.FwMemoryComparisonImpFactory_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  protected FwMemoryComparisonImp CreateOne(int parentID, String mn, int first, int last, boolean load, boolean dump) {
    long cPtr = (getClass() == FwMemoryComparisonImpFactory.class) ? ScWrapJNI.FwMemoryComparisonImpFactory_CreateOne(swigCPtr, this, parentID, mn, first, last, load, dump) : ScWrapJNI.FwMemoryComparisonImpFactory_CreateOneSwigExplicitFwMemoryComparisonImpFactory(swigCPtr, this, parentID, mn, first, last, load, dump);
    return (cPtr == 0) ? null : new FwMemoryComparisonImp(cPtr, false);
  }

}
