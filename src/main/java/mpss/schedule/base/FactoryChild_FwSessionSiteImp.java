/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FactoryChild_FwSessionSiteImp extends FwBaseImpFactory {
  private long swigCPtr;

  protected FactoryChild_FwSessionSiteImp(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FactoryChild_FwSessionSiteImp_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FactoryChild_FwSessionSiteImp obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FactoryChild_FwSessionSiteImp(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FactoryChild_FwSessionSiteImp_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FactoryChild_FwSessionSiteImp_change_ownership(this, swigCPtr, true);
  }

  public FwSessionSiteImp GetID(int arg0) {
    long cPtr = ScWrapJNI.FactoryChild_FwSessionSiteImp_GetID(swigCPtr, this, arg0);
    return (cPtr == 0) ? null : new FwSessionSiteImp(cPtr, false);
  }

  public void GetAll(FwSessionSiteImpVector arg0, int parentID) {
    if (getClass() == FactoryChild_FwSessionSiteImp.class) ScWrapJNI.FactoryChild_FwSessionSiteImp_GetAll(swigCPtr, this, FwSessionSiteImpVector.getCPtr(arg0), arg0, parentID); else ScWrapJNI.FactoryChild_FwSessionSiteImp_GetAllSwigExplicitFactoryChild_FwSessionSiteImp(swigCPtr, this, FwSessionSiteImpVector.getCPtr(arg0), arg0, parentID);
  }

  public boolean Store(FwSessionSiteImpVector imps, FwSessionSiteImpVector delimps) {
    return (getClass() == FactoryChild_FwSessionSiteImp.class) ? ScWrapJNI.FactoryChild_FwSessionSiteImp_Store__SWIG_0(swigCPtr, this, FwSessionSiteImpVector.getCPtr(imps), imps, FwSessionSiteImpVector.getCPtr(delimps), delimps) : ScWrapJNI.FactoryChild_FwSessionSiteImp_StoreSwigExplicitFactoryChild_FwSessionSiteImp__SWIG_0(swigCPtr, this, FwSessionSiteImpVector.getCPtr(imps), imps, FwSessionSiteImpVector.getCPtr(delimps), delimps);
  }

  public boolean Store(FwSessionSiteImp imp) {
    return (getClass() == FactoryChild_FwSessionSiteImp.class) ? ScWrapJNI.FactoryChild_FwSessionSiteImp_Store__SWIG_1(swigCPtr, this, FwSessionSiteImp.getCPtr(imp), imp) : ScWrapJNI.FactoryChild_FwSessionSiteImp_StoreSwigExplicitFactoryChild_FwSessionSiteImp__SWIG_1(swigCPtr, this, FwSessionSiteImp.getCPtr(imp), imp);
  }

  public void Delete(FwSessionSiteImp arg0) {
    if (getClass() == FactoryChild_FwSessionSiteImp.class) ScWrapJNI.FactoryChild_FwSessionSiteImp_Delete__SWIG_0(swigCPtr, this, FwSessionSiteImp.getCPtr(arg0), arg0); else ScWrapJNI.FactoryChild_FwSessionSiteImp_DeleteSwigExplicitFactoryChild_FwSessionSiteImp__SWIG_0(swigCPtr, this, FwSessionSiteImp.getCPtr(arg0), arg0);
  }

  public void Delete(FwSessionSiteImpVector arg0) {
    if (getClass() == FactoryChild_FwSessionSiteImp.class) ScWrapJNI.FactoryChild_FwSessionSiteImp_Delete__SWIG_1(swigCPtr, this, FwSessionSiteImpVector.getCPtr(arg0), arg0); else ScWrapJNI.FactoryChild_FwSessionSiteImp_DeleteSwigExplicitFactoryChild_FwSessionSiteImp__SWIG_1(swigCPtr, this, FwSessionSiteImpVector.getCPtr(arg0), arg0);
  }

  public void Set_FwChildImpFactory_JavaInstance(FactoryChild_FwSessionSiteImp ins) {
    ScWrapJNI.FactoryChild_FwSessionSiteImp_Set_FwChildImpFactory_JavaInstance(swigCPtr, this, FactoryChild_FwSessionSiteImp.getCPtr(ins), ins);
  }

  protected FactoryChild_FwSessionSiteImp() {
    this(ScWrapJNI.new_FactoryChild_FwSessionSiteImp(), true);
    ScWrapJNI.FactoryChild_FwSessionSiteImp_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

}
