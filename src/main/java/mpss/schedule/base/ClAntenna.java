/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClAntenna extends ClTimelined {
  private long swigCPtr;

  protected ClAntenna(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.ClAntenna_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClAntenna obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClAntenna(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public void PrintOn(SWIGTYPE_p_std__ostream os) {
    ScWrapJNI.ClAntenna_PrintOn(swigCPtr, this, SWIGTYPE_p_std__ostream.getCPtr(os));
  }

  public boolean _LessThan_(ClAntenna arg0) {
    return ScWrapJNI.ClAntenna__LessThan_(swigCPtr, this, ClAntenna.getCPtr(arg0), arg0);
  }

  public boolean _LessThanOrEqual_(ClAntenna rhs) {
    return ScWrapJNI.ClAntenna__LessThanOrEqual_(swigCPtr, this, ClAntenna.getCPtr(rhs), rhs);
  }

  public boolean _GreaterThan_(ClAntenna rhs) {
    return ScWrapJNI.ClAntenna__GreaterThan_(swigCPtr, this, ClAntenna.getCPtr(rhs), rhs);
  }

  public boolean _GreaterThanOrEqual_(ClAntenna rhs) {
    return ScWrapJNI.ClAntenna__GreaterThanOrEqual_(swigCPtr, this, ClAntenna.getCPtr(rhs), rhs);
  }

  public boolean _Equal_(ClAntenna arg0) {
    return ScWrapJNI.ClAntenna__Equal_(swigCPtr, this, ClAntenna.getCPtr(arg0), arg0);
  }

  public boolean _NotEqual_(ClAntenna rhs) {
    return ScWrapJNI.ClAntenna__NotEqual_(swigCPtr, this, ClAntenna.getCPtr(rhs), rhs);
  }

  public ClAntenna _Asign_(ClAntenna rhs) {
    return new ClAntenna(ScWrapJNI.ClAntenna__Asign_(swigCPtr, this, ClAntenna.getCPtr(rhs), rhs), false);
  }

  public int GetID() {
    return ScWrapJNI.ClAntenna_GetID(swigCPtr, this);
  }

  public ClSatellite GetSatellite() {
    return new ClSatellite(ScWrapJNI.ClAntenna_GetSatellite(swigCPtr, this), true);
  }

  public String GetName() {
    return ScWrapJNI.ClAntenna_GetName(swigCPtr, this);
  }

  public boolean SetName(String Name) {
    return ScWrapJNI.ClAntenna_SetName(swigCPtr, this, Name);
  }

  public String GetType() {
    return ScWrapJNI.ClAntenna_GetType(swigCPtr, this);
  }

  public void SetType(String type) {
    ScWrapJNI.ClAntenna_SetType(swigCPtr, this, type);
  }

  public boolean IsDeleteable(java.lang.String[] INOUT) {
    return ScWrapJNI.ClAntenna_IsDeleteable(swigCPtr, this, INOUT);
  }

}
