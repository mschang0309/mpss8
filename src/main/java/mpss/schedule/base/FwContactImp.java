/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwContactImp extends FwBaseImp {
  private long swigCPtr;

  protected FwContactImp(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FwContactImp_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwContactImp obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    //delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwContactImp(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public int GetSatellite(boolean arg0) {
    return ScWrapJNI.FwContactImp_GetSatellite__SWIG_0(swigCPtr, this, arg0);
  }

  public int GetSatellite() {
    return ScWrapJNI.FwContactImp_GetSatellite__SWIG_1(swigCPtr, this);
  }

  public int GetSite(boolean arg0) {
    return ScWrapJNI.FwContactImp_GetSite__SWIG_0(swigCPtr, this, arg0);
  }

  public int GetSite() {
    return ScWrapJNI.FwContactImp_GetSite__SWIG_1(swigCPtr, this);
  }

  public int GetRev() {
    return ScWrapJNI.FwContactImp_GetRev(swigCPtr, this);
  }

  public GlTimeRange GetTimeRange() {
    return new GlTimeRange(ScWrapJNI.FwContactImp_GetTimeRange(swigCPtr, this), true);
  }

  public int GetBranch() {
    return ScWrapJNI.FwContactImp_GetBranch(swigCPtr, this);
  }

  public void SetSatellite(int arg0) {
    ScWrapJNI.FwContactImp_SetSatellite(swigCPtr, this, arg0);
  }

  public void SetSite(int arg0) {
    ScWrapJNI.FwContactImp_SetSite(swigCPtr, this, arg0);
  }

  public void SetRevID(int arg0) {
    ScWrapJNI.FwContactImp_SetRevID(swigCPtr, this, arg0);
  }

  public void SetTimeRange(GlTimeRange arg0) {
    ScWrapJNI.FwContactImp_SetTimeRange(swigCPtr, this, GlTimeRange.getCPtr(arg0), arg0);
  }

  public void SetBranch(int arg0) {
    ScWrapJNI.FwContactImp_SetBranch(swigCPtr, this, arg0);
  }

  public GlTime GetZeroRise() {
    return new GlTime(ScWrapJNI.FwContactImp_GetZeroRise(swigCPtr, this), true);
  }

  public void SetZeroRise(GlTime Rise) {
    ScWrapJNI.FwContactImp_SetZeroRise(swigCPtr, this, GlTime.getCPtr(Rise), Rise);
  }

  public GlTime GetZeroFade() {
    return new GlTime(ScWrapJNI.FwContactImp_GetZeroFade(swigCPtr, this), true);
  }

  public void SetZeroFade(GlTime Fade) {
    ScWrapJNI.FwContactImp_SetZeroFade(swigCPtr, this, GlTime.getCPtr(Fade), Fade);
  }

  public GlTime GetCmdRise() {
    return new GlTime(ScWrapJNI.FwContactImp_GetCmdRise(swigCPtr, this), true);
  }

  public void SetCmdRise(GlTime Rise) {
    ScWrapJNI.FwContactImp_SetCmdRise(swigCPtr, this, GlTime.getCPtr(Rise), Rise);
  }

  public GlTime GetCmdFade() {
    return new GlTime(ScWrapJNI.FwContactImp_GetCmdFade(swigCPtr, this), true);
  }

  public void SetCmdFade(GlTime Fade) {
    ScWrapJNI.FwContactImp_SetCmdFade(swigCPtr, this, GlTime.getCPtr(Fade), Fade);
  }

  public GlTime GetXbandRise() {
    return new GlTime(ScWrapJNI.FwContactImp_GetXbandRise(swigCPtr, this), true);
  }

  public void SetXbandRise(GlTime Rise) {
    ScWrapJNI.FwContactImp_SetXbandRise(swigCPtr, this, GlTime.getCPtr(Rise), Rise);
  }

  public GlTime GetXbandFade() {
    return new GlTime(ScWrapJNI.FwContactImp_GetXbandFade(swigCPtr, this), true);
  }

  public void SetXbandFade(GlTime Fade) {
    ScWrapJNI.FwContactImp_SetXbandFade(swigCPtr, this, GlTime.getCPtr(Fade), Fade);
  }

  public GlTime GetMaxElTime() {
    return new GlTime(ScWrapJNI.FwContactImp_GetMaxElTime(swigCPtr, this), true);
  }

  public void SetMaxElTime(GlTime maxElTime) {
    ScWrapJNI.FwContactImp_SetMaxElTime(swigCPtr, this, GlTime.getCPtr(maxElTime), maxElTime);
  }

  public double GetMaxEl() {
    return ScWrapJNI.FwContactImp_GetMaxEl(swigCPtr, this);
  }

  public void SetMaxEl(double MaxEl) {
    ScWrapJNI.FwContactImp_SetMaxEl(swigCPtr, this, MaxEl);
  }

  public boolean GetAvailable() {
    return ScWrapJNI.FwContactImp_GetAvailable(swigCPtr, this);
  }

  public void SetAvailable(boolean Available) {
    ScWrapJNI.FwContactImp_SetAvailable(swigCPtr, this, Available);
  }

  public long GetFrontExpansion() {
    return ScWrapJNI.FwContactImp_GetFrontExpansion(swigCPtr, this);
  }

  public void SetFrontExpansion(long FrontExpansion) {
    ScWrapJNI.FwContactImp_SetFrontExpansion(swigCPtr, this, FrontExpansion);
  }

  public long GetBackExpansion() {
    return ScWrapJNI.FwContactImp_GetBackExpansion(swigCPtr, this);
  }

  public void SetBackExpansion(long BackExpansion) {
    ScWrapJNI.FwContactImp_SetBackExpansion(swigCPtr, this, BackExpansion);
  }

  public long GetFrontBias() {
    return ScWrapJNI.FwContactImp_GetFrontBias(swigCPtr, this);
  }

  public long GetBackBias() {
    return ScWrapJNI.FwContactImp_GetBackBias(swigCPtr, this);
  }

  public void SetFrontBias(long arg0) {
    ScWrapJNI.FwContactImp_SetFrontBias(swigCPtr, this, arg0);
  }

  public void SetBackBias(long arg0) {
    ScWrapJNI.FwContactImp_SetBackBias(swigCPtr, this, arg0);
  }

  public FwExtentImpVector GetExtents() {
    return new FwExtentImpVector(ScWrapJNI.FwContactImp_GetExtents(swigCPtr, this), false);
  }

  public FwExtentImpVector GetExpensiveExtents() {
    return new FwExtentImpVector(ScWrapJNI.FwContactImp_GetExpensiveExtents(swigCPtr, this), false);
  }

  public boolean AddExtent(FwExtentImp arg0) {
    return ScWrapJNI.FwContactImp_AddExtent(swigCPtr, this, FwExtentImp.getCPtr(arg0), arg0);
  }

  public void RemoveExtent(FwExtentImp arg0) {
    ScWrapJNI.FwContactImp_RemoveExtent(swigCPtr, this, FwExtentImp.getCPtr(arg0), arg0);
  }

  public boolean SetExtents(FwExtentImpVector arg0) {
    return ScWrapJNI.FwContactImp_SetExtents(swigCPtr, this, FwExtentImpVector.getCPtr(arg0), arg0);
  }

  public FwUnobscuredTimeImpVector GetUnobscuredTimes() {
    return new FwUnobscuredTimeImpVector(ScWrapJNI.FwContactImp_GetUnobscuredTimes(swigCPtr, this), false);
  }

  public boolean AddUnobscuredTime(GlTimeRange arg0) {
    return ScWrapJNI.FwContactImp_AddUnobscuredTime(swigCPtr, this, GlTimeRange.getCPtr(arg0), arg0);
  }

  public void SetUOTCount(int arg0) {
    ScWrapJNI.FwContactImp_SetUOTCount(swigCPtr, this, arg0);
  }

  public void SetUOT(GlTimeRange arg0) {
    ScWrapJNI.FwContactImp_SetUOT(swigCPtr, this, GlTimeRange.getCPtr(arg0), arg0);
  }

  public int GetUOTCount() {
    return ScWrapJNI.FwContactImp_GetUOTCount(swigCPtr, this);
  }

  public GlTimeRange GetUOT() {
    return new GlTimeRange(ScWrapJNI.FwContactImp_GetUOT(swigCPtr, this), true);
  }

  public boolean SaveAssociated() {
    return ScWrapJNI.FwContactImp_SaveAssociated(swigCPtr, this);
  }

  public void RemoveAssociated() {
    ScWrapJNI.FwContactImp_RemoveAssociated(swigCPtr, this);
  }

  public FwContactImp(int arg0, int sat, int site, int ar, GlTime zeroRise, GlTime zeroFade, GlTime cmdRise, GlTime cmdFade, GlTime xbandRise, GlTime xbandFade, GlTime maxElTime, double maxEl, boolean avail, long front, long back, long frontbias, long backbias, int uotcount, GlTimeRange uot, int branch, boolean isNew) {
    this(ScWrapJNI.new_FwContactImp__SWIG_0(arg0, sat, site, ar, GlTime.getCPtr(zeroRise), zeroRise, GlTime.getCPtr(zeroFade), zeroFade, GlTime.getCPtr(cmdRise), cmdRise, GlTime.getCPtr(cmdFade), cmdFade, GlTime.getCPtr(xbandRise), xbandRise, GlTime.getCPtr(xbandFade), xbandFade, GlTime.getCPtr(maxElTime), maxElTime, maxEl, avail, front, back, frontbias, backbias, uotcount, GlTimeRange.getCPtr(uot), uot, branch, isNew), true);
  }

  public FwContactImp(int arg0, int sat, int site, int ar, GlTime zeroRise, GlTime zeroFade, GlTime cmdRise, GlTime cmdFade, GlTime xbandRise, GlTime xbandFade, GlTime maxElTime, double maxEl, boolean avail, long front, long back, long frontbias, long backbias, int uotcount, GlTimeRange uot, int branch) {
    this(ScWrapJNI.new_FwContactImp__SWIG_1(arg0, sat, site, ar, GlTime.getCPtr(zeroRise), zeroRise, GlTime.getCPtr(zeroFade), zeroFade, GlTime.getCPtr(cmdRise), cmdRise, GlTime.getCPtr(cmdFade), cmdFade, GlTime.getCPtr(xbandRise), xbandRise, GlTime.getCPtr(xbandFade), xbandFade, GlTime.getCPtr(maxElTime), maxElTime, maxEl, avail, front, back, frontbias, backbias, uotcount, GlTimeRange.getCPtr(uot), uot, branch), true);
  }

  public String CheckMeString() {
    return ScWrapJNI.FwContactImp_CheckMeString(swigCPtr, this);
  }

  public void Replace(GlTime zeroRise, GlTime zeroFade, GlTime cmdRise, GlTime cmdFade, GlTime xbandRise, GlTime xbandFade, GlTime maxElTime, double maxEl) {
    ScWrapJNI.FwContactImp_Replace(swigCPtr, this, GlTime.getCPtr(zeroRise), zeroRise, GlTime.getCPtr(zeroFade), zeroFade, GlTime.getCPtr(cmdRise), cmdRise, GlTime.getCPtr(cmdFade), cmdFade, GlTime.getCPtr(xbandRise), xbandRise, GlTime.getCPtr(xbandFade), xbandFade, GlTime.getCPtr(maxElTime), maxElTime, maxEl);
  }

  public void DeleteKids() {
    ScWrapJNI.FwContactImp_DeleteKids(swigCPtr, this);
  }

  public void LoadUnobscuredTimes() {
    ScWrapJNI.FwContactImp_LoadUnobscuredTimes(swigCPtr, this);
  }

  public void SetAssociatedSaved() {
    ScWrapJNI.FwContactImp_SetAssociatedSaved(swigCPtr, this);
  }

  public FwExtentImpFactory GetExtentFactory() {
    long cPtr = ScWrapJNI.FwContactImp_GetExtentFactory(swigCPtr, this);
    return (cPtr == 0) ? null : new FwExtentImpFactory(cPtr, false);
  }

  public FwUnobscuredTimeImpFactory GetUnobscuredTimeImpFactory() {
    long cPtr = ScWrapJNI.FwContactImp_GetUnobscuredTimeImpFactory(swigCPtr, this);
    return (cPtr == 0) ? null : new FwUnobscuredTimeImpFactory(cPtr, false);
  }

  public void setMySatelliteID(int value) {
    ScWrapJNI.FwContactImp_mySatelliteID_set(swigCPtr, this, value);
  }

  public int getMySatelliteID() {
    return ScWrapJNI.FwContactImp_mySatelliteID_get(swigCPtr, this);
  }

  public void setMySiteID(int value) {
    ScWrapJNI.FwContactImp_mySiteID_set(swigCPtr, this, value);
  }

  public int getMySiteID() {
    return ScWrapJNI.FwContactImp_mySiteID_get(swigCPtr, this);
  }

  public void setMyRevID(int value) {
    ScWrapJNI.FwContactImp_myRevID_set(swigCPtr, this, value);
  }

  public int getMyRevID() {
    return ScWrapJNI.FwContactImp_myRevID_get(swigCPtr, this);
  }

  public void setMyTimeRange(GlTimeRange value) {
    ScWrapJNI.FwContactImp_myTimeRange_set(swigCPtr, this, GlTimeRange.getCPtr(value), value);
  }

  public GlTimeRange getMyTimeRange() {
    long cPtr = ScWrapJNI.FwContactImp_myTimeRange_get(swigCPtr, this);
    return (cPtr == 0) ? null : new GlTimeRange(cPtr, false);
  }

  public void setMyBranchID(int value) {
    ScWrapJNI.FwContactImp_myBranchID_set(swigCPtr, this, value);
  }

  public int getMyBranchID() {
    return ScWrapJNI.FwContactImp_myBranchID_get(swigCPtr, this);
  }

  public void setMyZeroRise(GlTime value) {
    ScWrapJNI.FwContactImp_myZeroRise_set(swigCPtr, this, GlTime.getCPtr(value), value);
  }

  public GlTime getMyZeroRise() {
    long cPtr = ScWrapJNI.FwContactImp_myZeroRise_get(swigCPtr, this);
    return (cPtr == 0) ? null : new GlTime(cPtr, false);
  }

  public void setMyZeroFade(GlTime value) {
    ScWrapJNI.FwContactImp_myZeroFade_set(swigCPtr, this, GlTime.getCPtr(value), value);
  }

  public GlTime getMyZeroFade() {
    long cPtr = ScWrapJNI.FwContactImp_myZeroFade_get(swigCPtr, this);
    return (cPtr == 0) ? null : new GlTime(cPtr, false);
  }

  public void setMyCmdRise(GlTime value) {
    ScWrapJNI.FwContactImp_myCmdRise_set(swigCPtr, this, GlTime.getCPtr(value), value);
  }

  public GlTime getMyCmdRise() {
    long cPtr = ScWrapJNI.FwContactImp_myCmdRise_get(swigCPtr, this);
    return (cPtr == 0) ? null : new GlTime(cPtr, false);
  }

  public void setMyCmdFade(GlTime value) {
    ScWrapJNI.FwContactImp_myCmdFade_set(swigCPtr, this, GlTime.getCPtr(value), value);
  }

  public GlTime getMyCmdFade() {
    long cPtr = ScWrapJNI.FwContactImp_myCmdFade_get(swigCPtr, this);
    return (cPtr == 0) ? null : new GlTime(cPtr, false);
  }

  public void setMyXbandRise(GlTime value) {
    ScWrapJNI.FwContactImp_myXbandRise_set(swigCPtr, this, GlTime.getCPtr(value), value);
  }

  public GlTime getMyXbandRise() {
    long cPtr = ScWrapJNI.FwContactImp_myXbandRise_get(swigCPtr, this);
    return (cPtr == 0) ? null : new GlTime(cPtr, false);
  }

  public void setMyXbandFade(GlTime value) {
    ScWrapJNI.FwContactImp_myXbandFade_set(swigCPtr, this, GlTime.getCPtr(value), value);
  }

  public GlTime getMyXbandFade() {
    long cPtr = ScWrapJNI.FwContactImp_myXbandFade_get(swigCPtr, this);
    return (cPtr == 0) ? null : new GlTime(cPtr, false);
  }

  public void setMyMaxElTime(GlTime value) {
    ScWrapJNI.FwContactImp_myMaxElTime_set(swigCPtr, this, GlTime.getCPtr(value), value);
  }

  public GlTime getMyMaxElTime() {
    long cPtr = ScWrapJNI.FwContactImp_myMaxElTime_get(swigCPtr, this);
    return (cPtr == 0) ? null : new GlTime(cPtr, false);
  }

  public void setMyMaxEl(double value) {
    ScWrapJNI.FwContactImp_myMaxEl_set(swigCPtr, this, value);
  }

  public double getMyMaxEl() {
    return ScWrapJNI.FwContactImp_myMaxEl_get(swigCPtr, this);
  }

  public void setMyAvailable(boolean value) {
    ScWrapJNI.FwContactImp_myAvailable_set(swigCPtr, this, value);
  }

  public boolean getMyAvailable() {
    return ScWrapJNI.FwContactImp_myAvailable_get(swigCPtr, this);
  }

  public void setMyFrontExpansion(long value) {
    ScWrapJNI.FwContactImp_myFrontExpansion_set(swigCPtr, this, value);
  }

  public long getMyFrontExpansion() {
    return ScWrapJNI.FwContactImp_myFrontExpansion_get(swigCPtr, this);
  }

  public void setMyBackExpansion(long value) {
    ScWrapJNI.FwContactImp_myBackExpansion_set(swigCPtr, this, value);
  }

  public long getMyBackExpansion() {
    return ScWrapJNI.FwContactImp_myBackExpansion_get(swigCPtr, this);
  }

  public void setMyFrontBias(long value) {
    ScWrapJNI.FwContactImp_myFrontBias_set(swigCPtr, this, value);
  }

  public long getMyFrontBias() {
    return ScWrapJNI.FwContactImp_myFrontBias_get(swigCPtr, this);
  }

  public void setMyBackBias(long value) {
    ScWrapJNI.FwContactImp_myBackBias_set(swigCPtr, this, value);
  }

  public long getMyBackBias() {
    return ScWrapJNI.FwContactImp_myBackBias_get(swigCPtr, this);
  }

  public void setMyUOTCount(int value) {
    ScWrapJNI.FwContactImp_myUOTCount_set(swigCPtr, this, value);
  }

  public int getMyUOTCount() {
    return ScWrapJNI.FwContactImp_myUOTCount_get(swigCPtr, this);
  }

  public void setMyUOT(GlTimeRange value) {
    ScWrapJNI.FwContactImp_myUOT_set(swigCPtr, this, GlTimeRange.getCPtr(value), value);
  }

  public GlTimeRange getMyUOT() {
    long cPtr = ScWrapJNI.FwContactImp_myUOT_get(swigCPtr, this);
    return (cPtr == 0) ? null : new GlTimeRange(cPtr, false);
  }

  public void setMyExtents(FwExtentImpVector value) {
    ScWrapJNI.FwContactImp_myExtents_set(swigCPtr, this, FwExtentImpVector.getCPtr(value), value);
  }

  public FwExtentImpVector getMyExtents() {
    long cPtr = ScWrapJNI.FwContactImp_myExtents_get(swigCPtr, this);
    return (cPtr == 0) ? null : new FwExtentImpVector(cPtr, false);
  }

  public void setMyUnobscuredTimesLoaded(boolean value) {
    ScWrapJNI.FwContactImp_myUnobscuredTimesLoaded_set(swigCPtr, this, value);
  }

  public boolean getMyUnobscuredTimesLoaded() {
    return ScWrapJNI.FwContactImp_myUnobscuredTimesLoaded_get(swigCPtr, this);
  }

  public void setMyUnobscuredTimes(FwUnobscuredTimeImpVector value) {
    ScWrapJNI.FwContactImp_myUnobscuredTimes_set(swigCPtr, this, FwUnobscuredTimeImpVector.getCPtr(value), value);
  }

  public FwUnobscuredTimeImpVector getMyUnobscuredTimes() {
    long cPtr = ScWrapJNI.FwContactImp_myUnobscuredTimes_get(swigCPtr, this);
    return (cPtr == 0) ? null : new FwUnobscuredTimeImpVector(cPtr, false);
  }

  public void setMyDeletedKids(FwUnobscuredTimeImpVector value) {
    ScWrapJNI.FwContactImp_myDeletedKids_set(swigCPtr, this, FwUnobscuredTimeImpVector.getCPtr(value), value);
  }

  public FwUnobscuredTimeImpVector getMyDeletedKids() {
    long cPtr = ScWrapJNI.FwContactImp_myDeletedKids_get(swigCPtr, this);
    return (cPtr == 0) ? null : new FwUnobscuredTimeImpVector(cPtr, false);
  }

  public static void setOurExtentFactory(FwExtentImpFactory value) {
    ScWrapJNI.FwContactImp_ourExtentFactory_set(FwExtentImpFactory.getCPtr(value), value);
  }

  public static FwExtentImpFactory getOurExtentFactory() {
    long cPtr = ScWrapJNI.FwContactImp_ourExtentFactory_get();
    return (cPtr == 0) ? null : new FwExtentImpFactory(cPtr, false);
  }

  public static void setOurUnobscuredTimeImpFactory(FwUnobscuredTimeImpFactory value) {
    ScWrapJNI.FwContactImp_ourUnobscuredTimeImpFactory_set(FwUnobscuredTimeImpFactory.getCPtr(value), value);
  }

  public static FwUnobscuredTimeImpFactory getOurUnobscuredTimeImpFactory() {
    long cPtr = ScWrapJNI.FwContactImp_ourUnobscuredTimeImpFactory_get();
    return (cPtr == 0) ? null : new FwUnobscuredTimeImpFactory(cPtr, false);
  }

}
