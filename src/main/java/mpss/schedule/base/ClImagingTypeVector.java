/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClImagingTypeVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClImagingTypeVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClImagingTypeVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClImagingTypeVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClImagingTypeVector() {
    this(ScWrapJNI.new_ClImagingTypeVector(), true);
  }

  public long size() {
    return ScWrapJNI.ClImagingTypeVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.ClImagingTypeVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.ClImagingTypeVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.ClImagingTypeVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.ClImagingTypeVector_clear(swigCPtr, this);
  }

  public void add(ClImagingType x) {
    ScWrapJNI.ClImagingTypeVector_add(swigCPtr, this, ClImagingType.getCPtr(x), x);
  }

  public ClImagingType get(int i) {
    return new ClImagingType(ScWrapJNI.ClImagingTypeVector_get(swigCPtr, this, i), false);
  }

  public void set(int i, ClImagingType val) {
    ScWrapJNI.ClImagingTypeVector_set(swigCPtr, this, i, ClImagingType.getCPtr(val), val);
  }

}
