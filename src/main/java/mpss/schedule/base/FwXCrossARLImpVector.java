/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FwXCrossARLImpVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected FwXCrossARLImpVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FwXCrossARLImpVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    //delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_FwXCrossARLImpVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public FwXCrossARLImpVector() {
    this(ScWrapJNI.new_FwXCrossARLImpVector__SWIG_0(), true);
  }

  public FwXCrossARLImpVector(long n) {
    this(ScWrapJNI.new_FwXCrossARLImpVector__SWIG_1(n), true);
  }

  public long size() {
    return ScWrapJNI.FwXCrossARLImpVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.FwXCrossARLImpVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.FwXCrossARLImpVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.FwXCrossARLImpVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.FwXCrossARLImpVector_clear(swigCPtr, this);
  }

  public void add(FwXCrossARLImp x) {
    ScWrapJNI.FwXCrossARLImpVector_add(swigCPtr, this, FwXCrossARLImp.getCPtr(x), x);
  }

  public FwXCrossARLImp get(int i) {
    long cPtr = ScWrapJNI.FwXCrossARLImpVector_get(swigCPtr, this, i);
    return (cPtr == 0) ? null : new FwXCrossARLImp(cPtr, false);
  }

  public void set(int i, FwXCrossARLImp val) {
    ScWrapJNI.FwXCrossARLImpVector_set(swigCPtr, this, i, FwXCrossARLImp.getCPtr(val), val);
  }

}
