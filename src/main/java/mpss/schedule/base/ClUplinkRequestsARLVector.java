/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClUplinkRequestsARLVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClUplinkRequestsARLVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClUplinkRequestsARLVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClUplinkRequestsARLVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClUplinkRequestsARLVector() {
    this(ScWrapJNI.new_ClUplinkRequestsARLVector(), true);
  }

  public long size() {
    return ScWrapJNI.ClUplinkRequestsARLVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.ClUplinkRequestsARLVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.ClUplinkRequestsARLVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.ClUplinkRequestsARLVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.ClUplinkRequestsARLVector_clear(swigCPtr, this);
  }

  public void add(ClUplinkRequestsARL x) {
    ScWrapJNI.ClUplinkRequestsARLVector_add(swigCPtr, this, ClUplinkRequestsARL.getCPtr(x), x);
  }

  public ClUplinkRequestsARL get(int i) {
    return new ClUplinkRequestsARL(ScWrapJNI.ClUplinkRequestsARLVector_get(swigCPtr, this, i), false);
  }

  public void set(int i, ClUplinkRequestsARL val) {
    ScWrapJNI.ClUplinkRequestsARLVector_set(swigCPtr, this, i, ClUplinkRequestsARL.getCPtr(val), val);
  }

}
