/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClSiteVector {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClSiteVector(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClSiteVector obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClSiteVector(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClSiteVector() {
    this(ScWrapJNI.new_ClSiteVector(), true);
  }

  public long size() {
    return ScWrapJNI.ClSiteVector_size(swigCPtr, this);
  }

  public long capacity() {
    return ScWrapJNI.ClSiteVector_capacity(swigCPtr, this);
  }

  public void reserve(long n) {
    ScWrapJNI.ClSiteVector_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return ScWrapJNI.ClSiteVector_isEmpty(swigCPtr, this);
  }

  public void clear() {
    ScWrapJNI.ClSiteVector_clear(swigCPtr, this);
  }

  public void add(ClSite x) {
    ScWrapJNI.ClSiteVector_add(swigCPtr, this, ClSite.getCPtr(x), x);
  }

  public ClSite get(int i) {
    return new ClSite(ScWrapJNI.ClSiteVector_get(swigCPtr, this, i), false);
  }

  public void set(int i, ClSite val) {
    ScWrapJNI.ClSiteVector_set(swigCPtr, this, i, ClSite.getCPtr(val), val);
  }

}
