/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class FactoryParent_FwImagingTypImp extends FwBaseImpFactory {
  private long swigCPtr;

  protected FactoryParent_FwImagingTypImp(long cPtr, boolean cMemoryOwn) {
    super(ScWrapJNI.FactoryParent_FwImagingTypImp_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(FactoryParent_FwImagingTypImp obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        throw new UnsupportedOperationException("C++ destructor does not have public access");
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    ScWrapJNI.FactoryParent_FwImagingTypImp_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    ScWrapJNI.FactoryParent_FwImagingTypImp_change_ownership(this, swigCPtr, true);
  }

  public FactoryParent_FwImagingTypImp() {
    this(ScWrapJNI.new_FactoryParent_FwImagingTypImp(), true);
    ScWrapJNI.FactoryParent_FwImagingTypImp_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

  public FwImagingTypeImp GetID(int arg0) {
    long cPtr = ScWrapJNI.FactoryParent_FwImagingTypImp_GetID(swigCPtr, this, arg0);
    return (cPtr == 0) ? null : new FwImagingTypeImp(cPtr, false);
  }

  public void GetAll(FwImagingTypeImpVector arg0) {
    ScWrapJNI.FactoryParent_FwImagingTypImp_GetAll(swigCPtr, this, FwImagingTypeImpVector.getCPtr(arg0), arg0);
  }

  public void Delete(FwImagingTypeImpVector arg0) {
    ScWrapJNI.FactoryParent_FwImagingTypImp_Delete__SWIG_0(swigCPtr, this, FwImagingTypeImpVector.getCPtr(arg0), arg0);
  }

  public void Delete(FwImagingTypeImp arg0) {
    ScWrapJNI.FactoryParent_FwImagingTypImp_Delete__SWIG_1(swigCPtr, this, FwImagingTypeImp.getCPtr(arg0), arg0);
  }

  public boolean Store(FwImagingTypeImpVector vec) {
    return (getClass() == FactoryParent_FwImagingTypImp.class) ? ScWrapJNI.FactoryParent_FwImagingTypImp_Store__SWIG_0(swigCPtr, this, FwImagingTypeImpVector.getCPtr(vec), vec) : ScWrapJNI.FactoryParent_FwImagingTypImp_StoreSwigExplicitFactoryParent_FwImagingTypImp__SWIG_0(swigCPtr, this, FwImagingTypeImpVector.getCPtr(vec), vec);
  }

  public boolean Store(FwImagingTypeImpVector savem, FwImagingTypeImpVector deleteem) {
    return (getClass() == FactoryParent_FwImagingTypImp.class) ? ScWrapJNI.FactoryParent_FwImagingTypImp_Store__SWIG_1(swigCPtr, this, FwImagingTypeImpVector.getCPtr(savem), savem, FwImagingTypeImpVector.getCPtr(deleteem), deleteem) : ScWrapJNI.FactoryParent_FwImagingTypImp_StoreSwigExplicitFactoryParent_FwImagingTypImp__SWIG_1(swigCPtr, this, FwImagingTypeImpVector.getCPtr(savem), savem, FwImagingTypeImpVector.getCPtr(deleteem), deleteem);
  }

  public boolean Store(FwImagingTypeImp imp) {
    return (getClass() == FactoryParent_FwImagingTypImp.class) ? ScWrapJNI.FactoryParent_FwImagingTypImp_Store__SWIG_2(swigCPtr, this, FwImagingTypeImp.getCPtr(imp), imp) : ScWrapJNI.FactoryParent_FwImagingTypImp_StoreSwigExplicitFactoryParent_FwImagingTypImp__SWIG_2(swigCPtr, this, FwImagingTypeImp.getCPtr(imp), imp);
  }

  public boolean Store() {
    return ScWrapJNI.FactoryParent_FwImagingTypImp_Store__SWIG_3(swigCPtr, this);
  }

  public boolean DeleteAll() {
    return ScWrapJNI.FactoryParent_FwImagingTypImp_DeleteAll(swigCPtr, this);
  }

  public void CancelAll() {
    if (getClass() == FactoryParent_FwImagingTypImp.class) ScWrapJNI.FactoryParent_FwImagingTypImp_CancelAll(swigCPtr, this); else ScWrapJNI.FactoryParent_FwImagingTypImp_CancelAllSwigExplicitFactoryParent_FwImagingTypImp(swigCPtr, this);
  }

  public void Cancel(FwImagingTypeImp arg0) {
    ScWrapJNI.FactoryParent_FwImagingTypImp_Cancel(swigCPtr, this, FwImagingTypeImp.getCPtr(arg0), arg0);
  }

  public void SyncList(FwImagingTypeImpVector arg0) {
    ScWrapJNI.FactoryParent_FwImagingTypImp_SyncList(swigCPtr, this, FwImagingTypeImpVector.getCPtr(arg0), arg0);
  }

  public FwImagingTypeImp SyncImp(FwImagingTypeImp arg0) {
    long cPtr = ScWrapJNI.FactoryParent_FwImagingTypImp_SyncImp(swigCPtr, this, FwImagingTypeImp.getCPtr(arg0), arg0);
    return (cPtr == 0) ? null : new FwImagingTypeImp(cPtr, false);
  }

  public void AddID(FwImagingTypeImp arg0) {
    ScWrapJNI.FactoryParent_FwImagingTypImp_AddID(swigCPtr, this, FwImagingTypeImp.getCPtr(arg0), arg0);
  }

  public void Cleanup() {
    ScWrapJNI.FactoryParent_FwImagingTypImp_Cleanup(swigCPtr, this);
  }

  public void Set_FwParentImpFactory_JavaInstance(FactoryParent_FwImagingTypImp ins) {
    ScWrapJNI.FactoryParent_FwImagingTypImp_Set_FwParentImpFactory_JavaInstance(swigCPtr, this, FactoryParent_FwImagingTypImp.getCPtr(ins), ins);
  }

  protected void AddNew(FwImagingTypeImp arg0) {
    if (getClass() == FactoryParent_FwImagingTypImp.class) ScWrapJNI.FactoryParent_FwImagingTypImp_AddNew(swigCPtr, this, FwImagingTypeImp.getCPtr(arg0), arg0); else ScWrapJNI.FactoryParent_FwImagingTypImp_AddNewSwigExplicitFactoryParent_FwImagingTypImp(swigCPtr, this, FwImagingTypeImp.getCPtr(arg0), arg0);
  }

  protected void AddDeleted(FwImagingTypeImp arg0) {
    if (getClass() == FactoryParent_FwImagingTypImp.class) ScWrapJNI.FactoryParent_FwImagingTypImp_AddDeleted(swigCPtr, this, FwImagingTypeImp.getCPtr(arg0), arg0); else ScWrapJNI.FactoryParent_FwImagingTypImp_AddDeletedSwigExplicitFactoryParent_FwImagingTypImp(swigCPtr, this, FwImagingTypeImp.getCPtr(arg0), arg0);
  }

  protected void DoCancel(FwImagingTypeImp arg0) {
    if (getClass() == FactoryParent_FwImagingTypImp.class) ScWrapJNI.FactoryParent_FwImagingTypImp_DoCancel(swigCPtr, this, FwImagingTypeImp.getCPtr(arg0), arg0); else ScWrapJNI.FactoryParent_FwImagingTypImp_DoCancelSwigExplicitFactoryParent_FwImagingTypImp(swigCPtr, this, FwImagingTypeImp.getCPtr(arg0), arg0);
  }

  protected void DoCancelAll() {
    if (getClass() == FactoryParent_FwImagingTypImp.class) ScWrapJNI.FactoryParent_FwImagingTypImp_DoCancelAll(swigCPtr, this); else ScWrapJNI.FactoryParent_FwImagingTypImp_DoCancelAllSwigExplicitFactoryParent_FwImagingTypImp(swigCPtr, this);
  }

  protected void DoGetAll(FwImagingTypeImpVector arg0) {
    if (getClass() == FactoryParent_FwImagingTypImp.class) ScWrapJNI.FactoryParent_FwImagingTypImp_DoGetAll(swigCPtr, this, FwImagingTypeImpVector.getCPtr(arg0), arg0); else ScWrapJNI.FactoryParent_FwImagingTypImp_DoGetAllSwigExplicitFactoryParent_FwImagingTypImp(swigCPtr, this, FwImagingTypeImpVector.getCPtr(arg0), arg0);
  }

}
