/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.11
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package mpss.schedule.base;

public class ClCompressionRatio {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected ClCompressionRatio(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ClCompressionRatio obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        ScWrapJNI.delete_ClCompressionRatio(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public ClCompressionRatio(int arg0) {
    this(ScWrapJNI.new_ClCompressionRatio__SWIG_0(arg0), true);
  }

  public ClCompressionRatio _Equal_(ClCompressionRatio rhs) {
    return new ClCompressionRatio(ScWrapJNI.ClCompressionRatio__Equal_(swigCPtr, this, ClCompressionRatio.getCPtr(rhs), rhs), false);
  }

  public ClCompressionRatio(ClCompressionRatio rhs) {
    this(ScWrapJNI.new_ClCompressionRatio__SWIG_1(ClCompressionRatio.getCPtr(rhs), rhs), true);
  }

  public int GetID() {
    return ScWrapJNI.ClCompressionRatio_GetID(swigCPtr, this);
  }

  public int GetImagingModeID() {
    return ScWrapJNI.ClCompressionRatio_GetImagingModeID(swigCPtr, this);
  }

  public void SetImagingModeID(int arg0) {
    ScWrapJNI.ClCompressionRatio_SetImagingModeID(swigCPtr, this, arg0);
  }

  public double GetRatio() {
    return ScWrapJNI.ClCompressionRatio_GetRatio(swigCPtr, this);
  }

  public void SetRatio(double arg0) {
    ScWrapJNI.ClCompressionRatio_SetRatio(swigCPtr, this, arg0);
  }

  public boolean Store() {
    return ScWrapJNI.ClCompressionRatio_Store(swigCPtr, this);
  }

  public void Delete() {
    ScWrapJNI.ClCompressionRatio_Delete(swigCPtr, this);
  }

  public void PrintOn(SWIGTYPE_p_std__ostream os) {
    ScWrapJNI.ClCompressionRatio_PrintOn(swigCPtr, this, SWIGTYPE_p_std__ostream.getCPtr(os));
  }

}
