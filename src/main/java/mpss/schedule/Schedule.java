/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mpss.schedule;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import mpss.configFactory;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Eclipse;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Extentactivitytimerange;
import mpss.common.jpa.Externalactivityrequestinfo;
import mpss.common.jpa.Rev;
import mpss.common.jpa.Satellitep;
import mpss.common.jpa.Session;
import mpss.common.jpa.Sessionsatxref;
import mpss.common.jpa.Unavailtimerange;
import mpss.common.jpa.Xcrossrequest;
import mpss.schedule.base.ClActivity;
import mpss.schedule.base.ClActivityVector;
import mpss.schedule.base.ClBranch;
import mpss.schedule.base.ClContact;
import mpss.schedule.base.ClMaster;
import mpss.schedule.base.ClParameters;
import mpss.schedule.base.ClRSIImagingARLEntry;
import mpss.schedule.base.ClSatellite;
import mpss.schedule.base.ClSatelliteVector;
import mpss.schedule.base.ClSchedule;
import mpss.schedule.base.ClSession;
import mpss.schedule.base.ClSite;
import mpss.schedule.base.ClSiteVector;
import mpss.schedule.base.FwImpFactory;
import mpss.schedule.base.FwRSIImagingARLEntryImp;
import mpss.schedule.base.GlIDVector;
import mpss.schedule.base.GlLog;
import mpss.schedule.base.GlStringVector;
import mpss.schedule.base.GlTimeRange;
import mpss.schedule.base.ScScheduler;
import mpss.schedule.base.ScSchedulerType;
import mpss.schedule.imp.*;
import mpss.util.ExtentActivityTimeRangeUtils;
import mpss.util.IlogLogUtil;
import mpss.util.timeformat.DateUtil;

/**
 * 
 * @author user
 */
public class Schedule {

	public static enum SchType {
		COLD, HOT, RESCH, MOVE,ADJUST,RESCH_FILTER,NONE
	}
	
	public static enum SessionType {
		NORMAL,BRANCH
	}
	
	public static int branch_type_id = 0;

	public static SchType type = SchType.NONE;
	public static SessionType s_type = SessionType.NORMAL;
	public static boolean updateDB = true;

	public static int coldrun1 = 0;
	public static int coldrun2 = 0;
	public static int hotrun1 = 0;
	public static int hotrun2 = 0;
	public static int resch = 0;	
	public static int old_extent_count=0;
	public static int new_extent_count=0;
	
	public static int debugging =10;

	@SuppressWarnings("rawtypes")
	private static HashMap<String, UIDB> facts = new HashMap<String, UIDB>();
	
	private static AntennaImpFactory mAntennaFac = null;
	private static ArlImpFactory mARLFac = null;
	private static ActivityImpFactory mActivityFac = null;
	private static ContactImpFactory mContactFac = null;
	private static ExtentImpFactory mExtentFac = null;
	private static GlobalParametersImpFactory mParameterFac = null;
	private static ParameterSetImpFactory mParameSetFac = null;
	private static SessionImpFactory mSessionFac = null;
	private static SitePSImpFactory mSitePsFac = null;
	private static SatelliteImpFactory mSatelliteFac = null;
	private static SiteImpFactory mSiteFac = null;
	private static SessionSatImpFactory mSessionSatFac = null;
	private static SessionSiteImpFactory mSessionSiteFac = null;
	private static SessionSatARLImpFactory mSessionSatARLFac = null;
	private static RevImpFactory mRevFac = null;
	private static SatellitePSImpFactory mSatellitePSFac = null;
	private static UnavailTimeRangeImpFactory munavailTRFac = null;
	private static ExternalActivityRequestInfoImpFactory earFac = null;
	private static RecorderImpFactory recorderFac = null;
	private static RecorderPSImpFactory recorderpFac = null;
	private static TransmitterImpFactory transmitterFac = null;
	private static DutyCycleImpFactory dutycycleFac = null;
	private static ElementsImpFactory elementsFac = null;
	private static EclipseImpFactory eclipseFac = null;
	private static ManeuverARLEntryImpFactory maneuverFac = null;
	private static RSIImagingARLEntryImpFactory rsiimagingFac = null;
	private static ImagingModeImpFactory imagingFac = null;
	private static SiteRSIActPrefImpFactory sitersiactprefFac = null;
	private static ExtentActivityTimeRangeImpFactory extentactivitytimerangeFac = null;
	private static UnobscuredTimeImpFactory unobscuredtimeFac = null;
	private static MiscCmdProcImpFactory misccmdprocFac = null;
	private static XcrossProcImpFactory xcrossFac = null;
	private static UplinkProcImpFactory uplinkrequestsFac = null;
	private static RtcCmdProcImpFactory rtcmdProcFac = null;
	private static IsualProcImpFactory isualProcFac = null;
	private static AipProcImpFactory aipProcFac = null;	
	private static ParamSetImpFactory parametersPSFac = null;
	
	private static Logger logger = Logger.getLogger(configFactory.getLogName());

	static {
		initLib();
		initFwModule();
	}

	public Schedule() {
		
	}

	@SuppressWarnings("rawtypes")
	public static UIDB getUiDbFactory(String tablename) {
		return facts.get(tablename);
	}
	
	public static ActivityImpFactory getActivityImpFactory(){
		return mActivityFac;
	}

	public ClSession getSession(String sessionID, Date sessionStart,Date sessionStop) {		
		ClSession session = null;
		try {
			session = new ClSession(sessionID, DateUtil.getGlTimeRange(sessionStart, sessionStop));

		} catch (Throwable e) {
			logger.info("Create ClSession fail:" + e.getMessage());			
		}
		return session;
	}

	/**
	 * 
	 */

	public static void initFwModule() {
		FwImpFactory instance = new FwImpFactory();
		try {
			// ****Dynamic instance would be crash
			// X ==> instance.SetSessionImpFactory(new SessionImpFactory());
			// O ==> instance.SetSessionImpFactory(mSessionFac);

			mParameSetFac = new ParameterSetImpFactory();
			mAntennaFac = new AntennaImpFactory();
			mSessionFac = new SessionImpFactory();
			mActivityFac = new ActivityImpFactory();
			mSatelliteFac = new SatelliteImpFactory();
			mSiteFac = new SiteImpFactory();
			mSitePsFac = new SitePSImpFactory();
			mParameterFac = new GlobalParametersImpFactory();
			mSessionSatFac = new SessionSatImpFactory();
			mSessionSiteFac = new SessionSiteImpFactory();
			mSessionSatARLFac = new SessionSatARLImpFactory();
			mARLFac = new ArlImpFactory();
			mContactFac = new ContactImpFactory();
			mExtentFac = new ExtentImpFactory();
			earFac = new ExternalActivityRequestInfoImpFactory();
			mRevFac = new RevImpFactory();
			mSatellitePSFac = new SatellitePSImpFactory();
			munavailTRFac = new UnavailTimeRangeImpFactory();
			recorderFac = new RecorderImpFactory();
			recorderpFac = new RecorderPSImpFactory();
			transmitterFac = new TransmitterImpFactory();
			dutycycleFac = new DutyCycleImpFactory();
			elementsFac = new ElementsImpFactory();
			eclipseFac = new EclipseImpFactory();
			maneuverFac = new ManeuverARLEntryImpFactory();
			rsiimagingFac = new RSIImagingARLEntryImpFactory();
			imagingFac = new ImagingModeImpFactory();
			sitersiactprefFac = new SiteRSIActPrefImpFactory();
			extentactivitytimerangeFac = new ExtentActivityTimeRangeImpFactory();
			unobscuredtimeFac = new UnobscuredTimeImpFactory();			
			misccmdprocFac = new MiscCmdProcImpFactory();
			xcrossFac = new XcrossProcImpFactory();
			uplinkrequestsFac = new UplinkProcImpFactory();
			rtcmdProcFac = new RtcCmdProcImpFactory();		
			isualProcFac = new IsualProcImpFactory();			
			aipProcFac = new AipProcImpFactory();
			parametersPSFac = new ParamSetImpFactory();
			
			
			mExtentFac.setEatrFactory(extentactivitytimerangeFac);

			instance.SetAntennaImpFactory(mAntennaFac);
			instance.SetActivityImpFactory(mActivityFac);
			instance.SetARLImpFactory(mARLFac);
			instance.SetSatelliteImpFactory(mSatelliteFac);
			instance.SetParametersImpFactory(mParameterFac);
			instance.SetParamSetImpFactory(mParameSetFac);			
			instance.SetParametersPSImpFactory(parametersPSFac);			
			instance.SetSessionImpFactory(mSessionFac);
			instance.SetSessionSatImpFactory(mSessionSatFac);
			instance.SetSessionSatARLImpFactory(mSessionSatARLFac);
			instance.SetSessionSiteImpFactory(mSessionSiteFac);
			instance.SetFwSiteImpFactory(mSiteFac);
			instance.SetSitePSImpFactory(mSitePsFac);
			instance.SetExtentImpFactory(mExtentFac);
			instance.SetContactImpFactory(mContactFac);
			instance.SetRevImpFactory(mRevFac);
			instance.SetSatellitePSImpFactory(mSatellitePSFac);
			instance.SetUnavailTimeRangeImpFactory(munavailTRFac);
			instance.SetExternalActivityRequestInfoImpFactory(earFac);
			instance.SetRecorderImpFactory(recorderFac);
			instance.SetRecorderPSImpFactory(recorderpFac);
			instance.SetTransmitterImpFactory(transmitterFac);
			instance.SetDutyCycleImpFactory(dutycycleFac);
			instance.SetElementsImpFactory(elementsFac);
			instance.SetEclipseImpFactory(eclipseFac);
			instance.SetManeuverARLEntryImpFactory(maneuverFac);
			instance.SetRSIImagingARLEntryImpFactory(rsiimagingFac);
			instance.SetImagingModeImpFactory(imagingFac);
			instance.SetSiteRSIActPrefImpFactory(sitersiactprefFac);
			instance.SetExtentActivityTimeRangeImpFactory(extentactivitytimerangeFac);
			instance.SetUnobscuredTimeImpFactory(unobscuredtimeFac);
			instance.SetMiscCmdProcARLEntryImpFactory(misccmdprocFac);
			instance.SetXCrossARLEntryImpFactory(xcrossFac);
			instance.SetUplinkRequestsARLEntryImpFactory(uplinkrequestsFac);
			instance.SetRTCmdProcARLEntryImpFactory(rtcmdProcFac);
			instance.SetISUALProcARLEntryImpFactory(isualProcFac);
			

			facts.put("Parameterset", mParameSetFac);
			facts.put("Antenna", mAntennaFac);
			facts.put("Session", mSessionFac);
			facts.put("Activity", mActivityFac);
			facts.put("Satellite", mSatelliteFac);
			facts.put("Site", mSiteFac);
			facts.put("Siteps", mSitePsFac);
			facts.put("Globalparameter", mParameterFac);
			facts.put("Sessionsatxref", mSessionSatFac);
			facts.put("Sessionsitexref", mSessionSiteFac);
			facts.put("Sessionsatarlxref", mSessionSatARLFac);
			facts.put("Arl", mARLFac);
			facts.put("Contact", mContactFac);
			facts.put("Extent", mExtentFac);
			facts.put("Externalactivityrequestinfo", earFac);
			facts.put("Rev", mRevFac);
			facts.put("Satellitep", mSatellitePSFac);
			facts.put("Unavailtimerange", munavailTRFac);
			facts.put("Recorder", recorderFac);
			facts.put("Recorderp", recorderpFac);
			facts.put("Transmitter", transmitterFac);
			facts.put("Dutycycle", dutycycleFac);
			facts.put("Element", elementsFac);
			facts.put("Eclipse", eclipseFac);
			facts.put("Maneuverrequest", maneuverFac);
			facts.put("Rsiimagingrequest", rsiimagingFac);
			facts.put("Imagingmode", imagingFac);
			facts.put("Sitersiactpref", sitersiactprefFac);
			facts.put("Extentactivitytimerange", extentactivitytimerangeFac);
			facts.put("Unobscuredtime", unobscuredtimeFac);
			facts.put("Misccmdprocrequests", misccmdprocFac);
			facts.put("Xcrossrequests", xcrossFac);
			facts.put("Uplinkrequests", uplinkrequestsFac);
			facts.put("Rtcmdprocrequests", rtcmdProcFac);
			facts.put("Isualprocrequests", isualProcFac);

		} catch (Exception e) {
			logger.info("Create factory of FW module fail:"+ e.getMessage());	
			System.exit(-1);
		}		
		FwImpFactory.Init(instance);
	}

	public static void resetFwImp() {

		mParameSetFac.releaseFwImp();
		mAntennaFac.releaseFwImp();
		mSessionFac.releaseFwImp();
		mActivityFac.releaseFwImp();
		mSatelliteFac.releaseFwImp();
		mSiteFac.releaseFwImp();
		mSitePsFac.releaseFwImp();
		mParameterFac.releaseFwImp();
		mSessionSatFac.releaseFwImp();
		mSessionSiteFac.releaseFwImp();
		mSessionSatARLFac.releaseFwImp();
		mARLFac.releaseFwImp();
		mContactFac.releaseFwImp();
		mExtentFac.releaseFwImp();
		earFac.releaseFwImp();
		mRevFac.releaseFwImp();		
		munavailTRFac.releaseFwImp();
		recorderFac.releaseFwImp();
		recorderpFac.releaseFwImp();
		transmitterFac.releaseFwImp();
		dutycycleFac.releaseFwImp();
		elementsFac.releaseFwImp();
		eclipseFac.releaseFwImp();
		maneuverFac.releaseFwImp();
		rsiimagingFac.releaseFwImp();
		imagingFac.releaseFwImp();
		sitersiactprefFac.releaseFwImp();
		extentactivitytimerangeFac.releaseFwImp();
		unobscuredtimeFac.releaseFwImp();
		misccmdprocFac.releaseFwImp();
		xcrossFac.releaseFwImp();
		uplinkrequestsFac.releaseFwImp();
		rtcmdProcFac.releaseFwImp();
		isualProcFac.releaseFwImp();
		aipProcFac.releaseFwImp();		
	}

	public static void initcount() {
		
		if(System.getProperty("user.mode","0").equals("0")){
			ScSchedulerType.SetMode(0);
		}else{
			ScSchedulerType.SetMode(1);
		}
		
		Schedule.coldrun1 = 0;
		Schedule.coldrun2 = 0;
		Schedule.hotrun1 = 0;
		Schedule.hotrun2 = 0;
		Schedule.resch=0;
		Schedule.old_extent_count=0;
		Schedule.new_extent_count=0;
		s_type = SessionType.NORMAL;
		
		ScSchedulerType.SetRocsat(configFactory.getRocsat());
	}	

	public static void initLib() {
		try {
			//logger.info("Load ScDll.dll path:" + System.getProperty("java.library.path"));	
			System.loadLibrary("ScDll");
			GlLog.SetLog(IlogLogUtil.getFullFileName());			
		} catch (Exception ex) {
			logger.info("Load ScDll.dll error:" + ex.getMessage());	
			System.exit(-1);
		}
	}
	
	private void setExtentActivityTimeRangeUtils(int sessionid){		
		List<Sessionsatxref> refs =mSessionSatFac.getSessionsatxrefBySessionID(sessionid);
		if(refs.size()>0){
			Sessionsatxref ref=refs.get(0);			
			Session session =mSessionFac.getSessionByID(sessionid);			
			Satellitep ps =mSatellitePSFac.getBySatIdNPsId(ref.getSatelliteid(), session.getPsid());			
			ExtentActivityTimeRangeUtils.rsirecsetupdelay=ps.getRsirecsetupdelay();
			ExtentActivityTimeRangeUtils.rsireccleanupdelay=ps.getRsireccleanupdelay();
			ExtentActivityTimeRangeUtils.rsipbsetupdelay=ps.getRsipbsetupdelay();
			ExtentActivityTimeRangeUtils.rsipbcleanupdelay=ps.getRsipbcleanupdelay();
		}			
	}
	
	private int getExtentTotalCount(){		
		return mExtentFac.getExtentTotalCount();
	}
	
	private HashMap<Long,Long> copyObite(Session norSession,Session branch){
		HashMap<Long,Long> contactid_maps = new HashMap<Long,Long>();
		logger.info("copy contact rev eclispe");	
		System.exit(-1);
		int extend_int =2*(102*60);
        List<Rev> lists_r =mRevFac.getRevByTime(DateUtil.dateToString(DateUtil.timestampToDate(norSession.getStarttime()),"yyyy-MM-dd HH:mm:ss"), DateUtil.dateToString(DateUtil.DateAddSec(DateUtil.timestampToDate(branch.getEndtime()),extend_int),"yyyy-MM-dd HH:mm:ss"), 0);
		logger.info("rev size ="+ lists_r.size());	
		
		HashMap<Long,Long> revid_maps = new HashMap<Long,Long>();
		for(Rev rev :lists_r){
			rev.setBranchid(branch.getId().intValue());
			Long id= rev.getId();
			rev.setId(null);					
			revid_maps.put(id, mRevFac.createGetId(rev));					
		}
		
        List<Eclipse> lists_e =eclipseFac.getEclipseByTime(DateUtil.dateToString(DateUtil.timestampToDate(norSession.getStarttime()),"yyyy-MM-dd HH:mm:ss"), DateUtil.dateToString(DateUtil.DateAddSec(DateUtil.timestampToDate(branch.getEndtime()),extend_int),"yyyy-MM-dd HH:mm:ss"), 0);
		logger.info("eclispe size ="+ lists_e.size());
		
		// copy contact rev eclispe	
		for(Eclipse eclipse :lists_e){
			eclipse.setBranchid(branch.getId().intValue());	
			if(revid_maps.get(Long.valueOf(eclipse.getRevid().intValue()))!=null){
				eclipse.setRevid(revid_maps.get(Long.valueOf(eclipse.getRevid().intValue())).intValue());					
				eclipse.setId(null);
				eclipseFac.create(eclipse);
			}							
		}
		
					
        List<Contact> lists_c =mContactFac.getContactByTime(DateUtil.dateToString(DateUtil.timestampToDate(norSession.getStarttime()),"yyyy-MM-dd HH:mm:ss"), DateUtil.dateToString(DateUtil.DateAddSec(DateUtil.timestampToDate(branch.getEndtime()),extend_int),"yyyy-MM-dd HH:mm:ss"), 0);
		logger.info("contact size ="+ lists_c.size());
		for(Contact contact :lists_c){
			contact.setBranchid(branch.getId().intValue());	
			if(revid_maps.get(Long.valueOf(contact.getRevid().intValue()))!=null){
				contact.setRevid(revid_maps.get(Long.valueOf(contact.getRevid().intValue())).intValue());
				Long id= contact.getId();
				contact.setId(null);
				contactid_maps.put(id, mContactFac.createGetId(contact));						
			}									
		}	
		
		List<Unavailtimerange> lists_u =munavailTRFac.getUnavailtimerangeByTime(DateUtil.dateToString(DateUtil.timestampToDate(norSession.getStarttime()),"yyyy-MM-dd HH:mm:ss"), DateUtil.dateToString(DateUtil.DateAddSec(DateUtil.timestampToDate(branch.getEndtime()),extend_int),"yyyy-MM-dd HH:mm:ss"), 0);
		logger.info("Unavailtimerange size ="+ lists_c.size());
		for(Unavailtimerange u_timerarnge :lists_u){
			u_timerarnge.setBranchid(branch.getId().intValue());
			u_timerarnge.setId(null);
			munavailTRFac.create(u_timerarnge);												
		}		
		
		branch.setAcqsumloaded(1);	
		mSessionFac.update(branch);
		return contactid_maps;
	}
	
	public void deleteObite(Session branch){
		logger.info("delete contact rev eclispe");
		mRevFac.doSql("DELETE FROM rev  WHERE branchid="+ branch.getId());
		eclipseFac.doSql("DELETE FROM eclipse  WHERE branchid="+ branch.getId());
		mContactFac.doSql("DELETE FROM contact  WHERE branchid="+ branch.getId());
		mContactFac.doSql("DELETE FROM unavailtimerange  WHERE branchid="+ branch.getId());
	}	
	
	// for rsi and aip , rsi =1 , aip =2
	public Long copyUserCreatActivity(int mode,HashMap<Long,Long> map,int arlid){
		Long tempid= new Long(0); ;
		if(mode==1){
			tempid =rsiimagingFac.getArl(arlid);
		}else{
			try{
				tempid =aipProcFac.getArl(arlid);
			}catch(Exception ex){
				System.out.println("");
			}
			
		}
		
		int new_arlid =mARLFac.addArlByIDBranch(tempid.intValue());
		logger.info("get new arlid="+new_arlid);
		if(new_arlid!=-1){
			if(mode==1){
				HashMap<Long,Long> temp= rsiimagingFac.copyArl(rsiimagingFac.getArl(arlid).intValue(),new_arlid);
				for (Long key : temp.keySet()) {		            
		            map.put(key, temp.get(key));
		            return temp.get(key);
		        }
				
			}else if(mode==2){
				HashMap<Long,Long> temp= aipProcFac.copyArl(aipProcFac.getArl(arlid).intValue(),new_arlid);
				for (Long key : temp.keySet()) {		            
		            map.put(key, temp.get(key));
		            return temp.get(key);
		        }				
			}
			
		}
		return null;
	}
	
	private HashMap<Long,Long> addMap(HashMap<Long,Long> maps,HashMap<Long,Long> tempmaps){
		for (Long key : tempmaps.keySet()) {		            
            maps.put(key, tempmaps.get(key));           
        }
		return maps;
	}
	
	private void checkColdSchIsbranch(String sessionName){
		Session session =mSessionFac.getSessionByName(sessionName);
		List<Sessionsatxref> sesssats = mSessionSatFac.getSessionsatxrefBySessionID(session.getId().intValue());
		Session norSession =mSessionFac.getSessionbyTime(session.getStarttime(), 0, sesssats.get(0).getSatelliteid());
		HashMap<Long,Long> contactid_maps = new HashMap<Long,Long>();
		if(session.getIsbranch()==1){						
			if(session.getAcqsumloaded()==0){
				// copy obite ,unavailtimerange
				contactid_maps = copyObite(norSession,session);
			}else{	
				// delete obite ,unavailtimerange
				deleteObite(session);
				// copy obite ,unavailtimerange
				contactid_maps = copyObite(norSession,session);				
			}
			
		}
		
		
		HashMap<Long,Long> rsimaps = new HashMap<Long,Long>();
		HashMap<Long,Long> isualprocmaps = new HashMap<Long,Long>();
		HashMap<Long,Long> maneuvermaps = new HashMap<Long,Long>();		
		HashMap<Long,Long> aipmaps = new HashMap<Long,Long>();
		
		//HashMap<Long,Long> isualimagemaps = new HashMap<Long,Long>();	  // ??  not this factory	
		HashMap<Long,Long> misccmdprocmaps = new HashMap<Long,Long>();		
		HashMap<Long,Long> rtcmdprocmaps = new HashMap<Long,Long>();
		HashMap<Long,Long> uplinkmaps = new HashMap<Long,Long>();
		HashMap<Long,Long> xcrossmaps = new HashMap<Long,Long>();
		
		
		if(session.getIsbranch()==1){			
			if(session.getNominalapplied()==0){
				logger.info("copy Nominal ers...");
				//int extend_int =2*(102*60);
				List<Long> arlids =earFac.getArlId(DateUtil.dateToString(DateUtil.DateAddSec(DateUtil.timestampToDate(session.getStarttime()),0),"yyyy-MM-dd HH:mm:ss"), DateUtil.dateToString(DateUtil.DateAddSec(DateUtil.timestampToDate(session.getEndtime()),0),"yyyy-MM-dd HH:mm:ss"));
				
				for(Long arlid:arlids){
					if(arlid!=-1){					
						int new_arlid =mARLFac.addArlByID(arlid.intValue() ,sessionName);
						logger.info("get new arlid="+new_arlid);
						
						Externalactivityrequestinfo info =earFac.getArlById(arlid);
						info.setId(null);
						info.setArlid(new_arlid);					
						info.setBranchid(Long.valueOf(session.getId()).intValue());
						info.setEndtime(session.getStarttime());
						earFac.create(info);
						logger.info("save  Externalactivityrequestinfo new arlid="+new_arlid);
						
						rsimaps = addMap(rsimaps , rsiimagingFac.copyArl(arlid.intValue(),new_arlid));
						isualprocmaps = addMap(isualprocmaps,isualProcFac.copyArl(arlid.intValue(),new_arlid));							
						maneuvermaps = addMap(maneuvermaps,maneuverFac.copyArl(arlid.intValue(),new_arlid));
						aipmaps = addMap(aipmaps,aipProcFac.copyArl(arlid.intValue(),new_arlid));						
						misccmdprocmaps = addMap(misccmdprocmaps,misccmdprocFac.copyArl(arlid.intValue(),new_arlid));						
						rtcmdprocmaps = addMap(rtcmdprocmaps,rtcmdProcFac.copyArl(arlid.intValue(),new_arlid));
						uplinkmaps = addMap(uplinkmaps,uplinkrequestsFac.copyArl(arlid.intValue(),new_arlid));
						xcrossmaps = addMap(xcrossmaps,xcrossFac.copyArl(arlid.intValue(),new_arlid));						
						
					}else{					
						logger.info("no arlid ");
					}
					
				}				
				
				
				logger.info("copy Nominal activity");
				HashMap<Long,Long> tmaps = new HashMap<Long,Long>();				
				Collection<Activity> actcols=mActivityFac.getNorSession(norSession.getId().intValue(),DateUtil.dateToString(DateUtil.timestampToDate(session.getStarttime()),"yyyy-MM-dd HH:mm:ss"));
				
				for(Activity act :actcols){
					act.setSessionid(session.getId().intValue());
					Integer old_extendid = act.getExtentid();	
					act.setExtentid(0);
					act.setScheduled(0);
					Long id= act.getId();
					act.setId(null);															
					Long activityid =  mActivityFac.createGetId(act);
					act.setId(activityid);
					tmaps.put(id,activityid);
					
					if(old_extendid!=0){
						if(act.getArlentrytype().equals("DELETE")){
							Extent ex =mExtentFac.getExtentById(old_extendid);
							ex.setId(null);
							ex.setActivityid(activityid.intValue());
							ex.setBranchid(session.getId().intValue());
							Long extendid =mExtentFac.createGetId(ex);
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							new_act.setExtentid(extendid.intValue());
							new_act.setScheduled(1);
							if(new_act.getArlentryid()!=0){
								if(rsimaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(rsimaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}else{
									Long newarlid =copyUserCreatActivity(1,rsimaps,new_act.getArlentryid());
									if(newarlid!=null){
										new_act.setArlentryid(newarlid.intValue());
									}else{										
										logger.info("branch copy delete error  ");
									}
								}							
							}
							mActivityFac.update(new_act);
							Schedule.coldrun1 =Schedule.coldrun1+1;
							
						}else if(act.getArlentrytype().equals("MON")){
							Extent ex =mExtentFac.getExtentById(old_extendid);
							ex.setId(null);
							ex.setActivityid(activityid.intValue());
							ex.setBranchid(session.getId().intValue());
							if(ex.getContactid()!=0){
								if(contactid_maps.get(Long.valueOf(ex.getContactid().intValue()))!=null){
									ex.setContactid(contactid_maps.get(Long.valueOf(ex.getContactid().intValue())).intValue());
								}							
							}
							Long extendid =mExtentFac.createGetId(ex);
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							new_act.setExtentid(extendid.intValue());
							new_act.setScheduled(1);
							mActivityFac.update(new_act);	
							Schedule.coldrun1 =Schedule.coldrun1+1;
							
						}else if(act.getArlentrytype().equals("UPLINK")){
							Extent ex =mExtentFac.getExtentById(old_extendid);
							ex.setId(null);
							ex.setActivityid(activityid.intValue());
							ex.setBranchid(session.getId().intValue());
							if(ex.getContactid()!=0){
								if(contactid_maps.get(Long.valueOf(ex.getContactid().intValue()))!=null){
									ex.setContactid(contactid_maps.get(Long.valueOf(ex.getContactid().intValue())).intValue());
								}							
							}
							Long extendid =mExtentFac.createGetId(ex);
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							new_act.setExtentid(extendid.intValue());
							new_act.setScheduled(1);
							if(new_act.getArlentryid()!=0){
								if(uplinkmaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(uplinkmaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}							
							}
							//mActivityFac.update(new_act);
							
						}else if(act.getArlentrytype().equals("RTCMD")){
							Extent ex =mExtentFac.getExtentById(old_extendid);
							ex.setId(null);
							ex.setActivityid(activityid.intValue());
							ex.setBranchid(session.getId().intValue());
							if(ex.getContactid()!=0){
								if(contactid_maps.get(Long.valueOf(ex.getContactid().intValue()))!=null){
									ex.setContactid(contactid_maps.get(Long.valueOf(ex.getContactid().intValue())).intValue());
								}							
							}
							Long extendid =mExtentFac.createGetId(ex);
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							new_act.setExtentid(extendid.intValue());
							new_act.setScheduled(1);
							if(new_act.getArlentryid()!=0){
								if(rtcmdprocmaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(rtcmdprocmaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}							
							}
							//mActivityFac.update(new_act);
							
						}else if(act.getArlentrytype().equals("MISC")){
							Extent ex =mExtentFac.getExtentById(old_extendid);
							ex.setId(null);
							ex.setActivityid(activityid.intValue());
							ex.setBranchid(session.getId().intValue());
							Long extendid =mExtentFac.createGetId(ex);
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							new_act.setExtentid(extendid.intValue());
							new_act.setScheduled(1);
							if(new_act.getArlentryid()!=0){
								if(misccmdprocmaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(misccmdprocmaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}							
							}
							//mActivityFac.update(new_act);
							
						}else if(act.getArlentrytype().equals("ISUALPROC")){
							Extent ex =mExtentFac.getExtentById(old_extendid);
							ex.setId(null);
							ex.setActivityid(activityid.intValue());
							ex.setBranchid(session.getId().intValue());
							if(ex.getContactid()!=0){
								if(contactid_maps.get(Long.valueOf(ex.getContactid().intValue()))!=null){
									ex.setContactid(contactid_maps.get(Long.valueOf(ex.getContactid().intValue())).intValue());
								}							
							}
							Long extendid =mExtentFac.createGetId(ex);
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							new_act.setExtentid(extendid.intValue());
							new_act.setScheduled(1);
							if(new_act.getArlentryid()!=0){
								if(isualprocmaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(isualprocmaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}							
							}
							//mActivityFac.update(new_act);
							
						}else if(act.getArlentrytype().equals("MANEUVER")){
							Extent ex =mExtentFac.getExtentById(old_extendid);
							ex.setId(null);
							ex.setActivityid(activityid.intValue());
							ex.setBranchid(session.getId().intValue());
							Long extendid =mExtentFac.createGetId(ex);
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							new_act.setExtentid(extendid.intValue());
							new_act.setScheduled(1);
							if(new_act.getArlentryid()!=0){
								if(maneuvermaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(maneuvermaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}							
							}
							//mActivityFac.update(new_act);
							
						}else if(act.getArlentrytype().equals("XCROSS")){
							Extent ex =mExtentFac.getExtentById(old_extendid);
							ex.setId(null);
							ex.setActivityid(activityid.intValue());
							ex.setBranchid(session.getId().intValue());
							Long extendid =mExtentFac.createGetId(ex);
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							new_act.setExtentid(extendid.intValue());
							new_act.setScheduled(1);
							if(new_act.getArlentryid()!=0){
								if(xcrossmaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(xcrossmaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}							
							}
							//mActivityFac.update(new_act);
							
						}else if(act.getArlentrytype().equals("AIPPROC")){
							Extent ex =mExtentFac.getExtentById(old_extendid);
							ex.setId(null);
							ex.setActivityid(activityid.intValue());
							ex.setBranchid(session.getId().intValue());							
							Long extendid =mExtentFac.createGetId(ex);
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							new_act.setExtentid(extendid.intValue());
							new_act.setScheduled(1);
							if(new_act.getArlentryid()!=0){
								if(aipmaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(aipmaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}else{
									Long newarlid =copyUserCreatActivity(2,rsimaps,new_act.getArlentryid());
									if(newarlid!=null){
										new_act.setArlentryid(newarlid.intValue());
									}else{										
										logger.info("branch copy aipproc error  ");
									}
								}	
								
							}
							mActivityFac.update(new_act);
							Schedule.coldrun1 =Schedule.coldrun1+1;
							
						}else if(act.getArlentrytype().equals("AIPPBK")){
							Extent ex =mExtentFac.getExtentById(old_extendid);
							ex.setId(null);
							ex.setActivityid(activityid.intValue());
							ex.setBranchid(session.getId().intValue());
							if(ex.getContactid()!=0){
								if(contactid_maps.get(Long.valueOf(ex.getContactid().intValue()))!=null){
									ex.setContactid(contactid_maps.get(Long.valueOf(ex.getContactid().intValue())).intValue());
								}							
							}
							Long extendid =mExtentFac.createGetId(ex);
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							new_act.setExtentid(extendid.intValue());
							new_act.setScheduled(1);							
							mActivityFac.update(new_act);			
							Schedule.coldrun1 =Schedule.coldrun1+1;
						}else if(act.getArlentrytype().equals("RSI")){
							// ddt rec pbk 
							if(act.getActsubtype().equals("REC")){	
								try{
									Extent ex =mExtentFac.getExtentById(old_extendid);
									ex.setId(null);
									ex.setActivityid(activityid.intValue());
									ex.setBranchid(session.getId().intValue());								
									Long extendid =mExtentFac.createGetId(ex);
									Activity new_act =mActivityFac.getById(activityid.intValue());	
									System.out.println("rec id ="+new_act.getId());
									new_act.setExtentid(extendid.intValue());
									new_act.setScheduled(1);
									if(new_act.getArlentryid()!=0){
										if(rsimaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
											new_act.setArlentryid(rsimaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
										}else{
											Long newarlid =copyUserCreatActivity(1,rsimaps,new_act.getArlentryid());
											if(newarlid!=null){
												new_act.setArlentryid(newarlid.intValue());
											}else{										
												logger.info("branch copy rec  error  ");
											}
										}						
									}
									
									mActivityFac.update(new_act);								
									ArrayList<Extentactivitytimerange> ranges =extentactivitytimerangeFac.GetByExtentId(old_extendid);
									for(Extentactivitytimerange range:ranges){
										range.setParentid(extendid.intValue());
										extentactivitytimerangeFac.insertExtentActivityTimeRange(range);
									}
								}catch(Exception ex){
									logger.info("branch copy rec  error 2 ");
								}
								
								
							}else if(act.getActsubtype().equals("PBK")){
								Extent ex =mExtentFac.getExtentById(old_extendid);
								ex.setId(null);
								ex.setActivityid(activityid.intValue());
								ex.setBranchid(session.getId().intValue());
								if(ex.getContactid()!=0){
									if(contactid_maps.get(Long.valueOf(ex.getContactid().intValue()))!=null){
										ex.setContactid(contactid_maps.get(Long.valueOf(ex.getContactid().intValue())).intValue());
									}							
								}
								Long extendid =mExtentFac.createGetId(ex);
								Activity new_act =mActivityFac.getById(activityid.intValue());						
								new_act.setExtentid(extendid.intValue());
								new_act.setScheduled(1);
								if(new_act.getArlentryid()!=0){
									if(rsimaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
										new_act.setArlentryid(rsimaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
									}else{
										Long newarlid =copyUserCreatActivity(1,rsimaps,new_act.getArlentryid());
										if(newarlid!=null){
											new_act.setArlentryid(newarlid.intValue());
										}else{										
											logger.info("branch copy pbk error  ");
										}
									}							
								}
								mActivityFac.update(new_act);
								Schedule.coldrun1 =Schedule.coldrun1+1;
								
								ArrayList<Extentactivitytimerange> ranges =extentactivitytimerangeFac.GetByExtentId(old_extendid);
								for(Extentactivitytimerange range:ranges){
									range.setParentid(extendid.intValue());
									extentactivitytimerangeFac.insertExtentActivityTimeRange(range);
								}
								
							}else if(act.getActsubtype().equals("DDT")){
								Extent ex =mExtentFac.getExtentById(old_extendid);
								ex.setId(null);
								ex.setActivityid(activityid.intValue());
								ex.setBranchid(session.getId().intValue());								
								Long extendid =mExtentFac.createGetId(ex);
								Activity new_act =mActivityFac.getById(activityid.intValue());						
								new_act.setExtentid(extendid.intValue());
								new_act.setScheduled(1);
								if(new_act.getArlentryid()!=0){
									if(rsimaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
										new_act.setArlentryid(rsimaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
									}else{
										Long newarlid =copyUserCreatActivity(1,rsimaps,new_act.getArlentryid());
										if(newarlid!=null){
											new_act.setArlentryid(newarlid.intValue());
										}else{										
											logger.info("branch copy ddt error  ");
										}
									}							
								}
								mActivityFac.update(new_act);
								Schedule.coldrun1 =Schedule.coldrun1+1;
								ArrayList<Extentactivitytimerange> ranges =extentactivitytimerangeFac.GetByExtentId(old_extendid);
								for(Extentactivitytimerange range:ranges){
									range.setParentid(extendid.intValue());
									extentactivitytimerangeFac.insertExtentActivityTimeRange(range);
								}
							}
							
						}else if(act.getArlentrytype().equals("ISUAL")){
							// ??
							
						}					

						
					}else{
						// unschedule
						if(act.getArlentrytype().equals("DELETE")){	
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							if(new_act.getArlentryid()!=0){
								if(rsimaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(rsimaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}else{
									Long newarlid =copyUserCreatActivity(1,rsimaps,new_act.getArlentryid());
									if(newarlid!=null){
										new_act.setArlentryid(newarlid.intValue());
									}else{										
										logger.info("branch copy delete error  ");
									}
								}							
							}
							mActivityFac.update(new_act);
							Schedule.coldrun1 =Schedule.coldrun1+1;
							Schedule.coldrun2 =Schedule.coldrun2+1;
						}else if(act.getArlentrytype().equals("MON")){							
						}else if(act.getArlentrytype().equals("UPLINK")){
							Activity new_act =mActivityFac.getById(activityid.intValue());							
							if(new_act.getArlentryid()!=0){
								if(uplinkmaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(uplinkmaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}							
							}
							//mActivityFac.update(new_act);
							
						}else if(act.getArlentrytype().equals("RTCMD")){							
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							if(new_act.getArlentryid()!=0){
								if(rtcmdprocmaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(rtcmdprocmaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}							
							}
							//mActivityFac.update(new_act);
							
						}else if(act.getArlentrytype().equals("MISC")){
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							if(new_act.getArlentryid()!=0){
								if(misccmdprocmaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(misccmdprocmaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}							
							}
							//mActivityFac.update(new_act);
							
						}else if(act.getArlentrytype().equals("ISUALPROC")){
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							if(new_act.getArlentryid()!=0){
								if(isualprocmaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(isualprocmaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}							
							}
							//mActivityFac.update(new_act);
							
						}else if(act.getArlentrytype().equals("MANEUVER")){
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							if(new_act.getArlentryid()!=0){
								if(maneuvermaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(maneuvermaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}							
							}
							//mActivityFac.update(new_act);
							
						}else if(act.getArlentrytype().equals("XCROSS")){
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							if(new_act.getArlentryid()!=0){
								if(xcrossmaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(xcrossmaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}							
							}
							//mActivityFac.update(new_act);
							
						}else if(act.getArlentrytype().equals("AIPPROC")){
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							if(new_act.getArlentryid()!=0){
								if(aipmaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
									new_act.setArlentryid(aipmaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
								}else{
									Long newarlid =copyUserCreatActivity(2,rsimaps,new_act.getArlentryid());
									if(newarlid!=null){
										new_act.setArlentryid(newarlid.intValue());
									}else{										
										logger.info("branch copy aipproc error  ");
									}
								}							
							}
							mActivityFac.update(new_act);
							Schedule.coldrun1 =Schedule.coldrun1+1;
							Schedule.coldrun2 =Schedule.coldrun2+1;
							
						}else if(act.getArlentrytype().equals("AIPPBK")){	
							Activity new_act =mActivityFac.getById(activityid.intValue());						
							mActivityFac.update(new_act);
							Schedule.coldrun1 =Schedule.coldrun1+1;
							Schedule.coldrun2 =Schedule.coldrun2+1;
						}else if(act.getArlentrytype().equals("RSI")){
							// ddt rec pbk 
							if(act.getActsubtype().equals("REC")){								
								Activity new_act =mActivityFac.getById(activityid.intValue());						
								if(new_act.getArlentryid()!=0){
									if(rsimaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
										new_act.setArlentryid(rsimaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
									}else{
										Long newarlid =copyUserCreatActivity(1,rsimaps,new_act.getArlentryid());
										if(newarlid!=null){
											new_act.setArlentryid(newarlid.intValue());
										}else{										
											logger.info("branch copy rec error  ");
										}
									}							
								}
								mActivityFac.update(new_act);
								Schedule.coldrun1 =Schedule.coldrun1+1;
								Schedule.coldrun2 =Schedule.coldrun2+1;
							}else if(act.getActsubtype().equals("PBK")){	
								Activity new_act =mActivityFac.getById(activityid.intValue());						
								if(new_act.getArlentryid()!=0){
									if(rsimaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
										new_act.setArlentryid(rsimaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
									}else{
										Long newarlid =copyUserCreatActivity(1,rsimaps,new_act.getArlentryid());
										if(newarlid!=null){
											new_act.setArlentryid(newarlid.intValue());
										}else{										
											logger.info("branch copy pbk error  ");
										}
									}							
								}
								mActivityFac.update(new_act);
								Schedule.coldrun1 =Schedule.coldrun1+1;
								Schedule.coldrun2 =Schedule.coldrun2+1;
							}else if(act.getActsubtype().equals("DDT")){
								Activity new_act =mActivityFac.getById(activityid.intValue());						
								if(new_act.getArlentryid()!=0){
									if(rsimaps.get(Long.valueOf(new_act.getArlentryid()))!=null){
										new_act.setArlentryid(rsimaps.get(Long.valueOf(new_act.getArlentryid())).intValue());
									}else{
										Long newarlid =copyUserCreatActivity(1,rsimaps,new_act.getArlentryid());
										if(newarlid!=null){
											new_act.setArlentryid(newarlid.intValue());
										}else{										
											logger.info("branch copy ddt error  ");
										}
									}						
								}
								mActivityFac.update(new_act);
								Schedule.coldrun1 =Schedule.coldrun1+1;
								Schedule.coldrun2 =Schedule.coldrun2+1;
							}
							
						}else if(act.getArlentrytype().equals("ISUAL")){							
							
						}
					}
					
				}
				
				// update  prevactid ,nextactid
				for(Activity act :actcols){
					
					Activity new_act =mActivityFac.getById(act.getId().intValue());						
					if(new_act.getPrevactid()!=0){
						if(tmaps.get(Long.valueOf(new_act.getPrevactid()))!=null){
							new_act.setPrevactid(tmaps.get(Long.valueOf(new_act.getPrevactid())).intValue());
						}							
					}
					if(new_act.getNextactid()!=0){
						if(tmaps.get(Long.valueOf(new_act.getNextactid()))!=null){
							new_act.setNextactid(tmaps.get(Long.valueOf(new_act.getNextactid())).intValue());
						}							
					}
					mActivityFac.update(new_act);
					
				}
				
				session.setNominalapplied(1);
			}else{
				
			}
		}
		session.setColdscheduled(1);
		mSessionFac.update(session);
		
	}
	
	private void checkIsBranch(String sessionName){
		resetFwImp();
		Session session =mSessionFac.getSessionByName(sessionName);
		if(session.getIsbranch()==1){
			s_type = SessionType.BRANCH;
			branch_type_id = session.getId().intValue();
		}	
		resetFwImp();
	}
	
	private void checkIsBranch(int sessionid){
		resetFwImp();
		Session session =mSessionFac.getSessionByID(sessionid);
		if(session.getIsbranch()==1){
			s_type = SessionType.BRANCH;
			branch_type_id = session.getId().intValue();
		}	
		resetFwImp();
	}
	
	private boolean checkAIP(int sessionid){
        Collection<Activity> acts =mActivityFac.getSchAip(sessionid,1);
        if(acts.size()>0){
        	 //return true;
             return false;
        }else{   
             return false;
        }
    }



	public boolean runCold(String sessionName) {
		
		Schedule.initcount();
		Schedule.type = SchType.COLD;
		checkIsBranch(sessionName);					
			
		
        if(Schedule.s_type == SessionType.NORMAL){
        	ClSession ses = new ClSession(sessionName);			
    		return this.runCold(ses);
		}else{
			// branch
			checkColdSchIsbranch(sessionName);
			return true;
		}		
		
		
		
	}

	public boolean runCold(ClSession session) {
		boolean flag = true;
		try {
			System.out.println(session.GetStartTime().utc());
			System.out.println(session.GetStopTime().utc());
			String starttime = session.GetStartTime().asString("%Y-%m-%d %H:%M:%S");
			String endtime = session.GetStopTime().asString("%Y-%m-%d %H:%M:%S");
			logger.info("run cold session time : start =" + starttime + ", stop =" + endtime);
			
			setExtentActivityTimeRangeUtils(session.GetID());
			
			if (session.GetColdScheduled() == true) {
				logger.info("Session " + session.GetName()+ " has already been scheduled.");
			} else {
				ClParameters parms = new ClParameters();
				long timetorun = parms.GetColdSchedulingLimit();
				logger.debug("Cold Schedule limit:" + timetorun);
				ScScheduler scr;
				try {
					ScSchedulerType.InitILOG();
					
					scr = ScScheduler.Create(session);
					
					int totalActs = 0;
					int failedActs = 0;
					
					ScScheduler.SetDebugging(debugging);

					// scr.GenerateColdSchedule(totalActs,failedActs,timetorun,true);
					scr.GenerateColdSchedule(totalActs, failedActs, timetorun,false);	
				    //  mon not to sch must delete 
					mActivityFac.DeleteMon(session.GetID());
					scr = null;					
				} catch (Throwable e) {
					logger.info("Create Schedule fail:" + e.getMessage());
					flag = false;
				}
			}

		} catch (Exception ex) {
			logger.info("java schudule ex======" + ex.getMessage());
			flag = false;
		} catch (Throwable e) {
			logger.info("java schudule Throwable======" + e.getMessage());
			flag = false;
		} finally {
			resetFwImp();
		}

		return flag;
	}
	
	private GlTimeRange getSessionTimeRage(ClSession ses){
		
		if(Schedule.s_type == SessionType.NORMAL){
			Date d1 = DateUtil.stringToDate(ses.GetStartTime().asString("%Y-%m-%d %H:%M:%S"),"yyyy-MM-dd HH:mm:ss");
			Date d2 = DateUtil.stringToDate(ses.GetStopTime().asString("%Y-%m-%d %H:%M:%S"),"yyyy-MM-dd HH:mm:ss");
			Calendar cal = Calendar.getInstance();
			cal.setTime(d2);
			cal.add(Calendar.MINUTE, 1644);
			return DateUtil.getGlTimeRange(d1, cal.getTime());			
		}else{
			Session session =mSessionFac.getSessionByName(ses.GetName());
			List<Sessionsatxref> sesssats = mSessionSatFac.getSessionsatxrefBySessionID(session.getId().intValue());
			Session norSession =mSessionFac.getSessionbyTime(session.getStarttime(), 0, sesssats.get(0).getSatelliteid());
			ClSession temp = new ClSession(norSession.getId().intValue());
			Date d1 = DateUtil.stringToDate(temp.GetStartTime().asString("%Y-%m-%d %H:%M:%S"),"yyyy-MM-dd HH:mm:ss");
			Date d2 = DateUtil.stringToDate(ses.GetStopTime().asString("%Y-%m-%d %H:%M:%S"),"yyyy-MM-dd HH:mm:ss");
			Calendar cal = Calendar.getInstance();
			cal.setTime(d2);
			cal.add(Calendar.MINUTE, 1644);
			return DateUtil.getGlTimeRange(d1, cal.getTime());			
		}
		
	}

	public boolean runHot(int sessionID) {	
		Schedule.initcount();
		checkIsBranch(sessionID);		
		Schedule.type = SchType.HOT;
		setExtentActivityTimeRangeUtils(sessionID);
		ClSession ses = new ClSession(sessionID);	
		ClBranch bn = null;	
		//ClBranch bn = new ClBranch(ses.GetID());	
		ses.SetTimeRange(getSessionTimeRage(ses));	
		ClSchedule sched = new ClSchedule(ses.GetTimeRange(), bn);
		return this.runHot(ses, sched);
		
	}

	public boolean runHot(ClSession session, ClSchedule sched) {
		boolean flag = true;
		try{
			
			ClSatelliteVector sateVec = new ClSatelliteVector();
			ClSiteVector siteVec = new ClSiteVector();
			ClActivityVector actVec = new ClActivityVector();
			ClActivityVector nothingActVec = new ClActivityVector();
			GlIDVector nothingIDs = new GlIDVector();
			int total = 0;
			int failed = 0;
			ScScheduler myScheduler;
			ScSchedulerType.InitILOG();	
			
			myScheduler = ScScheduler.Create(sched, true);		
			ScScheduler.SetDebugging(debugging);
			
			if (sched.GetBranchID() == 0) {
					GetSessionInfo(sateVec, siteVec);
			} else {
					ClBranch branch = new ClBranch(sched.GetBranchID());
					branch.GetSatellites(sateVec);
					branch.GetSites(siteVec);					
					branch.delete();
			}	
			

			ClActivity.Get(actVec, session);

			String starttime = session.GetStartTime().asString("%Y-%m-%d %H:%M:%S");
			String endtime = session.GetStopTime().asString("%Y-%m-%d %H:%M:%S");
			logger.info("run hot session time : start =" + starttime	+ ", stop =" + endtime);
			if (!myScheduler.HotScheduleAll(sateVec, siteVec,sched.GetTimeRange(), false, total, failed, nothingActVec,nothingIDs, actVec)) {
					logger.info("hot sch fail ======="+ myScheduler.GetFailReason());
					flag = false;
			}			

			//  xcross not to sch must delete 
			mActivityFac.DeleteNoXcross(session.GetID());
			// contact is unuse , if it has xcross need delete
			Collection<Activity> acts =mActivityFac.getSchXcross(session.GetID(),1);
			for(Activity act : acts){					
				Xcrossrequest cr =xcrossFac.getXcrossrequestByID(Long.valueOf(act.getArlentryid().intValue()));		
				if(cr!=null){
					long id= cr.getContactid();
					//System.out.println("in sch xcross ,contact id="+String.valueOf(id).toString());
					if(id!=0){
						if(!mExtentFac.getContactHasExtent(id)){
							//delete xcross
							logger.info("sch end : delete xcross id , activity id ="+act.getId());
							mActivityFac.DeleteNoXcross(act);
							xcrossFac.delete(cr);							
						}
					}
				}			    
			}			
			
			myScheduler.delete();
			myScheduler = null;		
			
		}catch (Exception ex) {
			logger.info("java schudule ex======" + ex.getMessage());
			flag = false;
		} catch (Throwable e) {			
			logger.info("java Throwable ex======" + e.getMessage());
			flag = false;
		} finally {
			resetFwImp();
			changValidation(session.GetID());
		}		

		return flag;
	}
	
	
	
	
	public boolean runUIReHot(int sessionID,long reschatcid,String station, LinkedList<Long> activityids, int contactid,LinkedList<Long> deleteactivityids) {	
		Schedule.initcount();
		Schedule.type = SchType.RESCH;	
		checkIsBranch(sessionID);	
		if(checkAIP(sessionID)){
            return false;
		}
		boolean flag = true;	
		
		ClSession ses = new ClSession(sessionID);		
		setExtentActivityTimeRangeUtils(sessionID);
		Schedule.old_extent_count=getExtentTotalCount();
		ClBranch bn = null;
		ses.SetTimeRange(getSessionTimeRage(ses));		
		ClSchedule sched = new ClSchedule(ses.GetTimeRange(), bn);
		ScScheduler myScheduler;
		ScSchedulerType.InitILOG();

		try {
			
			Collections.sort(activityids, new Comparator<Long>() {
		         @Override
		         public int compare(Long o1, Long o2) {
		        	 int a1 = o1.intValue();
		        	 int a2 =o2.intValue();
		        	 if(a1 > a2) {
		        		 return 1;
		        		 } else if(a1 == a2) {
		        		 return 0;
		        		 } else {
		        		 return -1;
		        		 }
		         }
		     });
			
			myScheduler = ScScheduler.Create(sched, false);
			ScScheduler.SetDebugging(debugging);
            
			if(contactid==-1){
				ClSatelliteVector sateVec = new ClSatelliteVector();
				ClSiteVector siteVec = new ClSiteVector();
				ClActivityVector nothingActVec = new ClActivityVector();
				ClActivityVector actVec = new ClActivityVector();
				GlIDVector nothingIDs = new GlIDVector();
				int total = 0;
				int failed = 0;
				
				ses.GetSatellites(sateVec);
				ses.GetSites(siteVec);				
				
				for(Long activityid: activityids ){
					ClActivity act = new ClActivity(activityid.intValue());
					actVec.add(act);					
				}				
				flag =myScheduler.HotScheduleAll(sateVec, siteVec,sched.GetTimeRange(), false, total, failed,	nothingActVec, nothingIDs, actVec);
				
			}else{
				if(activityids.size()>1){
					// only pbk resch 					
					ClContact con = new ClContact(contactid);
					ClSatelliteVector sateVec = new ClSatelliteVector();
					ClSiteVector siteVec = new ClSiteVector();
					ClActivityVector actVec = new ClActivityVector();
					ClActivityVector nothingActVec = new ClActivityVector();
					GlIDVector nothingIDs = new GlIDVector();
					int total = 0;
					int failed = 0;

					sateVec.add(con.GetSatellite());
					siteVec.add(con.GetSite());
					for(Long activityid: activityids ){
						ClActivity act = new ClActivity(activityid.intValue());
						if(reschatcid==activityid.longValue()){
							ClRSIImagingARLEntry ent = new ClRSIImagingARLEntry(act);
							FwRSIImagingARLEntryImp rsientry =rsiimagingFac.GetJavaID(ent.GetID());
							rsientry.SetStation(station);	
							myScheduler.AddActivityRestriction(act, con);
						}
						actVec.add(act);
					}					
					flag =myScheduler.HotScheduleAll(sateVec, siteVec,sched.GetTimeRange(), false, total, failed,	nothingActVec, nothingIDs, actVec);		
					
				}else{	
					
					logger.info("test by jeff one activity to contact");
                    //ClActivity act= new ClActivity(activityids.get(0).intValue());
                    //ClContact con = new ClContact(contactid);
                    //ClActivityVector nothingActVec = new ClActivityVector();
                    //GlIDVector nothingIDs = new GlIDVector();                       
                    //flag =myScheduler.HotSchedule(act, con,nothingActVec, nothingIDs,false );
					
					ArrayList<String> lists = new ArrayList<String>();	
					GlIDVector nothingIDs = new GlIDVector();
					ClActivityVector modifyActVec = new ClActivityVector();
					ClActivityVector actVec = new ClActivityVector();
					ClActivity act= new ClActivity(activityids.get(0).intValue());					
					ClContact con = new ClContact(contactid);
					myScheduler.AddActivityRestriction(act, con);
					actVec.add(act);
					lists.add("");					
					String[] failreason = lists.toArray(new String[lists.size()]);			
					flag =myScheduler.AddToContact(actVec,	con, modifyActVec, nothingIDs, failreason,false) ;					
				}			
				
			}
			
			if(deleteactivityids!=null && deleteactivityids.size()>0){
				logger.info("call delete activity size ="+deleteactivityids.size());
				for(Long deleteactivityid : deleteactivityids){
					int i =mActivityFac.DeleteSchActivity(Long.valueOf(deleteactivityid).intValue());
					if(i==1){
						mExtentFac.doSql("delete from extent where activityid =" + String.valueOf(deleteactivityid));
												
					}
				}				
			}		
			
			Schedule.new_extent_count=getExtentTotalCount();
			Schedule.resch = Schedule.new_extent_count-Schedule.old_extent_count;
			myScheduler.delete();
			myScheduler = null;

		} catch (Exception ex) {
			logger.info("runUIHot ex=" + ex.getMessage());
			flag = false;
		}finally{
			resetFwImp();
			changValidation(sessionID);
		}
		
		return flag;
	}	
	
	public boolean runUIReFilterHot(int sessionID,LinkedList<Long> sites, LinkedList<Long> activityids, LinkedList<Long> deleteactivityids) {
		Schedule.initcount();
		Schedule.type = SchType.RESCH_FILTER;	
		checkIsBranch(sessionID);	
		if(checkAIP(sessionID)){
            return false;
		}
		boolean flag = true;	
		
		ClSession ses = new ClSession(sessionID);		
		setExtentActivityTimeRangeUtils(sessionID);
		Schedule.old_extent_count=getExtentTotalCount();
		ClBranch bn = null; 
		ses.SetTimeRange(getSessionTimeRage(ses));	
		ClSchedule sched = new ClSchedule(ses.GetTimeRange(), bn);
		ScScheduler myScheduler;
		ScSchedulerType.InitILOG();

		try {
			
			Collections.sort(activityids, new Comparator<Long>() {
		         @Override
		         public int compare(Long o1, Long o2) {
		        	 int a1 = o1.intValue();
		        	 int a2 =o2.intValue();
		        	 if(a1 > a2) {
		        		 return 1;
		        		 } else if(a1 == a2) {
		        		 return 0;
		        		 } else {
		        		 return -1;
		        		 }
		         }
		     });
			
			myScheduler = ScScheduler.Create(sched, true);
			ClSatelliteVector sateVec = new ClSatelliteVector();
			ClSiteVector siteVec = new ClSiteVector();
			ClActivityVector nothingActVec = new ClActivityVector();
			ClActivityVector actVec = new ClActivityVector();
			GlIDVector nothingIDs = new GlIDVector();
			int total = 0;
			int failed = 0;
				
			ses.GetSatellites(sateVec);
			for(Long siteid: sites ){
				ClSite site = new ClSite(siteid.intValue());
				siteVec.add(site);
			}								
				
			for(Long activityid: activityids ){
				ClActivity act = new ClActivity(activityid.intValue());
				actVec.add(act);
			}	
				
			flag =myScheduler.HotScheduleAll(sateVec, siteVec,sched.GetTimeRange(), false, total, failed,	nothingActVec, nothingIDs, actVec);
				
			if(deleteactivityids!=null && deleteactivityids.size()>0){
				for(Long deleteactivityid : deleteactivityids){
					int i =mActivityFac.DeleteSchActivity(Long.valueOf(deleteactivityid).intValue());
					if(i==1){
						mExtentFac.doSql("delete from extent where activityid =" + String.valueOf(deleteactivityid));						
					}
				}				
			}			
			
			Schedule.new_extent_count=getExtentTotalCount();
			Schedule.resch = Schedule.new_extent_count-Schedule.old_extent_count;
			myScheduler.delete();
			myScheduler = null;

		} catch (Exception ex) {
			logger.info("runUIReFilterHot ex=" + ex.getMessage());
			flag = false;
		}finally{
			resetFwImp();
			changValidation(sessionID);
		}		
		return flag;
	}
	
	public boolean runUIReFilterHot(Date starttime,Date endtime,LinkedList<Long> sites, LinkedList<Long> activityids, LinkedList<Long> deleteactivityids) {		
		Schedule.initcount();
		Schedule.type = SchType.RESCH_FILTER;		
		Schedule.old_extent_count=getExtentTotalCount();
		boolean flag = true;
		
		Collection<Session> sessions =mSessionFac.getSessionByTimeRange(DateUtil.dateToString(starttime,"yyyy-MM-dd HH:mm:ss"), DateUtil.dateToString(endtime,"yyyy-MM-dd HH:mm:ss"));
		ClSession ses ;
		if(sessions.size()>0){			
			int sessionID = Long.valueOf(sessions.iterator().next().getId()).intValue();
			checkIsBranch(sessionID);
			logger.info("runUIReFilterHot db session id ="+String.valueOf(sessionID));
			ses = new ClSession(sessionID);			
			setExtentActivityTimeRangeUtils(ses.GetID());
		}else{
			return flag;
		}
		
		ClBranch bn = null;
 
		Date d1 = starttime;
		Date d2 = endtime;
		Calendar cal = Calendar.getInstance();
		cal.setTime(d2);
		cal.add(Calendar.MINUTE, 1644);		
		ClSchedule sched = new ClSchedule(DateUtil.getGlTimeRange(d1, d2), bn);
		ScScheduler myScheduler;
		ScSchedulerType.InitILOG();

		try {
			
			Collections.sort(activityids, new Comparator<Long>() {
		         @Override
		         public int compare(Long o1, Long o2) {
		        	 int a1 = o1.intValue();
		        	 int a2 =o2.intValue();
		        	 if(a1 > a2) {
		        		 return 1;
		        		 } else if(a1 == a2) {
		        		 return 0;
		        		 } else {
		        		 return -1;
		        		 }
		         }
		     });
			
			myScheduler = ScScheduler.Create(sched, true);
			ClSatelliteVector sateVec = new ClSatelliteVector();
			ClSiteVector siteVec = new ClSiteVector();
			ClActivityVector nothingActVec = new ClActivityVector();
			ClActivityVector actVec = new ClActivityVector();
			GlIDVector nothingIDs = new GlIDVector();
			int total = 0;
			int failed = 0;
				
			ses.GetSatellites(sateVec);
			for(Long siteid: sites ){
				ClSite site = new ClSite(siteid.intValue());
				siteVec.add(site);
			}								
				
			for(Long activityid: activityids ){
				int id = activityid.intValue();
				ClActivity act = new ClActivity(id);
				actVec.add(act);
			}	
				
			//flag =myScheduler.HotScheduleAll(sateVec, siteVec,sched.GetTimeRange(), false, total, failed,	nothingActVec, nothingIDs, actVec);
			flag =myScheduler.HotScheduleAll(sateVec, siteVec,sched.GetTimeRange(), true, total, failed,	nothingActVec, nothingIDs, actVec);
			
				
			if(deleteactivityids!=null && deleteactivityids.size()>0){
				for(Long deleteactivityid : deleteactivityids){
					int i =mActivityFac.DeleteSchActivity(Long.valueOf(deleteactivityid).intValue());
					if(i==1){
						mExtentFac.doSql("delete from extent where activityid =" + String.valueOf(deleteactivityid));						
					}
				}				
			}		
			
			Schedule.new_extent_count=getExtentTotalCount();
			Schedule.resch = Schedule.new_extent_count-Schedule.old_extent_count;
			myScheduler.delete();
			myScheduler = null;

		} catch (Exception ex) {
			logger.info("runUIReFilterHot ex=" + ex.getMessage());
			flag = false;
		}finally{
			resetFwImp();
			for(Session session :sessions){				
				changValidation( Long.valueOf(session.getId()).intValue());
			}
			
		}		
		return flag;
	}
	
	
	public boolean AddResourceChange(int sessionID,int activityid,Date starttime){
		Schedule.initcount();
		Schedule.type = SchType.MOVE;
		checkIsBranch(sessionID);		
		boolean flag = true;	
		
		ClSession ses = new ClSession(sessionID);		
		setExtentActivityTimeRangeUtils(sessionID);
		ClBranch bn = null;

		ses.SetTimeRange(getSessionTimeRage(ses));	
		ClSchedule sched = new ClSchedule(ses.GetTimeRange(), bn);
		ScScheduler myScheduler;
		ScSchedulerType.InitILOG();

		try {				
			
			myScheduler = ScScheduler.Create(sched, false);	
			
			ClActivity act = new ClActivity(Integer.valueOf(activityid).intValue());				
			flag= myScheduler.AddResourceChange(act, DateUtil.timestampToGlTIme(DateUtil.dateToTimestamp(starttime)));
			
			myScheduler.delete();
			myScheduler = null;

		} catch (Exception ex) {
			logger.info("AddResourceChange ex=" + ex.getMessage());
			flag = false;
		}finally{
			resetFwImp();
			changValidation(sessionID);
		}
		
		return flag;	
		
	}
	
	public boolean AdjustActivity(int sessionID,int contactid,LinkedList<Long> activityids,Date starttime){			
		Schedule.type = SchType.ADJUST;	
		checkIsBranch(sessionID);
		boolean flag = true;		
		ClSession ses = new ClSession(sessionID);		
		setExtentActivityTimeRangeUtils(sessionID);
		ClBranch bn = null;

		ses.SetTimeRange(getSessionTimeRage(ses));	
		ClSchedule sched = new ClSchedule(ses.GetTimeRange(), bn);
		ScScheduler myScheduler;
		ScSchedulerType.InitILOG();

		try {				
			ClActivityVector actVec = new ClActivityVector();
			ClContact con = new ClContact(contactid);			
			GlIDVector nothingIDs = new GlIDVector();
			ClActivityVector modifyActVec = new ClActivityVector();
			ArrayList<String> lists = new ArrayList<String>();			
			
			myScheduler = ScScheduler.Create(sched, false);			
			for(Long activityid: activityids ){
				ClActivity act = new ClActivity(activityid.intValue());
				myScheduler.AddMinStartActivityRestriction(act, DateUtil.timestampToGlTIme(DateUtil.dateToTimestamp(starttime)));
				actVec.add(act);
				lists.add("");
			}
			
			String[] failreason = lists.toArray(new String[lists.size()]);			
			flag =myScheduler.AddToContact(actVec,	con, modifyActVec, nothingIDs, failreason,false) ;
			
			myScheduler.delete();
			myScheduler = null;

		} catch (Exception ex) {
			logger.info("AdjustActivity ex=" + ex.getMessage());
			flag = false;
		}finally{
			resetFwImp();
			changValidation(sessionID);
		}
		
		return flag;
		
	}
	
	
	public void changValidation(int sessionID){
		mSessionFac.doSql("update sessions set needsvalidation=1  where id="+sessionID);		
	}
	
	

	public void GetSessionInfo_fake(ClSatelliteVector satvec,ClSiteVector sitevec) {

		if (satvec == null) {
			satvec = new ClSatelliteVector();
		}

		if (sitevec == null) {
			sitevec = new ClSiteVector();
		}

		try {
			ClSatellite sat = new ClSatellite("ROCSAT2");
			satvec.add(sat);

			ClSite site1 = new ClSite("CHUNGLI");
			sitevec.add(site1);			
		} catch (Throwable e) {
			logger.info(e.getMessage());
		}

	}

	public void GetSessionInfo(ClSatelliteVector satvec, ClSiteVector sitevec) {
		GlStringVector names = ClMaster.GetSessionNames();

		for (int i = 0; i < names.size(); i++) {

			ClSession ses = new ClSession(names.get(i));
			ClSatelliteVector vec1 = new ClSatelliteVector();
			ClSiteVector vec2 = new ClSiteVector();

			ses.GetSatellites(vec1);
			ses.GetSites(vec2);

			AddSats(satvec, vec1);
			AddSites(sitevec, vec2);

		}

		names.delete();
	}

	@SuppressWarnings("unused")
	private void showSatelliteAndSite(ClSatelliteVector satvec,	ClSiteVector sitevec) {

		for (int i = 0; i < satvec.size(); i++) {
			logger.info("Satallite:" + satvec.get(i).GetName());
		}

		for (int i = 0; i < sitevec.size(); i++) {
			logger.info("Site:" + sitevec.get(i).GetName());
		}

	}

	private void AddSats(ClSatelliteVector vec1, ClSatelliteVector vec2) {
		for (int i = 0; i < vec2.size(); i++) {
			boolean found = false;
			for (int j = 0; j < vec1.size(); j++) {
				if (vec2.get(i) == vec1.get(j)) {
					found = true;
					break;
				}
			}
			if (!found) {
				vec1.add(vec2.get(i));
			}
		}
	}

	private void AddSites(ClSiteVector vec1, ClSiteVector vec2) {
		for (int i = 0; i < vec2.size(); i++) {
			boolean found = false;
			for (int j = 0; j < vec1.size(); j++) {
				if (vec2.get(i) == vec1.get(j)) {
					found = true;
					break;
				}
			}
			if (!found) {
				vec1.add(vec2.get(i));
			}
		}
	}

	public boolean runUnColdSch(String sessionName) {
		return true;
	}

	

	public boolean runUIHot(int sessionID, int activityid, int contactid) {
		Schedule.type = SchType.RESCH;

		ClSession ses = new ClSession(sessionID);
		ClBranch bn = null;

		Date d1 = DateUtil.stringToDate(ses.GetStartTime().asString("%Y-%m-%d %H:%M:%S"),"yyyy-MM-dd HH:mm:ss");
		Date d2 = DateUtil.stringToDate(ses.GetStopTime().asString("%Y-%m-%d %H:%M:%S"),"yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		cal.setTime(d2);
		cal.add(Calendar.MINUTE, 1644);
		ses.SetTimeRange(DateUtil.getGlTimeRange(d1, cal.getTime()));
		ClSchedule sched = new ClSchedule(ses.GetTimeRange(), bn);
		return this.runUIHot(ses, sched, activityid, contactid);
	}

	public boolean runUIHot(ClSession session, ClSchedule sched,int activityid, int contactid) {
		ScScheduler myScheduler;
		ScSchedulerType.InitILOG();
		try {
			myScheduler = ScScheduler.Create(sched, true);

			ClActivity act = new ClActivity(activityid);
			ClContact con = new ClContact(contactid);
			runUISch(myScheduler, act, con);

		} catch (Exception ex) {
			logger.info("runUIHot ex=" + ex.getMessage());
		}
		return true;
	}

	public boolean runUISch(ScScheduler myScheduler, ClActivity activitie,
			ClContact contact) {

		boolean schedulerWorked = false;
		//String failReason = "";

		// ClActivityVector tmpActivityVector = new ClActivityVector() ; // for
		// ScScheduler

		logger.info("activities:" + activitie.GetName());

		//ClActivityVector actsToSched = new ClActivityVector();

		// AddActsForSched(tmpActivityVector, actsToSched, contact);

		// if(actsToSched.size() == 0)
		// return true;

		// UiGcScheduleItemVector unscheduledGCActs ;// UiGcSchedule
		//
		// // for each activity remaining in actsToSched, find its associated UI
		// version
		// // and add to UI vector
		// for(ClActivityVector::const_iterator act = actsToSched.begin();
		// act != actsToSched.end();
		// act++)
		// {
		// UiGcUnscheduledActivity* unscheduledActivity = GetUiGcVersion(*act);
		// unscheduledGCActs.push_back(unscheduledActivity);
		// }

		try {
			ClActivityVector modActs = new ClActivityVector();
			GlIDVector deletedExtentIDs = new GlIDVector();
			//String[] aa = { "", "" };
			// schedulerWorked = myScheduler.AddToContact(activitie, contact,
			// modActs, deletedExtentIDs, aa,true);
			schedulerWorked = myScheduler.HotSchedule(activitie, contact,modActs, deletedExtentIDs, true);
			

			if (!schedulerWorked) {
				logger.info("reui sch addtocontact ex");
				schedulerWorked = false;
			}
		} catch (Throwable ex) {
			ex.printStackTrace();
			logger.info("reui sch addtocontact ex=" + ex.getMessage());
			schedulerWorked = false;
		}

		// //=================

		if (schedulerWorked) {
			// track listing of activities schedule to report back to user
			ClActivityVector actsSched = new ClActivityVector();

			// if we had some activities successfully scheduled display message
			// which ones and
			// where
			if (actsSched.size() > 0) {
				// String msg;
				// msg << "Successfully scheduled (" << actsSched.size() <<
				// ") activity(s) \n"
				// << "on site: " << contact->GetSite().GetName()
				// << " contact window: " <<
				// GlUtil::AsJulianUTCTime(contact->GetTimeRange())
				// << endl;

			}

			// make the UiSchedule match the new (Cl) data

		} // end if schedulerWorked
		else {
			// if (schedulerWorked.TimeExpired())
			// failReason = " hot scheduling time expired.";
		}

		return schedulerWorked;

	}

}
