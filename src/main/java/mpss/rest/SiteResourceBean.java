package mpss.rest;
 
import java.util.List;

import mpss.common.jpa.*;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mpss.common.dao.*;

//@Stateless
//@LocalBean
@Component
@Path("/site")
public class SiteResourceBean implements SiteResource {
 
	  @Autowired
    SiteDao siteDao;
	
    @POST
    @Consumes(MediaType.APPLICATION_JSON) 
    public void create(Site site) {
    	siteDao.create(site);
    }
 
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("list")
    public List<Site> readAll() {
    	if (siteDao == null)
    	{
    		System.out.println("site dao is not available");
    		return null;
    	}
      return siteDao.getAll();
    }
 
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("{id}")
    public Site read(@PathParam("id") long id) {
    	if (siteDao == null)
    	{
    		System.out.println("site dao is not available");
    		return null;
    	}
      return siteDao.get(id);
    }
 
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("/byName/{name}")
    public Site getByName(@PathParam("name") String name) {
    	if (siteDao == null)
    	{
    		System.out.println("site dao is not available");
    		return null;
    	}
      return siteDao.getByName(name);
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON) 
    public void update(Site site) {
    	siteDao.update(site);
    }
 
    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") long id) {
        Site site = read(id);
        if(null != site) {
        	siteDao.delete(site);
        }
    } 
}
