package mpss.rest;
 
import java.util.List;
import javax.ws.rs.PathParam;
import mpss.common.jpa.PsOrbitEvents;
import mpss.common.jpa.Rev;
import mpss.util.files.FileInfo;
import mpss.util.jaxb.JaxbList;


//@Path("/orbit")
public interface OrbitResource {
 
    //@POST
    //@Consumes(MediaType.APPLICATION_JSON)
    public void create(Rev rev);
 
    //@PUT
    //@Consumes(MediaType.APPLICATION_JSON) 
    public void update(Rev rev);
 
    //@DELETE
    //@Path("{id}") 
    public void delete(@PathParam("id") long id);
 
    //@GET
    //@Produces(MediaType.APPLICATION_JSON) 
    //@Path("/rev/byNo/{scId}/{revNo}")
    public Rev getRevByPass(@PathParam("scId") int scId, @PathParam("revNo") int revNo);
    
    //@GET
    //@Produces(MediaType.APPLICATION_JSON) 
    //@Path("/oe/byPass/{scId}/{revNo}")
    public PsOrbitEvents getOrbitEventsByPass(@PathParam("scId") int scId, @PathParam("revNo") int revNo);
    
    public List<PsOrbitEvents> getOrbitEventsByTimeRange(@PathParam("scid") int scId, @PathParam("from") String from, @PathParam("to") String to);
    
    //public List<String> browseOrbitEventFiles(@PathParam("scid") int scId, @PathParam("from") String from, @PathParam("to") String to); 
    public List<FileInfo> browseOrbitEventFiles(); 
    //public GenericEntity<List<String>> browseOrbitEventFileNames();
    public JaxbList<String> browseOrbitEventFileNames();
 }
