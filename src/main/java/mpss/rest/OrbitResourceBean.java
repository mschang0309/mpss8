package mpss.rest;
 
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import mpss.common.dao.OrbitEventsDao;
import mpss.common.dao.RevDao;
import mpss.common.jpa.PsOrbitEvents;
import mpss.common.jpa.Rev;
import mpss.util.files.FileInfo;
import mpss.util.files.FileLister;
import mpss.util.jaxb.JaxbList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


//@Stateless
//@LocalBean
@Component
@Path("/orbit")
public class OrbitResourceBean implements OrbitResource {
 
	  @Autowired
    RevDao revDao;
	
	  @Autowired
    OrbitEventsDao oeDao;
	  
	@Context 
	private ServletContext servletContext;
    
    //@Context
    //public void setServletContext(ServletContext context) {
    //    System.out.println("servlet context set here");
    //    this.servletContext = context;
    //}
	
    @POST
    @Consumes(MediaType.APPLICATION_JSON) 
    public void create(Rev rev) {
    	revDao.create(rev);
    }
 
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("list")
    public List<Rev> readAll() {
    	if (revDao == null)
    	{
    		System.out.println("rev dao is not available");
    		return null;
    	}
      return revDao.getAll();
    }
 
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("{id}")
    public Rev read(@PathParam("id") long id) {
    	if (revDao == null)
    	{
    		System.out.println("rev dao is not available");
    		return null;
    	}
      return revDao.get(id);
    }
 
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("/byPass/{scid}/{revno}")
    public Rev getRevByPass(@PathParam("scid") int scId, @PathParam("revno") int revNo) {
    	if (oeDao == null)
    	{
    		System.out.println("orbitevents dao is not available");
    		return null;
    	}
      return oeDao.getRevByPass(scId, revNo);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("/oe/byPass/{scid}/{revno}")
    public PsOrbitEvents getOrbitEventsByPass(@PathParam("scid") int scId, @PathParam("revno") int revNo) {
    	if (oeDao == null)
    	{
    		System.out.println("orbitevents dao is not available");
    		return null;
    	}
      return oeDao.getOrbitEventsByPass(scId, revNo);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("/oe/byTime/{scid}/{from}/{to}")
    public List<PsOrbitEvents> getOrbitEventsByTimeRange(@PathParam("scid") int scId, @PathParam("from") String from, @PathParam("to") String to) {
    	if (oeDao == null)
    	{
    		System.out.println("orbitevents dao is not available");
    		return null;
    	}
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Date starttime = new Date();
        Date endtime = new Date();
        try
        {
            starttime = formatter.parse(from);
            endtime = formatter.parse(to);	 
        }
        catch (Exception ex) {
        	System.out.println("getOrbitEventsByTimeRange exception: "+ ex.getMessage());
        }
    	
      return oeDao.getOrbitEventsByTimeRange(scId, starttime, endtime);
    }
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    //@Path("/oe/browse/{scid}/{from}/{to}")
    //public List<String> browseOrbitEventFiles(@PathParam("scid") int scId, @PathParam("from") String from, @PathParam("to") String to) {
    @Path("/file/browse")
    public List<FileInfo> browseOrbitEventFiles() {
    	//if (!from.isEmpty() && !to.isEmpty())
    	//{
        //  SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        //  Date starttime = new Date();
        //  Date endtime = new Date();
        //  try
        //  {
        //    starttime = formatter.parse(from);
        //    endtime = formatter.parse(to);	 
        //  }
        //  catch (Exception ex) {
        //	System.out.println("browseOrbitEventFiles exception: "+ ex.getMessage());
        //  }
    	//}
    	
    	if (servletContext == null)
    	{
        	System.out.println("Servlet context not injected");
        	//return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        	return null;
    		
    	}
        String oeFileLocation = servletContext.getInitParameter("oeFileLocation");
        if (oeFileLocation == null|| oeFileLocation.isEmpty())
        {
        	System.out.println("Orbit event file location not defined ");
        	//return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        	return null;
       	
        }
    	System.out.printf("Orbit event file location : %s\n", oeFileLocation);
    	
        FileLister fileLister = new FileLister(); 

        List<FileInfo> fileInfo = fileLister.listFiles(oeFileLocation);
        
        for (FileInfo fi : fileInfo)
        {
        	System.out.printf("Orbit event file name : %s\n", fi.getName());
        }
        

       return fileInfo;
        
        //return Response.ok(entity).build();
        //return Response.ok(names.toArray(new String[names.size()])).build();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    //@Path("/oe/browse/{scid}/{from}/{to}")
    //public List<String> browseOrbitEventFiles(@PathParam("scid") int scId, @PathParam("from") String from, @PathParam("to") String to) {
    @Path("/file/namelist")
    //public GenericEntity<List<String>> browseOrbitEventFileNames() {
    public JaxbList<String> browseOrbitEventFileNames() {
    	if (servletContext == null)
    	{
        	System.out.println("Servlet context not injected");
        	//return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        	return null;
    		
    	}
        String oeFileLocation = servletContext.getInitParameter("oeFileLocation");
        if (oeFileLocation == null|| oeFileLocation.isEmpty())
        {
        	System.out.println("Orbit event file location not defined ");
        	//return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        	return null;
       	
        }
    	System.out.printf("Orbit event file location : %s\n", oeFileLocation);
    	
        FileLister fileLister = new FileLister(); 

        List<String> names = fileLister.listFileNames(oeFileLocation);
        JaxbList<String> res = new JaxbList<String>(names);
        
        return res;
    
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON) 
    public void update(Rev rev) {
    	revDao.update(rev);
    }
 
    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") long id) {
        Rev rev = read(id);
        if(null != rev) {
        	revDao.delete(rev);
        }
    } 
}
