package mpss.rest;
 
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import mpss.common.jpa.PsMissionTimeLine;


//@Path("/mtl")
public interface MtlResource {
  
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("listFileNames")
    public List<String> listExternalFileNames();
    
    //@GET
    //@Produces(MediaType.APPLICATION_JSON) 
    //@Path("byName/{arlName}")
	public PsMissionTimeLine getMissionTimeLines(@PathParam("arlName") String arlName);
    
 }
