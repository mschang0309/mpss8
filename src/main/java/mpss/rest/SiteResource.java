package mpss.rest;
 
import mpss.common.jpa.*; 
import javax.ws.rs.PathParam;


//@Path("/site")
public interface SiteResource {
 
    //@POST
    //@Consumes(MediaType.APPLICATION_JSON)
    public void create(Site site);
 
    //@GET
    //@Produces(MediaType.APPLICATION_JSON) 
    //@Path("{id}")
    public Site read(@PathParam("id") long id);
 
    //@GET
    //@Produces(MediaType.APPLICATION_JSON) 
    //@Path("/byName/{name}")
    public Site getByName(@PathParam("name") String name);
    
    //@PUT
    //@Consumes(MediaType.APPLICATION_JSON) 
    public void update(Site site);
 
    //@DELETE
    //@Path("{id}") 
    public void delete(@PathParam("id") long id);
 
}
