package mpss.rest;
 

import java.util.Set;
import mpss.common.jpa.*; 
import javax.ws.rs.PathParam;


//@Path("/satellite")
public interface SatelliteResource {
 
    //@POST
    //@Consumes(MediaType.APPLICATION_JSON)
    public void create(Satellite satellite);
 
    //@GET
    //@Produces(MediaType.APPLICATION_JSON) 
    //@Path("{id}")
    public Satellite read(@PathParam("id") long id);
 
    //@GET
    //@Produces(MediaType.APPLICATION_JSON) 
    //@Path("/byName/{name}")
    public Satellite getByName(@PathParam("name") String name);
    
    public Set<Recorder> getRecorders(@PathParam("id") long id);
    
    //@PUT
    //@Consumes(MediaType.APPLICATION_JSON) 
    public void update(Satellite satellite);
 
    //@DELETE
    //@Path("{id}") 
    public void delete(@PathParam("id") long id);
 
}
