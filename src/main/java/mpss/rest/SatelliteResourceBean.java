package mpss.rest;
 
import java.util.List;
import java.util.Set;
import mpss.common.jpa.*;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mpss.common.dao.*;

//@Stateless
//@LocalBean
@Component
@Path("/satellite")
public class SatelliteResourceBean implements SatelliteResource {
 
	  @Autowired
    SatelliteDao satelliteDao;
	
    @POST
    @Consumes(MediaType.APPLICATION_JSON) 
    public void create(Satellite satellite) {
    	satelliteDao.create(satellite);
    }
 
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("list")
    public List<Satellite> readAll() {
    	if (satelliteDao == null)
    	{
    		System.out.println("satellite dao is not available");
    		return null;
    	}
      return satelliteDao.getAll();
    }
 
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("{id}")
    public Satellite read(@PathParam("id") long id) {
    	if (satelliteDao == null)
    	{
    		System.out.println("satellite dao is not available");
    		return null;
    	}
      return satelliteDao.get(id);
    }
 
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("/byName/{name}")
    public Satellite getByName(@PathParam("name") String name) {
    	if (satelliteDao == null)
    	{
    		System.out.println("satellite dao is not available");
    		return null;
    	}
      return satelliteDao.getByName(name);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("{id}/recorders")
    public Set<Recorder> getRecorders(@PathParam("id") long id) {
    	if (satelliteDao == null)
    	{
    		System.out.println("satellite dao is not available");
    		return null;
    	}
      return satelliteDao.getRecorders(id);
    }
 
    @PUT
    @Consumes(MediaType.APPLICATION_JSON) 
    public void update(Satellite satellite) {
    	satelliteDao.update(satellite);
    }
 
    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") long id) {
        Satellite satellite = read(id);
        if(null != satellite) {
        	satelliteDao.delete(satellite);
        }
    } 
}
