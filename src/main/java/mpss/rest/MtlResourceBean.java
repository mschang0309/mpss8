package mpss.rest;
 
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import mpss.common.dao.ActivityRequestListDao;
import mpss.common.jpa.PsMissionTimeLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//@Stateless
//@LocalBean
@Component
@Path("/mtl")
public class MtlResourceBean implements MtlResource {
 
	  @Autowired
    ActivityRequestListDao arlDao;
	
	@Context 
	private ServletContext servletContext;
    
    //@Context
    //public void setServletContext(ServletContext context) {
    //    System.out.println("servlet context set here");
    //    this.servletContext = context;
    //}
	
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("byName/{arlName}")
    public PsMissionTimeLine getMissionTimeLines(@PathParam("arlName") String arlName) {
    	if (arlDao == null)
    	{
    		System.out.println("ActivityRequestList dao is not available");
    		return null;
    	}
      return arlDao.getMissionTimeLines(arlName);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON) 
    @Path("listFileNames")
    public List<String> listExternalFileNames()
    {
    	String[] names = (String[])arlDao.listExternalFileNames().toArray();
    	return Arrays.asList(names);
    }
 
}
