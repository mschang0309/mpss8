package mpss.ac;

import java.sql.Timestamp;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mpss.common.jpa.*;
import mpss.common.dao.*;
import mpss.util.timeformat.*;

@Service
public class RtcmdActivityCreator extends ActivityCreatorBase
{
	@Autowired
	ActivityRequestListDao activityRequestListDao;
	
	 
	public RtcmdActivityCreator()
	{
	}
	
	public RtcmdActivityCreator(Satellite satellite, Session session)
	{
		super(satellite, session);
	}
	
	public List<Activity> create(String arlName)
	{
		if (activityRequestListDao.getArl(arlName) == null)
		{
			// arl does not exist
		    return null;
		}
		
		List<Rtcmdprocrequest> l = activityRequestListDao.listRtcmdprocrequests(arlName);
		if (l.isEmpty())
		{
			return null;
		}
		List<Activity> res = new ArrayList<Activity>();
		for (Rtcmdprocrequest r : l)
		{
			res.add(createFrom(arlName,r));
		}
		return res;
	}

	private Activity createFrom(String arlName, Rtcmdprocrequest arlEntry)
	{
	    // validate start time
		Timestamp starttime = YearDayTimeOffset.toTimestamp(arlEntry.getYear(), arlEntry.getDay(), arlEntry.getStarttime());
		if (starttime.before(this.session.getStarttime()) || starttime.after(this.session.getEndtime()))
		{
			return null;
		}
		Timestamp endtime = new Timestamp(starttime.getTime() + arlEntry.getDuration()*1000);
		return createActivity(arlName, arlEntry.getId().intValue(), "RTCMD", "NO", starttime, endtime, arlEntry.getDuration());
	}
	
	
}