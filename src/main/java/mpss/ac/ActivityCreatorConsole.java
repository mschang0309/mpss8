package mpss.ac;

//import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.stereotype.Component;

import mpss.common.dao.*;
import mpss.common.jpa.*;


@Component
public class ActivityCreatorConsole 
{
    @Autowired
    protected SatelliteDao satelliteDao;
    
    @Autowired
    protected SessionDao sessionDao;
    
    @Autowired
    protected MonitoringActivityCreator monitoringActivityCreator;
    
    @Autowired
    protected ManeuverActivityCreator maneuverActivityCreator;
    
    @Autowired
    protected RsiImagingActivityCreator rsiImagingActivityCreator;
    
    Satellite satellite;
    Session session;
    
	public static void main(String args[])
	{
	      try {
		      // Spring application context
	    	  ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf"}));
	        //if (context == null) {
	        //	System.out.println("load application context failed");
	        //	return;
	        //}
	    	  ActivityCreatorConsole console = (ActivityCreatorConsole)context.getBean(ActivityCreatorConsole.class);	  
	        if (console == null) {
	        	System.out.println("load ActivityCreatorConsole bean failed");
	        	return;
	        }
		    System.out.println("Activity Creator Console started");
		    
		    // initialize console
		    console.initialize();
		    
		    // create activities
		    console.create();
		    
		    
	       } catch (Exception ex) {
	    		ex.printStackTrace();
	    		return;
	      }
		  System.out.println("Activity Creator Console done");
	      
	      
	}  
	
	void initialize()
	{
	    // load properties
    	Properties prop = new Properties();
 
    	try {
    		
    		//load a properties file from class path, inside static method
    		prop.load(ActivityCreatorConsole.class.getClassLoader().getResourceAsStream("ActivityCreatorConsole.properties"));
 
        //get the satellite by name
    		String szScName= prop.getProperty("scName");
    		if (szScName == null)
    		{
    			System.out.println("scid cannot be found in property file");
    			return;
    		}
        satellite = satelliteDao.getByName(szScName);
        
        //get the session by name
    		String sessionName = prop.getProperty("session");
    		if (sessionName == null)
    		{
    			System.out.println("session name cannot be found in property file");
    			return;
    		}
        session = sessionDao.getSession(sessionName); 
        
    	} catch (IOException ex) {
    		ex.printStackTrace();
      }

	}
	
	void create()
	{
		  // create monitoring activities (by contacts)
		  System.out.println("reate monitoring activities");
		  monitoringActivityCreator.setSatellite(satellite);
		  monitoringActivityCreator.setSession(session);
		  List<Activity> acts = monitoringActivityCreator.create("");
		  //for (Activity a : acts)
		  //{
		  //System.out.printf("act name: %s\n", a.getName());
		  System.out.printf("monitoring activity created: %d\n", acts.size());
		  //}
      
  		// create maneuver and RSI activities (by ARL occurred within session)
		  System.out.println("create maneuver activities");
		  maneuverActivityCreator.setSatellite(satellite);
		  maneuverActivityCreator.setSession(session);
		  acts = maneuverActivityCreator.create("RS2_RSITimeline_20090602_A1");
		  //for (Activity a : acts)
		  //{
			//  System.out.printf("act name: %s\n", a.getName());
		  //}
		  System.out.printf("maneuver activity created (A1): %d\n", acts.size());

		  acts = maneuverActivityCreator.create("RS2_RSITimeline_20090602_A2");
		  //for (Activity a : acts)
		  //{
			//  System.out.printf("act name: %s\n", a.getName());
		  //}
		  System.out.printf("maneuver activity created (A2): %d\n", acts.size());
		  acts = maneuverActivityCreator.create("RS2_RSITimeline_20090602_B");
		  //for (Activity a : acts)
		  //{
			//  System.out.printf("act name: %s\n", a.getName());
		  //}
		  System.out.printf("maneuver activity created (B): %d\n", acts.size());
  		
  		
		  System.out.println("create RSI activities");
		  rsiImagingActivityCreator.setSatellite(satellite);
		  rsiImagingActivityCreator.setSession(session);
		  acts = rsiImagingActivityCreator.create("RS2_RSITimeline_20090602_A1");
		  //for (Activity a : acts)
		  //{
		//	  System.out.printf("act name: %s\n", a.getName());
		  //}
		  System.out.printf("rsiImaging activity created (A1): %d\n", acts.size());
		  acts = rsiImagingActivityCreator.create("RS2_RSITimeline_20090602_A2");
		  //for (Activity a : acts)
		  //{
		//	  System.out.printf("act name: %s\n", a.getName());
		  //}
		  System.out.printf("rsiImaging activity created (A2): %d\n", acts.size());
		  acts = rsiImagingActivityCreator.create("RS2_RSITimeline_20090602_B");
		  //for (Activity a : acts)
		  //{
			//  System.out.printf("act name: %s\n", a.getName());
		  //}
		  System.out.printf("rsiImaging activity created (B): %d\n", acts.size());
		  
      System.out.println("Activity Creator complete");
	    
  		
	}
}
