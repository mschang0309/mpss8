package mpss.ac;

import java.util.*;
import org.springframework.stereotype.Service;
import mpss.common.jpa.*;

@Service
public class PlaybackActivityCreatorPanPlusMs extends PlaybackActivityCreatorBase implements IPlaybackActivityCreator
{
	 
	 public PlaybackActivityCreatorPanPlusMs()
	 {
	 }
	 
	public PlaybackActivityCreatorPanPlusMs(Satellite satellite, Session session,  Rsiimagingrequest arlEntry, Recorderp recorderPS)
	{
		super(satellite, session, arlEntry, recorderPS);
	}
	
	public List<Activity> create(String arlName)
	{
        List<Activity> res = new ArrayList<Activity>();
		    // PAN activity
        int panDuration = getPlaybackDurationPan();            
        Activity act = createActivity("RSI_PBK:PAN", arlEntry.getId().intValue(), "RSI", "PBK",  this.startTime, this.endTime, panDuration);
        if (act != null)
        {
            res.add(act);
        }
        else 
        {
        	logger.info("PlaybackActivityCreatorPanPlusMs : fail to create PAN");        	
        }
		    // MS activity
        int msDuration = getPlaybackDurationMs();            
        act = createActivity("RSI_PBK:MS", arlEntry.getId().intValue(), "RSI", "PBK",  this.startTime, this.endTime, msDuration);
        if (act != null)
        {
            res.add(act);       	
        }
        else 
        {
        	logger.info("PlaybackActivityCreatorPanPlusMs : fail to create MS");        	
        }
               
        return res;
	}	
}