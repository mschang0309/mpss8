package mpss.ac;

import java.sql.Timestamp;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mpss.common.jpa.*;
import mpss.common.dao.*;
import mpss.resources.*;

@Service
public class IsualImagingActivityCreator extends ActivityCreatorBase
{
	 @Autowired
	 ActivityRequestListDao activityRequestListDao;
	 
	 @Autowired 
	 RecorderDao recorderDao;
	 
	 @Autowired 
	 AvailableContacts availableContacts;
	 
	 private Recorder recorder;
	 protected Recorderp recorderPS;
	 
	 public Satellite getSatellite()
	 {
			 return this.satellite;
	 }
	 public void setSatellite(Satellite satellite)
	 {
		 this.satellite = satellite;
	 }
	 
	 public Session getSession()
	 {
			 return this.session;
	 }
	 
	 public void setSession(Session session)
	 {
		 this.session = session;
	 }

	 public Recorderp getRecorderp()
	 {
			 return this.recorderPS;
	 }
	 public void setRecorderp(Recorderp recorderPS)
	 {
		 this.recorderPS = recorderPS;
	 }

	public IsualImagingActivityCreator()
	{
	}
	
	public IsualImagingActivityCreator(Satellite satellite, Session session)
	{
		super(satellite, session);
	}
	
	public List<Activity> create(String arlName)
	{
		this.recorder = recorderDao.listRecorders(satellite.getId().intValue()).get(0);
		if (activityRequestListDao.getArl(arlName) == null)
		{
			// arl does not exist
		    return null;
		}
		
		List<Isualimagingrequest> l = activityRequestListDao.listIsualimagingrequests(arlName);
		if (l.isEmpty())
		{
			return null;
		}
		List<Activity> res = new ArrayList<Activity>();
		for (Isualimagingrequest r : l)
		{
			res.add(createFrom(arlName, r));
		}
		return res;
	}

	private Activity createFrom(String arlName, Isualimagingrequest arlEntry)
	{
		// calculate ISUAL duration
        RecorderResources rr = new RecorderResources(this.recorderPS);      
        int duration = rr.calculateISUALDuration(this.recorder);   

        @SuppressWarnings("unused")
		List<Contact> contacts = availableContacts.getAvailableContactsBySiteType("SBAND");
		// if remote tracking station should be excluded
		if (arlEntry.getRemotetrackingflag() == 0)
		{
			// filtered out sites which are flagged as RTS
		}
		Timestamp starttime = new Timestamp(0);
		Timestamp endtime = new Timestamp(0);
		
		// trigger: 0 - first contact, 1 - last contact
		@SuppressWarnings("unused")
		int trigger = arlEntry.getContactoccurrence();
		return createActivity(arlName, arlEntry.getId().intValue(), "ISUAL", "NO", starttime, endtime,  duration);
	}
	
	
}