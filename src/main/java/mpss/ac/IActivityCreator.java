package mpss.ac;

import java.util.*;

import mpss.common.jpa.*;

public interface IActivityCreator 
{
	public Satellite getSatellite();
	public void setSatellite(Satellite satellite);
	public Session getSession();
	public void setSession(Session session);	
	public List<Activity> create(String arlName);
}