package mpss.ac;


import java.util.*;
import org.springframework.stereotype.Service;
import mpss.common.jpa.*;


@Service
public class PlaybackActivityCreatorMs extends PlaybackActivityCreatorBase  implements IPlaybackActivityCreator 
{
	 
	 public PlaybackActivityCreatorMs()
	 {
	 }
	
	public PlaybackActivityCreatorMs(Satellite satellite, Session session,  Rsiimagingrequest arlEntry, Recorderp recorderPS)
	{
		super(satellite, session, arlEntry, recorderPS);
	}
	
	public List<Activity> create(String arlName)
	{
        List<Activity> res = new ArrayList<Activity>();       
        int pbkDuration = getPlaybackDurationMs();           
        Activity act = createActivity("RSI_PBK:MS", arlEntry.getId().intValue(), "RSI", "PBK",  this.startTime, this.endTime, pbkDuration);
        res.add(act);
        return res;
	}	
}