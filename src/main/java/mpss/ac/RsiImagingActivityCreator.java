package mpss.ac;

import java.sql.Timestamp;
import java.util.*;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import mpss.common.jpa.*;
import mpss.common.dao.*;
import mpss.util.timeformat.YearDayTimeOffset;

//for Spring component scanning
@Service
public class RsiImagingActivityCreator extends ActivityCreatorBase
{
	// for Spring injection
	@Autowired
	private ApplicationContext appContext;
	
	@Autowired
	ActivityRequestListDao activityRequestListDao;
	
	@Autowired
	PsDao psDao;
	
	@Autowired
	ParametersetDao parametersetDao;
	
	@Autowired
	ActivityDao activityDao;
	
	//Timestamp maxDateTime =  java.sql.Timestamp.valueOf("294276-12-31 23:59:59");

	public RsiImagingActivityCreator() {	}

	public RsiImagingActivityCreator(Satellite satellite, Session session)
	{
		super(satellite, session);
	}
	
	public List<Activity> create(String arlName)
	{
		// get current parameter set name
		Parameterset ps = parametersetDao.get(session.getPsid().longValue());
		List<Recorderp> recs = psDao.listRecorderps(ps.getName());
		
		// get recorder parameter set
		Recorderp recorderPS = recs.get(0);
		
		if (activityRequestListDao.getArl(arlName) == null)
		{
			// arl does not exist
			logger.info("RsiImagingActivityCreator: ARL not found (" + arlName +")");   
		    return null;
		}
		
		List<Rsiimagingrequest> l = activityRequestListDao.listRsiimagingrequests(arlName);
		if (l.isEmpty())
		{
			logger.info("RsiImagingActivityCreator: no Rsi imaging requests"); 
			return null;
		}
		List<Activity> res = new ArrayList<Activity>();
		for (Rsiimagingrequest r : l)
		{
			String ssrStatus = r.getSsrstatus().toUpperCase();
			if (ssrStatus.compareTo("DDT") == 0){
				Activity act = createDDT(arlName,r);
				if (act != null)
				{
					res.add(act);
				}
			}
			else if (ssrStatus.compareTo("REC") == 0) {
				List<Activity> acts = createImaging(arlName,r, recorderPS);
				if (acts != null)
				{
					res.addAll(acts);						
				}
			}
			else {
				// unknown type
				logger.info("RsiImagingActivityCreator: unknown SSR status (" + ssrStatus +")");
			}
		}
		return res;
	}

	private Activity createDDT(String arlName, Rsiimagingrequest arlEntry)
	{
		System.out.println("RsiImagingActivityCreator: create DDT");
		Timestamp starttime = YearDayTimeOffset.toTimestamp(arlEntry.getYear(), arlEntry.getDay(), arlEntry.getStarttime());
		if (starttime.before(this.session.getStarttime()) || starttime.after(this.session.getEndtime()))
		{
			logger.info("RsiImagingActivityCreator: no ARL entry within session time range");
			return null;
		}
		Timestamp endtime = new Timestamp(starttime.getTime() + arlEntry.getDuration()*1000);
		return createActivity(arlName, arlEntry.getId().intValue(), "RSI", "DDT", starttime, endtime, arlEntry.getDuration());
	}
	
	
	private List<Activity> createImaging(String arlName, Rsiimagingrequest arlEntry, Recorderp recorderPS)
	{
		System.out.println("RsiImagingActivityCreator: create IMG");
		// assign start time and end time
		Timestamp starttime = YearDayTimeOffset.toTimestamp(arlEntry.getYear(), arlEntry.getDay(), arlEntry.getStarttime());
		if (starttime.before(this.session.getStarttime()) || starttime.after(this.session.getEndtime()))
		{
			logger.info("RsiImagingActivityCreator: no ARL entry within session time range");
			return null;
		}
		Timestamp endtime = new Timestamp(starttime.getTime() + arlEntry.getDuration()*1000);
		int duration = 0;
		
		// result of related activities
		List<Activity> res = new ArrayList<Activity>();
		
		// create REC activity 
		logger.info("RsiImagingActivityCreator: create REC activity");
		duration = arlEntry.getDuration();
		Activity rec =  createActivity(arlName, arlEntry.getId().intValue(), "RSI", "REC", starttime, endtime, duration);
		res.add(rec);
		
		// create PBK activity
		// MS or PAN : single activity
		// MS+PAN : PAN then MS activities
		logger.info("RsiImagingActivityCreator: create PBK activity");
		List<Activity> pbks = createPlayback(arlName, arlEntry, rec, recorderPS);
		res.addAll(pbks);
	
		// create DEL activity
		// duration = 1 second
		// start time = end of REC activity plus one second (1000ms)
		// end time = max Date in java
		logger.info("RsiImagingActivityCreator: create DEL activity");
		starttime = new Timestamp(rec.getEndtime().getTime() + 1000);
		//endtime = new Timestamp(Integer.MAX_VALUE);
		endtime = java.sql.Timestamp.valueOf("9999-12-31 23:59:59");
		duration = 1;
		Activity del =  createActivity(arlName, arlEntry.getId().intValue(), "DELETE", "NO", starttime, endtime, duration);
		res.add(del);
		
		// link record->playback->delete activity sequence
		logger.info("RsiImagingActivityCreator: link REC/PBK/DEL activities");
		linkActivities(rec, pbks, del);
		
		return res;
	}
	
	private List<Activity> createPlayback(String arlName, Rsiimagingrequest arlEntry, Activity recAct, Recorderp recorderPS)
	{
    // it was decided on Mar 17,2005 with Shin-Fa and Chia that the playback
    // windows start from the end of the record for 24 hours
		Timestamp pbkStarttime = recAct.getEndtime();
		Timestamp pbkEndtime = new Timestamp(pbkStarttime.getTime() + 24*60*60*1000);
		
		// imaging mode 1-MS, 2-PAN, 3-MS+PAN
		List<Activity> res = null;
		IPlaybackActivityCreator c = null;
		switch (arlEntry.getImagingmode())
		{
		case 1:
			//c = new PlaybackActivityCreatorMs(satellite, session, arlEntry, recorderPS);
			c= appContext.getBean(PlaybackActivityCreatorMs.class);
			break;
		case 2:
			//c = new PlaybackActivityCreatorPan(satellite, session, arlEntry, recorderPS);
			c= appContext.getBean(PlaybackActivityCreatorPan.class);
			break;
		case 3:
			//if the REC is performing PAN+MS generate 2 playback activities
			// one for PAN, one for MS
			//c = new PlaybackActivityCreatorPanPlusMs(satellite, session, arlEntry, recorderPS);
			c= appContext.getBean(PlaybackActivityCreatorPanPlusMs.class);
			break;
		}
		c.setSatellite(satellite);
		c.setSession(session);
		c.setRsiimagingrequest(arlEntry);
		c.setRecorderp(recorderPS);
		c.setStartTime(pbkStarttime);
		c.setEndTime(pbkEndtime);
		res = c.create("");
		
		return res;
	}
	
	// link record->playback->delete activity sequence
	private void linkActivities(Activity rec, List<Activity> pbks, Activity del)
	{
		if (pbks.size() < 1)
		{
			return;
		}
		// link rec to first pbk
		rec.setNextactid(pbks.get(0).getId().intValue());
		pbks.get(0).setPrevactid(rec.getId().intValue());
		
		// link pbks
		if (pbks.size() >= 2)
		{
		  for (int i = 0; i < pbks.size()-1; i++)
		  {		
				pbks.get(i).setNextactid(pbks.get(i+1).getId().intValue());
				pbks.get(i+1).setPrevactid(pbks.get(i).getId().intValue());
		  }
		}
		
		// link last pbk to del
		Activity lastPbk = pbks.get(pbks.size()-1);
		lastPbk.setNextactid(del.getId().intValue());
		del.setPrevactid(lastPbk.getId().intValue());
		
		// persist
		activityDao.update(rec);
		for (int i = 0; i < pbks.size(); i++)
		{		
			activityDao.update(pbks.get(i));
		}
		activityDao.update(del);
		
	}
}