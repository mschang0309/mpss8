package mpss.ac;

import java.sql.Timestamp;
import org.springframework.stereotype.Service;
import mpss.common.jpa.*;
import mpss.resources.RecorderResources;
import mpss.util.timeformat.YearDayTimeOffset;

@Service
//public abstract class PlaybackActivityCreatorBase implements IPlaybackActivityCreator
public abstract class PlaybackActivityCreatorBase extends ActivityCreatorBase
{
	protected Satellite satellite;
	protected Session session;
	protected Rsiimagingrequest arlEntry;
	protected Recorderp recorderPS;
	protected String arlMode;
	protected Timestamp startTime;
	protected Timestamp endTime;
	
	 public Rsiimagingrequest getRsiimagingrequest()
	 {
			 return this.arlEntry;
	 }
	 public void setRsiimagingrequest(Rsiimagingrequest arlEntry)
	 {
		 this.arlEntry = arlEntry;
		 switch (arlEntry.getImagingmode())
		 {
			case 1:
			    this.arlMode = "MS";
			    break;
			case 2:
			    this.arlMode = "PAN";
			    break;
			case 3:
			    this.arlMode = "PAN+MS";
			    break;
		 }
		 // playback time window 
		 this.startTime = YearDayTimeOffset.toTimestamp(arlEntry.getYear(), arlEntry.getDay(), arlEntry.getStarttime());
	 }
	 
	 public Recorderp getRecorderp()
	 {
			 return this.recorderPS;
	 }
	 public void setRecorderp(Recorderp recorderPS)
	 {
		 this.recorderPS = recorderPS;
	 }
	 
	public void setStartTime(Timestamp time)
	{
		this.startTime = time;
	}
	
	public void setEndTime(Timestamp time)
	{
		this.endTime = time;
		
	}
	 
	public PlaybackActivityCreatorBase() {
	}

	public PlaybackActivityCreatorBase(Satellite satellite, Session session, Rsiimagingrequest arlEntry, Recorderp recorderPS)
	{
		this.satellite = satellite;
		this.session = session;
		this.arlEntry = arlEntry;
		this.recorderPS = recorderPS;
		
		switch (arlEntry.getImagingmode())
		{
			case 1:
		    this.arlMode = "MS";
		    break;
			case 2:
		    this.arlMode = "PAN";
		    break;
			case 3:
		    this.arlMode = "PAN+MS";
		    break;
		}
	}
	
	//public abstract List<Activity> create();

  protected int getPlaybackDurationMs()
  {  
        
        // look at arlEntry and determine which compression ratio to use
        // if selected DEFAULT/LOW then use normal MS data rate.
        // If selected HIGH then use MS ratio from PAN+MS rate
        String compRatio = arlEntry.getMscompressionratio();
        double msRatio = (compRatio == "LOW" || compRatio == "DEFAULT" && this.arlMode != "PAN+MS") ?  
                          recorderPS.getMsimagingmode() : recorderPS.getMsplusimagingmode();
        RecorderResources rr = new RecorderResources(this.recorderPS);      
        int sectors = rr.calculateRecorderDelta(arlEntry.getDuration(), msRatio);   
        return rr.calculatePlaybackDuration(sectors);            
  }
  
  protected int getPlaybackDurationPan()
  {  
        // look at arlEntry and determine which compression ratio to use
        // if selected DEFAULT/LOW then use normal PAN data rate.
        // If selected HIGH then use PAN ratio from PAN+MS rate
        String compRatio = arlEntry.getPancompressionratio();
        double panRatio = (compRatio == "LOW" || compRatio == "DEFAULT" && this.arlMode != "PAN+MS") ?  
                          recorderPS.getPanimagingmode() : recorderPS.getPanplusimagingmode();
        RecorderResources rr = new RecorderResources(this.recorderPS);      
        int sectors = rr.calculateRecorderDelta(arlEntry.getDuration(), panRatio);   
        return rr.calculatePlaybackDuration(sectors);            
  }
  
  protected int getPlaybackDurationPanPlusMs()
  {
  	        // get VC1/VC2 bandwidth ratios (VC1 is PAN, VC2 is MS)
        double pan_vc1 = recorderPS.getVc1ratio();
        double ms_vc2 = recorderPS.getVc2ratio();
       
        // look at arlEntry and determine which compression ratio to use
        // if selected DEFAULT/LOW then use normal PAN data rate.
        // If selected HIGH then use PAN ratio from PAN+MS rate
        double panRatio = 1.0;
        String compRatio = arlEntry.getPancompressionratio();
        if (compRatio == "LOW")
        {
        	panRatio = recorderPS.getPanimagingmode();
        }
        else if (compRatio == "DEFAULT" || compRatio == "PAN+MS")
        {
        	panRatio = recorderPS.getPanplusimagingmode();
        }
                          
        // get number of sectors reserved for the duration of the imaging activity and imaging mode
        RecorderResources rr = new RecorderResources(this.recorderPS);      
        int pansectors = rr.calculateRecorderDelta(arlEntry.getDuration(), panRatio);   
        // calculate amount of time to PBK PAN data in interleaved mode
        int panDuration = rr.calculatePlaybackDuration(pansectors, pan_vc1); 
                   
        double msRatio = 1.0;
        if (compRatio == "LOW")
        {
        	msRatio = recorderPS.getMsimagingmode();
        }
        else if (compRatio == "DEFAULT" || compRatio == "PAN+MS")
        {
        	msRatio = recorderPS.getMsplusimagingmode();
        }
                         
        // get number of sectors reserved for the duration of the imaging activity and imaging mode
        int mssectors = rr.calculateRecorderDelta(arlEntry.getDuration(), msRatio);   
        // calculate amount of time to PBK PAN data in interleaved mode
        int msDuration = rr.calculatePlaybackDuration(mssectors, ms_vc2); 
        
        return (panDuration > msDuration) ? panDuration : msDuration;
  }
  
  /*
	protected Activity createActivity(String actName, Integer arlentryid, String arlentrytype, String actsubtype, Timestamp starttime, Timestamp endtime, Integer duration)
	{
		//String name,
		//Integer satelliteid,
		//Integer sessionid,
		//Integer arlentryid,
		//Integer extentid,
		//Integer nextactid,
		//Integer prevactid,
		//String arlentrytype,
		//String actsubtype,
		//Timestamp starttime,
		//Timestamp endtime,
		//Integer duration,
		//Integer scheduled,
		//String toytype,
		//Integer usermodified,
		//Integer valid,
		//String info
		
	    // validate start time
		if (starttime.before(this.session.getStarttime()) || 
			starttime.after(this.session.getEndtime()))
		{
			return null;
		}
		Activity act = new Activity();
		act.setStarttime(starttime);
		
		// calculate end time
		act.setEndtime(endtime); 
		
		// create activity name
		act.setName(actName);
		
		act.setSatelliteid(this.satellite.getId().intValue());
		act.setSessionid(this.session.getId().intValue());
		act.setArlentryid(arlentryid);
		act.setArlentrytype(arlentrytype);
		act.setActsubtype(actsubtype);
		
		act.setExtentid(0);
		act.setNextactid(0);
		act.setPrevactid(0);
		
		
		act.setScheduled(0);
		act.setToytype("NO");
		act.setValid(1);
		act.setUsermodified(0);
		act.setInfo("");	
		
		return act;
		
	}
  */
}