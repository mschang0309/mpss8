package mpss.ac;

import java.sql.Timestamp;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import mpss.common.jpa.*;
import mpss.common.dao.*;
import mpss.util.timeformat.YearDayTimeOffset;

@Service
public class ManeuverActivityCreator extends ActivityCreatorBase
{
	@Autowired
	ActivityRequestListDao activityRequestListDao;
	
	 public Satellite getSatellite()
	 {
			 return this.satellite;
	 }
	 public void setSatellite(Satellite satellite)
	 {
		 this.satellite = satellite;
	 }
	 
	 public Session getSession()
	 {
			 return this.session;
	 }
	 public void setSession(Session session)
	 {
		 this.session = session;
	 }
	 
	public ManeuverActivityCreator()
	{
	}
	
	public ManeuverActivityCreator(Satellite satellite, Session session)
	{
		super(satellite, session);
	}
	
	public List<Activity> create(String arlName)
	{
		if (activityRequestListDao.getArl(arlName) == null)
		{
			// arl does not exist
		    return null;
		}
		List<Maneuverrequest> l = activityRequestListDao.listManeuverrequests(arlName);
		if (l.isEmpty())
		{
			return null;
		}
		List<Activity> res = new ArrayList<Activity>();
		for (Maneuverrequest r : l)
		{
			Activity act = createFrom(arlName,r);
			if (act != null)
			{
			    res.add(act);
			}
		}
		return res;
	}

	private Activity createFrom(String arlName, Maneuverrequest arlEntry)
	{
		Timestamp starttime = YearDayTimeOffset.toTimestamp(arlEntry.getYear(), arlEntry.getDay(), arlEntry.getStarttime());
		if (starttime.before(this.session.getStarttime()) || starttime.after(this.session.getEndtime()))
		{
			logger.info("ManeuverActivityCreator: ARL entty not in session time range");
			return null;
		}
		Timestamp endtime = new Timestamp(starttime.getTime() + arlEntry.getDuration()*1000);
		return createActivity(arlName, arlEntry.getId().intValue(), "MANEUVER", "NO", starttime, endtime, arlEntry.getDuration());
	}
	
}