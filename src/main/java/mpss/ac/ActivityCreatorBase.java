package mpss.ac;

import java.sql.Timestamp;
import java.util.*;

import mpss.configFactory;
import mpss.common.jpa.*;
import mpss.common.dao.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public abstract class ActivityCreatorBase implements IActivityCreator
{
	protected Satellite satellite;
	protected Session session;
	protected Logger logger = Logger.getLogger(configFactory.getLogName());
	
	@Autowired
	ActivityDao activityDao;

	 public Satellite getSatellite()
	 {
			 return this.satellite;
	 }
	 public void setSatellite(Satellite satellite)
	 {
		 this.satellite = satellite;
	 }
	 
	 public Session getSession()
	 {
			 return this.session;
	 }
	 public void setSession(Session session)
	 {
		 this.session = session;
	 }
	 

	public ActivityCreatorBase() {		
	}

	public ActivityCreatorBase(Satellite satellite, Session session)
	{
		this.satellite = satellite;
		this.session = session;
	}
	
	public abstract List<Activity> create(String arlName);
	
	protected Activity createActivity(String actName, Integer arlentryid, String arlentrytype, String actsubtype, Timestamp starttime, Timestamp endtime, Integer duration)
	{
		//String name,
		//Integer satelliteid,
		//Integer sessionid,
		//Integer arlentryid,
		//Integer extentid,
		//Integer nextactid,
		//Integer prevactid,
		//String arlentrytype,
		//String actsubtype,
		//Timestamp starttime,
		//Timestamp endtime,
		//Integer duration,
		//Integer scheduled,
		//String toytype,
		//Integer usermodified,
		//Integer valid,
		//String info
		
	    // validate start time
		if (starttime.before(this.session.getStarttime()) || 
			starttime.after(this.session.getEndtime()))
		{
			return null;
		}
		Activity act = new Activity();
		act.setStarttime(starttime);
		
		// calculate end time
		act.setEndtime(endtime); 
		
		// duration
		act.setDuration(duration);
		
		// create activity name
		//act.setName(arlName +"[" + this.session.getName() + "]");
		act.setName(actName);
		
		act.setSatelliteid(this.satellite.getId().intValue());
		act.setSessionid(this.session.getId().intValue());
		act.setArlentryid(arlentryid);
		act.setArlentrytype(arlentrytype);
		act.setActsubtype(actsubtype);
		
		act.setExtentid(0);
		act.setNextactid(0);
		act.setPrevactid(0);
		
		
		act.setScheduled(0);
		act.setToytype("NO");
		act.setValid(1);
		act.setUsermodified(0);
		act.setInfo("");	

        Activity newAct = new Activity();
        try
        {
        	newAct = this.activityDao.create(act);
        	logger.info("Activity id = " + newAct.getId());
        }
        catch (Exception e)
        {
	        logger.info("createActivity: exception: "+ e.getMessage());
	        newAct = null;
        }

		return newAct;
		
	}
	
}