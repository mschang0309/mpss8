package mpss.ac;

import java.sql.Timestamp;
import java.util.*;
import org.springframework.stereotype.Service;
import mpss.common.jpa.*;
import mpss.common.dao.*;
import mpss.util.timeformat.*;

@Service
public class XcrossActivityCreator extends ActivityCreatorBase
{
	 
	public XcrossActivityCreator()
	{
	}
	
	public XcrossActivityCreator(Satellite satellite, Session session)
	{
		super(satellite, session);
	}
	
	public List<Activity> create(String arlName)
	{
		ActivityRequestListDao dao = new ActivityRequestListDaoImpl();
		if (dao.getArl(arlName) == null)
		{
			// arl does not exist
		    return null;
		}
		
		List<Xcrossrequest> l = dao.listXcrossrequests(arlName);
		if (l.isEmpty())
		{
			return null;
		}
		List<Activity> res = new ArrayList<Activity>();
		for (Xcrossrequest r : l)
		{
			res.add(createFrom(arlName,r));
		}
		return res;
	}

	private Activity createFrom(String arlName, Xcrossrequest arlEntry)
	{
	    // validate start time
		Timestamp starttime = YearDayTimeOffset.toTimestamp(arlEntry.getYear(), arlEntry.getDay(), arlEntry.getStarttime());
		if (starttime.before(this.session.getStarttime()) || starttime.after(this.session.getEndtime()))
		{
			return null;
		}
		Timestamp endtime = new Timestamp(starttime.getTime() + arlEntry.getDuration()*1000);
		return createActivity(arlName, arlEntry.getId().intValue(), "XCROSS", "NO", starttime, endtime, arlEntry.getDuration());
	}
	
	
}