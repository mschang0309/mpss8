package mpss.ac;

import java.sql.Timestamp;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mpss.common.jpa.*;
import mpss.common.dao.*;
import mpss.util.timeformat.*;

@Service
public class UplinkActivityCreator extends ActivityCreatorBase
{
	@Autowired
	ActivityRequestListDao activityRequestListDao;
	
	 private Set<String> dateProcessed = new HashSet<String>();
	 
	 public Satellite getSatellite()
	 {
			 return this.satellite;
	 }
	 public void setSatellite(Satellite satellite)
	 {
		 this.satellite = satellite;
	 }
	 
	 public Session getSession()
	 {
			 return this.session;
	 }
	 public void setSession(Session session)
	 {
		 this.session = session;
	 }
	 
	public UplinkActivityCreator()
	{
	}
	
	public UplinkActivityCreator(Satellite satellite, Session session)
	{
		super(satellite, session);
	}
	
	public List<Activity> create(String arlName)
	{
		if (activityRequestListDao.getArl(arlName) == null)
		{
			// arl does not exist
		    return null;
		}
		
		List<Uplinkrequest> l = activityRequestListDao.listUplinkrequests(arlName);
		if (l.isEmpty())
		{
			return null;
		}
		List<Activity> res = new ArrayList<Activity>();
		for (Uplinkrequest r : l)
		{
			// absolute date
			if (r.getYear() != 0)
			{
			    res.add(createAbsolute(arlName,r));
			    
			    // mark that the date has been processed
			    String thisDate = String.format("%d-%d", r.getYear(), r.getDay());
			    dateProcessed.add(thisDate);
		  }
		  // daily
		  else
		  {
		  	// create activities for the session period
		  	Calendar calcur = getCalendarDay(session.getStarttime());
		  	Calendar calend = getCalendarDay(session.getEndtime());
		  	while (calcur.compareTo(calend) <= 0)
		  	{
			      String thisDate = String.format("%d-%d", calcur.get(Calendar.YEAR), calcur.get(Calendar.DAY_OF_YEAR));
			      // date not processed yet
			      if (!dateProcessed.contains(thisDate))
			      {
			    	  // create activity
			    	  Activity act = createDaily(arlName, r, calcur);
			    	  if (act == null) {
					  	  calcur.add(Calendar.DAY_OF_YEAR, 1);
			    		  continue;
			    	  }
			    	  res.add(act);
			      }
		  		  // next day of year
		  	    calcur.add(Calendar.DAY_OF_YEAR, 1);
		  	}
		  }
		}
		return res;
	}

	private Activity createAbsolute(String arlName, Uplinkrequest arlEntry)
	{
	    // validate start time
		Timestamp starttime = YearDayTimeOffset.toTimestamp(arlEntry.getYear(), arlEntry.getDay(), arlEntry.getStarttime());
		if (starttime.before(this.session.getStarttime()) || starttime.after(this.session.getEndtime()))
		{
			return null;
		}
		Timestamp endtime = new Timestamp(starttime.getTime() + arlEntry.getDuration()*1000);
		return createActivity(arlName, arlEntry.getId().intValue(), "UPLINK", "NO", starttime, endtime, arlEntry.getDuration());
	}
	
	private Activity createDaily(String arlName, Uplinkrequest arlErntry, Calendar calcur)
	{
  	  long t1 = calcur.getTimeInMillis() + arlErntry.getStarttime() * 1000;
  	  if (t1 > session.getEndtime().getTime()) {
  		  // start time is later than session end, skip
  		  return null;
  	  }
  	  long t2 = t1 + 24*60*60*1000;
  	  if (t2 > session.getEndtime().getTime())
  	  {
  		  t2 = session.getEndtime().getTime();
  	  }
  	  // create activity
  	  Activity act = createActivity(arlName, arlErntry.getId().intValue(), "UPLINK", "NO", new Timestamp(t1), new Timestamp(t2), arlErntry.getDuration());
  	  return act;
		
	}
	private Calendar getCalendarDay(Timestamp ts)
	{
		  	Calendar cal = Calendar.getInstance();
		  	cal.setTimeInMillis(ts.getTime());
		  	// truncate
		  	cal.set(Calendar.HOUR_OF_DAY, 0);
		  	cal.set(Calendar.MINUTE, 0);
		  	cal.set(Calendar.SECOND, 0);
		  	cal.set(Calendar.MILLISECOND, 0);
		  	return cal;
	}
}