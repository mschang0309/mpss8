package mpss.ac;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mpss.common.jpa.*;
import mpss.resources.AvailableContacts;

@Service
public class MonitoringActivityCreator extends ActivityCreatorBase
{
	@Autowired
	AvailableContacts availableContacts;
	
	public MonitoringActivityCreator()
	{
	}
	
	public MonitoringActivityCreator(Satellite satellite, Session session)
	{
		super(satellite, session);
	}
	
	public List<Activity> create(String arlName)
	{
		List<Activity> res = createFromContacts();
		return res;
	}

	private List<Activity> createFromContacts()
	{
		List<Activity> res = new ArrayList<Activity>();
		
		// get all sband contacts that occurs during this session (sorted based on start time)
		//AvailableContacts acs = new AvailableContacts(satellite, session);
		availableContacts.setSatellite(satellite);
		availableContacts.setSession(session);
		List<Contact> contacts = availableContacts.getAvailableContactsBySiteType("SBAND");
		if (contacts.size() == 0)
		{
			return res;
		}
		
		// merge overlapping contacts (intervals)
		Stack<Contact> s = new Stack<Contact>();
		s.push(contacts.get(0));
		for (int i = 1; i < contacts.size(); i++)
		{
			Contact top = s.peek();
			// if current contact is not overlapping with stack top, push it to the stack
			Contact cur = contacts.get(i);
			if (top.getFade().getTime() < cur.getRise().getTime())
		  {
		      s.push(cur);
		  }
		  // otherwise update the end time of top
		  else if (top.getFade().getTime() < cur.getFade().getTime())
		  {
		      top.setFade(cur.getFade());
		  	  // update top (not necessary for java because every object is a reference) 
		      s.pop();
		      s.push(top);
		  }
		}
		
		// create activities by merged contacts
		while (!s.empty())
		{
			Contact c = s.peek();
			String actName = String.format("MON(%s)[%s]", satellite.getName(), session.getName());
			// set duration = 1, scheduler will expand out the monitoring to fit within the contact
			Activity act = createActivity(actName, 0, "MON", "NO", c.getRise(), c.getFade(), 1);
			res.add(act);
			s.pop();
		}
		return res;
	}
}