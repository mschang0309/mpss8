package mpss.ac;

import java.sql.Timestamp;
import mpss.common.jpa.*;

public interface IPlaybackActivityCreator extends IActivityCreator
{
	public Rsiimagingrequest getRsiimagingrequest();
	public void setRsiimagingrequest(Rsiimagingrequest arlEntry);
	public Recorderp getRecorderp();
	public void setRecorderp(Recorderp recorderp);
	public void setStartTime(Timestamp time);
	public void setEndTime(Timestamp time);
}