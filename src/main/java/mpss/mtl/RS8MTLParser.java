package mpss.mtl;

import java.util.Scanner;
import java.util.Date;
import java.lang.reflect.*;

public class RS8MTLParser extends MTLParserBase
{	
	private String[] rsiMemberNames =new String[]{"event", "i_ev", "imagine_mode", "utc" ,"dur", "q1" ,"q2", "q3", "q4", "r_rate", "p_rate", "y_rate", "mode", "station", "orbit", "ssr_status", "pancompressionratio","mscompressionratio", "pan_line_compressionratio","ms_line_compressionratio","videopangain","videomb1gain","videomb2gain","videomb3gain","videomb4gain","ratefit1_1","ratefit1_2","ratefit1_3","ratefit2_1","ratefit2_2","ratefit2_3","ratefit3_1","ratefit3_2","ratefit3_3"};
	private int[] rsiMemberTypes = new int[]{1,1,4,3,1,2,2,2,2,2,2,2,1,4,1,4,4,4,2,2,4,4,4,4,4,4,4,4,4,4,4,4,4,4};
	private boolean[] rsiMemberConcat = new boolean[]{false,false,false,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};
	
	private String[] aipMemberNames =new String[]{"event", "utc" ,"dur", "q1" ,"q2", "q3", "q4", "procedure"};
	private int[] aipMemberTypes = new int[]{1,3,1,2,2,2,2,4};
	private boolean[] aipMemberConcat = new boolean[]{false,true,false,false,false,false,false,false};
	
	public MTLRecord parseTimeline(String text)
	{
		String[] mtlMemberNames;
		int[] mtlMemberTypes;
		boolean[] mtlMemberConcat;
		
		if(instrument.equalsIgnoreCase("AIP")){
			mtlMemberNames = this.aipMemberNames;
			mtlMemberTypes = this.aipMemberTypes;
			mtlMemberConcat = this.aipMemberConcat;
		}else{
			mtlMemberNames = this.rsiMemberNames;
			mtlMemberTypes = this.rsiMemberTypes;
			mtlMemberConcat = this.rsiMemberConcat;
		}
		
    	// parse this line
    	Scanner lineTokenizer = new Scanner(text);
    	//lineTokenizer.useDelimiter(" +"); // one or more spaces
    	
	    MTLRecord entry = new MTLRecord();
		try
		{
		    Class<?>  clazz = entry.getClass();
	
		    for (int i = 0; lineTokenizer.hasNext() && i < mtlMemberNames.length; i++)
		    {		    	
           	    String data = lineTokenizer.next();
        	    if (mtlMemberConcat[i] == true && lineTokenizer.hasNext())
        	    {
        	    	// concatenate two neighboring elements
    	            data = data.concat(" ").concat(lineTokenizer.next());
        	    }
            	 
        	    //System.out.println("data: " + data);
				Field field = clazz.getDeclaredField(mtlMemberNames[i]);
				switch (mtlMemberTypes[i])
				{
					// integer
					case 1:
						Integer ival;
	                    if (data.equals("NA"))
	                    {
	                    	ival = null;
	                    }
	                    else
	                    {
	 					    ival = Integer.parseInt(data);
	                    }
						field.set(entry, ival);
						break;
				    // double
					case 2:
						Double dval;
	                    if (data.equals("NA"))
	                    {
	                    	//dval = Double.POSITIVE_INFINITY;
	                    	dval = Double.NaN;
	                    }
	                    else
	                    {
	   					   dval = Double.parseDouble(data);
	                    }
	                    field.set(entry, dval);
						break;
					// date (UTC)
					case 3:
						Date date = parseUTC(data);
						field.set(entry, date);
						break;
					// string
					case 4:
						data = data.trim();
						field.set(entry, data);
						break;
				}
		    }
		}
		catch (Exception e)
		{
			returnMsg = "Exception: "+ e.getMessage();
			logger.info("Exception: " + e.getMessage());
			lineTokenizer.close();
			return null;
		}
		
		lineTokenizer.close();		
	    return entry;
	}
	
	 public boolean parseTitle(String text){		 
		 String[] mtlMemberNames;
		 if(instrument.equalsIgnoreCase("AIP")){
				mtlMemberNames = this.aipMemberNames;				
		 }else{
				mtlMemberNames = this.rsiMemberNames;				
		 }	     
	     try{	  
	    	 if(text.trim().split("\\s+").length!=mtlMemberNames.length){	    		 
	    		 return false;
	    	 }	    	 
		 }catch (Exception e){			 
				return false;
		 }	     
	     return true;
	 }
	 
	 public boolean parseData(String text){
		 String[] mtlMemberNames;
		 if(instrument.equalsIgnoreCase("AIP")){
				mtlMemberNames = this.aipMemberNames;				
		 }else{
				mtlMemberNames = this.rsiMemberNames;				
		 }	     
	     try{	  
	    	 if(text.trim().split("\\s+").length!=mtlMemberNames.length+1){	    		 
	    		 return false;
	    	 }	    	 
		 }catch (Exception e){			 
				return false;
		 }	     
	     return true;
	 }
	
}
