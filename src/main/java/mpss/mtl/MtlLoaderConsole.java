package mpss.mtl;
import mpss.config;
import mpss.configFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class MtlLoaderConsole 
{
	@SuppressWarnings("unused")
	public static void main(String args[])
	{
		
		//String fileName = "FS5_RSITimeline_20180101_A_MA.rpt";		
		String fileName = "FS5_RSITimeline_20180926_1.rpt";
		String fileType = "RSI";
        //String fileName = "AIP_20160107_timeline";
		//String fileType = "AIP";
        //String fileType = "ISUAL";
        //String fileName = "ISUAL_2009159_timeline";
		boolean branch = false;
		boolean isCheck = false;		  
		
		System.setProperty("satName", "FORMOSAT5");
		config.load();  
		
		String mtlfilePath = System.getProperty("dataFileDir");
		if (fileType.equalsIgnoreCase("ISUAL"))
		{
			mtlfilePath += System.getProperty("mtlISUALFileDir") + fileName;
		}
		else if (fileType.equalsIgnoreCase("AIP"))
		{
			mtlfilePath += System.getProperty("mtlAIPFileDir") + fileName;
		}  		
		else
		{
			mtlfilePath += System.getProperty("mtlRSIFileDir") + fileName;
		}
		
		try 
		{
			// Spring application context
			ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf",configFactory.getSatelliteName()}));
			org.apache.log4j.xml.DOMConfigurator.configure(javae.AppDomain.getPath("log4j.xml", new String[]{"conf",configFactory.getSatelliteName()}));   
			
			String beanName =configFactory.getMtlLoaderName();
			MTLLoader mtlLoader = (MTLLoader)context.getBean(beanName);

			mtlLoader.setIsBranch(branch);
			//mtlLoader.setSessionId(sessionid);
			if (mtlLoader == null) 
			{
				System.out.println("load MtlLoader bean failed");
				return;
			}
		    
			if (mtlLoader.Load(mtlfilePath,isCheck))
			{
				System.out.println(mtlLoader.getActIdList());
				String msg = (mtlLoader.getMsg() == null) ? "MTL Loader complete" : mtlLoader.getMsg();
				System.out.println(msg);				
			}
			else
			{
				String msg = (mtlLoader.getMsg() == null) ? "MTL Loader failed" : mtlLoader.getMsg();
				System.out.println(msg);
				return;
			}
    	
      } catch (Exception ex) {
    		ex.printStackTrace();
      }
	}
}
