package mpss.mtl;

import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.util.Date;
import java.io.*;
import java.lang.reflect.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.LinkedList;

import mpss.configFactory;

import org.apache.log4j.Logger;

public abstract class MTLParserBase implements MTLParser
{
	protected Logger logger = Logger.getLogger(configFactory.getLogName());
	public Scanner input;	
	public String source;
	public String destination;
	public String filename;
	public Date datetime;
	public String spacecraft;
	public String instrument;
	public String requestwindow;
	public int orbit;
	public double tImagT;
	public Date requestwindowStarttime;
	public Date requestwindowEndtime;    
	public List<MTLRecord> missionTimelines;
    public String returnMsg = "";
    
    public String getReturnMsg()
    {
    	return this.returnMsg;
    }
    
    public String getSource()
    {
    	return this.source;
    }
    
    public String getDestination()
    {
    	return this.destination;
    }
    
    public String getFilename()
    {
    	return this.filename;
    }
    
    public Date getDatetime()
    {
        return this.datetime;
    }
    
    public String getSpacecraft()
    {
    	return this.spacecraft;
    }
    
    public String getInstrument()
    {
    	return this.instrument;
    }
    
    public int getOrbit()
    {
    	return this.orbit;
    }
    
    public double getTImagT()
    {
    	return this.tImagT;
    }

    public Date getRequestwindowStarttime()
    {
    	return this.requestwindowStarttime;
    }
    
    public Date getRequestwindowEndtime()
    {
    	return this.requestwindowEndtime;
    }
    
    public List<MTLRecord> getMissionTimelines()
    {
    	return this.missionTimelines;
    }
    
    public boolean parseTitle(String text){
    	return true;
    }
    
    public boolean parseData(String text){
    	return true;    	
    }
    
	public boolean open(String filepath)
	{
		try 
		{
		    File file = new File(filepath);
			this.input = new Scanner(file);
		} 
		catch (FileNotFoundException e) {
			return false;
	    }		
		return true;
	}
	
	public boolean parse()
	{
		if (!parseHeader())
		{
		    return false;
		}		
		
		this.missionTimelines = parseTimelines();
		this.input.close();
		if (this.missionTimelines == null){				
			return false;
		}	
		
	    return true;
	}
	
	
	public boolean parseHeader()
	{
		//SOURCE:         IPS PSS
		//DESTINATION:    MMC MCC
		//FILE NAME:      RS2_RSITimeline_20090601_A1.rpt
		//DATE TIME:      2009/05/31 02:25:28
		//SPACECRAFT:     ROCSAT-2
		//INSTRUMENT:     RSI
		//REQUEST WINDOW:         2009/152 01:38:00 - 2009/152 01:53:03
		String[] headerFields = new String[]{"SOURCE" , "DESTINATION","FILE NAME", "DATE TIME", "SPACECRAFT", "INSTRUMENT", "REQUEST WINDOW"};
		String[] headerMembers = new String[]{"source" , "destination","filename", "datetime", "spacecraft", "instrument", "requestwindow"};
		int[] headerTypes = new int[]{1,1,1,2,1,1,1};
		
		int lineno = 0;
        //for (int lineno = 0; lineno < 7; lineno++)
		while (this.input.hasNext())
        {
			if(lineno == 7 )
				break; 
			
	    	try
	    	{
	    		String nextLine = this.input.nextLine();
	    		if(!nextLine.startsWith("#"))
                //if (this.input.hasNext())
                {
            	    //Scanner lineTokenizer = new Scanner(this.input.nextLine());
	    			Scanner lineTokenizer = new Scanner(nextLine);
            	    lineTokenizer.useDelimiter(":");
            	    String name = lineTokenizer.next();
            	    if ((!name.equals(headerFields[lineno])) && (!name.replace("_", " ").equals(headerFields[lineno])))
            	    {
            	    	logger.info("Field not found: " + name);
            	    	lineTokenizer.close();
            	        return false;
            	    }
            	
            	    if (lineTokenizer.hasNext())
            	    {
                	    String data = lineTokenizer.next();
    	            	// date format will conflict with delimiter
                	    while (lineTokenizer.hasNext())
                	    {
        	            	data = data.concat(":").concat(lineTokenizer.next());
                	    }
                	    
                	    // use reflection to set class members
            	        Class<?>  clazz = this.getClass().getSuperclass();            	        
            	        Field field = clazz.getDeclaredField(headerMembers[lineno]);
            	        switch (headerTypes[lineno])
            	        {
            	            // string
            	            case 1:
            	            	data = data.trim();
            	                field.set(this, data);
            	                break;
            	            // date
            	            case 2:
            	    	        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            	    	        //formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            	    	        Date date = (Date) formatter.parse(data);
            	    	        field.set(this, date);
            	    	        //System.out.println(this.datetime);
              	                break;            	    	  
            	        }

            	        if (lineno == 4)
            	        {
                	        String scName = System.getProperty("satName");
                			if (!scName.equalsIgnoreCase(data.replace("-","")))
                			{
                				returnMsg = "Satellite must be " + scName + ",but MTL SPACECRAFT[" + data.replace("-","") + "] is invalid.";
                				lineTokenizer.close();
                				return false;
                			}
                			
            	        }
            	    }
            	    lineTokenizer.close();
            	    lineno++;
	    	    }
	    	}
            catch (Exception e)
	    	{
	    	    logger.info("Exception: " + e.getMessage());
	    	}
            
        }
		//ORBIT    TImagT
        if (this.input.hasNext())
        {
        	// skip this line
    	    if (this.input.nextLine().startsWith("ORBIT"))
    	    {
    			//25726       0.7
    	        if (this.input.hasNext())
    	        {
    	        	
    	        	// parse this line
    	        	Scanner lineTokenizer = new Scanner(this.input.nextLine());
    	        	//lineTokenizer.useDelimiter(" +");
    	        	if (lineTokenizer.hasNext())
    	        	{
    	        		//String s = lineTokenizer.next();
    	        		//System.out.println(s);
    	        		this.orbit = lineTokenizer.nextInt();
    	        	}
    	        	if (lineTokenizer.hasNext())
    	        	{
    	        		//String t = lineTokenizer.next();
    	        		this.tImagT = lineTokenizer.nextDouble();
    	        	}
    	        	lineTokenizer.close();
    	        }
    	    }
        }

		// post process request window (parse UTC time)
		if(!parseRequestWindow()){
			return false;
		}
        
		return true;
	}

	public boolean parseRequestWindow()
	{
		//REQUEST WINDOW:         2009/152 01:38:00 - 2009/152 01:53:03
		//REQUEST WINDOW(AIP):         2009/152 - 2009/152
		String[] tokens = this.requestwindow.split("-");
		if (tokens.length != 2)
		{
			return false;
		}
		
		for (int i=0; i< tokens.length;i++)
		{
			tokens[i] = tokens[i].trim();
		}  
		
		if (this.instrument.equalsIgnoreCase("AIP"))
		{
			if(tokens[0].length()==8){
				tokens[0] = tokens[0] + " 00:00:00";
			}else if(tokens[0].length()==17){
				
			}else{
				returnMsg = "REQUEST WINDOW Time Format error";
				return false;
			}
			
			if(tokens[1].length()==8){
				tokens[1] = tokens[1] + " 00:00:00";
			}else if(tokens[1].length()==17){
				
			}else{
				returnMsg = "REQUEST WINDOW Time Format error";
				return false;
			}						
		}else{
			if(tokens[0].length()!=17){
				returnMsg = "REQUEST WINDOW Time Format error";
				return false;
			}
			if(tokens[1].length()!=17){
				returnMsg = "REQUEST WINDOW Time Format error";
				return false;
			}			
		}
		
		
		this.requestwindowStarttime = parseUTC(tokens[0]);
		this.requestwindowEndtime = parseUTC(tokens[1]);
		
		return true;
	}
  
	public List<MTLRecord> parseTimelines()
	{
		List<MTLRecord> list = new LinkedList<MTLRecord>();
		
		// skip header
		if (this.input.hasNext())
        {
        	// skip this line
    	    //System.out.println("skip header: " + this.input.nextLine());
    	    if(!parseTitle(this.input.nextLine())){
    	    	returnMsg = "Parse Data Header is error";
    	    	return null;
    	    }    	    
        }
		//1     2     2009/152 01:38:00     52   0.97151   0.13126   0.19554  -0.02642     5   NSPO    4.7           11.7        25726 NA   NA   NA  NA   NA NA NA    NA
		//2     3     2009/152 01:38:59     13  NA  NA  NA  NA 3   NSPO  9.1          11.7        25726 0   0   0  0    41.2  40.7 1    REC HIGH HIGH G5 G6 G6 G6 G4
		//11     1     2009/152 01:49:03    180  NA  NA  NA  NA 5   NSPO 57.6         199.2        25726 NA   NA   NA  NA   NA NA 1    NA
		//END OF TIMELINE
        int lineNumber = 0;
        while (this.input.hasNext())
        {
        	// parse time line entry
        	String text = this.input.nextLine();
    		if (text.startsWith("END OF"))
    		{
        		System.out.println(text);
    			break;
    		}
    		if ((!text.startsWith("#")) && text.trim().length() > 0)
    		{
        		lineNumber++;
        		logger.info("parse line " + lineNumber + " text: " + text);
        		
        		if(!parseData(text)){
        	    	returnMsg = "Parse Data error : parse line " + lineNumber + " text: " + text;
        	    	return null;
        	    }

        		MTLRecord me =parseTimeline(text);
        		        		
            	if (me == null)
            	{
            		returnMsg = "parse line " + lineNumber + " text " + returnMsg;
            		return null;
            	}
            		
            	list.add(me);
    		}

        }
        
        
        Collections.sort(list,
        new Comparator<MTLRecord>() {
            public int compare(MTLRecord o1, MTLRecord o2) {
                return o1.utc.compareTo(o2.utc);
            }
        });
	
		return list;
	}	
	
	public Date parseUTC(String text)
	{
		SimpleDateFormat utc = new SimpleDateFormat("yyyy/D HH:mm:ss");
		//utc.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = new Date();
		try
		{
		   date = (Date) utc.parse(text);
		   //System.out.println("Date: " + text +" to " + date);
		}
		catch (Exception e)
		{
			logger.info("Exception: " + e.getMessage());
		}
        return date;
	}
}
