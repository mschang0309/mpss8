package mpss.mtl;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TimeZone;
import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Range;

import mpss.configFactory;
import mpss.common.jpa.*;
import mpss.common.dao.*;
import mpss.util.timeformat.*;

// for Spring component scanning
@Service
public class MTLLoaderBase implements MTLLoader
{
	protected Logger logger = Logger.getLogger(configFactory.getLogName());
	protected int scId;
	protected int sessionId;
	private String scName;
	protected boolean isBranch;
	protected boolean isCheck;
	private String arlName;
	
	private LinkedList<Long> actIdList = new LinkedList<Long>();
	private String sessName;
	protected TimeRange sessionTR;
	protected String filePath;
	
	protected MTLParser mtlParser;
	public String returnMsg;
	
	protected boolean needConfirm;
	private boolean needConfirm1;
	public String confirmMsg;
	
	// for Spring injection
	@Autowired
	SatelliteDao satelliteDao;
	
	@Autowired
	ArlDao arlDao;
	
	@Autowired
	SessionDao sessionDao;
	
	@Autowired
	ManeuverrequestDao manReqDao;
	
	@Autowired
	RsiimagingrequestDao rsiReqDao;
	
	@Autowired
	XcrossrequestDao xcrossReqDao;
	
	@Autowired
	ExternalactivityrequestinfoDao earDao;
	
	@Autowired
	ActivityDao actDao;
	
	@Autowired
	ExtentDao exeDao;
	
	@Autowired
	ImagingmodeDao imgModeDao;
	
	@Autowired
	RecorderpDao recpDao;
	
	@Autowired
	SatellitepDao satpDao;
	
	@Autowired
	UnavailableResourcesDao unavailableResourcesDao;
	
	@Autowired
	ContactDao contactDao;
	
	@Autowired
	SiteDao siteDao;
	
	@Autowired
	RevDao revDao;
	
	@Autowired
	IsualprocrequestDao isualReqDao;
	
	@Autowired
	AipprocrequestDao aipReqDao;
	
	@Autowired
	EclipseDao eclipseDao;
	
	@Autowired
	AipcommandDao aipcmdDao;
	
	public String getMsg()
	{
		return this.returnMsg;
	}
	public String getConfirmMsg()
	{
		return this.confirmMsg;
	}
	public LinkedList<Long> getActIdList()
	{
	    return this.actIdList;
	}

	public int getSessionId()
	{
	    return this.sessionId;
	}

	public boolean getNeedConfirm()
	{
	    return this.needConfirm;
	}
	
	public void setIsBranch(boolean branch)
	{
		 this.isBranch = branch;
	}
	
	public MTLLoaderBase()
	{
		// default constructor must exist for Spring
	}

	public boolean Load(String mtlFileName,boolean check)
	{
		isCheck = check;
		scName = System.getProperty("satName");
		scId = satelliteDao.getByName(scName).getId().intValue();
		filePath = mtlFileName.substring(0, mtlFileName.lastIndexOf("\\") +1 );
		// load mission time line (activity request list)
		if (!LoadMtl(mtlFileName))
		{
			return false;
		}
	  
		return true; 
	}

	// load mtl file
	@Override
	public boolean LoadMtl(String mtlFileName)
	{
	    this.mtlParser = configFactory.getMTLParser();
	    if (!mtlParser.open(mtlFileName))
	    {
	    	return false;
	    }
	    
	    if (!mtlParser.parse())
	    {
	    	returnMsg =  mtlParser.getReturnMsg();
	    	return false;
	    }
	    
	    logger.info("Total requests: " + mtlParser.getMissionTimelines().size());	    
        
	    //if import file Request Window range includes DB ear timerange,delete db MLT,then import new MLT
        this.arlName = extractFileName(mtlFileName);
        //this.arlPath = extractFilePath(mtlFileName);
        TimeRange rwRang = new TimeRange(new Timestamp(mtlParser.getRequestwindowStarttime().getTime()), new Timestamp(mtlParser.getRequestwindowEndtime().getTime()));
    	List<Externalactivityrequestinfo> ears = earDao.getBySatNameNBranch(mtlParser.getSpacecraft().replace("-",""),isBranch, mtlParser.getInstrument());
    	List<Integer> containArlId = new ArrayList<Integer>();
    	SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    	String strArlId = "(";
    	Timestamp ArlStart = Timestamp.valueOf("2099-01-01 00:00:00");
    	Timestamp ArlEnd = Timestamp.valueOf("2010-01-01 00:00:00");
    	TimeRange arlRange = null;
    	for (Externalactivityrequestinfo exitEar : ears)
    	{
    		//contain
    		if (rwRang.contains(exitEar.getStarttime()) && rwRang.contains(exitEar.getEndtime()))
    		{
    			containArlId.add(exitEar.getArlid());
    			strArlId += exitEar.getArlid() + ",";
    			ArlStart = exitEar.getStarttime().before(ArlStart) ? exitEar.getStarttime() : ArlStart;
    			ArlEnd = exitEar.getEndtime().after(ArlEnd) ? exitEar.getEndtime() : ArlEnd;
    			arlRange = new TimeRange(ArlStart,ArlEnd);
    		}
    		//Overlap not contain
    		else if(mtlParser.getRequestwindowEndtime().getTime() > exitEar.getStarttime().getTime()
        			&& mtlParser.getRequestwindowStarttime().getTime() < exitEar.getEndtime().getTime())
    		{
    			returnMsg = arlName + "'s time window[" + df.format(mtlParser.getRequestwindowStarttime()) +
    					" ~ " + df.format(mtlParser.getRequestwindowEndtime()) + "] must completely overlap (contain)" +
    					" the file has been previously imported time window[" + df.format(exitEar.getStarttime()) + 
    					" ~ " + df.format(exitEar.getEndtime()) + "]";
    			return false;
    		}
    		
    	}
	    
    	List<Site> sites = siteDao.getAll();
    	int lineNum = 0;
    	List<Integer> asyncIndex = new ArrayList<Integer>();
    	List<Integer> syncIndex = new ArrayList<Integer>();
    	boolean gohomeLimit = false;
    	Date gohomeLimitTime = null;
		String manType = "Maneuver";
    	int goHomeMinLimit = Integer.valueOf(System.getProperty("goHomeMinLimit"));

    	needConfirm = false;
    	confirmMsg = "";
    	
	    for (MTLRecord mr : mtlParser.getMissionTimelines())
	    {
	    	needConfirm1 = false;
	    	lineNum++;
	    	Timestamp sTime = new Timestamp(mr.utc.getTime());	   
	    	Timestamp eTime = new Timestamp(mr.utc.getTime() + mr.dur * 1000); 	
	    	
	    	//Get session
	    	if (lineNum == 1)
	    	{
	    		int br = isBranch ? 1 : 0;
	    		Session ss = sessionDao.getSessionbyTime(sTime, br, scId);
	    		if (ss == null)
	    		{
	    			returnMsg = "No session or Multi sessions contains MLT time[" + sTime.toString() + "]";
	    			return false;
	    		}
	    		this.sessionId = ss.getId().intValue();
	    		this.sessName = ss.getName();
	    		this.sessionTR = new TimeRange(ss.getStarttime(),ss.getEndtime());
	    		

	    		//if any MTL after injest MTL in this session, reject
	    		long row = earDao.getAfterTimeMTL(mtlParser.getRequestwindowStarttime(), sessionId, isBranch, mtlParser.getInstrument());
	    		if (row < 0)
	    		{
	    			returnMsg = "earDao.getAfterTimeMTL Exception";
	    			return false;
	    		}
	    		if (row > 0)
	    		{
	    			returnMsg = "The ingested MTL Time window is less than the existing or previously ingested MTL.";
	    			return false;
	    		}
	    	}
	    	
	    	//MissionTimeline UTC must be in REQUEST WINDOW
	    	if (!rwRang.contains(sTime) || !rwRang.contains(eTime))
	    	{
	    		returnMsg = "event " + mr.event + " time range [" + sTime.toString() + " ~ " + eTime.toString() + "] is not in request window";
	    		return false;
	    	}
	    	
	    	if (mtlParser.getInstrument().equalsIgnoreCase("RSI"))
	    	{
	    		//STATION must be in sites or ANY
		    	boolean isStation = false;
				for (Site s :  sites)
				{
					if (mr.station.equalsIgnoreCase(s.getName4()))
					{
						isStation = isStation || true;
					}
				}
				if (!(isStation || mr.station.equalsIgnoreCase("ANY")))
				{
					returnMsg = "event " + mr.event + " station [" + mr.station + "] is invalid.";
		    		return false;
				}
				
				//check 50 min limit
				//The interval between Attitude Maneuver(i_ev=2) and GOHOME Maneuver(i_ev=1) can't not over 50 min
				DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    	        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
				if (!gohomeLimit && mr.i_ev == 2)
				{
					gohomeLimit = true;
					Eclipse eclipse = eclipseDao.getByManeuver(isBranch ? sessionId : 0, mr.utc);
					if (eclipse == null)
					{
						gohomeLimitTime = DateUtil.DateAddSec(mr.utc, 60 * goHomeMinLimit);
						manType = "Maneuver[" + formatter.format(mr.utc) + "]";
					}
					else
					{
						gohomeLimitTime = TimeUtil.timeAppendSec(eclipse.getPenumbraentrancetime(), 60 * goHomeMinLimit);
						manType = "Maneuver in Eclipse[" + formatter.format(eclipse.getPenumbraentrancetime()) + "]";
					}
						
				}
				if (gohomeLimit && mr.i_ev == 1)
				{
					gohomeLimit = false;
					if( mr.utc.after(gohomeLimitTime))
					{
						returnMsg = "The interval between " + manType + " and GOHOME[" + formatter.format(mr.utc) + "] can't not over " + goHomeMinLimit + " min.";
			    		return false;
					}
				}
				
				
				// Video Gains(PAN : MB1~MB4) value must be G1 or G2 or G4
				if (mr.i_ev == 3){
						
					    // 20181123 by jeff , refactory for fs5,fs8
						if(checkGain(mr)==false){
							return false;
						}
						
						//find asynchronous and syncIndex REC
						if (mr.imagine_mode.equalsIgnoreCase("1"))
						{
							asyncIndex.add(lineNum - 1);
						}
						if (mr.imagine_mode.equalsIgnoreCase("0"))
						{
							syncIndex.add(lineNum - 1);
						}
						
						if (!isCheck)
						{
							// PAN_LINE - DUR*3367/40 > 5 or < 0 need confirm
							needConfirm1 = false;
							double calPanLine = Math.ceil((mr.dur * 3367) / 40);
							if(mr.pan_line_compressionratio - calPanLine > 5 || mr.pan_line_compressionratio - calPanLine < 0)
							{
								confirmMsg += "event " + mr.event + " PANline is [" + mr.pan_line_compressionratio + "] but DUR*3376/40 is [" + calPanLine + "]<br>";
								needConfirm1 = true;
								needConfirm = true;
							}
						}
					}
					
					if (!isCheck)
					{
						// Q1^2+Q2^2+Q3^2+Q4^2 - 1 > 0.00001 need confirm
						if (mr.i_ev == 2)
						{
							double sosQ = Math.pow(mr.q1,2) + Math.pow(mr.q2,2) + Math.pow(mr.q3,2) + Math.pow(mr.q4,2);
							if(Math.abs(sosQ - 1) > 0.00001)
							{
								if (needConfirm1)
									confirmMsg = confirmMsg.replace("<br>", "; ") + "Q1^2+Q2^2+Q3^2+Q4^2 is [" + sosQ + "]<br>";
									//returnMsg += "; Q1^2+Q2^2+Q3^2+Q4^2 is [" + sosQ + "]\n";
								else
									confirmMsg += "event " + mr.event + " Q1^2+Q2^2+Q3^2+Q4^2 is [" + sosQ + "]<br>";
									//returnMsg += "event " + mr.event + " Q1^2+Q2^2+Q3^2+Q4^2 is [" + sosQ + "]\n";
								needConfirm = true;
							}		
						}
					}			    				
	    	}
	    }
	    
	    
	  // 20170809 modify by jeff warring rsi sequence , start	 	
	  if (!isCheck){
	    // For MTL asynchronous and synchronous check
	    if (mtlParser.getInstrument().equalsIgnoreCase("RSI")){
	    	// if REC is asynchronous,previous data must be Attitude Maneuver and rate not zero
		    // next data must be GOHOME Maneuver
	    	// 20181123 by jeff , refactory for fs5,fs8
	    	checkRSI(scName,asyncIndex,syncIndex);
	    }	    
	  }
	    
	 //  20170809 modify by jeff warring rsi sequence , end
	  
	  // 20170922 add by jeff warring check mlt in db
	  if (!isCheck){
	  if(containArlId.size() > 0){
		  if (!mtlParser.getInstrument().equalsIgnoreCase("AIP"))
	    	{
	    		strArlId = strArlId.substring(0, strArlId.length()-1) + ")";
		    	List<Integer> extIds = actDao.getExtentIdNotinArlid(strArlId, arlRange);
		    	List<Long> actIds = actDao.getActivityIdNotinArlid(strArlId, arlRange);		    	
		    	if (extIds == null)
	    		{
	    			returnMsg = "actDao.getExtentIdByManu Exception";
	    			return false;
	    		}
		    	else if (extIds.size() > 0)
		    	{		    
		    		needConfirm = true;
	    			for (int i =0; i<extIds.size();i++)
	    			{	  	    		
	    				confirmMsg += "[Activity id="+actIds.get(i) +",extent id="+extIds.get(i)+"]";
	    			}	    			
	    			
		    	}
	    	}else{
	    		returnMsg = "AIP has already ingest. Can't ingest.";
  			    return false;
	    	}
	    }
	    
	  }
	  
	// 20170922 add by jeff warring check mlt in end
	    
	    if (needConfirm)
	    	return true;
	    
	    //check AIP procedure
	    if (mtlParser.getInstrument().equalsIgnoreCase("AIP"))
	    {
	    	if(!checkAIPPROCMTLs())
	    		return false;
	    }
	    
	    
	    
	    //import file Request Window range includes DB ear timerange,delete db MLT,then import new MLT 
	    if(containArlId.size() > 0)
	    {
	    	
	    	for (Integer arlId : containArlId)
		    {
	    		Externalactivityrequestinfo delEar = earDao.getByArlId(arlId);
    	    	if (delEar != null)
    	    	{
    	    		earDao.delete(delEar);
    	    	}
    	    	Arl delArl = arlDao.get((long)arlId);
    	    	if (delArl != null)
    	    	{
    	    		arlDao.delete(delArl);
    	    	}
    	    	for(Rsiimagingrequest entry : rsiReqDao.getByArlId(arlId))
    	    	{
    	    		for(Activity delAct : actDao.getByEntryId(entry.getId().intValue()))
        	    	{
    	    			Extent delExe = exeDao.getByActId(delAct.getId().intValue());
    	    			if (delExe != null)
    	    			{
    	    				exeDao.delete(delExe);
    	    			}
    	    			actDao.delete(delAct);
        	    	}
    	    		
    	    		rsiReqDao.delete(entry);
    	    	}
    	    	for(Maneuverrequest entry : manReqDao.getByArlId(arlId))
    	    	{
    	    		for(Activity delAct : actDao.getByEntryId(entry.getId().intValue()))
        	    	{
    	    			Extent delExe = exeDao.getByActId(delAct.getId().intValue());
    	    			if (delExe != null)
    	    			{
    	    				exeDao.delete(delExe);
    	    			}
    	    			actDao.delete(delAct);
        	    	}
    	    		manReqDao.delete(entry);
    	    	}
    	    	for(Xcrossrequest entry : xcrossReqDao.getByArlId(arlId))
    	    	{
    	    		for(Activity delAct : actDao.getByEntryId(entry.getId().intValue()))
        	    	{
    	    			Extent delExe = exeDao.getByActId(delAct.getId().intValue());
    	    			if (delExe != null)
    	    			{
    	    				exeDao.delete(delExe);
    	    			}
    	    			actDao.delete(delAct);
        	    	}
    	    		xcrossReqDao.delete(entry);
    	    	}
    	    	for(Isualprocrequest entry : isualReqDao.getByArlId(arlId))
    	    	{
    	    		for(Activity delAct : actDao.getByEntryId(entry.getId().intValue()))
        	    	{
    	    			Extent delExe = exeDao.getByActId(delAct.getId().intValue());
    	    			if (delExe != null)
    	    			{
    	    				exeDao.delete(delExe);
    	    			}
    	    			actDao.delete(delAct);
        	    	}
    	    		isualReqDao.delete(entry);
    	    	}
				for(Aipprocrequest entry : aipReqDao.getByArlId(arlId))
    	    	{
    	    		for(Activity delAct : actDao.getByEntryId(entry.getId().intValue()))
        	    	{
    	    			Extent delExe = exeDao.getByActId(delAct.getId().intValue());
    	    			if (delExe != null)
    	    			{
    	    				exeDao.delete(delExe);
    	    			}
    	    			actDao.delete(delAct);
        	    	}
    	    		aipReqDao.delete(entry);
    	    	}
		    }
	    }
	    
		  
		Arl arl = new Arl();
		arl.setArltype("EXTFILE");
		String arlNameWithBr = isBranch ? this.arlName + "_(" + sessName + ")" : this.arlName;
		arl.setName(arlNameWithBr);
		arl.setShadow(1);
   
		// if arl exist, delete arl and Externalactivityrequestinfo
		try
		{
			Arl oldarl = arlDao.getByName(arlNameWithBr);
			if(oldarl != null)
			{
				arlDao.delete(oldarl);
				Externalactivityrequestinfo oldear = earDao.getByArlId(oldarl.getId().intValue());
				if(oldear != null)
					earDao.delete(oldear);
			}
		}
		catch (Exception e)
		{			
			logger.info("LoadMtl: Check Activity request list Existed exception, ex= "+ e.getMessage());	 
			return false;
		}
		  
		// new ARL name, create ARL
		try
		{
			// with generated ID
			arl = arlDao.create(arl);			
			logger.debug("LoadMtl: Arl created: " + arl.getId());
		}
		catch (Exception e)
		{			
			logger.info("create Arl exception , ex="+e.getMessage());
			return false;
		}
		  
	    // create Externalactivityrequestinfo from MTL file header
		// SOURCE:         IPS PSS
		// DESTINATION:    MMC MCC
		// FILE NAME:      RS2_RSITimeline_20090602_A1.rpt
		// DATE TIME:      2009/06/01 03:08:19
		// SPACECRAFT:     ROCSAT-2
		// INSTRUMENT:     RSI
		// REQUEST WINDOW:         2009/153 01:39:11 - 2009/153 01:53:02
		// ORBIT    TImagT
		// 25740       2.0
	    Externalactivityrequestinfo ear = new Externalactivityrequestinfo();
        ear.setArlid(arl.getId().intValue());
        ear.setBranchid(isBranch ? sessionId : 0);
        // SPACECRAFT:
        String spacecraft;
        spacecraft = mtlParser.getSpacecraft().replace("-","");
        ear.setSatellite(spacecraft);
        // INSTRUMENT:
        ear.setInstrument(mtlParser.getInstrument());
        // REQUEST WINDOW: 
        ear.setStarttime(new Timestamp(mtlParser.getRequestwindowStarttime().getTime()));
        ear.setEndtime(new Timestamp(mtlParser.getRequestwindowEndtime().getTime()));
		
		// create new Externalactivityrequestinfo entry
		try
		{
			// with generated ID
			ear = earDao.create(ear);			
			logger.debug("LoadMtl: Externalactivityrequestinfo created: " + ear.getId());
		}
		catch (Exception e)
		{			
			logger.info("create Externalactivityrequestinfo exception , ex ="+e.getMessage());
			return false;
		}

		  
		// create mission time lines
		if (mtlParser.getInstrument().equalsIgnoreCase("RSI"))
		{
			if(!createRSIMTLs(arl)){				
				logger.debug("create RSI failed.");
				returnMsg = (returnMsg.length() > 0) ? returnMsg : "create RSI failed.";
				return false;
			}
			
		}
		else if (mtlParser.getInstrument().equalsIgnoreCase("AIP"))
		{
						
			if (!createPROCMTLs(arl))
			{				
				logger.debug("createPROCMTLs failed.");
				returnMsg = (returnMsg.length() > 0) ? returnMsg : "createPROCMTLs failed.";
				return false;
			}
		}
		
		return true;
	}
	
	public boolean createRSIMTLs(Arl arl){
		return true;
	}
	
	public boolean createPROCMTLs(Arl arl){
		return true;
	}
	
	public boolean checkAIPPROCMTLs(){
		return true;
	}
	
	
  
	@SuppressWarnings({ "rawtypes", "unused" })
	public void genXcrossEvent(int arlId)
	{
		int nominalorbit = satelliteDao.get((long) scId).getNominalorbit();
		TimeRange TR = new TimeRange(new Timestamp(mtlParser.getRequestwindowStarttime().getTime()), new Timestamp(mtlParser.getRequestwindowEndtime().getTime()));
		TimeRange newTR = RangeUtil.rangeAppendSec(TR, - nominalorbit, nominalorbit);
		
		Timestamp STime = sessionDao.get((long) sessionId).getStarttime();
		Timestamp ETime = sessionDao.get((long) sessionId).getEndtime();
		
		//Mapping Unavailtimerang
		List<Unavailtimerange> unavailTR = unavailableResourcesDao.getSitesUnavailableTimes(STime, ETime, 0);
		Map<Integer, Set<Range> > unTRMap = new HashMap<Integer, Set<Range> >();
		for (Unavailtimerange tr : unavailTR)
		 {
			 if (unTRMap.get(tr.getParentid()) == null)
        	 {
				 unTRMap.put(tr.getParentid(), new HashSet<Range>());
        	 }
			 Set<Range> trList = unTRMap.get(tr.getParentid());
			 try
        	{
        		//System.out.println("site id:" + tr.getParentid());
        		trList.add(Range.closed(tr.getStarttime(), tr.getEndtime()));
        	}
        	catch (Exception e)
        	{        	   
        	    logger.info("Can not add Unavailable TimeRange Map (" + e.getMessage() + ")");
        	}
		 }

		 //Mapping Contact
		 //Get X-Band ClContact information during the session's time range
		 List<Contact> cons = contactDao.getBySiteTypeNSatNBranchNRange("XBAND",scId, (isBranch ? sessionId : 0), STime, ETime, sessionId);
		 //List<Contact> cons = contactDao.getXbandBySatNBranchNTimeRange(1, 0, STime, ETime, sessionId);
		 Map<Integer, Set<Contact> > contactListMap = new HashMap<Integer, Set<Contact> >();
		 for (Contact cn : cons)
		 {
			 if (contactListMap.get(cn.getSiteid()) == null)
        	 {
				 contactListMap.put(cn.getSiteid(), new HashSet<Contact>());
        	 }
			 Set<Contact> consList = contactListMap.get(cn.getSiteid());
			 try
			 {
        		//System.out.println("site id:" + cn.getSiteid());
        		consList.add(cn);
			 }
			 catch (Exception e)
			 {
				logger.info("Can not add contact Map (" + e.getMessage() + ")");        	    
			 }
		 }
		//Print Contact
		 for (Integer id : contactListMap.keySet())
		 {
			 System.out.println("contact Site Id : [" + id + "] " + contactListMap.get(id).size() + " Rows");
			 for (Contact c : contactListMap.get(id))
			 {				 
				 logger.debug("Contact : " + c.getRise() + " ~ " + c.getFade());  
			 }
		 }
		 
		 logger.info("Total : " + contactListMap.keySet().size() + " Site Rows " + cons.size() + " Contact Rows ");  
		 
		 //Remove the contact that is unavailable
		//Filter Contact
		 List<Contact> filterCons = new ArrayList<Contact>();
		 for (Integer id : contactListMap.keySet())
		 {
			 if (!unTRMap.keySet().contains(id))
			 {
//				 System.out.println("contact Site Id : [" + id + "] " + contactListMap.get(id).size() + " Rows not in Unavail Timerange");
//				 for (Contact c : contactListMap.get(id))
//				 {
//					System.out.println("Contact : " + c.getRise() + " ~ " + c.getFade() + " -- Avail");
//				 }
			 }
			 else
			 {
				 System.out.println("contact Site Id : [" + id + "] " + contactListMap.get(id).size() + " Rows in Unavail Timerange");
				 for (Contact c : contactListMap.get(id))
				 {
					 System.out.println("Check Contact : " + c.getRise() + " ~ " + c.getFade());
					 for (Range rg : unTRMap.get(id))
					 {
						 System.out.println("Check Unavail Timerange : " + rg.lowerEndpoint() + " ~ " + rg.upperEndpoint());
						 // modify by jeff 20171109 , contact has Unavail time can't add xcross ==> contact has "full" Unavail time can't add xcross
						 //if(c.getRise().compareTo((Timestamp) rg.upperEndpoint()) <= 0 && c.getFade().compareTo((Timestamp) rg.lowerEndpoint()) >= 0)
						 if(TimeUtil.compareTimestamp(c.getRise(), (Timestamp) rg.lowerEndpoint()) && TimeUtil.compareTimestamp(c.getFade(),(Timestamp) rg.upperEndpoint()))
						 {
							 System.out.println("Unavail Timerange -- Remove");
							 cons.remove(c);
							 break;
						 }
						 else
						 {
//							 System.out.println(" -- Avail");
						 }
					 }
				 }
			 }
		 }
		 
		 
		 logger.info("Filter Contact : " + cons.size() + " Rows");  
		 for (Contact cn : cons)
		 {
			 if (newTR.contains(cn.getRise()))
			 {				 
				 logger.info("Match Contact : [" + cn.getId() + "]" + cn.getRise() + " ~ " + cn.getFade());  
				 //Create XCross event
				 Xcrossrequest xcross = createXcrossEvent(arlId, cn);
				 
				 if(xcross!=null){
					//create Activity entries
					 Activity act = new Activity();
					 act.setName("XCROSS");
			    	 act.setArlentrytype("XCROSS");
					 act.setValid(1);
					
					act.setDuration(xcross.getDuration());
					act.setStarttime(YearDayTimeOffset.toTimestamp(xcross.getYear(), xcross.getDay(), xcross.getStarttime()));
					act.setEndtime(YearDayTimeOffset.toTimestamp(xcross.getYear(), xcross.getDay(), xcross.getStarttime() + xcross.getDuration()));
					act.setSatelliteid(scId);
					act.setArlentryid(xcross.getId());
					act.setActsubtype("NO");
					act.setToytype("NO");
					act.setSessionid(sessionId);
					act.setExtentid(0);
					act.setScheduled(0);
					act.setPrevactid(0);
					act.setNextactid(0);
					act.setUsermodified(0);
			      
					// create Activity entries
					act = createActivity(act);
				 }
				 
			 }
		 }
	}
  
	public Xcrossrequest createXcrossEvent(int arlId, Contact cn)
	{
		 //Create XCross event		 
		 logger.info("Match Contact : [" + cn.getId() + "]" + cn.getRise() + " ~ " + cn.getFade());  
		 
		 // The example of the XCROSS event:
		 //91     4     2009/021 20:20:14   7    NA  NA  NA  NA  1    ANY  50.2       18.0          6126   NA   NA   NA   NA   NA
		 
		 //XCross StartTime
		 int xcross_minus = Integer.valueOf(System.getProperty("x_nspo.xcross.minus","-30"));
		 int xcross_plus = Integer.valueOf(System.getProperty("x_nspo.xcross.plus","30"));
		 Timestamp xcrsStartTime;
		 Calendar cal = Calendar.getInstance();
	     cal.setTimeInMillis(cn.getMaxeltime().getTime());
		 if (siteDao.getSiteById((long) cn.getSiteid()).getName().equalsIgnoreCase("X_NSPO"))
		 {
			 if (cal.get(Calendar.HOUR_OF_DAY) < 4)
			 {
				 xcrsStartTime =  new Timestamp (cn.getMaxeltime().getTime() + xcross_minus*1000);
			 }
			 else
			 {
				 xcrsStartTime =  new Timestamp (cn.getMaxeltime().getTime() + xcross_plus*1000);
			 }
		 }
		 else
		 {
			 xcrsStartTime = cn.getMaxeltime();
		 }
		 
		 Xcrossrequest xcrossReq = new Xcrossrequest();
		 if (actDao.getActBySessionIdNStartTime(sessionId, xcrsStartTime).size() > 0)
		 {
			 logger.info("This XCROSS event is a redundant one.  No need to create this XCROSS event.");			 
			 xcrossReq = null;
		 }
		 else
		 {
			 int xBandSwitchDelay = satpDao.getBySatIdNPsId(scId, sessionDao.get((long) sessionId).getPsid()).getXbandswitchdelay();
			 int revNo = revDao.get((long) cn.getRevid()).getRevno();
			 
			 xcrossReq.setArlid(arlId);
			 YearDayTimeOffset convert = new YearDayTimeOffset(new Timestamp(xcrsStartTime.getTime()));
			 xcrossReq.setYear(convert.getYear());
			 xcrossReq.setDay(convert.getDayOfYear());
			 xcrossReq.setStarttime(convert.getSecondsOfDay());
			 xcrossReq.setDuration(xBandSwitchDelay);
		 	 xcrossReq.setStation("ANY");
			 xcrossReq.setElevation(cn.getMaxel().toString());
			 xcrossReq.setAzimuth("18");
			 xcrossReq.setOrbit(revNo);
			 xcrossReq.setContactid(cn.getId().intValue());

		    try
		    {
		    	xcrossReq = xcrossReqDao.create(xcrossReq);
		    	logger.info("LoadMtl: Xcrossrequest created: " + xcrossReq.getId());
		        
		    }
		    catch (Exception e)
		    {		    	
		    	logger.warn("create Xcrossrequest failed , ex="+e.getMessage());
		    	xcrossReq = null;
		    }
		 }
		return xcrossReq;
	}
	
	public Activity createActivity(Activity act)
	{	
		try
		{
			act = actDao.create(act);
			this.actIdList.add(act.getId());
			logger.info("LoadMtl: Activity created: " + act.getId());
		}
		catch (Exception e)
		{
			logger.warn("LoadMtl: Create Activity failed , ex="+e.getMessage());
			act = null;
		}
		
		return act;
	}

	public String extractFileName( String filePathName )
  {
    if ( filePathName == null )
      return null;

    int dotPos = filePathName.lastIndexOf( '.' );
    int slashPos = filePathName.lastIndexOf( '\\' );
    if ( slashPos == -1 )
      slashPos = filePathName.lastIndexOf( '/' );

    if ( dotPos > slashPos )
    {
      return filePathName.substring( slashPos > 0 ? slashPos + 1 : 0,
          dotPos );
    }

    return filePathName.substring( slashPos > 0 ? slashPos + 1 : 0 );
  }
  
	public String extractFilePath( String filePathName )
	{
		if ( filePathName == null )
			return null;

	    int slashPos = filePathName.lastIndexOf( '\\' );
	    if ( slashPos == -1 )
	    slashPos = filePathName.lastIndexOf( '/' );

	    return filePathName.substring( 0, slashPos > 0 ? slashPos + 1 : 1);
	}

  	//Creating RSI PBK and DELETE activities
	public void createPbkDelActivity(Activity act, int entryid)
	{
	    act.setNextactid(act.getId().intValue() + 1 );
	    actDao.update(act);
	    
	    // if the REC is performing PAN+MS generate 2 playback activities,1 for PAN 1 for MS
		List<Activity> actList = new ArrayList<Activity>();
		String mode = act.getInfo();		
		
		int i = 0;
		if (mode.equals("PAN+MS"))
		{
			for (i = 0 ; i<2 ;i++)
			{
				mode = (i==0 ? "PAN" : "MS");
				Activity pbk = new Activity();
				
				//Calculate Duration
			    int pbkDur = genPbkDur(entryid, mode);
				
				pbk.setName("RSI_PBK:" + mode);
				pbk.setActsubtype("PBK");
				pbk.setArlentrytype("RSI");
				pbk.setInfo(mode);

				pbk.setDuration(pbkDur);
				pbk.setStarttime(act.getEndtime());
				pbk.setEndtime(new Timestamp(act.getEndtime().getTime() + 24*60*60*1000));
				pbk.setSatelliteid(scId);
				pbk.setArlentryid(entryid);
				pbk.setToytype("NO");
				pbk.setValid(1);
				
				pbk.setSessionid(sessionId);
				pbk.setExtentid(0);
				pbk.setScheduled(0);
				pbk.setPrevactid(act.getId().intValue() + i);
				pbk.setNextactid(act.getId().intValue() + i + 2);
				pbk.setUsermodified(0);
				
				actList.add(pbk);
			}
		}
		else
		{
			i=1;
			Activity pbk = new Activity();
			
			//Calculate Duration
		    int pbkDur = genPbkDur(entryid, mode);
			
			pbk.setName("RSI_PBK:" + mode);
			pbk.setActsubtype("PBK");
			pbk.setArlentrytype("RSI");
			pbk.setInfo(act.getInfo());
			pbk.setDuration(pbkDur);
			pbk.setStarttime(act.getEndtime());
			
			pbk.setEndtime(new Timestamp(act.getEndtime().getTime() + 24*60*60*1000));
	        
			pbk.setSatelliteid(scId);
			pbk.setArlentryid(entryid);
			pbk.setToytype("NO");
			pbk.setValid(1);
			
			pbk.setSessionid(sessionId);
			pbk.setExtentid(0);
			pbk.setScheduled(0);
			pbk.setPrevactid(act.getId().intValue());
			pbk.setNextactid(act.getId().intValue() + 2);
			pbk.setUsermodified(0);
			
			actList.add(pbk);
		}
		
		// generate delete activity
		Activity del = new Activity();
		
		del.setName("DELETE");
		del.setActsubtype("NO");
		del.setArlentrytype("DELETE");
		//del.setInfo("");

		del.setDuration(1);
		del.setStarttime(new Timestamp(act.getEndtime().getTime() + 1000));
		del.setEndtime(Timestamp.valueOf("2030-01-01 00:00:00"));
		del.setSatelliteid(scId);
		del.setArlentryid(entryid);
		del.setToytype("NO");
		del.setValid(1);
		
		del.setSessionid(sessionId);
		del.setExtentid(0);
		del.setScheduled(0);
		del.setPrevactid(act.getId().intValue() + i);
		del.setNextactid(0);
		del.setUsermodified(0);
		
		actList.add(del);
		
		for(Activity ac : actList)
		{
			createActivity(ac);
		}
	}

	public int genPbkDur(int entryid, String mode)
	{		
			return 0;	
	}
	
	public boolean checkGain(MTLRecord mr){
		return true;
	}
	
	public void checkRSI(String scName,List<Integer> asyncIndex,List<Integer> syncIndex){
		
	}

}

