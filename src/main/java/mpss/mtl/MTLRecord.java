package mpss.mtl;

import java.util.Date;

public class MTLRecord 
{
	//RS2
	public int event;
	public int i_ev;
	public Date utc;
	public int dur;
	public Double q1;
	public Double q2;
	public Double q3;
	public Double q4;
	public int mode;
	public String station;
	public String elev;
	public String azim;
	public int orbit;
	public String gi;
	public String gj_s;
	public String gj_e;
	public String gk;
	public double lat_s;
	public double lat_e;
	public Integer type;
	public String ssr_status;
	public String pancompressionratio;
	public String mscompressionratio;
	public String videopangain;
	public String videomb1gain;
	public String videomb2gain;
	public String videomb3gain;
	public String videomb4gain;

	//RS5
	public String imagine_mode;//“0” for synchronous imaging, “1” for asynchronous imaging
	public Double y_rate;
	public Double p_rate;
	public Double r_rate;
	public Double pan_line_compressionratio;
	public Double ms_line_compressionratio;
	
	// add 9 parameters by jeff 
	public String ratefit1_1;
	public String ratefit2_1;
	public String ratefit3_1;
	public String ratefit1_2;
	public String ratefit2_2;
	public String ratefit3_2;
	public String ratefit1_3;
	public String ratefit2_3;
	public String ratefit3_3;

	// ISUAL or AIP
	public String procedure;
	
	public String toRS5RSIString(){
		return event + ", " + i_ev + ", " + imagine_mode + ", " + utc + ", " +
				dur + ", " + q1 + ", " + q2 + ", " + q3 + ", " + q4 + ", " + y_rate +
				", " + p_rate + ", " + r_rate + ", " + mode + ", " + station + ", " + orbit +
				", " + ssr_status + ", " + pancompressionratio + ", " + mscompressionratio + ", " +
				pan_line_compressionratio + ", " + ms_line_compressionratio + ", " + videopangain +
				", " + videomb1gain + ", " + videomb2gain + ", " + videomb3gain + ", " + videomb4gain;
	}
	
	public String toAPIString(){
		return event + ", " + utc + ", " +
				dur + ", " + q1 + ", " + q2 + ", " + q3 + ", " + q4 + ", " + procedure;
	}
	
}
