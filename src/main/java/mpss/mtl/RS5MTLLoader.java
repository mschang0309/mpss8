package mpss.mtl;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.springframework.stereotype.Component;

import mpss.common.jpa.*;
import mpss.util.timeformat.*;

@Component("rs5MTLLoader")
public class RS5MTLLoader  extends MTLLoaderBase
{
	
	private String downlinlfile;
	private String errCmd;

	public RS5MTLLoader()
	{
		// default constructor must exist for Spring
	}
	
	
	@Override
	public boolean createRSIMTLs(Arl arl)
	{
		// check rec rate 
		for (MTLRecord mr : mtlParser.getMissionTimelines())
	    {
			if(mr.i_ev==3 && !mr.ssr_status.equals("DDT")){
				if(mr.pancompressionratio.equalsIgnoreCase("HIGH") || mr.pancompressionratio.equalsIgnoreCase("LOW") || mr.pancompressionratio.equalsIgnoreCase("NONE") || mr.pancompressionratio.equalsIgnoreCase("MEDIUM")){
					
				}else{
					returnMsg = "RSI PAN compression ratio is incorrect";
					return false;
				}
				
				if( mr.mscompressionratio.equalsIgnoreCase("HIGH") || mr.mscompressionratio.equalsIgnoreCase("LOW") || mr.mscompressionratio.equalsIgnoreCase("NONE") || mr.mscompressionratio.equalsIgnoreCase("MEDIUM")){
					
				}else {
					returnMsg = "RSI MS compression ratio is incorrect";
					return false;
				}
			}
			if(mr.i_ev==3 && mr.ssr_status.equals("DDT")){
				if( !mr.pancompressionratio.equalsIgnoreCase("HIGH") ){
					returnMsg = "DDT compression ratio is incorrect";
					return false;
				}else if (!mr.mscompressionratio.equalsIgnoreCase("HIGH") ){
					returnMsg = "DDT compression ratio is incorrect";
					return false;
				}
			}
	    }
		
		
		genXcrossEvent(arl.getId().intValue());
		
		for (MTLRecord mr : mtlParser.getMissionTimelines())
	    {
	    	Activity act = new Activity();
	    	TimeRange tr = RangeUtil.startTimeAppendSec(new Timestamp(mr.utc.getTime()), mr.dur);
	    	act.setStarttime(tr.first);
			act.setEndtime(tr.second);
			act.setDuration(mr.dur);
			act.setSatelliteid(scId);
			//act.setActsubtype("NO");
			act.setToytype("NO");
			act.setValid(1);
			act.setSessionid(sessionId);
			act.setExtentid(0);
			act.setScheduled(0);
			act.setPrevactid(0);
			act.setNextactid(0);
			act.setUsermodified(0);
			
	    	// create ARL entries
			switch (mr.i_ev)
			{
				case 1:
					act.setArlentrytype("MANEUVER");
					act.setName("GOHOME_Maneuver");
					act.setActsubtype("NO");
					
					if(!createSupEvent(arl, mr, act))
						return false;
					
					break;
					
				case 2:
					act.setArlentrytype("MANEUVER");
					act.setName("Attitude_Maneuver");
					act.setActsubtype("NO");
					
					if(!createManEvent(arl, mr, tr, "RSI", act))
						return false;
					
					break;
					
				case 3:	      		
					String mode = imgModeDao.getImagingModeByInstNModeid("RSI", mr.mode).getModeshort();
					if (mr.ssr_status.equals("DDT"))
					{
						act.setName("RSI_DDT");
						act.setActsubtype("DDT");
					}
					else
					{	
						act.setName("RSI_REC:" + mode);
						act.setActsubtype("REC");
					}
				    act.setArlentrytype("RSI");
					act.setInfo(mode);
					
					if(!createRsiEvent(arl, mr, act))
						return false;
					
					break;
					
				default :
					break;
			}
		
	    }		
		return true;
	}
	
	@Override
	public boolean checkAIPPROCMTLs()
	{
		returnMsg = null;
		
		//check procedure
		int firfileNum = 0;
		int secfileNum = 0;
		Timestamp AipMidtime = TimeUtil.timeAppendHour(sessionTR.first, 12);
		System.out.println("AipMidtime:" + AipMidtime);
		for (MTLRecord mr : mtlParser.getMissionTimelines())
	    {
			int AipDownlinkFileValue = getAipDownlinkFile(filePath + mr.procedure);
			
			if( AipDownlinkFileValue < 0)
			{
				return false;
			}
			//count AIP rec file between 12 hours
			else if (AipDownlinkFileValue == 1)
			{
				if (AipMidtime.after(mr.utc))
					firfileNum +=1;
				else
					secfileNum +=1;
			}
			
			if (errCmd.length() > 0)
			{
				returnMsg = mr.procedure + " [" + errCmd + "] Command mismatch";				
				logger.info(mr.procedure + " [" + errCmd + "] Command mismatch");
				return false;
			}
	    }
		// AIP rec file more than 1 between 12 hours
		if(firfileNum > 1 || secfileNum > 1)
		{
			returnMsg = "AIP Rec file more than 1 between 12 hours.";
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean createPROCMTLs(Arl arl)
	{
		TimeRange tr;
		//create AIP PROC
		for (MTLRecord mr : mtlParser.getMissionTimelines())
	    {
	    	Activity act = new Activity();
	    	tr = RangeUtil.startTimeAppendSec(new Timestamp(mr.utc.getTime()), mr.dur);
			act.setDuration(mr.dur);
			act.setStarttime(tr.first);
			act.setEndtime(tr.second);
			act.setSatelliteid(scId);
			act.setActsubtype("NO");
			act.setToytype("NO");
			act.setValid(1);
			act.setSessionid(sessionId);
			act.setExtentid(0);
			act.setScheduled(0);
			act.setPrevactid(0);
			act.setNextactid(0);
			act.setUsermodified(0);
	    	
			tr = RangeUtil.startTimeAppendSec(new Timestamp(mr.utc.getTime()), 60*60*24);
			if ((tr.second.getTime() > mtlParser.getRequestwindowEndtime().getTime()) && (tr.first.getTime() < mtlParser.getRequestwindowEndtime().getTime()))
			{
				tr.closed(tr.first , new Timestamp(mtlParser.getRequestwindowEndtime().getTime()));
			}
			int validProc = 1;
			act.setName("AIP_Proc");
			act.setArlentrytype("AIPPROC");
			act.setValid(validProc);
			
			if( getAipDownlinkFile(filePath + mr.procedure) > 0)
			{
				act.setInfo(downlinlfile);
			}
			
			// create AIP Proc
			if(!createAipProcEvent(arl, mr, tr, act))
				return false;
	    }
		
		// create AIP PBK
		createAipPbkEvent();
		
		// create AIP Extent
		createAIPExtent();
		
		return true;
	}

	private boolean createSupEvent(Arl arl, MTLRecord mr, Activity act)
	{
	    Maneuverrequest supReq = new Maneuverrequest();
	    supReq.setArlid(arl.getId().intValue());
	    YearDayTimeOffset convert = new YearDayTimeOffset(new Timestamp(mr.utc.getTime()));
	    supReq.setYear(convert.getYear());
	    supReq.setDay(convert.getDayOfYear());
	    supReq.setStarttime(convert.getSecondsOfDay());
	    supReq.setDuration(mr.dur);
	    supReq.setManeuverduration(mr.dur);
	    supReq.setAsyncflag(0);
	    if (!mr.q1.isNaN())
	    {
          supReq.setQ1(mr.q1.floatValue());
	    }
	    if (!mr.q2.isNaN())
	    {
          supReq.setQ2(mr.q2.floatValue());
	    }
	    if (!mr.q3.isNaN())
	    {
          supReq.setQ3(mr.q3.floatValue());
	    }
	    if (!mr.q4.isNaN())
	    {
          supReq.setQ4(mr.q4.floatValue());
	    }
	    supReq.setStation(mr.station);
	    supReq.setOrbit(mr.orbit);
	    // 0=Attitude, 1=SUP(GOHOME), 2=Orbit
	    supReq.setType("GOHOME");

	    // create Maneuverrequest entries
	    try
	    {
	    	supReq = manReqDao.create(supReq);
	    	logger.info("LoadMtl: Maneuverrequest (SUP) entry created: " + supReq.getId());
	    }
	    catch (Exception e)
	    {	    	
	    	logger.warn("LoadMtl: Create Maneuverrequest (SUP) entry failed,ex="+e.getMessage());
	    	return false;
	    }
	    
	    // create Activity entries
		act.setArlentryid(supReq.getId().intValue());
		if (createActivity(act) == null)
			return false;
        
		return true;
  }

	private boolean createManEvent(Arl arl, MTLRecord mr, TimeRange tr ,String type, Activity act)
	{
		Maneuverrequest manReq = new Maneuverrequest();
		YearDayTimeOffset convert = new YearDayTimeOffset(new Timestamp(mr.utc.getTime()));
		
//		Timestamp noOffset = Timestamp.valueOf(tr.first.toString().split(" ")[0] + " 00:00:00");
//    	Long offset = (tr.first.getTime() - noOffset.getTime())/1000;
    	
    	manReq.setArlid(arl.getId().intValue());
		manReq.setYear(convert.getYear());
		manReq.setDay(convert.getDayOfYear());
        manReq.setManeuverduration(mr.dur);
		if (!mr.q1.isNaN())
	    {
			manReq.setQ1(mr.q1.floatValue());
	    }
		if (!mr.q2.isNaN())
	    {
			manReq.setQ2(mr.q2.floatValue());
	    }
		if (!mr.q3.isNaN())
	    {
			manReq.setQ3(mr.q3.floatValue());
	    }
		if (!mr.q4.isNaN())
	    {
			manReq.setQ4(mr.q4.floatValue());
	    }
		
		if (mtlParser.getInstrument().equalsIgnoreCase("RSI"))
		{
			if (!mr.y_rate.isNaN())
		    {
				manReq.setYRate(mr.y_rate.floatValue());
		    }
			if (!mr.p_rate.isNaN())
		    {
				manReq.setPRate(mr.p_rate.floatValue());
		    }
			if (!mr.r_rate.isNaN())
		    {
				manReq.setRRate(mr.r_rate.floatValue());
		    }	
			
			if (!mr.ratefit1_1.isEmpty())
		    {
				manReq.setRatefit1_1(mr.ratefit1_1);
		    }
			
			if (!mr.ratefit1_2.isEmpty())
		    {
				manReq.setRatefit1_2(mr.ratefit1_2);
		    }
			
			if (!mr.ratefit1_3.isEmpty())
		    {
				manReq.setRatefit1_3(mr.ratefit1_3);
		    }
			
			if (!mr.ratefit2_1.isEmpty())
		    {
				manReq.setRatefit2_1(mr.ratefit2_1);
		    }
			
			if (!mr.ratefit2_2.isEmpty())
		    {
				manReq.setRatefit2_2(mr.ratefit2_2);
		    }
			
			if (!mr.ratefit2_3.isEmpty())
		    {
				manReq.setRatefit2_3(mr.ratefit2_3);
		    }
			
			if (!mr.ratefit3_1.isEmpty())
		    {
				manReq.setRatefit3_1(mr.ratefit3_1);
		    }
			
			if (!mr.ratefit3_2.isEmpty())
		    {
				manReq.setRatefit3_2(mr.ratefit3_2);
		    }
			
			if (!mr.ratefit3_3.isEmpty())
		    {
				manReq.setRatefit3_3(mr.ratefit3_3);
		    }
		}

		manReq.setStarttime(convert.getSecondsOfDay());
		manReq.setDuration(mr.dur);
		manReq.setStation(mr.station);
		manReq.setOrbit(mr.orbit);
		manReq.setType("ATTITUDE");
      
		if (mr.imagine_mode.equalsIgnoreCase("1"))
			manReq.setAsyncflag(1);
		else
			manReq.setAsyncflag(0);
      
		try
	    {
	    	manReq = manReqDao.create(manReq);
	        logger.info("LoadMtl: Maneuverrequest (MAN) entry created: " + manReq.getId());
	    }
	    catch (Exception e)
	    {
	    	logger.warn("LoadMtl: Create Maneuverrequest (MAN) entry failed,ex="+ e.getMessage());
	    	return false;
	    }
		
		// create Activity entries
		act.setArlentryid(manReq.getId().intValue());
		if (createActivity(act) == null)
			return false;
        
		return true;
	    
	}

	private boolean createRsiEvent(Arl arl, MTLRecord mr, Activity act)
	{
		Rsiimagingrequest rsiReq = new Rsiimagingrequest();
		rsiReq.setArlid(arl.getId().intValue());
		YearDayTimeOffset convert = new YearDayTimeOffset(new Timestamp(mr.utc.getTime()));
		rsiReq.setYear(convert.getYear());
		rsiReq.setDay(convert.getDayOfYear());
		rsiReq.setStarttime(convert.getSecondsOfDay());
		rsiReq.setDuration(mr.dur);
		rsiReq.setImagingmode(mr.mode); // 3
		rsiReq.setStation(mr.station);
		rsiReq.setOrbit(mr.orbit);
		rsiReq.setSsrstatus(mr.ssr_status); // REC
		rsiReq.setPancompressionratio(mr.pancompressionratio.toUpperCase()); // HIGH
		rsiReq.setMscompressionratio(mr.mscompressionratio.toUpperCase()); // HIGH
		rsiReq.setPanlinecompressionratio(mr.pan_line_compressionratio.floatValue());
		rsiReq.setMslinecompressionratio(mr.ms_line_compressionratio.floatValue());
		rsiReq.setVideopangain(mr.videopangain.replace("G", "GAIN")); // G1/G2/G4-->GAIN1/GAIN2/GAIN4
		rsiReq.setVideomb1gain(mr.videomb1gain.replace("G", "GAIN")); // G1/G2/G4-->GAIN1/GAIN2/GAIN4
		rsiReq.setVideomb2gain(mr.videomb2gain.replace("G", "GAIN")); // G1/G2/G4-->GAIN1/GAIN2/GAIN4
		rsiReq.setVideomb3gain(mr.videomb3gain.replace("G", "GAIN")); // G1/G2/G4-->GAIN1/GAIN2/GAIN4
		rsiReq.setVideomb4gain(mr.videomb4gain.replace("G", "GAIN")); // G1/G2/G4-->GAIN1/GAIN2/GAIN4
		rsiReq.setTrackGi("0");
		rsiReq.setGridJe("0");
		rsiReq.setGridJs("0");
		rsiReq.setGridK("0");

	    try
	    {
	    	rsiReq = rsiReqDao.create(rsiReq);
	        logger.info("LoadMtl: Rsiimagingrequest entry created: " + rsiReq.getId());
	    }
	    catch (Exception e)
	    {
	    	logger.info("LoadMtl: Create Rsiimagingrequest entry failed ,ex="+e.getMessage());
	    	return false;
	    }
	    
	    // create Activity entries
		act.setArlentryid(rsiReq.getId().intValue());
		if (createActivity(act) == null)
			return false;
        
		//Creating RSI PBK and DELETE activities
		if(act.getActsubtype().equalsIgnoreCase("REC"))
		{
			logger.info("LoadMtl: Creating RSI PBK and DELETE activities.");
			createPbkDelActivity(act, rsiReq.getId().intValue());	
		}
		return true;
		
	}

	private boolean createAipProcEvent(Arl arl, MTLRecord mr, TimeRange tr, Activity act)
	{
		returnMsg = "";
		Aipprocrequest aipReq = new Aipprocrequest();
		YearDayTimeOffset convert = new YearDayTimeOffset(new Timestamp(mr.utc.getTime()));
		
		Timestamp noOffset = Timestamp.valueOf(tr.first.toString().split(" ")[0] + " 00:00:00");
		Long offset = (tr.first.getTime() - noOffset.getTime())/1000;
  	
		aipReq.setArlid(arl.getId().intValue());
		aipReq.setYear(convert.getYear());
		aipReq.setDay(convert.getDayOfYear());
		aipReq.setStarttime(offset.intValue());
		aipReq.setDuration(mr.dur);
		aipReq.setAipproc(System.getProperty("dataFileDir") + System.getProperty("mtlAIPFileDir") + mr.procedure);
		aipReq.setComments(errCmd);
		//aipReq.setComments("AIP Proc from file");

	    try
	    {
	    	aipReq = aipReqDao.create(aipReq);
	        logger.info("LoadMtl: Aipprocrequest created: " + aipReq.getId());
	    }
	    catch (Exception e)
	    {
	    	logger.warn("create Aipprocrequest failed,ex="+e.getMessage());
	    	returnMsg = "create Aipprocrequest failed:" + e.getMessage();
	    	return false;
	    }
	    
		// create Activity entries
		act.setArlentryid(aipReq.getId().intValue());
		if (createActivity(act) == null)
		{
			logger.info("create AIP PROC Activity failed.");
	    	returnMsg = "create AIP PROC Activity failed.";
			return false;
		}
		
		return true;
	}
	
	private boolean createAIPExtent()
	{
		List<Activity> acts = actDao.getAIPBySession(sessionId);
		for (Activity act : acts)
		{
			Extent ext = new Extent();
			
			ext.setActivityid(act.getId().intValue());
			ext.setStarttime(act.getStarttime());
			ext.setEndtime(act.getEndtime());
			if (act.getArlentrytype().equalsIgnoreCase("AIPPBK"))
				ext.setContactid(act.getExtentid());
			else
				ext.setContactid(0);
			ext.setBranchid(0);
			ext.setTransmitterid(0);
			ext.setRecorderid(0);
			ext.setAntennaid(0);
			ext.setRecorderdelta(0);
			ext.setSecondrecorderdelta(0);
			ext.setFilename(0);
			ext.setSecondfilename(0);
			ext.setUsermodified(0);
			ext.setUserlocked(0);
			ext.setSoftconstraintviolated(0);
			ext.setHardconstraintviolated(0);
			ext.setIsaip(1);
			
			try
			{
				ext = exeDao.create(ext);
				
				act.setExtentid(ext.getId().intValue());
				act.setScheduled(1);
				actDao.update(act);
				logger.info("LoadAIPMtl: Extent created: " + ext.getId());
			}
			catch (Exception e)
			{
				logger.info("LoadAIPMtl: Create Extent failed,ex="+ e.getMessage());
				return false;
			}
		}
		
		return true;
	}
	
	private int getAipDownlinkFile(String procfileName)
	{
		int returnValue = 0;
		downlinlfile="";
		errCmd = "";
		try 
		{
		    File file = new File(procfileName);
		    Scanner input = new Scanner(file);
		    while (input.hasNext())
	        {
		    	String nextLine = input.nextLine();
		    	
		    	//check AIP procedure command
		    	int indexComment = nextLine.indexOf("#");
		    	int indexCmd = nextLine.indexOf(" CMD ");
		    	int indexMPQ = nextLine.indexOf("MPQ");
		    	if (indexMPQ > -1 && indexCmd > -1 && (indexComment == -1 || indexCmd < indexComment))
		    	{
		    		String cmd = nextLine.substring(indexCmd + 4).trim().split(" ")[0];
			    	errCmd += checkAipCommand(cmd);	
		    	}
		    	
		    	
	    		if(nextLine.indexOf("DCDSSTOPREC") > -1)
	    		{
	    			downlinlfile = nextLine.split("'")[1] + ",";
	    		}
	        }
			
		    if (downlinlfile.length()>0)
		    {
		    	downlinlfile = downlinlfile.substring(0, downlinlfile.length()-1);
    			returnValue = 1;
		    }
		    else
		    	returnValue = 0;
		    	 
			input.close();
		} 
		catch (FileNotFoundException e) {
			returnMsg = "Open AIP procedure file failed (%s)" + e.getMessage();
			logger.info("Open AIP procedure file failed,ex="+ e.getMessage());
			return -1;
	    }
		catch (Exception ex) {
			returnMsg =  ex.getMessage();
			return -1;
	    }
		
		return returnValue;
	}
	
	private String checkAipCommand(String cmd)
	{
		String errorCmd = "";
		try 
		{
			if (aipcmdDao.getByCmd(cmd) == null)
				errorCmd = "," + cmd;
		} 
		catch (Exception ex) {
			logger.warn("AIP procedure command mapping failed ,ex="+ ex.getMessage());
	    }
		
		if (errorCmd.length() > 0)
			errorCmd = errorCmd.substring(1);
		
		return errorCmd;
	}
	
	private boolean createAipPbkEvent()
	{
//		int brId = isBranch ? sessionId : 0;
//		List<Contact> contacts = contactDao.getBySessionNAIP(brId, sessionTR.first, sessionTR.second);
		List<Contact> contacts = contactDao.getBySessionExtent(sessionId);
		
		List<Activity> acts = initAct();
		Timestamp session_middletime = TimeUtil.timeAppendHour(sessionTR.first, 12);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for(Contact contact :contacts){			
			if(contact.getRise().before(session_middletime)){
				if(contact.getMaxel()>acts.get(0).getDuration()){
					acts.get(0).setDuration(contact.getMaxel());
					acts.get(0).setStarttime(Timestamp.valueOf(df.format(contact.getCmdrise())));
					acts.get(0).setEndtime(Timestamp.valueOf(df.format(contact.getCmdfade())));
					acts.get(0).setSatelliteid(contact.getSatelliteid());
					acts.get(0).setSessionid(sessionId);
					acts.get(0).setExtentid(contact.getId().intValue());
					acts.get(0).setScheduled(0);
					acts.get(0).setArlentrytype("AIPPBK");
					acts.get(0).setArlentryid(0);
					acts.get(0).setName("AIP_PBK");
					acts.get(0).setActsubtype("NO");
					acts.get(0).setToytype("NO");
					acts.get(0).setPrevactid(0);
					acts.get(0).setNextactid(0);
					acts.get(0).setUsermodified(0);
					acts.get(0).setValid(1);
					acts.get(0).setInfo("");
				}
			}else{
				if(contact.getMaxel()>acts.get(1).getDuration()){
					acts.get(1).setDuration(contact.getMaxel());
					acts.get(1).setStarttime(Timestamp.valueOf(df.format(contact.getCmdrise())));
					acts.get(1).setEndtime(Timestamp.valueOf(df.format(contact.getCmdfade())));
					acts.get(1).setSatelliteid(contact.getSatelliteid());
					acts.get(1).setSessionid(sessionId);
					acts.get(1).setExtentid(contact.getId().intValue());
					acts.get(1).setScheduled(0);
					acts.get(1).setArlentrytype("AIPPBK");
					acts.get(1).setArlentryid(0);
					acts.get(1).setName("AIP_PBK");
					acts.get(1).setActsubtype("NO");
					acts.get(1).setToytype("NO");
					acts.get(1).setPrevactid(0);
					acts.get(1).setNextactid(0);
					acts.get(1).setUsermodified(0);
					acts.get(1).setValid(1);
					acts.get(1).setInfo("");
					
				}
			}
		}
		
		// if no downlink file , not create AIPPBK activity
		List<Activity> pbkActs = new ArrayList<Activity>();
		int i = 0;
		for(String fName : getAIPDownlinkFile())
		{
			if (fName.length() > 0)
			{
				acts.get(i).setArlentryid(Integer.parseInt(fName.split(",")[0]));
				acts.get(i).setInfo(fName.split(",")[1]);
				pbkActs.add(acts.get(i));
			}
			i++;
		}

		for (Activity pbkAct : pbkActs)
		{
			if (createActivity(pbkAct) == null)
				return false;
		}
		
		
		return true;
	}
	
	private List<Activity> initAct()
	{
		List<Activity> acts = new ArrayList<Activity>();
		Activity act1 = new Activity();
		act1.setDuration(0);
		Activity act2 = new Activity();	
		act2.setDuration(0);
		acts.add(act1);
		acts.add(act2);		
		return acts;
	}
	
	private List<String> getAIPDownlinkFile()
	{
		List<String> entryIdandName = new ArrayList<String>();
		Timestamp AipDLStartTime = TimeUtil.timeAppendHour(sessionTR.first, -12);
		Timestamp AipDLEndTime = TimeUtil.timeAppendHour(sessionTR.first, 12);
		List<Activity> AIPDL1 = actDao.getAIPDownlinkFile(AipDLStartTime, sessionTR.first);
		List<Activity> AIPDL2 = actDao.getAIPDownlinkFile(sessionTR.first,AipDLEndTime);
		String file1="";
		String file2="";
		if (AIPDL1.size() > 0)
			file1 = AIPDL1.get(0).getArlentryid() + "," + AIPDL1.get(0).getInfo();
		if (AIPDL2.size() > 0)
			file2 = AIPDL2.get(0).getArlentryid() + "," + AIPDL2.get(0).getInfo();
		
		entryIdandName.add(file1);
		entryIdandName.add(file2);
		
		return entryIdandName;
	}
	
	public int genPbkDur(int entryid, String mode){
		Rsiimagingrequest rsi = rsiReqDao.get((long)entryid);
		
		// look at arlEntry and determine which compression ratio to use
        // if selected DEFAULT/LOW then use normal MS data rate.
        // If selected HIGH then use MS ratio from PAN+MS rate
		int pbkDur = 0;
		int fsm = 0;
		double odr = 1;
		Recorderp recp = recpDao.getBySessionAndSat(sessionId, scId);		
		
		//for RS5
		double pan1 = 0;
		double pan1_5 = 0;
		double pan3_75 = 0;
		double pan7_5 = 0;
		double ms1 = 0;
		double ms1_5 = 0;
		double ms3_75 = 0;
		double ms7_5 = 0;
		
		if(recp != null)
		{
			ms1 = recp.getMs1();
			ms1_5 = recp.getMs1_5();
			ms3_75 = recp.getMs3_75();
			ms7_5 = recp.getMs7_5();
			pan1 = recp.getPan1();
			pan1_5 = recp.getPan1_5();
			pan3_75 = recp.getPan3_75();
			pan7_5 = recp.getPan7_5();
			fsm = recp.getFilesectormargin();
			odr = recp.getOutputdatarate();
		}
		else
		{
			return 0;
		}
		
		if (mode.equalsIgnoreCase("MS"))
		{
			switch (rsi.getMscompressionratio())
			{
				case "NONE":
					pbkDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * ms1) + fsm) / odr));
					break;
				case "LOW":
					pbkDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * ms1_5) + fsm) / odr));
					break;
				case "MEDIUM":
					pbkDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * ms3_75) + fsm) / odr));
					break;
				case "HIGH":
					pbkDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * ms7_5) + fsm) / odr));
					break;
			}
		}
		else if (mode.equalsIgnoreCase("PAN"))
		{
			switch (rsi.getPancompressionratio())
			{
				case "NONE":
					pbkDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * pan1) + fsm) / odr));
					break;
				case "LOW":
					pbkDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * pan1_5) + fsm) / odr));
					break;
				case "MEDIUM":
					pbkDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * pan3_75) + fsm) / odr));
					break;
				case "HIGH":
					pbkDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * pan7_5) + fsm) / odr));
					break;
			}
		}
		
		return pbkDur;		
	}
	
	public boolean checkGain(MTLRecord mr){
		if(!mr.videopangain.equalsIgnoreCase("G1") && !mr.videopangain.equalsIgnoreCase("G2") && !mr.videopangain.equalsIgnoreCase("G4"))
		{
			returnMsg = "event " + mr.event + " PAN_GAIN [" + mr.videopangain + "] is invalid.";
    		return false;
		}
		if(!mr.videomb1gain.equalsIgnoreCase("G1") && !mr.videomb1gain.equalsIgnoreCase("G2") && !mr.videomb1gain.equalsIgnoreCase("G4"))
		{
			returnMsg = "event " + mr.event + " MS1_GAIN [" + mr.videomb1gain + "] is invalid.";
    		return false;
		}
		if(!mr.videomb2gain.equalsIgnoreCase("G1") && !mr.videomb2gain.equalsIgnoreCase("G2") && !mr.videomb2gain.equalsIgnoreCase("G4"))
		{
			returnMsg = "event " + mr.event + " MS2_GAIN [" + mr.videomb2gain + "] is invalid.";
    		return false;
		}
		if(!mr.videomb3gain.equalsIgnoreCase("G1") && !mr.videomb3gain.equalsIgnoreCase("G2") && !mr.videomb3gain.equalsIgnoreCase("G4"))
		{
			returnMsg = "event " + mr.event + " MS3_GAIN [" + mr.videomb3gain + "] is invalid.";
    		return false;
		}
		if(!mr.videomb4gain.equalsIgnoreCase("G1") && !mr.videomb4gain.equalsIgnoreCase("G2") && !mr.videomb4gain.equalsIgnoreCase("G4"))
		{
			returnMsg = "event " + mr.event + " MS4_GAIN [" + mr.videomb4gain + "] is invalid.";
    		return false;
		}
		
		//DDT must be high compression
		if(mr.ssr_status.equalsIgnoreCase("DDT") && (!mr.pancompressionratio.equalsIgnoreCase("HIGH") || !mr.mscompressionratio.equalsIgnoreCase("HIGH")) )
		{
			returnMsg = "event " + mr.event + " DDT must be HIGH compression.";
    		return false;
		}
		return true;
	}
	
	
	public void checkRSI(String scName,List<Integer> asyncIndex,List<Integer> syncIndex){
		for (int idx : asyncIndex){
    		
			Satellite sat = satelliteDao.getByName(scName);
    		if (idx == 0)
    		{
    			needConfirm = true;
    			confirmMsg += "The first event for REC is invalid. <br>";    			
    		}else{
    			// check preMr
    			MTLRecord preMr = mtlParser.getMissionTimelines().get(idx - 1);
    			if (preMr.i_ev != 2){
    				
	    			needConfirm = true;
	    			confirmMsg += "event " + idx + " must be Attitude Maneuver(I_EV=2) <br>";	    			
	    		}
	    			
    			if (preMr.p_rate.equals(0) && preMr.r_rate.equals(0) && preMr.y_rate.equals(0)){
	    				needConfirm = true;
		    			confirmMsg += "event " + idx + " rate[" + preMr.y_rate + "," + preMr.p_rate + "," + preMr.r_rate + "] is invalid. <br>";
	    		}
	    			
	    		// Maneuver stop time must be before Rec time
	    		//Date manStopTime = DateUtil.DateAddSec(preMr.utc,preMr.dur);
	    		// 20170810 modify by jeff , async REC , Maneuver must stop REC before 20s
	    		Date manStopTime = DateUtil.DateAddSec(preMr.utc,preMr.dur+19);
	    		if (!mtlParser.getMissionTimelines().get(idx).utc.after(manStopTime)){
	    				needConfirm = true;
		    			confirmMsg += "event " + idx + " maneuver stop time and event " + (idx+1) + " rec start time need 20s gap. <br>";
	    		}
	    			
	    		// p_rate,r_rate,y_rate can not over upperbound and lowerbound
	    		if (preMr.p_rate.floatValue() - sat.getPrate_upperbound() > 0 || preMr.p_rate.floatValue() - sat.getPrate_lowerbound() < 0){
	    				needConfirm = true;
		    			confirmMsg += "event " + idx + " p_rate value over upperbound or lowerbound. <br>";	    				
	    		}
	    		if (preMr.r_rate.floatValue() - sat.getRrate_upperbound() > 0 || preMr.r_rate.floatValue() - sat.getRrate_lowerbound() < 0){
	    				needConfirm = true;
		    			confirmMsg += "event " + idx + " r_rate value over upperbound or lowerbound. <br>";	    				
	    		}
	    		if (preMr.y_rate.floatValue() - sat.getYrate_upperbound() > 0 || preMr.y_rate.floatValue() - sat.getYrate_lowerbound() < 0){
	    				needConfirm = true;
		    			confirmMsg += "event " + idx + " y_rate value over upperbound or lowerbound. <br>";	    				
	    		}	    				
	    		preMr.imagine_mode = "1";	    		
    		}
    		
    		if ( (idx + 1) == mtlParser.getMissionTimelines().size())
    		{
    			needConfirm = true;
    			confirmMsg += "The final event for REC is invalid. <br>";    			
    		}else{
    			// check nextMr
    			MTLRecord nextMr = mtlParser.getMissionTimelines().get(idx + 1);
    			if (nextMr.i_ev != 1)
	    		{
	    			needConfirm = true;
	    			confirmMsg += "event " + (idx+2) + " must be GOHOME Maneuver(I_EV=1) <br>";	    			
	    		}
    		}
    			    		
    		// rec duration can not over asynchronous rec duration limit
    		if (mtlParser.getMissionTimelines().get(idx).dur > sat.getAsyncrec_durlimit())
    		{
    			needConfirm = true;
    			confirmMsg += "event " + (idx+1) + " rec duration value over asynchronous rec duration limit <br>";    			
    		}
    	}
    	// if REC is synchronous,previous data must be Attitude Maneuver and rate must be zero
    	for (int idx : syncIndex){
    		if (idx == 0){
    			needConfirm = true;
    			confirmMsg += "The first event for REC is invalid. <br>";    			
    		}else{
    			MTLRecord preMr = mtlParser.getMissionTimelines().get(idx - 1);
	    		// check preMr
	    		if (preMr.i_ev != 2){
	    			needConfirm = true;
	    			confirmMsg += "event " + idx + " must be Attitude Maneuver(I_EV=2) <br>";	    			
	    		}
	    		
	    		// 20170810 add by jeff , sync REC , Maneuver must stop REC before 10s
	    		Date manStopTime = DateUtil.DateAddSec(preMr.utc,preMr.dur+9);
	    		if (!mtlParser.getMissionTimelines().get(idx).utc.after(manStopTime)){
	    				needConfirm = true;
		    			confirmMsg += "event " + idx + " maneuver stop time and event " + (idx+1) + " rec start time need  10s gap. <br>";		    				
	    		}
	    		
	    		if (!(preMr.p_rate.compareTo(0.0) == 0 && preMr.r_rate.compareTo(0.0) == 0 && preMr.y_rate.compareTo(0.0) == 0)){
	    				needConfirm = true;
		    			confirmMsg += "event " + idx + " rate[" + preMr.y_rate + "," + preMr.p_rate + "," + preMr.r_rate + "] is invalid. <br>";	    				
	    		}		    		
    		}    		
    	}
	}
}

