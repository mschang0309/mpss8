package mpss.mtl;

import java.sql.Timestamp;

import org.springframework.stereotype.Component;

import mpss.common.jpa.*;
import mpss.util.timeformat.*;

@Component("rs2MTLLoader")
public class RS2MTLLoader extends MTLLoaderBase
{
	
	public RS2MTLLoader()
	{
		// default constructor must exist for Spring
	}

	@Override
	public boolean createRSIMTLs(Arl arl)
	{
		genXcrossEvent(arl.getId().intValue());
		
		for (MTLRecord mr : mtlParser.getMissionTimelines())
	    {
	    	Activity act = new Activity();
	    	TimeRange tr = RangeUtil.startTimeAppendSec(new Timestamp(mr.utc.getTime()), mr.dur);
	    	act.setStarttime(tr.first);
			act.setEndtime(tr.second);
			act.setDuration(mr.dur);
			act.setSatelliteid(scId);
			//act.setActsubtype("NO");
			act.setToytype("NO");
			act.setValid(1);
			act.setSessionid(sessionId);
			act.setExtentid(0);
			act.setScheduled(0);
			act.setPrevactid(0);
			act.setNextactid(0);
			act.setUsermodified(0);
			
	    	// create ARL entries
			switch (mr.i_ev)
			{
				case 1:
					act.setArlentrytype("MANEUVER");
					act.setName("GOHOME_Maneuver");
					act.setActsubtype("NO");
					
					createSupEvent(arl, mr, act);
					break;
					
				case 2:
					act.setArlentrytype("MANEUVER");
					act.setName("Attitude_Maneuver");
					act.setActsubtype("NO");
					
					createManEvent(arl, mr, tr, "RSI", act);
					break;
					
				case 3:	      		
					String mode = imgModeDao.getImagingModeByInstNModeid("RSI", mr.mode).getModeshort();
					if (mr.ssr_status.equals("DDT"))
					{
						act.setName("RSI_DDT");
						act.setActsubtype("DDT");
					}
					else
					{	
						act.setName("RSI_REC:" + mode);
						act.setActsubtype("REC");
					}
					act.setArlentrytype("RSI");
					act.setInfo(mode);
					
					createRsiEvent(arl, mr, act);
					break;
					
				case 4:
//					entryid = createXcrossEvent(arl, mr).getId().intValue();
//					
//					act.setArlentrytype("XCROSS");
//					act.setName("XCROSS");
//					act.setActsubtype("NO");
//					act.setInfo("");
//					break;
					break;
					
				default :
					break;
			}
		
	    }		
		return true;
	}
	
	@Override
	public boolean createPROCMTLs(Arl arl)
	{
		TimeRange tr;
    	//Range<Timestamp> tr;
		
		for (MTLRecord mr : mtlParser.getMissionTimelines())
	    {
	    	Activity act = new Activity();
	    	tr = RangeUtil.startTimeAppendSec(new Timestamp(mr.utc.getTime()), mr.dur);
			act.setDuration(mr.dur);
			act.setStarttime(tr.first);
			act.setEndtime(tr.second);
			act.setSatelliteid(scId);
			act.setActsubtype("NO");
			act.setToytype("NO");
			act.setValid(1);
			act.setSessionid(sessionId);
			act.setExtentid(0);
			act.setScheduled(0);
			act.setPrevactid(0);
			act.setNextactid(0);
			act.setUsermodified(0);
	    	
	    	
	    	// create ARL entries
	    	if (mr.q1.equals(Double.NaN))
	    	{	
	    		// if this is a GOHOME maneuver request, generate GOHOme maneuver
	    		if (mr.procedure.equalsIgnoreCase("GOHOME"))
	    		{
		    		act.setName("ISUAL_GOHOME");
		    		act.setArlentrytype("MANEUVER");
					act.setValid(1);

		    		createManEvent(arl, mr, tr,"ISUAL GOHOME" , act);
	    		}
	    		else
	    		{
	    			tr = RangeUtil.startTimeAppendSec(new Timestamp(mr.utc.getTime()), 60*60*24);
	    			if ((tr.second.getTime() > mtlParser.getRequestwindowEndtime().getTime()) && (tr.first.getTime() < mtlParser.getRequestwindowEndtime().getTime()))
	    			{
	    				tr.closed(tr.first , new Timestamp(mtlParser.getRequestwindowEndtime().getTime()));
	    			}
		    		int validProc = 1;
					act.setName("ISUAL_Proc");
					act.setArlentrytype("ISUALPROC");
					act.setValid(validProc);
					
		    		createIsualEvent(arl, mr, tr, act);
	    		}
	    	}
	    	else
	    	{
	    		int goodManeuver = 1;	
	    		act.setName("ISUAL_Maneuver");
	    		act.setArlentrytype("MANEUVER");
				act.setValid(goodManeuver);

				createManEvent(arl, mr, tr,"ISUAL", act);
	    	}
	    }
		return true;
	}
	
	private boolean createSupEvent(Arl arl, MTLRecord mr, Activity act)
	{
	    Maneuverrequest supReq = new Maneuverrequest();
	    supReq.setArlid(arl.getId().intValue());
	    YearDayTimeOffset convert = new YearDayTimeOffset(new Timestamp(mr.utc.getTime()));
	    supReq.setYear(convert.getYear());
	    supReq.setDay(convert.getDayOfYear());
	    supReq.setStarttime(convert.getSecondsOfDay());
	    supReq.setDuration(mr.dur);
	    supReq.setManeuverduration(mr.dur);
	    supReq.setAsyncflag(0);
	    if (!mr.q1.isNaN())
	    {
          supReq.setQ1(mr.q1.floatValue());
	    }
	    if (!mr.q2.isNaN())
	    {
          supReq.setQ2(mr.q2.floatValue());
	    }
	    if (!mr.q3.isNaN())
	    {
          supReq.setQ3(mr.q3.floatValue());
	    }
	    if (!mr.q4.isNaN())
	    {
          supReq.setQ4(mr.q4.floatValue());
	    }
	    supReq.setStation(mr.station);
	    supReq.setElevation(mr.elev);
	    supReq.setAzimuth(mr.azim);
	    supReq.setOrbit(mr.orbit);
	    // 0=Attitude, 1=SUP(GOHOME), 2=Orbit
	    supReq.setType("GOHOME");

	    // create Maneuverrequest entries
	    try
	    {
	    	supReq = manReqDao.create(supReq);
	    	logger.info("LoadMtl: Maneuverrequest (SUP) entry created: " + supReq.getId());
	    }
	    catch (Exception e)
	    {
	    	logger.info("LoadMtl: Create Maneuverrequest (SUP) entry failed (%s)\n" +e.getMessage());
	    	return false;
	    }
	    
	    // create Activity entries
		act.setArlentryid(supReq.getId().intValue());
		if (createActivity(act) == null)
			return false;
        
		return true;
  }

	private boolean createManEvent(Arl arl, MTLRecord mr, TimeRange tr ,String type, Activity act)
	{
		Maneuverrequest manReq = new Maneuverrequest();
		YearDayTimeOffset convert = new YearDayTimeOffset(new Timestamp(mr.utc.getTime()));
		
		Timestamp noOffset = Timestamp.valueOf(tr.first.toString().split(" ")[0] + " 00:00:00");
    	Long offset = (tr.first.getTime() - noOffset.getTime())/1000;
    	
    	manReq.setArlid(arl.getId().intValue());
		manReq.setYear(convert.getYear());
		manReq.setDay(convert.getDayOfYear());
        manReq.setManeuverduration(mr.dur);
        manReq.setAsyncflag(0);
		if (!mr.q1.isNaN())
	    {
			manReq.setQ1(mr.q1.floatValue());
	    }
		if (!mr.q2.isNaN())
	    {
			manReq.setQ2(mr.q2.floatValue());
	    }
		if (!mr.q3.isNaN())
	    {
			manReq.setQ3(mr.q3.floatValue());
	    }
		if (!mr.q4.isNaN())
	    {
			manReq.setQ4(mr.q4.floatValue());
	    }
		if (type.equals("ISUAL GOHOME"))
		{
    	  manReq.setStarttime(offset.intValue());
    	  manReq.setDuration(tr.duration);
    	  manReq.setOrbit(0);
          manReq.setType("GOHOME");
  	      manReq.setComments("ISUAL GOHOME Maneuver");
		}
		else if (type.equals("ISUAL"))
		{
    	  manReq.setStarttime(offset.intValue());
    	  manReq.setDuration(tr.duration);
    	  manReq.setOrbit(0);
          manReq.setType("ATTITUDE");
  	      manReq.setComments("ISUAL maneuver");
		}
		else
		{
    	  manReq.setStarttime(convert.getSecondsOfDay());
    	  manReq.setDuration(mr.dur);
    	  manReq.setStation(mr.station);
    	  manReq.setElevation(mr.elev);
          manReq.setAzimuth(mr.azim);
          manReq.setOrbit(mr.orbit);
          manReq.setType("ATTITUDE");
		}
      
		try
	    {
	    	manReq = manReqDao.create(manReq);
	        logger.info("LoadMtl: Maneuverrequest (MAN) entry created: " + manReq.getId());
	    }
	    catch (Exception e)
	    {
	    	logger.info("LoadMtl: Create Maneuverrequest (MAN) entry failed (%s)\n" + e.getMessage());
	    	return false;
	    }
		
		// create Activity entries
		act.setArlentryid(manReq.getId().intValue());
		if (createActivity(act) == null)
			return false;
        
		return true;
	    
	}

	private boolean createRsiEvent(Arl arl, MTLRecord mr, Activity act)
	{
		Rsiimagingrequest rsiReq = new Rsiimagingrequest();
		rsiReq.setArlid(arl.getId().intValue());
		YearDayTimeOffset convert = new YearDayTimeOffset(new Timestamp(mr.utc.getTime()));
		rsiReq.setYear(convert.getYear());
		rsiReq.setDay(convert.getDayOfYear());
		rsiReq.setStarttime(convert.getSecondsOfDay());
		rsiReq.setDuration(mr.dur);
		rsiReq.setImagingmode(mr.mode); // 3
		rsiReq.setStation(mr.station);
		rsiReq.setElevation(mr.elev);
		rsiReq.setAzimuth(mr.azim);
		rsiReq.setOrbit(mr.orbit);
		rsiReq.setTrackGi(mr.gi); // 0
		rsiReq.setGridJe(mr.gj_e); // 0
		rsiReq.setGridJs(mr.gj_s); // 0
		rsiReq.setGridK(mr.gk); // 0
		rsiReq.setStartlatitude((float)mr.lat_s);
		rsiReq.setStoplatitude((float)mr.lat_e);
		rsiReq.setImagingtype(mr.type); // 1
		rsiReq.setSsrstatus(mr.ssr_status); // REC
		rsiReq.setPancompressionratio(mr.pancompressionratio.toUpperCase()); // HIGH
		rsiReq.setMscompressionratio(mr.mscompressionratio.toUpperCase()); // HIGH
		rsiReq.setVideopangain(mr.videopangain); // G1~G6
		rsiReq.setVideomb1gain(mr.videomb1gain); // G1~G6
		rsiReq.setVideomb2gain(mr.videomb2gain); // G1~G6
		rsiReq.setVideomb3gain(mr.videomb3gain); // G1~G6
		rsiReq.setVideomb4gain(mr.videomb4gain); // G1~G6

	    try
	    {
	    	rsiReq = rsiReqDao.create(rsiReq);
	        logger.info("LoadMtl: Rsiimagingrequest entry created: " + rsiReq.getId());
	    }
	    catch (Exception e)
	    {
	    	logger.info("LoadMtl: Create Rsiimagingrequest entry failed (%s)\n"+ e.getMessage());
	    	return false;
	    }
	    
	    // create Activity entries
		act.setArlentryid(rsiReq.getId().intValue());
		if (createActivity(act) == null)
			return false;
        
		//Creating RSI PBK and DELETE activities
		if(act.getActsubtype().equalsIgnoreCase("REC"))
		{
			logger.info("LoadMtl: Creating RSI PBK and DELETE activities.");
			createPbkDelActivity(act, rsiReq.getId().intValue());	
		}
		return true;
		
  }

	private boolean createIsualEvent(Arl arl, MTLRecord mr, TimeRange tr, Activity act)
	{
		Isualprocrequest isualReq = new Isualprocrequest();
		YearDayTimeOffset convert = new YearDayTimeOffset(new Timestamp(mr.utc.getTime()));
		
		Timestamp noOffset = Timestamp.valueOf(tr.first.toString().split(" ")[0] + " 00:00:00");
		Long offset = (tr.first.getTime() - noOffset.getTime())/1000;
  	
		isualReq.setArlid(arl.getId().intValue());
		isualReq.setYear(convert.getYear());
		isualReq.setDay(convert.getDayOfYear());
		isualReq.setStarttime(offset.intValue());
		isualReq.setDuration(mr.dur);
		isualReq.setIsualproc(mr.procedure);
		isualReq.setComments("ISUAL Proc from file");

	    try
	    {
	    	isualReq = isualReqDao.create(isualReq);
	        logger.info("LoadMtl: Isualprocrequest created: " + isualReq.getId());
	    }
	    catch (Exception e)
	    {
	    	logger.info("create Isualprocrequest failed (%s)\n"+ e.getMessage());
	    	return false;
	    }
	    
		// create Activity entries
		act.setArlentryid(isualReq.getId().intValue());
		if (createActivity(act) == null)
			return false;
        
		return true;
	}
	
	public int genPbkDur(int entryid, String mode){
		Rsiimagingrequest rsi = rsiReqDao.get((long)entryid);
		
		// look at arlEntry and determine which compression ratio to use
        // if selected DEFAULT/LOW then use normal MS data rate.
        // If selected HIGH then use MS ratio from PAN+MS rate
		int pbkDur = 0;
		int fsm = 0;
		double odr = 1;
		Recorderp recp = recpDao.getBySessionAndSat(sessionId, scId);
		
		//for RS2
		double ms = 0;
		double msp = 0;
		double pan = 0;
		double panp = 0;
		
		if(recp != null)
		{
			ms = recp.getMsimagingmode();
			msp = recp.getMsplusimagingmode();
			pan = recp.getPanimagingmode();
			panp = recp.getPanplusimagingmode();
			fsm = recp.getFilesectormargin();
			odr = recp.getOutputdatarate();
		}
		else
		{
			return 0;
		}
		
		if (mode.equalsIgnoreCase("MS"))
		{
			if(rsi.getMscompressionratio().equalsIgnoreCase("LOW") || rsi.getMscompressionratio().equalsIgnoreCase("DEFAULT"))
			{
				pbkDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * ms) + fsm) / odr));
			}
			else
			{
				pbkDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * msp) + fsm) / odr));
			}
			
		}
		else if (mode.equalsIgnoreCase("PAN"))
		{
			if(rsi.getPancompressionratio().equalsIgnoreCase("LOW") || rsi.getPancompressionratio().equalsIgnoreCase("DEFAULT"))
			{
				pbkDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * pan) + fsm) / odr));
				//System.out.println("PAN LOW: " + (Math.floor(rsi.getDuration() * pan) + fsm));
			}
			else
			{
				pbkDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * panp) + fsm) / odr));
				//System.out.println("PAN else: " + (Math.floor(rsi.getDuration() * pan) + fsm));
			}
		}
		else if (mode.equalsIgnoreCase("PAN+MS"))
		{
			// get VC1/VC2 bandwidth ratios (VC1 is PAN, VC2 is MS)
			double vc1,vc2;
			int v1 = recp.getVc1ratio();
			int v2 = recp.getVc2ratio();
			if (v1+v2 == 0){
				logger.info("GetVC1VC2Ratio:Invalid Virtual Channel Ratios with zero values.");
			}
			if(v1 != 0)
			{
				if(v2 != 0)
				{
					vc1=((100/(v1+v2))* v1) / 100;
					vc2=((100/(v1+v2))* v2) / 100;
				}
				else
				{
					vc1=1;
					vc2=0;
				}
			}
			else
			{
				vc1=0;
				vc2=1;
			}

			int MSDur,PANDur;
			switch (rsi.getPancompressionratio())
			{
				case "LOW":
					PANDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * pan) + fsm) / (odr * vc1)));
				case "DEFAULT":
					PANDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * panp) + fsm) / (odr * vc1)));
				default:
					PANDur = (int) Math.ceil(fsm / (odr * vc1));
			}
			switch (rsi.getMscompressionratio())
			{
				case "LOW":
					MSDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * ms) + fsm) / (odr * vc2)));
				case "DEFAULT":
					MSDur = (int) Math.ceil(((Math.floor(rsi.getDuration() * msp) + fsm) / (odr * vc2)));
				default:
					MSDur = (int) Math.ceil(fsm / (odr * vc2));
			}
			pbkDur = (PANDur > MSDur) ? PANDur : MSDur;
		}

		return pbkDur;
	}

}

