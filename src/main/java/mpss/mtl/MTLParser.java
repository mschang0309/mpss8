package mpss.mtl;

import java.util.Date;
import java.util.List;

public interface MTLParser 
{	
	public String getReturnMsg();
	
    public String getSource();
    
    public String getDestination();
    
    public String getFilename();
    
    public Date getDatetime();
    
    public String getSpacecraft();
    
    public String getInstrument();
    
    public int getOrbit();
    
    public double getTImagT();

    public Date getRequestwindowStarttime();
    
    public Date getRequestwindowEndtime();
    
    public List<MTLRecord> getMissionTimelines();
    
	public boolean open(String filepath);
	
	public boolean parse();
	
	public boolean parseHeader();

	public boolean parseRequestWindow();
  
	public List<MTLRecord> parseTimelines();
	
	public MTLRecord parseTimeline(String text);
	
	public boolean parseTitle(String text);
	
	public boolean parseData(String text);
	
	public Date parseUTC(String text);
}
