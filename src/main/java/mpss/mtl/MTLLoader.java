package mpss.mtl;


import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import mpss.common.jpa.*;

@Service
public interface MTLLoader 
{	
	public LinkedList<Long> getActIdList();
	public int getSessionId();	
	public void setIsBranch(boolean branch);
	public boolean Load(String mtlFileName,boolean isCheck);
	public boolean LoadMtl(String mtlFileName);
	public void genXcrossEvent(int arlId);  
	public Xcrossrequest createXcrossEvent(int arlId, Contact cn);	
	public Activity createActivity(Activity act);
	public String extractFileName( String filePathName ); 
	public String extractFilePath( String filePathName );
  	public void createPbkDelActivity(Activity act, int entryid);
  	public int genPbkDur(int entryid, String mode);
  	public boolean getNeedConfirm();
  	public String getConfirmMsg();
  	public String getMsg();
  	
  	public boolean checkGain(MTLRecord mr);
  	public void checkRSI(String scName,List<Integer> asyncIndex,List<Integer> syncIndex);
}

