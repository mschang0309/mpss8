package mpss.sesnman;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mpss.common.dao.*;

// for Spring component scanning
@Service
public class SessionManager 
{
	@SuppressWarnings("unused")
	private int scId;
	
	// for Spring injection
	@Autowired
	SatelliteDao satelliteDao;
	
	@Autowired
	SiteDao siteDao;
	
	public void setScid(int scid)
	{
	  this.scId = scid;
	}

	
	public SessionManager()
	{
		// default constructor must exist for Spring
	}

}
