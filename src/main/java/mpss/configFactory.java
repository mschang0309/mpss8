package mpss;

import mpss.mtl.MTLParser;
import mpss.mtl.RS5NewMTLParser;
import mpss.mtl.RS8MTLParser;
import mpss.schedule.base.GlSatellite;

public class configFactory {
	
	// OK
	public static String getLogName(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return "FS8LOG";
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return "FS5LOG";
		}else{
			return "FS5LOG";
		}		
	}
	
	// ok
	public static String getOrbitEventsLoader(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return "FS5orbitEventsLoader";
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return "FS5orbitEventsLoader";
		}else{
			return "FS5orbitEventsLoader";
		}		
	}
	
	// need to test xml 
	public static String getRTSCreator(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return "FS8RtsCreator";			
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return "FS5RtsCreator";
		}else{
			return "FS5RtsCreator";
		}		
	}
	
	
	public static String getMtlLoaderName(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return "rs8MTLLoader";			
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return "rs5MTLLoader";
		}else{
			return "rs5MTLLoader";
		}		
	}
	
	
	
	public static String getValidator(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return "rs8svValidator";			
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return "rs5svValidator";
		}else{
			return "rs5svValidator";
		}
	}
	
	
	
	public static String getPassPlanName(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return "FS8passPlan";			
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return "FS5passPlan";
		}else{
			return "FS5passPlan";
		}		
	}
	
	public static String getReportCreator(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return "FS8reportCreator";			
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return "FS5reportCreator";
		}else{
			return "FS5reportCreator";
		}		
	}
	
	
	// get event to read ele , generate command rpt
	public static String getEventGenerator(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return "fs8scheduledEventGenerator";			
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return "fs5scheduledEventGenerator";
		}else{
			return "fs5scheduledEventGenerator";
		}		
	}
	
	// read command rpt to load
	public static String getProductBuilder(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return "FS8scheduleProductBuilder";			
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return "FS5scheduleProductBuilder";
		}else{
			return "FS5scheduleProductBuilder";
		}		
	}	
	
	public static MTLParser getMTLParser(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return new RS8MTLParser();			
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return new RS5NewMTLParser();
		}else{
			return new RS5NewMTLParser();
		}		
	} 
	
	public static String getAipCreator(){		
		return "aipCreator";		
    }
	
	public static String getMisCreator(){		
			return "FS5MisCreator";			
	}
	
	//OK
	public static String getRocsatName(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return "ROCSAT8";
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return "ROCSAT5";
		}else{
			return "ROCSAT5";
		}		
	}
	
	
	//ok , need add command8.cfg 
	public static String getCmdcfg(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return "command8.cfg";
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return "command5.cfg";
		}else{
			return "command5.cfg";
		}		
	}
	
	//ok
	public static int getRocsat(){
		return 2;
	}
	
	
	//ok
	public static GlSatellite.Type getSetlliteType(){
		return GlSatellite.Type.ROCSAT2;	
	}
	
	//ok
	public static String getMtlFolder(String filetype){
		if(filetype.equalsIgnoreCase("ISUAL")){			
			return System.getProperty("mtlISUALFileDir");
		}else if(filetype.equalsIgnoreCase("AIP")){
			return System.getProperty("mtlAIPFileDir");
		}else{
			return System.getProperty("mtlRSIFileDir");
		}		
	}
	
	// ok
	public static String getSatelliteName(){
		if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT8")){			
			return "FS8";
		}else if(System.getProperty("satName").equalsIgnoreCase("FORMOSAT5")){
			return "FS5";
		}else{
			return "FS5";
		}		
	}

}
