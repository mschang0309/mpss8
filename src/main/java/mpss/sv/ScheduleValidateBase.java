package mpss.sv;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mpss.configFactory;
import mpss.common.dao.*;
import mpss.common.jpa.*;
import mpss.util.timeformat.TimeRange;

@Component("ScheduleValidateBase")
public class ScheduleValidateBase implements ScheduleValidate{
	
	@Autowired
	ExtentDao extentDao;
	@Autowired
	ActivityDao activityDao;
	@Autowired
	SatelliteDao satelliteDao;
	@Autowired
	SessionDao sessionDao;
	@Autowired
	SatellitepDao satellitepDao;
	@Autowired
	ContactDao contactDao;
	@Autowired
	SiteDao siteDao;
	@Autowired
	RecorderpDao recorderpDao;	
	@Autowired
	RecorderDao recDao;	
	@Autowired
	SessionManDao sessionManDao;
	@Autowired
	ActivityManDao actManDao; 

	protected Logger logger =null;
	private Session session;
	private Activity activity;
	private Extent extent;
	private Contact contact;

	private Satellite	satellite;
	private Site site;
	private Satellitep satelliteps;
	private Recorderp recorderp;

	private Recorder recorder;
//	private Antenna antenna;
//	private Transmitter transmitter;

	private Validator validator;
	
	public ScheduleValidateBase(){
		logger =Logger.getLogger(configFactory.getLogName());
	}
	
	public void setValidator(Validator validator)
	{
		this.validator = validator;
	}
	protected Validator getValidator()
	{
		return validator;
	}
	
	protected Extent getExtent()
	{
		return this.extent;
	}
	
	private void setExtent(Extent extent)
	{
		this.extent = extent;
	}
	
	protected Activity getActivity()
	{
		return this.activity;
	}
	
	private void setActivity(Activity activity)
	{
		this.activity = activity;
	}

	protected Contact getContact()
	{
		return this.contact;
	}
	
	private void setContact(Contact contact)
	{
	  	 this.contact = contact;		
	}
	
	protected Session getSession()
	{
		return this.session;
	}
	
	private void setSession(Session session)
	{
	  	this.session = session;		
	}
	
	protected Satellite getSatellite()
	{
		return this.satellite;
	}
	
	private void setSatellite(Satellite satellite)
	{
	  	 this.satellite = satellite;		
	}

	protected Site getSite()
	{
		return this.site;
	} 
	
	private void setSite(Site site)
	{
	  	 this.site = site;		
	}

	protected Satellitep getSatelliteps()
	{
		return this.satelliteps;
	}
	
	private void setSatelliteps(Satellitep satelliteps)
	{
	  	 this.satelliteps = satelliteps;		
	}
	
	protected Recorderp getRecorderp()
	{
		return this.recorderp;
	}
	
	private void setRecorderp(Recorderp recorderp)
	{
	  	 this.recorderp = recorderp;		
	}
	
	protected Recorder getRecorder()
	{
		return this.recorder;
	}
	
	private void setRecorder(Recorder recorder)
	{
		this.recorder = recorder;
	}
		
	
	public void generate(Extent extent)
	{
		setExtent(extent);
		if (extent.getRecorderid() != 0){
			setRecorder(recDao.get((long)extent.getRecorderid()));
		}

		Activity a = activityDao.get((long)extent.getActivityid());
		setActivity(a);
		if (a!=null)
		{
			setSession(sessionDao.get((long)a.getSessionid()));
			setSatellite(satelliteDao.get((long)a.getSatelliteid()));
			setRecorderp(recorderpDao.getBySessionAndSat(a.getSessionid(), a.getSatelliteid()));
		}
		
		Contact c = contactDao.get((long)extent.getContactid());
		setContact(c);
		if (c !=null)
		{
			setSite(siteDao.get((long)c.getSiteid()));
		}
    	
		int satId = this.satellite.getId().intValue();
		int psId = this.session.getPsid();
		setSatelliteps(satellitepDao.getBySatIdNPsId(satId,psId));

	}

	public void generate(Activity activity)
	{
		setActivity(activity);
		setSession(sessionDao.get((long)activity.getSessionid()));
		setSatellite(satelliteDao.get((long)activity.getSatelliteid()));
		setExtent(extentDao.get((long)activity.getExtentid()));

		Contact c = contactDao.get((long)this.extent.getContactid());
		setContact(c);
		if (c !=null)
		{
			setSite(siteDao.get((long)c.getSiteid()));
		}
    	  
		setSatelliteps(satellitepDao.getBySatIdNPsId(this.satellite.getId().intValue(),this.session.getPsid()));
	}
	
	public void getCriterion(LinkedList<String> msgs) {
	}
	
	public boolean ValidateActivity(Activity act) {
		return true;
	}
	
	public boolean ValidateExtent(Extent extent){
		return true;
	}
	
	public boolean ValidateExtents(Set<Extent> extents){
		return true;
	}
	
	public boolean ValidateContacts(List<Contact> contacts){
		return true;
	}
	
	public TimeRange getOperationalPeriod(Extent extent)
	{
//		Timestamp startTime = extent.getStarttime();
//		Timestamp endtime = extent.getEndtime();
//		Activity act = activityDao.get((long)extent.getActivityid());
//		Satellitep satps = sessionManDao.getSatellitep(act.getSessionid(), act.getSatelliteid());
//		
//		int setupDelay,cleanupDelay;
//		switch (act.getActsubtype())
//		{
//			case "PBK" :
//				setupDelay = satps.getRsipbsetupdelay();
//				cleanupDelay = satps.getRsipbcleanupdelay();
//				break;
//				
//			case "REC" :
//				setupDelay = satps.getRsirecsetupdelay();
//				cleanupDelay = satps.getRsireccleanupdelay();
//				break;
//			
//			case "DDT" :
//				setupDelay = satps.getRsirecsetupdelay();
//				cleanupDelay = (int)Math.ceil((extent.getRecorderdelta() + extent.getSecondrecorderdelta()) * satps.getPostddtdelay());
//				break;
//			
//			default:
//				setupDelay = 0;
//				cleanupDelay = 0;
//				break;
//		}
//
//		// calculate start/stop times buy considering ssr setup/cleanup times
//	    TimeRange TR = new TimeRange(startTime, endtime);
//	    TimeRange tRange = RangeUtil.rangeAppendSec(TR , setupDelay, - cleanupDelay);
//
//		return tRange;
		return actManDao.getOperationalPeriod(extent);
		
	}
}
