package mpss.sv;


import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Site;
import org.springframework.stereotype.Component;

@Component("rs8svValidator")
public class RS8SvValidator extends SvValidatorBase {
	

	public RS8SvValidator() {
		super();
		try
		{			
			advisoryBeans.put(AdvisoryType.SiteTurnArround, "svAdvSiteTurnArnd");
			advisoryBeans.put(AdvisoryType.UnscheduleActivity, "svAdvUnschedAct");
			//----------------------------------------------------------------------------
			violationBeans.put(ViolationType.SiteUnavail, "svVioSiteUnavail");
			violationBeans.put(ViolationType.RecordUnavail, "svVioRecUnavail");
			violationBeans.put(ViolationType.AntennaUnavail, "svVioAntUnavail");
			violationBeans.put(ViolationType.TransmitterUnavail, "svVioTxUnavail");
			violationBeans.put(ViolationType.ContactBound, "svVioContactBounds");
			violationBeans.put(ViolationType.RecordRSICapacity, "svVioRecRSICapacity");
			violationBeans.put(ViolationType.RFPAInterval, "svVioRFPAInterval");
			violationBeans.put(ViolationType.Rs2XCrossOverlap, "svAdvRS2XCrossOverlap");
			violationBeans.put(ViolationType.DutyCycle, "svVioDutyCycle");
			violationBeans.put(ViolationType.MANInterval, "svVioMANInterval");			
			violationBeans.put(ViolationType.Rs2ManDutESUmbraPeriod,"svVioRs2ManDurESUmbraPeriod");
			violationBeans.put(ViolationType.RevMan, "SvVioRevDnTimeMan");
			violationBeans.put(ViolationType.RevGohomeMan, "SvVioRevDnTimeGoMan");
		}
		catch (Exception e)
		{			
			logger.warn(e.getMessage());
		}

	}	
	
	public boolean checkActivitys() {
		
		logger.info("Checking individual activity constraints");
		boolean status = true;
		
		try
		{
			// get all the unscheduled activities during the window of the schedule
			List<Activity> unschedActs = actDao.getUnschByRangeNBranchId(svSessionId,branchid, svTR);
			// list all unscheduled activities first
			ScheduleValidate SvObserver = advisorys.get(AdvisoryType.UnscheduleActivity);
			SvObserver.setValidator(this);
			for(Activity act : unschedActs)
			{
				status = SvObserver.ValidateActivity(act) && status;
			}
			
			
			Set<Extent> filterExtents = new HashSet<Extent>(extentDao.getStartWithinTimeRange(svTR,branchid));
			//List<Extent> filterExtents = extentDao.getStartWithinTimeRange(svTR,branchid);
			// check if extent overlaps any of the unavailable times
			Set<Extent> recUnavailExtents = new HashSet<Extent>();
			Set<Extent> antUnavailExtents = new HashSet<Extent>();
			Set<Extent> txUnavailExtents = new HashSet<Extent>();
			boolean checkRecUnavail = false;
			boolean checkAntUnavail = false;
			boolean checkTxUnavail = false;
			
			for(Extent ex : filterExtents)
			{
				if (ex.getRecorderid() != 0){
					if (unTimeDao.isUnavailableTime(svSessionId,"REC",ex.getStarttime(),ex.getEndtime(), ex.getBranchid())){
						recUnavailExtents.add(ex);
						checkRecUnavail = checkRecUnavail || true;
					}
				}
				if (ex.getAntennaid() != 0){
					if (unTimeDao.isUnavailableTime(svSessionId,"ANT",ex.getStarttime(),ex.getEndtime(), ex.getBranchid())){
						antUnavailExtents.add(ex);
						checkAntUnavail = checkAntUnavail || true;	
					}
				}
				if (ex.getTransmitterid() != 0){
					if (unTimeDao.isUnavailableTime(svSessionId,"XMIT",ex.getStarttime(),ex.getEndtime(), ex.getBranchid())){
						txUnavailExtents.add(ex);
						checkTxUnavail = checkTxUnavail || true;
					}
				}
			}
			// violations that require one activity at a time for validation
			Map<ScheduleValidate, Set<Extent>> svObserverMap = new HashMap<ScheduleValidate, Set<Extent>>();
			if(checkRecUnavail) svObserverMap.put(violations.get(ViolationType.RecordUnavail),recUnavailExtents);
			if(checkAntUnavail) svObserverMap.put(violations.get(ViolationType.AntennaUnavail),antUnavailExtents);
			if(checkTxUnavail) svObserverMap.put(violations.get(ViolationType.TransmitterUnavail),txUnavailExtents);
			
			for (ScheduleValidate sv : svObserverMap.keySet())
			{
				logger.info("Validate Observer : " );
				sv.setValidator(this);
				for(Extent ex : svObserverMap.get(sv))
				{
					status = sv.ValidateExtent(ex) && status;
				}
			}
			
			svObserverMap.clear();
			svObserverMap.put(violations.get(ViolationType.SiteUnavail),filterExtents);
			svObserverMap.put(violations.get(ViolationType.ContactBound),filterExtents);			
			svObserverMap.put(violations.get(ViolationType.DutyCycle),filterExtents);
			svObserverMap.put(violations.get(ViolationType.Rs2XCrossOverlap),filterExtents);
			svObserverMap.put(violations.get(ViolationType.RecordRSICapacity),filterExtents);
			svObserverMap.put(violations.get(ViolationType.MANInterval),filterExtents);
			
			svObserverMap.put(violations.get(ViolationType.Rs2ManDutESUmbraPeriod),filterExtents);
			svObserverMap.put(violations.get(ViolationType.RevMan),filterExtents);
			svObserverMap.put(violations.get(ViolationType.RevGohomeMan),filterExtents);		

			
			for (ScheduleValidate sv : svObserverMap.keySet())
			{
				logger.info("Validate Observer : " +sv.toString());
				sv.setValidator(this);
				status = sv.ValidateExtents(svObserverMap.get(sv)) && status;
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}

		validateStatus =   validateStatus && status;
		logger.info("Check individual activity status : " + status);
		return true;

	}

	public boolean checkSites() {
		logger.info("Checking site related constraints");
		boolean status = true;
	    
		try
		{
			// get all sites used during scheduling of this session
			List<Site> sites = sessionManDao.getRefSitesById(svSessionId);
			
			ScheduleValidate SvObserver = advisorys.get(AdvisoryType.SiteTurnArround);
			SvObserver.setValidator(this);
			
			// Validate all scheduled contacts of specified site are not violating site turnaround time.
			for(Site s : sites)
			{
				List<Contact> contacts = contactDao.getConsbyRangeNSatNSite(svTR, satId, s.getId().intValue(), branchid);
				status = SvObserver.ValidateContacts(contacts);
			}
		}
		catch (Exception e)
		{
			logger.info("RS8SvValidator checkSites ex="+e.getMessage());
			return false;
		}
		
		
		validateStatus =   validateStatus && status;
		logger.info("Checking site related status : " + status);
		return true;
	}
	
	
	public boolean checkSatellites() {
		return true;
	}
	
	public boolean checkContacts() {
		return true;
	}
	
		
	
}
