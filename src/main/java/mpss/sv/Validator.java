package mpss.sv;

import java.util.LinkedList;
import java.util.Set;

import mpss.common.jpa.Activity;
import mpss.common.jpa.Satellitep;
import mpss.common.jpa.Session;
import mpss.util.timeformat.TimeRange;

import org.springframework.stereotype.Service;

@Service
public interface Validator {
	
	public boolean Validate(int sessionId,boolean isBranch);
	public void Criterion();
	public boolean validateSession();
	public boolean checkActivitys();
	public boolean checkSites();
	public boolean checkSatellites();
	public boolean checkContacts();
	public void AddAdvisory(Activity act, String msg);
	public void AddViolation(Activity act, String msg);
	public LinkedList<String> getAdvisoryCriterion();
	public LinkedList<String> getViolationCriterion();
	public LinkedList<String> getAdvisorySummary() ;
	public LinkedList<String> getViolationSummary() ;
	public void addActToUnschedule(Activity act);
	public Set<Activity> AddChainTo(Activity act);
	public boolean getSVStatus();
	public TimeRange getSVTR();
	public int getBranchid();
	public Satellitep getSatellitep();
	public Session getSession();
	public int getSatId();
	public String getSatName();
	public String getReturnMsg();
	

}
