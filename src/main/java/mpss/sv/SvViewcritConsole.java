package mpss.sv;

import java.util.LinkedList;

import mpss.config;
import mpss.configFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class SvViewcritConsole 
{
	public static void main(String args[])
	{
		System.setProperty("satName", "FORMOSAT5");
		config.load();  
		try 
		{
			// Spring application context
			ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf",configFactory.getSatelliteName()}));
			
			// Spring application context
			Validator validator = (Validator)context.getBean(configFactory.getValidator());
			if (validator == null) {
				System.out.println("Schedule Validator bean failed");
				return;
			}			
			
			validator.Criterion();			
			LinkedList<String> advisorys= validator.getAdvisoryCriterion();
			System.out.println("advisorys =============================");
			for(String txt : advisorys){				
				System.out.println(txt);
			}
			System.out.println("violations =============================");
			LinkedList<String> violations = validator.getViolationCriterion();
			for(String txt : violations){				
				System.out.println(txt);
			}
			
    	
      } catch (Exception ex) {
    		ex.printStackTrace();
      }
      
	    
	}
}
