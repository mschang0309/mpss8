package mpss.sv.violation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mpss.common.dao.ActivityDao;
import mpss.common.dao.ActivityManDao;
import mpss.common.dao.ExtentDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Extent;
import mpss.sv.ScheduleValidateBase;
import mpss.sv.Validator;
import mpss.util.timeformat.TimeToString;

@Component("svVioRecRSICapacity")
public class SvVioRecRSICapacity extends ScheduleValidateBase{
	@Autowired
	ExtentDao extentDao;
	@Autowired
	ActivityDao actDao;
	@Autowired
//	SessionManDao sessManDao;
//	@Autowired
//	RecorderDao recDao;
	ActivityManDao actManDao;
	
	private String SvVioRecRSICapacityMsg = "Required RSI Capacity exceeds available RSI Capacity." ;
	private String SvVioRecRSICapacityUsageMsg = "Required RSI Capacity: ";
	private String SvVioRecRSICapacityAvailMsg = " exceeds available RSI Capacity: ";
	
	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add("RSI Recorder Capacity availability: "+ SvVioRecRSICapacityMsg);
	}
	
	@Override
	public boolean ValidateExtents(Set<Extent> extents) 
	{	
		Validator Validator = super.getValidator();
		@SuppressWarnings("unused")
		int sessId = Validator.getSession().getId().intValue();
		boolean status = true;
		
		Extent[] extArray = extents.toArray(new Extent[extents.size()]);
		Arrays.sort(extArray, new Comparator<Extent>() {
			@Override
			public int compare(Extent e1, Extent e2) {
				return e1.getStarttime().compareTo(e2.getStarttime());
			}
		});

		for (Extent ext : extArray)
		{
			//System.out.println("Extent : [" + ext.getId() + "]" + ext.getStarttime());
			//get scheduled activity  
			Activity act = actDao.get((long)ext.getActivityid());
			
			// make sure I am RSI rec/pbk/ddt/del
			//if(!(act.getArlentrytype().equalsIgnoreCase("RSI") || act.getArlentrytype().equalsIgnoreCase("DELETE")))
			if(!(act.getArlentrytype().equalsIgnoreCase("RSI")))
			    continue;
			
			if (ext.getRecorderid() > 0)
			{
				// this is the total capacity used in this extent 
				int usedCap = getRecorderDelta(act, ext);
				if (usedCap != 0)
				{
//					int totalCapinMbits = 0; 
//				    Recorderp recp = sessManDao.getRecorderp(ext.getRecorderid(), act.getSessionid());
//				    if(recp != null)
//				    	totalCapinMbits = recp.getRsicapacity();
//					
//					// convert megabits to sectors
//				    int Sectorsize = recDao.get((long)ext.getRecorderid()).getSectorsize();
//				    double totalCap = 0;
//				    if (Sectorsize != 0)
//				    	totalCap = totalCapinMbits / Sectorsize;
//					
//					// get the available capacity of the RSI reservoir at the start of this extent
//					int availCap = getRSICapacity(ext.getStarttime(),ext.getRecorderid(),sessId);
					
					double availRSICap = actManDao.getAvailCapacity(ext,"RSI");
					
					// if used capacity does not equal 0 (PBK require 0 sectors - no need to 
					// report on extents that don't use any SSR when scheduled)
					// and if the available capacity + used capacity is greater than total capacity error
					//System.out.println("totalCap : " + totalCap + " / availCap :" + availCap + " + usedCap : " + usedCap);
//					if(usedCap > (totalCap - availCap))
					if(usedCap > availRSICap)
					{
						//System.out.println("False");
						String msg = SvVioRecRSICapacityUsageMsg + usedCap 
					       + SvVioRecRSICapacityAvailMsg + availRSICap//(totalCap - availCap)
					       + " (at time: " + TimeToString.AsLogTime(ext.getStarttime()) + ")";
					        
					    Validator.AddViolation(act, msg);
						status = status && false;
					}
				}
			}
		}
		
		return status;
	}
	
	@SuppressWarnings("unused")
	private int getRSICapacity(Timestamp tval,int recorderId,int sessId)
	{
		int capacity = 0;

		// now need to filter extents by:
		//  - prior to passed time
		//  - associated with specified recorder
		//  - associated to RSI related activities
		// and put in rsiExtents vector
		List<Extent> rsiExtents = new ArrayList<Extent>();
		List<Extent> delExtents = new ArrayList<Extent>();
		rsiExtents = extentDao.getExtentsByRSICapacity("RSI", tval, recorderId, sessId);
		delExtents = extentDao.getExtentsByRSICapacity("DELETE", tval, recorderId, sessId);
		//System.out.println("rsiExtents size : " + rsiExtents.size());
		for (Extent ex : delExtents)
		{
//			//System.out.println("Extent : [" + ex.getId() + "]" + ex.getStarttime());
//			// DELETE activities are chained to associated records which are the head of the linked activities list
//			Activity preAct = actDao.get((long)ex.getActivityid());
//			while (preAct.getPrevactid()>0)
//				preAct = actDao.get((long)preAct.getPrevactid());
//		    
//			if(preAct.getArlentrytype().equalsIgnoreCase("RSI"))
//			{
//				boolean addFlag = true;
//				for(Extent e : rsiExtents)
//				{
//					if(e.getActivityid().compareTo(preAct.getId().intValue()) == 0)
//					{
//						addFlag = false;
//						break;
//					}
//				}
//				if (addFlag)
//					rsiExtents.add(extentDao.get((long)preAct.getExtentid()));
//			}
			capacity += ex.getRecorderdelta();
		}
		// total up capacities of all extents on rsiExtents vector and return
		for(Extent rsiEx :rsiExtents)
		{
			capacity += rsiEx.getRecorderdelta();
			// if RSI is performing PAN+MS (requiring 2 filenames) then add in the second recorder delta
			if(rsiEx.getSecondfilename() > 0)
				capacity += rsiEx.getSecondrecorderdelta();
		}
		
		return capacity;
	}

	private int getRecorderDelta(Activity act, Extent ext)
	{
		//System.out.println("Actsubtype : " + act.getActsubtype());
	    int delta = 0;
	    if (act.getArlentrytype().equalsIgnoreCase("RSI") && 
	    	(act.getActsubtype().equalsIgnoreCase("DDT") || act.getActsubtype().equalsIgnoreCase("REC")))
	    {
	    	// if record is performing PAN+MS imaging need to add both
	        // recorder deltas from extent (first is PAN second is MS)
	    	delta = ext.getRecorderdelta();
	    	if (act.getInfo() != null)
	    	{
	    		if(act.getInfo().equalsIgnoreCase("PAN+MS"))
		        {
		            int panDelta = ext.getRecorderdelta();
		            int msDelta = ext.getSecondrecorderdelta();

		            delta = panDelta+msDelta;
		        }
	    	}
	    }
	    else if(act.getArlentrytype().equalsIgnoreCase("DELETE"))
	    {
	    	delta = ext.getRecorderdelta();
	    	if (act.getInfo() != null)
	    	{
	    		if(act.getInfo().equalsIgnoreCase("PAN+MS"))
		        {
		            int panDelta = ext.getRecorderdelta();
		            int msDelta = ext.getSecondrecorderdelta();

		            delta = panDelta+msDelta;
		        }
	    	}
	    	
	    	delta = - delta;
	    }
	    //System.out.println("delta : " + delta);
	    return delta; 
	}
}
