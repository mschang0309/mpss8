package mpss.sv.violation;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mpss.common.dao.ActivityDao;
import mpss.common.dao.EclipseDao;
import mpss.common.dao.SessionDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Eclipse;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Session;
import mpss.sv.ScheduleValidateBase;
import mpss.util.timeformat.TimeUtil;

@Component("svVioMANInterval")
public class SvVioMANInterval extends ScheduleValidateBase{

	@Autowired
	ActivityDao actDao;
	
	@Autowired
	EclipseDao eclipseDao;
	
	@Autowired
	SessionDao sessionDao;
	
	private String SvVioMANIntervalMsg = "The interval between Maneuver and GOHOME : ";
	private int goHomeMinLimit = 50;

	public SvVioMANInterval() {
		if(System.getProperty("goHomeMinLimit") != null)
			goHomeMinLimit = Integer.valueOf(System.getProperty("goHomeMinLimit"));
	}
	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add(SvVioMANIntervalMsg + goHomeMinLimit + " min");
	}

	@Override
	public boolean ValidateExtents(Set<Extent> extents) {
		
		Extent[] extsArray = extents.toArray(new Extent[extents.size()]);
		Arrays.sort(extsArray, new Comparator<Extent>() {
			public int compare(Extent e1, Extent e2) {
				return e1.getStarttime().compareTo(e2.getStarttime());
			}
		});
		boolean status = true;
		boolean gohomeLimit = false;
		Timestamp gohomeLimitTime = null;
		String manType = "Maneuver";
		for(Extent e : extsArray)
	    {
			String msg="";
			Activity act = actDao.get((long)e.getActivityid());
			Session sess = sessionDao.get((long)act.getSessionid());
			if(!gohomeLimit && act.getName().contains("Attitude"))
			{
				gohomeLimit = true;
				Eclipse eclipse = eclipseDao.getByManeuver(sess.getIsbranch() == 0 ? 0 : act.getSessionid(), act.getStarttime());
				if (eclipse == null)
				{
					gohomeLimitTime = TimeUtil.timeAppendSec(act.getStarttime(), 60 * goHomeMinLimit);
				}
				else
				{
					gohomeLimitTime = TimeUtil.timeAppendSec(eclipse.getPenumbraentrancetime(), 60 * goHomeMinLimit);
					manType = "Maneuver(Eclipse)";
				}
			}
			if (gohomeLimit && act.getName().contains("GOHOME"))
			{
				gohomeLimit = false;
				if( act.getStarttime().after(gohomeLimitTime))
				{
					int overSec = (int)(act.getStarttime().getTime() - gohomeLimitTime.getTime())/(1000);
					int overMin = (int) Math.floor(overSec/60);
					overSec = overSec - overMin * 60;
					msg = "The interval between " + manType + " and GOHOME over " + goHomeMinLimit + " min : " + (50+overMin) + " Minunts " + overSec + " seconds" ;
					super.getValidator().AddViolation(act, msg);
					status = false;
				}
			}
	    }

		return status;
	}

}
