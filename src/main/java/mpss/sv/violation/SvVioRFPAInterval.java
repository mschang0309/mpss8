package mpss.sv.violation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mpss.common.dao.ActivityDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Extent;
import mpss.sv.ScheduleValidateBase;

@Component("svVioRFPAInterval")
public class SvVioRFPAInterval extends ScheduleValidateBase{//implements IScheduleValidate{

	@Autowired
	ActivityDao actDao;
	
	private String SvVioRfpaRSIRecMsg = "RSI Record activity scheduled outside RFPA window: ";
	private String SvVioRfpaRSIDDTMsg = "RSI DDT activity scheduled outside RFPA window: ";
	
	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add("RFPA window: "+ SvVioRfpaRSIRecMsg);
		msgs.add("RFPA window: "+ SvVioRfpaRSIDDTMsg);
	}

	@Override
	public boolean ValidateExtents(Set<Extent> extents) {
		
		boolean status = true;
		status = validateRSIDDT(extents);
		status = validateRSIRecord(extents) && status;
		return status;
	}

	private boolean validateRSIDDT(Set<Extent> extents)
	{
		boolean status = true;
		// search thru all extents for all extents with RSI type and DDT subtypes 
		List<Extent> rsiExtents = new ArrayList<Extent>();
		for(Extent e : extents)
	    {
			Activity act = actDao.get((long)e.getActivityid());
			if(act.getArlentrytype().equalsIgnoreCase("RSI") && act.getActsubtype().equalsIgnoreCase("DDT"))
				rsiExtents.add(e);
	    }
		if (!HandleValidate(rsiExtents, SvVioRfpaRSIDDTMsg))
			status = false;

		return status;
	}
	
	private boolean validateRSIRecord(Set<Extent> extents)
	{
		boolean status = true;
		// search thru all extents for all extents with RSI type and RECORD subtypes 
		List<Extent> rsiExtents = new ArrayList<Extent>();
		for(Extent e : extents)
	    {
			Activity act = actDao.get((long)e.getActivityid());
			if(act.getArlentrytype().equalsIgnoreCase("RSI") && act.getActsubtype().equalsIgnoreCase("REC"))
				rsiExtents.add(e);
	    }
		if (!HandleValidate(rsiExtents, SvVioRfpaRSIRecMsg))
			status = false;
		
		return status;	
	}

	private boolean HandleValidate(List<Extent> extents, String rsiMsg){
		return true;
	}
}
