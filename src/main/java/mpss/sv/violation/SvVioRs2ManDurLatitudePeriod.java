package mpss.sv.violation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mpss.common.dao.ActivityDao;
import mpss.common.dao.EclipseDao;
import mpss.common.dao.LatitudeDao;
import mpss.common.dao.SatelliteDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Latitude;
import mpss.sv.ScheduleValidateBase;
import mpss.sv.Validator;
import mpss.util.timeformat.RangeUtil;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeToString;

@Component("svVioRs2ManDurLatitudePeriod")
public class SvVioRs2ManDurLatitudePeriod extends ScheduleValidateBase{

	@Autowired
	ActivityDao actDao;
	@Autowired
	SatelliteDao satDao;
	@Autowired
	EclipseDao epDao;	
	@Autowired
	LatitudeDao latitudeDao;
	
	private String SvVioRs2ManDurLatitudeMsg = "Maneuver occuring within Latitude 75 degree transition period: ";	

	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add("Latitude 75 degree Transition: "+ SvVioRs2ManDurLatitudeMsg);
		
	}
	
	@Override
	public boolean ValidateExtents(Set<Extent> extents) {
		boolean status = true;

	    // filter for maneuver extents
	    Set<Extent> manExtents = filterManeuverExtents(extents);

	    // if no maneuver extents were found - just return
	    if(manExtents.size() == 0)
	        return status;

	    // get Latitude transition periods
	    List<TimeRange> umbraEnterRanges = getLatitudeRanges();	    

	    // for each maneuver, check if it occurs within one of the umbra enter or exit transition periods
	    for(Extent manEx : manExtents)
	    {
	    	boolean manDuringUmbra = false;

	        // first check if maneuver is during Latitude ranges
	        //  this is a "common" method, so need to send violation message
	        manDuringUmbra = checkManDuringLatitudePeriod(manEx, 
	                                                        umbraEnterRanges, 
	                                                        SvVioRs2ManDurLatitudeMsg);

	        // if a maneuver occurred during a Latitude transition period, need to 
	        // update the violation status flag to false....
	        if(manDuringUmbra == true)
	            status = false;
	    }

		return status;
	}

	private Set<Extent> filterManeuverExtents(Set<Extent> allExtents)
	{
		Set<Extent> manExtents = new HashSet<Extent>();
		for(Extent ex : allExtents)
		{
			Activity act = actDao.get((long) ex.getActivityid());
			if(act.getArlentrytype().equalsIgnoreCase("MANEUVER") && !act.getName().startsWith("GOHOME"))
			{
					manExtents.add(ex);
			}
		}
		return manExtents;
	}
	
	private List<TimeRange> getLatitudeRanges()
	{
	    Validator validator = super.getValidator();

	    // filter through all latitudes events only 
	    List<Latitude> latitudes = latitudeDao.getSatNameNTimerange(validator.getBranchid(), validator.getSatName(), validator.getSVTR());
	    
	    List<TimeRange> latitudeRanges = new LinkedList<TimeRange>();
	    // for each latitudes, create latitudes transition periods
    	// get latitudes start period and latitudes stop period times
	    int startPeriod = Integer.valueOf(System.getProperty("latitudes.minus","60"));
        int stopPeriod = Integer.valueOf(System.getProperty("latitudes.plus","360"));
	    
	    for(Latitude ec : latitudes)
	    {
	        TimeRange latitudeRange = RangeUtil.timeAppendSec(ec.getLatitude70time(), - startPeriod, stopPeriod);
	        // add Latitude transition range to vector
	        latitudeRanges.add(latitudeRange); 	
	    }
	    return latitudeRanges;
	}
	
	
	private boolean checkManDuringLatitudePeriod(Extent manEx, List<TimeRange> umbraPeriods, String vioMsg)
	{
	    // flag to indicate if maneuver occurs during Latitude transition.  
		boolean Status = false;
		TimeRange manTR = new TimeRange(manEx.getStarttime(), manEx.getEndtime());
	    // iterate through the Latitude ranges
	    for(TimeRange umbraRange :  umbraPeriods)
	    {
	        // check if maneuver overlaps the Latitude range
	    	if(manTR.overlaps(umbraRange))
	        {
	            String msg = vioMsg + " " + TimeToString.AsLogTime(umbraRange);

	            super.getValidator().AddViolation(actDao.get((long)manEx.getActivityid()), msg);
	            Status = true;

	            break;
	        }
	    }
	    return Status;
	}
}

