package mpss.sv.violation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mpss.cl.RS2Policy;
import mpss.common.dao.UnavailableResourcesDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Extent;
import mpss.sv.ScheduleValidateBase;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeToString;

@Component("svVioSiteUnavail")
public class SvVioSiteUnavail extends ScheduleValidateBase{

	@Autowired
	UnavailableResourcesDao unTimeDao;
	
	private String SvVioSiteUnavailMsg = "Site unavailable for time range: ";
	
	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add("Site availability: " + SvVioSiteUnavailMsg + "<activity time range>");
	}


	@Override
	public boolean ValidateExtents(Set<Extent> extents) {
		boolean status = true;
		for (Extent ex : extents)
		{
			if (ex.getContactid() != 0)
			{
				super.generate(ex);
				Activity act = super.getActivity();
				
				TimeRange tr = getOperationalPeriod(ex);
				
				int siteId = super.getContact().getSiteid();
				int sessionId = act.getSessionid();
				if (unTimeDao.isUnavailableTimeBySite(sessionId,siteId,tr.first,tr.second, ex.getBranchid()))
				{
					String msg = SvVioSiteUnavailMsg + " [" + TimeToString.AsLogTime(ex.getStarttime()) +
			            	" - " + TimeToString.AsLogTime(ex.getEndtime()) + "]";
					super.getValidator().AddViolation(act, msg);

			        Set<Activity> actsToUnsched= new HashSet<Activity>();
			        //Set<Activity> actsToUnsched = null;
			        RS2Policy policy = new RS2Policy();
			        if (!policy.AtomicForScheduleLoading(act))
			        	actsToUnsched = super.getValidator().AddChainTo(act);
			        else
			            actsToUnsched.add(act);
			        
			        for(Activity actToUnsched : actsToUnsched)
			        	super.getValidator().addActToUnschedule(actToUnsched);

			        status = status && false  ;
				}
			}
		}
			
		return status;
	}
	

}

