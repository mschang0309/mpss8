package mpss.sv.violation;



import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mpss.common.dao.ActivityDao;
import mpss.common.dao.EclipseDao;
import mpss.common.dao.LatitudeDao;
import mpss.common.dao.SessionDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Latitude;
import mpss.common.jpa.Session;
import mpss.sv.ScheduleValidateBase;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeUtil;

@Component("SvVioLatitudeTimeMan")
public class SvVioLatitudeTimeMan extends ScheduleValidateBase{

	@Autowired
	ActivityDao actDao;
	
	@Autowired
	EclipseDao eclipseDao;
	
	@Autowired
	LatitudeDao latitudeDao;
	
	@Autowired
	SessionDao sessionDao;
	
	private int latitudelimit = 300;

	public SvVioLatitudeTimeMan() {
		if(System.getProperty("latitude.att.man") != null)
			latitudelimit = Integer.valueOf(System.getProperty("latitude.att.man","300"));
	}
	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add("Latitude 75 degree Time +"+latitudelimit+"s overlap Attitude Maneuver start time ");
	}

	@Override
	public boolean ValidateExtents(Set<Extent> extents) {
		
		Extent[] extsArray = extents.toArray(new Extent[extents.size()]);
		Arrays.sort(extsArray, new Comparator<Extent>() {
			public int compare(Extent e1, Extent e2) {
				return e1.getStarttime().compareTo(e2.getStarttime());
			}
		});
		
		boolean status = true;	
		TimeRange LimitTimerange = null;		
		for(Extent e : extsArray)
	    {
			String msg="";
			Activity act = actDao.get((long)e.getActivityid());
			Session sess = sessionDao.get((long)act.getSessionid());
			
			if(act.getName().contains("Attitude")){
				Latitude latitude = latitudeDao.getByStartTime(sess.getIsbranch() == 0 ? 0 : act.getSessionid(), act.getStarttime());
				if(latitude!=null){
					LimitTimerange =new TimeRange(latitude.getLatitude70time(),TimeUtil.timeAppendSec(latitude.getLatitude70time(), latitudelimit));					
					if(LimitTimerange.contains(act.getStarttime()))
					{					
						msg = "Latitude 75 degree Time +"+latitudelimit+"s overlap Attitude Maneuver start time" ;
						super.getValidator().AddViolation(act, msg);
						status = status && false;
					}
				}
				
			}
	    }

		return status;
	}

}
