package mpss.sv.violation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mpss.common.dao.ActivityDao;
import mpss.common.dao.EclipseDao;
import mpss.common.dao.SatelliteDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Eclipse;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Satellitep;
import mpss.sv.ScheduleValidateBase;
import mpss.sv.Validator;
import mpss.util.timeformat.RangeUtil;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeToString;

@Component("svVioRs2ManDurESUmbraPeriod")
public class SvVioRs2ManDurESUmbraPeriod extends ScheduleValidateBase{

	@Autowired
	ActivityDao actDao;
	@Autowired
	SatelliteDao satDao;
	@Autowired
	EclipseDao epDao;
	
//	private String satName = "ROCSAT2";
	
	private String SvVioRs2ManDurESUmbraEnterMsg = "Maneuver occuring within EARTH/SUN Umbra ENTER transition period: ";
	private String SvVioRs2ManDurESUmbraExitMsg = "Maneuver occuring within EARTH/SUN Umbra EXIT transition period: ";

	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add("EARTH/SUN Umbra Enter Transition: "+ SvVioRs2ManDurESUmbraEnterMsg);
		msgs.add("EARTH/SUN Umbra Exit Transition: "+ SvVioRs2ManDurESUmbraExitMsg);
	}
	
	@Override
	public boolean ValidateExtents(Set<Extent> extents) {
		boolean status = true;

	    // filter for maneuver extents
	    Set<Extent> manExtents = filterManeuverExtents(extents);

	    // if no maneuver extents were found - just return
	    if(manExtents.size() == 0)
	        return status;

	    // get Earth/Sun umbra enter/exit transition periods
	    List<TimeRange> umbraEnterRanges = getESUmbraRanges("Enter");
	    List<TimeRange> umbraExitRanges = getESUmbraRanges("Exit");

	    // for each maneuver, check if it occurs within one of the umbra enter or exit transition periods
	    for(Extent manEx : manExtents)
	    {
	    	boolean manDuringUmbra = false;

	        // first check if maneuver is during umbra enter ranges
	        //  this is a "common" method, so need to send violation message
	        manDuringUmbra = checkManDuringUmbraPeriod(manEx, 
	                                                        umbraEnterRanges, 
	                                                        SvVioRs2ManDurESUmbraEnterMsg);

	        if( !manDuringUmbra)
	        {
	            // if maneuver was not found during a umbra enter transition, check if
	            // it occurs during a umbra exit transition
	            //  this is a "common" method, so need to send violation message
	            manDuringUmbra = checkManDuringUmbraPeriod(manEx, 
	                                                            umbraExitRanges,
	                                                            SvVioRs2ManDurESUmbraExitMsg);
	        }

	        // if a maneuver occurred during a umbra transition period, need to 
	        // update the violation status flag to false....
	        if(manDuringUmbra == true)
	            status = false;
	    }

		return status;
	}

	private Set<Extent> filterManeuverExtents(Set<Extent> allExtents)
	{
		Set<Extent> manExtents = new HashSet<Extent>();
		// loop through all the extents, and add all ROCSAT2 maneuvers to the maneuver extents vector
		for(Extent ex : allExtents)
		{
			Activity act = actDao.get((long) ex.getActivityid());
			if(act.getArlentrytype().equalsIgnoreCase("MANEUVER") && !act.getName().startsWith("GOHOME"))
			{
//				if(satDao.get((long)act.getSatelliteid()).getSattype().equalsIgnoreCase(satName))
					manExtents.add(ex);
			}
		}
		return manExtents;
	}
	
	private List<TimeRange> getESUmbraRanges(String type)
	{
	    Validator validator = super.getValidator();

	    // filter through all eclipse events only for EARTH/SUN eclipses
	    List<Eclipse> earthSunEclipses = epDao.getSatNameNTimerange(validator.getBranchid(), validator.getSatName(), "Earth/Sun", validator.getSVTR());
	    
	    List<TimeRange> umbraRanges = new LinkedList<TimeRange>();
	    // for each EARTH/SUN eclipse, create umbra enter/exit transition periods
    	// get umbra enter start period and umbra enter stop period times
	    int startPeriod = 60;
        int stopPeriod = 360;
	    Satellitep satPs = validator.getSatellitep();
	    if (satPs != null)
	    {
	    	startPeriod = satPs.getEclipseenterdelay();
	        stopPeriod = satPs.getEclipseexitdelay();
	    }

	    for(Eclipse ec : earthSunEclipses)
	    {


	        TimeRange umbraRange;
	        if (type == "Enter")
	        	umbraRange = RangeUtil.timeAppendSec(ec.getUmbraentrancetime(), - startPeriod, stopPeriod);
	        else
	        	umbraRange = RangeUtil.timeAppendSec(ec.getUmbraexittime(), - startPeriod, stopPeriod);

	        // add umbra enter/exit transition range to vector
	        umbraRanges.add(umbraRange); 	
	    }
	    return umbraRanges;
	}
	
	
	private boolean checkManDuringUmbraPeriod(Extent manEx, List<TimeRange> umbraPeriods, String vioMsg)
	{
	    // flag to indicate if maneuver occurs during umbra transition.  
		boolean Status = false;
		TimeRange manTR = new TimeRange(manEx.getStarttime(), manEx.getEndtime());
	    // iterate through the umbra enter ranges
	    for(TimeRange umbraRange :  umbraPeriods)
	    {
	        // check if maneuver overlaps the umbra range
	    	if(manTR.overlaps(umbraRange))
	        {
	            String msg = vioMsg + " " + TimeToString.AsLogTime(umbraRange);

	            super.getValidator().AddViolation(actDao.get((long)manEx.getActivityid()), msg);
	            Status = true;

	            break;
	        }
	    }
	    return Status;
	}
}

