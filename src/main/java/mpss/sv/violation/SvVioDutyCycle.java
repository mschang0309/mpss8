package mpss.sv.violation;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mpss.common.dao.ActivityDao;
import mpss.common.dao.RevDao;
import mpss.common.dao.SessionManDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Dutycycle;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Rev;
import mpss.sv.ScheduleValidateBase;
import mpss.util.timeformat.DateUtil;
import mpss.util.timeformat.TimeRange;

@Component("svVioDutyCycle")
public class SvVioDutyCycle extends ScheduleValidateBase{

	@Autowired
	ActivityDao actDao;
	@Autowired
	SessionManDao sessionManDao;
	@Autowired
	RevDao revDao;
	
	private String SvVioDutyCycleMsg = "Requested Duty Cycle usage exceeds the total available Duty Cycles.";
	private String SvVioDutyCycleUsageMsg = "Requested Duty Cycle usage: ";
	private String SvVioDutyCycleTotalMsg = " exceeds the total available Duty Cycles: ";
	
	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add("Duty Cycle availability: " + SvVioDutyCycleMsg + " <activity time range>");
	}


	@Override
	public boolean ValidateExtents(Set<Extent> extents) {
		boolean status = true;
		
		int satId = super.getValidator().getSatId();
		int sessionId = super.getValidator().getSession().getId().intValue();
		int branchid = super.getValidator().getBranchid();
		for (Extent ex : extents)
		{
			Activity act = actDao.get((long)ex.getActivityid());

			if(act.getActsubtype().equalsIgnoreCase("REC") || act.getActsubtype().equalsIgnoreCase("DDT"))
			{
				Dutycycle dc = sessionManDao.getDutycycle(sessionId, satId, "RSI", "REC");
				if(dc != null)
				{
					// get the time range for the rev that contain this trv
					Rev rev = revDao.getMaxTimeRev(satId, branchid, ex.getStarttime());
					TimeRange revRange = new TimeRange(rev.getAntime(), rev.getRevendtime());
					
					int usedDC = GetDutyCycleCapacity(satId, ex.getStarttime(), branchid, revRange);
					
					// GetPowerBusy returns a vector of the time ranges for given extent 
					TimeRange tr = getOperationalPeriod(ex);
					
					// this is the total power time needed by this extent 
					int totalpowerNeeded = 0;
					
					// if power time is fully contained add the "duration" to the totalpowerNeeded
					if(revRange.encloses(tr))
						totalpowerNeeded += tr.duration;
					// power usage is not fully contained within revwindow, only add
					// overlapping times to totalpowerNeeded
					else
					{
						TimeRange overlap = revRange.contractTo(tr);
						if(overlap != null)
							totalpowerNeeded += overlap.duration;
					}
					
					if (totalpowerNeeded + usedDC > dc.getDutycycles())
					{
						String msg = SvVioDutyCycleUsageMsg + "[" +
							DateUtil.parseSecondsToHms(totalpowerNeeded + usedDC) 
							+ "] " + SvVioDutyCycleTotalMsg+ "[" + 
							DateUtil.parseSecondsToHms(dc.getDutycycles()) + "]";

						super.getValidator().AddViolation(act, msg);
						status = status && false;						
					}
				}
			}
		}
			
		return status;
	}
	
	
	// use arl type and subtypes and satellite to get the 
	// the time of duty cycle usage at the specified extent strat time.
	// this will be the time of power used up to the start of the extent
	public int GetDutyCycleCapacity(int satId, Timestamp tval, int branchid, TimeRange revRange)
	{
		int capacity = 0;

		// holder of all duty cycle related extents
		List<Extent> dcExtents = sessionManDao.getDutyCycleExtents(satId, branchid, tval);
		
		// contains listing of all power usage times
		List<TimeRange> powerTimes = new ArrayList<TimeRange>();
		
		// loop through dcExtents adding the duty cycle capacitities
		for(Extent e : dcExtents)
			powerTimes.add(super.getOperationalPeriod(e));
		
		// loop through the power usage times vector, verifying the time window
		// is fully contained within the rev window and add to the time window
		// (in seconds) to the capacity.  If the power time is not fully contained 
		// within the rev window, only add the range that is contained within the window.
		for(TimeRange tr : powerTimes)
		{
			// if power time is fully contained add the "range" to the capacity
			if(revRange.encloses(tr))
				capacity += tr.duration;
			// power usage is not fully contained within revwindow, only add overlapping times to capacity
			else
			{
				TimeRange overlapTR = revRange.contractTo(tr);
				if(overlapTR != null)
					capacity += overlapTR.duration;
			}
		}
		
		return capacity;
	}
}

