package mpss.sv.violation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.springframework.stereotype.Component;

import mpss.cl.RS2Policy;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Extent;
import mpss.sv.ScheduleValidateBase;
import mpss.util.timeformat.TimeToString;

@Component("svVioRecUnavail")
public class SvVioRecUnavail extends ScheduleValidateBase{
	private String SvVioRecUnavailMsg = "Recorder unavailable for time range: ";
	
	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add("Recorder availability: " + SvVioRecUnavailMsg + "<activity time range>");
	}


	@Override
	public boolean ValidateExtent(Extent extent) {
		
		super.generate(extent);
		Activity act = super.getActivity();

		String msg = SvVioRecUnavailMsg + " [" + TimeToString.AsLogTime(extent.getStarttime()) +
            	" - " + TimeToString.AsLogTime(extent.getEndtime()) + "]";
		super.getValidator().AddViolation(act, msg);

        Set<Activity> actsToUnsched= new HashSet<Activity>();
        //Set<Activity> actsToUnsched = null;
        RS2Policy policy = new RS2Policy();
        if (!policy.AtomicForScheduleLoading(act))
        	actsToUnsched = super.getValidator().AddChainTo(act);
        else
            actsToUnsched.add(act);
        
        for(Activity actToUnsched : actsToUnsched)
        	super.getValidator().addActToUnschedule(actToUnsched);

		return false;

	}

}
