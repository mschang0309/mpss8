package mpss.sv.violation;

import java.util.LinkedList;
import java.util.Set;
import org.springframework.stereotype.Component;

import mpss.common.jpa.Extent;
import mpss.sv.ScheduleValidateBase;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeToString;

@Component("svVioContactBounds")
public class SvVioContactBounds extends ScheduleValidateBase{

	private String SvVioContactBoundsMsg = "Activity not scheduled within bounds of contact.";
	private String SvVioExpansionBoundsMsg = "Expansion range extends past bounds of contact.";
	private String SvXBandVioContactBoundsMsg = "Activity scheduled outside bounds of X-Band contact mask: ";
	private String SvSBandVioContactBoundsMsg = "Activity scheduled outside bounds of S-Band contact mask: ";
//	private String SvMonVioContactBoundsMsg = "Activity scheduled outside bounds of contact: ";
	
	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add("Contact Bounds: "+ SvVioContactBoundsMsg);
		msgs.add("Expansion Bounds: "+ SvVioExpansionBoundsMsg);
	}
	
	@Override
	public boolean ValidateExtents(Set<Extent> extents) {
		boolean status = true;
		for (Extent ex : extents)
		{
			if (ex.getContactid() != 0)
			{
				boolean found = false;
				String msg="";
				super.generate(ex);
				
				TimeRange extentTR = getOperationalPeriod(ex);
				// if contact is X-Band 		
				if (super.getSite().getSitetype().equalsIgnoreCase("XBAND"))
				{
					// then check and make sure is scheduled within the 20 degrees boundry
					// This verifies that the extent is fully contained within any of the time windows of the contact
					TimeRange contTR = new TimeRange(super.getContact().getXbandrise(), super.getContact().getXbandfade());
					if(!contTR.encloses(extentTR))
						found = true;
					msg = SvXBandVioContactBoundsMsg + " " + TimeToString.AsLogTime(contTR);
				}
				
				if (super.getSite().getSitetype().equalsIgnoreCase("SBAND"))
				{
					TimeRange contTR;
					// if contact is monitoring
					if (super.getActivity().getArlentrytype().equalsIgnoreCase("MON")) 
					{
						contTR = new TimeRange(super.getContact().getRise(), super.getContact().getFade());
						msg = SvVioContactBoundsMsg + " " + TimeToString.AsLogTime(contTR);
					}
					// if contact is S-Band and not monitoring check and make sure is scheduled within the 10 degrees boundry
					else
					{
						contTR = new TimeRange(super.getContact().getCmdrise(), super.getContact().getCmdfade());
						msg = SvSBandVioContactBoundsMsg + " " + TimeToString.AsLogTime(contTR);
					}

		            // verify extent is within contact bounds (either 0 degree or 10 degree)
					if(!contTR.encloses(extentTR))
						found = true;			
				}
				
				// if I find a valid time window, report violation
				if(found)
				{
					super.getValidator().AddViolation(super.getActivity(), msg);
					status = status && false ;
				}
			}
		}
		
		return status;
	}
	
}
