package mpss.sv;


import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import mpss.configFactory;
import mpss.common.dao.ActivityDao;
import mpss.common.dao.ContactDao;
import mpss.common.dao.ExtentDao;
import mpss.common.dao.SatelliteDao;
import mpss.common.dao.SessionDao;
import mpss.common.dao.SessionManDao;
import mpss.common.dao.UnavailableResourcesDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Satellite;
import mpss.common.jpa.Satellitep;
import mpss.common.jpa.Session;
import mpss.util.timeformat.RangeUtil;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeToString;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class SvValidatorBase implements Validator {
	
	protected Logger logger =null;

	@Autowired
	ActivityDao actDao;
	@Autowired
	SessionDao sessionDao;	
	@Autowired
	SessionManDao sessionManDao;
	@Autowired
	SatelliteDao satDao;	
	@Autowired
	ContactDao contactDao;	
	@Autowired
	ExtentDao extentDao;
	@Autowired
	UnavailableResourcesDao unTimeDao;
	
	public static final int extension = 2*(102*60);
	
	public enum AdvisoryType {
		ContactSep, Rs2XCrossOverlap, ScheduleBound, SiteTurnArround, UnscheduleActivity
	}

	public enum ViolationType {
		AntennaUnavail, ContactBound, RecordRSICapacity, RecordUnavail, ResourceNotOperational,DutyCycle,
		RFPAInterval, Rs2ManDutESUmbraPeriod, SiteUnavail, TransmitterUnavail, Rs2XCrossOverlap, MANInterval,RevMan,RevGohomeMan,Rs2ManDurLatitudePeriod,LatitudeMan,LatitudeGohomeMan
	}
	
	protected EnumMap<AdvisoryType, String> advisoryBeans = new EnumMap<AdvisoryType, String>(
			AdvisoryType.class);
	protected EnumMap<ViolationType, String> violationBeans = new EnumMap<ViolationType, String>(
			ViolationType.class);
	protected EnumMap<AdvisoryType, ScheduleValidate> advisorys = new EnumMap<AdvisoryType, ScheduleValidate>(
			AdvisoryType.class);
	protected EnumMap<ViolationType, ScheduleValidate> violations = new EnumMap<ViolationType, ScheduleValidate>(
			ViolationType.class);


	protected HashMap<Activity, String> advisoriesMap = new HashMap<Activity, String>(); // key=Activity
	protected HashMap<Activity, String> violationsMap = new HashMap<Activity, String>(); // key=Activity

	public String returnMsg;
	protected boolean validateStatus;
	protected Set<Activity> ActsToUnschedule = new HashSet<Activity>();

	//Validate Session Info
	protected int svSessionId;
	protected Session svSession;
	protected boolean isBranch;
	protected int branchid;
	protected int satId;
	protected String satName;
	protected TimeRange svTR;
	protected Satellitep satPs;
	

	public SvValidatorBase() {		
		logger =Logger.getLogger(configFactory.getLogName());
	}
	
	
	
	public boolean Validate(int sessionId,boolean isBranch)
	{		
		logger.info("Session[" + sessionId + "] Schedule Validator Generator beginning...");
		this.svSessionId = sessionId;
		this.isBranch = isBranch;
		
		if (actDao == null) {			
			logger.info("Schedule Validator failed: ActivityDao not injected");
			return false;
		}
		if (sessionDao == null) {			
			logger.info("Schedule Validator failed: SessionDao not injected");
			return false;
		}
		if (satDao == null) {			
			logger.info("Schedule Validator failed: satDao not injected");
			return false;
		}
		if (extentDao == null) {			
			logger.info("Schedule Validator failed: extentDao not injected");
			return false;
		}
		if (contactDao == null) {			
			logger.info("Schedule Validator failed: contactDao not injected");
			return false;
		}
		
		ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf"}));
		for (AdvisoryType type : advisoryBeans.keySet())
		{
			ScheduleValidate SvObserver = (ScheduleValidate)context.getBean(advisoryBeans.get(type));
			advisorys.put(type, SvObserver);
		}
		for (ViolationType type : violationBeans.keySet())
		{
			ScheduleValidate SvObserver = (ScheduleValidate)context.getBean(violationBeans.get(type));
			violations.put(type, SvObserver);
		}
		
		//Validate Session Info
		this.branchid = (!isBranch) ? 0 : sessionId;
		this.svSession = sessionDao.get((long) svSessionId);
		Satellite sat = sessionManDao.getSatellite(sessionId);
		this.satId = sat.getId().intValue();
		this.satName = sat.getName();
		int maxSoFar = sat.getNominalorbit();
		TimeRange tr = new TimeRange(svSession.getStarttime(), svSession.getEndtime());
		this.svTR = RangeUtil.rangeAppendSec(tr, - maxSoFar,extension);
		this.satPs = sessionManDao.getSatellitep(svSessionId, satId);
		
		
		if (!validateSession())
		  {
		    return false;
		  }
		
	  return true;
	  
	}
	
	public void Criterion()
	{		
		logger.info("Schedule Validator Criterion beginning...");
		
		ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf"}));
		for (AdvisoryType type : advisoryBeans.keySet())
		{
			ScheduleValidate SvObserver = (ScheduleValidate)context.getBean(advisoryBeans.get(type));
			advisorys.put(type, SvObserver);
		}
		for (ViolationType type : violationBeans.keySet())
		{
			ScheduleValidate SvObserver = (ScheduleValidate)context.getBean(violationBeans.get(type));
			violations.put(type, SvObserver);
		}
		
	}

	public boolean validateSession() {
		validateStatus = true;
		advisoriesMap.clear();
		violationsMap.clear();
		
		
		if (!checkActivitys())
			return false;
		if (!checkSites())
			return false;
		if (!checkSatellites())
			return false;
		if (!checkContacts())
			return false;
				
		

		for (String msg : getAdvisorySummary())
		{			
			logger.info("AdvisorySummary : " + msg);
		}
		for (String msg : getViolationSummary())
		{			
			logger.info("ViolationSummary : " + msg);
		}		
		logger.info("Validate Status : " + validateStatus);
		
		return true;

	}

	public boolean checkActivitys() {	
		
		return true;

	}

	public boolean checkSites() {		
		return true;
	}
	
	
	public boolean checkSatellites() {
		return true;
	}
	
	public boolean checkContacts() {
		return true;
	}
	
		
	public void AddAdvisory(Activity act, String msg) {
	    if(act.getSessionid() == svSessionId){
	    	advisoriesMap.put(act, msg);
	    }
	}
	
	public void AddViolation(Activity act, String msg) {
	    if(act.getSessionid() == svSessionId){
	    	violationsMap.put(act, msg);
	    }
	}
	
	public LinkedList<String> getAdvisoryCriterion() {
		LinkedList<String> msgs = new LinkedList<String>();

		for (ScheduleValidate sv : advisorys.values()) {
			sv.getCriterion(msgs);
		}

		return msgs;
	}

	public LinkedList<String> getViolationCriterion() {
		LinkedList<String> msgs = new LinkedList<String>();

		for (ScheduleValidate sv : violations.values()) {
			sv.getCriterion(msgs);
		}

		return msgs;

	}
	
	public LinkedList<String> getAdvisorySummary() {

		LinkedList<String> msgs = new LinkedList<String>();

		if(advisoriesMap.size() > 0)
		{
			for(Activity act : advisoriesMap.keySet())
			{
				String str;
	            // display Activity type, and IF scheduled, the scheduled time, other wise the activity time window
				str = act.getArlentrytype();
				
				if(isRecord(act) || isPlayback(act) || isDelete(act))
	            {
	                if(isRecord(act))
	                    str += ":REC";
	                if(isPlayback(act))
	                    str += ":PBK";
	            }
				if(isDDT(act))
					str += ":DDT";

	            if(act.getScheduled() == 1)
	            {
	            	str += " (eid " + act.getExtentid() + ") [" + TimeToString.AsLogTime(extentDao.get((long)act.getExtentid()).getStarttime()) +
	                	" - " + TimeToString.AsLogTime(extentDao.get((long)act.getExtentid()).getEndtime()) + "]";
	            }
	            else
	            {
	            	str += " (aid " + act.getId() + ") [" + TimeToString.AsLogTime(act.getStarttime()) + " - " + 
	                	TimeToString.AsLogTime(act.getEndtime()) +"]";
	            }
	            str += ", Sat: " + satDao.get((long)act.getSatelliteid()).getName() +
	            	": " + advisoriesMap.get(act);
	            
	            msgs.add(str);
			}
		}
		return msgs;
	}
	
	public LinkedList<String> getViolationSummary() {
		LinkedList<String> msgs = new LinkedList<String>();

		if(violationsMap.size() > 0)
		{
			for(Activity act : violationsMap.keySet())
			{
				String str;
	            // display Activity type, and IF scheduled, the scheduled time, other wise the activity time window
				str = act.getArlentrytype();
				
				if(isRecord(act) || isPlayback(act) || isDelete(act))
	            {
	                if(isRecord(act))
	                    str += ":REC";
	                if(isPlayback(act))
	                    str += ":PBK";
	            }
				if(isDDT(act))
					str += ":DDT";
				
				if(act.getScheduled() == 1)
	            {
	                str += " (eid " + act.getExtentid() + ")";
	            }
	            else
	            {
	                str += " (aid " + act.getId() + ")";
	            }

	            str += ", Sat: " + satDao.get((long)act.getSatelliteid()).getName() +
		            	": " + violationsMap.get(act);
		            
	            msgs.add(str);
			}
		}

		return msgs;
	}
	
	public void addActToUnschedule(Activity act){
		if (!ActsToUnschedule.contains(act))
			ActsToUnschedule.add(act);
	}
	
	// adds this activity and all activities in its chain to the given vector
	public Set<Activity> AddChainTo(Activity act)
	{
		Set<Activity> activities = new HashSet<Activity>();
		try
	    {
	    	activities.add(act);
		    
		    while (act.getNextactid()>0)
		    {
		    	act = actDao.get((long)act.getNextactid());
		    	if(!activities.contains(act))
		    		activities.add(act);
		    }
		    while (act.getPrevactid()>0)
		    {
		    	act = actDao.get((long)act.getPrevactid());
		    	if(!activities.contains(act))
		    		activities.add(act);
		    }
		    
	    } catch (Exception ex) {
	    	ex.printStackTrace();
	    }
		
	    return activities;
	}
	
	public boolean getSVStatus(){
		return validateStatus;
	}
	public TimeRange getSVTR(){
		return svTR;
	}
	public int getBranchid(){
		return branchid;
	}
	public Satellitep getSatellitep(){
		return satPs;
	}
	public Session getSession(){
		return svSession;
	}
	public int getSatId(){
		return satId;
	}
	
	public String getSatName(){
		return satName;
	}
	
	private boolean isRecord(Activity act){ 
		return act.getActsubtype().equalsIgnoreCase("REC") || act.getToytype().equalsIgnoreCase("TREC");
		
	}
	private boolean isPlayback(Activity act){
		return act.getActsubtype().equalsIgnoreCase("PBK") || act.getToytype().equalsIgnoreCase("TPBK");
	}
	private boolean isDDT(Activity act){
		return act.getArlentrytype().equalsIgnoreCase("RSI") && act.getActsubtype().equalsIgnoreCase("DDT");
	}
	private boolean isDelete(Activity act){
		return act.getArlentrytype().equalsIgnoreCase("DELETE");
	}
	
	public String getReturnMsg(){
		return this.returnMsg;
		
	}
}
