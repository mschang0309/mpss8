package mpss.sv;

import mpss.config;
import mpss.configFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class SvValidatorConsole 
{
	public static void main(String args[])
	{
		int sessionid = 7;//47267;//47257
		boolean isBranch = false;
		System.setProperty("satName", "FORMOSAT5");
		config.load();   
		try 
		{
			// Spring application context
			ApplicationContext context = new FileSystemXmlApplicationContext(javae.AppDomain.getPath("applicationContext.xml", new String[]{"conf",configFactory.getSatelliteName()}));
			org.apache.log4j.xml.DOMConfigurator.configure(javae.AppDomain.getPath("log4j.xml", new String[]{"conf",configFactory.getSatelliteName()}));   
			
			// Spring application context
			Validator validator = (Validator)context.getBean(configFactory.getValidator());
			if (validator == null) {
				System.out.println("Schedule Validator bean failed");
				return;
			}
			
			
		    //Schedule Validate
			if (validator.Validate(sessionid, isBranch))
			{
				System.out.println("Schedule Validator complete");
			}
			else
			{
				String msg = (validator.getReturnMsg() == null) ? "Schedule Validator failed" : validator.getReturnMsg();
				System.out.println(msg);
				return;
			}
    	
      } catch (Exception ex) {
    		ex.printStackTrace();
      }
      
	    
	}
}
