package mpss.sv.advisory;

import java.util.LinkedList;

import org.springframework.stereotype.Component;

import mpss.common.jpa.Activity;
import mpss.sv.ScheduleValidateBase;

@Component("svAdvUnschedAct")
public class SvAdvUnschedAct extends ScheduleValidateBase{
	private String SvAdvUnschedActMsg = "Not scheduled";
	
	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add("Unscheduled Activity: "+ SvAdvUnschedActMsg);
	}

	@Override
	public boolean ValidateActivity(Activity act) {
		if(act.getScheduled() == 0)
		{
		    if(act.getArlentrytype().equalsIgnoreCase("UPLINK"))
		    	super.getValidator().AddViolation(act, SvAdvUnschedActMsg);
		    else
		    	super.getValidator().AddAdvisory(act, SvAdvUnschedActMsg);
			
		    return false;
		}
		
		return true;
	}

}
