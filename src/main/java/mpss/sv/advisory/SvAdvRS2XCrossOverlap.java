package mpss.sv.advisory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import mpss.common.dao.ActivityDao;
import mpss.common.dao.ContactDao;
import mpss.common.dao.SatelliteDao;
import mpss.common.dao.SessionManDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Extent;
import mpss.sv.ScheduleValidateBase;
import mpss.sv.Validator;
import mpss.util.timeformat.RangeUtil;
import mpss.util.timeformat.TimeRange;
import mpss.util.timeformat.TimeToString;
import mpss.util.timeformat.TimeUtil;

@Component("svAdvRS2XCrossOverlap")
public class SvAdvRS2XCrossOverlap extends ScheduleValidateBase{

	@Autowired
	ActivityDao actDao;
	@Autowired
	ContactDao contDao;
	@Autowired
	SatelliteDao satDao;
	@Autowired
	SessionManDao sessionManDao;
	
	private String SvAdvRS2XCrossOverlapMsg = "XCROSS overlaps data transmission activity (RSI PBK or DDT) scheduled at: ";
	private String SvAdvRS2XCrossOverlapStartMsg = "XCROSS overlaps start of data transmission activity (RSI PBK or DDT) starting at: ";
	private String SvAdvRS2XCrossOverlapEndMsg = "XCROSS overlaps end of data transmission activity (RSI PBK or DDT) ending at: ";
	private String SvAdvRS2XCrossOverlapRFMsg = "XCROSS overlaps RF enable period starting at: ";

	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add("XCROSS Overlaps Start: " + SvAdvRS2XCrossOverlapStartMsg);
		msgs.add("XCROSS Overlaps End: " + SvAdvRS2XCrossOverlapEndMsg);
		msgs.add("XCROSS Overlaps RF: " + SvAdvRS2XCrossOverlapRFMsg);
	}

	@Override
	public boolean ValidateExtents(Set<Extent> extents) 
	{	
		boolean status = true;
		
	    // filter extents into 2 seperate vectors - XCROSS, then RSI PBK/DDT
	    List<Extent> xcrossExtents = new ArrayList<Extent>();
	    List<Extent> xmitExtents = new ArrayList<Extent>();
	    for(Extent ext : extents)
	    {	    	
	    	Activity act = actDao.get((long)ext.getActivityid());
	    	String extType = act.getArlentrytype();

	        if(extType.equalsIgnoreCase("XCROSS"))
	            xcrossExtents.add(ext);
	        else if(extType.equalsIgnoreCase("RSI"))
	        {
	            if(act.getActsubtype().equalsIgnoreCase("PBK") || act.getActsubtype().equalsIgnoreCase("DDT"))
	                xmitExtents.add(ext);
	        }
	    }

	    // sort by Start time
		Collections.sort(xcrossExtents, new Comparator<Extent>() {
			@Override
			public int compare(Extent arg0, Extent arg1) {
				return arg0.getStarttime().compareTo(arg1.getStarttime());
			}
		});
		Collections.sort(xmitExtents, new Comparator<Extent>() {
			@Override
			public int compare(Extent e1, Extent e2) {
				return e1.getStarttime().compareTo(e2.getStarttime());
			}
		});
    	
	    status = CheckForRFEnableOverlap(xcrossExtents, xmitExtents);
	    status = CheckForXmitOverlaps(xcrossExtents, xmitExtents) && status;

		return status;
	}
	
	private boolean CheckForRFEnableOverlap(List<Extent> xcrossExtents, List<Extent> xmitExtents)
	{
		boolean status = true;
		
	    // sort xmit extents into sequences
	    Map<TimeRange, List<Extent>> xmitSequences = new HashMap<TimeRange, List<Extent>>();
	    GroupXmitExtentsIntoSequences(xmitExtents, xmitSequences);

	    // iterate through xcross extents, checking if any overlap the RF enable time of a sequence
	    // if so, report the xcross overlap message
	    for(Extent xcross : xcrossExtents)
	    {
	        // get time window of xcross extent
	        TimeRange xcrossWindow = new TimeRange(xcross.getStarttime(), xcross.getEndtime());

	        // iterator through RFwindow/xmit sequence retrieving RF window for each sequence
	        for(TimeRange rfWindow : xmitSequences.keySet())
	        {
	            // get rf window from sequence iterator
	            Timestamp rfEnableTime = rfWindow.first;

	            if(xcrossWindow.contains(rfEnableTime))
	            {
	                String msg = SvAdvRS2XCrossOverlapRFMsg + TimeToString.AsLogTime(rfEnableTime);
			        super.getValidator().AddViolation(actDao.get((long)xcross.getActivityid()), msg);
			        status = false;
	                break;
	            }
	        }
	    }

		return status;
	}
	
	private boolean CheckForXmitOverlaps(List<Extent> xcrossExtents, List<Extent> xmitExtents)
	{
		boolean status = true;
		Validator Validator = super.getValidator();

	    // iterate through xcross events, checking if one overlaps the START of an xmit extent
	    // if so, report the xcross overlap message
	    for(Extent xcross : xcrossExtents)
	    {
	    	Activity act = actDao.get((long)xcross.getActivityid());
	        // get time window of xcross extent
	        TimeRange xcrossWindow = new TimeRange(xcross.getStarttime(),xcross.getEndtime());

	        for(Extent xmit : xmitExtents)
	        {
	            // get start time of xmit extent
	            Timestamp xmitStart = xmit.getStarttime();
	            Timestamp xmitStop = xmit.getEndtime();
	            TimeRange xmitWindow = new TimeRange(xmitStart,xmitStop);

	            // if the xcross window contains the starttime of the xmit extent,then report the overlap message
	            if(xcrossWindow.contains(xmitStart))
	            {
	                String msg = SvAdvRS2XCrossOverlapStartMsg + TimeToString.AsLogTime(xmitStart);
	                Validator.AddViolation(act, msg);
			        status = false;
	            }
	            // check if xcross window contains the stop time of the xmit extent,report overlap message if true
	            else if(xcrossWindow.contains(xmitStop))
	            {
	                String msg = SvAdvRS2XCrossOverlapEndMsg + TimeToString.AsLogTime(xmitStop);
	                Validator.AddViolation(act, msg);
			        status = false;
	            }
	            else if(xmitWindow.encloses(xcrossWindow))
	            {
	                String msg = SvAdvRS2XCrossOverlapMsg + TimeToString.AsLogTime(xmitWindow);
	                Validator.AddAdvisory(act, msg);
			        status = false;
	            }
	        }
		}

		return status;
	}
	
	private void GroupXmitExtentsIntoSequences(List<Extent> xmitExtents, Map<TimeRange, List<Extent>> xmitSequences)
	{
		Map<TimeRange, List<Extent>> MapRangeExtents = new HashMap<TimeRange, List<Extent>>();
		Map<Integer, Set<Extent>> contExtentsMap = new HashMap<Integer, Set<Extent> >(); //key contactId
		// vector of non-contact extents
		List<Extent> ncExtents = new ArrayList<Extent>();
		
		// iterate through extents, creating contact/extents mapping
		for(Extent extent : xmitExtents)
		{
			// does this extent use a contact
			if(extent.getContactid() > 0)
			{
				// get contact from extent
				Contact contact = contDao.get((long)extent.getContactid());
				int contId = contact.getId().intValue();
				
				if (contExtentsMap.get(contId) == null)
	        	{
					contExtentsMap.put(contId, new HashSet<Extent>());
	        	}
				Set<Extent> extSet = contExtentsMap.get(contId);
				try
				{
					extSet.add(extent);
				}
				catch (Exception e)
				{
					logger.info("Can not add contExtentsMap (" + e.getMessage() + ")");
				}
			}
			// must not contain contact pointer
			else
				ncExtents.add(extent);
		}
		
		// vector of nc extents that do not occur during a contact
		List<Extent> nonConExtents = new ArrayList<Extent>();
		// now determine if any of the ncextents occur during a contact
		for(Extent ncExtent : ncExtents)
		{
			// get nc extent time window
			TimeRange ncWindow = new TimeRange(ncExtent.getStarttime(), ncExtent.getEndtime());
			boolean duringContact = false;
			
			// iterator through contact/extents mapping
			for(int csIter : contExtentsMap.keySet())
			{
				// get contact time window
				Contact cont = contDao.get((long)csIter);
				TimeRange conWindow = new TimeRange(cont.getRise(), cont.getFade());
				
				// if contact window contains the nc window, then add the nc to the
				// listing of extents during the contact
				if(conWindow.encloses(ncWindow))
				{
					contExtentsMap.get(csIter).add(ncExtent);
					duringContact = true;
				}
			}
			
			if(!duringContact)
				nonConExtents.add(ncExtent);
		}
		
		// add contact related extents to RF mapping
		for(int csIter : contExtentsMap.keySet())
		{
			Contact cont = contDao.get((long)csIter);
			Set<Extent> exts= contExtentsMap.get(csIter);
			
			Extent[] extsArray = exts.toArray(new Extent[exts.size()]);
			// get extent sequence, sort then add to RFWindowToXmitSequence
			Arrays.sort(extsArray, new Comparator<Extent>() {
				@Override
				public int compare(Extent e1, Extent e2) {
					return e1.getStarttime().compareTo(e2.getStarttime());
				}
			});
			
			// get parameter set associated with schedule and relative resources
			int sessionId = super.getValidator().getSession().getId().intValue();
			
			// get satellite id from contact
			int satId = cont.getSatelliteid();
			
			// Get TWTA preheat period
			int TWTA = sessionManDao.getSatellitep(sessionId, satId).getTwtaheatdelay();
			
			// get start time of first extent in vector, subtract off TWTA to determine RF enable time
			Extent firstExtent = extsArray[0];
			Extent lastExtent = extsArray[exts.size()-1];
			
			// need to determine when the transmission of data starts within the extent
			TimeRange firstXmit = super.getOperationalPeriod(firstExtent);
			
			// calculate RFEnable start time
			Timestamp RFEnableStart = TimeUtil.timeAppendSec(firstXmit.first, - TWTA);
			
			// generate rfwindow - from rfenable start time to end of last extent(at least close enough)
			TimeRange rfWindow = new TimeRange(RFEnableStart, lastExtent.getEndtime());
			
			// add rfwindow to sequence to map
			MapRangeExtents.put(rfWindow, Arrays.asList(extsArray));
		}
		
		if(nonConExtents.size() > 0)
		{
			Extent ext = nonConExtents.get(0);
			
			// Get RFPA duration and RSI Imager preheat duration to determine time range
			// for RECORD and DDT extents
			Activity act = actDao.get((long)ext.getActivityid());
			int rsiPreheat = sessionManDao.getSatellitep(act.getSessionid(), act.getSatelliteid()).getRsisecondarypreheatdelay();
			int rfpa = sessionManDao.getSatellitep(act.getSessionid(), act.getSatelliteid()).getRsifocalplanedelay();
			
			// imaging duration is RFPA - rsi preheat.  Assuming RSI Preheat is not > than RFPA
			int imagingDuration = rfpa - rsiPreheat;
			
			// need to get the start of the imaging of the activity - not the start of the extent
			// when determining imaging range duration
			TimeRange firstImaging = super.getOperationalPeriod(ext);
			
			// create imaging duration from first imaging start to end of first imaging start + imaging duration 
			TimeRange imagingRange = RangeUtil.timeAppendSec(firstImaging.first, - rsiPreheat, imagingDuration);
			
			// add non-contact related extents to RF mapping
			for(Extent ncX : nonConExtents)
			{
				// create ScActivity from extent to get the "Operational" period of the activity
				// Operational period does not contain the setup or cleanup times, which we don't want
				TimeRange operationalPeriod = super.getOperationalPeriod(ncX);
				
				// if operating range is contained within imaging range copy all events into vector for later processing
				if(imagingRange.encloses(operationalPeriod))
				{
					boolean isFind = false;
					for (TimeRange tr : MapRangeExtents.keySet())
					{
						if (tr.first.equals(imagingRange.first) && tr.second.equals(imagingRange.second))
						{
							isFind = true;
							MapRangeExtents.get(tr).add(ncX);
						}
					}
					
					if(!isFind)
					{
						List<Extent> es = new ArrayList<Extent>();
						es.add(ncX);
						MapRangeExtents.put(imagingRange, es);
					}
				}
				else
				{
					// update operational range based on new extent
					Timestamp newStart = operationalPeriod.first;
					imagingRange = RangeUtil.timeAppendSec(newStart, - rsiPreheat, imagingDuration);
					
					List<Extent> es = new ArrayList<Extent>();
					es.add(ncX);
					MapRangeExtents.put(imagingRange, es);
				}
			}
		}
		
		xmitSequences = MapRangeExtents;
		
		// dump sequences
		for(TimeRange t: MapRangeExtents.keySet())
		{
			logger.info("Sequence: " + t.first + " ~ " + t.second);
			for(Extent ex : MapRangeExtents.get(t))
			{
				Activity ac = actDao.get((long)ex.getActivityid());
				logger.info(ex.getId() + " " + ac.getArlentrytype() + "." + ac.getActsubtype());
			}
		}
	}

}
