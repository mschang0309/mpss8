package mpss.sv.advisory;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mpss.common.dao.ActivityDao;
import mpss.common.dao.ExtentDao;
import mpss.common.dao.SessionManDao;
import mpss.common.dao.SitepsDao;
import mpss.common.jpa.Activity;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Satellitep;
import mpss.common.jpa.Siteps;
import mpss.sv.ScheduleValidateBase;
import mpss.sv.Validator;
import mpss.util.timeformat.RangeUtil;
import mpss.util.timeformat.TimeRange;

@Component("svAdvSiteTurnArnd")
public class SvAdvSiteTurnArnd extends ScheduleValidateBase{
	@Autowired
	SitepsDao sitepsDao;
	@Autowired
	ExtentDao extentDao;
	@Autowired
	ActivityDao activityDao;
	@Autowired
	SessionManDao sessionManDao;
	
	private String SvAdvSiteTurnArndMsg = "Exceeded site turnaround time for handling 2 consecutive contacts: ";

	@Override
	public void getCriterion(LinkedList<String> msgs) {
		msgs.add("Site turn around time: " + SvAdvSiteTurnArndMsg + "<site default>");
	}

	@Override
	public boolean ValidateContacts(List<Contact> contacts) {
		boolean status = true;
//		int extension = validator.extension;

		if (contacts.size() > 1) {
			
			Validator validator = super.getValidator();
			int psId = validator.getSession().getPsid();
			int siteId =contacts.get(0).getSiteid();
			// for site, get the turnaround
			Siteps ps = sitepsDao.getBySiteIdNPsId(siteId, psId);
			if (ps == null)
				return true;
			int turnArnd = sitepsDao.getBySiteIdNPsId(siteId, psId).getTurnaroundtime();
			// sort by rise time
			Collections.sort(contacts, new Comparator<Contact>() {
				@Override
				public int compare(Contact arg0, Contact arg1) {
					return arg0.getRise().compareTo(arg1.getRise());
				}
			});
			
			for (int i = 0; i < contacts.size() - 1; i++ )
			{
				Contact c1 =contacts.get(i);
				Contact c2 =contacts.get(i+1);
				if((getBaseTime(c2).first.getTime() - getBaseTime(c1).second.getTime()) 
						< turnArnd * 1000)
			    {
				    // need to find activity from nextCon that violated site turnaround time.
				    List<Extent> extents = extentDao.getByContactId(c2.getId().intValue());
				    if (extents.size() > 0)
				    {
				    	String msg = SvAdvSiteTurnArndMsg + turnArnd;
						validator.AddAdvisory(activityDao.get((long)extents.get(0).getActivityid()), msg);
					    status = false;
				    }
				    
			    }
			}
		}

		return status;
	}
	
	private TimeRange getBaseTime(Contact cont)
	{
		TimeRange baseTR =new TimeRange(cont.getRise(), cont.getFade());
		
		List<Extent> exts = extentDao.getByContactId(cont.getId().intValue());
		if (exts !=null)
		{
			//Range<Timestamp> expandTR;
			for(int i=0; i < exts.size(); i++)
			{
				Timestamp startTime = exts.get(i).getStarttime();
				Timestamp endtime = exts.get(i).getEndtime();
				Activity act = activityDao.get((long)exts.get(i).getActivityid());
				Satellitep satps = sessionManDao.getSatellitep(act.getSessionid(), act.getSatelliteid());
				
				int setupDelay = 0;
				int cleanupDelay = 0;
				if (act.getActsubtype().equalsIgnoreCase("PBK"))
				{
					setupDelay = satps.getRsipbsetupdelay();
					cleanupDelay = satps.getRsipbcleanupdelay();
				}

				// calculate start/stop times buy considering ssr setup/cleanup times
				TimeRange tRange = RangeUtil.rangeAppendSec(new TimeRange(startTime, endtime) , setupDelay, - cleanupDelay);
			    
			    if( i==0 )
			    {
			    	baseTR = tRange;
			    }
			    else
			    {
			    	Timestamp first = baseTR.first.before(tRange.first) ? baseTR.first : tRange.first;
			    	Timestamp second = baseTR.second.after(tRange.second) ? baseTR.second : tRange.second;
			    	baseTR.closed(first, second);
			    }
			}
		}
		
		return baseTR;
	}

}
