package mpss.sv;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;


import mpss.common.jpa.Activity;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Extent;
import mpss.util.timeformat.TimeRange;


public interface ScheduleValidate {

	public void setValidator(Validator validator);
	public void generate(Extent extent);
	public void generate(Activity activity);
	
	public void getCriterion(LinkedList<String> msgs);
	public boolean ValidateActivity(Activity act);
	public boolean ValidateExtent(Extent extent);
	public boolean ValidateExtents(Set<Extent> extents);
	public boolean ValidateContacts(List<Contact> contacts);
	
	public TimeRange getOperationalPeriod(Extent extent);
}
