package mpss.common.dao.tests;

import java.util.*;
import java.text.SimpleDateFormat;
import java.sql.Timestamp;

import mpss.common.jpa.*;
import mpss.common.dao.*;
import mpss.schedule.base.FwActivityImpVector;
import mpss.schedule.base.FwImagingModeImp;
import mpss.schedule.base.GlDate;
import mpss.schedule.base.GlTime;
import mpss.schedule.base.GlTimeRange;
import mpss.schedule.imp.*;
import mpss.util.timeformat.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

//import static org.junit.Assert.*;
import org.junit.Test;


@ContextConfiguration(locations = { "/applicationContext.xml" })
public class TestDao1 extends AbstractTransactionalJUnit4SpringContextTests {

	@Autowired
	protected ArlDao arlDao;

	@Autowired
	protected ActivityDao activityDao;

	@Autowired
	protected DutycycleDao dutycycleDao;

	@Autowired
	protected ImagingmodeDao imagingmodeDao;

	@Autowired
	protected SessionDao sessionDao;
	
	@Autowired
	protected ContactDao contactDao;
	
	@Autowired
	protected ExtentDao extentDao;

	 @Autowired
	 protected SatelliteDao satelliteDao;
	 
	 @Autowired
	 protected SessionSectionDao ssDao;

//	@Autowired
//	protected TestImpFactory test;

	/** The factory that produces entity manager. */
	// private static EntityManagerFactory emf;
	/** The entity manager that persists and queries the DB. */
	 
	
	 
	@Test
	public void testSessSection() {
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
//		Timestamp compareTime = Timestamp.valueOf("1970-01-01 " + currentTime.getHours() + ":" +currentTime.getMinutes()+":" + currentTime.getSeconds());
		Timestamp compareTime = Timestamp.valueOf("1970-01-01 03:00:00");
		System.out.println("compareTime: " + compareTime.toString());
		SessionSection ss = ssDao.getSectionbyTime(compareTime);
		if (ss == null)
		{
			compareTime.setDate(2);
			System.out.println("compareTime2: " + compareTime.toString());
			ss = ssDao.getSectionbyTime(compareTime);
		}
		System.out.println("Section: " + ss.getSectionname());
		
		
		

	}
	
	
	@Test
	public void test1() {
		Satellite obj = satelliteDao.getByOps4number("OP31");
		
		
		System.out.println("test result: " + obj.getName());

	}

	@Test
	public void testGetById() {
		//Activity obj = activityDao.getActivityById(Long.valueOf(2));
		//Contact obj = contactDao.getContactById(Long.valueOf(338397));
		//Extent obj = extentDao.getExtentById(Long.valueOf(2));
		Satellite obj = satelliteDao.getSatelliteById(Long.valueOf(1));

		System.out.println("obj : " + obj.getName());

	}

	@Test
	public void testGetNextId() {
		
		System.out.println("new id : " + satelliteDao.getNextId());

	}

	@Test
	public void testGetDutycyclesBySatPsId() {

		List<Dutycycle> dList = dutycycleDao.getBySatPsId(74);
		for (Dutycycle d : dList)
			System.out.println("acttype : " + d.getActivitytype());
		// System.out.println("aaaaa");
		// Dutycycle d = dutycycleDao.getById(41);
		// System.out.println("acttype : " + d.getActivitytype());

	}

	@Test
	public void testGetDutycyclesById() {

		System.out.println("aaaaa");
		Dutycycle d = dutycycleDao.getById(41);
		System.out.println("acttype : " + d.getActivitytype());

	}

	@Test
	public void testGetImagingModeByInstrument() {
		System.out.println("iiiii");
		Imagingmode i = imagingmodeDao.getImagingModeByInstNModeid("RSI", 1);
		System.out.println("Mode Name: " + i.getModename());

		// List<Imagingmode> list =
		// imagingmodeDao.getImagingModeByInst("ISUAL");
		//
		// for (Imagingmode i : list)
		// System.out.println("Mode Name: " + i.getModename());

	}

	@Test
	public void testInsSession() {
		Date d = new Date();
		Session s = new Session();
		s.setId(Long.valueOf(1));
		s.setAcqsumloaded(2);
		s.setColdscheduled(3);
		s.setDescription("abc");
		s.setEndtime(new Timestamp(d.getTime()));
		s.setIsbranch(0);
		s.setLastsave(new Timestamp(d.getTime()));
		s.setName("111");
		s.setNeedsvalidation(4);
		s.setNominalapplied(5);
		s.setPsid(6);
		// s.setSatArlXrefs(null);
		// s.setSatXrefs(null);
		// s.setSiteXrefs(null);
		s.setStarttime(new Timestamp(d.getTime()));

		sessionDao.create(s);

		long i = sessionDao.getMaxId();

		System.out.println("Session Name: " + s.getName() + "   New Id:" + i);
	}

	@Test
	public void testGetSessionMaxId() {
		System.out.println("ssss");
		long i = sessionDao.getMaxId();

		System.out.println("Max Id: " + i);
	}

	@Test
	public void testDelSession() {
		System.out.println("dddd");
		Session s = sessionDao.getSessionById(47245);
		sessionDao.delete(s);

		System.out.println("del ok");
	}

	@Test
	public void testGetAssociatedAct() {
		System.out.println("ssss");
		Collection result = activityDao.getAssociatedAct(1896278);

		ArrayList al = new ArrayList(result);
		for (int i = 0; i < al.size(); i++) {
			Activity vo = (Activity) al.get(i);
			System.out.println(i + " =  " + vo.getId());
		}
	}

	@Test
	public void testCollection() {
		System.out.println("ssss");
		// Collection result = activityDao.getByEntryIdNType(402005, "RSI");
		//Collection result = activityDao.getBySessionIdNTimerange(47233, "2014-01-19 14:00:55", "2014-01-19 14:08:31");
		Collection result = activityDao.getUnschBySessionIdNTimerange(47243, "2014-01-20 13:02:01", "2014-01-20 16:02:01");

		ArrayList al = new ArrayList(result);
		for (int i = 0; i < al.size(); i++) {
			Activity vo = (Activity) al.get(i);
			System.out.println(i + " =  " + vo.getId() + "   start:" + vo.getStarttime());
		}
	}

	@Test
	public void testImagingmodeDao() {
		System.out.println("ssss");
		Imagingmode imp = imagingmodeDao.getImagingModeByInstNModeid("RSI", 1);

		System.out.println("val: " + imp.getModename());
	}

	@Test
	public void testImagingModeImpFactory() {
		try {

			try {
				System.loadLibrary("ScDll");
			} catch (UnsatisfiedLinkError e) {
				System.err.println("Native code library failed to load. See the chapter on Dynamic Linking Problems in the SWIG Java documentation for help.\n" + e);
				System.exit(1);
			}

			ImagingModeImpFactory fac = new ImagingModeImpFactory();

			System.out.println(fac.myVal);
			FwImagingModeImp imp = fac.Get(1, "RSI");
			if (fac != null)
				System.out.println("getModename value=" + imp.GetMode());
			else
				System.out.println("value=nothing");

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}

	@Test
	public void testActivityImpFactory() {
		System.out.println("aaa");
		FwActivityImpVector v = null;
		ActivityImpFactory actimp = new ActivityImpFactory();
		actimp.GetAll(v, 47233);
		System.out.println(" size=  " + v.size());

	}

	@Test
	public void testTimeRange() {

		DateUtil du = new DateUtil();
		try {
			System.loadLibrary("ScDll");
		} catch (UnsatisfiedLinkError e) {
			System.err.println("Native code library failed to load. See the chapter on Dynamic Linking Problems in the SWIG Java documentation for help.\n" + e);
			System.exit(1);
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			GlTime time = new GlTime();
			System.out.println("GLTime Systime= " + time.asString("%Y-%m-%d %H:%M:%S"));

			Date d = new Date();
			System.out.println("Date systime=  " + sdf.format(d));

			 
			GlDate glDate = GlDate.now();
			GlTime glTime = GlTime.now();

			long delta = du.getClSeconds((int)glDate.year(), (int)glDate.month()-1, (int)glDate.dayOfMonth(), (int)glTime.hour(), (int)glTime.minute(), (int)glTime.second());

			GlTime aa = new GlTime(delta);
			System.out.println("GlTime created by delta = "+aa.asString("%Y-%m-%d %H:%M:%S"));

			
			GlTimeRange tr = new GlTimeRange(new GlTime(delta), new GlTime(delta));

			GlTime start = tr.GetStartTime();
			GlTime end = tr.GetEndTime();
			String str = start.asString("%Y-%m-%d %H:%M:%S");
			System.out.println("sys from Timerange start=  " + str.toString());

			System.out.println("-----------------------------  ");
			Date dateStart = sdf.parse("2009-02-05 14:31:00");
			System.out.println("Date dateStart=  " + sdf.format(dateStart));
			Date dateEnd = sdf.parse("2009-02-06 15:34:03");
			Calendar cal = Calendar.getInstance();
			cal.setTime(dateStart);
			System.out.println("cal1="+sdf.format(cal.getTime()));
			long delta1 = du.getClSeconds(cal.get(cal.YEAR), cal.get(cal.MONTH) , cal.get(cal.DATE), cal.get(cal.HOUR_OF_DAY), cal.get(cal.MINUTE), cal.get(cal.SECOND));
			
			cal.setTime(dateEnd);
			System.out.println("cal2="+sdf.format(cal.getTime()));
			long delta2 =  du.getClSeconds(cal.get(cal.YEAR), cal.get(cal.MONTH) , cal.get(cal.DATE), cal.get(cal.HOUR_OF_DAY), cal.get(cal.MINUTE), cal.get(cal.SECOND));
			
			GlTimeRange tr2 = new GlTimeRange(new GlTime(delta1), new GlTime(delta2));
			GlTime a = tr2.GetStartTime();
			GlTime b = tr2.GetEndTime();
			String strStart = a.asString("%Y-%m-%d %H:%M:%S");
			String strEnd = b.asString("%Y-%m-%d %H:%M:%S");

			System.out.println("strStart=  " + strStart.toString());
			System.out.println("strEnd=    " + strEnd.toString());

		} catch (Exception ex) {
			System.out.println("ex" + ex.toString());
		}

	}

	public long returnSeconds(int year, int month, int date, int hour, int min, int sec) {
		Calendar calendar1 = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		calendar1.set(1990, 0, 1, 8, 0, 0);

		calendar2.set(year, month, date, hour, min, sec);
		long milliseconds1 = calendar1.getTimeInMillis();
		long milliseconds2 = calendar2.getTimeInMillis();
		long diff = milliseconds2 - milliseconds1;
		long seconds = diff / 1000;
		return seconds;
	}

	@Test
	public void testDateUtil() {
		System.out.println("aaa");
		DateUtil du = new DateUtil();
		String str = "2009-02-05 14:31:00";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date d = du.stringToDate(str, "yyyy-MM-dd HH:mm:ss");

		Timestamp timeStampDate = du.dateToTimestamp(d);
		System.out.println("Today is " + timeStampDate);
		System.out.println("Today2 is " + timeStampDate.toString());
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(timeStampDate.getTime()));
		System.out.println("cal="+sdf.format(cal.getTime()));
		System.out.println("cal="+cal.get(cal.YEAR)+" "+ cal.get(cal.MONTH)+" "+ cal.get(cal.DATE)+" "+ cal.get(cal.HOUR_OF_DAY)+" "+ cal.get(cal.MINUTE)+" "+ cal.get(cal.SECOND));

	}
}
