package mpss.common.dao.tests;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.text.SimpleDateFormat;
import java.sql.Timestamp;

import javax.persistence.*;
import javax.xml.bind.*;

import mpss.common.jpa.*;
import mpss.common.dao.*;
import mpss.mtl.MTLLoader;
import mpss.oe.*;
import mpss.rest.*;
import mpss.se.RS2SSRDeleteEvent;
import mpss.util.timeformat.TimeRange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

//import static org.junit.Assert.*;

import org.eclipse.persistence.sessions.CopyGroup;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Range;

@ContextConfiguration(locations = { "/applicationContext.xml" })
public class TestDaoClasses extends
		AbstractTransactionalJUnit4SpringContextTests {

	// @Autowired
	// protected CmdStructureDao cmdStructureDao;

	@Autowired
	protected SatelliteDao satelliteDao;

	@Autowired
	protected RevDao revDao;

	@Autowired
	protected OrbitEventsDao oeDao;

	@Autowired
	private OrbitResourceBean oeResource;

	@Autowired
	protected OrbitEventsLoader orbitEventsLoader;

	@Autowired
	protected PsDao psDao;

	@Autowired
	protected SessionDao sessionDao;

	@Autowired
	protected ActivityRequestListDao arListDao;

	@Autowired
	protected SessionManDao sessionManDao;

	// @Autowired
	// protected Session s;

	@Autowired
	protected ExtentDao extentDao;

	@Autowired
	protected ActivityDao activityDao;

	@Autowired
	protected ContactDao contactDao;
	
	@Autowired
	protected ArlDao arlDao;

	@Autowired
	protected RecorderpDao recpDao;
	
	@Autowired
	protected ImagingmodeDao imagingmodeDao;
	
	@Autowired
	protected UnavailableResourcesDao unavailableResourcesDao;
	@Autowired
	MTLLoader mtlLoader;
	@Autowired
	SitepsDao sitepsDao;

	/** The factory that produces entity manager. */
	// private static EntityManagerFactory emf;
	/** The entity manager that persists and queries the DB. */
	
	@Test
	public void testGetdutycycle()
	{
		Dutycycle dc = sessionManDao.getDutycycle(47243, 1, "RSI", "PBK");
		System.out.println("Dutycycle: " + dc.getDutycycles());
	}
	
	@Test
	public void testGetBySiteIdNPsId()
	{
		Siteps ps = sitepsDao.getBySiteIdNPsId(2, 383);
		System.out.println("id: " + ps.getId());
	}
	@Test
	public void testgetExtentsByRSICapacity()
	{
		Timestamp st = Timestamp.valueOf("2014-01-20 17:04:00");
		List<Extent> exs = extentDao.getExtentsByRSICapacity("DELETE",st,12,47243);
		System.out.println("size: " + exs.size());
		for (Extent ex : exs)
		{
			System.out.println("extent id: " + ex.getId());
		}
		
	}
//	@Test
//	public void testGenPbkDur()
//	{
//		this.mtlLoader.scId =1;
//		this.mtlLoader.sessionId = 47243;
//		int PANdur = mtlLoader.genPbkDur(402248, "PAN");
//		System.out.println("PAN: " + PANdur);
//		int MSdur = mtlLoader.genPbkDur(402248, "MS");
//		System.out.println("MS: " + MSdur);
//	}
	
	@Test
	public void testIsUnavailableTime() {
		Timestamp st = Timestamp.valueOf("2010-12-28 21:35:00");
		Timestamp et = Timestamp.valueOf("2010-12-28 23:00:00");
		boolean status = this.unavailableResourcesDao.isUnavailableTime(47243,"ANT",st,et, 0);
		System.out.println("IsUnavailableTime status: " + status);
		// return act;
	}
	
	@Test
	public void testGetUnschByRangeNBranchId() {
		TimeRange tr = new TimeRange(Timestamp.valueOf("2014-01-20 13:40:00.0"), Timestamp.valueOf("2014-01-21 13:40:00.0"));
		List <Activity> acts = this.activityDao.getUnschByRangeNBranchId(19243,0, tr);
		System.out.println("Activity Size : " + acts.size());
		// return act;
	}
	
	@Test
	public void testUnavailableTimeRange() {
		Timestamp STime = Timestamp.valueOf("2014-01-19 13:40:00");
		Timestamp ETime = Timestamp.valueOf("2014-01-20 13:40:00");
		Timestamp NSTime = Timestamp.valueOf("2014-01-20 00:08:50");
		Timestamp NETime = Timestamp.valueOf("2014-01-20 03:50:02");
		boolean firstAdd = true;
		
		//Mapping Unavailtimerang
		List<Unavailtimerange> unavailTR = unavailableResourcesDao.getSitesUnavailableTimes(STime, ETime, 0);
		Map<Integer, Set<Range> > unTRMap = new HashMap<Integer, Set<Range> >();
		for (Unavailtimerange tr : unavailTR)
		 {
			 if (unTRMap.get(tr.getParentid()) == null)
        	 {
				 unTRMap.put(tr.getParentid(), new HashSet<Range>());
        	 }
			 Set<Range> trList = unTRMap.get(tr.getParentid());
			 try
        	{
        		//System.out.println("site id:" + tr.getParentid());
        		trList.add(Range.closed(tr.getStarttime(), tr.getEndtime()));
        	}
        	catch (Exception e)
        	{
        	    System.out.println("Can not add Unavailable TimeRange Map (" + e.getMessage() + ")");
        	}
		 }
		//Print Unavailtimerang
		 for (Integer id : unTRMap.keySet())
		 {
			 System.out.println("Site Id : [" + id + "] " + unTRMap.get(id).size() + " Rows");
			 for (Range rg : unTRMap.get(id))
			 {
				 System.out.println("Unavail Timerange : " + rg.lowerEndpoint() + " ~ " + rg.upperEndpoint());
			 }
		 }
		 System.out.println("Total Unavail TR : " + unTRMap.keySet().size() + " Site Rows " + unavailTR.size() + " TR Rows ");

		//Mapping Contact
		 List<Contact> cons = contactDao.getBySiteTypeNSatNBranchNRange("XBAND",1, 0, STime, ETime,27423);
		 Map<Integer, Set<Contact> > contactListMap = new HashMap<Integer, Set<Contact> >();
		 for (Contact cn : cons)
		 {
			 if (contactListMap.get(cn.getSiteid()) == null)
        	 {
				 contactListMap.put(cn.getSiteid(), new HashSet<Contact>());
        	 }
			 Set<Contact> consList = contactListMap.get(cn.getSiteid());
			 try
        	{
        		//System.out.println("site id:" + cn.getSiteid());
        		consList.add(cn);
        	}
        	catch (Exception e)
        	{
        	    System.out.println("Can not add contact Map (" + e.getMessage() + ")");
        	}
		 }
		//Print Contact
		 for (Integer id : contactListMap.keySet())
		 {
			 System.out.println("contact Site Id : [" + id + "] " + contactListMap.get(id).size() + " Rows");
			 for (Contact c : contactListMap.get(id))
			 {
				 System.out.println("Contact : " + c.getRise() + " ~ " + c.getFade());
			 }
		 }
		 System.out.println("Total : " + contactListMap.keySet().size() + " Site Rows " + cons.size() + " Contact Rows ");
		 
		
		//Filter Contact
		 List<Contact> filterCons = new ArrayList<Contact>();
		 for (Integer id : contactListMap.keySet())
		 {
			 if (!unTRMap.keySet().contains(id))
			 {
				 System.out.println("contact Site Id : [" + id + "] " + contactListMap.get(id).size() + " Rows not in Unavail Timerange");
				 for (Contact c : contactListMap.get(id))
				 {
					System.out.println("Contact : " + c.getRise() + " ~ " + c.getFade() + " -- Avail");
				 }
			 }
			 else
			 {
				 System.out.println("contact Site Id : [" + id + "] " + contactListMap.get(id).size() + " Rows in Unavail Timerange");
				 for (Contact c : contactListMap.get(id))
				 {
					 System.out.println("Check Contact : " + c.getRise() + " ~ " + c.getFade());
					 for (Range rg : unTRMap.get(id))
					 {
						 System.out.println("Check Unavail Timerange : " + rg.lowerEndpoint() + " ~ " + rg.upperEndpoint());
						 if(c.getRise().compareTo((Timestamp) rg.upperEndpoint()) <= 0 && c.getFade().compareTo((Timestamp) rg.lowerEndpoint()) >= 0)
						 {
							 System.out.println("Unavail Timerange -- Remove");
							 cons.remove(c);
							 break;
						 }
						 else
						 {
							 System.out.println(" -- Avail");
						 }
						 
					 }
				 }
			 }
		 }
		 
		 System.out.println("Filter Contact : " + cons.size() + " Rows");
		 for (Contact cn : cons)
		 {
			 if (cn.getRise().compareTo(NSTime) >= 0 && cn.getRise().compareTo(NETime) <= 0)
			 {
				 System.out.println("Match Contact : " + cn.getRise() + " ~ " + cn.getFade());
			 }
		 }
	}

	
	
	@Test
	public void testGetBySessionAndSat() {
		Recorderp recp = recpDao.getBySessionAndSat(19243, 1);
		System.out.println("Recorderp : " + recp.getCapacity());

	}
	
	@Test
	public void testGetArlByName() {
		Arl arl = arlDao.getByName("man_test2");
		// for (String sesionName : sesionNames)
		// System.out.println("sesionNames : " + sesionName);
		System.out.println("arl type : " + arl.getArltype());

	}
	
	@Test
	public void testGetSessionNamesByRange() {
		TimeRange range = new TimeRange(
				Timestamp.valueOf("2009-02-01 00:00:00"),
				Timestamp.valueOf("2009-02-28 00:00:00"));
		List<Session> sesions = sessionDao.getSessionsbyRange(range, 0);
		// for (String sesionName : sesionNames)
		// System.out.println("sesionNames : " + sesionName);
		if (!sesions.isEmpty())
		{
			System.out.println("Sesions : " + sesions);
		}
		else
		{
			System.out.println("Sesions : null");
		}
		

	}

	@Test
	public void testgetActivityname() {
		int activityId = 2826;
		System.out.println("Start getActivity ActivityId : " + activityId);
		Activity act = this.activityDao.get((long) activityId);
		System.out.println("End getActivity ActivityName : " + act.getName());
		// return act;
	}

	// private static EntityManager em = null;

	// @BeforeClass
	// public static void setUpBeforeClass() throws Exception {
	// if (em == null) {
	// try
	// {
	// //EntityManagerFactory emf=
	// Persistence.createEntityManagerFactory("MyFirstJpa");
	// em = (EntityManager)
	// Persistence.createEntityManagerFactory("mpss4j").createEntityManager();
	// }
	// catch (Exception ex)
	// {
	// System.out.print(ex.getMessage());
	// }
	//
	// }
	// }

	// @AfterClass
	// public static void tearDownAfterClass() throws Exception {
	// em.close();
	// }

	// getAdjacentPenumbraExits
	@Test
	public void testgetAdjacentPenumbraExits() {
		int sId = 1;
		Timestamp tdt = Timestamp.valueOf("2009-06-02 02:55:00");

		//List<Timestamp> DT = oeDao.getAdjacentPenumbraExits(sId, tdt);
		List<Timestamp> DT = oeDao.getAdjacentPenumbraentrance(sId, tdt);

		System.out.printf("AdjacentPenumbra time: %s", DT);
	}

	// getOrbitEventsByPass
	/*
	 * @Test public void testCmdStructureGetById() { long sId = 5;
	 * 
	 * CmdStructure cs = cmdStructureDao.getById(sId);
	 * 
	 * System.out.printf(
	 * "CmdStructure: structure_name: %s, structure_item_name: %s\n",
	 * cs.getStructureName(), cs.getStructureItemName() ); }
	 */

	@Test
	public void testSatellite() {
		// JPA
		// Satellite sat = (Satellite) em.find(Satellite.class, (long) 1);
		Satellite sat = satelliteDao.get((long) 1);

		System.out.println("Satellite is " + sat.getName());
		for (Antenna a : sat.getAntennas()) {
			System.out.println("Antenna is " + a.getName());
		}
		for (Recorder r : sat.getRecorders()) {
			System.out.println("Recorder is " + r.getName());
		}
		for (Transmitter tx : sat.getTransmitters()) {
			System.out.println("Transmitter is " + tx.getName());
		}

		// JAXB marshalling to XML/JSON
		try {
			JAXBContext jc = JAXBContext.newInstance(Satellite.class);
			Marshaller marshaller = jc.createMarshaller();
			// XML
			System.out.println("To XML:");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(sat, System.out);
			// further declaration to JSON
			System.out.println("To JSON:");
			marshaller
					.setProperty("eclipselink.media-type", "application/json");
			marshaller.setProperty("eclipselink.json.include-root", false);
			marshaller.marshal(sat, System.out);

			// partial output (does not work)
			// CopyGroup cg = new CopyGroup();
			// cg.addAttribute("id");
			// cg.addAttribute("name");
			// cg.addAttribute("opsnumber");
			// cg.addAttribute("antennas.anttype");
			// cg.addAttribute("address.name");
			// cg.addAttribute("address.satelliteid");

			// Satellite custCopy = (Satellite)
			// em.unwrap(JpaEntityManager.class).copy(sat, cg);;
			// marshaller.marshal(sat.getAntennas(), System.out);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	/*
	 * @Test public void testSite() { //JPA //Site site = (Site)
	 * em.find(Site.class, (long) 1); //System.out.println("Site is " +
	 * site.getName());
	 * 
	 * // JAXB marshalling to XML/JSON try { String query =
	 * "select s from Site s order by s.id"; List<Site> sites =
	 * em.createQuery(query).getResultList();; for (Site site : sites) {
	 * System.out.println("Site is " + site.getName()); JAXBContext jc =
	 * JAXBContext.newInstance(Site.class); Marshaller marshaller =
	 * jc.createMarshaller(); // XML System.out.println("To XML:");
	 * marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	 * marshaller.marshal(site, System.out); System.out.println(); // further
	 * declaration to JSON System.out.println("To JSON:");
	 * marshaller.setProperty("eclipselink.media-type", "application/json");
	 * marshaller.setProperty("eclipselink.json.include-root", false);
	 * marshaller.marshal(site, System.out); System.out.println(); } } catch
	 * (Exception e) { System.out.println(e.getMessage()); } }
	 * 
	 * 
	 * @Test public void testSession() { //JPA //Site site = (Site)
	 * em.find(Site.class, (long) 1); //System.out.println("Site is " +
	 * site.getName());
	 * 
	 * // JAXB marshalling to XML/JSON try { String query =
	 * "select s from Session s order by s.id"; List<Session> sessions =
	 * em.createQuery(query).getResultList();; for (Session session : sessions)
	 * { System.out.println("Session is " + session.getName()); JAXBContext jc =
	 * JAXBContext.newInstance(Session.class); Marshaller marshaller =
	 * jc.createMarshaller(); // XML System.out.println("To XML:");
	 * marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	 * marshaller.marshal(session, System.out); System.out.println(); // further
	 * declaration to JSON System.out.println("To JSON:");
	 * marshaller.setProperty("eclipselink.media-type", "application/json");
	 * marshaller.setProperty("eclipselink.json.include-root", false);
	 * marshaller.marshal(session, System.out); System.out.println(); } } catch
	 * (Exception e) { System.out.println(e.getMessage()); } }
	 */

	@Test
	public void testRevCreate() {
		// Long id;
		// Timestamp antime;
		// Integer branchid;
		// Timestamp dntime;
		// Timestamp revendtime;
		// Integer revno;
		// Integer scid;
		Rev rev = new Rev();
		rev.setBranchid(0);
		rev.setRevno(50);
		rev.setScid(1);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			Date antime = formatter.parse("2009/02/25 00:00:00");
			Date dntime = formatter.parse("2009/02/25 12:00:00");
			Date revendtime = formatter.parse("2009/02/25 23:59:59");
			rev.setAntime(new Timestamp(antime.getTime()));
			rev.setDntime(new Timestamp(dntime.getTime()));
			rev.setRevendtime(new Timestamp(revendtime.getTime()));

			Rev newREv = revDao.create(rev);
			System.out.printf("Rev Id = %d\n", newREv.getId());
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}

	}

	@Test
	public void testGetRevByPass() {
		int scId = 1;
		int revNo = 24738;
		try {
			Rev rev = oeDao.getRevByPass(scId, revNo);
			System.out.printf("pass: %d, antime: %s, revendtime: %s\n",
					rev.getRevno(), new Date(rev.getAntime().getTime()),
					new Date(rev.getRevendtime().getTime()));
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}

	@Test
	public void testGetRevsByTimeRange() {
		int scId = 1;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			Date startTime = formatter.parse("2009/06/01 00:00:00");
			Date endTime = formatter.parse("2009/06/03 00:00:00");
			List<Rev> revs = oeDao.getRevsByTimeRange(scId, startTime, endTime);
			System.out.println("Exce");
			for (Rev rev : revs) {
				System.out.printf("pass: %d, antime: %s, revendtime: %s\n",
						rev.getRevno(), new Date(rev.getAntime().getTime()),
						new Date(rev.getRevendtime().getTime()));
			}
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}

	@Test
	public void testGetEclipsesByPass() {
		int scId = 1;
		int revNo = 25739;
		List<Eclipse> ecs = oeDao.getEclipsesByPass(scId, revNo);
		for (Eclipse e : ecs) {
			System.out
					.printf("pass: %d, type: %s, penumbraentrancetime: %s, penumbraexittime: %s\n",
							revNo, e.getEclipsetype(), new Date(e
									.getPenumbraentrancetime().getTime()),
							new Date(e.getPenumbraexittime().getTime()));
		}
	}

	@Test
	public void testGetEclipsesByRev() {
		int scId = 1;
		int revNo = 25739;
		Rev rev = oeDao.getRevByPass(scId, revNo);
		List<Eclipse> ecs = oeDao.getEclipsesByRev(rev);
		for (Eclipse e : ecs) {
			System.out
					.printf("pass: %d, type: %s, penumbraentrancetime: %s, penumbraexittime: %s\n",
							revNo, e.getEclipsetype(), new Date(e
									.getPenumbraentrancetime().getTime()),
							new Date(e.getPenumbraexittime().getTime()));
		}
	}

	@Test
	public void testGetEclipsesByTimeRange() {
		int scId = 1;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			Date startTime = formatter.parse("2009/06/01 00:00:00");
			Date endTime = formatter.parse("2009/06/03 00:00:00");
			List<Eclipse> ecs = oeDao.getEclipsesByTimeRange(scId, startTime,
					endTime);
			for (Eclipse e : ecs) {
				System.out
						.printf("type: %s, penumbraentrancetime: %s, penumbraexittime: %s\n",
								e.getEclipsetype(), new Date(e
										.getPenumbraentrancetime().getTime()),
								new Date(e.getPenumbraexittime().getTime()));
			}
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}

	@Test
	public void testGetContactsByPass() {
		int scId = 1;
		int revNo = 25739;
		List<Contact> cts = oeDao.getContactsByPass(scId, revNo);
		for (Contact c : cts) {
			System.out.printf("pass: %d, site: %d, rise: %s, fade: %s\n",
					revNo, c.getSiteid(), new Date(c.getRise().getTime()),
					new Date(c.getFade().getTime()));
		}
	}

	@Test
	public void testGetContactsByRev() {
		int scId = 1;
		int revNo = 25739;
		Rev rev = oeDao.getRevByPass(scId, revNo);
		List<Contact> cts = oeDao.getContactsByRev(rev);
		for (Contact c : cts) {
			System.out.printf("pass: %d, site: %d, rise: %s, fade: %s\n",
					revNo, c.getSiteid(), new Date(c.getRise().getTime()),
					new Date(c.getFade().getTime()));
		}
	}

	@Test
	public void testGetContactsByTimeRange() {
		int scId = 1;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			Date startTime = formatter.parse("2009/06/01 00:00:00");
			Date endTime = formatter.parse("2009/06/03 00:00:00");
			List<Contact> cts = oeDao.getContactsByTimeRange(scId, startTime,
					endTime);
			for (Contact c : cts) {
				System.out.printf("site: %d, rise: %s, fade: %s\n",
						c.getSiteid(), new Date(c.getRise().getTime()),
						new Date(c.getFade().getTime()));
			}
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}

	@Test
	public void testGetContactsByTimeRangeSiteType() {
		int scId = 1;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			Date startTime = formatter.parse("2009/06/01 00:00:00");
			Date endTime = formatter.parse("2009/06/03 00:00:00");
			List<Contact> cts = oeDao.getContactsByTimeRangeSiteType(scId,
					startTime, endTime, "XBAND");
			for (Contact c : cts) {
				System.out.printf("site: %d, rise: %s, fade: %s\n",
						c.getSiteid(), new Date(c.getRise().getTime()),
						new Date(c.getFade().getTime()));
			}
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}

	// getOrbitEventsByPass
	@Test
	public void testGetOrbitEventsByPass() {
		int scId = 1;
		int revNo = 25739;
		PsOrbitEvents oe = oeDao.getOrbitEventsByPass(scId, revNo);

		Rev rev = oe.getRev();
		System.out.printf("Rev: pass: %d, antime: %s, revendtime: %s\n", rev
				.getRevno(), new Date(rev.getAntime().getTime()), new Date(rev
				.getRevendtime().getTime()));

		List<Eclipse> ecs = oe.getEclipses();
		for (Eclipse e : ecs) {
			System.out
					.printf("Eclipse: pass: %d, type: %s, penumbraentrancetime: %s, penumbraexittime: %s\n",
							revNo, e.getEclipsetype(), new Date(e
									.getPenumbraentrancetime().getTime()),
							new Date(e.getPenumbraexittime().getTime()));
		}

		List<Contact> cts = oe.getContacts();
		for (Contact c : cts) {
			System.out.printf(
					"Contact: pass: %d, site: %d, rise: %s, fade: %s\n", revNo,
					c.getSiteid(), new Date(c.getRise().getTime()), new Date(c
							.getFade().getTime()));
		}

		try {
			JAXBContext jc = JAXBContext.newInstance(PsOrbitEvents.class);
			Marshaller marshaller = jc.createMarshaller();
			// JSON
			System.out.println("To JSON:");
			marshaller
					.setProperty("eclipselink.media-type", "application/json");
			marshaller.setProperty("eclipselink.json.include-root", false);
			marshaller.marshal(oe, System.out);
			System.out.println();
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}

	// getOrbitEventsByTimeRange
	@Test
	public void testGetOrbitEventsByTimeRange() {
		List<PsOrbitEvents> oes = new ArrayList<PsOrbitEvents>();
		int scId = 1;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			Date startTime = formatter.parse("2009/06/01 00:00:00");
			Date endTime = formatter.parse("2009/06/03 00:00:00");
			oes = oeDao.getOrbitEventsByTimeRange(scId, startTime, endTime);

			for (PsOrbitEvents oe : oes) {
				Rev rev = oe.getRev();
				System.out.printf(
						"*Rev: pass: %d, antime: %s, revendtime: %s\n",
						rev.getRevno(), new Date(rev.getAntime().getTime()),
						new Date(rev.getRevendtime().getTime()));

				List<Eclipse> ecs = oe.getEclipses();
				for (Eclipse e : ecs) {
					System.out
							.printf("Eclipse: type: %s, penumbraentrancetime: %s, penumbraexittime: %s\n",
									e.getEclipsetype(), new Date(e
											.getPenumbraentrancetime()
											.getTime()), new Date(e
											.getPenumbraexittime().getTime()));
				}

				List<Contact> cts = oe.getContacts();
				for (Contact c : cts) {
					System.out.printf(
							"Contact: site: %d, rise: %s, fade: %s\n",
							c.getSiteid(), new Date(c.getRise().getTime()),
							new Date(c.getFade().getTime()));
				}
			}
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}

		try {
			JAXBContext jc = JAXBContext.newInstance(PsOrbitEvents.class);
			Marshaller marshaller = jc.createMarshaller();
			// JSON
			System.out.println("To JSON:");
			marshaller
					.setProperty("eclipselink.media-type", "application/json");
			marshaller.setProperty("eclipselink.json.include-root", false);
			marshaller.marshal(oes, System.out);
			System.out.println();
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}

	// getOrbitEventsByTimeRange
	@Test
	public void testRestGetOrbitEventsByTimeRange() {
		List<PsOrbitEvents> oes = new ArrayList<PsOrbitEvents>();
		int scId = 1;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try {
			// Date startTime = formatter.parse("2009/02/25 00:00:00");
			// Date endTime = formatter.parse("2009/02/26 00:00:00");
			String szStartTime = "20090225000000";
			String szEndTime = "20090226000000";
			oes = oeResource.getOrbitEventsByTimeRange(scId, szStartTime,
					szEndTime);

			try {
				JAXBContext jc = JAXBContext.newInstance(PsOrbitEvents.class);
				Marshaller marshaller = jc.createMarshaller();
				// JSON
				System.out.println("To JSON:");
				marshaller.setProperty("eclipselink.media-type",
						"application/json");
				marshaller.setProperty("eclipselink.json.include-root", false);
				marshaller.marshal(oes, System.out);
				System.out.println();
			} catch (Exception ex) {
				System.out.println("Exception: " + ex.getMessage());
			}
		} catch (Exception ex) {
			System.out.println("Exception: " + ex.getMessage());
		}
	}

	@Test
	public void testOrbitEventsLoader() {
		int scId = 1;
		int branchId = 0;
		// OrbitEventsLoader l = new OrbitEventsLoader(scId, branchId);
		// OrbitEventsLoader l = new OrbitEventsLoader();
		orbitEventsLoader.setScid(scId);
		orbitEventsLoader.setBranchid(branchId);

		String revFileName = "F:\\Projects\\NSPO\\MCC\\05 MPSS\\10 References\\Interface Files\\In\\FDF\\ROCSAT2_056_061_revs.rpt";
		String consFileName = "F:\\Projects\\NSPO\\MCC\\05 MPSS\\10 References\\Interface Files\\In\\FDF\\ROCSAT2_056_061_cons.rpt";
		orbitEventsLoader.Load(revFileName, consFileName);

	}

	@Test
	public void testListParametersets() {
		List<Parameterset> psList = psDao.listParametersets();
		for (Parameterset ps : psList) {
			System.out.println("PS name: " + ps.getName());
		}
	}

	@Test
	public void testGetParameterset() {
		Parameterset ps = psDao.getParameterset("From178");
		System.out.println("PS name: " + ps.getName());
	}

	@Test
	public void testListRecorderps() {
		List<Recorderp> psList = psDao.listRecorderps("From178");
		for (Recorderp ps : psList) {
			System.out.println("PS id: " + ps.getPsid() + " ,Recorder id:"
					+ ps.getRecorderid());
		}
	}

	@Test
	public void testGetRecorderp() {
		Recorderp ps = psDao.getRecorderp("From178", "ROCSAT2", "R1");
		System.out.println("PS id: " + ps.getPsid() + " ,Recorder id:"
				+ ps.getRecorderid());
	}

	@Test
	public void testListDutycycles() {
		List<Dutycycle> psList = psDao.listDutycycles("From178", "ROCSAT2");
		for (Dutycycle ps : psList) {
			System.out.println("SATPS id: " + ps.getSatpsid() + " ,Act type:"
					+ ps.getActivitytype());
		}
	}

	@Test
	public void testGetDutycycle() {
		Dutycycle ps = psDao.getDutycycle("From178", "ROCSAT2", "RSI", "PBK");
		System.out.println("SATPS id: " + ps.getSatpsid() + " ,Act type:"
				+ ps.getActivitytype() + " ,Act subtype:"
				+ ps.getActivitysubtype());
	}

	@Test
	public void testListSiteps() {
		List<Siteps> psList = psDao.listSiteps("From178");
		for (Siteps ps : psList) {
			System.out.println("PS id: " + ps.getPsid() + ", Site Id:"
					+ ps.getSiteid());
		}
	}

	@Test
	public void testGetSiteps() {
		Siteps ps = psDao.getSitep("From178", "X_NSPO");
		System.out.println("PS id: " + ps.getPsid() + ", Site Id:"
				+ ps.getSiteid());
	}

	@Test
	public void testUpdateSession() {
		Date d = new Date();
		Session s = new Session();
		s.setId(Long.valueOf(1));
		s.setAcqsumloaded(2);
		s.setColdscheduled(3);
		s.setDescription("abc");
		s.setEndtime(new Timestamp(d.getTime()));
		s.setIsbranch(0);
		s.setLastsave(new Timestamp(d.getTime()));
		s.setName("111");
		s.setNeedsvalidation(4);
		s.setNominalapplied(5);
		s.setPsid(6);
		// s.setSatArlXrefs(null);
		// s.setSatXrefs(null);
		// s.setSiteXrefs(null);
		s.setStarttime(new Timestamp(d.getTime()));

		sessionDao.update(s);

		System.out.println("Session Name: " + s.getName());
	}

	@Test
	public void testListSessions() {
		List<Session> sessList = sessionDao.listSessions();
		for (Session s : sessList) {
			System.out.println("Session Name: " + s.getName());
		}
	}

	@Test
	public void testGetSessionByName() {
		Session s = sessionDao.getSession("day035");
		System.out.println("Session Name: " + s.getName());
		System.out.println("Session Id: " + s.getId());
	}
	
	@Test
	public void testGetSessionMinStarttime() {
		Date d = sessionDao.getMaxEndtime();
		System.out.println("date: " + d);
		
	}

	@Test
	public void testGetSessionById() {
		Session s = sessionDao.getSessionById(28);
		System.out.println("Session Name-->: " + s.getName());
	}
	
	@Test
	public void testGetSession() {
		Session s = sessionDao.get((long)28);
		System.out.println("Session Name-->: " + s.getName());
	}
	
	@Test
	public void testGetSessionNextId() {
		//Session s = sessionDao.getSessionById(28);
		System.out.println("Session getNextId-->: " + sessionDao.getNextId());
	}

	@Test
	public void testGetSessionSatillites() {
		List<String> names = sessionDao.getSatelliteNames("day035");
		for (String name : names) {
			System.out.println("Satellite Name: " + name);
		}
	}

	@Test
	public void testGetSessionSites() {
		List<String> names = sessionDao.getSiteNames("day035");
		for (String name : names) {
			System.out.println("Site Name: " + name);
		}
	}
	
	@Test
	public void testgetSessionByRangeNBranch() {
		Collection col = sessionDao.getSessionByRangeNBranch("2014-01-19 13:40:00", "2014-01-21 13:40:00", 1);
		ArrayList al = new ArrayList(col);
		for (int i = 0; i<al.size(); i++){
			Session s = (Session)al.get(i);
			System.out.println("name="+ s.getName());
		}
		//System.out.println("Session Name-->: " + s.getName());
	}
	
	@Test
	public void testgetSessionByTimeNBranch() {
		Collection col = sessionDao.getSessionByTimeNBranch("2014-01-19 13:41:00", 1);
		ArrayList al = new ArrayList(col);
		for (int i = 0; i<al.size(); i++){
			Session s = (Session)al.get(i);
			System.out.println("name="+ s.getName());
		}
		//System.out.println("Session Name-->: " + s.getName());
	}

	@Test
	public void testListManeuverrequests() {
		String arlName = "RS2_RSITimeline_20090602_A2";
		List<Maneuverrequest> reqs = arListDao.listManeuverrequests(arlName);
		for (Maneuverrequest r : reqs) {
			System.out.println("Id: " + r.getId());
		}
	}

	@Test
	public void testListRsiimagingrequests() {
		String arlName = "RS2_RSITimeline_20090602_A2";
		List<Rsiimagingrequest> reqs = arListDao
				.listRsiimagingrequests(arlName);
		for (Rsiimagingrequest r : reqs) {
			System.out.println("Id: " + r.getId());
		}
	}

	@Test
	public void testGetMissionTimeLines() {
		String arlName = "RS2_RSITimeline_20090602_A2";
		PsMissionTimeLine mtl = arListDao.getMissionTimeLines(arlName);
		if (mtl == null) {
			System.out.println("Null mission time line");
		}
		for (Rsiimagingrequest r : mtl.getRsiimagingrequests()) {
			System.out.println("Id: " + r.getId());
		}
		for (Maneuverrequest r : mtl.getManeuverrequests()) {
			System.out.println("Id: " + r.getId());
		}
	}

	@Test
	public void testSessionManDao() {
		String name = "day153";
		Session sess = sessionManDao.getSessionByName(name);
		if (sess != null) {
			System.out.println("Session: " + sess.getId());
		}
		List<Satellite> sats = sessionManDao.getRefSatellites(name);
		System.out.println("No of Satellite: " + sats.size());
		for (Satellite s : sats) {
			System.out.println("Satellite: " + s.getName());
		}

		List<Site> sites = sessionManDao.getRefSites(name);
		System.out.println("No of Sites: " + sites.size());
		for (Site s : sites) {
			System.out.println("Site: " + s.getName());
		}

		// parameter set used by the session
		Parameterset ps = sessionManDao.getParameterset(name);
		System.out.println("Parameter set: " + ps.getName());

		// satellite ps belonging to the parameter set
		for (Satellite s : sats) {
			Satellitep sps = sessionManDao.getSatellitep(ps.getName(),
					s.getName());
			System.out.println("Satellite Id: " + sps.getSatelliteid()
					+ ", Rsifocalplanedelay = " + sps.getRsifocalplanedelay());
		}

	}

	 @Test
	 public void testGetImagingModeByInstrument() {
	
	 Imagingmode i = imagingmodeDao.getImagingModeByInstNModeid("RSI", 1);
	
	
	 System.out.println("Mode Name: " + i.getModename());
	
	 }
}
