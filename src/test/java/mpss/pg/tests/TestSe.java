package mpss.pg.tests;

import mpss.common.jpa.*;
//import mpss.common.jpa.fs3command.*;
import mpss.common.dao.*;
import mpss.pg.*;
import mpss.pg.load.TTQLoad;
import mpss.pg.product.ScheduleProductBuilder;
import mpss.se.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

//import static org.junit.Assert.*;

import org.junit.Test;

import com.google.common.collect.Range;

import java.sql.Timestamp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ContextConfiguration(locations = {"/applicationContext.xml"})
public class TestSe extends AbstractTransactionalJUnit4SpringContextTests {

	private Pattern startPattern = Pattern.compile("[$?]");
	//private Pattern endPattern = Pattern.compile("[ ()<>:%^&@*'!-+/\"\t]");
	private Pattern endPattern = Pattern.compile("[!* )(<>:%^;&@'/\"\t+-]");
	
	  
	@Autowired
    protected SatelliteDao satelliteDao;
	

    @Autowired
    protected PsDao psDao;
    
    @Autowired
    protected SessionDao sessionDao;
    
    @Autowired
    protected ExtentDao extentDao;

    
    @Autowired
    protected ActivityDao activityDao;
    
    @Autowired
    RS2SSRDeleteEvent ssrDeleteEvent;
    
    @Autowired
    RS2RSIRecordEvent rs2RSIRecordEvent;

    @Autowired
    RS2RSIPlaybackEvent rs2RSIPlaybackEvent;
    
    @Autowired
    ScheduledEventGenerator scheduledEventGenerator;
    
    @Autowired
    TTQLoad ttqLoad;
    
    @Autowired
    ScheduleProductBuilder builder;
  
   // @Autowired
   // EventTemplateLine eventTemplateLine;
    
    @Test
    public void testgetActivityname()
  	{		
    		int activityId=2826;
    		System.out.println("Start getActivity ActivityId : " + activityId);
  			Activity act = this.activityDao.get((long)activityId);
  			System.out.println("End getActivity ActivityName : " + act.getName());
  			//return act;
    }
   

    @Test
    public void testSSRDeleteEvent() {
    	long extId=897212;
    	Extent ext= extentDao.get(extId);
    	
    	//SSRDeleteEvent DelEvent = new SSRDeleteEvent();
    	
    	ssrDeleteEvent.generate(ext);
    }

    @Test
    public void testRS2RSIRecordEvent() {
    	long extId=897266;
    	Extent ext= extentDao.get(extId);
    	
    	rs2RSIRecordEvent.generate(ext);
    }   
    
    @Test
    public void testRS2RSIPlaybackEvent() {
    	long extId=897305;
    	Extent ext= extentDao.get(extId);
    	rs2RSIPlaybackEvent.generate(ext);
    }   
    
 
	/*
	
  
  
	@Test
	public void testGetRevsByTimeRange() {
		int scId = 1;
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	    try
	    {
		    Date startTime = formatter.parse("2009/06/01 00:00:00");
		    Date endTime = formatter.parse("2009/06/03 00:00:00");
		    List<Rev> revs = oeDao.getRevsByTimeRange(scId, startTime, endTime);
		    System.out.println("Exce");
		    for (Rev rev : revs)
		    {
		        System.out.printf("pass: %d, antime: %s, revendtime: %s\n", rev.getRevno(), 
				new Date(rev.getAntime().getTime()), 
				new Date(rev.getRevendtime().getTime()) );	
			}
		}
	    catch (Exception ex)
	    {
		     System.out.println("Exception: " + ex.getMessage());
	    }
	}
	

	

	*/
 
}
	
