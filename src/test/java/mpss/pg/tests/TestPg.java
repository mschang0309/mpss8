package mpss.pg.tests;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Range;

import mpss.config;
import mpss.common.dao.*;
import mpss.common.jpa.Session;
import mpss.se.RS2CmdProcEvent;
import mpss.se.RS2ISUALPlaybackEvent;
import mpss.se.RS2ManeuverEvent;
import mpss.se.RS2OrbitManeuverEvent;
import mpss.se.RS2RSIDDTEvent;
import mpss.se.RS2RSIPlaybackEvent;
import mpss.se.RS2RSIRecordEvent;
import mpss.se.RS2SupManeuverEvent;
import mpss.se.RS2UplinkEvent;
import mpss.se.RS2XCrossEvent;
import mpss.se.RS2SSRDeleteEvent;
import mpss.se.ScheduledEvent;
import mpss.se.ScheduledEventGenerator;
import mpss.util.timeformat.*;
import mpss.pg.*;
import mpss.pg.product.ScheduleProductBuilder;
import nspo.mpss.amq.IMpssAmqData;
import nspo.mpss.amq.cmd.ProductGen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;


//import static org.junit.Assert.*;

import org.junit.Test;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javae.jmx.amq.topic.AmqJmxDriver;
import javae.jmx.topic.JmxConnection;
import javae.jmx.topic.JmxDriverManager;
import javae.jmx.topic.JmxException;
import javae.jmx.topic.JmxPublisher;

@ContextConfiguration(locations = {"/applicationContext.xml"})
public class TestPg extends AbstractTransactionalJUnit4SpringContextTests {

	private Pattern startPattern = Pattern.compile("[$?]");
	private Pattern endPattern = Pattern.compile("[!* )(<>:%^;&@'/\"\t+-]");
	
	Map<String, Integer> dataTypeCount = new HashMap<String, Integer>();
	List<String> pms = ImmutableList.of("PN", "CSV", "PM","SFG","SD","PN","PN","PM","");
	
 
  @Autowired
  protected SatelliteDao satelliteDao;
	

  @Autowired
  protected PsDao psDao;
  
  @Autowired
  SessionDao sessionDao;
  
  @Autowired
  protected ExtentDao extentDao;

  @Autowired
  protected ActivityDao activityDao;

  @Autowired
  ScheduleProductSession session;
	
  @Autowired
  ScheduleProductBuilder builder;
  
  @Autowired
  ScheduledEventGenerator seGenerator;
	
    //@Autowired
    //MPQLoad mpqLoad;
  
  
  @Test
  public void testGetTemplate()
  {
	  System.setProperty("satName", "FORMOSAT5");
	  config.load();
	  String satName = "ROCSAT5";
		String templateDir=System.getProperty("dataFileDir") + satName + "\\" + System.getProperty("eventTemplates");
		EventTemplates eventTemplates = new EventTemplates(templateDir);
		eventTemplates.load();
		
		String[] tpName = new String[20];
		tpName[0] = "AIP_PBK";
		tpName[1] = "AIP_PROC";
		tpName[2] = "m_ACQ_E";
		tpName[3] = "m_ACQ_S";
		tpName[4] = "m_AOS";
		tpName[5] = "m_ATT_MAN";
		tpName[6] = "m_ATTITUDE_MAN";
		tpName[7] = "m_GOHOME_MAN";
		tpName[8] = "m_LOS";
		tpName[9] = "m_RSI_PBK";
		tpName[10] = "m_RSI_REC_INIT";
		tpName[11] = "m_RSI_REC_SEC";
		tpName[12] = "m_RT_PROC";
		tpName[13] = "m_SSR_DEL";
		tpName[14] = "m_SUP_MAN";
		tpName[15] = "m_TTQ_LOAD";
		tpName[16] = "m_XCROSS";
		tpName[17] = "MISC_PROC";
		tpName[18] = "MPQ_LOAD";
		tpName[19] = "ORB_MAN";
		
		
	 
	  for (String name : tpName)
	  {
		  List<EventTemplateLine> tempLines = eventTemplates.getTemplate(name);
		  System.out.println(name + "==============================================================================");
		  for(EventTemplateLine el : tempLines)
		  {
			  System.out.println(el.getMicIsCommand() + ",	" + el.getMicCmdSource() + ", " + el.getFullLineText());
			  for (VariableSymbol vs : el.getVariableSymbols())
			  {
				  System.out.println("		Symbol:" + vs.getSymbolName() + "[" + vs.getSymbolType() + "] start[" + vs.getStartPos() + "] end[" + vs.getEndPos() + "]" );  
			  }
			  
		  }
	  }

  }
  
  @Test
  public void testMath()
  {
	  int dur = 36065;
	  System.out.println(dur/3600);
	  System.out.println("floor : " + Math.floor(dur/3600.0));
	  System.out.println("ceil : " + Math.ceil(dur/3600.0));
	  int h = dur/3600;
  	int m = (dur - h * 3600) / 60;
  	int s = dur - h * 3600 - m * 60;
  	System.out.println(String.format("%1$02d", h) + ":" + String.format("%1$02d", m) + ":" + String.format("%1$02d", s) + ".000");
	  
  }
  
  @Test
  public void timeZone() throws Exception {
	  TimeZone defaultZone = TimeZone.getDefault();
	  System.out.println(defaultZone.getID());//UTC
	   
	   
	  Timestamp currTime = new Timestamp(System.currentTimeMillis());	  
	  System.out.println("currTime :" + currTime);
	  System.out.println("GMT :" + currTime.toGMTString());
	  System.out.println("Local :"  + currTime.toLocaleString());
	  System.out.println("String :" + currTime.toString());
//	  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//	  sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
//	  date = (Date) utc.parse(text);
	  
  }
  
  @Test
  public void Datatest1() throws Exception {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyDDDHHmmss.SSS");
      JmxConnection<IMpssAmqData, IMpssAmqData> conn = null;
      JmxDriverManager.register("ActiveMQ", new AmqJmxDriver());
      try {
          conn = JmxDriverManager.connectObject(IMpssAmqData.class, IMpssAmqData.class, "ActiveMQ", "tcp://127.0.0.1:61616");
          conn.start();
      } catch (JmxException ex) {
          //Logger.getLogger(AmqComm.class.getName()).log(Level.SEVERE, null, ex);
      }

      JmxPublisher<IMpssAmqData, IMpssAmqData> amqPub = conn.createPublisher();

      //----------------------------------------
      ProductGen pg = new ProductGen();
      //pg.setBranchId(19273);
      pg.setProdDurHour(48);
      pg.setProdDurMin(10);
      pg.setIntegrateRpt(false);
      pg.setTtq(true);
      pg.setMpq(false);
      pg.setLoadDurHour(48);
      pg.setLoadDurMin(20);
      pg.setNominal(true);
      pg.setPassPlan(false);
      pg.setResolveAcq(false);
      pg.setSatelliteId(1);
      pg.setLoadStartTime(Timestamp.valueOf("2009-06-02 10:12:30"));
      pg.setProdStartTime(Timestamp.valueOf("2009-06-02 10:12:30"));
      //pg.setLoadStartTime(Timestamp.valueOf("2014-01-19 10:12:30"));
      //pg.setProdStartTime(Timestamp.valueOf("2014-01-19 10:12:30"));


      try {
          amqPub.publish("nspo.mpss.web", pg);
          //ProdGenResp pgr = (ProdGenResp) amqPub.publish("nspo.mpss.web", pg, ms);
          Thread.sleep(100000);
          System.out.println("Send");

      } catch (Exception ex) {
          System.out.println(ex.getMessage());
      }


      Thread.sleep(100000);
  }
  
  @Test
	public void testMap() 
	{
	  SortedMap<Integer, Integer> revIdMap = new TreeMap<Integer, Integer>();
	  revIdMap.put(125, 1212);
	  revIdMap.put(124, 1211);
	  revIdMap.put(123, 1213);
	  revIdMap.put(126, 1216);
	  revIdMap.put(128, 1215);
	  revIdMap.put(129, 1217);
	  revIdMap.put(127, 1214);
	  System.out.println("first key value :" + revIdMap.get(revIdMap.firstKey()));
	  System.out.println("last key value:" + revIdMap.get(revIdMap.lastKey()));
	}
  
  	@Test
	public void testFileRename() 
	{
  		String loadPath = "D:/MPSS.Launch/reports/ROCSAT/";
  		String configValue = "MPQs";
		loadPath = loadPath + configValue + "\\";
		String fName = "_2009089004000_2009089022000.dat";
		loadPath = loadPath + fName;
		
  		File o = new File(loadPath);
  		File b = new File(loadPath + ".bak");
  		try {
			if (o.createNewFile())
			{
				System.out.println("Success");
			}
			else
			{
				System.out.println("Error: Unknown log_files directory");
			}
		} catch (IOException e) {
			System.err.println("PgSchProductBuilder Error: Log file can not be opened: ");
			System.err.println(e.getMessage());
		}
  		
  		if (b.exists())
  			b.delete();
  		
  	   if(o.renameTo(new File(loadPath + ".bak")))
  		    System.out.println("Move Success");
	}
  	
  	@Test
	public void testFileReadWrite() 
	{

    		String loadPath = "D:/MPSS.Launch/reports/ROCSAT2/";
      		String configValue = "MPQs";
    		loadPath = loadPath + configValue + "\\";
    		String Name1 = loadPath + "aocs_2009089004000_2009089022000.dat";
    		String Name2 = loadPath + "pdm_2009089004000_2009089022000.dat";
    		String Name3 = loadPath + "mpq_2009089004000_2009089022000.dat";
    		List<String> names = new ArrayList<String>();
    		names.add(0, Name1);
    		names.add(1, Name2);
    		
    		File file = new File(Name3);
    		if(file.exists())
    		{
    			String backupLoadName = Name3 + ".bak";
    			File backupLoad = new File(backupLoadName);
    	  		if (backupLoad.exists())
    	  			backupLoad.delete();
    	  		
    	  		file.renameTo(new File(backupLoadName));
    		}
    		
			//creat
    		try {
		        file.createNewFile();
    			FileOutputStream out = new FileOutputStream(Name3,true);
    	  		byte[] buf = new byte[1024];
                int len;

          		for(String name : names)
          		{
          	  		  FileInputStream fInstream = new FileInputStream(name);
          	  		  DataInputStream in = new DataInputStream(fInstream);
          	  		 
          	      	while ((len = in.read(buf)) > 0 )   {
          	    		out.write(buf, 0, len);
          	    	}
          	      	in.close();
          		}
            	out.close();
            	
		    } catch (Exception e) {
		        System.err.println(Name3 + " Creat Error");
		    }

	}
  	
	
	public void pgUISet() 
	{
    	// UiSchProductGenWindow
		session.setIntegratedRptSelected(true);
    	session.setMPQSelected(true);
    	session.setPassPlanSelected(true);
    	session.setRasSelected(true);
    	session.setTTQSelected(true);
    	session.setLoadTimeRange(RangeUtil.startTimeAppendHourMinu(Timestamp.valueOf("2009-06-02 17:12:30"), 5, 34));
    	session.setProductTimeRange(RangeUtil.startTimeAppendHourMinu(Timestamp.valueOf("2009-06-02 17:12:00"), 5, 43));
    	session.setIsNominal(true);
    	session.setSatelliteId(1);	
    	
	}
	
	
    @Test
    public void testString() {
    	String  s1="sdfs?df";
    	String  s2="s$fsdf";
    	String  s3="s$fss?df";
    	String  s4="sf?dgss$df";
    	String  s5="sdsgsfsdf";
    	String  s6="sds$?fsdf";
    	String  s8="jjj$load_stop dd ?2 fsdf ?5; $SKIP_TAG:";
    	String  s7="$1:	Maneuver $M_EVENT_DUR^ Orbit for $ops ($1) sfra $load_stop";
    	
    	String subText= s8;
    	String symbolName="";
    	int start = -1;
    	int end = -1;
    	int lTrimLen = 0;
    	Matcher matcher = null;
    	int endOfLine = s8.length() - 1;
    	boolean run = true;
    	while (run)
    	{
    		matcher = startPattern.matcher(subText);
	        if (matcher.find()) 
	        {
	        	start =  matcher.start();
	        	subText = subText.substring(start);
	        	lTrimLen += start; 
	        	symbolName = subText;
	        	matcher = endPattern.matcher(subText);
	        	if (matcher.find())
	        	{
	        		end = matcher.start();
	        		if (end < endOfLine)
	        		{
	        			subText = subText.substring(end + 1);
	        			lTrimLen += end + 1; 
	        			symbolName=symbolName.substring(0,end);
	        		}
	        		else
	        		{   
	        			run = false;
	        			lTrimLen += end + 1;
	        		}
	        	}
	        	else 
	        	{
	        		end = subText.length();
	        		lTrimLen = endOfLine + 2;
	        		run = false;
	        	}

	        	//System.out.println("s7-->:start: " + start + " End: " + end + " subText: " + subText + " symbol: " + subText.substring(start, end -1));
	        	System.out.println("s7-->:start: " + (lTrimLen - end -1) +  " End: " + (lTrimLen - 2) + " subText: " + subText + " symbol: " + symbolName);
	        	//System.out.println("substring: " + s7.substring(start, end - start + 1) + "Start: " + start + " End: " + end);
                        
          }
          // no more symbol
          else
          {
              run = false;;
          }
	}

    	System.out.println("s1:" + s1.contentEquals("$?"));
    	System.out.println("s2:" + s2.contentEquals("$?"));
    	System.out.println("s3:" + s3.contentEquals("$?"));
    	System.out.println("s4:" + s4.contentEquals("$?"));
    	System.out.println("s5:" + s5.contentEquals("$?"));
    	System.out.println("s6:" + s6.contentEquals("$?"));
    } 
    
    
    @Test
    public void testEventTemplateLine() {
    	String  s3="! jjj$load_stop dd ?2 fsdf ?5; $SKIP_TAG:";
    	String  s4="00:00:01.000- # Start rsi imaging";
    	String  s5="00:00:00.250^ # Compression ratio $6 for PAN, $7 for MS video channels";
    	String  s6="00:00:00.000^ CMD PIPFSELCRA PANCOMPRAT='$6' MSCOMPRAT='$7' SRC=MPQ";
    	String  s7="$1:	Maneuver $M_EVENT_DUR^ Orbit for $ops ($1) sfra $load_stop";
    	String  s8="jjj$load_stop dd ?2 fsdf ?5; $SKIP_TAG:";
    	
    	EventTemplateLine eventTemplateLine = new EventTemplateLine();
    	eventTemplateLine.setFullLineText(s8);
    	eventTemplateLine.parse(true);
    
    }
    
    @Test
    public void testltrim() {
    	String s="    ABCDE   ";
        int i = 0;
        while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
            System.out.println("s.charAt(i)  "+ s.charAt(i));
            i++;
        }
        //return s.substring(i);
        Timestamp currTime = new Timestamp(System.currentTimeMillis());
		String creationTime1 = String.format("%td/", currTime) + String.format("%tT", currTime);
		String creationTime2 = String.format("%tY",currTime) + String.format("%tj_",currTime);
		System.out.println("creationTime(dd/hh:mm:ss) : " + creationTime1);
		System.out.println("creationTime(yyyyDD) : " + creationTime2);
    }

    @Test
    public void testLoadRange() {
    	
		// from GUI product generation checkbox 
    	
    	pgUISet();
    	
    	System.out.println("Load Star Time:"+ session.getLoadTimeRange().first);
    	System.out.println("Load End Time:"+ session.getLoadTimeRange().second);
    	
    }
    
    @Test
    public void testVold() {
    	  	
    	System.out.println("Load Star Time");
    	System.out.println("'" + String.format("%1$02d", 1) + "'");
    	System.out.println(String.format("%1$-85s", "asdf"));
    	
    }
    
    @Test
    public void testEventTemplates() {

    	EventTemplates ets = new EventTemplates("D:\\MPSS\\OLD\\MPSS_Document\\MPSS.Launch\\data\\ROCSAT2\\event_templates");
    	ets.load();
    	List<EventTemplateLine> tls = ets.getTemplate("sSSR_DEL_old");
    	if (tls==null)
    	{
    		System.out.println("No file");
    	}
    	for (EventTemplateLine tl : tls) {
			System.out.println(tl.getFullLineText());
		}
    	
    }
    
    @Test
    public void testEventTriggerTag() {
    	String  s3="00:01:00.000- #  Turn spacecraft TX on";
    	String  s4="00:00:00.000^ CMD CTXTRSPON SRC=TTQ";
    	String  s5="00:10:00.000- #START /export/home/vincentm/procedures/rts-sequential.prc";
    	String  s6="20:32:01.250- #*****************************************************************************";
    	String  s7="$M_EVENT_DUR^ # Stop rsi imaging";
    	String  s8="$M_EVENT_DUR  # Playback end";

    }
    
    
    private void detPlaybackCounts(List<String> eventParms)
	{
		/// data type is in the first parameter
		//int paramIndex = 0;
		String dataType;
		
		
		dataTypeCount.put("PN", 0);	/// panchromatic
		dataTypeCount.put("MS", 0);	/// multi-spectral
		dataTypeCount.put("PM", 0);	/// pan-ms

		/// if data type is available
		for (int paramIndex=0; paramIndex < eventParms.size(); paramIndex++)
		//if (paramIndex < eventParms.size())
		{
			/// retrieve the data type
			dataType = eventParms.get(paramIndex);

			/// if the data type is valid
			if (dataType.equalsIgnoreCase("PN") || dataType.equalsIgnoreCase("MS") || dataType.equalsIgnoreCase("PM"))
			{
				/// increment the number of playbacks for this type
				dataTypeCount.put(dataType, dataTypeCount.get(dataType) + 1);
	
			}
			//paramIndex++;
		}
	}
    
    @Test
    public void testdetPlaybackCounts()
	{
		
    	detPlaybackCounts(pms);
    	System.out.println("PN count:" + dataTypeCount.get("PN"));
    	System.out.println("MS count:" + dataTypeCount.get("MS"));
    	System.out.println("PM count:" + dataTypeCount.get("PM"));
	}
    
    @Test
    public void testRangeContain()
	{
    	Timestamp startTime,endTime;
    	Range<Timestamp> r1,r2,r3;
    	r1=Range.closed(Timestamp.valueOf("2013-03-19 00:14:50"), Timestamp.valueOf("2013-03-21 13:14:50.000"));
    	r2=Range.closedOpen(Timestamp.valueOf("2013-03-22 02:00:50"), Timestamp.valueOf("2013-03-23 10:14:50.000"));
    	r1.encloses(r2);
    	System.out.println("r2 contains in r1 :" + r1.encloses(r2));
    	
	}
    
    @Test
    public void testRange()
	{
    	Timestamp startTime,endTime;
    	Range<Timestamp> r1,r2,r3;
    	r1=Range.closed(Timestamp.valueOf("2013-03-20 13:14:50"), Timestamp.valueOf("2013-03-21 13:14:50.000"));
    	r2=Range.closedOpen(Timestamp.valueOf("2013-03-19 13:14:50"), Timestamp.valueOf("2013-03-22 10:14:50.000"));
    	if(r1.lowerEndpoint().compareTo
            	(r2.lowerEndpoint()) < 0)
            {
            	startTime = r1.lowerEndpoint();
            }
            else
            {
            	startTime = r2.lowerEndpoint();
            }
            if(r1.upperEndpoint().compareTo
            	(r2.upperEndpoint()) > 0)
            {
            	endTime = r1.upperEndpoint();
            }
            else
            {
            	endTime = r2.upperEndpoint();
            }
            r3 = Range.closed(startTime,endTime); 
  
    	System.out.println("lowerEndpoint:" + r3.lowerEndpoint());
    	System.out.println("upperEndpoint:" + r3.upperEndpoint());
    	System.out.println("2013-03-19 13:14:50 in  r1 is " + r2.contains(Timestamp.valueOf("2013-03-19 13:14:50")));
    	System.out.println("2013-03-22 10:14:50.000 in  r2 is " + r2.contains(Timestamp.valueOf("2013-03-22 10:14:49.999")));
	}
    
}
	
