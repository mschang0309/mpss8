package mpss.util.timeformat.tests;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.text.SimpleDateFormat;
import java.sql.Timestamp;
import java.util.Formatter;

import mpss.pg.EventTemplateLine;
import mpss.util.timeformat.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestTimeUtils  {

	//@BeforeClass
	//public static void setUpBeforeClass() throws Exception {
  //     if (em == null) {
  //     	try
  //     	{
  //     	//EntityManagerFactory emf= Persistence.createEntityManagerFactory("MyFirstJpa");
  //         em = (EntityManager) Persistence.createEntityManagerFactory("mpss4j").createEntityManager();	
  //     	}
  //     	catch (Exception ex)
  //     	{
  //     		System.out.print(ex.getMessage());
  //     	}
  //     	
  //     }
	//}
 
	//@AfterClass
	//public static void tearDownAfterClass() throws Exception {
	//	em.close();
	//}
 
	@Test
	public void testString() {
		//System.out.println(String.format("%-50s", "test").replace(" ", "_"));
		System.out.println(String.format("%-108s", "TAINAN") + String.format("%24s", "CHUNGLI"));
		System.out.println(String.format("%-108s", "Predicted AOS/Actual AOS") + "Predicted AOS/Actual AOS");
		System.out.println(String.format("%-108s", "     " + "14:44:54" + "//_:_:_") + "       " + "14:45:50" + "//_:_:_");
		System.out.println(String.format("%-108s", "Predicted LOS/Actual LOS") + "Predicted LOS/Actual LOS");
		System.out.println(String.format("%-108s", "     " + "14:55:50" + "//_:_:_") + "       " + "14:56:06" + "//_:_:_");
		System.out.println(String.format("%-132s", "_").replace(" ", "_"));
		Timestamp currentTime = Timestamp.valueOf("1970-01-01 00:00:00.0");
		System.out.println("currentTime:" + currentTime.toString() + " : " + currentTime.getTime());
		System.out.println("-0.9996451 : " +(int)Double.parseDouble("-0.9996451"));
		System.out.println("-0.1996451 : " +(int)Double.parseDouble("-0.1996451"));
		System.out.println("-2.1 : " +(int)(-2.1));
	}
	
	@Test
	public void testStringList() {
		List<String> list1 = new ArrayList<String>();
		List<String> list2 = new ArrayList<String>();
		list1.add("0");
		list1.add("1");
		list1.add("2");
		list1.add("3");
		list1.add("4");
		list1.add("5");
		list1.add("6");
		list1.add("7");
		list1.add("8");

		int i =0;
		for( Iterator<String> iter = list1.iterator(); iter.hasNext();){
			iter.next();
			if ((i>=0 && i<3) || i==6)
			{
		        iter.remove();
		    }
			i++;
		}

		for (String s : list1)
		{
			System.out.println(s);	
		}
		
	}
	
	@Test
	public void testYearDayTimeOffset() {
		Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
		System.out.println("Current timestamp: " + currentTimestamp.getTime());
		YearDayTimeOffset yearDayTimeOffset = new YearDayTimeOffset(currentTimestamp);
		System.out.println("Current year: " + yearDayTimeOffset.getYear() + 
				           ", Current day: " +  yearDayTimeOffset.getDayOfYear() +
                           ", Current seconds: " +  yearDayTimeOffset.getSecondsOfDay());
		currentTimestamp = YearDayTimeOffset.toTimestamp(yearDayTimeOffset.getYear(), yearDayTimeOffset.getDayOfYear(), yearDayTimeOffset.getSecondsOfDay());
		System.out.println("Current timestamp: " + currentTimestamp.getTime());
	}
	
	@Test
	public void testTimeToString() {
		Timestamp ct = new Timestamp(Calendar.getInstance().getTime().getTime());
		Timestamp ts = Timestamp.valueOf("2014-04-15 15:54:30.123");
		String sf= String.format("SKIP%tj", ct) + String.format("%tT", ct).replace(":","" );
		String fillChar = String.format("%05d", 12);
		
		System.out.println("current Time AS Load Time:" +TimeToString.AsLoadTime(ct));
		System.out.println("current Time AS Epoch Time:" +TimeToString.AsEpochTime(ct));
		System.out.println("2014-04-15 15:54:30.123 AS Load Time:" +TimeToString.AsLoadTime(ts));
		System.out.println("2014-04-15 15:54:30.123 AS Epoch Time:" +TimeToString.AsEpochTime(ts));
		System.out.println("2014-04-15 15:54:30.123 AS Log Date:" +TimeToString.AsLogDate(ts));
		System.out.println("2014-04-15 15:54:30.123 AS Log Time:" +TimeToString.AsLogTime(ts));
		System.out.println("2014-04-15 15:54:30.123 AS Name Date:" +TimeToString.AsNameDate(ts));
		System.out.println("2014-04-15 15:54:30.123 AS Name Time:" +TimeToString.AsNameTime(ts));
		System.out.println("2014-04-15 15:54:30.123 AS Short Time:" +TimeToString.AsShortTime(ts));
		System.out.println("2014-04-15 15:54:30.123 AS Long Time:" +TimeToString.AsLongTime(ts));
		System.out.println("Format to SKIP%j%H%M%S:" + sf);
		System.out.println("5 characters, 0 filled on the left:" + fillChar);
	}

}
	
