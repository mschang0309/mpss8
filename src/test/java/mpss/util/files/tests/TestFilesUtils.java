package mpss.util.files.tests;

import java.util.Calendar;
import java.util.List;
import java.text.SimpleDateFormat;
import java.sql.Timestamp;

import mpss.util.files.*;

import java.io.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestFilesUtils  {

	//@BeforeClass
	//public static void setUpBeforeClass() throws Exception {
  //     if (em == null) {
  //     	try
  //     	{
  //     	//EntityManagerFactory emf= Persistence.createEntityManagerFactory("MyFirstJpa");
  //         em = (EntityManager) Persistence.createEntityManagerFactory("mpss4j").createEntityManager();	
  //     	}
  //     	catch (Exception ex)
  //     	{
  //     		System.out.print(ex.getMessage());
  //     	}
  //     	
  //     }
	//}
 
	//@AfterClass
	//public static void tearDownAfterClass() throws Exception {
	//	em.close();
	//}
 
	@Test
	public void testFileLister() {
		 FileLister fileLister = new FileLister();
		 List<String> names = fileLister.listFileNames("F:\\Projects\\NSPO\\MCC\\05 MPSS\\10 References\\Interface Files\\In\\FDF");
		 if (names.size() == 0)
		 {
		     System.out.println("no file");
		 }
		 for (String name : names)
		 {
		     System.out.println("file name: " + name);
		 }
	}
	

	@Test
	public void tetsOpenFile() {
			FileInputStream fin;
			try {
				FileInputStream fstream = new FileInputStream("c:\\file.txt");
                DataInputStream in = new DataInputStream(fstream);
            	BufferedReader br = new BufferedReader(new InputStreamReader(in));
            	String inputLine;
            	while ((inputLine = br.readLine()) != null)   {
                    System.out.println (inputLine);
            	}
                in.close();
            }
            catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
            }
				
	}


}
	
