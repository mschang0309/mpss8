package mpss.ac.tests;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.sql.Timestamp;

import mpss.common.jpa.*;
import mpss.common.dao.*;
import mpss.oe.*;
import mpss.ac.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

//import static org.junit.Assert.*;

//import org.junit.AfterClass;
//import org.junit.BeforeClass;
import org.junit.Test;

@ContextConfiguration(locations = {"/applicationContext.xml"})
public class TestMonitoringActivityCreator extends AbstractTransactionalJUnit4SpringContextTests {

    @Autowired
    protected SatelliteDao satelliteDao;
    
    @Autowired
    protected SessionDao sessionDao;
    
    @Autowired
    protected OrbitEventsLoader orbitEventsLoader;
    
    @Autowired
    protected MonitoringActivityCreator monitoringActivityCreator;
    
	//@BeforeClass
	//public static void setUpBeforeClass() throws Exception {
  //     if (em == null) {
  //     	try
  //     	{
  //     	//EntityManagerFactory emf= Persistence.createEntityManagerFactory("MyFirstJpa");
  //         em = (EntityManager) Persistence.createEntityManagerFactory("mpss4j").createEntityManager();	
  //     	}
  //     	catch (Exception ex)
  //     	{
  //     		System.out.print(ex.getMessage());
  //     	}
  //     	
  //     }
	//}
 
	//@AfterClass
	//public static void tearDownAfterClass() throws Exception {
	//	em.close();
	//}
 
	@Test
	public void testMonitoringActivityCreator() {
		// load orbit events into database (2009/02/24 ~ 2009/02/26)
		int scId = 1;
		int branchId = 0;
		orbitEventsLoader.setScid(scId);
		orbitEventsLoader.setBranchid(branchId);
	    //String revFileName="F:\\Projects\\NSPO\\MCC\\05 MPSS\\10 References\\Interface Files\\In\\FDF\\ROCSAT2_056_061_revs.rpt";
	    //String consFileName="F:\\Projects\\NSPO\\MCC\\05 MPSS\\10 References\\Interface Files\\In\\FDF\\ROCSAT2_056_061_cons.rpt";
	    //orbitEventsLoader.Load(revFileName, consFileName);
		
		// get current satellite
		Satellite satellite = satelliteDao.get((long)scId);
		// get current session (day055)
		Session session = sessionDao.getSession("day055");
		
		// generate monitoring activities based on contacts
		//MonitoringActivityCreator mac = new MonitoringActivityCreator(satellite, session);
		monitoringActivityCreator.setSatellite(satellite);
		monitoringActivityCreator.setSession(session);
		List<Activity> acts = monitoringActivityCreator.create("");
		for (Activity a : acts)
		{
			System.out.printf("act name: %s\n", a.getName());
		}

	}
	
}
	
