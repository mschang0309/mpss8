package mpss.sv.tests;

import com.google.common.collect.ImmutableList;

import mpss.common.dao.*;
import mpss.common.jpa.Contact;
import mpss.common.jpa.Extent;
import mpss.common.jpa.Session;
import mpss.pg.*;
import mpss.schedule.Schedule;
import mpss.sv.Validator;
import mpss.util.timeformat.TimeRange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@ContextConfiguration(locations = {"/applicationContext.xml"})
public class TestSV extends AbstractTransactionalJUnit4SpringContextTests {

	
	Map<String, Integer> dataTypeCount = new HashMap<String, Integer>();
	List<String> pms = ImmutableList.of("PN", "CSV", "PM","SFG","SD","PN","PN","PM","");
	
 
  @Autowired
  protected SatelliteDao satelliteDao;
	
	@Autowired
	ContactDao contDao;
  @Autowired
  protected PsDao psDao;
  
  @Autowired
  SessionDao sessionDao;
  
  @Autowired
  protected ExtentDao extentDao;

  @Autowired
  protected ActivityDao activityDao;

  @Autowired
  ScheduleProductSession session;
	
  @Autowired
  Validator validator;
  
  @Test
	public void testCold() {

		Schedule sch=new Schedule();
		sch.runCold("day019");		
		System.out.println("asdasd");
		
		//sch.runCold("day020");		
		//System.out.println("asdasd");
		
	}
	
  @Test
  public void testMapRange() throws Exception {
	  Map<TimeRange, Integer> MapRange = new HashMap<TimeRange, Integer>();
	  List<Session> ss = sessionDao.getAll();
	  TimeRange tRange = new TimeRange(Timestamp.valueOf("2014-01-19 13:40:10"),Timestamp.valueOf("2014-01-20 07:05:09"));
	  boolean isFind = false;
	  for(Session s: ss)
	  {
		  TimeRange tr = new TimeRange(s.getStarttime(),s.getLastsave());
		  MapRange.put(tr, s.getId().intValue());
	  }
	  
	  System.out.println("tRange :" + tRange.first + " ~ " + tRange.second);
	  for (TimeRange t: MapRange.keySet())
	  {
		  System.out.println(t.first + " ~ " + t.second);
		  if (t.first.equals(tRange.first) && t.second.equals(tRange.second))
			  isFind = true;
	  }
	  
	  System.out.println("Find status : " + isFind);
  }
  
  @Test
  public void testGroupXmitExtentsIntoSequences() throws Exception {
	  Map<Integer, Set<Extent>> contExtentsMap = new HashMap<Integer, Set<Extent> >();
	  List<Extent> esList = extentDao.getAll();
	  for(Extent ext : esList)
	  {
		  if(ext.getContactid() > 0)
		  {
			  Contact contact = contDao.get((long)ext.getContactid());
			  int contId = contact.getId().intValue();
				
				if (contExtentsMap.get(contId) == null)
	        	{
					contExtentsMap.put(contId, new HashSet<Extent>());
	        	}
				Set<Extent> extSet = contExtentsMap.get(contId);
				try
				{
					extSet.add(ext);
				}
				catch (Exception e)
				{
					System.out.println("Can not add contExtentsMap (" + e.getMessage() + ")");
				}
		  }
		  
		  for(int i : contExtentsMap.keySet())
		  {
			  //Contact c = contDao.get((long)i);
			  System.out.println("contact id: " + i);
			  Set<Extent> exts= contExtentsMap.get(i);
			  for(Extent e : exts)
			  {
				  System.out.println("Extent Set: " + e.getStarttime());
			  }
			  
			  Extent[] eeArray = exts.toArray(new Extent[exts.size()]);
			  //Arrays.sort(eeArray);
			  Arrays.sort(eeArray, new Comparator<Extent>() {
					@Override
					public int compare(Extent e1, Extent e2) {
						return e1.getStarttime().compareTo(e2.getStarttime());
					}
				});
			  
			  System.out.println("firstExtent: " + eeArray[0].getStarttime());
			  for(Extent s : eeArray)
			  {
				  System.out.println("Extent set to array: " + s.getStarttime());
			  }
			  System.out.println("lastExtent: " + eeArray[exts.size()-1].getStarttime());
		  }
		  
	  }
  }
  
  @Test
  public void SetToArray() throws Exception {

	  List<Session> ssList = sessionDao.getAll();
	  Set<Session> ssSet = new HashSet<Session>();
	  Set<Session> ssSortSet = new HashSet<Session>();
	  for(Session s : ssList)
	  {
		  System.out.println("no sort session List: " + s.getLastsave());
		  ssSet.add(s);
	  }
	  
	  for(Session s : ssSet)
	  {
		  System.out.println("no sort session Set: " + s.getLastsave());
	  }
	  
		Collections.sort(ssList, new Comparator<Session>() {
		
		public int compare(Session s1, Session s2) {
			return s1.getLastsave().compareTo(s2.getLastsave());
		}
	});
		for(Session s : ssList)
		{
			System.out.println("sort session List: " + s.getLastsave());
			ssSortSet.add(s);
		}
			
		for(Session s : ssSortSet)
		  {
			  System.out.println("sort session Set: " + s.getLastsave());
		  }
		
	  Session[] ssArray = ssSet.toArray(new Session[ssSet.size()]);
	  
	  for(Session s : ssArray)
	  {
		  System.out.println("session set to array: " + s.getLastsave());
	  }
  }
 

    
    
}
	
